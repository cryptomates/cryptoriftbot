# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added
- Implemented service management via applications GUI.
- Implemented "ECB ForeignExchange" service.
- Implemented services section in settings file.

### Changed
- Improved handling of web requests.
- Improved coin stream record: Now compressing files.
- Improved coin stream record: Reduced filesize (Not storing redundant candles anymore).
- Improved coin stream record: Implemented candle-preview when opening coin-stream records.

### Removed

### Fixed
- Fixed missing initial value of reports-directory in settings.

## [4.0.1] - 2021-03-27

### Changed

- Improved configuration files.

### Fixed

- Fixed application crash on currency update. [[1](#1)]
