﻿using CryptoRiftBot.Trading;
using CryptoRiftBot.Trading.Functions;
using CryptoRiftBot.Trading.Services;
using CryptoRiftBot.Trading.Strategy;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using Shared.Utils.Core;

namespace CryptoRiftBot.Controls
{
    public abstract class BotBaseChart : Chart
    {
        #region Constants
        public const String AREA_TITLE_CANDLES = "Candle-Sticks";
        public const String AREA_TITLE_TICKER = "Ticker";
        public const String AREA_TITLE_VARIATION = "Änderung";

        public static readonly Color SERIES_COLOR_BUY_LIMIT = Color.Green;
        public static readonly Color SERIES_COLOR_SELL_LIMIT = Color.Red;
        public static readonly Color SERIES_COLOR_PRICE = Color.DarkBlue;
        public static readonly Color SERIES_COLOR_BUY_AVERAGE_RATE = Color.DeepSkyBlue;

        protected static readonly Font FONT_TEXT = new Font(FontFamily.GenericSansSerif, 8.25F, FontStyle.Regular);
        protected static readonly Font FONT_HEADLINE = new Font(FontFamily.GenericSansSerif, 10, FontStyle.Bold);

        public const int CANDLE_INDEX_HIGH = 0;
        public const int CANDLE_INDEX_LOW = 1;
        public const int CANDLE_INDEX_OPEN = 2;
        public const int CANDLE_INDEX_CLOSE = 3;

        public const int RANGE_INDEX_LOW = 0;
        public const int RANGE_INDEX_HIGH = 1;

        public const String INDICATOR_LINE_TENSION = "0.5";

        public const double SCALE_FACTOR_TICKER = 0.006;
        public const double SCALE_FACTOR_CANDLES_SMALL_VAL = 0.92;
        public const double SCALE_FACTOR_CANDLES_LARGE_VAL = 0.99;

        public const int STRIPLINE_ALPHA_DEFAULT = 16;
        public const int STRIPLINE_ALPHA_STRONG = 64;

        public const int STRIPLINE_COUNT = 3;
        public const int STRIPLINE_RIFT_NEXT_BUY_LIMIT = 0;
        public const int STRIPLINE_RIFT_NEXT_SELL_LIMIT = 1;
        public const int STRIPLINE_NEUTRAL_ZONE = 2;
        #endregion
        #region Enumerations
        public enum BotSeriesType
        {
            [Description("Handelsvolumen")]
            SIDT_Candles_Volume,
            [Description("Candle-Sticks")]
            SIDT_Candles_Sticks,
            [Description("")]
            SIDT_Candles_Average1,
            [Description("")]
            SIDT_Candles_Average2,

            [Description("Kurs")]
            SIDT_Ticker_Price,
            [Description("Durchschn. Einkaufskurs")]
            SIDT_Ticker_BuyAverageRate,
            [Description("Grenzwert Nachkaufen")]
            SIDT_Ticker_AverageDownLimit,
            [Description("Grenzwert Kaufen")]
            SIDT_Ticker_NextBuyLimit,
            [Description("Grenzwert Verkaufen")]
            SIDT_Ticker_NextSellLimit,
            [Description("Grenzwert Panik-Leerverkauf")]
            SIDT_Ticker_PanicsellLimit,
            [Description("Min. Profit")]
            SIDT_Ticker_MinProphitSellRate,

            [Description("")]
            SIDT_Variation_Value,
            [Description("Tendenz")]
            SIDT_Variation_Tendency
        }
        #endregion


        #region Types
        // Identifikation von Chart-Serien.
        public class BotSeriesID
        {
            public BotSeriesID(BotSeriesType tType, BotBaseIndicator bRefIndicator = null)
            {
                this.Type = tType;
                this.ReferredIndicator = bRefIndicator;

                // Ggf. Referenz auf Indikator sicherstellen:
                Debug.Assert((Enum.GetValues(typeof(BotSeriesType)).Length == 13), "[SID1043] Ggf. neue Series berücksichtigen.");

                switch (tType)
                {
                    case BotSeriesType.SIDT_Candles_Average1:
                    case BotSeriesType.SIDT_Candles_Average2:
                    case BotSeriesType.SIDT_Variation_Value:
                    case BotSeriesType.SIDT_Variation_Tendency:
                        Debug.Assert((bRefIndicator != null), "[SID1048] Indikator-Referenz erforderlich!");
                        break;
                }
            }


            #region Properties.Management
            [Browsable(false)]
            public BotSeriesType Type { private set; get; }
            #endregion
            #region Properties.References
            [Browsable(false)]
            public BotBaseIndicator ReferredIndicator { private set; get; }
            #endregion
        }
        #endregion


        public BotBaseChart()
        {

        }


        #region Properties.Management
        public virtual bool IsValid { get { return (true); } }
        #endregion
        #region Properties
        public int Decimals { private set; get; } = 1;
        #endregion


        #region Events
        protected virtual void OnInit()
        {

        }
        #endregion


        #region Initialization
        public void Init()
        {
            BeginInit();

            // Initialize:
            OnInit();

            // Finalize:
            foreach (ChartArea _cArea in ChartAreas)
            {
                switch (_cArea.Name)
                {
                    case AREA_TITLE_CANDLES:
                        // Sekundäre Y-Achsen:
                        Enable_YAxis2(_cArea);

                        // Achsen-Farben (Chart "Candles"):
                        var _sSeries = FindSeries(BotSeriesType.SIDT_Candles_Volume);

                        _cArea.AxisY2.LabelStyle.ForeColor = Color.FromArgb(255, _sSeries.Color);

                        // X-Achsen-Intervall:
                        _cArea.AxisX.IntervalAutoMode = IntervalAutoMode.FixedCount;
                        _cArea.AxisX.IntervalType = DateTimeIntervalType.Hours;
                        _cArea.AxisX.Interval = 6;
                        break;

                    case AREA_TITLE_VARIATION:
                        // Sekundäre Y-Achsen:
                        Enable_YAxis2(_cArea);

                        // Margin:
                        _cArea.AxisX.IsMarginVisible = false;

                        break;
                }
            }

            EndInit();
        }
        protected ChartArea CreateArea(string sTitle, bool bShowTitle = true)
        {
            ChartArea cArea = new ChartArea(sTitle);

            cArea.AxisX.MajorGrid.LineColor = Color.LightGray;
            cArea.AxisY.MajorGrid.LineColor = Color.LightGray;
            cArea.CursorX.IsUserEnabled = true;
            cArea.CursorX.IsUserSelectionEnabled = true;
            cArea.CursorY.IsUserEnabled = true;
            cArea.CursorY.IsUserSelectionEnabled = true;

            // X-Achse:
            cArea.AxisX.LabelStyle.Format = "{0:HH:mm}";

            // Y-Achse:
            cArea.AxisY.TitleFont = FONT_HEADLINE;
            if (bShowTitle)
                cArea.AxisY.Title = cArea.Name;

            ChartAreas.Add(cArea);
            return (cArea);
        }
        protected Legend CreateLegend(ChartArea cOwner, Docking dDocking)
        {
            Legend lLegend = new Legend(cOwner.Name);

            lLegend.Alignment = StringAlignment.Far;
            lLegend.IsTextAutoFit = true;
            lLegend.BackColor = Color.Transparent;
            lLegend.Docking = dDocking;
            lLegend.IsDockedInsideChartArea = false;
            lLegend.DockedToChartArea = cOwner.Name;
            lLegend.Font = FONT_TEXT;

            Legends.Add(lLegend);
            return (lLegend);
        }
        protected void ResetData(string[] sAreas = null)
        {
            // Inhalte entfernen:
            Annotations.Clear();

            foreach (Series _sSeries in Series)
            {
                if ((sAreas != null) && (!sAreas.Contains(_sSeries.ChartArea)))
                    // Skip area:
                    continue;
                else
                    _sSeries.Points.Clear();
            }

            // Achsen zurücksetzen:
            foreach (ChartArea _cArea in ChartAreas)
            {
                _cArea.AxisY.Minimum = Double.NaN;
                _cArea.AxisY.Maximum = Double.NaN;

                _cArea.RecalculateAxesScale();
            }
        }
        #endregion
        #region Management
        protected Series AddSource(String sChartArea, BotSeriesType bType)
        {
            Series sResult;
            switch (bType)
            {
                case BotSeriesType.SIDT_Candles_Volume:
                    sResult = AddSource(sChartArea, new BotSeriesID(BotSeriesType.SIDT_Candles_Volume), Color.FromArgb(128, Color.LightSteelBlue), SeriesChartType.Column);
                    break;
                case BotSeriesType.SIDT_Candles_Sticks:
                    Color _cClrPrice = Color.FromArgb(128, BotTradeChart.SERIES_COLOR_PRICE);
                    sResult = AddSource(sChartArea, new BotSeriesID(BotSeriesType.SIDT_Candles_Sticks), _cClrPrice, SeriesChartType.Candlestick);
                    sResult.Color = _cClrPrice;
                    sResult.BorderColor = _cClrPrice;
                    sResult["PriceUpColor"] = "MediumAquamarine";
                    sResult["PriceDownColor"] = "LightCoral";
                    break;

                case BotSeriesType.SIDT_Ticker_MinProphitSellRate:
                    sResult = AddSource(sChartArea, new BotSeriesID(BotSeriesType.SIDT_Ticker_MinProphitSellRate), Color.LightGreen, SeriesChartType.StepLine);
                    break;
                case BotSeriesType.SIDT_Ticker_PanicsellLimit:
                    sResult = AddSource(sChartArea, new BotSeriesID(BotSeriesType.SIDT_Ticker_PanicsellLimit), Color.FromArgb(64, BotTradeChart.SERIES_COLOR_SELL_LIMIT), SeriesChartType.StepLine);
                    break;
                case BotSeriesType.SIDT_Ticker_NextBuyLimit:
                    sResult = AddSource(sChartArea, new BotSeriesID(BotSeriesType.SIDT_Ticker_NextBuyLimit), BotTradeChart.SERIES_COLOR_BUY_LIMIT, SeriesChartType.StepLine);
                    break;
                case BotSeriesType.SIDT_Ticker_NextSellLimit:
                    sResult = AddSource(sChartArea, new BotSeriesID(BotSeriesType.SIDT_Ticker_NextSellLimit), BotTradeChart.SERIES_COLOR_SELL_LIMIT, SeriesChartType.StepLine);
                    break;
                case BotSeriesType.SIDT_Ticker_AverageDownLimit:
                    sResult = AddSource(sChartArea, new BotSeriesID(BotSeriesType.SIDT_Ticker_AverageDownLimit), Color.MediumAquamarine, SeriesChartType.StepLine);
                    break;
                case BotSeriesType.SIDT_Ticker_BuyAverageRate:
                    sResult = AddSource(sChartArea, new BotSeriesID(BotSeriesType.SIDT_Ticker_BuyAverageRate), BotTradeChart.SERIES_COLOR_BUY_AVERAGE_RATE, SeriesChartType.StepLine);
                    break;

                case BotSeriesType.SIDT_Ticker_Price:
                    sResult = AddSource(sChartArea, new BotSeriesID(BotSeriesType.SIDT_Ticker_Price), BotTradeChart.SERIES_COLOR_PRICE, SeriesChartType.StepLine);
                    break;

                default:
                    throw new NotImplementedException("Unexpected series type!");
            }
            return (sResult);
        }
        protected Series AddSource(String sChartArea, BotSeriesID sID, Color cColor, SeriesChartType sChartType = SeriesChartType.Line)
        {
            return (AddSource(sChartArea, sID, UtlEnum.GetEnumDescription(sID.Type), cColor, sChartType));
        }
        protected Series AddSource(String sChartArea, BotSeriesID sID, String sTitle, Color cColor, SeriesChartType sChartType = SeriesChartType.Line)
        {
            Series sSeries = new Series(sTitle);

            sSeries.Tag = sID;
            sSeries.ChartType = sChartType;
            sSeries.ChartArea = sChartArea;
            sSeries.XValueType = ChartValueType.DateTime;
            sSeries.Legend = sChartArea;
            sSeries.Color = cColor;
            sSeries.IsVisibleInLegend = (sTitle.Length > 0);

            Series.Add(sSeries);
            return (sSeries);
        }
        public Series FindSeries(BotSeriesType tType, BotBaseIndicator bRefIndicator = null)
        {
            BotSeriesID sID;
            foreach (Series _sSeries in Series)
            {
                sID = (BotSeriesID)_sSeries.Tag;
                if ((sID.Type == tType) && (sID.ReferredIndicator == bRefIndicator))
                    return (_sSeries);
            }
            return (null);
        }
        #endregion
        #region Management.Data
        public void SetAxisLimits(ChartArea cArea, double dMin, double dMax, int iGain = 1)
        {
            double dFactor;
            if (dMin < 1)
            {
                dFactor = SCALE_FACTOR_CANDLES_SMALL_VAL;
                Decimals = BotConvention.NUM_SMALL_DOUBLE_DECIMALS;
            }
            else
            {
                dFactor = SCALE_FACTOR_CANDLES_LARGE_VAL;
                Decimals = 1;
            }
            cArea.AxisY.Minimum = UtlMath.Round((dMin * dFactor), UtlMath.Direction.Down, iGain);
            cArea.AxisY.Maximum = UtlMath.RoundEx((dMax * (1 / dFactor)), UtlMath.Direction.Up);
        }
        public void Enable_YAxis2(ChartArea cArea)
        {
            cArea.AxisY2.Enabled = AxisEnabled.True;
            cArea.AxisY2.LineColor = Color.Transparent;
            cArea.AxisY2.MajorGrid.Enabled = false;
            cArea.AxisY2.IsStartedFromZero = cArea.AxisY.IsStartedFromZero;
        }
        #endregion
        #region Management.Update
        protected void Update(BotCandleSubscription cCandles)
        {
            Update(cCandles, FindSeries(BotSeriesType.SIDT_Candles_Sticks), FindSeries(BotSeriesType.SIDT_Candles_Volume));
        }
        protected void Update(BotCandleSubscription cCandles, Series sCandleSeries, Series sVolumeSeries)
        {
            Debug.Assert((sCandleSeries != null), "[U360]");
            Debug.Assert((sVolumeSeries != null), "[U361]");

            lock (cCandles.oThreadLockObj)
            {
                BotCandleSubscription.BotCandleData _bCandle;

                Debug.Assert((sCandleSeries.Points.Count == sVolumeSeries.Points.Count), "[U260] Datenpunkt-Anzahl unterschiedlich!");

                // Candle-Sticks und Volumen einfügen:
                DataPoint _dPoint = null;
                int _iIndex = -1, iStart = Math.Max(0, (cCandles.lData.Count - BotConvention.DISPLAY_MAX_CANDLE_COUNT));
                double _dXVal;

                for (int i = iStart; i < cCandles.lData.Count; i++)
                {
                    _bCandle = cCandles.lData[i];
                    dLastCandleBegin = UtlDateTime.UTCTimeStampToDateTime(_bCandle.TimeStamp);
                    _dXVal = dLastCandleBegin.ToOADate();

                    if ((i == iStart) || (_bCandle.dHigh > dMaxCandleVal))
                        dMaxCandleVal = _bCandle.dHigh;
                    if ((i == iStart) || (_bCandle.dLow < dMinCandleVal))
                        dMinCandleVal = _bCandle.dLow;

                    if (_iIndex == -1)
                        _dPoint = sCandleSeries.Points.FirstOrDefault(dPoint => (dPoint.XValue == _dXVal));
                    else
                    {
                        _iIndex++;
                        if (_iIndex < sCandleSeries.Points.Count)
                            _dPoint = sCandleSeries.Points[_iIndex];
                        else
                            _dPoint = null;
                    }

                    if (_dPoint == null)
                    {
                        _iIndex = sCandleSeries.Points.AddXY(dLastCandleBegin, 1);
                        sVolumeSeries.Points.AddXY(dLastCandleBegin, 1);
                    }
                    else
                        _iIndex = sCandleSeries.Points.IndexOf(_dPoint);

                    // Candle-Sticks:
                    sCandleSeries.Points[_iIndex].XValue = _dXVal;
                    sCandleSeries.Points[_iIndex].YValues[CANDLE_INDEX_HIGH] = _bCandle.dHigh;
                    sCandleSeries.Points[_iIndex].YValues[CANDLE_INDEX_LOW] = _bCandle.dLow;
                    sCandleSeries.Points[_iIndex].YValues[CANDLE_INDEX_OPEN] = _bCandle.dOpen;
                    sCandleSeries.Points[_iIndex].YValues[CANDLE_INDEX_CLOSE] = _bCandle.dClose;

                    // Volume:
                    sVolumeSeries.Points[_iIndex].XValue = _dXVal;
                    sVolumeSeries.Points[_iIndex].SetValueY(_bCandle.dVolume);
                }

                // Skala anpassen:
                SetAxisLimits(ChartAreas[sCandleSeries.ChartArea], dMinCandleVal, dMaxCandleVal);
            }
        }
        #endregion
        #region Helper
        protected String GetChartToolTipValue(BotSeriesID sID, DataPoint dPoint)
        {
            switch (sID.Type)
            {
                case BotSeriesType.SIDT_Candles_Sticks:
                    return (String.Format("{0}- High: {1}{0}- Open: {2}{0}- Close: {3}{0}- Low: {4}", Environment.NewLine, dPoint.YValues[BotTradeChart.CANDLE_INDEX_HIGH], dPoint.YValues[BotTradeChart.CANDLE_INDEX_OPEN], dPoint.YValues[BotTradeChart.CANDLE_INDEX_CLOSE], dPoint.YValues[BotTradeChart.CANDLE_INDEX_LOW]));
                case BotSeriesType.SIDT_Variation_Value:
                    return (String.Format("{0}", dPoint.YValues[BotTradeChart.RANGE_INDEX_HIGH]));
            }
            return (String.Format("{0}", dPoint.YValues[0]));
        }
        #endregion


        protected DateTime dLastCandleBegin;
        protected double dMinCandleVal = 0;
        protected double dMaxCandleVal = 0;
    }
    public class BotCandleChart : BotBaseChart
    {
        public BotCandleChart()
        {
        }


        #region Events
        protected override void OnInit()
        {
            // Area:
            var _cArea = CreateArea(AREA_TITLE_CANDLES, false);
            _cArea.BackColor = this.BackColor;

            // Add series:
            AddSource(AREA_TITLE_CANDLES, BotSeriesType.SIDT_Candles_Sticks);
            AddSource(AREA_TITLE_CANDLES, BotSeriesType.SIDT_Candles_Volume);
        }
        #endregion
        #region Initialization
        public void ResetData()
        {
            base.ResetData();
        }
        #endregion
        #region Update
        public void UpdateChart(BotCandleSubscription cCandles)
        {
            Update(cCandles);
        }
        #endregion
    }
    public class BotTradeChart : BotBaseChart
    {
        #region Constants
        public const int AREA_COUNT = 3;
        public const int AREA_INDEX_CANDLES = 0;
        public const int AREA_INDEX_TICKER = 1;
        public const int AREA_INDEX_INDICATOR = 2;
        #endregion


        public BotTradeChart(BotTradingStrategy bStrategy)
        {
            ReferredStrategy = bStrategy;

            MouseClick += new MouseEventHandler(this.OnMouseClick);
        }


        #region Properties.Management
        public override bool IsValid { get { return ((base.IsValid) && (ReferredStrategy != null)); } }
        #endregion
        #region Properties.References
        public BotTradingStrategy ReferredStrategy { private set; get; }
        #endregion


        #region Events
        protected override void OnInit()
        {
            ChartArea[] cArea = new ChartArea[AREA_COUNT];
            String[] sTitles = new String[AREA_COUNT];
            bool bNewInit = (ChartAreas.Count != AREA_COUNT);

            Debug.Assert(IsValid, "[I91]");

            // Aktuellen Inhalt zurücksetzen:
            ResetData();
            Series.Clear();

            // Initialisierung:
            sTitles[AREA_INDEX_CANDLES] = AREA_TITLE_CANDLES;
            sTitles[AREA_INDEX_TICKER] = AREA_TITLE_TICKER;
            sTitles[AREA_INDEX_INDICATOR] = AREA_TITLE_VARIATION;

            for (int i = 0; i < AREA_COUNT; i++)
            {
                if (bNewInit)
                {
                    cArea[i] = CreateArea(sTitles[i]);
                    CreateLegend(cArea[i], ((i == AREA_INDEX_INDICATOR) ? Docking.Bottom : Docking.Top));
                }
                else
                    cArea[i] = ChartAreas[i];
            }

            if (bNewInit)
            {
                GetToolTipText += new EventHandler<ToolTipEventArgs>(OnGetToolTipText);

                // Strip-Lines:
                for (int i = 0; i < STRIPLINE_COUNT; i++)
                {
                    dStripLine[i] = new StripLine();
                    cArea[AREA_INDEX_CANDLES].AxisY.StripLines.Add(dStripLine[i]);
                }

                dStripLine[STRIPLINE_RIFT_NEXT_BUY_LIMIT].BackColor = Color.FromArgb(BotTradeChart.STRIPLINE_ALPHA_DEFAULT, SERIES_COLOR_BUY_LIMIT);
                dStripLine[STRIPLINE_RIFT_NEXT_SELL_LIMIT].BackColor = Color.FromArgb(BotTradeChart.STRIPLINE_ALPHA_DEFAULT, SERIES_COLOR_SELL_LIMIT);
                dStripLine[STRIPLINE_NEUTRAL_ZONE].BackColor = Color.FromArgb(BotTradeChart.STRIPLINE_ALPHA_STRONG, Color.Silver);
            }

            // Inhalt von Trading-Strategie initialisieren lassen:
            InitChart();

            // Charts aneinander ausrichten (Chart "Ticker" und "Indicator"):
            cArea[AREA_INDEX_TICKER].AxisX.LabelStyle.Enabled = false;

            cArea[AREA_INDEX_INDICATOR].AlignmentOrientation = AreaAlignmentOrientations.Vertical;
            cArea[AREA_INDEX_INDICATOR].AlignmentStyle = AreaAlignmentStyles.All;
            cArea[AREA_INDEX_INDICATOR].AlignWithChartArea = AREA_TITLE_TICKER;

            // Definieren, auf welchen Series ausgeführte Orders dargestellt werden.
            lAnotationSeries.Clear();

            lAnotationSeries.Add(FindSeries(BotSeriesType.SIDT_Ticker_Price));
            lAnotationSeries.Add(FindSeries(BotSeriesType.SIDT_Candles_Sticks));
        }
        #endregion
        #region Initialization
        public void ResetData()
        {
            base.ResetData(new string[] { AREA_TITLE_TICKER, AREA_TITLE_VARIATION });
        }
        #endregion
        #region Management.Data
        public void SetStripLine(int iIndex, double MinVal, double dMaxVal, bool bHatched = false)
        {
            if ((MinVal == 0) || (dMaxVal == 0) || (dMaxVal < MinVal))
            {
                dStripLine[iIndex].IntervalOffset = 0;
                dStripLine[iIndex].StripWidth = 0;
            }
            else
            {
                dStripLine[iIndex].BackHatchStyle = (bHatched ? ChartHatchStyle.BackwardDiagonal : ChartHatchStyle.None);
                dStripLine[iIndex].IntervalOffset = MinVal;
                dStripLine[iIndex].StripWidth = (dMaxVal - MinVal);
            }
        }
        public void SetStripLine(int iIndex, double dLimit, bool bHatched = false, bool bInvers = false)
        {
            double dMaxVal = 0;
            if (dLimit != 0)
            {
                if (bInvers)
                {
                    dMaxVal = dLimit;
                    dLimit = ChartAreas[AREA_INDEX_CANDLES].AxisY.Minimum;
                }
                else
                    dMaxVal = ChartAreas[AREA_INDEX_CANDLES].AxisY.Maximum;
            }
            SetStripLine(iIndex, dLimit, dMaxVal, bHatched);
        }
        #endregion
        #region Management.Update
        protected void Update(BotTickerSubscription tTicker)
        {
            // Prüfen dass Ticker-Daten existieren:
            // > Es kann vorkommen dass Verbindung zwischenzeitlich getrennt wurde und die Ticker-Daten somit zurückgesetzt sind!
            if (tTicker.lData.Count > 0)
            {
                ChartArea cAreaCandles = ChartAreas[AREA_INDEX_CANDLES];
                ChartArea cAreaTicker = ChartAreas[AREA_INDEX_TICKER];
                ChartArea cAreaIndicators = ChartAreas[AREA_INDEX_INDICATOR];
                BotCandleSubscription cCandleSubscr = BotCurrencyRating.GetCandles(ReferredStrategy);
                BotCandleSubscription.BotCandleData cLastCandleData = null;
                BotTickerSubscription.BotTickerData tLastTickerData = tTicker.lData.Last();
                bool _bResult, bResetAutoVal = false, bNewPoint = false;
                int _iMaxCount;
                double dMinVal, dMaxVal;
                String sCandleToolTip = "";
                DataPoint _dPoint;

                // Initialisieren:
                dMinVal = tLastTickerData.CurrentValue;
                dMaxVal = tLastTickerData.CurrentValue;

                // [ARCHIV]:
                // Folgender Code führte manchmal zu einem Dead-Lock!
                // Archivierter Code wurde durch ein "Try-Lock" ersetzt:
                // lock (ThreadLock.Object[ThreadLock.OBJ_CURRENCY_RATING])

                if (Monitor.TryEnter(BotThreadLock.Object[BotThreadLock.OBJ_CURRENCY_RATING]))
                {
                    try
                    {
                        // Prüfen ob neuer Datenpunkt erstellt werden muss:
                        if ((cCandleSubscr != null) && (cCandleSubscr.lData.Count > 0))
                        {
                            DateTime _dTime;

                            cLastCandleData = cCandleSubscr.lData.Last();
                            _dTime = UtlDateTime.UTCTimeStampToDateTime(cLastCandleData.TimeStamp);
                            if ((_dTime != dLastCandleBegin))
                            {
                                dLastCandleBegin = _dTime;
                                bNewPoint = true;

                                sCandleToolTip = GetChartToolTip();

                                if (cCandleSubscr.lData.Count > 1)
                                    // Coin-Info erstellen:
                                    // > Vorletzten Candles anwenden, da diese jetzt abgeschlossen wurden.
                                    BotDebugMode.OnCoinInfoUpdate(ReferredStrategy, cCandleSubscr.lData[cCandleSubscr.lData.Count - 2], tLastTickerData);
                            }
                        }

                        foreach (Series _sSeries in Series)
                        {
                            // Daten für neuen Intervall anfügen:
                            switch (_sSeries.ChartArea)
                            {
                                case AREA_TITLE_CANDLES:
                                    if (cLastCandleData == null)
                                        continue;
                                    else if ((bNewPoint) || (_sSeries.Points.Count == 0))
                                    {
                                        _dPoint = (bNewPoint) ? _sSeries.Points.LastOrDefault() : null;

                                        if (_dPoint != null)
                                            // Aktuellen ToolTip speichern:
                                            _dPoint.ToolTip = sCandleToolTip;

                                        _sSeries.Points.AddXY(dLastCandleBegin, 0);
                                    }
                                    break;
                                case AREA_TITLE_TICKER:
                                    _sSeries.Points.AddXY(tLastTickerData.dReceivedTime, 0);
                                    break;
                                case AREA_TITLE_VARIATION:
                                    break;

                                default:
                                    Debug.Assert(false, "[UC925]");
                                    break;
                            }

                            // Series via Strategie aktualisieren:
                            _bResult = UpdateChart(tLastTickerData, cLastCandleData, _sSeries);

                            _sSeries.BorderDashStyle = (_bResult ? ChartDashStyle.Solid : ChartDashStyle.Dash);
                            _sSeries.BackHatchStyle = (_bResult ? ChartHatchStyle.None : ChartHatchStyle.ZigZag);

                            // Anzahl an Datenpunkten begrenzen:
                            _iMaxCount = (_sSeries.ChartArea == AREA_TITLE_CANDLES) ? BotConvention.DISPLAY_MAX_CANDLE_COUNT : BotConvention.DISPLAY_MAX_TICKER_COUNT;

                            while (_sSeries.Points.Count > _iMaxCount)
                            {
                                // Ggf. referenzierende Annotations löschen:
                                for (int u = (Annotations.Count - 1); u >= 0; u--)
                                {
                                    if (Annotations[u].AnchorDataPoint == _sSeries.Points[0])
                                        Annotations.RemoveAt(u);
                                }

                                _sSeries.Points.RemoveAt(0);
                                bResetAutoVal = true;
                            }

                            // Ggf. kleinsten und größten Wert aus Ticker-Chart bestimmen:
                            if (_sSeries.ChartArea == AREA_TITLE_TICKER)
                            {
                                foreach (DataPoint dPoint in _sSeries.Points)
                                {
                                    if (dPoint.YValues[0] != 0)
                                    {
                                        dMinVal = Math.Min(dMinVal, dPoint.YValues[0]);
                                        dMaxVal = Math.Max(dMaxVal, dPoint.YValues[0]);
                                    }
                                }
                            }
                        }

                        if (bResetAutoVal)
                            ResetAutoValues();

                        // Skala (Ticker) anpassen:
                        SetAxisLimits(cAreaTicker, dMinVal, dMaxVal);

                        // Skala (Candles) anpassen:
                        dMinCandleVal = Math.Min(dMinCandleVal, tLastTickerData.CurrentValue);
                        dMaxCandleVal = Math.Max(dMaxCandleVal, tLastTickerData.CurrentValue);

                        SetAxisLimits(cAreaCandles, dMinCandleVal, dMaxCandleVal);

                        // Kurs-Simulation:
                        if (ReferredStrategy.Platform.IsSimulating)
                        {
                            IBotSimulatedPlatformService iSimulation = (IBotSimulatedPlatformService)ReferredStrategy.Platform;
                            if (iSimulation.Behaviour != BotTickerBehaviour.eManual)
                                // Aktueller Kurs als Ausgangs-Wert für manuelle Ticker-Simulation anwenden:
                                iSimulation.SetManualTickerSimValue(ReferredStrategy.TradingPair, tLastTickerData.dLastPrice);
                        }

                        // Elemente in Abhängigkeit von aktiven Settings anzeigen:
                        cAreaIndicators.Visible = (ReferredStrategy.ActiveSettingsClone != null);
                    }
                    finally
                    {
                        Monitor.Exit(BotThreadLock.Object[BotThreadLock.OBJ_CURRENCY_RATING]);
                    }
                }
            }
        }
        protected new void Update(BotCandleSubscription cCandles)
        {
            lock (cCandles.oThreadLockObj)
            {
                if (ReferredStrategy.ActiveSettingsClone != null)
                {
                    // Indikatoren aktualisieren:
                    foreach (BotBaseIndicator bIndicator in ReferredStrategy.Indicators)
                    {
                        if ((bIndicator.ReferredParameters.Enabled) && (bIndicator.ReferredParameters.SourceType == BotSourceType.eCandleSticks))
                        {
                            UpdateChart(bIndicator.mAverage1, FindSeries(BotSeriesType.SIDT_Candles_Average1, bIndicator));
                            if (bIndicator.ReferredParameters.IsDoubleAvgValue)
                                // Aktualisieren von zwei Durchschnittswerten notwendig:
                                UpdateChart(bIndicator.mAverage2, FindSeries(BotSeriesType.SIDT_Candles_Average2, bIndicator));
                        }
                    }
                }

                base.Update(cCandles);
            }
        }
        #endregion
        #region Events
        private void OnGetToolTipText(object oSender, ToolTipEventArgs eArgs)
        {
            if (ReferredStrategy != null)
            {
                Debug.Assert((eArgs.HitTestResult != null), "[GCTT2110]");

                if (eArgs.HitTestResult.ChartElementType == ChartElementType.DataPoint)
                {
                    DataPoint dTarget = eArgs.HitTestResult.Series.Points[eArgs.HitTestResult.PointIndex];

                    if (dTarget.ToolTip == "")
                        eArgs.Text = GetChartToolTip(eArgs.HitTestResult.ChartArea.Name, dTarget.XValue);
                    else
                        eArgs.Text = dTarget.ToolTip;
                }
            }
        }
        private void OnMouseClick(object oSender, MouseEventArgs eArgs)
        {
            if (eArgs.Button == MouseButtons.Right) 
            {
                if ((ReferredStrategy != null) && (ReferredStrategy.Platform.IsSimulating))
                {
                    Point pMousePos = eArgs.Location;
                    foreach (HitTestResult _hResult in HitTest(pMousePos.X, pMousePos.Y, false, ChartElementType.PlottingArea))
                    {
                        if ((_hResult.ChartArea != null) && (_hResult.ChartArea.Name == AREA_TITLE_TICKER))
                        {
                            ((IBotSimulatedPlatformService)ReferredStrategy.Platform).SetManualTickerSimValue(ReferredStrategy.TradingPair, _hResult.ChartArea.AxisY.PixelPositionToValue(pMousePos.Y));
                            return;
                        }
                    }
                }
            }
        }
        #endregion


        #region Update
        public void UpdateChart(BotTickerSubscription tTicker, BotCandleSubscription cCandles, bool bCandlesOnly = false)
        {
            Update(cCandles);
            if (!bCandlesOnly)
                Update(tTicker);
        }
        public void UpdateChart(BotTickerSubscription tTicker)
        {
            Update(tTicker);
        }
        public void UpdateChart(BotMovingAverage bAverage, Series sSeries)
        {
            Debug.Assert((sSeries != null), "[UC750]");

            int iInsertIndex = sSeries.Points.Count;
            DataPoint pLast = ((iInsertIndex == 0) ? null : sSeries.Points.Last());
            DateTime _dXVal;

            for (int i = (bAverage.lData.Count - 1); i >= 0; i--)
            {
                _dXVal = UtlDateTime.UTCTimeStampToDateTime(bAverage.lData[i].tTimeStamp);
                if ((pLast == null) || (_dXVal > DateTime.FromOADate(pLast.XValue)))
                    sSeries.Points.InsertXY(iInsertIndex, _dXVal, bAverage.lData[i].dPrice);
                else
                    break;
            }
        }
        #endregion
        #region Display.Strategy
        public void InitChart()
        {
            Series _sSeries;
            Color cClrPrice = Color.FromArgb(128, BotTradeChart.SERIES_COLOR_PRICE);

            // Schritt 1)
            // > Chart "Candles":
            _sSeries = AddSource(BotTradeChart.AREA_TITLE_CANDLES, BotSeriesType.SIDT_Candles_Volume);
            _sSeries.YAxisType = AxisType.Secondary;

            _sSeries = AddSource(BotTradeChart.AREA_TITLE_CANDLES, BotSeriesType.SIDT_Candles_Sticks);

            if (ReferredStrategy.ActiveSettingsClone != null)
            {
                foreach (BotBaseIndicator bIndicator in ReferredStrategy.Indicators)
                {
                    if (bIndicator.ReferredParameters.SourceType == BotSourceType.eCandleSticks)
                    {
                        _sSeries = AddSource(BotTradeChart.AREA_TITLE_CANDLES, new BotSeriesID(BotSeriesType.SIDT_Candles_Average1, bIndicator), bIndicator.DisplayNameAvg1, bIndicator.GraphColorAvg1);
                        _sSeries.IsVisibleInLegend = false;

                        if (bIndicator.ReferredParameters.IsDoubleAvgValue)
                        {
                            // Anzeigen von zwei Durchschnittswerten notwendig:
                            _sSeries = AddSource(BotTradeChart.AREA_TITLE_CANDLES, new BotSeriesID(BotSeriesType.SIDT_Candles_Average2, bIndicator), bIndicator.DisplayNameAvg2, bIndicator.GraphColorAvg2);
                            _sSeries.IsVisibleInLegend = false;
                        }
                    }
                }
            }

            // Schritt 2)
            // > Chart "Ticker":
            if (ReferredStrategy.ActiveSettingsClone != null)
            {
                AddSource(BotTradeChart.AREA_TITLE_TICKER, BotSeriesType.SIDT_Ticker_MinProphitSellRate);
                AddSource(BotTradeChart.AREA_TITLE_TICKER, BotSeriesType.SIDT_Ticker_PanicsellLimit);
                AddSource(BotTradeChart.AREA_TITLE_TICKER, BotSeriesType.SIDT_Ticker_NextBuyLimit);
                AddSource(BotTradeChart.AREA_TITLE_TICKER, BotSeriesType.SIDT_Ticker_NextSellLimit);
                AddSource(BotTradeChart.AREA_TITLE_TICKER, BotSeriesType.SIDT_Ticker_AverageDownLimit);
                AddSource(BotTradeChart.AREA_TITLE_TICKER, BotSeriesType.SIDT_Ticker_BuyAverageRate);
            }

            AddSource(BotTradeChart.AREA_TITLE_TICKER, BotSeriesType.SIDT_Ticker_Price);

            // Schritt 3)
            // > Chart "Indicator":
            if (ReferredStrategy.ActiveSettingsClone != null)
            {
                foreach (BotBaseIndicator bIndicator in ReferredStrategy.Indicators)
                {
                    _sSeries = AddSource(BotTradeChart.AREA_TITLE_VARIATION, new BotSeriesID(BotSeriesType.SIDT_Variation_Value, bIndicator), bIndicator.DisplayName, Color.FromArgb(64, bIndicator.GraphColorAvg1), SeriesChartType.SplineRange);
                    _sSeries["LineTension"] = BotTradeChart.INDICATOR_LINE_TENSION;

                    _sSeries = AddSource(BotTradeChart.AREA_TITLE_VARIATION, new BotSeriesID(BotSeriesType.SIDT_Variation_Tendency, bIndicator), ("Tendenz " + bIndicator.DisplayName), Color.FromArgb(196, bIndicator.GraphColorAvg1), SeriesChartType.Line);
                    _sSeries.YAxisType = AxisType.Secondary;
                    _sSeries.IsVisibleInLegend = false;
                }
            }
        }
        public bool UpdateChart(BotTickerSubscription.BotTickerData tLastTickerData, BotCandleSubscription.BotCandleData cLastCandleData, Series sSeries)
        {
            BotSeriesID sID = (BotSeriesID)sSeries.Tag;
            DataPoint dPoint;
            bool bResult = true;

            switch (sID.Type)
            {
                case BotSeriesType.SIDT_Candles_Volume:
                    break;

                case BotSeriesType.SIDT_Candles_Sticks:
                    dPoint = sSeries.Points.Last();

                    dPoint.YValues[BotTradeChart.CANDLE_INDEX_OPEN] = cLastCandleData.dOpen;
                    dPoint.YValues[BotTradeChart.CANDLE_INDEX_LOW] = cLastCandleData.dLow;
                    dPoint.YValues[BotTradeChart.CANDLE_INDEX_HIGH] = cLastCandleData.dHigh;
                    dPoint.YValues[BotTradeChart.CANDLE_INDEX_CLOSE] = cLastCandleData.dClose;

                    break;

                case BotSeriesType.SIDT_Ticker_Price:
                    ChartArea cArea = ChartAreas[BotTradeChart.AREA_INDEX_TICKER];

                    sSeries.Points.Last().SetValueY(tLastTickerData.dLastPrice);

                    // Neutrale Zone:
                    double dMin = 0, dMax = 0;

                    if (ReferredStrategy.ActiveSettingsClone != null)
                    {
                        dMin = ReferredStrategy.Rift_SellInfo.NeutralZone.LimitMin;
                        dMax = ReferredStrategy.Rift_SellInfo.NeutralZone.LimitMax;
                    }

                    SetStripLine(BotTradeChart.STRIPLINE_NEUTRAL_ZONE, dMin, dMax);

                    break;

                case BotSeriesType.SIDT_Ticker_BuyAverageRate:
                    double dAvgRate = ReferredStrategy.RiftSummaries.CurrentRift.Buys.TotalAverageRate;

                    sSeries.Points.Last().SetValueY(dAvgRate);

                    bResult = (dAvgRate > 0);
                    break;

                case BotSeriesType.SIDT_Ticker_MinProphitSellRate:
                    sSeries.Points.Last().SetValueY(ReferredStrategy.Calc_Rift_MinProphitSellRate);

                    bResult = (ReferredStrategy.Calc_Rift_MinProphitSellRate != 0);
                    break;

                case BotSeriesType.SIDT_Ticker_PanicsellLimit:
                    sSeries.Points.Last().SetValueY(ReferredStrategy.Calc_Rift_PanicShortSale_ExecuteRate);

                    bResult = (ReferredStrategy.Calc_Rift_PanicShortSale_ExecuteRate != 0);
                    break;

                case BotSeriesType.SIDT_Ticker_NextBuyLimit:
                    sSeries.Points.Last().SetValueY(ReferredStrategy.Rift_BuyInfo.NextOrderLimit.LimitValue);
                    bResult = ReferredStrategy.BuySlow_LimitEnabled;

                    SetStripLine(BotTradeChart.STRIPLINE_RIFT_NEXT_BUY_LIMIT, ReferredStrategy.Rift_BuyInfo.NextOrderLimit.LimitValue, !bResult);

                    break;

                case BotSeriesType.SIDT_Ticker_NextSellLimit:
                    sSeries.Points.Last().SetValueY(ReferredStrategy.Rift_SellInfo.NextOrderLimit.LimitValue);
                    bResult = ReferredStrategy.SellSlow_LimitEnabled;

                    SetStripLine(BotTradeChart.STRIPLINE_RIFT_NEXT_SELL_LIMIT, ReferredStrategy.Rift_SellInfo.NextOrderLimit.LimitValue, !bResult, true);
                    break;

                case BotSeriesType.SIDT_Ticker_AverageDownLimit:
                    sSeries.Points.Last().SetValueY(ReferredStrategy.Calc_Buy_AverageDownRate);

                    bResult = (ReferredStrategy.Calc_Buy_AverageDownRate != 0);
                    break;

                default:
                    // Indikatoren anwenden:
                    Debug.Assert((sID.ReferredIndicator != null), "[UC170] Referenz auf Indikator erforderlich!");

                    // Graph in Abhängigkeit zu Indikator de-/aktivieren:
                    sSeries.Enabled = sID.ReferredIndicator.ReferredParameters.Enabled;

                    if (sSeries.Enabled)
                    {
                        switch (sID.Type)
                        {
                            case BotSeriesType.SIDT_Candles_Average1:
                                sSeries.Points.Last().SetValueY((sID.ReferredIndicator.IsValid) ? sID.ReferredIndicator.ValueAvg1 : double.NaN);
                                break;

                            case BotSeriesType.SIDT_Candles_Average2:
                                sSeries.Points.Last().SetValueY((sID.ReferredIndicator.IsValid) ? sID.ReferredIndicator.ValueAvg2 : double.NaN);
                                break;

                            case BotSeriesType.SIDT_Variation_Value:
                                if (sID.ReferredIndicator.IsValid)
                                {
                                    dPoint = sSeries.Points[sSeries.Points.AddXY(tLastTickerData.dReceivedTime, 0)];

                                    dPoint.YValues[BotTradeChart.RANGE_INDEX_LOW] = 0;
                                    dPoint.YValues[BotTradeChart.RANGE_INDEX_HIGH] = sID.ReferredIndicator.VariationPercent;

                                    bResult = sID.ReferredIndicator.IsLimitEnabled;
                                }
                                break;

                            case BotSeriesType.SIDT_Variation_Tendency:
                                if (sID.ReferredIndicator.IsValid)
                                    sSeries.Points.AddXY(tLastTickerData.dReceivedTime, sID.ReferredIndicator.Tendency);
                                break;

                            default:
                                Debug.Assert(false, "[UC195] Unbehandelter Source-Type!");
                                break;
                        }
                    }

                    break;
            }

            return (bResult);
        }
        // Ermittelt den Tooltip für den zuletzt angezeigten Datenpunkt.
        public String GetChartToolTip()
        {
            Series sTickerSer = FindSeries(BotSeriesType.SIDT_Ticker_Price);

            if ((sTickerSer != null) && (sTickerSer.Points.Count > 0))
                return (GetChartToolTip(BotTradeChart.AREA_TITLE_TICKER, sTickerSer.Points.Last().XValue));
            else
                return (string.Empty);
        }
        // Ermittelt den Tooltip für Datenpunkt 'dXValue'.
        public String GetChartToolTip(String sTargetArea, double dXValue)
        {
            if ((dToolTip_LastPointX == 0) || (dToolTip_LastPointX != dXValue))
            {
                double dCandlePointX;

                sToolTip_LastValue = "";
                dToolTip_LastPointX = dXValue;

                if (sTargetArea == BotTradeChart.AREA_TITLE_CANDLES)
                    dCandlePointX = dXValue;
                else
                    // Zeitpunkt von zugehörigem Candle-Stick ermitteln:
                    dCandlePointX = UtlDateTime.RoundDown(DateTime.FromOADate(dXValue), BotCurrencyRating.CANDLES_INTERVAL).ToOADate();

                // Einzelne Serien nach Y-Wert für ermittelten X-Wert abfragen:
                DataPoint _dResult;
                foreach (Series sSeries in Series)
                {
                    if ((sSeries.Enabled) && (sSeries.Points.Count > 0))
                    {
                        // Datenpunkt ermitteln:
                        if (sSeries.ChartArea == BotTradeChart.AREA_TITLE_CANDLES)
                            _dResult = sSeries.Points.FirstOrDefault(_pPoint => (_pPoint.XValue == dCandlePointX));
                        else
                            _dResult = sSeries.Points.FirstOrDefault(_pPoint => (_pPoint.XValue == dXValue));

                        if (_dResult != null)
                        {
                            // Prüfen ob Datenpunkt Werte enthält:
                            if (_dResult.YValues.Sum() != 0)
                            {
                                if (sToolTip_LastValue.Length > 0)
                                    sToolTip_LastValue += Environment.NewLine;
                                sToolTip_LastValue += String.Format("{0}: {1}", sSeries.Name, GetChartToolTipValue((BotSeriesID)sSeries.Tag, _dResult));
                            }
                        }
                    }
                }

                if (sToolTip_LastValue.Length > 0)
                    // Zeitstempel anzeigen:
                    sToolTip_LastValue = String.Format("Zeitpunkt: {0:dd.MM.yyyy} um {0:HH:mm:ss}{1}{1}{2}", DateTime.FromOADate(dXValue), Environment.NewLine, sToolTip_LastValue);
            }
            return (sToolTip_LastValue);
        }
        #endregion


        #region Annotations
        public void AddAnnotation(BotBaseOrder oOrder)
        {
            bool bIsBuyOrder = (oOrder.IsBuyOrder);

            // Order-Anmerkung in entsprechende Series einfügen:
            foreach (Series _sSeries in lAnotationSeries)
            {
                if (_sSeries.Points.Count > 0)
                {
                    // Annotation für Order hinzufügen:
                    LineAnnotation lAnnotation = new LineAnnotation();

                    lAnnotation.LineColor = ((bIsBuyOrder) ? SERIES_COLOR_BUY_LIMIT : SERIES_COLOR_SELL_LIMIT);
                    lAnnotation.Height = 3;
                    lAnnotation.LineWidth = 2;
                    lAnnotation.StartCap = LineAnchorCapStyle.Arrow;
                    lAnnotation.EndCap = LineAnchorCapStyle.None;
                    lAnnotation.ToolTip = GetChartToolTip();

                    if (_sSeries.ChartArea == AREA_TITLE_CANDLES)
                    {
                        lAnnotation.AxisX = ChartAreas[AREA_INDEX_CANDLES].AxisX;
                        lAnnotation.AxisY = ChartAreas[AREA_INDEX_CANDLES].AxisY;
                        lAnnotation.AnchorX = _sSeries.Points.Last().XValue;
                        lAnnotation.AnchorY = _sSeries.Points.Last().YValues[BotTradeChart.CANDLE_INDEX_CLOSE];
                        lAnnotation.Width = (lAnnotation.Height / 2);
                    }
                    else
                    {
                        lAnnotation.AnchorDataPoint = _sSeries.Points.Last();
                        lAnnotation.AnchorOffsetX = 0.1;
                        lAnnotation.AnchorOffsetY = 0.1;
                        lAnnotation.Width = (lAnnotation.Height / 6);
                    }

                    if (bIsBuyOrder == false)
                        lAnnotation.Height *= -1;

                    Annotations.Add(lAnnotation);


                    // (BETA) ...
                    /*
                    // Label hinzufügen:
                    DataPoint dPoint = _sSeries.Points.Last();
                    TradingPair tPair = oOrder.tRefPair;

                    dPoint.Label = String.Format("{0} {1}{2}@{3} {4}", oOrder.FinalAmount, MyEnum.GetEnumDescription(tPair.CurrencyPrim), Environment.NewLine, oOrder.FinalPrice, MyEnum.GetEnumDescription(tPair.CurrencySec));
                    dPoint["LabelStyle"] = "Top";
                    dPoint.Font = new Font("Arial", 8, FontStyle.Bold);
                    dPoint.LabelForeColor = lAnnotation.LineColor;
                    */
                }
            }
        }
        #endregion


        private StripLine[] dStripLine = new StripLine[STRIPLINE_COUNT];
        private List<Series> lAnotationSeries = new List<Series>();

        private String sToolTip_LastValue = string.Empty;
        private double dToolTip_LastPointX = 0;
    }
}
