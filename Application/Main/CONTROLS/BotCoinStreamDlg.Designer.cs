﻿
using FileDialogExtenders;

namespace CryptoRiftBot.Controls
{
    partial class BotCoinStreamDlg
    {
        /// <summary> 
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Komponenten-Designer generierter Code

        /// <summary> 
        /// Erforderliche Methode für die Designerunterstützung. 
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.Properties = new System.Windows.Forms.PropertyGrid();
            this.Panel = new System.Windows.Forms.TableLayoutPanel();
            this.Chart = new CryptoRiftBot.Controls.BotCandleChart();
            this.Panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Chart)).BeginInit();
            this.SuspendLayout();
            // 
            // Properties
            // 
            this.Properties.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Properties.Enabled = false;
            this.Properties.HelpVisible = false;
            this.Properties.Location = new System.Drawing.Point(3, 187);
            this.Properties.Name = "Properties";
            this.Properties.PropertySort = System.Windows.Forms.PropertySort.Categorized;
            this.Properties.Size = new System.Drawing.Size(549, 124);
            this.Properties.TabIndex = 1;
            this.Properties.ToolbarVisible = false;
            // 
            // Panel
            // 
            this.Panel.ColumnCount = 1;
            this.Panel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.Panel.Controls.Add(this.Properties, 0, 1);
            this.Panel.Controls.Add(this.Chart, 0, 0);
            this.Panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Panel.Location = new System.Drawing.Point(0, 0);
            this.Panel.Name = "Panel";
            this.Panel.RowCount = 2;
            this.Panel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.Panel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 130F));
            this.Panel.Size = new System.Drawing.Size(555, 314);
            this.Panel.TabIndex = 2;
            // 
            // Chart
            // 
            this.Chart.BackColor = this.BackColor;
            this.Chart.BorderlineColor = System.Drawing.SystemColors.ControlDark;
            this.Chart.BorderlineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
            this.Chart.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Chart.Location = new System.Drawing.Point(3, 3);
            this.Chart.Name = "Chart";
            this.Chart.Size = new System.Drawing.Size(549, 178);
            this.Chart.TabIndex = 0;
            // 
            // BotCoinStreamDlg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.Panel);
            this.Name = "BotCoinStreamDlg";
            this.Size = new System.Drawing.Size(555, 314);
            this.Load += new System.EventHandler(this.OnLoad);
            this.Panel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Chart)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private BotCandleChart Chart;
        private System.Windows.Forms.PropertyGrid Properties;
        private System.Windows.Forms.TableLayoutPanel Panel;
    }
}
