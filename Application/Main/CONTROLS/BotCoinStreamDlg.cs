﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CryptoRiftBot.Trading.Services;
using FileDialogExtenders;
using Shared.Windows.Forms;
using Win32Types;

namespace CryptoRiftBot.Controls
{
    public partial class BotCoinStreamDlg : FileDialogControlBase
    {
        /// <summary>
        /// Creates a save-file dialog.
        /// </summary>
        /// <param name="oParam">Array of Header and Candles.</param>
        public BotCoinStreamDlg(params object[] oParam) : this((BotCoinStream.IFileHeader)oParam[0], (BotCandleSubscription)oParam[1])
        {
        }
        /// <summary>
        /// Creates a save-file dialog.
        /// </summary>
        public BotCoinStreamDlg(BotCoinStream.IFileHeader iHeader, BotCandleSubscription bCandles) : this()
        {
            Debug.Assert((iHeader != null), "[BCSD25]");
            Debug.Assert((bCandles != null), "[BCSD26]");

            this.FileDlgType = FileDialogType.SaveFileDlg;
            this.FileDlgAddExtension = true;
            this.FileDlgOkCaption = "Speichern";

            this.iSelHeader = iHeader;
            this.bSelCandles = bCandles;
        }
        /// <summary>
        /// Creates an open-file dialog.
        /// </summary>
        public BotCoinStreamDlg()
        {
            InitializeComponent();

            this.FileDlgType = FileDialogType.OpenFileDlg;
            this.FileDlgDereferenceLinks = true;
            this.FileDlgOkCaption = "Öffnen";
        }


        #region Properties.Management
        [Browsable(false)]
        public new FileDialogType FileDlgType { private set; get; }
        [Browsable(false)]
        public bool IsReadOnly { get { return (FileDlgType == FileDialogType.OpenFileDlg); } }
        #endregion


        #region Events
        private void OnLoad(object oSender, EventArgs eArgs)
        {
            // Initialize:
            Chart.Init();

            Properties.Enabled = !IsReadOnly;

            RefreshControls();
        }
        public override void OnFileNameChanged(IWin32Window iSender, string sFileName)
        {
            if (FileDlgType == FileDialogType.OpenFileDlg)
            {
                // Read preview:
                BotCoinStream.ReadFilePreview(sFileName, out iSelHeader, out bSelCandles);

                RefreshControls();
            }
        }
        #endregion
        #region Management
        private void RefreshControls()
        {
            Chart.ResetData();
            if ((iSelHeader == null) || (bSelCandles == null))
                Properties.SelectedObject = null;
            else
            {
                Chart.UpdateChart(bSelCandles);
                Properties.SelectedObject = iSelHeader;
            }
            PropertyGridLabel.SetSplitterWidth(Properties, 100);
        }
        #endregion

        private BotCoinStream.IFileHeader iSelHeader = null;
        private BotCandleSubscription bSelCandles = null;
    }
}
