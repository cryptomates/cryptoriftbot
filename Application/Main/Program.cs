using System;
using System.Windows.Forms;
using Shared.Utils.Framework;

namespace CryptoRiftBot
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            if (UtlApplication.GetOwnInstanceCount() > 1)
                MessageBox.Show("Die Anwendung wird bereits ausgeführt.", BotConvention.APP_TITLE, MessageBoxButtons.OK, MessageBoxIcon.Information);
            else
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new MainForm());
            }
        }
    }
}