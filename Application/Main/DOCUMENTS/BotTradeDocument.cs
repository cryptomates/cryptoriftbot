using CryptoRiftBot;
using CryptoRiftBot.Trading.Services;
using CryptoRiftBot.Trading.Strategy;
using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using Shared.Utils.Framework;
using CryptoRiftBot.Controls;

namespace CryptoRiftBot.Documents
{
    public partial class BotTradeDocument : BotBaseDocument<BotTradeChart>
    {
        #region Constants
        // Darstellung:
        public const int DISPLAY_MIN_TICKS = 490;                       // Minimale Anzahl an Ticks um Inhalte neuzuzeichnen
        #endregion


        public BotTradeDocument(BotTradingStrategy bStrategy) : base(bStrategy.TradingPair.Symbol, bStrategy.Title, new BotTradeChart(bStrategy))
        {
            // Referenzierte Strategie übernehmen:
            Tag = bStrategy;

            InitializeComponent();
        }


        #region Events
        protected override void OnPaint(PaintEventArgs pArgs)
        {
            base.OnPaint(pArgs);
        }
        #endregion
        #region Events
        private void OnActivated(object oSender, EventArgs eArgs)
        {
        }
        public void OnOrderFinished(BotBaseOrder oOrder)
        {
            Chart.AddAnnotation(oOrder);

            if (BotDebugMode.StateShotOnOrder)
            {
                if (ReferredStrategy.CoinStreamPlayer == null)
                    // State-Shot ausfüren wenn KEIN CoinStream-Replay abgespielt wird:
                    SaveStateShot();
            }
        }
        public void SaveImage(Stream sStream, ChartImageFormat cFormat = ChartImageFormat.Bmp)
        {
            Debug.Assert((sStream != null), "[SI482]");

            Chart.SaveImage(sStream, cFormat);
            sStream.Position = 0;
        }
        #endregion


        #region Properties.Management
        public override bool IsValid { get { return ((ReferredStrategy != null) && (base.IsValid)); } }
        public bool IsSetRedraw { private set; get; }
        #endregion
        #region Properties.References
        public BotTradingStrategy ReferredStrategy { get { return (BotTradingStrategy)Tag; } }
        #endregion


        #region Management
        // Aktualisiert das aktuell angezeigte Chart.
        public bool UpdateContent(bool bCandlesOnly = false)
        {
            Debug.Assert(IsValid, "UC242");

            if (ReferredStrategy.Enabled)
            {
                // Subscriptions ermitteln:
                BotTickerSubscription tTicker = (BotTickerSubscription)ReferredStrategy.Platform.FindPairSubscription(ReferredStrategy.TradingPair, BotSubscriptionType.eTicker);
                BotCandleSubscription cCandles = BotCurrencyRating.GetCandles(ReferredStrategy);

                if ((tTicker == null) || (cCandles == null))
                {
                    // Subscription(s) konnten nicht ermittelt werden.
                    // > Das kann passieren wenn Bot gerade erst gestartet wurde.
                }
                else
                {
                    Chart.UpdateChart(tTicker, cCandles, bCandlesOnly);
                    return (true);
                }
            }
            return (false);
        }
        public bool UpdateContent(BotTickerSubscription tTicker)
        {
            bool bResult = CalcRedraw();

            Debug.Assert(IsValid, "UC255");

            // Ihalt aktualisieren:
            Chart.UpdateChart(tTicker);

            return (bResult);
        }
        public void SaveStateShot()
        {
            using (MemoryStream mStream = new MemoryStream())
            {
                SaveImage(mStream);

                // Bitmap in Zwischenablage speichern:
                Bitmap bBitmap = new Bitmap(mStream);

                Clipboard.SetImage(bBitmap);

                new BotExcelDocThread(new BotStateShotExcelDoc(ReferredStrategy), bBitmap);
            }
        }
        #endregion
        #region Management
        protected override string GetPersistString()
        {
            // Serialisierung nicht notwendig:
            return ("");
        }
        #endregion
        #region Display
        public bool CalcRedraw(bool bForceRedraw = false)
        {
            int iCurTicks = Environment.TickCount;
            bool bSetRedraw = ((bForceRedraw) || (iLastUpdateTicks == 0) || (iCurTicks < iLastUpdateTicks) || ((iCurTicks - iLastUpdateTicks) >= DISPLAY_MIN_TICKS));

            Debug.Assert(IsValid, "UC255");

            // Prüfen ob Control neugezeichnet werden muss:
            if (bSetRedraw)
                iLastUpdateTicks = iCurTicks;

            if (bSetRedraw != IsSetRedraw)
            {
                IsSetRedraw = bSetRedraw;
                UtlWindow.SetRedraw(Chart, bSetRedraw);
            }
            if (bForceRedraw)
                // Control neuzeichnen:
                Chart.Invalidate();

            return (bSetRedraw);
        }
        #endregion


        private int iLastUpdateTicks = 0;
    }
}