﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CryptoRiftBot.Controls;
using WeifenLuo.WinFormsUI.Docking;

namespace CryptoRiftBot.Documents
{
    public abstract partial class BotBaseDocument<TChart> : DockContent where TChart : BotBaseChart
    {
        public BotBaseDocument(string sTabText, string sDescription, TChart tChart)
        {
            InitializeComponent(tChart);

            this.Text = sDescription;
            this.ToolTipText = sDescription;
            this.TabText = sTabText;
        }


        #region Properties.Management
        public virtual bool IsValid { get { return (bChart.IsValid); } }
        #endregion
        #region Properties.Controls
        public TChart Chart { get { return (bChart); } }
        #endregion


        #region Events
        protected override void OnLoad(EventArgs eArgs)
        {
            base.OnLoad(eArgs);

            Init();
        }
        #endregion
        #region Management
        public void Init()
        {
            this.SuspendLayout();

            // Initialisieren:
            bChart.Init();

            this.ResumeLayout();

            Debug.Assert(IsValid, "[I94]");
        }
        #endregion
    }
}
