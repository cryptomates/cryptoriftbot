﻿using System.Diagnostics;
using CryptoRiftBot.Controls;

namespace CryptoRiftBot.Documents
{
    partial class BotBaseDocument<TChart>
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent(TChart tChart)
        {
            Debug.Assert((tChart != null), "[IC33]");

            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BotTradeDocument));
            this.bChart = tChart;
            ((System.ComponentModel.ISupportInitialize)(this.bChart)).BeginInit();
            this.SuspendLayout();
            // 
            // gChart
            // 
            this.bChart.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bChart.Location = new System.Drawing.Point(3, 23);
            this.bChart.Name = "gChart";
            this.bChart.Size = new System.Drawing.Size(946, 434);
            this.bChart.TabIndex = 22;
            this.bChart.Text = "chart1";
            // 
            // BotBaseDocument
            // 
            this.ClientSize = new System.Drawing.Size(448, 393);
            this.Controls.Add(this.bChart);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ChartDocument";
            this.Padding = new System.Windows.Forms.Padding(0, 4, 0, 0);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AllowEndUserDocking = false;
            this.CloseButtonVisible = false;
            ((System.ComponentModel.ISupportInitialize)(this.bChart)).EndInit();
            this.ResumeLayout(false);
        }

        #endregion
        private TChart bChart;
    }
}