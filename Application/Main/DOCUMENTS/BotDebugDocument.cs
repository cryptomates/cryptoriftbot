﻿using CryptoRiftBot.Controls;
using CryptoRiftBot.Trading.Services;

namespace CryptoRiftBot.Documents
{
    #region Controls
    public class BotDebugCandleChart : BotBaseChart
    {
        public BotDebugCandleChart(params BotCandleSubscription[] bSubscriptions)
        {
            this.bSubscriptions = bSubscriptions;
        }


        #region Events
        protected override void OnInit()
        {
            foreach (var _bSubscr in bSubscriptions)
            {
                string _sTitle = string.Format("Candles {0}", (ChartAreas.Count + 1));

                // Area:
                var _cArea = CreateArea(_sTitle, false);

                // Add series:
                var _sCandles = AddSource(_sTitle, BotSeriesType.SIDT_Candles_Sticks);
                _sCandles.Name += ChartAreas.Count;
                var _sVolume = AddSource(_sTitle, BotSeriesType.SIDT_Candles_Volume);
                _sVolume.Name += ChartAreas.Count;

                Update(_bSubscr, _sCandles, _sVolume);

                // Update ToolTip:
                foreach (var _pPoint in _sCandles.Points)
                    _pPoint.ToolTip = GetChartToolTipValue((BotSeriesID)_sCandles.Tag, _pPoint);
            }
        }
        #endregion


        private BotCandleSubscription[] bSubscriptions;
    }
    #endregion
    #region Documents
    public abstract partial class BotDebugDocument<TChart> : BotBaseDocument<TChart> where TChart : BotBaseChart
    {
        protected BotDebugDocument(TChart tChart) : base("Debug", "Debug", tChart)
        {
            InitializeComponent();
        }
    }

    /// <summary>
    /// Debug document for display of candle-subscriptions.
    /// </summary>
    public class BotCandleSubscriptionDebugDocument : BotDebugDocument<BotDebugCandleChart>
    {
        public BotCandleSubscriptionDebugDocument(BotCandleSubscription cCandles) : base(new BotDebugCandleChart(cCandles)) { }
        public BotCandleSubscriptionDebugDocument(params BotCandleSubscription[] cCandles) : base(new BotDebugCandleChart(cCandles)) { }
    }
    #endregion
}
