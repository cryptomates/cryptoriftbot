using WeifenLuo.WinFormsUI.Docking;

namespace CryptoRiftBot.Customization
{
    internal static class BotDockHelper
    {
        public static bool IsDockStateAutoHide(DockState dockState)
        {
            switch (dockState)
            {
                case DockState.DockLeftAutoHide:
                case DockState.DockRightAutoHide:
                case DockState.DockTopAutoHide:
                case DockState.DockBottomAutoHide:
                    return (true);
                default:
                    return (false);
            }
        }
        public static bool IsDockStateDocked(DockState dockState)
        {
            switch (dockState)
            {
                case DockState.DockLeft:
                case DockState.DockRight:
                case DockState.DockTop:
                case DockState.DockBottom:
                    return (true);
                default:
                    return (false);
            }
        }
        public static bool IsDockBottom(DockState dockState)
        {
            switch (dockState)
            {
                case DockState.DockBottom:
                case DockState.DockBottomAutoHide:
                    return (true);
                default:
                    return (false);
            }
        }
        public static bool IsDockLeft(DockState dockState)
        {
            switch (dockState)
            {
                case DockState.DockLeft:
                case DockState.DockLeftAutoHide:
                    return (true);
                default:
                    return (false);
            }
        }
        public static bool IsDockRight(DockState dockState)
        {
            switch (dockState)
            {
                case DockState.DockRight:
                case DockState.DockRightAutoHide:
                    return (true);
                default:
                    return (false);
            }
        }
        public static bool IsDockTop(DockState dockState)
        {
            switch (dockState)
            {
                case DockState.DockTop:
                case DockState.DockTopAutoHide:
                    return (true);
                default:
                    return (false);
            }
        }
        public static bool IsDockStateValid(DockState dockState, DockAreas dockableAreas)
        {
            if (((dockableAreas & DockAreas.Float) == 0) &&
                (dockState == DockState.Float))
                return (false);
            else if (((dockableAreas & DockAreas.Document) == 0) &&
                (dockState == DockState.Document))
                return (false);
            else if (((dockableAreas & DockAreas.DockLeft) == 0) &&
                (dockState == DockState.DockLeft || dockState == DockState.DockLeftAutoHide))
                return (false);
            else if (((dockableAreas & DockAreas.DockRight) == 0) &&
                (dockState == DockState.DockRight || dockState == DockState.DockRightAutoHide))
                return (false);
            else if (((dockableAreas & DockAreas.DockTop) == 0) &&
                (dockState == DockState.DockTop || dockState == DockState.DockTopAutoHide))
                return (false);
            else if (((dockableAreas & DockAreas.DockBottom) == 0) &&
                (dockState == DockState.DockBottom || dockState == DockState.DockBottomAutoHide))
                return (false);
            else
                return (true);
        }
        public static bool IsDockWindowState(DockState state)
        {
            switch (state)
            {
                case DockState.DockTop:
                case DockState.DockBottom:
                case DockState.DockLeft:
                case DockState.DockRight:
                case DockState.Document:
                    return (true);
                default:
                    return (false);
            }
        }
        public static bool IsValidRestoreState(DockState state)
        {
            return (IsDockWindowState(state));
        }
        public static DockState ToggleAutoHideState(DockState state)
        {
            switch (state)
            {
                case DockState.DockLeft: return (DockState.DockLeftAutoHide);
                case DockState.DockRight: return (DockState.DockRightAutoHide);
                case DockState.DockTop: return (DockState.DockTopAutoHide);
                case DockState.DockBottom: return (DockState.DockBottomAutoHide);
                case DockState.DockLeftAutoHide: return (DockState.DockLeft);
                case DockState.DockRightAutoHide: return (DockState.DockRight);
                case DockState.DockTopAutoHide: return (DockState.DockTop);
                case DockState.DockBottomAutoHide: return (DockState.DockBottom);
                default:
                    return (state);
            }
        }
    }
}
