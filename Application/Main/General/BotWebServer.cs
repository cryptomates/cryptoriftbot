﻿using CryptoRiftBot;
using CryptoRiftBot.Trading.Services;
using CryptoRiftBot.Trading.Strategy;
using CryptoRiftBot.Trading;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Linq;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using Shared.Utils.Core;
using static CryptoRiftBot.BotLogWindow;
using Shared.Utils.Framework;
using static Shared.Utils.Core.UtlComponentModel;
using Shared.Net;
using CryptoRiftBot.Documents;

namespace CryptoRiftBot
{
    class BotWebServer : HttpServer
    {
        #region Constants
        // Konfiguration:
        public const bool CFG_SHOW_INACTIVE_PLATFORMS = true;
        public const bool CFG_SHOW_INACTIVE_STTRATEGIES = true;
        public const int CFG_MAX_LOG_ENTRIES = 70;

        // Tags:
        public const String TAG_OPTIONS = "opt";
        public const String TAG_PLATFORMS = "pltf";
        public const String TAG_RIFT_SUMMARY = "rftsum";
        public const String TAG_CHART = "chat";
        #endregion
        #region Enumerations
        public enum BotMenuID
        {
            Invalid,

            [Description("Platformen")]
            ePlatforms,
            [Description("Pärchen")]
            eTradingPairs
        }
        public enum BotContentID
        {
            Invalid,

            [Description("Optionen")]
            eOptions,
            [Description("Log")]
            eLog,
            [Description("Platform")]
            ePlatform,
            [Description("Pärchen")]
            eStrategy,
            [Description("Rift Summary")]
            eStrategy_RiftSummary,
            [Description("Chart")]
            eStrategy_Chart
        }
        public enum BotImageID
        {
            Invalid,

            [Description("chart.Png")]
            eChart
        }
        #endregion
        #region Delegates
        public delegate BotTradeDocument GetChartDocumentHandler(BotTradingStrategy bStrategy);
        #endregion


        public BotWebServer(GetChartDocumentHandler GetChartDocument) : base()
        {
            this.GetChartDocument = GetChartDocument;
        }


        #region Events
        public override void OnStart()
        {
            BotEnv.LogSystem.WriteLog("OS23", BotLogProcess.eWebserver, String.Format("Server gestartet (Port: {0})", iPort), BotLogType.eInfo, BotLogPriority.eLow);
        }
        public override void OnGETRequest(HttpProcessor hProc)
        {
            string[] sParts = hProc.sHTTP_URL.Split('/');
            bool bResult = true;

            Reset(hProc);

            if (sParts.Length > 0)
            {
                String sLastPart = sParts.Last();

                if (sLastPart.Contains('.'))
                    WriteCurImage(sLastPart);
                else
                {
                    hProc.WriteSuccess();

                    PrintStyles();
                    hProc.sOutput.WriteLine("<html><body>");

                    // Angeforderten Inhalt auswerten:
                    PrintHeader();

                    if (sParts[0] == "")
                    {
                        BotMenuID mMenu = BotMenuID.ePlatforms;
                        BotContentID cContent = BotContentID.Invalid;

                        AddLocation(GetURL());

                        if (sParts[1] == "")
                            // Hauptmenü:
                            cContent = BotContentID.eLog;

                        else if (sParts[1] == TAG_OPTIONS)
                        {
                            // Optionen:
                            cContent = BotContentID.eOptions;

                            AddLocation(GetURL(cContent));
                        }
                        else if (sParts[1] == TAG_PLATFORMS)
                        {
                            tBot = BotTradeBot.FindPlatform(sParts[2]);

                            if (tBot == null)
                                PrintError(1076, "Unknown platform request.");
                            else
                            {
                                mMenu = BotMenuID.eTradingPairs;
                                cContent = BotContentID.ePlatform;

                                AddLocation(GetURL(tBot));

                                if (sParts.Length > 3)
                                {
                                    bStrategy = tBot.FindStrategy(tBot.ReferredPlatform.FindPair(sParts[3]));
                                    if (bStrategy == null)
                                        PrintError(1094, "Unknown symbol request.");
                                    else
                                    {
                                        AddLocation(GetURL(bStrategy));

                                        if (sParts.Length > 4)
                                        {
                                            if (sParts[4] == TAG_RIFT_SUMMARY)
                                                cContent = BotContentID.eStrategy_RiftSummary;

                                            else if (sParts[4] == TAG_CHART)
                                                cContent = BotContentID.eStrategy_Chart;

                                            else
                                                bResult = false;

                                            if (bResult)
                                                AddLocation(GetURL(cContent));
                                        }
                                        else
                                            cContent = BotContentID.eStrategy;
                                    }
                                }
                                else
                                    bResult = false;
                            }
                        }
                        else
                            bResult = false;

                        if (mMenu != BotMenuID.Invalid)
                        {
                            if (sCurLocationCmd.Length > 0)
                                // Aktuelle Location darstellen:
                                PrintLocation();

                            // Inhalt darstellen:
                            hProc.sOutput.WriteLine("<table><tr>");

                            hProc.sOutput.WriteLine("<td class=\"MainTableCol1\">");
                            PrintMenu(mMenu);
                            hProc.sOutput.WriteLine("</td>");
                            hProc.sOutput.WriteLine("<td class=\"MainTableCol2\">");
                            PrintContent(cContent);
                            hProc.sOutput.WriteLine("</td>");

                            hProc.sOutput.WriteLine("</tr></table>");
                        }
                    }
                    else
                        bResult = false;

                    if (bResult == false)
                        PrintError(10404, "URL invalid.");

                    PrintFooter();

                    hProc.sOutput.WriteLine("</body></html>");
                }
            }
        }
        public override void OnPOSTRequest(HttpProcessor hProc, StreamReader sInputData)
        {
            Console.WriteLine("POST request: {0}", hProc.sHTTP_URL);
            string data = sInputData.ReadToEnd();

            hProc.WriteSuccess();
            hProc.sOutput.WriteLine("<html><body><h1>test server</h1>");
            hProc.sOutput.WriteLine("<a href=/test>return</a><p>");
            hProc.sOutput.WriteLine("postbody: <pre>{0}</pre>", data);
        }
        #endregion
        #region Events
        private EvaluationResult OnEvaluateCallback(PropertyInfo pInfo, String sName, String sValue, int iDepth, ref Object oTag)
        {
            if (iDepth == 0)
                // Überschrift:
                sName = String.Format("<b>{0}</b>", sName);
            else
            {
                // Eingerückte Eigenschaft:
                for (int i = 0; i < iDepth; i++)
                    sName = ("&emsp;" + sName);
            }

            hProc.sOutput.WriteLine("<tr>");
            hProc.PrintTaggedLine("td", sName, "class=\"ContentCol1\"");
            hProc.PrintTaggedLine("td", sValue);
            hProc.sOutput.WriteLine("</tr>");

            return (EvaluationResult.eSuccess);
        }
        #endregion


        #region Management
        public void Reset(HttpProcessor hProc = null)
        {
            this.hProc = hProc;
            this.sCurLocationCmd = "";
            this.tBot = null;
            this.bStrategy = null;
        }
        #endregion
        #region Contents
        private void PrintStyles()
        {
            hProc.sOutput.WriteLine("<style type=\"text/css\">");
            hProc.sOutput.WriteLine(".MainTableCol1");
            hProc.sOutput.WriteLine("{");
            hProc.sOutput.WriteLine("padding-right: 50px;");
            hProc.sOutput.WriteLine("vertical-align: top;");
            hProc.sOutput.WriteLine("}");
            hProc.sOutput.WriteLine(".MainTableCol2");
            hProc.sOutput.WriteLine("{");
            hProc.sOutput.WriteLine("vertical-align: top;");
            hProc.sOutput.WriteLine("}");
            hProc.sOutput.WriteLine(".MenuCol1");
            hProc.sOutput.WriteLine("{");
            hProc.sOutput.WriteLine("padding-right: 10px;");
            hProc.sOutput.WriteLine("}");
            hProc.sOutput.WriteLine(".ContentCol1");
            hProc.sOutput.WriteLine("{");
            hProc.sOutput.WriteLine("padding-right: 30px;");
            hProc.sOutput.WriteLine("}");
            hProc.sOutput.WriteLine(".LogCols");
            hProc.sOutput.WriteLine("{");
            hProc.sOutput.WriteLine("padding-right: 20px;");
            hProc.sOutput.WriteLine("}");
            hProc.sOutput.WriteLine(".LogRowHeader td");
            hProc.sOutput.WriteLine("{");
            hProc.sOutput.WriteLine("padding-right: 20px;");
            hProc.sOutput.WriteLine("font-weight: bold;");
            hProc.sOutput.WriteLine("border-bottom: 1pt solid black;");
            hProc.sOutput.WriteLine("}");

            foreach (BotLogPriority _bPrio in Enum.GetValues(typeof(BotLogPriority)))
            {
                hProc.sOutput.WriteLine(".LogRowPrio" + (int)_bPrio);
                hProc.sOutput.WriteLine("{");
                hProc.sOutput.WriteLine("padding-right: 50px;");
                FormatHTMLEntryStyle(hProc, _bPrio);
                hProc.sOutput.WriteLine("}");
            }

            hProc.sOutput.WriteLine("</style>");
        }
        private void PrintLocation()
        {
            PrintText("Ort:", sCurLocationCmd);
            hProc.sOutput.WriteLine("<hr>");
            hProc.sOutput.WriteLine("<br>");
        }
        private void PrintHeader()
        {
            hProc.PrintTaggedLine("h1", BotEnv.Title);

            hProc.sOutput.WriteLine("<hr>");
        }
        private void PrintFooter()
        {
            hProc.sOutput.WriteLine("<br><br>");
            hProc.sOutput.WriteLine("<hr>");
            PrintText("Zeit:", DateTime.Now.ToString());
        }
        private void PrintMenu(BotMenuID mID)
        {
            hProc.PrintTaggedLine("h2", "&nbsp;");

            // Statische Einträge ausgeben:
            hProc.sOutput.WriteLine(GetURL(BotContentID.eOptions) + "<br>");

            // Platform-Einträge ausgeben:
            hProc.sOutput.WriteLine("<br>");
            hProc.PrintTaggedLine("b", UtlEnum.GetEnumDescription(mID));
            hProc.sOutput.WriteLine("<table>");

            switch (mID)
            {
                case BotMenuID.ePlatforms:
                    for (int i = 0; i < BotTradeBot.TradeBots.Count; i++)
                    {
                        if ((CFG_SHOW_INACTIVE_PLATFORMS) || (BotTradeBot.TradeBots[i].IsActive()))
                            PrintStateLink(BotTradeBot.TradeBots[i].IsActive(), GetURL(BotTradeBot.TradeBots[i]));
                    }
                    break;

                case BotMenuID.eTradingPairs:
                    Debug.Assert((tBot != null), "[PM218]");
                    foreach (BotTradingStrategy _bStrategy in tBot.Strategies)
                    {
                        if ((CFG_SHOW_INACTIVE_STTRATEGIES) || (_bStrategy.Enabled))
                            PrintStateLink(_bStrategy.Enabled, GetURL(_bStrategy.TradingPair));
                    }
                    break;
            }

            hProc.sOutput.WriteLine("</table>");
        }
        private void PrintContent(BotContentID cID)
        {
            hProc.PrintTaggedLine("h2", UtlEnum.GetEnumDescription(cID));

            hProc.sOutput.WriteLine("<table>");

            switch (cID)
            {
                case BotContentID.eOptions:


                    // hProc.PrintTaggedLine("form", "<input type=\"checkbox\" name=\"zutat\" value=\"salami\">", "method=post action=/form");

                    hProc.sOutput.WriteLine("<form method=post action=/form>");
                    hProc.sOutput.WriteLine("<input type=text name=foo value=foovalue>");
                    hProc.sOutput.WriteLine("<input type=\"checkbox\" name=\"zutat\" value=\"salami\" onChange=\"submit();\">");
                    hProc.sOutput.WriteLine("</form>");

                    PrintText("(BETA) ...", "Noch Optionen machen");

                    //       WEITER MACHEN
                    // weitere Webserver-optionen machen (z.B. filter für Log ansicht, anz log einträge, ...).
                    // dann optionen mit component-model parsen und ausgeben.

                    break;

                case BotContentID.eLog:
                    int iStart = Math.Max(0, (BotEnv.LogSystem.Entries.Count - CFG_MAX_LOG_ENTRIES));

                    FormatHTMLRow(hProc);
                    for (int i = iStart; i < BotEnv.LogSystem.Entries.Count; i++)
                        FormatHTMLRow(hProc, i);

                    break;

                case BotContentID.ePlatform:
                    Debug.Assert((tBot != null), "[PC244]");
                    break;

                case BotContentID.eStrategy:
                    Debug.Assert((bStrategy != null), "[PC252]");

                    hProc.sOutput.WriteLine(GetURL(BotContentID.eStrategy_RiftSummary) + "<br>");
                    if (bStrategy.Enabled)
                        hProc.sOutput.WriteLine(GetURL(BotContentID.eStrategy_Chart) + "<br>");

                    break;

                case BotContentID.eStrategy_RiftSummary:
                    Debug.Assert((bStrategy != null), "[PC261]");

                    // Rift Summaries auflisten:
                    UtlComponentModel.EvaluateObject(bStrategy.RiftSummaries, OnEvaluateCallback);
                    break;

                case BotContentID.eStrategy_Chart:
                    Debug.Assert((bStrategy != null), "[PC270]");

                    // Chart anzeigen:
                    LoadImage(BotImageID.eChart, "Chart");
                    break;
            }

            hProc.sOutput.WriteLine("</table>");
        }
        #endregion


        #region Helper.Format
        private static void FormatHTMLEntryStyle(HttpProcessor hProc, BotLogPriority ePriority)
        {
            BotLogEntryStyle eStyle = BotLogWindow.EntryStyle[ePriority];

            Debug.Assert((hProc != null), "[FHES308]");

            hProc.sOutput.WriteLine("color: #" + UtlColor.ColorToHex(eStyle.TextColor) + ";");

            if (eStyle.Font.Bold)
                hProc.sOutput.WriteLine("font-weight: bold;");
        }
        // Gibt eine Zeile (Spalten-Titel oder entsprechenden Eintrag) in der Log-Ansicht aus.
        private void FormatHTMLRow(HttpProcessor hProc, int iIndex = -1)
        {
            Debug.Assert((hProc != null), "[FHR321]");

            Debug.Assert((BotEnv.LogSystem.Columns.Length == 5), "FHR327");

            if (iIndex == -1)
            {
                // Spalten-Titel ausgeben:
                hProc.sOutput.WriteLine("<tr class=\"LogRowHeader\">");

                foreach (String _sCol in BotEnv.LogSystem.Columns)
                    hProc.PrintTaggedLine("td", _sCol, "class=\"LogCols\"");
            }
            else
            {
                BotLogEntry eEntry = BotEnv.LogSystem.Entries[iIndex];

                // Spalten-Inhalte ausgeben:
                hProc.sOutput.WriteLine(String.Format("<tr class=\"LogRowPrio{0}\">", (int)eEntry.Priority));

                hProc.PrintTaggedLine("td", eEntry.TimeStamp.ToString(), "class=\"LogCols\"");
                hProc.PrintTaggedLine("td", eEntry.Code, "class=\"LogCols\"");
                hProc.PrintTaggedLine("td", eEntry.TypeTitle, "class=\"LogCols\"");
                hProc.PrintTaggedLine("td", eEntry.Process, "class=\"LogCols\"");
                hProc.PrintTaggedLine("td", eEntry.Description, "class=\"LogCols\"");
            }

            hProc.sOutput.WriteLine("</tr>");
        }
        #endregion


        #region SessionInfo
        private HttpProcessor hProc;
        private String sCurLocationCmd;
        private BotTradeBot tBot;
        private BotTradingStrategy bStrategy;
        private MemoryStream mCurImage;
        private BotImageID iCurImageID;
        #endregion


        #region Helper
        private void PrintStateLink(bool bActive, String sURL)
        {
            hProc.sOutput.WriteLine("<tr>");
            hProc.sOutput.WriteLine("<td class=\"MenuCol1\">" + (bActive ? "&#10003;" : String.Empty) + "</td>");
            hProc.sOutput.WriteLine("<td>" + sURL + "</td>");
            hProc.sOutput.WriteLine("</tr>");
        }
        private void PrintText(String LeadingTitle, String sText)
        {
            hProc.sOutput.WriteLine(String.Format("<b>{0}</b>&emsp;{1}", LeadingTitle, sText));
        }
        private void PrintError(int iCode, String sText)
        {
            hProc.PrintTaggedLine("h2", String.Format("Error {0}: {1}", iCode, sText));
        }

        private void AddLocation(String sLink)
        {
            if (sCurLocationCmd.Length > 0)
                // Trennzeichen einfügen:
                sCurLocationCmd = (sCurLocationCmd + "&emsp;|&emsp;");

            sCurLocationCmd += sLink;
        }

        private String GetURL(object oObject = null)
        {
            if (oObject == null)
                return ("<a href=\"/\">Home</a>");
            else
            {
                Type tObjType = oObject.GetType();
                if (tObjType == typeof(BotTradeBot))
                {
                    BotPlatformService pPlatform = ((BotTradeBot)oObject).ReferredPlatform;
                    return (String.Format("<a href=\"/{0}/{1}\">{2}</a>", TAG_PLATFORMS, pPlatform.PlatformDomain, pPlatform.PlatformName));
                }
                else if (tObjType == typeof(BotContentID))
                {
                    BotContentID cRequestedID = (BotContentID)oObject;

                    switch (cRequestedID)
                    {
                        case BotContentID.eOptions:
                            return (String.Format("<a href=\"/{0}\">{1}</a>", TAG_OPTIONS, UtlEnum.GetEnumDescription(cRequestedID)));

                        case BotContentID.eStrategy_RiftSummary:
                            Debug.Assert((tBot != null), "[GU472]");
                            Debug.Assert((bStrategy != null), "[473]");

                            return (String.Format("<a href=\"/{0}/{1}/{2}/{3}\">{4}</a>", TAG_PLATFORMS, tBot.ReferredPlatform.PlatformDomain, bStrategy.TradingPair.Symbol, TAG_RIFT_SUMMARY, UtlEnum.GetEnumDescription(cRequestedID)));

                        case BotContentID.eStrategy_Chart:
                            Debug.Assert((tBot != null), "[GU478]");
                            Debug.Assert((bStrategy != null), "[479]");

                            return (String.Format("<a href=\"/{0}/{1}/{2}/{3}\">{4}</a>", TAG_PLATFORMS, tBot.ReferredPlatform.PlatformDomain, bStrategy.TradingPair.Symbol, TAG_CHART, UtlEnum.GetEnumDescription(cRequestedID)));

                        default:
                            Debug.Assert(false, "[GU383] Invalid Content URL!");
                            break;
                    }
                }
                else if (tObjType.IsSubclassOf(typeof(BotTradingStrategy)))
                    return (GetURL(((BotTradingStrategy)oObject).TradingPair));

                else if (tObjType == typeof(BotTradingPair))
                {
                    Debug.Assert((tBot != null), "[GU322]");

                    BotTradingPair tPair = (BotTradingPair)oObject;
                    return (String.Format("<a href=\"/{0}/{1}/{2}\">{2}</a>", TAG_PLATFORMS, tBot.ReferredPlatform.PlatformDomain, tPair.Symbol, tPair.Title));
                }

                return (string.Empty);
            }
        }
        private bool LoadImage(BotImageID iImageID, String sTitle)
        {
            bool bResult = false;

            if (mCurImage != null)
            {
                // Zuvor geladenes Image löschen:
                mCurImage.Dispose();

                mCurImage = null;
                iCurImageID = BotImageID.Invalid;
            }

            switch (iImageID)
            {
                case BotImageID.eChart:
                    BotTradeDocument cDoc = GetChartDocument(bStrategy);
                    if (cDoc != null)
                    {
                        bResult = true;
                        mCurImage = new MemoryStream();
                        cDoc.InvokeEx(() => cDoc.SaveImage(mCurImage, ChartImageFormat.Png));
                    }
                    break;
            }

            if (bResult)
            {
                iCurImageID = iImageID;
                hProc.sOutput.WriteLine(String.Format("<img src=\"{0}\" alt=\"{1}\" />", UtlEnum.GetEnumDescription(iImageID), sTitle));
            }
            else
                PrintError(502, String.Format("Invalid {0} request.", sTitle));

            return (bResult);
        }
        private bool WriteCurImage(String sFileName)
        {
            if ((iCurImageID != BotImageID.Invalid) && (UtlEnum.GetEnumDescription(iCurImageID) == sFileName))
                // Aktuelles Image ausgeben:
                return (WriteCurImage());
            else
                return (false);
        }
        private bool WriteCurImage()
        {
            if (mCurImage == null)
            {
                Debug.Assert(false, "[WCI508] Ungültiges Image in aktuellem HTTP-Request!");
                return (false);
            }
            else
            {
                hProc.WriteSuccess("image/Png");
                hProc.sOutput.Flush();

                mCurImage.CopyTo(hProc.sOutput.BaseStream);
                hProc.sOutput.BaseStream.Flush();

                return (true);
            }
        }
        #endregion


        private GetChartDocumentHandler GetChartDocument;
    }
}
