using System;
using System.Drawing;
using System.Windows.Forms;

namespace CryptoRiftBot
{
    partial class BotPropertyWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent(PropertyValueChangedEventHandler pPropertyValueChangedEvent, ToolStripItemClickedEventHandler eToolbarItemClickedEvent)
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BotPropertyWindow));
            this.pgContent = new System.Windows.Forms.PropertyGrid();
            this.tsToolbarStrategy = new System.Windows.Forms.ToolStrip();
            this.tsToolbarParameters = new System.Windows.Forms.ToolStrip();
            this.tlpLayout = new System.Windows.Forms.TableLayoutPanel();
            this.tsStrategy_CoinStream_PlayerMode = new System.Windows.Forms.ToolStripButton();
            this.tsStrategy_CoinStream_Forward = new System.Windows.Forms.ToolStripButton();
            this.tsStrategy_Separator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsStrategy_CoinStream = new System.Windows.Forms.ToolStripSplitButton();
            this.tsStrategy_CoinStream_Record = new System.Windows.Forms.ToolStripMenuItem();
            this.tsParameters_New = new System.Windows.Forms.ToolStripSplitButton();
            this.tsParameters_Remove = new System.Windows.Forms.ToolStripButton();
            this.tsParameters_New_Selector = new System.Windows.Forms.ToolStripMenuItem();
            this.tsParameters_New_Settings = new System.Windows.Forms.ToolStripMenuItem();
            this.tsParameters_Choice = new System.Windows.Forms.ToolStripComboBox();
            this.tsParameters_ActivateChanges = new System.Windows.Forms.ToolStripButton();
            this.tsParameters_SetReference = new System.Windows.Forms.ToolStripButton();
            this.tlpLayout.SuspendLayout();
            this.SuspendLayout();
            // 
            // tlpLayout
            // 
            this.tlpLayout.ColumnCount = 1;
            this.tlpLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpLayout.Controls.Add(this.pgContent, 0, 1);
            this.tlpLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpLayout.Location = new System.Drawing.Point(0, 3);
            this.tlpLayout.Name = "tlpLayout";
            this.tlpLayout.RowCount = 2;
            this.tlpLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tlpLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpLayout.Size = new System.Drawing.Size(208, 283);
            this.tlpLayout.TabIndex = 2;
            // 
            // pgContent
            // 
            this.pgContent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pgContent.LineColor = System.Drawing.SystemColors.ScrollBar;
            this.pgContent.Location = new System.Drawing.Point(3, 28);
            this.pgContent.Name = "pgContent";
            this.pgContent.Size = new System.Drawing.Size(202, 252);
            this.pgContent.TabIndex = 0;
            this.pgContent.ToolbarVisible = false;
            this.pgContent.PropertyValueChanged += OnContent_PropertyValueChanged;
            this.pgContent.PropertyValueChanged += new System.Windows.Forms.PropertyValueChangedEventHandler(pPropertyValueChangedEvent);
            // 
            // tsToolbar (Strategy)
            // 
            this.tsToolbarStrategy.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsStrategy_CoinStream_PlayerMode,
            this.tsStrategy_CoinStream_Forward,
            this.tsStrategy_Separator1,
            this.tsStrategy_CoinStream});
            this.tsToolbarStrategy.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tsToolbarStrategy.Location = new System.Drawing.Point(0, 0);
            this.tsToolbarStrategy.Name = "tsToolbar";
            this.tsToolbarStrategy.Size = new System.Drawing.Size(208, 25);
            this.tsToolbarStrategy.TabIndex = 1;
            this.tsToolbarStrategy.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(eToolbarItemClickedEvent);
            // 
            // tsStrategy_CoinStream_PlayerMode
            // 
            this.tsStrategy_CoinStream_PlayerMode.AutoSize = false;
            this.tsStrategy_CoinStream_PlayerMode.AutoToolTip = false;
            this.tsStrategy_CoinStream_PlayerMode.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsStrategy_CoinStream_PlayerMode.Font = new System.Drawing.Font("Webdings", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.tsStrategy_CoinStream_PlayerMode.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsStrategy_CoinStream_PlayerMode.Name = "tsStrategy_CoinStream_Player_Mode";
            this.tsStrategy_CoinStream_PlayerMode.Size = new System.Drawing.Size(25, 17);
            this.tsStrategy_CoinStream_PlayerMode.Text = "4";
            this.tsStrategy_CoinStream_PlayerMode.ToolTipText = "Anzuwendender Wiedergabemodus des Coin-Stream Replays.";
            // 
            // tsStrategy_CoinStream_Forward
            // 
            this.tsStrategy_CoinStream_Forward.AutoSize = false;
            this.tsStrategy_CoinStream_Forward.AutoToolTip = false;
            this.tsStrategy_CoinStream_Forward.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsStrategy_CoinStream_Forward.Font = new System.Drawing.Font("Webdings", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.tsStrategy_CoinStream_Forward.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsStrategy_CoinStream_Forward.Name = "tsStrategy_CoinStream_Player_Mode";
            this.tsStrategy_CoinStream_Forward.Size = new System.Drawing.Size(25, 17);
            this.tsStrategy_CoinStream_Forward.Text = "8";
            this.tsStrategy_CoinStream_Forward.ToolTipText = "Coin-Stream Replay vorspulen";
            // 
            // tsStrategy_Separator1
            // 
            this.tsStrategy_Separator1.Name = "tsStrategy_Separator1";
            this.tsStrategy_Separator1.Size = new System.Drawing.Size(6, 20);
            // 
            // tsStrategy_CoinStream
            // 
            this.tsStrategy_CoinStream.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsStrategy_CoinStream.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsStrategy_CoinStream_Record});
            this.tsStrategy_CoinStream.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsStrategy_CoinStream.Name = "tsStrategy_CoinStream";
            this.tsStrategy_CoinStream.Size = new System.Drawing.Size(90, 17);
            this.tsStrategy_CoinStream.Text = "Coin-Stream";
            this.tsStrategy_CoinStream.DropDownItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(eToolbarItemClickedEvent);
            // 
            // tsStrategy_CoinStream_Record
            // 
            this.tsStrategy_CoinStream_Record.Name = "tsStrategy_CoinStream_Record";
            this.tsStrategy_CoinStream_Record.Size = new System.Drawing.Size(130, 22);
            this.tsStrategy_CoinStream_Record.Text = "Aufnahme";
            // 
            // tsToolbar (Strategy)
            // 
            this.tsToolbarParameters.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsParameters_New,
            this.tsParameters_Remove,
            this.tsParameters_Choice,
            this.tsParameters_ActivateChanges,
            this.tsParameters_SetReference});
            this.tsToolbarParameters.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tsToolbarParameters.Location = new System.Drawing.Point(0, 0);
            this.tsToolbarParameters.Name = "tsToolbar";
            this.tsToolbarParameters.Size = new System.Drawing.Size(208, 25);
            this.tsToolbarParameters.TabIndex = 1;
            this.tsToolbarParameters.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(eToolbarItemClickedEvent);
            // 
            // tsParameters_New
            // 
            this.tsParameters_New.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsParameters_New.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsParameters_New_Selector,
            this.tsParameters_New_Settings});
            this.tsParameters_New.Image = ((System.Drawing.Image)(resources.GetObject("tsParameters_New.Image")));
            this.tsParameters_New.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsParameters_New.Name = "tsParameters_New";
            this.tsParameters_New.Size = new System.Drawing.Size(32, 22);
            this.tsParameters_New.Text = "Neue Parameter erstellen";
            // 
            // tsParameters_New_Selector
            // 
            this.tsParameters_New_Selector.Name = "tsParameters_New_Selector";
            this.tsParameters_New_Selector.Size = new System.Drawing.Size(152, 22);
            this.tsParameters_New_Selector.Text = "Selektor";
            this.tsParameters_New_Selector.Click += OnParametersNewSelector_Click;
            // 
            // tsParameters_New_Settings
            // 
            this.tsParameters_New_Settings.Name = "tsParameters_New_Settings";
            this.tsParameters_New_Settings.Size = new System.Drawing.Size(152, 22);
            this.tsParameters_New_Settings.Text = "Einstellungen";
            this.tsParameters_New_Settings.Click += OnParametersNewSettings_Click;
            // 
            // tsParameters_Remove
            // 
            this.tsParameters_Remove.Image = ((System.Drawing.Image)(resources.GetObject("tsParameters_Remove.Image")));
            this.tsParameters_Remove.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsParameters_Remove.Name = "tsParameters_Remove";
            this.tsParameters_Remove.Size = new System.Drawing.Size(23, 22);
            this.tsParameters_Remove.ToolTipText = "Parameter entfernen";
            this.tsParameters_Remove.Click += OnParametersRemove_Click;
            // 
            // tsParameters_Choice
            // 
            this.tsParameters_Choice.Name = "tsParameters_Choice";
            this.tsParameters_Choice.Size = new System.Drawing.Size(150, 25);
            this.tsParameters_Choice.Sorted = true;
            this.tsParameters_Choice.DropDownStyle = ComboBoxStyle.DropDownList;
            this.tsParameters_Choice.SelectedIndexChanged += OnParametersChoice_SelectedIndexChanged;
            // 
            // tsParameters_Activate
            // 
            this.tsParameters_ActivateChanges.Image = ((System.Drawing.Image)(resources.GetObject("tsParameters_ActivateChanges.Image")));
            this.tsParameters_ActivateChanges.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsParameters_ActivateChanges.Name = "tsParameters_Activate";
            this.tsParameters_ActivateChanges.Size = new System.Drawing.Size(23, 22);
            this.tsParameters_ActivateChanges.ToolTipText = "�nderungen in aktiven P�rchen anwenden";
            this.tsParameters_ActivateChanges.Click += OnParametersActivate_Click;
            // 
            // tsParameters_SetReference
            // 
            this.tsParameters_SetReference.Image = ((System.Drawing.Image)(resources.GetObject("tsParameters_SetReference.Image")));
            this.tsParameters_SetReference.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsParameters_SetReference.Name = "tsParameters_SetReference";
            this.tsParameters_SetReference.Size = new System.Drawing.Size(23, 22);
            this.tsParameters_SetReference.ToolTipText = "Aktuelle Parameter in allen P�rchen referenzieren";
            this.tsParameters_SetReference.Click += OnParametersSetReference_Click;
            // 
            // PropertyWnd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.ClientSize = new System.Drawing.Size(208, 289);
            this.Controls.Add(this.tlpLayout);
            this.HideOnClose = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "PropertyWnd";
            this.Padding = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.ShowHint = WeifenLuo.WinFormsUI.Docking.DockState.DockRight;
            this.TabText = "[TITLE]";
            this.Text = "[TITLE]";
            this.tlpLayout.ResumeLayout(false);
            this.tlpLayout.PerformLayout();
            this.ResumeLayout(false);
        }
        #endregion

        private System.Windows.Forms.PropertyGrid pgContent;
        private System.Windows.Forms.ToolStrip tsToolbarStrategy;
        private System.Windows.Forms.TableLayoutPanel tlpLayout;
        public System.Windows.Forms.ToolStripButton tsStrategy_CoinStream_PlayerMode;
        public System.Windows.Forms.ToolStripButton tsStrategy_CoinStream_Forward;
        private System.Windows.Forms.ToolStripSeparator tsStrategy_Separator1;
        private System.Windows.Forms.ToolStripSplitButton tsStrategy_CoinStream;
        public System.Windows.Forms.ToolStripMenuItem tsStrategy_CoinStream_Record;
        private System.Windows.Forms.ToolStrip tsToolbarParameters;
        private System.Windows.Forms.ToolStripSplitButton tsParameters_New;
        public System.Windows.Forms.ToolStripButton tsParameters_Remove;
        private System.Windows.Forms.ToolStripMenuItem tsParameters_New_Selector;
        private System.Windows.Forms.ToolStripMenuItem tsParameters_New_Settings;
        private System.Windows.Forms.ToolStripComboBox tsParameters_Choice;
        public System.Windows.Forms.ToolStripButton tsParameters_ActivateChanges;
        public System.Windows.Forms.ToolStripButton tsParameters_SetReference;
    }
}