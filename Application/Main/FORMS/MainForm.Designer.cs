namespace CryptoRiftBot
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.bwWebReqThread = new System.ComponentModel.BackgroundWorker();
            this.mainMenu = new System.Windows.Forms.MenuStrip();
            this.miFile = new System.Windows.Forms.ToolStripMenuItem();
            this.miFile_State = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.miFile_Settings = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem4 = new System.Windows.Forms.ToolStripSeparator();
            this.miFile_Exit = new System.Windows.Forms.ToolStripMenuItem();
            this.miFile_ExitWithoutSavingLayout = new System.Windows.Forms.ToolStripMenuItem();
            this.miView = new System.Windows.Forms.ToolStripMenuItem();
            this.miView_Layouts = new System.Windows.Forms.ToolStripMenuItem();
            this.miView_Layouts_Scheme = new System.Windows.Forms.ToolStripMenuItem();
            this.miView_Layouts_Scheme_VS2015Light = new System.Windows.Forms.ToolStripMenuItem();
            this.miView_Layouts_Scheme_VS2015Blue = new System.Windows.Forms.ToolStripMenuItem();
            this.miView_Layouts_Scheme_VS2015Dark = new System.Windows.Forms.ToolStripMenuItem();
            this.miView_Layouts_Scheme_Separator1 = new System.Windows.Forms.ToolStripSeparator();
            this.miView_Layouts_Scheme_VS2013Light = new System.Windows.Forms.ToolStripMenuItem();
            this.miView_Layouts_Scheme_VS2013Blue = new System.Windows.Forms.ToolStripMenuItem();
            this.miView_Layouts_Scheme_VS2013Dark = new System.Windows.Forms.ToolStripMenuItem();
            this.miView_Layouts_Scheme_Separator2 = new System.Windows.Forms.ToolStripSeparator();
            this.miView_Layouts_Scheme_VS2012Light = new System.Windows.Forms.ToolStripMenuItem();
            this.miView_Layouts_Scheme_VS2012Blue = new System.Windows.Forms.ToolStripMenuItem();
            this.miView_Layouts_Scheme_VS2012Dark = new System.Windows.Forms.ToolStripMenuItem();
            this.miView_Layouts_Scheme_Separator3 = new System.Windows.Forms.ToolStripSeparator();
            this.miView_Layouts_Scheme_VS2005 = new System.Windows.Forms.ToolStripMenuItem();
            this.miView_Layouts_Scheme_VS2003 = new System.Windows.Forms.ToolStripMenuItem();
            this.miView_Layouts_Style = new System.Windows.Forms.ToolStripMenuItem();
            this.miView_Layouts_Style_DockingMDI = new System.Windows.Forms.ToolStripMenuItem();
            this.miView_Layouts_Style_DockingSDI = new System.Windows.Forms.ToolStripMenuItem();
            this.miView_Layouts_Style_DockingWindow = new System.Windows.Forms.ToolStripMenuItem();
            this.miView_Layouts_Style_MDI = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem5 = new System.Windows.Forms.ToolStripSeparator();
            this.menuItemLockLayout = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemShowDocumentIcon = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.miView_Layouts_Default = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.miView_PropWnd = new System.Windows.Forms.ToolStripMenuItem();
            this.miView_PropWnd_Platform = new System.Windows.Forms.ToolStripMenuItem();
            this.miView_PropWnd_Strategy = new System.Windows.Forms.ToolStripMenuItem();
            this.miView_PropWnd_Parameter = new System.Windows.Forms.ToolStripMenuItem();
            this.miView_PropWnd_BotAutomatic = new System.Windows.Forms.ToolStripMenuItem();
            this.miView_PropWnd_Statistics = new System.Windows.Forms.ToolStripMenuItem();
            this.miView_PropWnd_Variables = new System.Windows.Forms.ToolStripMenuItem();
            this.miView_WalletWnd = new System.Windows.Forms.ToolStripMenuItem();
            this.miView_LogWnd = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.miView_ToolBar = new System.Windows.Forms.ToolStripMenuItem();
            this.miView_StatusBar = new System.Windows.Forms.ToolStripMenuItem();
            this.miWnd = new System.Windows.Forms.ToolStripMenuItem();
            this.miWnd_New = new System.Windows.Forms.ToolStripMenuItem();
            this.miDebug = new System.Windows.Forms.ToolStripMenuItem();
            this.miDebug_ManOrders = new System.Windows.Forms.ToolStripMenuItem();
            this.miDebug_ManOrders_Buy = new System.Windows.Forms.ToolStripMenuItem();
            this.miDebug_ManOrders_Sell = new System.Windows.Forms.ToolStripMenuItem();
            this.miDebug_Simulation = new System.Windows.Forms.ToolStripMenuItem();
            this.miDebug_Simulation_ResetRift = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.miDebug_StateShot = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.miDebug_ToggleMode = new System.Windows.Forms.ToolStripMenuItem();
#if DEBUG
            this.miDebug_OpenDocument = new System.Windows.Forms.ToolStripMenuItem();
#endif
            this.miAsterisk = new System.Windows.Forms.ToolStripMenuItem();
            this.miAsterisk_Help = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem7 = new System.Windows.Forms.ToolStripSeparator();
            this.miAsterisk_About = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem6 = new System.Windows.Forms.ToolStripSeparator();
            this.ssStateBar = new System.Windows.Forms.StatusStrip();
            this.StateBar_TxtState = new System.Windows.Forms.ToolStripStatusLabel();
            this.StateBar_State = new System.Windows.Forms.ToolStripStatusLabel();
            this.StateBar_TxtServices = new System.Windows.Forms.ToolStripStatusLabel();
            this.StateBar_TxtPlatform = new System.Windows.Forms.ToolStripStatusLabel();
            this.StateBar_Platform = new System.Windows.Forms.ToolStripDropDownButton();
            this.StateBar_TxtPair = new System.Windows.Forms.ToolStripStatusLabel();
            this.StateBar_Pair = new System.Windows.Forms.ToolStripDropDownButton();
            this.StateBar_Progress = new System.Windows.Forms.ToolStripProgressBar();
            this.StateBar_CurOperation = new System.Windows.Forms.ToolStripStatusLabel();
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.toolBar = new System.Windows.Forms.ToolStrip();
            this.sbPropWnd = new System.Windows.Forms.ToolStripSplitButton();
            this.sbPropWnd_Platform = new System.Windows.Forms.ToolStripMenuItem();
            this.sbPropWnd_Strategy = new System.Windows.Forms.ToolStripMenuItem();
            this.sbPropWnd_Parameter = new System.Windows.Forms.ToolStripMenuItem();
            this.sbPropWnd_BotAutomatic = new System.Windows.Forms.ToolStripMenuItem();
            this.sbPropWnd_Statistics = new System.Windows.Forms.ToolStripMenuItem();
            this.sbPropWnd_Variables = new System.Windows.Forms.ToolStripMenuItem();
            this.sbWalletWnd = new System.Windows.Forms.ToolStripButton();
            this.toolBarButtonLogWindow = new System.Windows.Forms.ToolStripButton();
            this.dDockPanel = new WeifenLuo.WinFormsUI.Docking.DockPanel();
            this.vS2005Theme1 = new WeifenLuo.WinFormsUI.Docking.VS2005Theme();
            this.vS2003Theme1 = new WeifenLuo.WinFormsUI.Docking.VS2003Theme();
            this.vS2015LightTheme1 = new WeifenLuo.WinFormsUI.Docking.VS2015LightTheme();
            this.vS2015BlueTheme1 = new WeifenLuo.WinFormsUI.Docking.VS2015BlueTheme();
            this.vS2015DarkTheme1 = new WeifenLuo.WinFormsUI.Docking.VS2015DarkTheme();
            this.vS2013LightTheme1 = new WeifenLuo.WinFormsUI.Docking.VS2013LightTheme();
            this.vS2013BlueTheme1 = new WeifenLuo.WinFormsUI.Docking.VS2013BlueTheme();
            this.vS2013DarkTheme1 = new WeifenLuo.WinFormsUI.Docking.VS2013DarkTheme();
            this.vS2012LightTheme1 = new WeifenLuo.WinFormsUI.Docking.VS2012LightTheme();
            this.vS2012BlueTheme1 = new WeifenLuo.WinFormsUI.Docking.VS2012BlueTheme();
            this.vS2012DarkTheme1 = new WeifenLuo.WinFormsUI.Docking.VS2012DarkTheme();
            this.vsToolStripExtender1 = new WeifenLuo.WinFormsUI.Docking.VisualStudioToolStripExtender(this.components);
            this.mainMenu.SuspendLayout();
            this.ssStateBar.SuspendLayout();
            this.toolBar.SuspendLayout();
            this.SuspendLayout();
            // 
            // bwWebReqThread
            // 
            this.bwWebReqThread.WorkerReportsProgress = true;
            this.bwWebReqThread.WorkerSupportsCancellation = true;
            this.bwWebReqThread.DoWork += new System.ComponentModel.DoWorkEventHandler(this.OnWebReqThread_DoWork);
            this.bwWebReqThread.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.OnWebReqThread_ProgressChanged);
            this.bwWebReqThread.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.OnWebReqThread_RunWorkerCompleted);
            // 
            // mainMenu
            // 
            this.mainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miFile,
            this.miView,
            this.miWnd,
            this.miDebug,
            this.miAsterisk});
            this.mainMenu.Location = new System.Drawing.Point(0, 0);
            this.mainMenu.MdiWindowListItem = this.miWnd;
            this.mainMenu.Name = "mainMenu";
            this.mainMenu.Size = new System.Drawing.Size(579, 24);
            this.mainMenu.TabIndex = 7;
            // 
            // miFile
            // 
            this.miFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miFile_State,
            this.toolStripSeparator4,
            this.miFile_Settings,
            this.menuItem4,
            this.miFile_Exit,
            this.miFile_ExitWithoutSavingLayout});
            this.miFile.Name = "miFile";
            this.miFile.Size = new System.Drawing.Size(46, 20);
            this.miFile.Text = "&Datei";
            // 
            // miFile_State
            // 
            this.miFile_State.Name = "miFile_State";
            this.miFile_State.Size = new System.Drawing.Size(152, 22);
            this.miFile_State.Text = "State";
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(149, 6);
            // 
            // miFile_Settings
            // 
            this.miFile_Settings.Enabled = false;
            this.miFile_Settings.Name = "miFile_Settings";
            this.miFile_Settings.Size = new System.Drawing.Size(152, 22);
            this.miFile_Settings.Text = "Einstellungen";
            this.miFile_Settings.Click += new System.EventHandler(this.OnMenu_File_Settings);
            // 
            // menuItem4
            // 
            this.menuItem4.Name = "menuItem4";
            this.menuItem4.Size = new System.Drawing.Size(149, 6);
            // 
            // miFile_Exit
            // 
            this.miFile_Exit.Name = "miFile_Exit";
            this.miFile_Exit.Size = new System.Drawing.Size(152, 22);
            this.miFile_Exit.Text = "&Beenden";
            this.miFile_Exit.Click += new System.EventHandler(this.OnMenu_File_Exit);
            // 
            // miFile_ExitWithoutSavingLayout
            // 
            this.miFile_ExitWithoutSavingLayout.Name = "miFile_ExitWithoutSavingLayout";
            this.miFile_ExitWithoutSavingLayout.Size = new System.Drawing.Size(152, 22);
            this.miFile_ExitWithoutSavingLayout.Text = "Beenden (&O.L.)";
            this.miFile_ExitWithoutSavingLayout.Visible = false;
            this.miFile_ExitWithoutSavingLayout.Click += new System.EventHandler(this.OnMenu_File_ExitWithoutSavingLayout);
            // 
            // miView
            // 
            this.miView.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miView_Layouts,
            this.menuItem2,
            this.miView_PropWnd,
            this.miView_WalletWnd,
            this.miView_LogWnd,
            this.menuItem1,
            this.miView_ToolBar,
            this.miView_StatusBar});
            this.miView.MergeIndex = 1;
            this.miView.Name = "miView";
            this.miView.Size = new System.Drawing.Size(59, 20);
            this.miView.Text = "&Ansicht";
            // 
            // miView_Layouts
            // 
            this.miView_Layouts.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miView_Layouts_Scheme,
            this.miView_Layouts_Style,
            this.menuItem5,
            this.menuItemLockLayout,
            this.menuItemShowDocumentIcon,
            this.toolStripSeparator1,
            this.miView_Layouts_Default});
            this.miView_Layouts.Name = "miView_Layouts";
            this.miView_Layouts.Size = new System.Drawing.Size(148, 22);
            this.miView_Layouts.Text = "Layouts";
            // 
            // miView_Layouts_Scheme
            // 
            this.miView_Layouts_Scheme.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miView_Layouts_Scheme_VS2015Light,
            this.miView_Layouts_Scheme_VS2015Blue,
            this.miView_Layouts_Scheme_VS2015Dark,
            this.miView_Layouts_Scheme_Separator1,
            this.miView_Layouts_Scheme_VS2013Light,
            this.miView_Layouts_Scheme_VS2013Blue,
            this.miView_Layouts_Scheme_VS2013Dark,
            this.miView_Layouts_Scheme_Separator2,
            this.miView_Layouts_Scheme_VS2012Light,
            this.miView_Layouts_Scheme_VS2012Blue,
            this.miView_Layouts_Scheme_VS2012Dark,
            this.miView_Layouts_Scheme_Separator3,
            this.miView_Layouts_Scheme_VS2005,
            this.miView_Layouts_Scheme_VS2003});
            this.miView_Layouts_Scheme.Name = "miView_Layouts_Scheme";
            this.miView_Layouts_Scheme.Size = new System.Drawing.Size(188, 22);
            this.miView_Layouts_Scheme.Text = "Schemen";
            // 
            // miView_Layouts_Scheme_VS2015Light
            // 
            this.miView_Layouts_Scheme_VS2015Light.Name = "miView_Layouts_Scheme_VS2015Light";
            this.miView_Layouts_Scheme_VS2015Light.Size = new System.Drawing.Size(199, 22);
            this.miView_Layouts_Scheme_VS2015Light.Text = "Visual Studio 2015 Light";
            this.miView_Layouts_Scheme_VS2015Light.Visible = false;
            this.miView_Layouts_Scheme_VS2015Light.Click += new System.EventHandler(this.SetSchema);
            // 
            // miView_Layouts_Scheme_VS2015Blue
            // 
            this.miView_Layouts_Scheme_VS2015Blue.Name = "miView_Layouts_Scheme_VS2015Blue";
            this.miView_Layouts_Scheme_VS2015Blue.Size = new System.Drawing.Size(199, 22);
            this.miView_Layouts_Scheme_VS2015Blue.Text = "Visual Studio 2015 Blue";
            this.miView_Layouts_Scheme_VS2015Blue.Visible = false;
            this.miView_Layouts_Scheme_VS2015Blue.Click += new System.EventHandler(this.SetSchema);
            // 
            // miView_Layouts_Scheme_VS2015Dark
            // 
            this.miView_Layouts_Scheme_VS2015Dark.Name = "miView_Layouts_Scheme_VS2015Dark";
            this.miView_Layouts_Scheme_VS2015Dark.Size = new System.Drawing.Size(199, 22);
            this.miView_Layouts_Scheme_VS2015Dark.Text = "Visual Studio 2015 Dark";
            this.miView_Layouts_Scheme_VS2015Dark.Visible = false;
            this.miView_Layouts_Scheme_VS2015Dark.Click += new System.EventHandler(this.SetSchema);
            // 
            // miView_Layouts_Scheme_Separator1
            // 
            this.miView_Layouts_Scheme_Separator1.Name = "miView_Layouts_Scheme_Separator1";
            this.miView_Layouts_Scheme_Separator1.Size = new System.Drawing.Size(196, 6);
            this.miView_Layouts_Scheme_Separator1.Visible = false;
            // 
            // miView_Layouts_Scheme_VS2013Light
            // 
            this.miView_Layouts_Scheme_VS2013Light.Name = "miView_Layouts_Scheme_VS2013Light";
            this.miView_Layouts_Scheme_VS2013Light.Size = new System.Drawing.Size(199, 22);
            this.miView_Layouts_Scheme_VS2013Light.Text = "Visual Studio 2013 Light";
            this.miView_Layouts_Scheme_VS2013Light.Visible = false;
            this.miView_Layouts_Scheme_VS2013Light.Click += new System.EventHandler(this.SetSchema);
            // 
            // miView_Layouts_Scheme_VS2013Blue
            // 
            this.miView_Layouts_Scheme_VS2013Blue.Name = "miView_Layouts_Scheme_VS2013Blue";
            this.miView_Layouts_Scheme_VS2013Blue.Size = new System.Drawing.Size(199, 22);
            this.miView_Layouts_Scheme_VS2013Blue.Text = "Visual Studio 2013 Blue";
            this.miView_Layouts_Scheme_VS2013Blue.Visible = false;
            this.miView_Layouts_Scheme_VS2013Blue.Click += new System.EventHandler(this.SetSchema);
            // 
            // miView_Layouts_Scheme_VS2013Dark
            // 
            this.miView_Layouts_Scheme_VS2013Dark.Name = "miView_Layouts_Scheme_VS2013Dark";
            this.miView_Layouts_Scheme_VS2013Dark.Size = new System.Drawing.Size(199, 22);
            this.miView_Layouts_Scheme_VS2013Dark.Text = "Visual Studio 2013 Dark";
            this.miView_Layouts_Scheme_VS2013Dark.Visible = false;
            this.miView_Layouts_Scheme_VS2013Dark.Click += new System.EventHandler(this.SetSchema);
            // 
            // miView_Layouts_Scheme_Separator2
            // 
            this.miView_Layouts_Scheme_Separator2.Name = "miView_Layouts_Scheme_Separator2";
            this.miView_Layouts_Scheme_Separator2.Size = new System.Drawing.Size(196, 6);
            this.miView_Layouts_Scheme_Separator2.Visible = false;
            // 
            // miView_Layouts_Scheme_VS2012Light
            // 
            this.miView_Layouts_Scheme_VS2012Light.Name = "miView_Layouts_Scheme_VS2012Light";
            this.miView_Layouts_Scheme_VS2012Light.Size = new System.Drawing.Size(199, 22);
            this.miView_Layouts_Scheme_VS2012Light.Text = "Visual Studio 2012 Light";
            this.miView_Layouts_Scheme_VS2012Light.Visible = false;
            this.miView_Layouts_Scheme_VS2012Light.Click += new System.EventHandler(this.SetSchema);
            // 
            // miView_Layouts_Scheme_VS2012Blue
            // 
            this.miView_Layouts_Scheme_VS2012Blue.Name = "miView_Layouts_Scheme_VS2012Blue";
            this.miView_Layouts_Scheme_VS2012Blue.Size = new System.Drawing.Size(199, 22);
            this.miView_Layouts_Scheme_VS2012Blue.Text = "Visual Studio 2012 Blue";
            this.miView_Layouts_Scheme_VS2012Blue.Visible = false;
            this.miView_Layouts_Scheme_VS2012Blue.Click += new System.EventHandler(this.SetSchema);
            // 
            // miView_Layouts_Scheme_VS2012Dark
            // 
            this.miView_Layouts_Scheme_VS2012Dark.Name = "miView_Layouts_Scheme_VS2012Dark";
            this.miView_Layouts_Scheme_VS2012Dark.Size = new System.Drawing.Size(199, 22);
            this.miView_Layouts_Scheme_VS2012Dark.Text = "Visual Studio 2012 Dark";
            this.miView_Layouts_Scheme_VS2012Dark.Visible = false;
            this.miView_Layouts_Scheme_VS2012Dark.Click += new System.EventHandler(this.SetSchema);
            // 
            // miView_Layouts_Scheme_Separator3
            // 
            this.miView_Layouts_Scheme_Separator3.Name = "miView_Layouts_Scheme_Separator3";
            this.miView_Layouts_Scheme_Separator3.Size = new System.Drawing.Size(196, 6);
            this.miView_Layouts_Scheme_Separator3.Visible = false;
            // 
            // miView_Layouts_Scheme_VS2005
            // 
            this.miView_Layouts_Scheme_VS2005.Checked = true;
            this.miView_Layouts_Scheme_VS2005.CheckState = System.Windows.Forms.CheckState.Checked;
            this.miView_Layouts_Scheme_VS2005.Name = "miView_Layouts_Scheme_VS2005";
            this.miView_Layouts_Scheme_VS2005.Size = new System.Drawing.Size(199, 22);
            this.miView_Layouts_Scheme_VS2005.Text = "Visual Studio 200&5";
            this.miView_Layouts_Scheme_VS2005.Click += new System.EventHandler(this.SetSchema);
            // 
            // miView_Layouts_Scheme_VS2003
            // 
            this.miView_Layouts_Scheme_VS2003.Name = "miView_Layouts_Scheme_VS2003";
            this.miView_Layouts_Scheme_VS2003.Size = new System.Drawing.Size(199, 22);
            this.miView_Layouts_Scheme_VS2003.Text = "Visual Studio 200&3";
            this.miView_Layouts_Scheme_VS2003.Click += new System.EventHandler(this.SetSchema);
            // 
            // miView_Layouts_Style
            // 
            this.miView_Layouts_Style.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miView_Layouts_Style_DockingMDI,
            this.miView_Layouts_Style_DockingSDI,
            this.miView_Layouts_Style_DockingWindow,
            this.miView_Layouts_Style_MDI});
            this.miView_Layouts_Style.Name = "miView_Layouts_Style";
            this.miView_Layouts_Style.Size = new System.Drawing.Size(188, 22);
            this.miView_Layouts_Style.Text = "Stiele";
            this.miView_Layouts_Style.Visible = false;
            // 
            // miView_Layouts_Style_DockingMDI
            // 
            this.miView_Layouts_Style_DockingMDI.Checked = true;
            this.miView_Layouts_Style_DockingMDI.CheckState = System.Windows.Forms.CheckState.Checked;
            this.miView_Layouts_Style_DockingMDI.Name = "miView_Layouts_Style_DockingMDI";
            this.miView_Layouts_Style_DockingMDI.Size = new System.Drawing.Size(165, 22);
            this.miView_Layouts_Style_DockingMDI.Text = "Docking &MDI";
            this.miView_Layouts_Style_DockingMDI.Click += new System.EventHandler(this.SetDocumentStyle);
            // 
            // miView_Layouts_Style_DockingSDI
            // 
            this.miView_Layouts_Style_DockingSDI.Name = "miView_Layouts_Style_DockingSDI";
            this.miView_Layouts_Style_DockingSDI.Size = new System.Drawing.Size(165, 22);
            this.miView_Layouts_Style_DockingSDI.Text = "Docking &SDI";
            this.miView_Layouts_Style_DockingSDI.Click += new System.EventHandler(this.SetDocumentStyle);
            // 
            // miView_Layouts_Style_DockingWindow
            // 
            this.miView_Layouts_Style_DockingWindow.Name = "miView_Layouts_Style_DockingWindow";
            this.miView_Layouts_Style_DockingWindow.Size = new System.Drawing.Size(165, 22);
            this.miView_Layouts_Style_DockingWindow.Text = "Docking &Window";
            this.miView_Layouts_Style_DockingWindow.Click += new System.EventHandler(this.SetDocumentStyle);
            // 
            // miView_Layouts_Style_MDI
            // 
            this.miView_Layouts_Style_MDI.Name = "miView_Layouts_Style_MDI";
            this.miView_Layouts_Style_MDI.Size = new System.Drawing.Size(165, 22);
            this.miView_Layouts_Style_MDI.Text = "S&ystem MDI";
            this.miView_Layouts_Style_MDI.Click += new System.EventHandler(this.SetDocumentStyle);
            // 
            // menuItem5
            // 
            this.menuItem5.Name = "menuItem5";
            this.menuItem5.Size = new System.Drawing.Size(185, 6);
            // 
            // menuItemLockLayout
            // 
            this.menuItemLockLayout.Name = "menuItemLockLayout";
            this.menuItemLockLayout.Size = new System.Drawing.Size(188, 22);
            this.menuItemLockLayout.Text = "&Lock Layout";
            this.menuItemLockLayout.Click += new System.EventHandler(this.menuItemLockLayout_Click);
            // 
            // menuItemShowDocumentIcon
            // 
            this.menuItemShowDocumentIcon.Name = "menuItemShowDocumentIcon";
            this.menuItemShowDocumentIcon.Size = new System.Drawing.Size(188, 22);
            this.menuItemShowDocumentIcon.Text = "&Show Document Icon";
            this.menuItemShowDocumentIcon.Click += new System.EventHandler(this.menuItemShowDocumentIcon_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(185, 6);
            // 
            // miView_Layouts_Default
            // 
            this.miView_Layouts_Default.Image = ((System.Drawing.Image)(resources.GetObject("miView_Layouts_Default.Image")));
            this.miView_Layouts_Default.Name = "miView_Layouts_Default";
            this.miView_Layouts_Default.Size = new System.Drawing.Size(188, 22);
            this.miView_Layouts_Default.Text = "Standard-Anordnung";
            this.miView_Layouts_Default.Click += new System.EventHandler(this.OnMenu_View_Layouts_Default);
            // 
            // menuItem2
            // 
            this.menuItem2.Name = "menuItem2";
            this.menuItem2.Size = new System.Drawing.Size(145, 6);
            // 
            // miView_PropWnd
            // 
            this.miView_PropWnd.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miView_PropWnd_Platform,
            this.miView_PropWnd_Strategy,
            this.miView_PropWnd_Parameter,
            this.miView_PropWnd_BotAutomatic,
            this.miView_PropWnd_Statistics,
            this.miView_PropWnd_Variables});
            this.miView_PropWnd.Name = "miView_PropWnd";
            this.miView_PropWnd.Size = new System.Drawing.Size(148, 22);
            this.miView_PropWnd.Text = "&Eigenschaften";
            // 
            // miView_PropWnd_Platform
            // 
            this.miView_PropWnd_Platform.Name = "miView_PropWnd_Platform";
            this.miView_PropWnd_Platform.Size = new System.Drawing.Size(197, 22);
            this.miView_PropWnd_Platform.Text = "Plattform";
            this.miView_PropWnd_Platform.Click += new System.EventHandler(this.OnMenu_View_PropWnd_Platform);
            // 
            // miView_PropWnd_Strategy
            // 
            this.miView_PropWnd_Strategy.Name = "miView_PropWnd_Strategy";
            this.miView_PropWnd_Strategy.Size = new System.Drawing.Size(197, 22);
            this.miView_PropWnd_Strategy.Text = "Strategie";
            this.miView_PropWnd_Strategy.Click += new System.EventHandler(this.OnMenu_View_PropWnd_Strategy);
            // 
            // miView_PropWnd_Parameter
            // 
            this.miView_PropWnd_Parameter.Name = "miView_PropWnd_Parameter";
            this.miView_PropWnd_Parameter.Size = new System.Drawing.Size(197, 22);
            this.miView_PropWnd_Parameter.Text = "Parameter";
            this.miView_PropWnd_Parameter.Click += new System.EventHandler(this.OnMenu_View_PropWnd_Parameter);
            // 
            // miView_PropWnd_BotAutomatic
            // 
            this.miView_PropWnd_BotAutomatic.Name = "miView_PropWnd_BotAutomatic";
            this.miView_PropWnd_BotAutomatic.Size = new System.Drawing.Size(197, 22);
            this.miView_PropWnd_BotAutomatic.Text = "Trading-Bot Automatik";
            this.miView_PropWnd_BotAutomatic.Click += new System.EventHandler(this.OnMenu_View_PropWnd_BotAutomatic);
            // 
            // miView_PropWnd_Statistics
            // 
            this.miView_PropWnd_Statistics.Name = "miView_PropWnd_Statistics";
            this.miView_PropWnd_Statistics.Size = new System.Drawing.Size(197, 22);
            this.miView_PropWnd_Statistics.Text = "�bersicht";
            this.miView_PropWnd_Statistics.Click += new System.EventHandler(this.OnMenu_View_PropWnd_Statistics);
            // 
            // miView_PropWnd_Variables
            // 
            this.miView_PropWnd_Variables.Name = "miView_PropWnd_Variables";
            this.miView_PropWnd_Variables.Size = new System.Drawing.Size(197, 22);
            this.miView_PropWnd_Variables.Text = "Variablen";
            this.miView_PropWnd_Variables.Click += new System.EventHandler(this.OnMenu_View_PropWnd_Variables);
            // 
            // miView_WalletWnd
            // 
            this.miView_WalletWnd.Image = ((System.Drawing.Image)(resources.GetObject("miView_WalletWnd.Image")));
            this.miView_WalletWnd.Name = "miView_WalletWnd";
            this.miView_WalletWnd.Size = new System.Drawing.Size(148, 22);
            this.miView_WalletWnd.Text = "Wallets";
            this.miView_WalletWnd.Click += new System.EventHandler(this.OnMenu_View_WalletWnd);
            // 
            // miView_LogWnd
            // 
            this.miView_LogWnd.Image = ((System.Drawing.Image)(resources.GetObject("miView_LogWnd.Image")));
            this.miView_LogWnd.Name = "miView_LogWnd";
            this.miView_LogWnd.Size = new System.Drawing.Size(148, 22);
            this.miView_LogWnd.Text = "&Log";
            this.miView_LogWnd.Click += new System.EventHandler(this.OnMenu_View_LogWnd);
            // 
            // menuItem1
            // 
            this.menuItem1.Name = "menuItem1";
            this.menuItem1.Size = new System.Drawing.Size(145, 6);
            // 
            // miView_ToolBar
            // 
            this.miView_ToolBar.Checked = true;
            this.miView_ToolBar.CheckState = System.Windows.Forms.CheckState.Checked;
            this.miView_ToolBar.Name = "miView_ToolBar";
            this.miView_ToolBar.Size = new System.Drawing.Size(148, 22);
            this.miView_ToolBar.Text = "Tool &Bar";
            this.miView_ToolBar.Click += new System.EventHandler(this.miView_ToolBar_Click);
            // 
            // miView_StatusBar
            // 
            this.miView_StatusBar.Checked = true;
            this.miView_StatusBar.CheckState = System.Windows.Forms.CheckState.Checked;
            this.miView_StatusBar.Name = "miView_StatusBar";
            this.miView_StatusBar.Size = new System.Drawing.Size(148, 22);
            this.miView_StatusBar.Text = "Status B&ar";
            this.miView_StatusBar.Click += new System.EventHandler(this.miView_StatusBar_Click);
            // 
            // miWnd
            // 
            this.miWnd.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miWnd_New});
            this.miWnd.MergeIndex = 0;
            this.miWnd.Name = "miWnd";
            this.miWnd.Size = new System.Drawing.Size(57, 20);
            this.miWnd.Text = "&Fenster";
            // 
            // miWnd_New
            // 
            this.miWnd_New.Name = "miWnd_New";
            this.miWnd_New.Size = new System.Drawing.Size(148, 22);
            this.miWnd_New.Text = "&Neues Fenster";
            this.miWnd_New.Visible = false;
            this.miWnd_New.Click += new System.EventHandler(this.menuItemNewWindow_Click);
            // 
            // miDebug
            // 
            this.miDebug.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miDebug_ManOrders,
            this.miDebug_Simulation,
            this.toolStripSeparator2,
            this.miDebug_StateShot,
            this.toolStripSeparator3,
            this.miDebug_ToggleMode
#if DEBUG
            ,this.miDebug_OpenDocument
#endif
            });
            this.miDebug.Name = "miDebug";
            this.miDebug.Size = new System.Drawing.Size(54, 20);
            this.miDebug.Text = "Debug";
            // 
            // miDebug_ManOrders
            // 
            this.miDebug_ManOrders.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miDebug_ManOrders_Buy,
            this.miDebug_ManOrders_Sell});
            this.miDebug_ManOrders.Name = "miDebug_ManOrders";
            this.miDebug_ManOrders.Size = new System.Drawing.Size(180, 22);
            this.miDebug_ManOrders.Text = "Manuelle Orders";
            // 
            // miDebug_ManOrders_Buy
            // 
            this.miDebug_ManOrders_Buy.Name = "miDebug_ManOrders_Buy";
            this.miDebug_ManOrders_Buy.Size = new System.Drawing.Size(127, 22);
            this.miDebug_ManOrders_Buy.Text = "Kaufen";
            this.miDebug_ManOrders_Buy.Click += new System.EventHandler(this.OnMenu_Debug_ManOrders_Buy);
            // 
            // miDebug_ManOrders_Sell
            // 
            this.miDebug_ManOrders_Sell.Name = "miDebug_ManOrders_Sell";
            this.miDebug_ManOrders_Sell.Size = new System.Drawing.Size(127, 22);
            this.miDebug_ManOrders_Sell.Text = "Verkaufen";
            this.miDebug_ManOrders_Sell.Click += new System.EventHandler(this.OnMenu_Debug_ManOrders_Sell);
            // 
            // miDebug_Simulation
            // 
            this.miDebug_Simulation.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miDebug_Simulation_ResetRift});
            this.miDebug_Simulation.Name = "miDebug_Simulation";
            this.miDebug_Simulation.Size = new System.Drawing.Size(180, 22);
            this.miDebug_Simulation.Text = "Simulation";
            // 
            // miDebug_Simulation_ResetRift
            // 
            this.miDebug_Simulation_ResetRift.Name = "miDebug_Simulation_ResetRift";
            this.miDebug_Simulation_ResetRift.Size = new System.Drawing.Size(163, 22);
            this.miDebug_Simulation_ResetRift.Text = "Rift zur�cksetzen";
            this.miDebug_Simulation_ResetRift.Click += new System.EventHandler(this.OnMenu_Simulation_ResetRift);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(177, 6);
            // 
            // miDebug_StateShot
            // 
            this.miDebug_StateShot.Name = "miDebug_StateShot";
            this.miDebug_StateShot.Size = new System.Drawing.Size(180, 22);
            this.miDebug_StateShot.Text = "State-Shot";
            this.miDebug_StateShot.Click += new System.EventHandler(this.OnMenu_Debug_StateShot);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(177, 6);
            // 
            // miDebug_ToggleMode
            // 
            this.miDebug_ToggleMode.Name = "miDebug_ToggleMode";
            this.miDebug_ToggleMode.Size = new System.Drawing.Size(180, 22);
            this.miDebug_ToggleMode.Text = "Debug-Modus";
            this.miDebug_ToggleMode.Click += new System.EventHandler(this.OnMenu_Debug_ToggleMode);
#if DEBUG
            // 
            // miDebug_OpenDocument
            // 
            this.miDebug_OpenDocument.Name = "miDebug_OpenDocument";
            this.miDebug_OpenDocument.Size = new System.Drawing.Size(180, 22);
            this.miDebug_OpenDocument.Text = "Debug-Chart";
            this.miDebug_OpenDocument.Click += new System.EventHandler(this.OnMenu_Debug_OpenDocument);
#endif
            // 
            // miAsterisk
            // 
            this.miAsterisk.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miAsterisk_Help,
            this.menuItem7,
            this.miAsterisk_About});
            this.miAsterisk.MergeIndex = 3;
            this.miAsterisk.Name = "miAsterisk";
            this.miAsterisk.Size = new System.Drawing.Size(24, 20);
            this.miAsterisk.Text = "?";
            // 
            // miAsterisk_Help
            // 
            this.miAsterisk_Help.Name = "miAsterisk_Help";
            this.miAsterisk_Help.Size = new System.Drawing.Size(116, 22);
            this.miAsterisk_Help.Text = "&Help";
            this.miAsterisk_Help.Click += new System.EventHandler(this.OnMenu_Asterisk_Help);
            // 
            // menuItem7
            // 
            this.menuItem7.Name = "menuItem7";
            this.menuItem7.Size = new System.Drawing.Size(113, 6);
            // 
            // miAsterisk_About
            // 
            this.miAsterisk_About.Name = "miAsterisk_About";
            this.miAsterisk_About.Size = new System.Drawing.Size(116, 22);
            this.miAsterisk_About.Text = "&About...";
            this.miAsterisk_About.Click += new System.EventHandler(this.OnMenu_Asterisk_About);
            // 
            // menuItem6
            // 
            this.menuItem6.Name = "menuItem6";
            this.menuItem6.Size = new System.Drawing.Size(252, 6);
            // 
            // ssStateBar
            // 
            this.ssStateBar.BackColor = System.Drawing.SystemColors.Control;
            this.ssStateBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StateBar_TxtState,
            this.StateBar_State,
            this.StateBar_TxtServices,
            this.StateBar_TxtPlatform,
            this.StateBar_Platform,
            this.StateBar_TxtPair,
            this.StateBar_Pair,
            this.StateBar_Progress,
            this.StateBar_CurOperation});
            this.ssStateBar.Location = new System.Drawing.Point(0, 387);
            this.ssStateBar.Name = "ssStateBar";
            this.ssStateBar.ShowItemToolTips = true;
            this.ssStateBar.Size = new System.Drawing.Size(579, 22);
            this.ssStateBar.TabIndex = 4;
            this.ssStateBar.Text = "Status";
            // 
            // StateBar_TxtState
            // 
            this.StateBar_TxtState.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.StateBar_TxtState.Name = "StateBar_TxtState";
            this.StateBar_TxtState.Size = new System.Drawing.Size(54, 17);
            this.StateBar_TxtState.Text = "Zustand:";
            this.StateBar_TxtState.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // StateBar_State
            // 
            this.StateBar_State.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.StateBar_State.Name = "StateBar_State";
            this.StateBar_State.Size = new System.Drawing.Size(16, 17);
            this.StateBar_State.Text = "...";
            this.StateBar_State.MouseDown += new System.Windows.Forms.MouseEventHandler(this.OnState_MouseDown);
            // 
            // StateBar_TxtServices
            // 
            this.StateBar_TxtServices.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.StateBar_TxtServices.Name = "StateBar_TxtServices";
            this.StateBar_TxtServices.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.StateBar_TxtServices.Size = new System.Drawing.Size(67, 17);
            this.StateBar_TxtServices.Text = "Services:";
            this.StateBar_TxtServices.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // StateBar_TxtPlatform
            // 
            this.StateBar_TxtPlatform.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.StateBar_TxtPlatform.Name = "StateBar_TxtPlatform";
            this.StateBar_TxtPlatform.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.StateBar_TxtPlatform.Size = new System.Drawing.Size(74, 17);
            this.StateBar_TxtPlatform.Text = "Plattform:";
            this.StateBar_TxtPlatform.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // StateBar_Platform
            // 
            this.StateBar_Platform.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.StateBar_Platform.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.StateBar_Platform.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.StateBar_Platform.Name = "StateBar_Platform";
            this.StateBar_Platform.Size = new System.Drawing.Size(29, 20);
            this.StateBar_Platform.Text = "...";
            this.StateBar_Platform.DropDownOpening += new System.EventHandler(this.OnPlatform_DropDownOpening);
            this.StateBar_Platform.DropDownItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.OnPlatform_Clicked);
            // 
            // StateBar_TxtPair
            // 
            this.StateBar_TxtPair.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.StateBar_TxtPair.Name = "StateBar_TxtPair";
            this.StateBar_TxtPair.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.StateBar_TxtPair.Size = new System.Drawing.Size(65, 17);
            this.StateBar_TxtPair.Text = "P�rchen:";
            this.StateBar_TxtPair.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // StateBar_Pair
            // 
            this.StateBar_Pair.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.StateBar_Pair.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.StateBar_Pair.Name = "StateBar_Pair";
            this.StateBar_Pair.Size = new System.Drawing.Size(29, 20);
            this.StateBar_Pair.Text = "...";
            this.StateBar_Pair.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.StateBar_Pair.DropDownOpening += new System.EventHandler(this.OnPair_DropDownOpening);
            this.StateBar_Pair.DropDownItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.OnPair_Clicked);
            this.StateBar_Pair.MouseDown += new System.Windows.Forms.MouseEventHandler(this.OnPair_MouseDown);
            // 
            // StateBar_Progress
            // 
            this.StateBar_Progress.Name = "StateBar_Progress";
            this.StateBar_Progress.Size = new System.Drawing.Size(100, 16);
            // 
            // StateBar_CurOperation
            // 
            this.StateBar_CurOperation.Margin = new System.Windows.Forms.Padding(5, 3, 0, 2);
            this.StateBar_CurOperation.Name = "StateBar_CurOperation";
            this.StateBar_CurOperation.Size = new System.Drawing.Size(0, 17);
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "");
            this.imageList.Images.SetKeyName(1, "");
            this.imageList.Images.SetKeyName(2, "");
            this.imageList.Images.SetKeyName(3, "");
            this.imageList.Images.SetKeyName(4, "");
            this.imageList.Images.SetKeyName(5, "");
            this.imageList.Images.SetKeyName(6, "");
            this.imageList.Images.SetKeyName(7, "");
            this.imageList.Images.SetKeyName(8, "");
            // 
            // toolBar
            // 
            this.toolBar.ImageList = this.imageList;
            this.toolBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sbPropWnd,
            this.sbWalletWnd,
            this.toolBarButtonLogWindow});
            this.toolBar.Location = new System.Drawing.Point(0, 24);
            this.toolBar.Name = "toolBar";
            this.toolBar.Size = new System.Drawing.Size(579, 25);
            this.toolBar.TabIndex = 6;
            this.toolBar.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.toolBar_ButtonClick);
            // 
            // sbPropWnd
            // 
            this.sbPropWnd.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sbPropWnd_Platform,
            this.sbPropWnd_Strategy,
            this.sbPropWnd_Parameter,
            this.sbPropWnd_BotAutomatic,
            this.sbPropWnd_Statistics,
            this.sbPropWnd_Variables});
            this.sbPropWnd.ImageIndex = 3;
            this.sbPropWnd.Name = "sbPropWnd";
            this.sbPropWnd.Size = new System.Drawing.Size(32, 22);
            // 
            // sbPropWnd_Platform
            // 
            this.sbPropWnd_Platform.Name = "sbPropWnd_Platform";
            this.sbPropWnd_Platform.Size = new System.Drawing.Size(197, 22);
            this.sbPropWnd_Platform.Text = "Plattform";
            this.sbPropWnd_Platform.Click += new System.EventHandler(this.OnMenu_View_PropWnd_Platform);
            // 
            // sbPropWnd_Strategy
            // 
            this.sbPropWnd_Strategy.Name = "sbPropWnd_Strategy";
            this.sbPropWnd_Strategy.Size = new System.Drawing.Size(197, 22);
            this.sbPropWnd_Strategy.Text = "Strategie";
            this.sbPropWnd_Strategy.Click += new System.EventHandler(this.OnMenu_View_PropWnd_Strategy);
            // 
            // sbPropWnd_Parameter
            // 
            this.sbPropWnd_Parameter.Name = "sbPropWnd_Parameter";
            this.sbPropWnd_Parameter.Size = new System.Drawing.Size(197, 22);
            this.sbPropWnd_Parameter.Text = "Parameter";
            this.sbPropWnd_Parameter.Click += new System.EventHandler(this.OnMenu_View_PropWnd_Parameter);
            // 
            // sbPropWnd_BotAutomatic
            // 
            this.sbPropWnd_BotAutomatic.Name = "sbPropWnd_BotAutomatic";
            this.sbPropWnd_BotAutomatic.Size = new System.Drawing.Size(197, 22);
            this.sbPropWnd_BotAutomatic.Text = "Trading-Bot Automatik";
            this.sbPropWnd_BotAutomatic.Click += new System.EventHandler(this.OnMenu_View_PropWnd_Variables);
            // 
            // sbPropWnd_Statistics
            // 
            this.sbPropWnd_Statistics.Name = "sbPropWnd_Statistics";
            this.sbPropWnd_Statistics.Size = new System.Drawing.Size(197, 22);
            this.sbPropWnd_Statistics.Text = "�bersicht";
            // 
            // sbPropWnd_Variables
            // 
            this.sbPropWnd_Variables.Name = "sbPropWnd_Variables";
            this.sbPropWnd_Variables.Size = new System.Drawing.Size(197, 22);
            this.sbPropWnd_Variables.Text = "Variablen";
            // 
            // sbWalletWnd
            // 
            this.sbWalletWnd.Image = ((System.Drawing.Image)(resources.GetObject("sbWalletWnd.Image")));
            this.sbWalletWnd.Name = "sbWalletWnd";
            this.sbWalletWnd.Size = new System.Drawing.Size(23, 22);
            this.sbWalletWnd.ToolTipText = "Wallets";
            // 
            // toolBarButtonLogWindow
            // 
            this.toolBarButtonLogWindow.ImageIndex = 6;
            this.toolBarButtonLogWindow.Name = "toolBarButtonLogWindow";
            this.toolBarButtonLogWindow.Size = new System.Drawing.Size(23, 22);
            this.toolBarButtonLogWindow.ToolTipText = "Log";
            // 
            // dDockPanel
            // 
            this.dDockPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dDockPanel.DockBackColor = System.Drawing.SystemColors.AppWorkspace;
            this.dDockPanel.DockBottomPortion = 150D;
            this.dDockPanel.DockLeftPortion = 200D;
            this.dDockPanel.DockRightPortion = 200D;
            this.dDockPanel.DockTopPortion = 150D;
            this.dDockPanel.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World, ((byte)(0)));
            this.dDockPanel.Location = new System.Drawing.Point(0, 49);
            this.dDockPanel.Name = "dDockPanel";
            this.dDockPanel.RightToLeftLayout = true;
            this.dDockPanel.Size = new System.Drawing.Size(579, 338);
            this.dDockPanel.TabIndex = 0;
            this.dDockPanel.ActiveDocumentChanged += new System.EventHandler(this.OnActiveDocumentChanged);
            // 
            // vsToolStripExtender1
            // 
            this.vsToolStripExtender1.DefaultRenderer = null;
            // 
            // MainForm
            // 
            this.ClientSize = new System.Drawing.Size(579, 409);
            this.Controls.Add(this.dDockPanel);
            this.Controls.Add(this.toolBar);
            this.Controls.Add(this.mainMenu);
            this.Controls.Add(this.ssStateBar);
            this.Enabled = false;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.mainMenu;
            this.Name = "MainForm";
            this.Text = "Crypto Rift Bot";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Closing += new System.ComponentModel.CancelEventHandler(this.OnFormClosing);
            this.Load += new System.EventHandler(this.OnLoad);
            this.mainMenu.ResumeLayout(false);
            this.mainMenu.PerformLayout();
            this.ssStateBar.ResumeLayout(false);
            this.ssStateBar.PerformLayout();
            this.toolBar.ResumeLayout(false);
            this.toolBar.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        private WeifenLuo.WinFormsUI.Docking.DockPanel dDockPanel;
        private System.ComponentModel.BackgroundWorker bwWebReqThread;
        private System.Windows.Forms.ImageList imageList;
        private System.Windows.Forms.ToolStrip toolBar;
        private System.Windows.Forms.ToolStripButton toolBarButtonLogWindow;
        private System.Windows.Forms.MenuStrip mainMenu;
        private System.Windows.Forms.ToolStripMenuItem miFile;
        private System.Windows.Forms.ToolStripSeparator menuItem4;
        private System.Windows.Forms.ToolStripMenuItem miFile_Exit;
        private System.Windows.Forms.ToolStripMenuItem miView;
        private System.Windows.Forms.ToolStripMenuItem miView_PropWnd;
        private System.Windows.Forms.ToolStripMenuItem miView_LogWnd;
        private System.Windows.Forms.ToolStripSeparator menuItem1;
        private System.Windows.Forms.ToolStripMenuItem miView_ToolBar;
        private System.Windows.Forms.ToolStripMenuItem miView_StatusBar;
        private System.Windows.Forms.ToolStripSeparator menuItem2;
        private System.Windows.Forms.ToolStripMenuItem menuItemLockLayout;
        private System.Windows.Forms.ToolStripMenuItem miView_Layouts_Scheme_VS2005;
        private System.Windows.Forms.ToolStripMenuItem miView_Layouts_Scheme_VS2003;
        private System.Windows.Forms.ToolStripSeparator menuItem6;
        private System.Windows.Forms.ToolStripMenuItem miView_Layouts_Style_DockingMDI;
        private System.Windows.Forms.ToolStripMenuItem miView_Layouts_Style_DockingSDI;
        private System.Windows.Forms.ToolStripMenuItem miView_Layouts_Style_DockingWindow;
        private System.Windows.Forms.ToolStripMenuItem miView_Layouts_Style_MDI;
        private System.Windows.Forms.ToolStripSeparator menuItem5;
        private System.Windows.Forms.ToolStripMenuItem menuItemShowDocumentIcon;
        private System.Windows.Forms.ToolStripMenuItem miWnd;
        private System.Windows.Forms.ToolStripMenuItem miWnd_New;
        private System.Windows.Forms.ToolStripMenuItem miAsterisk;
        private System.Windows.Forms.ToolStripMenuItem miAsterisk_Help;
        private System.Windows.Forms.ToolStripSeparator menuItem7;
        private System.Windows.Forms.ToolStripMenuItem miAsterisk_About;
        private System.Windows.Forms.StatusStrip ssStateBar;
        private System.Windows.Forms.ToolStripStatusLabel StateBar_TxtServices;
        private System.Windows.Forms.ToolStripStatusLabel StateBar_State;
        private System.Windows.Forms.ToolStripStatusLabel StateBar_TxtState;
        private System.Windows.Forms.ToolStripStatusLabel StateBar_TxtPlatform;
        private System.Windows.Forms.ToolStripDropDownButton StateBar_Platform;
        private System.Windows.Forms.ToolStripStatusLabel StateBar_TxtPair;
        private System.Windows.Forms.ToolStripDropDownButton StateBar_Pair;
        private System.Windows.Forms.ToolStripProgressBar StateBar_Progress;
        private System.Windows.Forms.ToolStripStatusLabel StateBar_CurOperation;
        private System.Windows.Forms.ToolStripMenuItem miFile_ExitWithoutSavingLayout;
        private System.Windows.Forms.ToolStripMenuItem miView_Layouts_Scheme_VS2012Light;
        private System.Windows.Forms.ToolStripMenuItem miView_Layouts_Scheme_VS2012Blue;
        private System.Windows.Forms.ToolStripMenuItem miView_Layouts_Scheme_VS2012Dark;
        private System.Windows.Forms.ToolStripMenuItem miView_Layouts_Scheme_VS2013Light;
        private System.Windows.Forms.ToolStripMenuItem miView_Layouts_Scheme_VS2013Blue;
        private System.Windows.Forms.ToolStripMenuItem miView_Layouts_Scheme_VS2013Dark;
        private System.Windows.Forms.ToolStripMenuItem miView_Layouts_Scheme_VS2015Light;
        private System.Windows.Forms.ToolStripMenuItem miView_Layouts_Scheme_VS2015Blue;
        private System.Windows.Forms.ToolStripMenuItem miView_Layouts_Scheme_VS2015Dark;
        private WeifenLuo.WinFormsUI.Docking.VS2015LightTheme vS2015LightTheme1;
        private WeifenLuo.WinFormsUI.Docking.VS2015BlueTheme vS2015BlueTheme1;
        private WeifenLuo.WinFormsUI.Docking.VS2015DarkTheme vS2015DarkTheme1;
        private WeifenLuo.WinFormsUI.Docking.VS2013LightTheme vS2013LightTheme1;
        private WeifenLuo.WinFormsUI.Docking.VS2013BlueTheme vS2013BlueTheme1;
        private WeifenLuo.WinFormsUI.Docking.VS2013DarkTheme vS2013DarkTheme1;
        private WeifenLuo.WinFormsUI.Docking.VS2012LightTheme vS2012LightTheme1;
        private WeifenLuo.WinFormsUI.Docking.VS2012BlueTheme vS2012BlueTheme1;
        private WeifenLuo.WinFormsUI.Docking.VS2012DarkTheme vS2012DarkTheme1;
        private WeifenLuo.WinFormsUI.Docking.VS2003Theme vS2003Theme1;
        private WeifenLuo.WinFormsUI.Docking.VS2005Theme vS2005Theme1;
        private WeifenLuo.WinFormsUI.Docking.VisualStudioToolStripExtender vsToolStripExtender1;
        private System.Windows.Forms.ToolStripSplitButton sbPropWnd;
        private System.Windows.Forms.ToolStripMenuItem miView_PropWnd_Platform;
        private System.Windows.Forms.ToolStripMenuItem miView_PropWnd_Strategy;
        private System.Windows.Forms.ToolStripMenuItem miView_PropWnd_Parameter;
        private System.Windows.Forms.ToolStripMenuItem miView_PropWnd_BotAutomatic;
        private System.Windows.Forms.ToolStripMenuItem sbPropWnd_Platform;
        private System.Windows.Forms.ToolStripMenuItem sbPropWnd_Strategy;
        private System.Windows.Forms.ToolStripMenuItem sbPropWnd_Parameter;
        private System.Windows.Forms.ToolStripMenuItem sbPropWnd_BotAutomatic;
        private System.Windows.Forms.ToolStripMenuItem miView_Layouts;
        private System.Windows.Forms.ToolStripMenuItem miView_Layouts_Default;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem miView_Layouts_Scheme;
        private System.Windows.Forms.ToolStripMenuItem miView_Layouts_Style;
        private System.Windows.Forms.ToolStripSeparator miView_Layouts_Scheme_Separator1;
        private System.Windows.Forms.ToolStripSeparator miView_Layouts_Scheme_Separator2;
        private System.Windows.Forms.ToolStripSeparator miView_Layouts_Scheme_Separator3;
        private System.Windows.Forms.ToolStripMenuItem miDebug;
        private System.Windows.Forms.ToolStripMenuItem miDebug_ManOrders;
        private System.Windows.Forms.ToolStripMenuItem miDebug_ManOrders_Buy;
        private System.Windows.Forms.ToolStripMenuItem miDebug_ManOrders_Sell;
        private System.Windows.Forms.ToolStripMenuItem miDebug_Simulation;
        private System.Windows.Forms.ToolStripMenuItem miDebug_Simulation_ResetRift;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem miDebug_StateShot;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem miDebug_ToggleMode;
        private System.Windows.Forms.ToolStripButton sbWalletWnd;
        private System.Windows.Forms.ToolStripMenuItem miView_WalletWnd;
        private System.Windows.Forms.ToolStripMenuItem miFile_Settings;
        private System.Windows.Forms.ToolStripMenuItem miView_PropWnd_Statistics;
        private System.Windows.Forms.ToolStripMenuItem miView_PropWnd_Variables;
        private System.Windows.Forms.ToolStripMenuItem sbPropWnd_Statistics;
        private System.Windows.Forms.ToolStripMenuItem sbPropWnd_Variables;
        private System.Windows.Forms.ToolStripMenuItem miFile_State;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
#if DEBUG
        private System.Windows.Forms.ToolStripMenuItem miDebug_OpenDocument;
#endif
    }
}