using CryptoRiftBot;
using System.Linq;
using System.Drawing;
using System.Windows.Forms;
using System.Collections.Generic;
using Shared.Utils.Core;
using Shared.Utils.Framework;

namespace CryptoRiftBot
{
    public partial class BotLogWindow : BotToolWindow
    {
        #region Types
        public class BotLogEntryStyle
        {
            public BotLogEntryStyle(Font fFont, Color cTextColor)
            {
                this.Font = fFont;
                this.TextColor = cTextColor;
            }


            #region Properties
            public Font Font { private set; get; }
            public Color TextColor { private set; get; }
            #endregion
        }
        #endregion


        public BotLogWindow()
        {
            InitializeComponent();

            // Control initialisieren:
            lvContent.SmallImageList = new ImageList();

            lvContent.SmallImageList.Images.AddStrip(Image.FromStream(UtlResources.Load("CryptoRiftBot.Images.LogEntryTypes.bmp")));
            lvContent.SmallImageList.TransparentColor = Color.FromArgb(0, 255, 0);
        }


        #region Properties.Management
        public static IReadOnlyDictionary<BotLogPriority, BotLogEntryStyle> EntryStyle = new Dictionary<BotLogPriority, BotLogEntryStyle>()
        {
            { BotLogPriority.eLow, new BotLogEntryStyle(new Font(SystemFonts.DefaultFont.FontFamily, SystemFonts.DefaultFont.Size), Color.FromKnownColor(KnownColor.GrayText)) },
            { BotLogPriority.eNormal, new BotLogEntryStyle(new Font(SystemFonts.DefaultFont.FontFamily, SystemFonts.DefaultFont.Size), Color.FromKnownColor(KnownColor.ControlText)) },
            { BotLogPriority.eHigh, new BotLogEntryStyle(new Font(SystemFonts.DefaultFont.FontFamily, SystemFonts.DefaultFont.Size, FontStyle.Bold), Color.FromKnownColor(KnownColor.ControlText)) }
        };
        #endregion


        #region Event.Log
        public void OnWriteLog(BotLogEntry bEntry)
        {
            // Eintrag hinzufügen:
            BotLogEntryStyle eStyle = EntryStyle[bEntry.Priority];
            ListViewItem lItem = new ListViewItem();

            lItem.Font = eStyle.Font;
            lItem.ForeColor = eStyle.TextColor;
            lItem.ImageIndex = (int)bEntry.Type;
            lItem.ToolTipText = bEntry.DebugInfo;
            lItem.Tag = bEntry.Tag;

            lItem.Text = string.Empty;
            lItem.SubItems.Add(string.Format("{0:dd.MM.yyyy}, {0:HH:mm:ss}", bEntry.TimeStamp));
            lItem.SubItems.Add(bEntry.Code);
            lItem.SubItems.Add(bEntry.TypeTitle);
            lItem.SubItems.Add(bEntry.Process);
            lItem.SubItems.Add(bEntry.Description);

            lvContent.Items.Insert(0, lItem);

            // Spaltenbreite anpssen:
            lvContent.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
        }
        public void OnRemoveLog(object oTag)
        {
            for (int i = (lvContent.Items.Count - 1); i >= 0; i--)
            {
                if (lvContent.Items[i].Tag == oTag)
                {
                    lvContent.Items.RemoveAt(i);
                    break;
                }
            }
        }
        #endregion
        #region Events
        private void OnMenuStrip_Clear_Click(object oSender, System.EventArgs eArgs)
        {
            lvContent.Items.Clear();
        }
        #endregion
    }
}