using CryptoRiftBot.Trading.Services;
using CryptoRiftBot.Trading.Strategy;
using CryptoRiftBot.Trading;
using System;
using System.Collections;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using Shared.Utils.Core;
using Shared.Utils.Framework;

namespace CryptoRiftBot
{
    public partial class BotWalletWindow : BotToolWindow
    {
        #region Constants
        // Spalten (In angezeigter Reihenfolge):
        public const int COL_NAME = 1;
        public const int COL_AMOUNT_EXCHANGE = 0;
        public const int COL_SYMBOL = 2;
        #endregion


        public BotWalletWindow()
        {
            InitializeComponent();

            lvContent.ListViewItemSorter = new BotItemComparer();
        }


        #region Management
        public void RefreshContent(BotTradingStrategy bStrategy)
        {
            Type tEnumType = typeof(BotCoinType);
            Array aValArray = Enum.GetValues(tEnumType);
            BotWallet _bWallet;
            ListViewItem lItem;

            iSelColumn = -1;
            lvContent.Items.Clear();

            // INFO:
            // > Die Reihenfolge der Items entpricht nicht der Anzeige-Reihenfolge!
            //   Da nur die erste Spalte (OMG!) editierbar ist, musste dieser Weg gewählt werden :(

            // Wallets hinzufügen:
            foreach (int iVal in aValArray)
            {
                _bWallet = bStrategy.Platform.GetWallet((BotCoinType)tEnumType.GetField(Enum.GetName(tEnumType, iVal)).GetValue(iVal), BotWalletType.eExchange);
                if (_bWallet != null)
                {
                    // Eintrag hinzufügen:
                    lItem = new ListViewItem();

                    UpdateItem(lItem, _bWallet);

                    lvContent.Items.Add(lItem);
                }
            }

            // Eintrag für Gesamt-Guthabenhinzufügen:
            lItem = new ListViewItem();

            lItem.ToolTipText = "Kumuliertes Guthaben (in [$] umgerechnet) aller verfügbaren Währungen.";
            lItem.BackColor = Color.FromKnownColor(KnownColor.GradientInactiveCaption);

            for (int i = 1; i < lvContent.Columns.Count; i++)
                lItem.SubItems.Add("");

            lItem.SubItems[COL_SYMBOL].Text = UtlEnum.GetEnumDescription(BotConvention.TRADE_BASE_CURRENCY);

            lvContent.Items.Add(lItem);

            // Bearbeiten der Werte für Simulation erlauben:
            lvContent.LabelEdit = bStrategy.Platform.IsSimulating;

            // Gesamt-Guthaben aktualisieren:
            UpdateTotalFunds();
        }

        public bool RefreshContent(BotTradingStrategy bStrategy, BotWallet bWallet)
        {
            if (bWallet == null)
            {
                // Alle verfügbaren Informationen anzeigen:
                RefreshContent(bStrategy);

                return (true);
            }
            else
            {
                foreach (ListViewItem _lItem in lvContent.Items)
                {
                    if ((_lItem.Tag != null) && ((int)_lItem.Tag == (int)bWallet.Currency))
                    {
                        // Einzelne Wallet aktualisieren:
                        UpdateItem(_lItem, bWallet);

                        // Gesamt-Guthaben aktualisieren:
                        UpdateTotalFunds();

                        return (true);
                    }
                }

                Debug.Assert(false, String.Format("[RC65] Wallet vom Typ '{0}' unbekannt!", bWallet.Currency.ToString()));
                return (false);
            }
        }
        public void UpdateTotalFunds()
        {
            for (int i = 0; i < lvContent.Columns.Count; i++)
            {
                if (lvContent.Columns[i].Tag != null)
                {
                    ListViewItem lFundsItem = null;
                    BotWallet _bWallet;
                    double _dAmount, dTotalAmount = 0;
                    bool bApproximately = false;

                    foreach (ListViewItem _lItem in lvContent.Items)
                    {
                        if (_lItem.Tag == null)
                        {
                            Debug.Assert(lFundsItem == null, "[UTF141]");

                            // '_lItem' ist das Item für Gesamt-Guthaben:
                            lFundsItem = _lItem;
                        }
                        else
                        {
                            Debug.Assert(_lItem.SubItems[i].Tag != null, "[UTF146]");

                            // '_lItem' ist ein Wallet-Item:
                            _bWallet = (BotWallet)_lItem.SubItems[i].Tag;

                            _dAmount = _bWallet.BaseCurrencyFunds;
                            dTotalAmount += _dAmount;

                            if ((_dAmount > 0) && (_bWallet.Currency != BotConvention.TRADE_BASE_CURRENCY))
                                bApproximately = true;
                        }
                    }

                    if (lFundsItem == null)
                        Debug.Assert(false, "[UTF163] Eintrag für Gesamt-Guthaben konnte nicht ermittelt werden!");
                    else
                        lFundsItem.SubItems[i].Text = ((bApproximately ? "~ " : "") + dTotalAmount.ToString());
                }
            }

            // Spaltenbreite anpssen:
            lvContent.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
        }
        #endregion


        #region Events
        private void OnClick(object oSender, EventArgs eArgs)
        {
            if (oSender == lvContent)
                iSelColumn = lvContent.GetSelColumn();
            else
                iSelColumn = -1;
        }
        private void OnDoubleClick(object oSender, EventArgs eArgs)
        {
            OnClick(oSender, eArgs);
        }

        private void OnBeforeLabelEdit(object oSender, LabelEditEventArgs lArgs)
        {
            lArgs.CancelEdit = ((iSelColumn != COL_AMOUNT_EXCHANGE) || (lvContent.Items[lArgs.Item].SubItems[COL_AMOUNT_EXCHANGE].Tag == null));
        }

        private void OnAfterLabelEdit(object oSender, LabelEditEventArgs lArgs)
        {
            ListViewItem lItem = lvContent.Items[lArgs.Item];

            if ((lItem != null) && (lArgs.Label != null))
            {
                BotWallet wWallet = (BotWallet)lItem.SubItems[COL_AMOUNT_EXCHANGE].Tag;

                if (wWallet == null)
                    Debug.Assert(false, "[OALE180]");
                else
                {
                    double dValue;

                    if (UtlParse.Double(lArgs.Label, out dValue))
                    {
                        wWallet.Funds = dValue;

                        // Ansicht aktualisieren:
                        UpdateItem(lItem, wWallet);
                        UpdateTotalFunds();
                    }

                    // Label-Edit in jedem Fall abbrechen, da sonst Benutzereingabe angewendet werden würde:
                    lArgs.CancelEdit = true;
                }
            }
        }
        #endregion


        #region Helper
        private bool UpdateItem(ListViewItem lItem, BotWallet wWallet)
        {
            int iColCount = lvContent.Columns.Count;

            Debug.Assert((lItem != null), "[UI81]");
            Debug.Assert((wWallet != null), "[UI82]");

            if (lItem.SubItems.Count < (iColCount - 1))
            {
                for (int i = 1; i < iColCount; i++)
                    lItem.SubItems.Add("");

                // Item initialisieren:
                lItem.Tag = (int)wWallet.Currency;

                lItem.SubItems[COL_NAME].Text = BotCurrencyRating.CoinInfo.Find(wWallet.Currency).Name;
                lItem.SubItems[COL_SYMBOL].Text = UtlEnum.GetEnumDescription(wWallet.Currency);
            }

            for (int i = 0; i < iColCount; i++)
            {
                if ((lvContent.Columns[i].Tag != null) && ((int)lvContent.Columns[i].Tag == (int)wWallet.Type))
                {
                    lItem.SubItems[lvContent.Columns[i].Index].Tag = wWallet;
                    lItem.SubItems[lvContent.Columns[i].Index].Text = wWallet.Funds.ToString();
                    return (true);
                }
            }

            Debug.Assert(false, String.Format("[UI110] Wallet vom Typ '{0}' unbekannt!", wWallet.Currency.ToString()));
            return (false);
        }
        #endregion


        private int iSelColumn;
    }
    public class BotItemComparer : IComparer
    {
        public int Compare(object oObj1, object oObj2)
        {
            ListViewItem lItem1 = (ListViewItem)oObj1;
            ListViewItem lItem2 = (ListViewItem)oObj2;

            if (lItem1.Tag == null)
                return (1);
            else if (lItem2.Tag == null)
                return (-1);
            else
                return (String.Compare(lItem1.SubItems[BotWalletWindow.COL_NAME].Text, lItem2.SubItems[BotWalletWindow.COL_NAME].Text));
        }
    }
}