using CryptoRiftBot.Trading;

namespace CryptoRiftBot
{
    partial class BotWalletWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BotWalletWindow));
            this.lvContent = new System.Windows.Forms.ListView();
            this.colCurrency = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colFundsExchange = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colSymbol = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // lvContent
            // 
            this.lvContent.AllowColumnReorder = true;
            this.lvContent.AutoArrange = false;
            this.lvContent.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colFundsExchange,
            this.colCurrency,
            this.colSymbol});
            this.lvContent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvContent.FullRowSelect = true;
            this.lvContent.GridLines = true;
            this.lvContent.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lvContent.HideSelection = false;
            this.lvContent.LabelWrap = false;
            this.lvContent.Location = new System.Drawing.Point(0, 3);
            this.lvContent.MultiSelect = false;
            this.lvContent.Name = "lvContent";
            this.lvContent.Size = new System.Drawing.Size(337, 370);
            this.lvContent.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.lvContent.TabIndex = 0;
            this.lvContent.UseCompatibleStateImageBehavior = false;
            this.lvContent.View = System.Windows.Forms.View.Details;
            this.lvContent.AfterLabelEdit += new System.Windows.Forms.LabelEditEventHandler(this.OnAfterLabelEdit);
            this.lvContent.BeforeLabelEdit += new System.Windows.Forms.LabelEditEventHandler(this.OnBeforeLabelEdit);
            this.lvContent.Click += new System.EventHandler(this.OnClick);
            this.lvContent.DoubleClick += new System.EventHandler(this.OnDoubleClick);
            // 
            // colCurrency
            // 
            this.colCurrency.DisplayIndex = 0;
            this.colCurrency.Text = "Coin";
            // 
            // colFundsExchange
            // 
            this.colFundsExchange.DisplayIndex = 1;
            this.colFundsExchange.Tag = BotWalletType.eExchange;
            this.colFundsExchange.Text = "Guthaben";
            this.colFundsExchange.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // colSymbol
            // 
            this.colSymbol.Text = "Symbol";
            // 
            // WalletWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.ClientSize = new System.Drawing.Size(337, 376);
            this.Controls.Add(this.lvContent);
            this.HideOnClose = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "WalletWindow";
            this.Padding = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.ShowHint = WeifenLuo.WinFormsUI.Docking.DockState.DockRightAutoHide;
            this.TabText = "Wallets";
            this.Text = "Wallets";
            this.ResumeLayout(false);

        }
        #endregion

        private System.Windows.Forms.ListView lvContent;
        private System.Windows.Forms.ColumnHeader colCurrency;
        private System.Windows.Forms.ColumnHeader colFundsExchange;
        private System.Windows.Forms.ColumnHeader colSymbol;
    }
}