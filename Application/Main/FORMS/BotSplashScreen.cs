﻿using System.Windows.Forms;

namespace CryptoRiftBot
{
    public partial class BotSplashScreen : Form
    {
        public BotSplashScreen()
        {
            InitializeComponent();
            AutoScaleMode = AutoScaleMode.Dpi;

            Size = BackgroundImage.PhysicalDimension.ToSize();
        }
    }
}
