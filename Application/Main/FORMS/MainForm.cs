using CryptoRiftBot.Config;
using CryptoRiftBot.Trading;
using CryptoRiftBot.Trading.Services;
using CryptoRiftBot.Trading.Strategy;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Shared.Windows.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Timers;
using System.Windows.Forms;
using Shared.Utils.Core;
using Shared.Utils.Framework;
using WeifenLuo.WinFormsUI.Docking;
using static CryptoRiftBot.Trading.Services.BotServiceManager;
using CryptoRiftBot.Controls;
using CryptoRiftBot.Documents;
using Win32Types;

namespace CryptoRiftBot
{
    public partial class MainForm : Form
    {
        #region Constants
        // Platforms:
        private const int PLT_COUNT = 2;
        public const int PLT_IDX_TEST = 0;
        public const int PLT_IDX_BITFINEX = 1;

        // Registry:
        public const String REG_PATH = "HKEY_CURRENT_USER\\Software\\CRB";

        public const String REG_VAL_DEBUG = "Debug";

        // Thread IDs:
        public const int TID_COUNT = 1;
        public const int TID_WEB_EQUEST = 0;

        // Timer:
        public const int TMR_COUNT = 2;
        public const int TMR_ID_CLOSE_APPLICATION = 0;
        public const int TMR_ID_CURRENCY_RATING = 1;

        // Timer Delay:
        public readonly double TMR_DELAY_CURRENCY_RATING = (BotCurrencyRating.UPDATE_COIN_TICKER_INTERVAL.TotalMilliseconds + 3000);
        #endregion


        #region Types
        #endregion


        public MainForm()
        {
            InitializeComponent();
            SetSplashScreen();

            // Docking Panel initialisieren:
            AutoScaleMode = AutoScaleMode.Dpi;

            CreateStandardControls();

            dDeserializeDockContent = new DeserializeDockContent(GetContentFromPersistString);
            vsToolStripExtender1.DefaultRenderer = tToolStripRenderer;
            sDockCfgFile = Path.Combine(Path.GetDirectoryName(Application.ExecutablePath), "DockPanel.config");
            _sDockCfgFile = Path.Combine(Path.GetDirectoryName(Application.ExecutablePath), "DockPanel.temp.config");

            SetDocumentStyle(DocumentStyle.DockingMdi);

            // Environment initialisieren:
            BotEnv.OnWriteLog += lLogWindow.OnWriteLog;
            BotEnv.OnRemoveLog += lLogWindow.OnRemoveLog;
            BotEnv.OnEnterPassword += OnEnterPassword;
            BotEnv.OnOpenFileDialog += OnOpenFileDialog;
            BotEnv.OnSaveFileDialog += OnSaveFileDialog;
            BotEnv.OnSaveFileDialogEx += OnSaveFileDialogEx;
            BotEnv.OnTaskStateChanged += OnTaskStateChanged;
            BotEnv.OnRefreshView += OnRefreshView;
            BotEnv.OnTickerUpdate += OnTickerUpdate;
            BotEnv.OnServiceStateChanged += OnServiceStateChanged;
            BotEnv.OnBotStateChanged += OnBotStateChanged;
            BotEnv.OnRatingUpdateFinished += OnRatingUpdateFinished;
            BotEnv.OnWalletUpdated += OnWalletUpdated;
            BotEnv.OnOrderFinished += OnOrderFinished;
            BotEnv.OnRiftClosed += OnRiftClosed;
            BotEnv.OnRecalcStatistik += OnRecalcStatistik;

            if (BotEnv.Init(Text, typeof(BotConfigEx)))
            {
                ToolStripItem _tItem;

                bThreads = new BackgroundWorker[TID_COUNT];

                // Initialize status strip (Services):
                int _iIdx = (ssStateBar.Items.IndexOf(StateBar_TxtServices) + 1);
                BotServiceManager.ServiceTypes
                    .Where(_bPair => (_bPair.Value.MaxParallel == 1))
                    .OrderBy(_bPair => (_bPair.Value.ServiceType.Name))
                    .ForEach(_bPair => {
                        // Add button per service type:
                        var _tListBut = new ToolStripDropDownButton()
                        {
                            Tag = _bPair.Value,
                            Text = _bPair.Value.FormatServiceType(BotServiceManager.ShortCutFormatter)
                        };
                        ssStateBar.Items.Insert(_iIdx++, _tListBut);
                        lServiceButtons.Add(_tListBut);

                        // Iterate services:
                        _bPair.Value
                            .OrderBy(_iSvc => _iSvc.Name.ToLower())
                            .ForEach(_iSvc =>
                            {
                                _tItem = _tListBut.DropDownItems.Add(_iSvc.Name);
                                _tItem.Tag = _iSvc;
                                _tItem.Click += OnService_Clicked;
                            });
                    });

                // Initialize status strip (Platforms):
                foreach (BotTradeBot _bBot in BotTradeBot.TradeBots)
                {
                    // Rifts zurücksetzen:
                    _bBot.Strategies.ForEach(_bStrategy => { _bStrategy.ResetRift(); });

                    // Status-Bar:
                    _tItem = StateBar_Platform.DropDownItems.Add(_bBot.ReferredPlatform.PlatformName);
                    _tItem.Tag = _bBot;
                }

                // Initialize webserver:
                bWebServer = new BotWebServer(GetChartDocument);

                // Initialize debug-mode:
                bool bSetDebugMode = BotDebugMode.Active;
#if DEBUG
                bSetDebugMode = true;
#endif
                SetDebugMode(bSetDebugMode);

                // Initialize threads, timers:
                bThreads[TID_WEB_EQUEST] = bwWebReqThread;
                tTimer[TMR_ID_CLOSE_APPLICATION] = UtlTimer.CreateTimer((objSender, eObjArgs) => OnTimer_CloseApplication(objSender, eObjArgs));
                tTimer[TMR_ID_CURRENCY_RATING] = UtlTimer.CreateTimer((objSender, eObjArgs) => OnTimer_CurrencyRating(objSender, eObjArgs), TMR_DELAY_CURRENCY_RATING, true);
            }
            else
            {
                MessageBox.Show("Initialisierung fehlgeschlagen!\r\nSiehe Logfile für Details.", BotConvention.APP_TITLE, MessageBoxButtons.OK, MessageBoxIcon.Error);
                Close();
            }
        }
        ~MainForm()
        {
            BotEnv.Exit();
        }


        #region Properties.Config
        public static BotConfigEx Configuration { get { return ((BotConfigEx)BotEnv.Configuration); } }
        #endregion
        #region Properties.Management
        [Browsable(false)]
        public virtual bool IsSelStrategy { get { return (SelStrategy != null); } }
        [Browsable(false)]
        public virtual bool IsSelStrategyEnabled { get { return ((IsSelStrategy) && (SelStrategy.Enabled)); } }
        #endregion
        #region Properties.Trading
        [Browsable(false)]
        public BotTradingStrategy SelStrategy { private set; get; }
        [Browsable(false)]
        public BotTradeBot SelBot { get { return ((SelStrategy == null) ? null : (SelStrategy.Platform.Owner as BotTradeBot)); } }
        #endregion


        #region Events
        private async void OnLoad(object oSender, EventArgs eArgs)
        {
            BotEnv.LogSystem.WriteLog("MF35", BotLogProcess.eInit, String.Format("Programmstart (Version {0})", Util_Assembly.GetVersion().ToString()), BotLogType.eInfo, BotLogPriority.eLow);

            if (await BotCurrencyRating.UpdateRatingAsync())
            {
                // Dialogfeld aktivieren:
                BotTaskState.Unuse(BotTaskState.BotClass.eInit);
                Enabled = true;

                if (SelBot == null)
                    SelectTradingBot(BotTradeBot.TradeBots.First());

                // Finalize docking panel (Loads documents):
                SetSchema(vS2005Theme1, false);

                this.Visible = true;
            }
            else
                // Anwendung beenden:
                Close();
        }
        private void OnFormClosing(object oSender, System.ComponentModel.CancelEventArgs eArgs)
        {
            if ((BotTradeBot.Active) && (Configuration.General_ConfirmExit))
            {
                if (MessageBox.Show("Beenden der Anwendung bestäigen.", BotConvention.APP_TITLE, MessageBoxButtons.OKCancel, MessageBoxIcon.Information) != DialogResult.OK)
                {
                    // Schließen des Dialogfeldes verhindern:
                    eArgs.Cancel = true;

                    return;
                }
            }

            if (Visible)
            {
                // Layout speichern:
                if (bSaveLayout)
                    dDockPanel.SaveAsXml(sDockCfgFile);

                else if (File.Exists(sDockCfgFile))
                    File.Delete(sDockCfgFile);

                // Fenster verbergen bis Anwendung beendet werden kann:
                Visible = false;

                UtlTimer.ModifyTimer(tTimer[TMR_ID_CLOSE_APPLICATION], 100);

                // Schließen des Dialogfeldes verhindern bis Threads beendet worden sind:
                eArgs.Cancel = true;
            }
            else
            {
                // Schließen des Dialogfeldes bereits beabsichtigt.
                // > Vorgang fortsetzen.
            }
        }
        #endregion
        #region Events.Management
        public BotTradeDocument GetChartDocument(BotTradingStrategy bStrategy)
        {
            return (FindDocument(bStrategy));
        }
        #endregion
        #region Events.Information
        public void OnServiceStateChanged(IBotBaseService iService, BotServiceState bOldState)
        {
            RefreshView(true);
        }
        public void OnBotStateChanged(bool bIsActive)
        {
            if (bIsActive)
            {
                if (Configuration.WebServer_Enable)
                    bWebServer.Start(Configuration.WebServer_Port);
            }
            else
                bWebServer.Stop();

            // Button deaktivieren bis Worker-Thread beendet ist:
            StateBar_State.Enabled = bIsActive;

            RefreshView();

            if (bIsActive)
                // Threads und Timer starten:
                bwWebReqThread.RunWorkerAsync();
            else
            {
                // Threads und Timer beenden:
                for (int i = 0; i < TID_COUNT; i++)
                    bThreads[i].CancelAsync();
            }
        }
        public void OnSelectedBotChanged(BotTradeBot bPrevSelBot)
        {
            StateBar_Pair.DropDownItems.Clear();

            if (SelBot != null)
            {
                // Verfügbare Trading-Strategien anzeigen:
                ToolStripItem _tItem;
                SelBot.ForeachStrategy(_bStrategy => {
                    _tItem = StateBar_Pair.DropDownItems.Add(_bStrategy.TradingPair.Symbol);
                    _tItem.Tag = _bStrategy;
                });
            }
        }
        private void OnSelectedStrategyEnabled()
        {
            // Check if state has changed:
            if (SelStrategy.Enabled != IsDocumentCreated(SelStrategy))
            {
                if (SelStrategy.Enabled)
                    // Neues Dokument erzeugen:
                    InsertDocument(SelStrategy);
                else
                    // Geöffnetes Dokument schließen:
                    CloseDocument(SelStrategy);

                if ((SelStrategy.Enabled == false) && (BotTradeBot.Active == false))
                    // Letzte aktive Strategie wurde deaktiviert.
                    // > Funktion (in MainDlg) beenden:
                    BotTradeBot.Active = false;

                RefreshView();
            }
        }
        public void OnWalletUpdated(BotWallet wWallet = null)
        {
            if (SelStrategy != null)
                lWalletWindow.RefreshContent(SelStrategy, wWallet);
        }
        public void OnOrderFinished(BotTradingStrategy bStrategy, BotBaseOrder bOrder)
        {
            if (SelStrategy != null)
            {
                BotTradeDocument cDoc = FindDocument(bStrategy);

                // Persistente Daten zu atuellem Rift (in aktueller Konfiguration) speichern:
                BotEnv.SaveConfig(BotContentType.eConfig);

                if (cDoc != null)
                    cDoc.OnOrderFinished(bOrder);
            }
        }
        public void OnRiftClosed(BotTradingStrategy bStrategy, BotRiftSummary bSummary)
        {
            int iGroupID = BotExcelDocThread.GetUnusedGroupID();

            // Rift-Zusammenfassung schreiben:
            BotEnv.SaveConfig(BotContentType.eRiftSummary);

            // Wöchentlichen Report generieren:
            new BotExcelDocThread(new BotRiftSummaryExcelDoc(UtlDateTime.RepeatType.SRT_Weekly), bStrategy, iGroupID, bStrategy.OnAfterRiftClosed);

            // Kumulierte Reports generieren:
            foreach (UtlDateTime.RepeatType _rType in Enum.GetValues(typeof(UtlDateTime.RepeatType)))
            {
                if (_rType > UtlDateTime.RepeatType.SRT_Daily)
                    new BotExcelDocThread(new BotRiftSummaryExcelDoc(_rType), null, iGroupID, bStrategy.OnAfterRiftClosed);
            }
        }
        public void OnRecalcStatistik(BotTradingStrategy bStrategy)
        {
            BotTradeDocument cDoc = FindDocument(bStrategy);
            if (cDoc != null)
            {
                if (bStrategy.IsFirstCycle)
                    // Chart zurücksetzen:
                    cDoc.Init();
                cDoc.UpdateContent(true);
            }
        }
        #endregion
        #region Events.Display
        public void OnRefreshView(bool bSaveConfig = true)
        {
            BotTradeDocument cDoc = FindDocument(SelStrategy);
            RefreshView(bSaveConfig);
            if (cDoc != null)
                cDoc.CalcRedraw(true);
        }
        public void OnTickerUpdate(BotTradingStrategy bStrategy, BotTickerSubscription tTicker)
        {
            UpdateChart(bStrategy, tTicker);
        }
        #endregion
        #region Events.User
        private bool OnEnterPassword(string sTitle, string sMessage, out String sResult)
        {
            InputBox iDlg = new InputBox(sTitle, sMessage, string.Empty, true);
            if (iDlg.Show() == DialogResult.OK)
            {
                sResult = iDlg.UserInput;
                return (true);
            }
            else
            {
                sResult = string.Empty;
                return (false);
            }
        }
        private bool OnOpenFileDialog(string sTitle, string sDefaultExt, string sFilter, bool bCheckPathExists, string sInitialPath, out String sFilePath)
        {
            object[] _oResult;
            return (OnFileDialog(FileDialogType.OpenFileDlg, sTitle, sDefaultExt, sFilter, bCheckPathExists, sInitialPath, out sFilePath, out _oResult));
        }
        private bool OnSaveFileDialog(string sTitle, string sDefaultExt, string sFilter, string sInitialPath, out String sFilePath, params object[] oParams)
        {
            object[] _oResult;
            return (OnFileDialog(FileDialogType.SaveFileDlg, sTitle, sDefaultExt, sFilter, false, sInitialPath, out sFilePath, out _oResult, oParams));
        }
        private bool OnSaveFileDialogEx(string sTitle, string sDefaultExt, string sFilter, string sInitialPath, out String sFilePath, out object[] oResult, params object[] oParams)
        {
            return (OnFileDialog(FileDialogType.SaveFileDlg, sTitle, sDefaultExt, sFilter, false, sInitialPath, out sFilePath, out oResult, oParams));
        }
        private bool OnFileDialog(FileDialogType fDlgType, string sTitle, string sDefaultExt, string sFilter, bool bCheckPathExists, string sInitialPath, out String sFilePath, out object[] oResult, params object[] oParams)
        {
            DialogResult _dResult = DialogResult.None;

            switch (sDefaultExt)
            {
                case BotCoinStream.RECORD_FILE_EXTENT:
                    BotCoinStreamDlg bCoinStrDlg;
                    if (fDlgType == FileDialogType.OpenFileDlg)
                    {
                        bCoinStrDlg = new BotCoinStreamDlg();
                        bCoinStrDlg.FileDlgInitialDirectory = sInitialPath;
                    }
                    else
                    {
                        bCoinStrDlg = new BotCoinStreamDlg(oParams);
                        bCoinStrDlg.FileDlgInitialDirectory = Path.GetDirectoryName(sInitialPath);
                        bCoinStrDlg.FileDlgFileName = Path.GetFileName(sInitialPath);
                    }

                    bCoinStrDlg.FileDlgCaption = sTitle;
                    bCoinStrDlg.FileDlgCheckFileExists = bCheckPathExists;
                    bCoinStrDlg.FileDlgDefaultExt = sDefaultExt;
                    bCoinStrDlg.FileDlgFilter = sFilter;

                    _dResult = bCoinStrDlg.ShowDialog();
                    sFilePath = bCoinStrDlg.FileDlgFileName;
                    break;

                default:
                    FileDialog fFileDlg;
                    if (fDlgType == FileDialogType.OpenFileDlg)
                        fFileDlg = new OpenFileDialog();
                    else
                        fFileDlg = new SaveFileDialog();

                    fFileDlg.Title = sTitle;
                    fFileDlg.DefaultExt = sDefaultExt;
                    fFileDlg.Filter = sFilter;
                    fFileDlg.CheckPathExists = bCheckPathExists;
                    fFileDlg.InitialDirectory = sInitialPath;

                    _dResult = fFileDlg.ShowDialog();
                    sFilePath = fFileDlg.FileName;
                    break;
            }

            if (_dResult == DialogResult.OK)
            {
                oResult = oParams;
                return (true);
            }
            else
            {
                oResult = null;
                sFilePath = string.Empty;
                return (false);
            }
        }
        #endregion
        #region Events.TaskState
        private void OnTaskStateChanged()
        {
            int iCount = ((BotTaskState.CurUsage == null) ? 0 : BotTaskState.CurUsage.Count());
            bool bIsBusy = (iCount > 0);

            // Fortschritt aktualisieren:
            StateBar_Progress.Minimum = 0;
            StateBar_Progress.Maximum = BotTaskState.CurTotalCount;
            StateBar_Progress.Value = (bIsBusy) ? (BotTaskState.CurTotalCount - iCount) : 0;
            StateBar_Progress.Visible = bIsBusy;

            // Detailierte Beschreibung aktualisieren:
            BotTaskState.BotClassInfo bInfo;
            String sInfo = "";
            for (int i = 0; i < iCount; i++)
            {
                bInfo = BotTaskState.CurUsage.ElementAt(i).Value;

                if (i > 0)
                    sInfo += ",  ";
                if (bInfo.UseCount > 1)
                    sInfo += String.Format("{0} x {1}", bInfo.UseCount, bInfo.Description);
                else
                    sInfo += bInfo.Description;
            }

            if (iCount > 0)
                sInfo += "...";

            StateBar_CurOperation.Text = sInfo;
            StateBar_CurOperation.Visible = bIsBusy;
        }
        #endregion
        #region Events.DockPanel
        private void OnActiveDocumentChanged(object oSender, EventArgs eArgs)
        {
            if (dDockPanel.ActiveDocument != null)
            {
                switch (dDockPanel.ActiveDocument.DockHandler.Form)
                {
                    case BotCandleSubscriptionDebugDocument _bCandleDoc:
                        SelectTradingStrategy();
                        break;
                    case BotTradeDocument _bDoc:
                        SelectTradingStrategy(_bDoc.ReferredStrategy);
                        break;
                    default:
                        throw new NotImplementedException("[OCDC981] Invalid document type!");
                }
            }
        }
        #endregion
        #region Events.Menu
        private void OnMenu_File_State(object oSender, EventArgs eArgs)
        {
            // Aktuellen Zustand invertieren:
            BotTradeBot.Active = !BotTradeBot.Active;
        }
        private void OnMenu_File_Settings(object oSender, EventArgs eArgs)
        {
            // (BETA) ...
        }
        private void OnMenu_File_Exit(object oSender, EventArgs eArgs)
        {
            Close();
        }
        private void OnMenu_File_ExitWithoutSavingLayout(object oSender, EventArgs e)
        {
            bSaveLayout = false;
            Close();
            bSaveLayout = true;
        }

        private void OnMenu_View_PropWnd_Platform(object oSender, EventArgs eArgs)
        {
            dPropertyWnds[BotPropertyWindow.BotContentType.ePlatform].Show(dDockPanel);
        }
        private void OnMenu_View_PropWnd_Strategy(object oSender, EventArgs eArgs)
        {
            dPropertyWnds[BotPropertyWindow.BotContentType.eStrategy].Show(dDockPanel);
        }
        private void OnMenu_View_PropWnd_Parameter(object oSender, EventArgs eArgs)
        {
            dPropertyWnds[BotPropertyWindow.BotContentType.eParameter].Show(dDockPanel);
        }
        private void OnMenu_View_PropWnd_BotAutomatic(object oSender, EventArgs eArgs)
        {
            dPropertyWnds[BotPropertyWindow.BotContentType.eBotAutomatik].Show(dDockPanel);
        }
        private void OnMenu_View_PropWnd_Variables(object oSender, EventArgs eArgs)
        {
            dPropertyWnds[BotPropertyWindow.BotContentType.eVariables].Show(dDockPanel);
        }
        private void OnMenu_View_PropWnd_Statistics(object oSender, EventArgs eArgs)
        {
            dPropertyWnds[BotPropertyWindow.BotContentType.eStatistics].Show(dDockPanel);
        }
        private void OnMenu_View_WalletWnd(object oSender, EventArgs eArgs)
        {
            lWalletWindow.Show(dDockPanel);
        }
        private void OnMenu_View_LogWnd(object oSender, EventArgs eArgs)
        {
            lLogWindow.Show(dDockPanel);
        }

        private void OnMenu_View_Layouts_Default(object oSender, EventArgs eArgs)
        {
            LoadLayout("CryptoRiftBot.Resources.dockpanel_layout_default.xml");

            // (BETA) ... [ARCHIV]
            /*
            double dPropWndPortion = 300;
            DockPane dRightPane;

            dDockPanel.SuspendLayout(true);

            CloseAllContents();

            // Panes anzeigen:
            dDockPanel.DockLeftPortion = dPropWndPortion;
            dDockPanel.DockRightPortion = (dPropWndPortion * 2);

            // Panes anzeigen (linker Bereich):
            dPropertyWnds[PropertyWnd.ContentType.CT_Strategy].Show(dDockPanel, DockState.DockLeft);
            
            // Panes anzeigen (rechter Bereich):
            dPropertyWnds[PropertyWnd.ContentType.CT_Platform].Show(dDockPanel, DockState.DockRight);
            dRightPane = dPropertyWnds[PropertyWnd.ContentType.CT_Platform].DockHandler.Pane;

            dPropertyWnds[PropertyWnd.ContentType.CT_Parameter].Show(dRightPane, DockAlignment.Left, 0.5);

            lWalletWindow.Show(dRightPane, DockAlignment.Bottom, 0.4);
            dPropertyWnds[PropertyWnd.ContentType.CT_BotAutomatik].Show(dRightPane, DockAlignment.Bottom, 0.6);
            
            // Panes anzeigen (unterer Bereich):
            lLogWindow.Show(dDockPanel, DockState.DockBottom);
            
            // Dokumente anzeigen:
            CreateDocuments();

            dDockPanel.ResumeLayout(true, true);
            */
        }

        private void OnMenu_Debug_ManOrders_Buy(object oSender, EventArgs eArgs)
        {
            if (SelStrategy != null)
                EnableManualOrderButtons(!SelStrategy.TriggerManualOrder(BotOrderDirection.eBuy));
        }
        private void OnMenu_Debug_ManOrders_Sell(object oSender, EventArgs eArgs)
        {
            if (SelStrategy != null)
                EnableManualOrderButtons(!SelStrategy.TriggerManualOrder(BotOrderDirection.eSell));
        }
        private void OnMenu_Simulation_ResetRift(object sender, EventArgs e)
        {
            Debug.Assert((SelStrategy.Platform.IsSimulating), "[OCSRR654]");

            // Rift komplett zurücksetzen.
            IBotSimulatedPlatformService iSimulation = (IBotSimulatedPlatformService)SelStrategy.Platform;

            SelStrategy.ResetRift();
            iSimulation.ResetWallet();

            BotEnv.LogSystem.WriteLog("OCSRR662", BotLogProcess.eStrategy, "Rift zurückgesetzt.", BotLogType.eInfo, BotLogPriority.eNormal);

            // Anzeige aktualisieren:
            dPropertyWnds[BotPropertyWindow.BotContentType.eStatistics].RefreshContent(null, true);
        }
        private void OnMenu_Debug_StateShot(object oSender, EventArgs eArgs)
        {
            BotTradeDocument cDoc = FindDocument(SelStrategy);

            if (cDoc != null)
                cDoc.SaveStateShot();
        }
        private void OnMenu_Debug_ToggleMode(object oSender, EventArgs eArgs)
        {
            SetDebugMode(!BotDebugMode.Active);
        }
#if DEBUG
        private void OnMenu_Debug_OpenDocument(object oSender, EventArgs eArgs)
        {
            BotCandleSubscription _bInput = null;

            // Load candles (From Coin-Stream):
            /*BotCoinStream.IFileHeader _iHeader;
            var _sFiles = Directory.EnumerateFiles(BotEnv.Configuration.CoinStream_Directory.RemoveEnd("\\"));
            if (_sFiles.Count() == 0)
                MessageBox.Show("No records found!", BotConvention.APP_TITLE, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            else if (!BotCoinStream.ReadFilePreview(_sFiles.First(), out _iHeader, out _bInput))
                MessageBox.Show("Failed to load candles!", BotConvention.APP_TITLE, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            else
                ; // Loading suceeded.*/

            // Load candles (From Currency-Update):
            if (BotCurrencyRating.Candles.Count == 0)
                MessageBox.Show("Keine Candle-Subscriptions geladen.", BotConvention.APP_TITLE, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            else
                _bInput = BotCurrencyRating.Candles.First();

            // Load candles (Simulated):
            /*int _iCount = 5;
            _bInput = new BotCandleSubscription(-1, new BotTradingPair(BotCoinType.eBitcoin, BotCoinType.eUSDollar), new TimeSpan(1, 0, 0));
            _bInput.lData.Add(new BotCandleSubscription.BotCandleData()
            {
                tTimeStamp = new TimeSpan(0, 0, 0),
                dOpen = 100,
                dClose = 200,
                dHigh = 250,
                dLow = 50,
                dVolume = 1000,
            });
            for (int i = 1; i < _iCount; i++)
            {
                _bInput.lData.Add(new BotCandleSubscription.BotCandleData()
                {
                    tTimeStamp = new TimeSpan(i, 0, 0),
                    dOpen = _bInput.lData[0].dOpen + (i * 100),
                    dClose = _bInput.lData[0].dClose + (i * 100),
                    dHigh = _bInput.lData[0].dHigh + (i * 100),
                    dLow = _bInput.lData[0].dLow + (i * 100),
                    dVolume = _bInput.lData[0].dVolume,
                });
            }*/

            // Test 1) Merge candles:
            if (_bInput != null)
            {
                var _bOrigin = _bInput.lData.ToArray();
                var _bMerged = new BotCandleSubscription(0, _bInput.ReferredPair, new TimeSpan(_bInput.Interval.Ticks * 2));

                // Initialize with first X candles:
                int _iInitCount = 1;
                _bInput.lData.First(_iInitCount).ForEach(_bCandle => _bMerged.AddData(_bCandle));
                _bInput.lData.RemoveRange(0, _iInitCount);

                // Limit following candles:
                int _iMaxCount = 50;
                if ((_iMaxCount > 0) && (_bInput.lData.Count > _iMaxCount))
                    _bInput.lData.RemoveRange(_iMaxCount, (_bInput.lData.Count - _iMaxCount));

                // Merge with following candles:
                _bMerged.AddData(_bInput);

                // Restore original candles to be displayed in chart control:
                _bInput.lData.Clear();
                if (_iMaxCount > 0)
                    _bInput.lData.AddRange(_bOrigin.First(_iMaxCount));
                else
                    _bInput.lData.AddRange(_bOrigin);

                CreateDocument(new BotCandleSubscriptionDebugDocument(_bInput, _bMerged));

                // Print:
                /*
                Debug.WriteLine("[ORIGIN CANDLES]");
                _bInput.lData.ForEach(_bData => Debug.WriteLine(string.Format("- {0}", _bData)));
                Debug.WriteLine("[MERGED CANDLES]");
                _bMerged.lData.ForEach(_bData => Debug.WriteLine(string.Format("- {0}", _bData)));
                Debug.WriteLine("---");
                */
            }
        }
#endif

        private void OnMenu_Asterisk_Help(object oSender, EventArgs eArgs)
        {
            Process.Start("https://www.gitlab.com/cryptomates/cryptoriftbot/-/wikis/home");
        }
        private void OnMenu_Asterisk_About(object oSender, EventArgs eArgs)
        {
            new BotAboutDialog().ShowDialog(this);
        }

        private void miView_ToolBar_Click(object oSender, EventArgs eArgs)
        {
            toolBar.Visible = miView_ToolBar.Checked = !miView_ToolBar.Checked;
        }

        private void miView_StatusBar_Click(object oSender, EventArgs eArgs)
        {
            ssStateBar.Visible = miView_StatusBar.Checked = !miView_StatusBar.Checked;
        }

        private void toolBar_ButtonClick(object oSender, ToolStripItemClickedEventArgs tArgs)
        {
            if (tArgs.ClickedItem == sbWalletWnd)
                OnMenu_View_WalletWnd(null, null);
            else if (tArgs.ClickedItem == toolBarButtonLogWindow)
                OnMenu_View_LogWnd(null, null);
        }

        private void menuItemNewWindow_Click(object oSender, EventArgs eArgs)
        {
            // neues Fenster erstellen:
            MainForm mWindow = new MainForm();

            mWindow.Show();
        }

        private void menuItemTools_Popup(object oSender, EventArgs eArgs)
        {
            menuItemLockLayout.Checked = !this.dDockPanel.AllowEndUserDocking;
        }

        private void menuItemLockLayout_Click(object oSender, EventArgs eArgs)
        {
            dDockPanel.AllowEndUserDocking = !dDockPanel.AllowEndUserDocking;
        }

        private void menuItemShowDocumentIcon_Click(object oSender, EventArgs eArgs)
        {
            dDockPanel.ShowDocumentIcon = menuItemShowDocumentIcon.Checked = !menuItemShowDocumentIcon.Checked;
        }
        #endregion
        #region Events.Toolbar
        private void OnToolbar_ButtonClicked(object oSender, ToolStripItemClickedEventArgs tArgs)
        {
            if (SelStrategy != null)
            {
                BotPropertyWindow pStrategyWnd = dPropertyWnds[BotPropertyWindow.BotContentType.eStrategy];
                BotCoinStreamPlayer cPlayer = SelStrategy.CoinStreamPlayer;

                if (tArgs.ClickedItem == pStrategyWnd.tsStrategy_CoinStream_Record)
                {
                    // Coin-Stream starten / stoppen:
                    if (SelStrategy.CoinStreamRecorder != null)
                        SelStrategy.StopCoinStreamRecord();
                    else
                        SelStrategy.RecordCoinStream();
                }
                else if (cPlayer != null)
                {
                    if (tArgs.ClickedItem == pStrategyWnd.tsStrategy_CoinStream_PlayerMode)
                    {
                        BotTradeDocument cActiveDoc = GetActiveDocument();

                        // Zustand invertieren:
                        cPlayer.IsPaused = !cPlayer.IsPaused;

                        OnRefreshView(false);

                        if ((cActiveDoc != null) && (cPlayer.IsPaused == false))
                            // Content neuzeichnen:
                            cActiveDoc.CalcRedraw(true);
                    }
                    else if (tArgs.ClickedItem == pStrategyWnd.tsStrategy_CoinStream_Forward)
                    {
                        long lFramePos = cPlayer.CurFramePos;
                        InputBox iInBox = new InputBox(tArgs.ClickedItem.ToolTipText, "Spult das aktuelle Replay an den gewünschten Frame vor.\r\nVorspulen (d.h. Auslassen von Frames) verfälscht das Replay!", lFramePos.ToString());

                        iInBox.CheckUserInputEvent = (sInput) =>
                        {
                            // Sicherstellen dass Position NACH aktuellem Frame gwählt wird:
                            long lValue = long.Parse(sInput);
                            return ((lFramePos < lValue) && (lValue < cPlayer.FrameCount));
                        };

                        if (iInBox.Show() == DialogResult.OK)
                        {
                            long lTargetFrame = long.Parse(iInBox.UserInput);

                            if (cPlayer.Forward(lTargetFrame))
                            {
                                // Aktuelle Grenzwerte zurücksetzen:
                                SelStrategy.ResetCalc();
                                SelStrategy.ResetLimits(true, true);

                                BotEnv.LogSystem.WriteLog("OTBC774", BotLogProcess.eCoinStream, String.Format("Vorspulen zu Frame {0}.", lTargetFrame), BotLogType.eInfo, BotLogPriority.eNormal);
                            }
                            else
                                BotEnv.LogSystem.WriteLog("OTBC776", BotLogProcess.eCoinStream, "Vorspulen fehlgeschlagen.", BotLogType.eError, BotLogPriority.eNormal);
                        }
                    }
                }
            }
        }
        #endregion
        #region Events.StatusBar
        private void OnState_MouseDown(object objSender, MouseEventArgs eArgs)
        {
            if (eArgs.Button == MouseButtons.Right)
                OnMenu_File_State(objSender, eArgs);
        }
        private void OnService_Clicked(object objSender, EventArgs eArgs)
        {
            SelectService(((ToolStripItem) objSender).Tag as IBotBaseService);
        }
        private void OnPlatform_Clicked(object objSender, ToolStripItemClickedEventArgs eArgs)
        {
            SelectTradingBot((BotTradeBot)eArgs.ClickedItem.Tag);
        }
        private void OnPlatform_DropDownOpening(object objSender, EventArgs eArgs)
        {
            // Indikator-Farben anwenden:
            for (int i = 0; i < StateBar_Platform.DropDownItems.Count; i++)
                StateBar_Platform.DropDownItems[i].ForeColor = GetIndicatorColor((BotTradeBot)StateBar_Platform.DropDownItems[i].Tag);
        }
        private void OnPair_Clicked(object objSender, ToolStripItemClickedEventArgs eArgs)
        {
            SelectTradingStrategy((BotTradingStrategy)eArgs.ClickedItem.Tag);
        }
        private void OnPair_MouseDown(object objSender, MouseEventArgs eArgs)
        {
            if (eArgs.Button == MouseButtons.Right)
            {
                // Zustand von aktueller Strategie invertieren:
                SelStrategy.Enabled = (!SelStrategy.Enabled);

                OnSelectedStrategyEnabled();
            }
        }
        private void OnPair_DropDownOpening(object objSender, EventArgs eArgs)
        {
            // Indikator-Farben anwenden:
            foreach (ToolStripDropDownItem _tItem in StateBar_Pair.DropDownItems)
                _tItem.ForeColor = GetIndicatorColor((BotTradingStrategy)_tItem.Tag);
        }
        #endregion


        #region Management
        private void SelectService(IBotBaseService iService)
        {
            Debug.Assert(iService != null);

            BotServiceManager.ServiceTypes[iService.ServiceType].Select(iService.ServiceDescriptor.ServiceDomain);

            RefreshView(true);
        }
        private void SelectTradingBot(BotTradeBot tBot)
        {
            Debug.Assert((tBot != null), "[STB253]");

            // Trading-Strategie auswählen:
            // (BETA) ...
            // hier erstmal erste strategie auswählen (irgendwann die zuletzt angewählte):
            SelectTradingStrategy(tBot.Strategies.First());
        }
        private void SelectTradingStrategy(BotTradingStrategy bStrategy = null)
        {
            if (bStrategy != SelStrategy)
            {
                BotTradeBot bPrevSelBot = SelBot;
                BotBaseParameters bSelParam;
                bool bSelUpdated = (IsSelStrategy);

                // Auswahl übernehmen:
                SelStrategy = bStrategy;

                if (bStrategy != null)
                {
                    // Anzeige aktualisieren:
                    StateBar_Platform.Text = bStrategy.Platform.PlatformName;
                    StateBar_Pair.Text = SelStrategy.TradingPair.Symbol;

                    dPropertyWnds[BotPropertyWindow.BotContentType.eStrategy].SelectObject(bStrategy, true);
                    dPropertyWnds[BotPropertyWindow.BotContentType.ePlatform].SelectObject(bStrategy.Platform);
                    dPropertyWnds[BotPropertyWindow.BotContentType.eBotAutomatik].SelectObject(BotAutomatic.Parameter);
                    dPropertyWnds[BotPropertyWindow.BotContentType.eStatistics].SelectObject(bStrategy.RiftSummaries, true);
                    dPropertyWnds[BotPropertyWindow.BotContentType.eVariables].SelectObject(bStrategy.Calc_Variables);

                    if (bStrategy.ActiveSettingsClone != null)
                        // Original-Objekt aktivier Settings auswählen:
                        bSelParam = bStrategy.ActiveSettingsOriginal;
                    else
                        bSelParam = bStrategy.ReferredParameters;

                    if (bSelParam != null)
                        dPropertyWnds[BotPropertyWindow.BotContentType.eParameter].SelectObject(bSelParam, true);
                    else
                        dPropertyWnds[BotPropertyWindow.BotContentType.eParameter].RefreshContent(null, true);

                    // Wallet-Informationen initialisieren:
                    lWalletWindow.RefreshContent(bStrategy);

                    if ((IsSelStrategyEnabled) && (bSelUpdated))
                    {
                        BotTradeDocument cDoc = FindDocument(bStrategy);

                        // Bereits initialisiertes Chart aktualisieren:
                        cDoc.UpdateContent();

                        ActivateMdiChild(cDoc);
                    }

                    if (bPrevSelBot != SelBot)
                        OnSelectedBotChanged(bPrevSelBot);
                }
                RefreshView(false);
            }
        }
        private void RefreshView(bool bSaveConfig = true)
        {
            bool bIsStrategyReady = ((SelStrategy != null) && (SelStrategy.IsReady));

            // StatusBar aktualisieren:
            StateBar_State.Text = (BotTradeBot.Active ? "Aktiv" : "Inaktiv");
            StateBar_State.Enabled = BotServiceManager.IsReady;
            StateBar_Platform.ForeColor = GetIndicatorColor(SelBot);
            StateBar_Pair.ForeColor = GetIndicatorColor(SelStrategy);

            lServiceButtons.ForEach(_tListBut => {
                var _iList = (IBotTypedServiceList)_tListBut.Tag;
                var _iSelSvc = _iList.SelectedServices;

                _tListBut.AutoToolTip = false;
                _tListBut.ToolTipText = _iList.FormatServiceType(BotServiceManager.NameFormatter);
                _tListBut.ForeColor = GetIndicatorColor((_iSelSvc==null) ? null : _iSelSvc.First());
                if (_iSelSvc.Any())
                    _tListBut.ToolTipText += (":  " + string.Join(", ", _iList.SelectedServices.Select(_iSvc => _iSvc.Name)));

                foreach (ToolStripMenuItem _tSvcBut in _tListBut.DropDownItems)
                {
                    _tSvcBut.Enabled = !BotTradeBot.Active;
                    _tSvcBut.Checked = _iSelSvc.Contains((IBotBaseService)_tSvcBut.Tag);
                }
            });

            // Menü-Items aktualisieren:
            miFile_State.Text = (BotTradeBot.Active ? "Bot deaktivieren" : "Bot aktivieren");
            miDebug_Simulation.Visible = (((SelStrategy != null)) && (SelStrategy.Platform.IsSimulating));
            miDebug_Simulation_ResetRift.Enabled = bIsStrategyReady;

            // PropertyGrids aktualisieren:
            dPropertyWnds[BotPropertyWindow.BotContentType.eParameter].RefreshParameterMenuCommands();

            dPropertyWnds[BotPropertyWindow.BotContentType.eStrategy].RefreshContent(SelStrategy);
            dPropertyWnds[BotPropertyWindow.BotContentType.ePlatform].RefreshContent(SelStrategy);
            dPropertyWnds[BotPropertyWindow.BotContentType.eVariables].RefreshContent(SelStrategy);
            dPropertyWnds[BotPropertyWindow.BotContentType.eStatistics].RefreshContent(null, true);

            // Restliche Constols aktualisieren:
            miDebug_StateShot.Enabled = (SelStrategy != null);
            EnableManualOrderButtons(bIsStrategyReady);

            if (bSaveConfig)
                // Aktuelle Konfiguration speichern:
                BotEnv.SaveConfig(BotContentType.eConfig);
        }
        #endregion


        #region Document
        private BotTradeDocument CreateDocument(BotTradingStrategy bStrategy)
        {
            return ((BotTradeDocument)CreateDocument(new BotTradeDocument(bStrategy)));
        }
        private DockContent CreateDocument(DockContent dDocument)
        {
            Debug.Assert((dDocument != null), "[CD930]");

            dDocument.Show(dDockPanel, DockState.Document);

            return (dDocument);
        }
        private void InsertDocument(BotTradingStrategy bStrategy)
        {
            if (FindDocument(bStrategy, false) == null)
            {
                BotTradeDocument cDocument = CreateDocument(bStrategy);

                if (dDockPanel.DocumentStyle == DocumentStyle.SystemMdi)
                {
                    cDocument.MdiParent = this;
                    cDocument.Show();
                }
                else
                    cDocument.Show(dDockPanel);
            }
            else
                Debug.Assert(false, String.Format("[CD265] Dokument '{0}' bereits geöffnet", bStrategy.TradingPair.Symbol));
        }
        private bool IsDocumentCreated(BotTradingStrategy bStrategy)
        {
            return (FindDocument(bStrategy, false) != null);
        }
        private BotTradeDocument FindDocument(BotTradingStrategy bStrategy, bool bCanAssert = true)
        {
            if (bStrategy != null)
            {
                if (dDockPanel.DocumentStyle == DocumentStyle.SystemMdi)
                {
                    foreach (Form fForm in MdiChildren)
                    {
                        if (fForm.Tag == bStrategy)
                            return (fForm as BotTradeDocument);
                    }
                }
                else
                {
                    foreach (IDockContent iContent in dDockPanel.Documents)
                    {
                        if (iContent.DockHandler.Form.Tag == bStrategy)
                            return (iContent.DockHandler.Form as BotTradeDocument);
                    }
                }

                if (bCanAssert)
                    Debug.Assert(false, "[FD253]");
            }

            return (null);
        }
        private BotTradeDocument GetActiveDocument()
        {
            Form fActiveForm;
            if (dDockPanel.DocumentStyle == DocumentStyle.SystemMdi)
                fActiveForm = ActiveMdiChild;
            else
                fActiveForm = dDockPanel.ActiveDocument.DockHandler.Form;

            if ((fActiveForm != null) && (fActiveForm.GetType().IsSubclassOf(typeof(BotTradeDocument))))
                return (fActiveForm as BotTradeDocument);
            else
                return (null);
        }

        private void CreateDocuments()
        {
            dDockPanel.SuspendLayout(true);
            foreach (BotTradeBot _bBot in BotTradeBot.TradeBots)
            {
                foreach (BotTradingStrategy _bStrategy in _bBot.Strategies)
                {
                    if (_bStrategy.Enabled)
                        CreateDocument(_bStrategy);
                }
            }
            dDockPanel.ResumeLayout(true, true);
        }

        private bool CloseDocument(BotTradingStrategy bStrategy)
        {
            Debug.Assert((bStrategy != null), "[CD261]");

            if (dDockPanel.DocumentStyle == DocumentStyle.SystemMdi)
            {
                foreach (Form fForm in MdiChildren)
                {
                    if (fForm.Tag == bStrategy)
                    {
                        fForm.Close();
                        return (true);
                    }
                }
            }
            else
            {
                foreach (IDockContent iContent in dDockPanel.Documents)
                {
                    if (iContent.DockHandler.Form.Tag == bStrategy)
                    {
                        iContent.DockHandler.Close();
                        return (true);
                    }
                }
            }

            return (false);
        }
        private void CloseAllDocuments()
        {
            if (dDockPanel.DocumentStyle == DocumentStyle.SystemMdi)
            {
                foreach (Form fForm in MdiChildren)
                    fForm.Close();
            }
            else
            {
                foreach (IDockContent document in dDockPanel.DocumentsToArray())
                {
                    // IMPORANT: dispose all panes.
                    document.DockHandler.DockPanel = null;
                    document.DockHandler.Close();
                }
            }
        }
        #endregion
        #region DockPanel
        private void CreateStandardControls()
        {
            // Forms initialisieren:
            lLogWindow = new BotLogWindow();
            lWalletWindow = new BotWalletWindow();

            // Property-Forms initialisieren:
            dPropertyWnds[BotPropertyWindow.BotContentType.ePlatform] = new BotPropertyWindow(BotPropertyWindow.BotContentType.ePlatform, "Plattform", OnPlatform_PropValChanged, OnToolbar_ButtonClicked);
            dPropertyWnds[BotPropertyWindow.BotContentType.eStrategy] = new BotPropertyWindow(BotPropertyWindow.BotContentType.eStrategy, "Strategie", OnStrategy_PropValChanged, OnToolbar_ButtonClicked);
            dPropertyWnds[BotPropertyWindow.BotContentType.eParameter] = new BotPropertyWindow(BotPropertyWindow.BotContentType.eParameter, "Parameter", OnParameters_PropValChanged, OnToolbar_ButtonClicked);
            dPropertyWnds[BotPropertyWindow.BotContentType.eBotAutomatik] = new BotPropertyWindow(BotPropertyWindow.BotContentType.eBotAutomatik, "Trading-Bot Automatik", OnBotAutomatic_PropValChanged, OnToolbar_ButtonClicked);
            dPropertyWnds[BotPropertyWindow.BotContentType.eStatistics] = new BotPropertyWindow(BotPropertyWindow.BotContentType.eStatistics, "Übersicht", OnStatistics_PropValChanged, OnToolbar_ButtonClicked);
            dPropertyWnds[BotPropertyWindow.BotContentType.eVariables] = new BotPropertyWindow(BotPropertyWindow.BotContentType.eVariables, "Variablen", OnVariables_PropValChanged, OnToolbar_ButtonClicked);
        }

        private IDockContent GetContentFromPersistString(string sPersistString)
        {
            if (sPersistString.Length == 0)
            {
                // Ein Dokument wird deserialisiert.
                // > Diese Funktionalität wird in dieser Anwendung aber nicht gebraucht.
            }
            else
            {
                if (sPersistString == typeof(BotLogWindow).ToString())
                    return (lLogWindow);
                else if (sPersistString == typeof(BotWalletWindow).ToString())
                    return (lWalletWindow);
                else if (sPersistString == typeof(BotCandleSubscriptionDebugDocument).ToString())
                    return (null);
                else
                {
                    foreach (var _bWnd in dPropertyWnds)
                    {
                        if (_bWnd.Value.ToString() == sPersistString)
                            return (_bWnd.Value);
                    }
                    Debug.Assert(false, String.Format("[GCPS377] Unbekannter persistent String '{0}'!", sPersistString));
                }
            }

            return (null);
        }

        private void CloseAllContents()
        {
            // we don't want to create another instance of tool window, set DockPanel to null
            lWalletWindow.DockPanel = null;
            lLogWindow.DockPanel = null;

            foreach (KeyValuePair<BotPropertyWindow.BotContentType, BotPropertyWindow> kTemp in dPropertyWnds)
                kTemp.Value.DockPanel = null;

            // Close all other document windows
            CloseAllDocuments();

            // IMPORTANT: dispose all float windows.
            foreach (var window in dDockPanel.FloatWindows.ToList())
                window.Dispose();

            Debug.Assert((dDockPanel.Panes.Count == 0), "CAC332");
            Debug.Assert((dDockPanel.Contents.Count == 0), "CAC333");
            Debug.Assert((dDockPanel.FloatWindows.Count == 0), "CAC334");
        }
        private void LoadLayout(String sResourcePath)
        {
            dDockPanel.SuspendLayout(true);

            // Aktuelle Inhalte schließen:
            CloseAllContents();

            // Resource-Stream öffnen:
            Assembly aAssembly = GetType().Assembly;
            Stream sStream = aAssembly.GetManifestResourceStream(sResourcePath);
            Debug.Assert((sStream != null), "[LL633] Failed to docking panel layout resource!");

            dDockPanel.LoadFromXml(sStream, dDeserializeDockContent);
            sStream.Close();

            // Dokumente anzeigen:
            CreateDocuments();

            dDockPanel.ResumeLayout(true, true);
        }
        private void SetSchema(object oSender, EventArgs eArgs)
        {
            if (oSender == this.miView_Layouts_Scheme_VS2005)
                SetSchema(vS2005Theme1);
            else if (oSender == this.miView_Layouts_Scheme_VS2003)
                SetSchema(vS2003Theme1);
            else if (oSender == this.miView_Layouts_Scheme_VS2012Light)
                SetSchema(vS2012LightTheme1);
            else if (oSender == this.miView_Layouts_Scheme_VS2012Blue)
                SetSchema(vS2012BlueTheme1);
            else if (oSender == this.miView_Layouts_Scheme_VS2012Dark)
                SetSchema(vS2012DarkTheme1);
            else if (oSender == this.miView_Layouts_Scheme_VS2013Blue)
                SetSchema(vS2013BlueTheme1);
            else if (oSender == this.miView_Layouts_Scheme_VS2013Light)
                SetSchema(vS2013LightTheme1);
            else if (oSender == this.miView_Layouts_Scheme_VS2013Dark)
                SetSchema(vS2013DarkTheme1);
            else if (oSender == this.miView_Layouts_Scheme_VS2015Blue)
                SetSchema(vS2015BlueTheme1);
            else if (oSender == this.miView_Layouts_Scheme_VS2015Light)
                SetSchema(vS2015LightTheme1);
            else if (oSender == this.miView_Layouts_Scheme_VS2015Dark)
                SetSchema(vS2015DarkTheme1);
        }

        private bool SetSchema(ThemeBase tTheme, bool bUseTemporary = true)
        {
            String sCfgFile = (bUseTemporary ? _sDockCfgFile : sDockCfgFile);
            bool bFileExists = ((bUseTemporary) || (File.Exists(sCfgFile)));

            if (bUseTemporary)
                // Persist settings when rebuilding UI
                dDockPanel.SaveAsXml(sCfgFile);

            CloseAllContents();

            if (tTheme == vS2005Theme1)
                this.EnableVSRenderer(VisualStudioToolStripExtender.VsVersion.Vs2005, tTheme);
            else if (tTheme == vS2003Theme1)
                this.EnableVSRenderer(VisualStudioToolStripExtender.VsVersion.Vs2003, tTheme);
            else if ((tTheme == vS2012LightTheme1) || (tTheme == vS2012BlueTheme1) || (tTheme == vS2012DarkTheme1))
                this.EnableVSRenderer(VisualStudioToolStripExtender.VsVersion.Vs2012, tTheme);
            else if ((tTheme == vS2013BlueTheme1) || (tTheme == vS2013LightTheme1) || (tTheme == vS2013DarkTheme1))
                this.EnableVSRenderer(VisualStudioToolStripExtender.VsVersion.Vs2013, tTheme);
            else if ((tTheme == vS2015BlueTheme1) || (tTheme == vS2015LightTheme1) || (tTheme == vS2015DarkTheme1))
                this.EnableVSRenderer(VisualStudioToolStripExtender.VsVersion.Vs2015, tTheme);
            else
            {
                Debug.Assert(false, "[SS260]");
                return (false);
            }

            // Theme übernehmen:
            this.dDockPanel.Theme = tTheme;

            miView_Layouts_Scheme_VS2005.Checked = (tTheme == vS2005Theme1);
            miView_Layouts_Scheme_VS2003.Checked = (tTheme == vS2003Theme1);
            miView_Layouts_Scheme_VS2012Light.Checked = (tTheme == vS2012LightTheme1);
            miView_Layouts_Scheme_VS2012Blue.Checked = (tTheme == vS2012BlueTheme1);
            miView_Layouts_Scheme_VS2012Dark.Checked = (tTheme == vS2012DarkTheme1);
            miView_Layouts_Scheme_VS2013Light.Checked = (tTheme == vS2013LightTheme1);
            miView_Layouts_Scheme_VS2013Blue.Checked = (tTheme == vS2013BlueTheme1);
            miView_Layouts_Scheme_VS2013Dark.Checked = (tTheme == vS2013DarkTheme1);
            miView_Layouts_Scheme_VS2015Light.Checked = (tTheme == vS2015LightTheme1);
            miView_Layouts_Scheme_VS2015Blue.Checked = (tTheme == vS2015BlueTheme1);
            miView_Layouts_Scheme_VS2015Dark.Checked = (tTheme == vS2015DarkTheme1);

            if (dDockPanel.Theme.ColorPalette != null)
                ssStateBar.BackColor = dDockPanel.Theme.ColorPalette.MainWindowStatusBarDefault.Background;

            if (bFileExists)
            {
                dDockPanel.LoadFromXml(sCfgFile, dDeserializeDockContent);

                // Dokumente anzeigen:
                CreateDocuments();
            }
            else
                // Standard-Konfiuration anwenden:
                OnMenu_View_Layouts_Default(null, null);

            return (true);
        }

        private void EnableVSRenderer(VisualStudioToolStripExtender.VsVersion version, ThemeBase theme)
        {
            vsToolStripExtender1.SetStyle(mainMenu, version, theme);
            vsToolStripExtender1.SetStyle(toolBar, version, theme);
            vsToolStripExtender1.SetStyle(ssStateBar, version, theme);
        }

        private void SetDocumentStyle(object oSender, EventArgs eArgs)
        {
            if (oSender == miView_Layouts_Style_DockingMDI)
                SetDocumentStyle(DocumentStyle.DockingMdi);
            else if (oSender == miView_Layouts_Style_DockingWindow)
                SetDocumentStyle(DocumentStyle.DockingWindow);
            else if (oSender == miView_Layouts_Style_DockingSDI)
                SetDocumentStyle(DocumentStyle.DockingSdi);
            else
                SetDocumentStyle(DocumentStyle.SystemMdi);
        }
        private void SetDocumentStyle(DocumentStyle dStyle)
        {
            DocumentStyle dOldStyle = dDockPanel.DocumentStyle;

            if (dOldStyle != dStyle)
            {
                if ((dOldStyle == DocumentStyle.SystemMdi) || (dStyle == DocumentStyle.SystemMdi))
                    CloseAllDocuments();

                miView_Layouts_Style_DockingMDI.Checked = (dStyle == DocumentStyle.DockingMdi);
                miView_Layouts_Style_DockingWindow.Checked = (dStyle == DocumentStyle.DockingWindow);
                miView_Layouts_Style_DockingSDI.Checked = (dStyle == DocumentStyle.DockingSdi);
                miView_Layouts_Style_MDI.Checked = (dStyle == DocumentStyle.SystemMdi);

                dDockPanel.DocumentStyle = dStyle;
            }
        }
        #endregion
        #region SplashScreen
        private void SetSplashScreen()
        {
#if DEBUG
            // Skip splash screen.
#else
            bShowSplash = true;
            sSplashScreen = new BotSplashScreen();

            sSplashScreen.Visible = true;
            sSplashScreen.TopMost = true;

            System.Windows.Forms.Timer _timer = new System.Windows.Forms.Timer();
            _timer.Tick += (oSender, e) =>
            {
                sSplashScreen.Visible = false;
                _timer.Enabled = false;
                bShowSplash = false;
            };
            _timer.Interval = 4000;
            _timer.Enabled = true;
#endif
        }
        #endregion
        #region Chart
        private void UpdateChart(BotTradingStrategy bStrategy, BotTickerSubscription tTicker)
        {
            BotTradeDocument cDoc = FindDocument(bStrategy);
            if (cDoc != null)
            {
                if ((cDoc.UpdateContent(tTicker)) && (SelStrategy == bStrategy))
                    // Anzeige aktualisieren:
                    RefreshView(false);
            }
        }
        #endregion
        #region PropertyView
        private void OnStrategy_PropValChanged(object oObject, PropertyValueChangedEventArgs pArgs)
        {
            if (IsSelStrategy)
            {
                switch (pArgs.ChangedItem.Label)
                {
                    case BotConvention.PROP_NAME_ACTIVATED:
                        OnSelectedStrategyEnabled();
                        break;

                    default:
                        RefreshView();
                        break;
                }
            }
        }
        private void OnParameters_PropValChanged(object oObject, PropertyValueChangedEventArgs pArgs)
        {
            // (BETA) ... [PARAMETER ANWENDEN]
            // ggf. müssen manche parameter in die anwendenden variablen geschrieben werden
            // > hier weiter machen wenn SMAs direkt gebildet werden können (nicht erst nach ablauf einer perioden-dauer)
            /*
            // Ggf. geänderte Parameter anwenden:
            BasicTradingStrategy.BasicParam bParams = (BasicTradingStrategy.BasicParam)pgParameters.SelectedObject;

            bSelStrategy.Trend_IndikatorFast.mAverage1.PeriodDuration = bParams.CfgIndikator_Fast.Avg1_Period;
            bSelStrategy.Trend_IndikatorFast.mAverage2.PeriodDuration = bParams.CfgIndikator_Fast.Avg2_Period;
            */

            // Anzeige aktualisieren:
            dPropertyWnds[BotPropertyWindow.BotContentType.eParameter].RefreshContent(null, true);

            RefreshView();
        }
        private void OnPlatform_PropValChanged(object oObject, PropertyValueChangedEventArgs pArgs)
        {
            RefreshView();
        }
        private void OnBotAutomatic_PropValChanged(object oObject, PropertyValueChangedEventArgs pArgs)
        {
            RefreshView();
        }
        private void OnStatistics_PropValChanged(object oObject, PropertyValueChangedEventArgs pArgs)
        {
            Debug.Assert(false, "[DEBUG] Hier Änderungen implementieren falls Stelle erreicht wird.");
        }
        private void OnVariables_PropValChanged(object oObject, PropertyValueChangedEventArgs pArgs)
        {
            RefreshView();
        }
        #endregion



        #region Thread
        private const int THREAD_DELAY = 1000;


        private void OnWebReqThread_DoWork(object oSender, DoWorkEventArgs eArgs)
        {
            BackgroundWorker bThreadObj = (BackgroundWorker)oSender;
            bool bRun = true;

            while ((bRun) && (bThreadObj.CancellationPending == false))
            {
                // Web-Requests behandeln:
                if (BotTradeBot.ExecuteWebRequests())
                    // Anzuwendende Web-Requests via Fortschritt signalisieren:
                    bThreadObj.ReportProgress(1, eArgs.Argument);

                Thread.Sleep(THREAD_DELAY);
            }

            // Ergebnis ist der Modus:
            eArgs.Result = eArgs.Argument;
        }
        private void OnWebReqThread_RunWorkerCompleted(object oSender, RunWorkerCompletedEventArgs eArgs)
        {
            // Button für Status wieder freigeben:
            StateBar_State.Enabled = true;
        }
        private void OnWebReqThread_ProgressChanged(object oSender, ProgressChangedEventArgs pArgs)
        {
            // Web-Requests anwenden:
            BotTradeBot.UseSubscriptionWebRequests();
        }

        private void OnRatingUpdateFinished(bool bResult)
        {
            if ((bResult == false) && (tTimer[TMR_ID_CURRENCY_RATING].Interval == TMR_DELAY_CURRENCY_RATING))
                // Neuern Versuch zum Aktualisieren mit kurzer Verzögerung starten:
                tTimer[TMR_ID_CURRENCY_RATING].Interval = (10 * 1000);
            else
                tTimer[TMR_ID_CURRENCY_RATING].Interval = TMR_DELAY_CURRENCY_RATING;

            if (bResult)
                lWalletWindow.UpdateTotalFunds();
        }
        #endregion
        #region Timer
        private void OnTimer_CloseApplication(object oSender, ElapsedEventArgs eArgs)
        {
            bool bRetry = false;

            tTimer[TMR_ID_CLOSE_APPLICATION].Enabled = false;

            if ((BotTaskState.IsBusy) || (BotExcelDocThread.ActiveCount > 0))
                // Auf Abschluss von Tasks warten:
                bRetry = true;
            else
            {
                if (BotTradeBot.Active)
                {
                    SelStrategy = null;
                    BotTradeBot.Active = false;
                }

                // Warten bis Threads beendet wurden:
                for (int i = 0; i < TID_COUNT; i++)
                {
                    if (bThreads[i].IsBusy)
                    {
                        bRetry = true;
                        break;
                    }
                }
            }

            if (bRetry)
                // Neuen Versuch starten:
                tTimer[TMR_ID_CLOSE_APPLICATION].Enabled = true;
            else
                // Sämtliche Threads wurden beendet.
                // > Dialogfeld schließen:
                Application.Exit();
        }
        private async void OnTimer_CurrencyRating(object oSender, ElapsedEventArgs eArgs)
        {
            // Currency-Rating aktualisieren:
            await BotCurrencyRating.UpdateRatingAsync();
        }
        #endregion


        #region Helper
        private void EnableManualOrderButtons(bool bEnable = true)
        {
            miDebug_ManOrders_Buy.Enabled = bEnable;
            miDebug_ManOrders_Sell.Enabled = bEnable;
        }
        private void SetDebugMode(bool bEnable = true)
        {
            miDebug_ToggleMode.Checked = bEnable;

            if (BotDebugMode.Active != bEnable)
            {
                BotDebugMode.Active = bEnable;

                BotEnv.SaveConfig(BotContentType.eConfig);
            }
        }
        private Color GetIndicatorColor(IBotBaseService iService)
        {
            if (iService == null)
                return (BotConvention.COLOR_ACTIVE);
            else
            {
                switch (iService.State)
                {
                    case BotServiceState.eError: return (BotConvention.COLOR_ERROR);
                    case BotServiceState.eUnknown: return (BotConvention.COLOR_UNKNOWN);
                    case BotServiceState.eOnline: return (BotConvention.COLOR_ONLINE);
                }
                return (BotConvention.COLOR_INACTIVE);
            }
        }
        private Color GetIndicatorColor(BotTradeBot bBot)
        {
            if (bBot == null)
                return (BotConvention.COLOR_ACTIVE);
            else
            {
                switch (bBot.ReferredPlatform.State)
                {
                    case BotPlatformService.BotPlatformState.BTSS_Offline:
                        break;
                    case BotPlatformService.BotPlatformState.BTSS_InitConnection: return (BotConvention.COLOR_EXCLAMATION);
                    case BotPlatformService.BotPlatformState.BTSS_Online: return (BotConvention.COLOR_ACTIVE);
                    case BotPlatformService.BotPlatformState.BTSS_Maintenance: return (BotConvention.COLOR_MAINTENANCE);
                    default:
                        Debug.Assert(false, "[IC1148]");
                        break;
                }
                return (BotConvention.COLOR_INACTIVE);
            }
        }
        private Color GetIndicatorColor(BotTradingStrategy bStrategy)
        {
            if (bStrategy == null)
                return (BotConvention.COLOR_ACTIVE);
            else if (bStrategy.IsAvailable)
                return (bStrategy.Enabled ? BotConvention.COLOR_ACTIVE : BotConvention.COLOR_INACTIVE);
            else
                return (Color.Red);
        }
        #endregion


        // Docking Panel / Dokument:
        private bool bSaveLayout = true;
        private bool bShowSplash = false;

        private List<ToolStripDropDownButton> lServiceButtons = new List<ToolStripDropDownButton>();

        private Dictionary<BotPropertyWindow.BotContentType, BotPropertyWindow> dPropertyWnds = new Dictionary<BotPropertyWindow.BotContentType, BotPropertyWindow>();
        private BotWalletWindow lWalletWindow;
        private BotLogWindow lLogWindow;

        private BotSplashScreen sSplashScreen;

        private DeserializeDockContent dDeserializeDockContent;
        private String sDockCfgFile;
        private String _sDockCfgFile;

        private readonly ToolStripRenderer tToolStripRenderer = new ToolStripProfessionalRenderer();

        private System.Timers.Timer[] tTimer = new System.Timers.Timer[TMR_COUNT];
        private static BackgroundWorker[] bThreads;

        // Webserver:
        private static BotWebServer bWebServer;
    }
    public class BotConfigEx : BotConfig
    {
        #region Constants
        private const String DATAFILE_JSON_GENERAL_CONFIRMEXIT = "General_ConfirmExit";
        private const String DATAFILE_JSON_WEBSERVER_ENABLE = "WebServer_Enable";
        private const String DATAFILE_JSON_WEBSERVER_PORT = "WebServer_Port";
        #endregion


        #region Properties
        [Description("Verlangt eine Bestätigung beim Beenden der Anwendung (Nur notwendig wenn aktiv).")]
        [Browsable(false)]
        public bool General_ConfirmExit { set; get; } = true;
        [Browsable(false)]
        public bool WebServer_Enable { set; get; } = false;
        [Browsable(false)]
        public int WebServer_Port
        {
            get { return (iWebServer_Port); }
            set { iWebServer_Port = Math.Max(1, value); }
        }
        private int iWebServer_Port = 8080;
        #endregion


        #region Configuration
        public override void Write(JsonWriter jWriter)
        {
            jWriter.WritePropertyName(DATAFILE_JSON_GENERAL_CONFIRMEXIT);
            jWriter.WriteValue(General_ConfirmExit);
            jWriter.WritePropertyName(DATAFILE_JSON_WEBSERVER_ENABLE);
            jWriter.WriteValue(WebServer_Enable);
            jWriter.WritePropertyName(DATAFILE_JSON_WEBSERVER_PORT);
            jWriter.WriteValue(iWebServer_Port);

            base.Write(jWriter);
        }
        public override bool Read(JObject dObjects)
        {
            General_ConfirmExit = dObjects.ParseValue(DATAFILE_JSON_GENERAL_CONFIRMEXIT, General_ConfirmExit);
            WebServer_Enable = dObjects.ParseValue(DATAFILE_JSON_WEBSERVER_ENABLE, WebServer_Enable);
            WebServer_Port = dObjects.ParseValue(DATAFILE_JSON_WEBSERVER_PORT, WebServer_Port);

            return (base.Read(dObjects));
        }
        #endregion
    }
}