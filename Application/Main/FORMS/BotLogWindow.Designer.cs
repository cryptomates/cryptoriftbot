namespace CryptoRiftBot
{
    partial class BotLogWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();

            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BotLogWindow));
            this.lvContent = new System.Windows.Forms.ListView();
            this.colTimeStamp = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colCode = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colImage = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colType = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colProcess = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colDescription = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cMenuStrip.SuspendLayout();
            this.tMenuStrip_Clear = new System.Windows.Forms.ToolStripMenuItem();
            this.SuspendLayout();
            // 
            // lvContent
            // 
            this.lvContent.AutoArrange = false;
            this.lvContent.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colImage,
            this.colTimeStamp,
            this.colCode,
            this.colType,
            this.colProcess,
            this.colDescription});
            this.lvContent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvContent.FullRowSelect = true;
            this.lvContent.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lvContent.HideSelection = false;
            this.lvContent.Location = new System.Drawing.Point(0, 3);
            this.lvContent.MultiSelect = false;
            this.lvContent.Name = "lvContent";
            this.lvContent.Size = new System.Drawing.Size(337, 370);
            this.lvContent.TabIndex = 0;
            this.lvContent.UseCompatibleStateImageBehavior = false;
            this.lvContent.View = System.Windows.Forms.View.Details;
            // 
            // colTimeStamp
            // 
            this.colTimeStamp.Text = "Zeit";
            this.colTimeStamp.Width = 30;
            // 
            // colCode
            // 
            this.colCode.Text = "Code";
            this.colCode.Width = 30;
            // 
            // colImage
            // 
            this.colImage.Text = "";
            this.colImage.Width = 20;
            // 
            // colType
            // 
            this.colType.Text = "Typ";
            this.colType.Width = 30;
            // 
            // colProcess
            // 
            this.colProcess.Text = "Vorgang";
            this.colProcess.Width = 30;
            // 
            // colDescription
            // 
            this.colDescription.Text = "Details";
            this.colDescription.Width = 30;
            // 
            // LogWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.ClientSize = new System.Drawing.Size(337, 376);
            this.Controls.Add(this.lvContent);
            this.HideOnClose = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "LogWindow";
            this.Padding = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.ShowHint = WeifenLuo.WinFormsUI.Docking.DockState.DockTopAutoHide;
            this.TabText = "Log";
            this.Text = "Log";
            // 
            // cMenuStrip
            // 
            this.TabPageContextMenuStrip = this.cMenuStrip;
            this.cMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tMenuStrip_Clear});
            this.cMenuStrip.Name = "cMenuStrip";
            this.cMenuStrip.Size = new System.Drawing.Size(113, 70);
            // 
            // tMenuStrip_Clear
            // 
            this.tMenuStrip_Clear.Name = "tMenuStrip_Clear";
            this.tMenuStrip_Clear.Size = new System.Drawing.Size(152, 22);
            this.tMenuStrip_Clear.Text = "Eintr�ge entfernen";
            this.tMenuStrip_Clear.Click += OnMenuStrip_Clear_Click;
            this.cMenuStrip.ResumeLayout(false);

            this.ResumeLayout(false);
        }
        #endregion

        private System.Windows.Forms.ListView lvContent;
        private System.Windows.Forms.ColumnHeader colTimeStamp;
        private System.Windows.Forms.ColumnHeader colCode;
        private System.Windows.Forms.ColumnHeader colType;
        private System.Windows.Forms.ColumnHeader colProcess;
        private System.Windows.Forms.ColumnHeader colDescription;
        private System.Windows.Forms.ColumnHeader colImage;
        private System.Windows.Forms.ContextMenuStrip cMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem tMenuStrip_Clear;
    }
}