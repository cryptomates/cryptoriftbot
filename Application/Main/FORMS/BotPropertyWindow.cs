using CryptoRiftBot;
using CryptoRiftBot.Trading.Strategy;
using Shared.Windows.Forms;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;

namespace CryptoRiftBot
{
    public partial class BotPropertyWindow : BotToolWindow
    {
        #region Enumerations
        public enum BotContentType
        {
            ePlatform,
            eStrategy,
            eParameter,
            eBotAutomatik,
            eStatistics,
            eVariables
        }
        #endregion


        public BotPropertyWindow(BotContentType cContentType, String sWndTitle, PropertyValueChangedEventHandler pPropertyValueChangedEvent, ToolStripItemClickedEventHandler eToolbarItemClickedEvent)
        {
            Control cToolbar = null;

            InitializeComponent(pPropertyValueChangedEvent, eToolbarItemClickedEvent);

            this.OnPropertyValueChanged = pPropertyValueChangedEvent;
            this.ContentType = cContentType;

            // Initialisieren:
            this.Text = sWndTitle;
            this.TabText = sWndTitle;

            // Toolbar anzeigen:
            switch (cContentType)
            {
                case BotContentType.eStrategy:
                    cToolbar = tsToolbarStrategy;
                    break;
                case BotContentType.eParameter:
                    tsParameters_Choice.ComboBox.DataSource = BotParameterManager.Content;

                    RefreshParametersChoice(true);

                    cToolbar = tsToolbarParameters;
                    break;
            }

            tsToolbarStrategy.Visible = (cContentType == BotContentType.eStrategy);
            tsToolbarParameters.Visible = (cContentType == BotContentType.eParameter);

            if (cToolbar == null)
                tlpLayout.RowStyles[0].Height = 0;
            else
                tlpLayout.Controls.Add(cToolbar, 0, 0);
        }


        #region Properties.Management
        public BotContentType ContentType { private set; get; }
        #endregion


        #region Management
        public override string ToString()
        {
            return (GetPersistString());
        }

        protected override string GetPersistString()
        {
            return (String.Format("{0}.{1}", base.GetPersistString(), ContentType.ToString()));
        }
        #endregion
        #region Management
        public void SelectObject(object oObject, bool bSetSplitterWidth = false)
        {
            if (pgContent.SelectedObject != oObject)
            {
                if (oObject != null)
                    pgContent.SelectedObject = null;

                pgContent.SelectedObject = oObject;

                if (bSetSplitterWidth)
                    PropertyGridLabel.SetSplitterWidth(pgContent, 60, true, false);

                pgContent.ExpandAllGridItems();
                pgContent.Focus();

                if (oObject != null)
                {
                    switch (ContentType)
                    {
                        case BotContentType.eParameter:
                            Debug.Assert(oObject.GetType().IsSubclassOf(typeof(BotBaseParameters)), "[SO94]");

                            RefreshParametersChoice((BotBaseParameters)oObject);
                            break;
                    }
                }
            }
        }
        public void RefreshContent(BotTradingStrategy bStrategy = null, bool bExpandContent = false)
        {
            // (BETA) ... [PropertyGrid][Content-Refresh]
            /*
            andauernd scrollt propertyGrind nach oben wenn man einen wert ändert :(
              1) See https://stackoverflow.com/questions/3120496/updating-a-propertygrid
                 "[RefreshProperties(RefreshProperties.All)]"
              2) Hier ist ein Alternativ-Code zu "Soft-Refresh" (Der aber nur das geänderte Item aktualisiert):
                 // var peMain = pgContent.GetType().GetField("peMain", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(pgContent) as System.Windows.Forms.GridItem;
                 // if (peMain != null)
                 // {
                 //     var refreshMethod = peMain.GetType().GetMethod("Refresh");
                 //     if (refreshMethod != null)
                 //     {
                 //         refreshMethod.Invoke(peMain, null);
                 //         pgContent.Invalidate(true);
                 //     }
                 // }
            */

            int iScrollPos = pgContent.VerticalScroll.Value;


            // (BETA) ...
            bExpandContent = true;


            pgContent.Refresh();

            if (bExpandContent)
                pgContent.ExpandAllGridItems();

            pgContent.VerticalScroll.Value = iScrollPos;
            pgContent.Invalidate();

            // Ggf. weitere Inhalte aktualisieren:
            bool bIsStrategyReady = ((bStrategy != null) && (bStrategy.IsReady));
            BotCoinStreamPlayer cPlayer = ((bStrategy == null) ? null : bStrategy.CoinStreamPlayer);

            switch (ContentType)
            {
                case BotContentType.eStrategy:
                    bool bPlayerActive = (cPlayer != null);

                    // Toolbars aktualisieren:
                    tsStrategy_CoinStream.Enabled = bIsStrategyReady;
                    tsStrategy_CoinStream_Record.Checked = ((bStrategy != null) && (bStrategy.CoinStreamRecorder != null));
                    tsStrategy_CoinStream_PlayerMode.Enabled = bPlayerActive;
                    tsStrategy_CoinStream_Forward.Enabled = bPlayerActive;

                    if (cPlayer == null)
                        tsStrategy_CoinStream_PlayerMode.Text = "0";
                    else
                        tsStrategy_CoinStream_PlayerMode.Text = (cPlayer.IsPaused ? "4" : ";");

                    break;

                case BotContentType.eParameter:
                    RefreshParameterMenuCommands();
                    break;

            }
        }
        #endregion
        #region Management.Parameters
        public void RefreshParameterMenuCommands()
        {
            BotBaseParameters bSelParam = null;

            if (BotBaseParameters.IsObjectValid(tsParameters_Choice.SelectedItem))
                bSelParam = (BotBaseParameters)tsParameters_Choice.SelectedItem;

            tsParameters_Remove.Enabled = (bSelParam != null);
            tsParameters_ActivateChanges.Enabled = ((bSelParam != null) && (bSelParam.HasPendingChanges) && (BotTradeBot.Active));
            tsParameters_SetReference.Enabled = (bSelParam != null);
        }


        protected void RefreshParametersChoice(BotBaseParameters bSelParameters)
        {
            RefreshParametersChoice(tsParameters_Choice.Items.IndexOf(bSelParameters), true);
        }
        protected void RefreshParametersChoice(bool bSelectLastItem = false)
        {
            RefreshParametersChoice(((bSelectLastItem) ? (tsParameters_Choice.Items.Count - 1) : -1), bSelectLastItem);
        }
        protected void RefreshParametersChoice(int iSelIndex, bool bCanDeselect = false)
        {
            if ((0 <= iSelIndex) && (iSelIndex < tsParameters_Choice.Items.Count))
                tsParameters_Choice.SelectedItem = tsParameters_Choice.Items[iSelIndex];

            else if (bCanDeselect)
                // Auswahl entfernen (Da Index ungültig):
                tsParameters_Choice.SelectedItem = null;

            else
                return;

            object oSelObject = tsParameters_Choice.SelectedItem;

            if (BotBaseParameters.IsObjectValid(oSelObject) == false)
                oSelObject = null;

            RefreshParameterMenuCommands();
            SelectObject(oSelObject, true);
        }
        #endregion


        #region Events.General
        private void OnContent_PropertyValueChanged(object oObject, PropertyValueChangedEventArgs pArgs)
        {
            switch (ContentType)
            {
                case BotContentType.eParameter:
                    Debug.Assert(pgContent.SelectedObject.GetType().IsSubclassOf(typeof(BotBaseParameters)), "[SO94]");

                    // Änderungen signalisieren:
                    ((BotBaseParameters)pgContent.SelectedObject).HasPendingChanges = true;

                    RefreshParameterMenuCommands();

                    if (pArgs.ChangedItem.Label == BotConvention.PROP_NAME_TITLE)
                        BotParameterManager.Content.FireOnListChanged(new ListChangedEventArgs(ListChangedType.ItemMoved, tsParameters_Choice.SelectedIndex, tsParameters_Choice.SelectedIndex));

                    break;
            }
        }
        #endregion
        #region Events.Parameters
        private void OnParametersChoice_SelectedIndexChanged(Object oSender, EventArgs eArgs)
        {
            RefreshParametersChoice(tsParameters_Choice.SelectedIndex);
        }
        private void OnParametersNewSelector_Click(Object oSender, EventArgs eArgs)
        {
            BotSelectorParameters sSelector = new BotSelectorParameters();

            // Selektor registrieren:
            BotParameterManager.Content.Add(sSelector);

            //RefreshParametersChoice(sSelector);
            SelectObject(sSelector, true);
        }
        private void OnParametersNewSettings_Click(Object oSender, EventArgs eArgs)
        {
            BotStrategySettings sSettings = new BotStrategySettings();

            // Einstellungen registrieren:
            BotParameterManager.Content.Add(sSettings);

            //RefreshParametersChoice(sSettings);
            SelectObject(sSettings, true);
        }
        private void OnParametersRemove_Click(Object oSender, EventArgs eArgs)
        {
            object oSelObject = tsParameters_Choice.SelectedItem;

            if (BotBaseParameters.IsObjectValid(oSelObject))
            {
                if (BotParameterManager.Remove((BotBaseParameters)oSelObject))
                {
                    OnPropertyValueChanged(null, null);
                    RefreshParametersChoice(true);
                }
            }
        }
        private void OnParametersActivate_Click(Object oSender, EventArgs eArgs)
        {
            BotTradeBot.ApplySettingsToActiveParameters(true);
        }
        private void OnParametersSetReference_Click(Object oSender, EventArgs eArgs)
        {
            object oSelObject = tsParameters_Choice.SelectedItem;

            if (BotBaseParameters.IsObjectValid(oSelObject))
                BotTradeBot.ForeachPlatformsStrategy(tStrategy => tStrategy.ReferredParameters = (BotBaseParameters)oSelObject);
        }
        #endregion


        private PropertyValueChangedEventHandler OnPropertyValueChanged;
    }
}