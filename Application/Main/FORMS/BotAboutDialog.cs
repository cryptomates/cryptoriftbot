using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using Shared.Utils.Core;
using Shared.Utils.Framework;
using WeifenLuo.WinFormsUI.Docking;

namespace CryptoRiftBot
{
    public partial class BotAboutDialog : Form
    {
        public BotAboutDialog()
        {
            InitializeComponent();

            AutoScaleMode = AutoScaleMode.Dpi;
        }


        #region Events
        private void OnLoad(object oSender, EventArgs eArgs)
        {
            // Initialize:
            FileVersionInfo _fAsm = Util_Assembly.GetFileInfo();

            pgContent.Font = new Font("Consolas", Math.Max(9, this.Font.Size));

            // Display details:
            AddTitle("Anwendung");
            AddProperty("Name", _fAsm.ProductName);
            AddProperty("Entwickler", _fAsm.CompanyName);
            AddProperty("Version", _fAsm.FileVersion);
            //AddProperty("Version", _fAsm.ProductVersion);
            AddProperty("Copyright", _fAsm.LegalCopyright);
            pgContent.AddText();

            AddTitle("Projekt");
            pgContent.AddText("https://www.gitlab.com/cryptomates/cryptoriftbot", "Mitmachen auf {0} :)", "gitlab.com");
            pgContent.AddText();
            pgContent.AddText();

            AddTitle("Lizenzen");
            AddLicense(_fAsm.ProductName, "LICENSE.txt");
            pgContent.AddText();

            AddTitle("3rd party Lizenzen");
            Add3rdPartyLicense("CustomFileDialog.htm", Assembly.Load("FileDlgExtenders"));
            Add3rdPartyLicense("DockPanel Suite.txt", Assembly.Load("WeifenLuo.WinFormsUI.Docking"));
            Add3rdPartyLicense("MathParser.org-mXparser.txt", Assembly.Load("MathParser.org-mXparser"));
            Add3rdPartyLicense("Newtonsoft.Json.txt", Assembly.Load("Newtonsoft.Json"));
            Add3rdPartyLicense("SimpleHttpServer.txt");
            Add3rdPartyLicense("websocket-sharp.txt", Assembly.Load("websocket-sharp-standard"));
            pgContent.AddText();
        }
        #endregion
        #region Events.Controls
        private void OnLinkClicked(object oSender, LinkLabelLinkClickedEventArgs lArgs)
        {
            Process.Start(lArgs.Link.LinkData.ToString());
        }
        #endregion


        #region Helper
        private void AddTitle(string sTitle)
        {
            pgContent.AddText(string.Format("[{0}]", sTitle));
        }
        private void AddProperty(string sProperty, string sValue)
        {
            pgContent.AddText(string.Format("{0}:  {1}", sProperty, sValue));
        }
        private void AddLicense(string sFileName, string sName)
        {
            pgContent.AddText(sName, string.Format("{0}\\Licenses\\{1}", sLocation, sFileName));
        }
        private void Add3rdPartyLicense(string sFileName)
        {
            Add3rdPartyLicense(sFileName, string.Empty);
        }
        private void Add3rdPartyLicense(string sFileName, string sVersion)
        {
            string _sTitle = Path.GetFileNameWithoutExtension(sFileName);
            pgContent.AddText(string.Format("{0}\\Licenses\\3rd party\\{1}", sLocation, sFileName), "{0}:  {1}", _sTitle, sVersion);
        }
        private void Add3rdPartyLicense(string sFileName, Assembly aAssembly)
        {
            Add3rdPartyLicense(sFileName, aAssembly.GetName().Version.ToString());
        }
        #endregion


        private string sLocation = Util_Assembly.GetLocationFolder();
    }
}