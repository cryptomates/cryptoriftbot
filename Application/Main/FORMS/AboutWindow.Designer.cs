﻿namespace CryptoRiftBot.FORMS
{
    partial class BotAboutWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pgContent = new System.Windows.Forms.PropertyGrid();
            this.SuspendLayout();
            // 
            // pgContent
            // 
            this.pgContent.HelpVisible = false;
            this.pgContent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pgContent.Location = new System.Drawing.Point(371, 98);
            this.pgContent.Name = "pgContent";
            this.pgContent.Size = new System.Drawing.Size(130, 130);
            this.pgContent.TabIndex = 6;
            this.pgContent.ToolbarVisible = false;
            // 
            // AboutWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.pgContent);
            this.Name = "AboutWindow";
            this.Text = "AboutWindow";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PropertyGrid pgContent;
    }
}