﻿using System;

namespace CryptoRiftBot.Trading.Services.SocketTest
{
    /// <summary>
    /// Platform testing.
    /// </summary>
    [BotServiceDescriptor("socket.test.cryptoriftbot", "SocketTest")]
    public class BotSvcSocketTest : BotPlatformService
    {
        public BotSvcSocketTest(BotPlatformServiceOwner pOwner) : base(pOwner, "ws://echo.websocket.org")
        {
            lLogin = new BotBaseLoginInfo();
        }


        #region Events
        protected override void OnSockOpen(object objSender, EventArgs eArgs)
        {
            SocketSend("Hi, there!");
        }
        #endregion


        #region Management
        public override BotBaseLoginInfo GetLoginInfo()
        {
            return (lLogin);
        }
        #endregion
        #region Communication.Commands
        public override bool Login()
        {
            // Login unnötig:
            return (true);
        }
        #endregion


        private BotBaseLoginInfo lLogin;
    }
}
