﻿using System;
using Shared.Net;
using Newtonsoft.Json;
using Shared.Utils.Core;
using Newtonsoft.Json.Linq;
using System.Diagnostics;
using System.Threading.Tasks;

namespace CryptoRiftBot.Trading.Services.CryptoCompare
{
    [BotServiceDescriptor("min-api.cryptocompare.com", "CryptoCompare.com", new string[]{ "https://www.cryptocompare.com/api", "https://min-api.cryptocompare.com" })]
    public class BotSvcCryptoCompare : BotServiceProvider
    {
        public BotSvcCryptoCompare()
        {
            AddService(new BotSvcTicker(this));
        }


        #region Types.Services
        public class BotSvcTicker : BotBaseService, IBotTickerService
        {
            #region Constans
            private const int REQ_HOUR_OHLCV = 100;
            private const int REQ_SYMBOLS_FULL_DATA = 101;
            private const int REQ_TOPLIST_TOTAL_VOLUME = 102;
            #endregion


            #region Types
            private class BotDataOHLCV
            {
                public double dClose;
                public double dHigh;
                public double dOpen;
                public double dLow;
                public double dVolumeFrom;
                public double dVolumeTo;
            }
            #endregion


            public BotSvcTicker(IBotServiceProvider iProvider) : base(iProvider)
            {
            }


            #region Communication
            public async Task<bool> GetTickerAsync(IBotCoinInfoList bList)
            {
                String sToSymbol = UtlEnum.GetEnumDescription(BotConvention.TRADE_BASE_CURRENCY);

                if (!await ReqHTTP_GetToplistTotalVolumeAsync(sToSymbol, bList, BotCurrencyRating.UPDATE_COIN_INFO_ENTRY_LIMIT, BotCurrencyRating.UPDATE_COIN_INFO_INTERVAL))
                    ;
                else if (!await ReqHTTP_GetSymbolsFullDataAsync(sToSymbol, bList))
                    ;
                else
                    return (true);

                return (false);
            }
            #endregion
            #region Communication.HTTP-Requests
            private bool ReqHTTP_GetDayAveragePrice(String sFromSymbol, String sToSymbol, out double dResult, TimeSpan tDelay = default(TimeSpan))
            {
                double _dVal = 0;
                bool _bResult = ExecuteRequestAsync(BotLogProcess.eUpdateTicker, CreateCommand(String.Format("https://min-api.cryptocompare.com/data/dayAvg?fsym={0}&tsym={1}", sFromSymbol, sToSymbol)), (sResponse) =>
                {
                    dynamic dData = JsonConvert.DeserializeObject<dynamic>(sResponse);
                    if ((dData != null) && (UtlParse.Double((double)dData[sToSymbol], out _dVal)))
                        ;
                    else
                        throw new BotInvalidRequestDataException();
                }).Result;

                dResult = _dVal;
                return (_bResult);
            }
            private async Task<bool> ReqHTTP_GetHourOHLCVAsync(String sFromSymbol, String sToSymbol, BotDataOHLCV dResult, TimeSpan tDelay = default(TimeSpan))
            {
                return (await ExecuteRequestAsync(BotLogProcess.eUpdateTicker, REQ_HOUR_OHLCV, tDelay, CreateCommand(String.Format("https://min-api.cryptocompare.com/data/histohour?fsym={0}&tsym={1}&limit=1", sFromSymbol, sToSymbol)), (sResponse) =>
                {
                    dynamic dData = JsonConvert.DeserializeObject<dynamic>(sResponse);
                    if (dData != null)
                        dData = dData["Data"];
                    if (dData != null)
                        dData = dData[dData.Count - 1];
                    if (dData != null)
                    {
                        dResult.dClose = dData["close"];
                        dResult.dHigh = dData["high"];
                        dResult.dOpen = dData["open"];
                        dResult.dLow = dData["low"];
                        dResult.dVolumeFrom = dData["volumefrom"];
                        dResult.dVolumeTo = dData["volumeto"];
                    }
                    else
                        throw new BotInvalidRequestDataException();
                }) != WebRequestEx.Result.Error);
            }
            private async Task<bool> ReqHTTP_GetSymbolsFullDataAsync(String sToSymbol, IBotCoinInfoList bList, TimeSpan tDelay = default(TimeSpan))
            {
                bool _bResult = false;
                if (bList.Count > 0)
                {
                    BotCoinType _bType;
                    String _sSymbols = "";

                    // Symbole auflisten:
                    for (int i = 0; i < bList.Count; i++)
                    {
                        _bType = bList.GetTypeAt(i);
                        if (_bType >= BotConvention.TRADE_FIRST_CRYPTO_CURRENCY)
                        {
                            if (_sSymbols.Length > 0)
                                _sSymbols += ",";
                            _sSymbols += UtlEnum.GetEnumDescription(_bType);
                        }
                    }

                    if (await ExecuteRequestAsync(BotLogProcess.eUpdateTicker, REQ_SYMBOLS_FULL_DATA, tDelay, CreateCommand(String.Format("https://min-api.cryptocompare.com/data/pricemultifull?fsyms={0}&tsyms={1}", _sSymbols, sToSymbol)), (sResponse) =>
                    {
                        dynamic dData = JsonConvert.DeserializeObject<dynamic>(sResponse);
                        if (dData != null)
                            dData = dData["RAW"];
                        if (dData != null)
                        {
                            IBotCoinInfo _bInfo;
                            BotCoinType _bPrimCoin;
                            Type tType = typeof(BotCoinType);
                            dynamic _dSymbol;

                            foreach (dynamic _dData in dData)
                            {
                                if (_dData.First != null)
                                {
                                    _dSymbol = _dData.First[sToSymbol];
                                    if (_dSymbol == null)
                                        break;
                                    else
                                        _bPrimCoin = (BotCoinType)UtlEnum.GetEnumValue(GetSymbolName((String)_dSymbol["FROMSYMBOL"]), tType);

                                    if (_bPrimCoin != BotCoinType.Invalid)
                                    {
                                        _bInfo = bList.Find(_bPrimCoin);
                                        if (_bInfo == null)
                                            Debug.Assert(false, "[GTD67]");
                                        else
                                        {
                                            _bInfo.Update(
                                                null, null,
                                                UtlParse.Double(_dSymbol["PRICE"]),
                                                UtlParse.Double(_dSymbol["MKTCAP"]),
                                                new double[] { double.NaN, UtlParse.Double(_dSymbol["CHANGEPCT24HOUR"]), double.NaN });
                                        }
                                    }
                                    _bResult = true;
                                }
                            }
                        }
                        else
                            throw new BotInvalidRequestDataException();
                    }) == WebRequestEx.Result.Error)
                        _bResult = false;
                }
                
                return (_bResult);
            }
            private async Task<bool> ReqHTTP_GetToplistTotalVolumeAsync(String sToSymbol, IBotCoinInfoList bList, int iEntryLimit = 100, TimeSpan tDelay = default(TimeSpan))
            {
                bool _bResult = false;
                if (bList.Count > 0)
                {
                    if (await ExecuteRequestAsync(BotLogProcess.eUpdateTicker, REQ_TOPLIST_TOTAL_VOLUME, tDelay, CreateCommand(String.Format("https://min-api.cryptocompare.com/data/top/totalvol?limit={1}&tsym={0}", sToSymbol, iEntryLimit)), (sResponse) =>
                    {
                        dynamic dData = JsonConvert.DeserializeObject<dynamic>(sResponse);
                        if (dData != null)
                            dData = dData["Data"];
                        if (dData != null)
                        {
                            IBotCoinInfo _bInfo;
                            BotCoinType _bSecCoin;
                            dynamic _dCoinInfo;

                            foreach (dynamic _dObj in dData)
                            {
                                _dCoinInfo = _dObj["CoinInfo"];
                                if (_dCoinInfo != null)
                                {
                                    _bSecCoin = (BotCoinType)UtlEnum.GetEnumValue(GetSymbolName((String)_dCoinInfo["Name"]), typeof(BotCoinType));
                                    if (_bSecCoin != BotCoinType.Invalid)
                                    {
                                        _bInfo = bList.Find(_bSecCoin);
                                        if (_bInfo == null)
                                            Debug.Assert(false, "[GTD67]");
                                        else
                                            _bInfo.Update((string)_dCoinInfo["FullName"], ("http://www.cryptocompare.com" + _dCoinInfo["Url"]));
                                    }
                                    _bResult = true;
                                }
                            }
                        }
                        else
                            throw new BotInvalidRequestDataException();
                    }) == WebRequestEx.Result.Error)
                        _bResult = false;
                }

                return (_bResult);
            }
            #endregion


            #region Helper.Communication
            private static string CreateCommand(string sCommand, DateTime dDate = default(DateTime))
            {
                if (string.IsNullOrEmpty(sCommand))
                    throw new ArgumentException("Failed to create empty command!");
                else
                {
                    if (dDate != default(DateTime))
                    {
                        // Auf Stunden runden:
                        dDate = UtlDateTime.RoundDown(dDate, new TimeSpan(1, 0, 0));

                        // Zu UTC-Zeit konvertieren:
                        sCommand += String.Format("&toTs={0}", UtlDateTime.DateTimeToUTCTimeStamp(dDate, true).TotalSeconds);
                    }

                    sCommand += String.Format("&extraParams={0}", BotConvention.APP_SHORTCUT);

                    return (sCommand);
                }
            }
            #endregion
            #region Helper
            private static bool IsString(dynamic dData)
            {
                return ((dData != null) && (((JToken)dData).Type != JTokenType.String));
            }
            private static String GetSymbolName(String sSymbol)
            {
                // Ggf. Bezeichnung anpassen:
                switch (sSymbol)
                {
                    case "MIOTA":
                        return ("IOT");
                    case "DASH":
                        return ("DSH");
                }

                return (sSymbol);
            }
            #endregion
        }
        #endregion
    }
}
