﻿using System;
using System.Linq;
using Shared.Utils.Core;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Globalization;

namespace CryptoRiftBot.Trading.Services.EcbForeignExchange
{
    [BotServiceDescriptor("ecb.int/eurofxref", "ECB ForeignExchange", new[] { "http://www.ecb.int/stats/exchange/eurofxref/html/index.en.html#dev" })]
    public class BotSvcEcbForeignExchange : BotServiceProvider
    {
        public BotSvcEcbForeignExchange()
        {
            AddService(new BotSvcExchangeRate(this));
        }


        #region Types.Services
        public class BotSvcExchangeRate : BotBaseService, IBotExchangeRateService
        {
            public BotSvcExchangeRate(IBotServiceProvider iProvider) : base(iProvider)
            {
            }


            #region Communication
            public async Task<bool> GetExchangeRateAsync(IBotExchangeRate bRate)
            {
                return (await ReqHTTP_GetExchangeRateAsync(bRate));
            }
            #endregion
            #region Communication.HTTP-Requests
            private async Task<bool> ReqHTTP_GetExchangeRateAsync(IBotExchangeRate iRate)
            {
                // [INFO]:
                // An dieser Stelle KEIN "eExchangeRate.Reset()" ausführen!
                // > Sobald der Dollar-Umrechnungskurs einmal ermittelt werden konnte ist es unwichtig diesen zu aktualisieren da er kurzfristig als konstant betrachtet werden kann!
                // > Ausserdem ist somit sichergestellt dass der Umrechnungskurs immer zur Verfügung steht.

                return (await ExecuteRequestAsync(BotLogProcess.eUpdateExchangeRate, "http://www.ecb.int/stats/eurofxref/eurofxref-daily.xml", (sResponse) =>
                {
                    XAttribute xRate = XDocument.Parse(sResponse)
                        .Descendants()
                        .First((_xElement) => {
                            XAttribute _xAttrib = _xElement.Attribute("currency");
                            return ((_xAttrib != null) && (_xAttrib.Value == "USD"));
                        })
                        .Attribute("rate");

                    if (xRate != null)
                        iRate.Update(Convert.ToDouble(xRate.Value, CultureInfo.InvariantCulture.NumberFormat));
                    else
                        throw new BotInvalidRequestDataException();
                }));
            }
            #endregion
        }
        #endregion
    }
}
