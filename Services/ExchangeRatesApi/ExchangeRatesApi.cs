﻿using System;
using Newtonsoft.Json;
using System.Threading.Tasks;
using Shared.Utils.Core;

namespace CryptoRiftBot.Trading.Services.ExchangeRatesApi
{
    [BotServiceDescriptor("api.exchangeratesapi.io", "ExchangeRatesApi.io")]
    public class BotSvcExchangeRatesApi : BotServiceProvider
    {
        public BotSvcExchangeRatesApi()
        {
            AddService(new BotSvcExchangeRate(this));
        }


        #region Types.Services
        public class BotSvcExchangeRate : BotBaseService, IBotExchangeRateService
        {
            #region Constants
            // Update:
            private static readonly TimeSpan UPDATE_RATING_INTERVAL = new TimeSpan(0, 30, 0);

            // Tags:
            private const String TAG_TO = "USD";
            #endregion


            public BotSvcExchangeRate(IBotServiceProvider iProvider) : base(iProvider)
            {
            }


            #region Communication
            public async Task<bool> GetExchangeRateAsync(IBotExchangeRate eRate)
            {
                return (await ReqHTTP_GetExchangeRateAsync(eRate));
            }
            #endregion
            #region Communication.HTTP-Requests
            private async Task<bool> ReqHTTP_GetExchangeRateAsync(IBotExchangeRate iRate)
            {
                // [INFO]:
                // An dieser Stelle KEIN "eExchangeRate.Reset()" ausführen!
                // > Sobald der Dollar-Umrechnungskurs einmal ermittelt werden konnte ist es unwichtig diesen zu aktualisieren da er kurzfristig als konstant betrachtet werden kann!
                // > Ausserdem ist somit sichergestellt dass der Umrechnungskurs immer zur Verfügung steht.

                return (await ExecuteRequestAsync(BotLogProcess.eUpdateExchangeRate, String.Format("https://api.exchangeratesapi.io/latest?symbols={0}", TAG_TO), (sResponse) =>
                {
                    dynamic dData = JsonConvert.DeserializeObject<dynamic>(sResponse);
                    if (dData != null)
                        dData = dData["rates"];
                    if (dData != null)
                        dData = dData[TAG_TO];
                    if (dData != null)
                        iRate.Update(UtlParse.Double(dData));
                    else
                        throw new BotInvalidRequestDataException();
                }));
            }
            #endregion
        }
        #endregion
    }
}
