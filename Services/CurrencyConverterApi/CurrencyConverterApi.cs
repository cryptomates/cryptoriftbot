﻿using Newtonsoft.Json;
using System;
using Shared.Utils.Core;
using System.Threading.Tasks;

namespace CryptoRiftBot.Trading.Services.CurrencyConverterApi
{
    [BotServiceDescriptor("currencyconverterapi.com/api", "CurrencyConverterApi.com")]
    public class BotSvcCurrencyConverterApi : BotServiceProvider
    {
        public BotSvcCurrencyConverterApi()
        {
            AddService(new BotSvcExchangeRate(this));
        }


        #region Types.Services
        public class BotSvcExchangeRate : BotBaseService, IBotExchangeRateService
        {
            #region Constants
            // Update:
            private static readonly TimeSpan UPDATE_RATING_INTERVAL = new TimeSpan(0, 30, 0);

            // Tags:
            private const String TAG_PAIR = "USD_EUR";
            #endregion


            public BotSvcExchangeRate(IBotServiceProvider iProvider) : base(iProvider)
            {
            }


            #region Communication
            public async Task<bool> GetExchangeRateAsync(IBotExchangeRate bRate)
            {
                return (await ReqHTTP_GetExchangeRateAsync(bRate));
            }
            #endregion
            #region Communication.HTTP-Requests
            private async Task<bool> ReqHTTP_GetExchangeRateAsync(IBotExchangeRate iRate)
            {
                // [INFO]:
                // An dieser Stelle KEIN "eExchangeRate.Reset()" ausführen!
                // > Sobald der Dollar-Umrechnungskurs einmal ermittelt werden konnte ist es unwichtig diesen zu aktualisieren da er kurzfristig als konstant betrachtet werden kann!
                // > Ausserdem ist somit sichergestellt dass der Umrechnungskurs immer zur Verfügung steht.

                return (await ExecuteRequestAsync(BotLogProcess.eUpdateExchangeRate, String.Format("http://free.currencyconverterapi.com/api/v5/convert?q={0}&compact=y", TAG_PAIR), (sResponse) =>
                {
                    dynamic dData = JsonConvert.DeserializeObject<dynamic>(sResponse);
                    if (dData != null)
                        dData = dData[TAG_PAIR];
                    if (dData != null)
                        dData = dData["val"];
                    if (dData != null)
                        iRate.Update(UtlParse.Double(dData));
                    else
                        throw new BotInvalidRequestDataException();
                }));
            }
            #endregion
        }
        #endregion
    }
}
