﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Shared.Net;
using System.Diagnostics;
using Newtonsoft.Json.Linq;
using Shared.Utils.Core;
using System.Text.RegularExpressions;
using System.Security.Cryptography;
using CryptoRiftBot.Config;

namespace CryptoRiftBot.Trading.Services.Bitfinex
{
    [BotServiceDescriptor("api.bitfinex.com", "Bitfinex.com", new[] { "https://bitfinex.readme.io/v2/docs/ws-general" })]
    public class BotSvcBitfinex : BotServiceProvider
    {
        public BotSvcBitfinex()
        {
            AddService(new BotSvcCandle(this));
            AddService(new BotSvcPlatform(this));
        }


        #region Constants
        // Candle-Sticks.
        // > Intervalle angeforderter Candle-Sticks.
        private const String CNDL_TIMEFRAME_1m = "1m";
        private const String CNDL_TIMEFRAME_5m = "5m";
        private const String CNDL_TIMEFRAME_15m = "15m";
        private const String CNDL_TIMEFRAME_30m = "30m";
        private const String CNDL_TIMEFRAME_1h = "1h";
        private const String CNDL_TIMEFRAME_3h = "3h";
        private const String CNDL_TIMEFRAME_6h = "6h";
        private const String CNDL_TIMEFRAME_12h = "12h";
        private const String CNDL_TIMEFRAME_1D = "1D";
        private const String CNDL_TIMEFRAME_14D = "14D";
        private const String CNDL_TIMEFRAME_1M = "1M";

        // > Sections:
        private const String CNDL_SECTION_LAST = "last";
        private const String CNDL_SECTION_HIST = "hist";

        // > Limits:
        private const int CNDL_LIMIT_PER_REQUEST = 1000;                                 // Maximale Anzahl an Datensätzen pro Request
        #endregion


        #region Types.Services
        public class BotSvcCandle : BotBaseService, IBotCandleService
        {
            public BotSvcCandle(IBotServiceProvider iProvider) : base(iProvider)
            {
            }


            #region Communication
            public async Task<bool> GetCandlesAsync(BotCandleSubscription cResult, int iLimit = 100)
            {
                return (await ReqHTTP_GetCandlesAsync(cResult, iLimit));
            }
            #endregion
            #region Communication.HTTP-Requests
            private async Task<bool> ReqHTTP_GetCandlesAsync(BotCandleSubscription cResult, int iLimit = 100, String sSection = CNDL_SECTION_HIST)
            {
                iLimit = Math.Min(CNDL_LIMIT_PER_REQUEST, iLimit);
                return (await ExecuteRequestAsync(BotLogProcess.eUpdateCandles, String.Format("https://api.bitfinex.com/v2/candles/trade:{0}:t{1}/{2}?limit={3}", ToTimeFrame(cResult.Interval), cResult.ReferredPair.Symbol, sSection, iLimit), (sResponse) =>
                {
                    dynamic dData = JsonConvert.DeserializeObject<dynamic>(sResponse);
                    if (dData != null)
                        ParseSubscription(dData[1], cResult);
                    else
                        throw new BotInvalidRequestDataException();
                }));
            }
            #endregion
            #region Helper.Communication
            private void ParseSubscription(dynamic dData, BotCandleSubscription bSubscription)
            {
                BotCandleSubscription.BotCandleData _cCandle;
                int iReceived = 0;
                bool bLimitMax = (bSubscription.lData.Count == 0);

                // Candle-Datensätze wurden empfangen:
                if (((JToken)dData[0]).Type == JTokenType.Array)
                    // Es wurden geschachtelte Datensätze ([[...],[...]]) empfangen:
                    dData = dData[0];

                while (dData != null)
                {
                    _cCandle = new BotCandleSubscription.BotCandleData();

                    // Zeitstempel durch 'Zeitzone' und ggf. Sommerzeit erweitern:
                    // --------------------------------------------------------------------------------------------------------------------------------
                    // (BETA) ... UTC-TimeStamp von Bitfinex komisch
                    // Der Zeitstempel von Bitfinex ist 2 Stunden zu früh... keine Ahnung warum.
                    // > Ich addiere hier jetzt hardcoded einfach 2 Stunden hinzu um das Problem zu umgehen.
                    double dTemp;
                    UtlParse.Double(dData[0], out dTemp);
                    _cCandle.tTimeStamp = System.TimeSpan.FromMilliseconds(dTemp + (2 * (1000 * 60 * 60)));
                    // [TEST-CODE]
                    // Console.WriteLine(String.Format("[{0}] STAMP={1}, RAW={2}, DOUBLE={3}", iReceived, cData.tTimeStamp, dData[0], UtlParse.Double(dData[0])));
                    // [ORIGINAL CODE]:
                    // cData.tTimeStamp = MyDateTime.SetDaylightChanges(System.TimeSpan.FromMilliseconds(UtlParse.Double(dData[0])) + TimeZoneInfo.Local.BaseUtcOffset);
                    // --------------------------------------------------------------------------------------------------------------------------------

                    UtlParse.Double(dData[1], out _cCandle.dOpen);
                    UtlParse.Double(dData[2], out _cCandle.dClose);
                    UtlParse.Double(dData[3], out _cCandle.dHigh);
                    UtlParse.Double(dData[4], out _cCandle.dLow);
                    UtlParse.Double(dData[5], out _cCandle.dVolume);

                    bSubscription.AddData(_cCandle);

                    dData = dData.Next;
                    iReceived++;
                }

                if (bLimitMax)
                    bSubscription.MaxDataCount = iReceived;

                bSubscription.UpdatedBy = this;
            }
            #endregion
        }
        public class BotSvcPlatform : BotPlatformService
        {
            #region Constants
            // Allgemein:
            private const int GEN_VERSION = 2;

            // Info-Codes:
            private const int IC_RESTART_SERVER = 20051;
            private const int IC_MAINTENANCE_START = 20060;
            private const int IC_MAINTENANCE_END = 20061;

            // Message-Codes:
            private const String MC_OK = "OK";
            private const String MC_ERROR = "FAILED";

            // Channel-Names:
            private const String CN_BALANCE_UPDATE = "bu";
            private const String CN_POSITION_SNAPSHOT = "ps";
            private const String CN_NEW_POSITION = "pn";
            private const String CN_POSITION_UPDATE = "pu";
            private const String CN_POSITION_CLOSE = "pc";
            private const String CN_WALLET_SNAPSHOT = "ws";
            private const String CN_WALLET_UPDATE = "wu";
            private const String CN_ORDER_SNAPSHOT = "os";
            private const String CN_NEW_ORDER = "on";
            private const String CN_ORDER_UPDATE = "ou";
            private const String CN_ORDER_CANCEL = "oc";
            private const String CN_ORDER_CANCEL_REQ = "oc-req";
            private const String CN_TRADE_EXECUTED = "te";
            private const String CN_TRADE_EXECUTION_UPDATE = "tu";
            private const String CN_FUNDING_TRADE_EXECUTION = "fte";
            private const String CN_FUNDING_TRADE_UPDATE = "ftu";
            private const String CN_HISTORICAL_ORDER_SNAPSHOT = "hos";
            private const String CN_MARGIN_INFORMATION_SNAPSHOT = "mis";
            private const String CN_MARGIN_INFORMATION_UPDATE = "miu";
            private const String CN_NOTIFICATION = "n";
            private const String CN_FUNDING_OFFER_SNAPSHOT = "fos";
            private const String CN_FUNDING_OFFER_NEW = "fon";
            private const String CN_FUNDING_OFFER_UPDATE = "fou";
            private const String CN_FUNDING_OFFER_CANCEL = "foc";
            private const String CN_HISTORICAL_FUNDING_OFFER_SNAPSHOT = "hfos";
            private const String CN_FUNDING_CREDITS_SNAPSHOT = "fcs";
            private const String CN_FUNDING_CREDITS_NEW = "fcn";
            private const String CN_FUNDING_CREDITS_UPDATE = "fcu";
            private const String CN_FUNDING_CREDITS_CLOSE = "fcc";
            private const String CN_HISTORICAL_FUNDING_CREDITS_SNAPSHOT = "hfcs";
            private const String CN_FUNDING_LOAN_SNAPSHOT = "fls";
            private const String CN_FUNDING_LOAN_NEW = "fln";
            private const String CN_FUNDING_LOAN_UPDATE = "flu";
            private const String CN_FUNDING_LOAN_CLOSE = "flc";
            private const String CN_HISTORICAL_FUNDING_LOAN_SNAPSHOT = "hfls";
            private const String CN_HISTORICAL_FUNDING_TRADE_SNAPSHOT = "hfts";
            private const String CN_USER_CUSTOM_PRICE_ALERT = "uac";

            // Order Group-IDs:
            private const int OGID_INDICATING_TRADEBOT = 999;                                // ID, die eine via Trade-Bot generierte Order andeuten soll (willkürlich festgelegt)
            #endregion


            #region Types
            // Enthält Informationen zur Authentifikation.
            private class BotLoginInfo : BotBaseLoginInfo
            {
                public int iAuthChanID;
                public ulong ulAuthUserID;


                public BotLoginInfo()
                {
                    Reset();
                }


                #region Management
                public override void Reset()
                {
                    iAuthChanID = 0;
                    ulAuthUserID = 0;

                    base.Reset();
                }
                #endregion
            }
            private class BotOrder : BotBaseOrder
            {
                public ulong ulOrderID = 0;
                public ulong ulGroupID = 0;                 // Optional (Wird auf Bitfinex in einer Gruppe mit diesem Wert aufgeführt)


                public new bool IsValid()
                {
                    if (base.IsValid())
                    {
                        if (oState >= BotOrderState.eActive)
                            return (ulOrderID != 0);

                        else if (oState == BotOrderState.ExInitialized)
                            return (true);
                    }

                    return (false);
                }
            }
            #endregion


            public BotSvcPlatform(IBotServiceProvider iProvider) : base(iProvider, "wss://api.bitfinex.com/ws/2")
            {
                // Pairs initialisieren:
                lPairs.Add(new BotTradingPair(BotCoinType.eBitcoin, BotCoinType.eUSDollar));
                lPairs.Add(new BotTradingPair(BotCoinType.eEthereum, BotCoinType.eUSDollar));
                lPairs.Add(new BotTradingPair(BotCoinType.eEthereum, BotCoinType.eBitcoin));
                lPairs.Add(new BotTradingPair(BotCoinType.eBitcoinCash, BotCoinType.eUSDollar));
                lPairs.Add(new BotTradingPair(BotCoinType.eBitcoinCash, BotCoinType.eBitcoin));
                lPairs.Add(new BotTradingPair(BotCoinType.eBitcoinCash, BotCoinType.eEthereum));
                lPairs.Add(new BotTradingPair(BotCoinType.eMonero, BotCoinType.eUSDollar));
                lPairs.Add(new BotTradingPair(BotCoinType.eMonero, BotCoinType.eBitcoin));
                lPairs.Add(new BotTradingPair(BotCoinType.eIOTA, BotCoinType.eUSDollar));
                lPairs.Add(new BotTradingPair(BotCoinType.eIOTA, BotCoinType.eBitcoin));
                lPairs.Add(new BotTradingPair(BotCoinType.eIOTA, BotCoinType.eEthereum));
                lPairs.Add(new BotTradingPair(BotCoinType.eLitecoin, BotCoinType.eUSDollar));
                lPairs.Add(new BotTradingPair(BotCoinType.eLitecoin, BotCoinType.eBitcoin));
                lPairs.Add(new BotTradingPair(BotCoinType.eRipple, BotCoinType.eUSDollar));
                lPairs.Add(new BotTradingPair(BotCoinType.eRipple, BotCoinType.eBitcoin));
                lPairs.Add(new BotTradingPair(BotCoinType.eDash, BotCoinType.eUSDollar));
                lPairs.Add(new BotTradingPair(BotCoinType.eDash, BotCoinType.eBitcoin));
                lPairs.Add(new BotTradingPair(BotCoinType.eNeo, BotCoinType.eUSDollar));
                lPairs.Add(new BotTradingPair(BotCoinType.eNeo, BotCoinType.eBitcoin));
                lPairs.Add(new BotTradingPair(BotCoinType.eNeo, BotCoinType.eEthereum));
                lPairs.Add(new BotTradingPair(BotCoinType.eEthereumClassic, BotCoinType.eUSDollar));
                lPairs.Add(new BotTradingPair(BotCoinType.eEthereumClassic, BotCoinType.eBitcoin));

                // Wallets initialisieren:
                Array aValArray = System.Enum.GetValues(typeof(BotWalletType));
                BotWalletType _bType;
                foreach (int iVal in aValArray)
                {
                    _bType = (BotWalletType)iVal;
                    if (_bType > BotWalletType.Invalid)
                    {
                        Wallets.Add(new BotWallet(BotCoinType.eUSDollar, _bType));
                        Wallets.Add(new BotWallet(BotCoinType.eBitcoin, _bType));
                        Wallets.Add(new BotWallet(BotCoinType.eBitcoinCash, _bType));
                        Wallets.Add(new BotWallet(BotCoinType.eEthereum, _bType));
                        Wallets.Add(new BotWallet(BotCoinType.eEthereumClassic, _bType));
                        Wallets.Add(new BotWallet(BotCoinType.eMonero, _bType));
                        Wallets.Add(new BotWallet(BotCoinType.eIOTA, _bType));
                        Wallets.Add(new BotWallet(BotCoinType.eLitecoin, _bType));
                        Wallets.Add(new BotWallet(BotCoinType.eRipple, _bType));
                        Wallets.Add(new BotWallet(BotCoinType.eDash, _bType));
                        Wallets.Add(new BotWallet(BotCoinType.eNeo, _bType));
                    }
                }

                // Requests initialisieren:
                SubscriptionWebRequests.Add(new BotSubscriptionWebRequest(BotSubscriptionType.eTicker, "https://api.bitfinex.com/v2/tickers?symbols={0}"));
            }


            #region Initialization
            public override bool ActivateService(BotServiceOwner bOwner = null)
            {
                if (base.ActivateService(bOwner))
                {
                    return (true);
                }
                else
                    return (false);
            }
            public override bool DeactivateService()
            {
                return (base.DeactivateService());
            }
            #endregion


            #region Events.Socket
            protected override bool OnSockMessage(string sMessage)
            {
                try
                {
                    bool bResult = true;

                    if (sMessage[0] == '{')
                    {
                        // Eine Nachricht im "JSON"-Format wurde empfangen:
                        dynamic dMsgObjects = JsonConvert.DeserializeObject<dynamic>(sMessage);

                        // Nachricht auswerten:
                        if (dMsgObjects["event"] != null)
                        {
                            string sEvent = dMsgObjects["event"];

                            // Typ: Event.

                            if (sEvent == "info")
                            {
                                if (dMsgObjects["version"] != null)
                                {
                                    int iVersion = dMsgObjects["version"];

                                    // Typ: Event / Version Info.

                                    if (iVersion == GEN_VERSION)
                                    {
                                        // Standard-Konfiguration anwenden:
                                        SetConfig();

                                        BotEnv.LogSystem.WriteLog("OSM200", BotLogProcess.eCommunication, "Verbindungsaufbu erfolgreich.", BotLogType.ePlatformInfo, BotLogPriority.eLow);
                                        Owner.OnSocketConnected();
                                    }
                                    else
                                    {
                                        BotEnv.LogSystem.WriteLog("OSM206", BotLogProcess.eVersion, String.Format("Unerwartete Version {0}. Trenne Verbindung!", iVersion), BotLogType.ePlatformWarning, BotLogPriority.eHigh);

                                        // Verbindung trennen:
                                        Connection = false;
                                    }
                                }
                                else if (dMsgObjects["code"] != null)
                                {
                                    int iCode = dMsgObjects["code"];

                                    // INFO:
                                    // > https://bitfinex.readme.io/v2/docs/ws-general

                                    // Typ: Event / Code.
                                    BotEnv.LogSystem.WriteLog("OSM216", BotLogProcess.eEvent, String.Format("{0} (Code: {1})", (String)dMsgObjects["msg"], iCode), BotLogType.ePlatformEvent, BotLogPriority.eLow);

                                    switch (iCode)
                                    {
                                        case IC_RESTART_SERVER:
                                            // Verbindung neu aufbauen:
                                            Reconnect();

                                            break;

                                        case IC_MAINTENANCE_START:
                                        case IC_MAINTENANCE_END:
                                            bIsMaintenance = (iCode == IC_MAINTENANCE_START);

                                            // Owner benachrichtigen:
                                            Owner.OnMaintenance(bIsMaintenance);
                                            break;

                                        default:
                                            Debug.Assert(false, "[OSM] Unbekannter Code!");
                                            bResult = false;
                                            break;
                                    }
                                }
                                else
                                {
                                    Debug.Assert(false, "[OSM] Unbekannte Information!");
                                    bResult = false;
                                }
                            }
                            else if (sEvent == "auth")
                            {
                                String sState = dMsgObjects["status"];

                                if (sState == MC_OK)
                                {
                                    // Beispiel-Daten:
                                    // { event: 'auth', status: 'OK', chanId: 0, userId: 269312, caps: '{"orders": {"read": "1", "write": "0"}, "account": {"read": "1", "write": "0"}, "funding": {"read": "1", "write": "1"}, "history": {"read": "1", "write": "0"}, "wallets": {"read": "1", "write": "1"}, "withdraw": {"read": "0", "write": "1"}, "positions": {"read": "1", "write": "1"}}' }

                                    lLogin.bLoggedIn = true;
                                    UtlParse.Int(dMsgObjects["chanId"], out lLogin.iAuthChanID);
                                    UtlParse.Ulong(dMsgObjects["userId"], out lLogin.ulAuthUserID);

                                    Owner.OnLoggedIn();
                                }
                                else if (sState == MC_ERROR)
                                {
                                    // Beispiel-Daten:
                                    // {{ "event": "auth", "status": "FAILED", "chanId": 0, "code": 10100, "msg": "apikey: invalid"}}

                                    BotEnv.LogSystem.WriteLog("OSM507", BotLogProcess.eLogin, ("Authentifizierung fehlgeschlagen (Code=" + dMsgObjects["code"] + ", " + dMsgObjects["msg"] + ")."), BotLogType.ePlatformError, BotLogPriority.eHigh);
                                }
                                else
                                {
                                    Debug.Assert(false, "[OSM410] Unbekannter Status!");
                                    bResult = false;
                                }
                            }
                            else if (sEvent == "conf")
                            {
                                String sState = dMsgObjects["status"];

                                if (sState.ToUpper() != "OK")
                                    BotEnv.LogSystem.WriteLog("OSM313", BotLogProcess.eConfig, String.Format("Status: {0}", sState), BotLogType.eInfo);
                            }
                            else if (sEvent == "pong")
                            {
                            }
                            else if (sEvent == "subscribed")
                            {
                                BotBaseSubscription bNewSubscription = null;
                                int iChannelID = -1;
                                string sChannel = dMsgObjects["channel"];

                                if (sChannel == "ticker")
                                {
                                    BotTradingPair tRefPair;

                                    // Beispiel-Daten:
                                    // {"event":"subscribed","channel":"ticker", "chanId":5,"symbol":"tBTCUSD","pair":"BTCUSD"}

                                    iChannelID = dMsgObjects["chanId"];
                                    tRefPair = FindPair((String)dMsgObjects["pair"]);

                                    if ((iChannelID >= 0) && (tRefPair != null))
                                        bNewSubscription = new BotTickerSubscription(iChannelID, tRefPair);
                                }
                                else if (sChannel == "trades")
                                {
                                    // (BETA) ...
                                }
                                else if (sChannel == "book")
                                {
                                    // (BETA) ...
                                }
                                else if (sChannel == "candles")
                                {
                                    BotTradingPair tRefPair;
                                    String sKey;

                                    // Beispiel-Daten:
                                    // { "event": "subscribed", "channel": "candles", "chanId": 290202, "key": "trade:1m:tBTCUSD" }

                                    iChannelID = dMsgObjects["chanId"];
                                    sKey = dMsgObjects["key"];

                                    // Informationen aus Key ermitteln:
                                    Regex rRegexObj = new Regex(@"trade:(.*):t(.*)");
                                    Match mResult = rRegexObj.Match(sKey);

                                    if (mResult.Success)
                                    {
                                        tRefPair = FindPair(mResult.Groups[2].Value);

                                        if (tRefPair != null)
                                            bNewSubscription = new BotCandleSubscription(iChannelID, tRefPair, FromTimeFrame(mResult.Groups[1].Value));
                                    }
                                }
                                else
                                {
                                    Debug.Assert(false, "[OSM476] Unbekannter Subscription-Typ!");
                                    bResult = false;
                                }

                                if (bResult)
                                {
                                    if ((iChannelID >= 0) && (bNewSubscription != null))
                                    {
                                        lSubscriptions.Add(bNewSubscription);

                                        Owner.OnSubscribed(bNewSubscription);
                                    }
                                    else
                                        BotEnv.LogSystem.WriteLog("OSM415", BotLogProcess.eCommunication, ("Ungültiges Abonnement (ID=" + iChannelID + ")"), BotLogType.ePlatformWarning);
                                }
                            }
                            else if (sEvent == "unsubscribed")
                            {
                                // Beispiel-Daten:
                                // { "event":"unsubscribed","status":"OK","chanId":4}

                                if (dMsgObjects["status"] == MC_OK)
                                {
                                    int iChannelID = dMsgObjects["chanId"];
                                    BotBaseSubscription bSubscription = FindSubscription(iChannelID);

                                    if (bSubscription == null)
                                        BotEnv.LogSystem.WriteLog("OSM506", BotLogProcess.eCommunication, ("Ungültiges Abonnement (Abonnement-ID=" + iChannelID + ")"), BotLogType.eWarning);
                                    else
                                        lSubscriptions.Remove(bSubscription);
                                }
                                else
                                {
                                    Debug.Assert(false, "[OSM511] Unbekannter Code!");
                                    bResult = false;
                                }
                            }
                            else if (sEvent == "error")
                            {
                                // Beispiel-Daten:
                                // "{\"event\":\"error\",\"msg\":\"auth: invalid\",\"code\":10100}"

                                Owner.OnError((String)dMsgObjects["code"], (String)dMsgObjects["msg"]);
                            }
                            else
                            {
                                Debug.Assert(false, "[OSM529] Unbekanntes Event!");
                                bResult = false;
                            }
                        }
                        else
                        {
                            Debug.Assert(false, "[OSM529] Unbekannte Message!");
                            bResult = false;
                        }
                    }
                    else if (sMessage[0] == '[')
                    {
                        BotBaseSubscription bSubscr = null;
                        dynamic dMsgObjects;
                        int iChannelID;

                        // Eine Nachricht im "JSON-Stream"-Format wurde empfangen:
                        dMsgObjects = JsonConvert.DeserializeObject<dynamic>(sMessage);
                        iChannelID = dMsgObjects[0];

                        // Subscription empfangen:
                        if (iChannelID > lLogin.iAuthChanID)
                        {
                            // Kanal / Subscription - Informationen empfangen:
                            bSubscr = FindSubscription(iChannelID);

                            if (bSubscr == null)
                            {
                                BotEnv.LogSystem.WriteLog("OSM781", BotLogProcess.eAccount, ("Unbekanntes Abonnement '" + iChannelID + "' empfangen)."), BotLogType.ePlatformError);
                                bResult = false;
                            }
                        }

                        if (bResult)
                        {
                            if ((((JToken)dMsgObjects[1]).Type == JTokenType.String) && (dMsgObjects[1].Value == "hb"))
                            {
                                // Ein Heardbeat wurde empfangen.

                                // Beispiel-Daten:
                                // [0,"hb"]
                                // [0,"hb",56606]

                                if ((bSubscr != null) && (bSubscr.IsReady))
                                {
                                    switch (bSubscr.GetSubscriptionType())
                                    {
                                        case BotSubscriptionType.eTicker:
                                            BotTickerSubscription tTicker = (BotTickerSubscription)bSubscr;
                                            BotTickerSubscription.BotTickerData tCopy;

                                            // Kopieder veralteten Informationen erstellen:
                                            tCopy = (BotTickerSubscription.BotTickerData)tTicker.lData.Last().Clone();

                                            tCopy.dReceivedTime = DateTime.Now;

                                            // Auswertung der veralteten Daten erneut ausführen:
                                            Owner.OnSubscriptionData(BotSubscriptionPurpose.eRegular, bSubscr);
                                            break;
                                    }
                                }
                            }
                            else
                            {
                                // Beispiel-Daten:
                                // [0,"os",[...]]

                                if (dMsgObjects[0] == lLogin.iAuthChanID)
                                    // Account-Informationen empfangen:
                                    bResult = ParseMessageStream((String)dMsgObjects[1], dMsgObjects[2]);
                                else if (bSubscr != null)
                                {
                                    bResult = ParseSubscription(dMsgObjects[1], bSubscr);
                                    if (bResult)
                                        Owner.OnSubscriptionData(BotSubscriptionPurpose.eRegular, bSubscr);
                                    else
                                        BotEnv.LogSystem.WriteLog("OSM494", BotLogProcess.eCommunication, ("Ungültiger Subscription-Daten (Abonnement-ID=" + iChannelID + ")"), BotLogType.ePlatformWarning);
                                }
                            }
                        }
                    }
                    else
                    {
                        BotEnv.LogSystem.WriteLog("OSM1023", BotLogProcess.eAccount, "Unbekanntes Nachrichten-Format.", BotLogType.ePlatformError, BotLogPriority.eHigh);
                        bResult = false;
                    }

                    return (bResult);
                }
                catch (Exception eExcp)
                {
                    BotEnv.LogSystem.WriteDebugLog("OSM730", BotLogProcess.eCommunication, String.Format("Exception: {0}", eExcp.Message), sMessage, BotLogType.eError);
                    return (false);
                }
            }
            #endregion


            #region Configuration
            public override void WriteFile(JsonWriter jWriter, BotContentType bContent)
            {
                base.WriteFile(jWriter, bContent);
                {
                    jWriter.WriteEndObject();
                }
            }
            public override bool ReadFile(JObject dObjects, BotContentType bContent)
            {
                if (base.ReadFile(dObjects, bContent))
                {
                    return (true);
                }

                return (false);
            }
            #endregion




            #region Management
            public override void Reset()
            {
                lLogin.Reset();

                base.Reset();
            }
            public override BotBaseLoginInfo GetLoginInfo()
            {
                return (lLogin);
            }
            #endregion
            #region Communication.HTTP-Requests


            // Ermittelt Platform-spezifische Informationen:
            // > Symbol Details (https://docs.bitfinex.com/v1/reference#rest-public-symbol-details)
            private bool ReqHTTP_GetPltfSpecific()
            {
                return (ExecuteRequestAsync(BotLogProcess.eCommunication, "https://api.bitfinex.com/v1/symbols_details", (sResponse) =>
                {
                    dynamic dData = JsonConvert.DeserializeObject<dynamic>(sResponse);
                    if ((dData != null) && (dData.Count > 0))
                    {
                        // Beispiel-Daten:
                        // [{...},{...}]

                        for (int i = 0; i < dData.Count; i++)
                        {
                            dynamic _dData = dData[i];
                            BotTradingPair _bPair = FindPair((String)_dData["pair"], false);

                            if (_bPair != null)
                            {
                                _bPair.Precision = _dData["price_precision"];
                                _bPair.MinOrderSize = _dData["minimum_order_size"];
                                _bPair.MaxOrderSize = _dData["maximum_order_size"];
                            }
                        }
                    }
                    else
                        throw new BotInvalidRequestDataException();
                }).Result);
            }
            #endregion
            #region Communication.WebSockets
            public override bool Ping()
            {
                return (SocketSend("{\"event\":\"ping\"}"));
            }
            public override bool Login()
            {
                String sKey, sSecret;

                if ((eAuthAPIKey.GetValue(out sKey)) && (eAuthAPISecret.GetValue(out sSecret)))
                {
                    String sPayloadSign, sPayload, sNonce;

                    sNonce = GenerateClientOrderID();
                    sPayload = "AUTH" + sNonce;

                    // Hash erstellen:
                    // > Tipps: https://bitcoin.stackexchange.com/questions/25835/bitfinex-api-call-returns-400-bad-request
                    HMACSHA384 hSHA = new HMACSHA384(Encoding.UTF8.GetBytes(sSecret));
                    byte[] bHash = hSHA.ComputeHash(Encoding.UTF8.GetBytes(sPayload));
                    sPayloadSign = BitConverter.ToString(bHash).Replace("-", "").ToLower();

                    return (SocketSend("{ \"event\": \"auth\", \"apiKey\": \"" + sKey + "\", \"authSig\": \"" + sPayloadSign + "\", \"authPayload\": \"" + sPayload + "\", \"authNonce\": \"" + sNonce + "\" }"));
                }
                else
                {
                    BotEnv.LogSystem.WriteLog("L307", BotLogProcess.eAccount, "Angegebene Informationen für Authentifizierung ungültig!", BotLogType.ePlatformError);
                    return (false);
                }
            }
            public override bool PlaceOrder(BotTradingPair tPair, double dAmount, BotBaseOrder bOrder = null)
            {
                return (base.PlaceOrder(tPair, dAmount, new BotOrder()));
            }
            public override bool CancelOrder(BotBaseOrder bOrder)
            {
                BotOrder bCurOrder = (BotOrder)bOrder;

                Debug.Assert((bCurOrder != null), "[CO332]");

                return (SocketSend("[ " + lLogin.iAuthChanID + ", \"oc\", null, { \"id\": " + bCurOrder.ulOrderID + " } ]"));
            }

            public override bool GetPlatformSpecific()
            {
                return (ReqHTTP_GetPltfSpecific());
            }

            public override bool SubscribeTicker(BotTradingPair tPair)
            {
                Debug.Assert(tPair != null, "[ST189]");

                if (FindPairSubscription(tPair, BotSubscriptionType.eTicker) == null)
                    return (SocketSend("{ \"event\": \"subscribe\", \"channel\": \"ticker\", \"symbol\": \"" + "t" + tPair.Symbol + "\" }"));
                else
                    return (false);
            }
            public override bool SubscribeCandles(BotTradingPair tPair, TimeSpan tInterval)
            {
                Debug.Assert(tPair != null, "[SC198]");

                return (SocketSend("{ \"event\": \"subscribe\", \"channel\": \"candles\", \"key\": \"trade:" + ToTimeFrame(tInterval) + ":" + "t" + tPair.Symbol + "\" }"));
            }
            public override bool Unsubscribe(int iChannelID)
            {
                return (SocketSend("{ \"event\": \"unsubscribe\", \"chanId\": \"" + iChannelID + "\" }"));
            }


            protected override bool OnSendNewOrder(BotBaseOrder bOrder)
            {
                BotOrder fOrder = (BotOrder)bOrder;
                String sMessage;
                short sHidden = 0;

                fOrder.ulGroupID = OGID_INDICATING_TRADEBOT;

                // Werte auf Gültigkeit prüfen:
                switch (fOrder.oType)
                {
                    case BotOrderType.eExchangeMarket:
                        sMessage = "{ \"gid\": " + fOrder.ulGroupID + ", \"cid\": " + fOrder.ulClientOrderID + ", \"type\": \"" + UtlEnum.GetEnumDescription(fOrder.oType) + "\", \"symbol\": \"t" + fOrder.tRefPair.Symbol + "\", \"amount\": \"" + UtlParse.ToString(fOrder.dAmount) + "\", \"hidden\": " + sHidden + " }";
                        break;

                    case BotOrderType.eExchangeLimit:
                        sMessage = "{ \"gid\": " + fOrder.ulGroupID + ", \"cid\": " + fOrder.ulClientOrderID + ", \"type\": \"" + UtlEnum.GetEnumDescription(fOrder.oType) + "\", \"symbol\": \"t" + fOrder.tRefPair.Symbol + "\", \"amount\": \"" + UtlParse.ToString(fOrder.dAmount) + "\", \"price\": \"" + UtlParse.ToString(fOrder.dLimitRate) + "\", \"hidden\": " + sHidden + " }";
                        break;

                    default:
                        // Noch nicht implementiert!
                        Debug.Assert(false, (String.Format("[SNO283] Order-Typ '%s' noch nicht implementiert!", fOrder.oType.ToString())));
                        return (false);
                }

                // Order an Platform senden:
                return (SocketSend("[ 0, \"" + CN_NEW_ORDER + "\", null, " + sMessage + "]"));
            }
            #endregion
            #region Communication.WebSockets
            private bool SetConfig(bool bDecimalAsStrings = false, bool bTimesAsDateStrings = false, bool bSequencing = false)
            {
                int iOptionVal = 0;

                if (bDecimalAsStrings)
                    iOptionVal += 8;

                if (bTimesAsDateStrings)
                    iOptionVal += 32;

                if (bSequencing)
                    iOptionVal += 65536;

                return (SocketSend("{ \"event\": \"conf\", \"flags\": " + iOptionVal + " }"));
            }
            #endregion


            #region Helper.Communication
            // Parst enthaltene Gruppen in einem Message-Stream.
            private bool ParseSubGroups(String sGroupChannelName, dynamic dMsgObjects, bool bAllowEmpty = false)
            {
                // Beispiel-Daten:
                // [[...],[...]]

                if (dMsgObjects.Count > 0)
                {
                    // Orders wurden empfangen:
                    for (int i = 0; i < dMsgObjects.Count; i++)
                    {
                        if (ParseMessageStream(sGroupChannelName, dMsgObjects[i]) == false)
                            return (false);
                    }

                    return (true);
                }
                else
                    return (bAllowEmpty);
            }

            // Parst den Stream-Teil einer eingehenden Nachricht.
            // > Mit Stream ist der eigentliche Daten-Teil einer Nachricht gemeint.
            //   z.B. [0,"<CODE>",[<STREAM_DATA>]]
            private bool ParseMessageStream(String sChannelName, dynamic dMsgObjects)
            {
                BotOrder bOrder = null;
                int iIndex;
                bool bResult = true;

                switch (sChannelName)
                {
                    case CN_BALANCE_UPDATE:
                        // Beispiel-Daten:
                        // [0,"bu",[353.662327834,353.662327834]]
                        bResult = true; // (BETA) ...
                        break;

                    case CN_POSITION_SNAPSHOT:
                        // Beispiel-Daten:
                        // [0,"ps",[]]
                        break;

                    case CN_NEW_POSITION:
                        bResult = false; // (BETA) ...
                        break;

                    case CN_POSITION_UPDATE:
                        bResult = false; // (BETA) ...
                        break;

                    case CN_POSITION_CLOSE:
                        bResult = false; // (BETA) ...
                        break;

                    case CN_WALLET_SNAPSHOT:
                        bResult = ParseSubGroups(CN_WALLET_UPDATE, dMsgObjects);
                        break;

                    case CN_WALLET_UPDATE:
                        // Beispiel-Daten:
                        // "exchange","USD",66.90854874,0,null

                        String sWalletType = dMsgObjects[0];
                        BotWalletType wType = (BotWalletType)UtlEnum.GetEnumValue(sWalletType, typeof(BotWalletType));

                        if (wType == BotWalletType.eExchange)
                        {
                            String sCoinType = dMsgObjects[1];
                            BotWallet bWallet = GetWallet((BotCoinType)UtlEnum.GetEnumValue(sCoinType, typeof(BotCoinType)), wType);

                            if (bWallet == null)
                                BotEnv.LogSystem.WriteLog("PMS763", BotLogProcess.eWallet, String.Format("Wallet für unbekannte Typen '{0} ({1})' empfangen).", sCoinType, sWalletType), BotLogType.ePlatformError);
                            else
                            {
                                double dTemp;

                                UtlParse.Double(dMsgObjects[2], out dTemp);

                                bWallet.Funds = dTemp;
                                Owner.OnWalletUpdated(bWallet);
                            }
                        }
                        else
                        {
                            // (BETA) ...
                            // Wallet-Type (noch) nicht implementiert!
                        }

                        break;

                    case CN_ORDER_SNAPSHOT:
                        bResult = ParseSubGroups(CN_NEW_ORDER, dMsgObjects, true);
                        break;

                    case CN_NEW_ORDER:
                    case CN_ORDER_CANCEL:
                    case CN_ORDER_UPDATE:
                        bResult = ParseOrder(dMsgObjects, ref bOrder);

                        if (bResult)
                        {
                            iIndex = oOrders.FindIndex(oTemp => (((BotOrder)oTemp).ulClientOrderID == bOrder.ulClientOrderID));

                            if (iIndex == -1)
                            {
                                // Order konnte nicht gefunden werden!
                                // > Das kann sein, da eine neue Order auch via Website direkt in Börse erzeugt werden kann.
                            }
                            else
                            {
                                BotOrder bCurOrder = (BotOrder)oOrders[iIndex];
                                BotOrderState oOldState = bCurOrder.oState;
                                bool bIsNewOrder = false;

                                // Order aktualisieren:
                                ParseOrder(dMsgObjects, ref bCurOrder);

                                // Benachrichtigen:
                                switch (sChannelName)
                                {
                                    case CN_NEW_ORDER:
                                        bIsNewOrder = true;
                                        break;

                                    case CN_ORDER_CANCEL:
                                    case CN_ORDER_UPDATE:
                                        // Info:
                                        // > Reaktion auf Zustandsänderung erfolgt via 'CN_TRADE_EXECUTION_UPDATE'.

                                        if ((sChannelName == CN_ORDER_CANCEL) &&
                                            (oOldState == BotOrderState.ExInitialized) && (bCurOrder.oState == BotOrderState.eExecuted))
                                        {
                                            // Eine Order wird sofort ausgeführt:
                                            // > Als neue Order behandeln.
                                            // > Weitere Behandlung via 'CN_TRADE_EXECUTION_UPDATE'.
                                            bIsNewOrder = true;
                                        }
                                        else if (bCurOrder.oState == BotOrderState.eCanceled)
                                        {
                                            // Benachrichtigen:
                                            if (bCurOrder.iExecutedTrades == 0)
                                                // Order wurde abgebrochen:
                                                Owner.OnOrderClosed(bCurOrder, BotOrderResult.eCanceled);
                                            else
                                                // Order wurde teilweise ausgeführt:
                                                Owner.OnOrderClosed(bCurOrder, BotOrderResult.ePartiallyExecuted);

                                            oOrders.RemoveAt(iIndex);
                                        }

                                        break;

                                    default:
                                        Debug.Assert(false, "[PMS773]");
                                        break;
                                }

                                if (bIsNewOrder)
                                    // Benachrichtigen:
                                    Owner.OnOrderCreated(bCurOrder);
                            }
                        }

                        break;

                    case CN_ORDER_CANCEL_REQ:
                        bResult = false; // (BETA) ...
                        break;

                    case CN_TRADE_EXECUTED:
                        // Beispiel-Daten:
                        // [0,"te",[61223779,"tIOTUSD",1504382461000,3636595172,1,0.67466,"EXCHANGE MARKET",0.67466,-1]]
                        break;

                    case CN_TRADE_EXECUTION_UPDATE:
                        iIndex = oOrders.FindIndex(oTemp => (((BotOrder)oTemp).ulOrderID == (ulong)dMsgObjects[3]));

                        // Beispiel-Daten:
                        // [0,"tu",[61223779,"tIOTUSD",1504382461000,3636595172,1,0.67466,"EXCHANGE MARKET",0.67466,-1,-0.002,"IOT"]]

                        if (iIndex == -1)
                        {
                            // Order konnte nicht gefunden werden!
                            // > Das kann sein, da eine neue Order auch via Website direkt in Börse erzeugt werden kann.
                        }
                        else
                        {
                            bOrder = (BotOrder)oOrders[iIndex];

                            switch (bOrder.oState)
                            {
                                case BotOrderState.ePartiallyFilled:
                                case BotOrderState.eExecuted:
                                    double dAmount = dMsgObjects[4];
                                    double dRate = dMsgObjects[5];

                                    if (bOrder.iExecutedTrades == 0)
                                        // Aktuellen Kurs übernehmen:
                                        bOrder.dRate = dRate;
                                    else
                                        // Gewichteten Mittelwert mit zuletzt ausgeführten Trades bilden:
                                        bOrder.dRate = UtlMath.GetWeightedAverage(Math.Abs(bOrder.dExecutedAmountCum), Math.Abs(dAmount), bOrder.dRate, dRate);

                                    bOrder.dExecutedAmountCum += dAmount;
                                    bOrder.iExecutedTrades++;

                                    // Gebühr ermitteln:
                                    String sFeeCoinType = (String)dMsgObjects[10];
                                    BotCoinType cFeeCoinType = (BotCoinType)UtlEnum.GetEnumValue(sFeeCoinType, typeof(BotCoinType));

                                    if (cFeeCoinType == BotCoinType.Invalid)
                                        BotEnv.LogSystem.WriteLog("PMS967", BotLogProcess.eOrder, String.Format("Ungültige Währung '{0}' für Order-Gebühr empfangen.", sFeeCoinType), BotLogType.ePlatformError);
                                    else
                                        bOrder.AddFee((double)dMsgObjects[9], cFeeCoinType);

                                    if (bOrder.oState == BotOrderState.eExecuted)
                                    {
                                        // Owner informieren:
                                        Owner.OnOrderClosed(bOrder, BotOrderResult.eExecuted);

                                        // Abgeschlossene Order löschen:
                                        oOrders.RemoveAt(iIndex);
                                    }

                                    break;

                                default:
                                    BotEnv.LogSystem.WriteLog("PMS1011", BotLogProcess.eOrder, String.Format("Unerwarteter Status '{0}' eines Trade-Updates.", UtlEnum.GetEnumDescription(bOrder.oState)), BotLogType.ePlatformError);
                                    break;
                            }
                        }

                        break;

                    case CN_FUNDING_TRADE_EXECUTION:
                        bResult = false; // (BETA) ...
                        break;

                    case CN_FUNDING_TRADE_UPDATE:
                        bResult = false; // (BETA) ...
                        break;

                    case CN_HISTORICAL_ORDER_SNAPSHOT:
                        // Beispiel-Daten:
                        // [3576956306,null,66144678820,"tXRPUSD",1504030945134,1504030945161,0,-40,"EXCHANGE MARKET",null,null,null,0,"EXECUTED @ 0.223(-10.55): was PARTIALLY FILLED @ 0.223(-3.52), PARTIALLY FILLED @ 0.223(-25.93)",null,null,0.22302,0.22302824,0,0,null,null,null,0,0,0],[...]
                        break;

                    case CN_MARGIN_INFORMATION_SNAPSHOT:
                        bResult = false; // (BETA) ...
                        break;

                    case CN_MARGIN_INFORMATION_UPDATE:
                        bResult = false; // (BETA) ...
                        break;

                    case CN_NOTIFICATION:
                        // Beispiel-Daten:
                        // [null,"on-req",null,null,[3630488336,null,636399546388490000,null,null,null,-10,null,"EXCHANGE LIMIT",null,null,null,null,null,null,null,1,null,null,null,null,null,null,0,null,null],null,"SUCCESS","Submitting exchange limit sell order for 10.0 IOTA."]]
                        // [null,"on-req",null,null,[null,null,636456046892950000,null,null,null,0.00156231,null,"EXCHANGE MARKET",null,null,null,null,null,null,null,7081.8,null,null,null,null,null,null,0,null,null],null,"ERROR","Invalid order: minimum size for BTC/USD is 0.004"]]
                        break;

                    case CN_FUNDING_OFFER_SNAPSHOT:
                        // Beispiel-Daten:
                        // [0,"fos",[]]
                        break;

                    case CN_FUNDING_OFFER_NEW:
                        bResult = false; // (BETA) ...
                        break;

                    case CN_FUNDING_OFFER_UPDATE:
                        bResult = false; // (BETA) ...
                        break;

                    case CN_FUNDING_OFFER_CANCEL:
                        bResult = false; // (BETA) ...
                        break;

                    case CN_HISTORICAL_FUNDING_OFFER_SNAPSHOT:
                        bResult = false; // (BETA) ...
                        break;

                    case CN_FUNDING_CREDITS_SNAPSHOT:
                        // Beispiel-Daten:
                        // [0,"fcs",[]]
                        break;

                    case CN_FUNDING_CREDITS_NEW:
                        bResult = false; // (BETA) ...
                        break;

                    case CN_FUNDING_CREDITS_UPDATE:
                        bResult = false; // (BETA) ...
                        break;

                    case CN_FUNDING_CREDITS_CLOSE:
                        bResult = false; // (BETA) ...
                        break;

                    case CN_HISTORICAL_FUNDING_CREDITS_SNAPSHOT:
                        bResult = false; // (BETA) ...
                        break;

                    case CN_FUNDING_LOAN_SNAPSHOT:
                        // Beispiel-Daten:
                        // [0,"fls",[]]
                        break;

                    case CN_FUNDING_LOAN_NEW:
                        bResult = false; // (BETA) ...
                        break;

                    case CN_FUNDING_LOAN_UPDATE:
                        bResult = false; // (BETA) ...
                        break;

                    case CN_FUNDING_LOAN_CLOSE:
                        bResult = false; // (BETA) ...
                        break;

                    case CN_HISTORICAL_FUNDING_LOAN_SNAPSHOT:
                        bResult = false; // (BETA) ...
                        break;

                    case CN_HISTORICAL_FUNDING_TRADE_SNAPSHOT:
                        // Beispiel-Daten:
                        // [0,"hfts",[]]
                        break;

                    case CN_USER_CUSTOM_PRICE_ALERT:
                        bResult = false; // (BETA) ...
                        break;

                    // UNBEKANNTE KANÄLE:
                    case "hts":
                        // Beispiel-Daten:
                        // [59574241,"tXRPUSD",1504030945000,3576956306,-10.551995,0.22302,"EXCHANGE MARKET",0.22302,-1,-0.00000103,"BTC"],[...]
                        break;

                    case "ats":
                        // Beispiel-Daten:
                        // [0,"ats",[]]
                        break;

                    case "fiu":
                        // Beispiel-Daten:
                        // [0,"fiu",["sym","fUSD",[0,0,0,0]]]
                        break;

                    default:
                        BotEnv.LogSystem.WriteLog("PMS822", BotLogProcess.eAccount, ("Ungültige Kanal-Bezeichnung '" + sChannelName + "' empfangen)."), BotLogType.ePlatformError);
                        bResult = false;
                        break;
                }

                return (bResult);
            }

            private bool ParseOrder(dynamic dMsgObjects, ref BotOrder bResult)
            {
                bool bNewOrder = (bResult == null);
                // Beispiel-Daten:
                // 3544878669,null,63515873259,"tIOTUSD",1503855516314,1503915261619,-100,-100,"EXCHANGE LIMIT",null,null,null,0,"ACTIVE",null,null,0.975,0,0,0,null,null,null,0,0,0

                if (bNewOrder)
                {
                    bResult = new BotOrder();

                    // Order-Daten parsen:
                    bResult.tRefPair = FindPair((String)dMsgObjects[3]);
                    bResult.oType = (BotOrderType)UtlEnum.GetEnumValue((string)dMsgObjects[8], typeof(BotOrderType));

                    UtlParse.Double(dMsgObjects[7], out bResult.dAmount);                  // Field: AMOUNT
                    UtlParse.Double(dMsgObjects[17], out bResult.dRate);                   // Field: PRICE AVERAGE (Durchschnittspreis zu dem die Order letztendlich ausgeführt wurde)

                    switch (bResult.oType)
                    {
                        case BotOrderType.eExchangeLimit:
                        case BotOrderType.eLimit:
                            UtlParse.Double(dMsgObjects[16], out bResult.dLimitRate);      // Field: PRICE
                            break;
                    }
                }

                // Order-Daten parsen (bzw. aktualisieren, im Fall von 'bNewOrder == true'):
                bResult.oState = (BotOrderState)UtlEnum.GetEnumValue((string)dMsgObjects[13], typeof(BotOrderState), false);

                UtlParse.Ulong(dMsgObjects[0], out bResult.ulOrderID);
                UtlParse.Ulong(dMsgObjects[1], out bResult.ulGroupID);
                UtlParse.Ulong(dMsgObjects[2], out bResult.ulClientOrderID);

                if ((bNewOrder == false) || (bResult.IsValid()))
                    return (true);
                else
                {
                    bResult = null;
                    GC.Collect();

                    return (false);
                }
            }
            private bool ParseSubscription(dynamic dData, BotBaseSubscription bSubscription)
            {
                bool bResultTmp = false;

                Debug.Assert((bSubscription != null), "[PS1135]");

                BotSubscriptionType _bType = bSubscription.GetSubscriptionType();
                switch (_bType)
                {
                    case BotSubscriptionType.eTicker:
                        double dPrice = dData[6];

                        if (dPrice != 0)
                        {
                            BotTickerSubscription tSubscr = (BotTickerSubscription)bSubscription;
                            BotTickerSubscription.BotTickerData tData = new BotTickerSubscription.BotTickerData();

                            // Ticker-Daten wurden empfangen:
                            UtlParse.Double(dData[0], out tData.dBid);
                            UtlParse.Double(dData[1], out tData.dBidSize);
                            UtlParse.Double(dData[2], out tData.dAsk);
                            UtlParse.Double(dData[3], out tData.dAskSize);
                            UtlParse.Double(dData[4], out tData.dDailyChange);
                            UtlParse.Double(dData[5], out tData.dDailyChangePerc);
                            UtlParse.Double(dPrice, out tData.dLastPrice);
                            UtlParse.Double(dData[7], out tData.dVolume);
                            UtlParse.Double(dData[8], out tData.dHigh);
                            UtlParse.Double(dData[9], out tData.dLow);

                            tSubscr.AddData(tData);

                            bResultTmp = true;
                        }
                        break;

                    case BotSubscriptionType.eTrade:
                        // (BETA) ...
                        Debug.Assert(false, "[PS1160] Noch nicht implementiert!");
                        break;

                    case BotSubscriptionType.eBook:
                        // (BETA) ...
                        Debug.Assert(false, "[PS1165] Noch nicht implementiert!");
                        break;

                    case BotSubscriptionType.eRawBook:
                        // (BETA) ...
                        Debug.Assert(false, "[PS1170] Noch nicht implementiert!");
                        break;

                    default:
                        Debug.Assert(false, string.Format("[PS1175] Nicht unterstützter Subscription-Typ '{0}' angefordert!", _bType.ToString()));
                        break;
                }

                return (bResultTmp);
            }
            #endregion


            private BotLoginInfo lLogin = new BotLoginInfo();
        }
        #endregion


        #region Helper.Communication
        #endregion
        #region Helper.Translation
        private static String ToTimeFrame(TimeSpan tInterval)
        {
            // Intervall in Zeitfenster umrechnen:
            if (tInterval.TotalMinutes <= 60)
                return (String.Format("{0}m", tInterval.TotalMinutes));

            else if (tInterval.TotalHours <= 24)
                return (String.Format("{0}h", tInterval.TotalHours));

            else if (tInterval.TotalDays <= 30)
                return (String.Format("{0}D", tInterval.TotalDays));

            else
                return (String.Format("1M"));
        }
        private static TimeSpan FromTimeFrame(String sTimeFrame)
        {
            // Zeitfenster in Intervall umrechnen:
            switch (sTimeFrame)
            {
                case CNDL_TIMEFRAME_1m: return (new TimeSpan(0, 1, 0));
                case CNDL_TIMEFRAME_5m: return (new TimeSpan(0, 5, 0));
                case CNDL_TIMEFRAME_15m: return (new TimeSpan(0, 15, 0));
                case CNDL_TIMEFRAME_30m: return (new TimeSpan(0, 30, 0));
                case CNDL_TIMEFRAME_1h: return (new TimeSpan(1, 0, 0));
                case CNDL_TIMEFRAME_3h: return (new TimeSpan(3, 0, 0));
                case CNDL_TIMEFRAME_6h: return (new TimeSpan(6, 0, 0));
                case CNDL_TIMEFRAME_12h: return (new TimeSpan(12, 0, 0));
                case CNDL_TIMEFRAME_1D: return (new TimeSpan(1, 0, 0, 0));
                case CNDL_TIMEFRAME_14D: return (new TimeSpan(14, 0, 0));
                case CNDL_TIMEFRAME_1M: return (new TimeSpan(31, 0, 0));
                default:
                    Debug.Assert(false, "[FTF796]");
                    break;
            }
            return (default(TimeSpan));
        }
        #endregion
    }
}
