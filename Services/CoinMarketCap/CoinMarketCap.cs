﻿using System;
using Shared.Net;
using Newtonsoft.Json;
using System.Diagnostics;
using Shared.Utils.Core;
using System.Threading.Tasks;

namespace CryptoRiftBot.Trading.Services.CoinMarketCap
{
    [BotServiceDescriptor("api.coinmarketcap.com", "CoinMarketCap.com")]
    public class BotSvcCoinMarketCap : BotServiceProvider
    {
        public BotSvcCoinMarketCap()
        {
            AddService(new BotSvcTicker(this));
        }


        #region Types.Services
        public class BotSvcTicker : BotBaseService, IBotTickerService
        {
            #region Constans
            private const int REQ_TICKER_DATA = 100;

            private static readonly TimeSpan INTERVAL_TICKER_DATA = new TimeSpan(0, 5, 0);
            #endregion


            public BotSvcTicker(IBotServiceProvider iProvider) : base(iProvider)
            {
            }


            #region Communication
            public async Task<bool> GetTickerAsync(IBotCoinInfoList bList)
            {
                return (await ReqHTTP_GetTickerDataAsync(bList));
            }
            #endregion
            #region Communication.HTTP-Requests
            private async Task<bool> ReqHTTP_GetTickerDataAsync(IBotCoinInfoList bList)
            {
                // INFO:
                // > Es stehen nur Informationen für Sekundär-Währungen "USD" und "BTC" zur Verfügung!

                // (BETA) ...
                // > Es werden nur Informationen für Sekundär-Währung "USD" berücksichtigt!

                bool _bResult = false;
                if (bList.Count > 0)
                {
                    if (await ExecuteRequestAsync(BotLogProcess.eUpdateRating, REQ_TICKER_DATA, INTERVAL_TICKER_DATA, "https://api.coinmarketcap.com/v1/ticker/", (sResponse) =>
                    {
                        dynamic dData = JsonConvert.DeserializeObject<dynamic>(sResponse);
                        if (dData != null)
                        {
                            IBotCoinInfo _bInfo;
                            BotCoinType _bSecCoin;
                            Type tType = typeof(BotCoinType);

                            // Ticker-Daten auslesen:
                            // > Reihenfolge in 'cCoinInfo' entspricht Ranking auf "coinmarketcap.com".
                            dynamic _dData;
                            for (int i = 0; i < dData.Count; i++)
                            {
                                _dData = dData[i];
                                _bSecCoin = (BotCoinType)UtlEnum.GetEnumValue(GetSymbolName((String)_dData["symbol"]), tType);
                                if (_bSecCoin != BotCoinType.Invalid)
                                {
                                    _bInfo = bList.Find(_bSecCoin);
                                    if (_bInfo == null)
                                        Debug.Assert(false, "[GTD67]");
                                    else
                                    {
                                        _bInfo.Update(
                                            _dData["name"],
                                            null,
                                            UtlParse.Double(_dData["price_usd"]),
                                            UtlParse.Double(_dData["market_cap_usd"]),
                                            new double[] { UtlParse.Double(_dData["percent_change_1h"]), UtlParse.Double(_dData["percent_change_24h"]), UtlParse.Double(_dData["percent_change_7d"]) });

                                        _bResult = true;
                                    }
                                }
                            }
                        }
                        else
                            throw new BotInvalidRequestDataException();
                    }) == WebRequestEx.Result.Error)
                        _bResult = false;
                }

                return (_bResult);
            }
            #endregion


            #region Helper
            private static String GetSymbolName(String sSymbol)
            {
                // Ggf. Bezeichnung anpassen:
                switch (sSymbol)
                {
                    case "MIOTA":
                        return ("IOT");
                    case "DASH":
                        return ("DSH");
                }

                return (sSymbol);
            }
            #endregion
        }
        #endregion
    }
}
