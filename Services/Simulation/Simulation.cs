﻿using CryptoRiftBot.Config;
using CryptoRiftBot.Trading.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Timers;
using Shared.Utils.Core;
using System.IO;

namespace CryptoRiftBot.Trading.Services.Simulation
{
    /// <summary>
    /// Simulation von Handelsplatformen zum Optimieren von Strategien.
    /// </summary>
    [BotServiceDescriptor("simulation.cryptoriftbot", "Simulation")]
    public class BotSvcSimulation : BotServiceProvider
    {
        public BotSvcSimulation()
        {
            AddService(new BotSvcSimPlatform(this));
        }


        #region Types
        // Definiert Eigenschaften von Währungen für realitätsnahe Simulation.
        internal class BotSimPairProperties
        {
            #region Constants
            public const double SIMVAL_LIMIT_FACTOR = 0.18;                                     // Faktor für Definition eines Bereichs zur Simulation von Ticker-Werten
            #endregion


            public BotSimPairProperties(BotTradingPair tPair)
            {
                this.tRefPair = tPair;
                this.dMinVal = 0;
                this.dMaxVal = 0;
            }


            public BotTradingPair tRefPair;
            public double dMinVal;
            public double dMaxVal;
        }
        internal class BotSimOrder : BotBaseOrder
        {
            public bool CalcCreated(BotTickerSubscription sTicker)
            {
                if (sTicker == null)
                    Debug.Assert(false, "[CC43] Subscription ungültig!");
                else if (sTicker.lData.Count == 0)
                    Debug.Assert(false, "[CC47]");
                else
                {
                    // Empfangszeitpunkt von letzten Ticker-Daten anwenden:
                    CreationTime = sTicker.lData.Last().dReceivedTime;
                    return (true);
                }
                return (false);
            }
        }

        internal class BotSimStateMachine
        {
            public abstract class BotBase
            {
                #region Events
                public bool SimulateTickerData(BotSvcSimPlatform.BotSimTickerSubscription tTicker, BotSimPairProperties sProperties, double dFilterFactor)
                {
                    if (tTicker.lData.Count > 0)
                    {
                        // Aktuelle Ticker-Daten ermitteln:
                        tCurData = tTicker.lData.Last();

                        if (tTicker.lData.Count == 1)
                        {
                            // Currency-Bewertung ermitteln:
                            IBotCoinInfo bInfo;
                            if (BotCurrencyRating.GetLatestCoinInfo(tTicker.ReferredPair.CurrencyPrim, out bInfo))
                            {
                                // Aktuellen Preis initialisieren:
                                tCurData.dLastPrice = bInfo.Price;

                                OnInit();

                                return (true);
                            }
                        }
                        else
                        {
                            // Vorherige Ticker-Daten ermitteln:
                            tPrevData = tTicker.lData[tTicker.lData.Count - 2];

                            OnTickerCycle(sProperties, dFilterFactor);

                            return (true);
                        }
                    }
                    else
                        Debug.Assert(false, "[STD1184]");

                    return (false);
                }
                #endregion
                #region Events
                protected virtual void OnInit() { }
                protected abstract void OnTickerCycle(BotSimPairProperties sProperties, double dFilterFactor);
                #endregion


                protected BotTickerSubscription.BotTickerData tCurData;
                protected BotTickerSubscription.BotTickerData tPrevData;
            }
            public class BotRandomSmoothed : BotBase
            {
                #region Events
                protected override void OnTickerCycle(BotSimPairProperties sProperties, double dFilterFactor)
                {
                    // Wert glätten:
                    tCurData.dLastPrice = UtlMath.FilterTP1(tPrevData.dLastPrice, tCurData.dLastPrice, dFilterFactor);
                }
                #endregion
            }
            public class BotRandomFollowing : BotBase
            {
                #region Events
                protected override void OnInit()
                {
                    dTargetPrice = tCurData.dLastPrice;
                }
                protected override void OnTickerCycle(BotSimPairProperties sProperties, double dFilterFactor)
                {
                    // Aktuellen Wert in Richtung des anvisierten Kurs-Wert filtern:
                    tCurData.dLastPrice = UtlMath.FilterTP1(tPrevData.dLastPrice, dTargetPrice, (dFilterFactor * 10));

                    if (Math.Abs(tCurData.dLastPrice - dTargetPrice) <= (dTargetPrice * 0.1))
                        // Neuen Kurs-Wert anvisieren (Filter anwenden um deutliche Kurs-Sprünge zu vermeiden):
                        dTargetPrice = UtlMath.FilterTP1(dTargetPrice, UtlNumber.GetRandom(sProperties.dMinVal, sProperties.dMaxVal), 0.12);
                }
                #endregion


                private double dTargetPrice;
            }
            public class BotRealistic : BotBase
            {
                #region Constants
                public const double PROP_IDLE_FILTER = 0.01;

                public const double PROP_STRONGPEAK_STRENGTH_FACTOR = 0.1;
                #endregion


                #region Enumerations
                public enum BotStateMode
                {
                    Undefined,

                    eIdleLow,
                    eIdleMed,
                    eIdleHigh,
                    eSlowStairs,                           // Langsames absteigen / abfallen des Kurses
                    eExtStairs,                            // Erweitertes absteigen / abfallen des Kurses
                    eStrongPeak,                           // Starker Ausschlag des Kurses
                    eHeavyPeak                             // Krasser Ausschlag des Kurses
                }
                public enum BotDirection
                {
                    eRandom,

                    eFall,
                    eRise
                }
                [Flags]
                public enum BotFlags
                {
                    eRandomDirection = 0x01
                }
                #endregion


                #region Types
                public class BotRange<T>
                {
                    public BotRange(T tMin, T tMax)
                    {
                        this.tMin = tMin;
                        this.tMax = tMax;
                    }


                    public T tMin;
                    public T tMax;
                }
                public class BotQueueItem
                {
                    public BotQueueItem(BotStateMode sMode, BotDirection dDirection = BotDirection.eRandom)
                    {
                        this.sMode = sMode;
                        this.dDirection = dDirection;
                    }


                    public BotStateMode sMode;
                    public BotDirection dDirection;
                }
                public class BotStateProperties
                {
                    public BotStateProperties(int iWeight, BotRange<int> rCycleLength, BotRange<double> rStrengthFactor, int iQueueChance = 0, BotFlags fFlags = 0)
                    {
                        this.rCycleLength = rCycleLength;
                        this.rStrengthFactor = rStrengthFactor;
                        this.iWeight = iWeight;
                        this.iQueueChance = UtlNumber.LimitEx(0, iQueueChance, 100);
                        this.fFlags = fFlags;
                    }


                    public BotRange<int> rCycleLength;
                    public BotRange<double> rStrengthFactor;
                    public int iWeight;
                    public int iQueueChance;                        // Chance nach Abschluss des Zustands eine vordefinierte Abfolge an Zuständen auszulösen
                    public BotFlags fFlags;
                }
                public class BotStateData
                {
                    public BotStateMode sMode;
                    public BotDirection dDirection;
                    public int iRemainingCycles;
                }
                #endregion


                #region Properties.Management
                [Browsable(false)]
                protected int TotalWeight { private set; get; }
                [Browsable(false)]
                protected double DirectionVal { private set; get; }
                #endregion
                #region Properties
                [Browsable(false)]
                public BotStateData CurState { private set; get; }
                [Browsable(false)]
                protected BotStateProperties CurProperties { get { return dStateProperties[CurState.sMode]; } }
                #endregion


                #region Events
                protected override void OnInit()
                {
                    if (dStateProperties == null)
                    {
                        CurState = new BotStateData();

                        dStateProperties = new Dictionary<BotStateMode, BotStateProperties>();
                        lStateQueue = new List<BotQueueItem>();

                        Debug.Assert((UtlEnum.GetEnumCount(typeof(BotStateMode)) == 8), "[OI1026]");

                        // Zustand-Propertiesrmationen initialisieren:
                        dStateProperties[BotStateMode.eIdleLow] = new BotStateProperties(100, new BotRange<int>(3, 15), new BotRange<double>(0.0001, 0.0004), 0, BotFlags.eRandomDirection);
                        dStateProperties[BotStateMode.eIdleMed] = new BotStateProperties(75, new BotRange<int>(2, 10), new BotRange<double>(0.0004, 0.001), 0, BotFlags.eRandomDirection);
                        dStateProperties[BotStateMode.eIdleHigh] = new BotStateProperties(60, new BotRange<int>(2, 5), new BotRange<double>(0.001, 0.0017), 0, BotFlags.eRandomDirection);
                        dStateProperties[BotStateMode.eSlowStairs] = new BotStateProperties(40, new BotRange<int>(1, 4), new BotRange<double>(0.002, 0.0037), 30);
                        dStateProperties[BotStateMode.eExtStairs] = new BotStateProperties(35, new BotRange<int>(1, 4), new BotRange<double>(0.0037, 0.005), 30);
                        dStateProperties[BotStateMode.eStrongPeak] = new BotStateProperties(20, new BotRange<int>(1, 3), new BotRange<double>(0.005, 0.006), 30);
                        dStateProperties[BotStateMode.eHeavyPeak] = new BotStateProperties(15, new BotRange<int>(1, 2), new BotRange<double>(0.0065, 0.0075), 30);

                        // Gesamt-Gewichtung ermitteln:
                        TotalWeight = 0;

                        foreach (var kPairTmp in dStateProperties)
                            TotalWeight += kPairTmp.Value.iWeight;
                    }

                    SetState(new BotQueueItem(BotStateMode.eIdleLow));
                }
                protected override void OnTickerCycle(BotSimPairProperties sProperties, double dFilterFactor)
                {
                    if (CurState.iRemainingCycles == 0)
                        // Aktueller Zustand abgeschlossen.
                        // > Nächsten Zustand aktivieren:
                        SetStateEx();

                    // Aktuelen Zustand aktualisieren:
                    if (CurState.dDirection == BotDirection.eRandom)
                        DirectionVal = (UtlNumber.GetRandom() ? 1.0 : -1.0);
                    else
                        DirectionVal = ((CurState.dDirection == BotDirection.eRise) ? 1.0 : -1.0);

                    // Nächsten Kurswert bestimmen:
                    double dMin = (tPrevData.dLastPrice * (CurProperties.rStrengthFactor.tMin * dFilterFactor));
                    double dMax = (tPrevData.dLastPrice * (CurProperties.rStrengthFactor.tMax * dFilterFactor));
                    double dValue = (UtlNumber.GetRandom(dMin, dMax) * DirectionVal);

                    tCurData.dLastPrice = (tPrevData.dLastPrice + dValue);


                    // (BETA) ... killme
                    Debug.WriteLine(String.Format("   {0} (Min={1}, Max={2})", dValue, dMin, dMax));


                    CurState.iRemainingCycles--;
                }
                #endregion


                #region Management
                private void SetState(BotQueueItem qItem)
                {
                    if (qItem.sMode == BotStateMode.Undefined)
                        Debug.Assert(false, "[SS1096]");
                    else
                    {
                        CurState.sMode = qItem.sMode;

                        // Zustand initialisieren:
                        CurState.iRemainingCycles = UtlNumber.GetRandom(CurProperties.rCycleLength.tMin, CurProperties.rCycleLength.tMax);

                        if (CurProperties.fFlags.HasFlag(BotFlags.eRandomDirection))
                            CurState.dDirection = BotDirection.eRandom;
                        else
                            CurState.dDirection = (UtlNumber.GetRandom() ? BotDirection.eRise : BotDirection.eFall);


                        // (BETA) ... killme
                        Debug.WriteLine(String.Format(">> Set State: {0} (Cycles={1}, Direction={2})", qItem.sMode, CurState.iRemainingCycles, CurState.dDirection));
                    }
                }
                // Ermittelt den anzuwendenden Zustand zufällig (unter Berücksichtigung der Gewichtung).
                private void SetStateEx()
                {
                    BotQueueItem qResult = new BotQueueItem(BotStateMode.Undefined);

                    if ((lStateQueue.Count == 0) && (CurProperties.iQueueChance >= UtlNumber.GetRandom(1, 100)))
                    {
                        BotStateMode sModeTmp;

                        // Vordefinierte Abfolge an Zuständen definieren.

                        // Ruhe-Phase:
                        for (int i = 0; i < UtlNumber.GetRandom(1, 2); i++)
                        {
                            sModeTmp = (BotStateMode)UtlNumber.GetRandom((int)BotStateMode.eIdleLow, (int)BotStateMode.eIdleHigh);

                            lStateQueue.Add(new BotQueueItem(sModeTmp, BotDirection.eRandom));
                        }

                        // Unruhige Phase:
                        switch (CurState.sMode)
                        {
                            case BotStateMode.eSlowStairs:
                            case BotStateMode.eExtStairs:
                                lStateQueue.Add(new BotQueueItem(BotStateMode.eStrongPeak, CurState.dDirection));
                                break;

                            case BotStateMode.eStrongPeak:
                            case BotStateMode.eHeavyPeak:
                                lStateQueue.Add(new BotQueueItem(BotStateMode.eSlowStairs, CurState.dDirection));
                                break;
                        }


                        // (BETA) ... killme
                        Debug.WriteLine(">> Triggered predefined Queue:");
                        foreach (var vState in lStateQueue)
                            Debug.WriteLine(String.Format("   - {0}", vState.sMode));
                    }

                    if (lStateQueue.Count > 0)
                    {
                        // Nächsten geplanten Zustand ausführen:
                        qResult = lStateQueue.First();

                        lStateQueue.RemoveAt(0);
                    }

                    if (qResult.sMode == BotStateMode.Undefined)
                    {
                        // Nachfolgenden Zustand ermitteln:
                        int iNumber = UtlNumber.GetRandom(1, TotalWeight);

                        foreach (var kPairTmp in dStateProperties)
                        {
                            if (iNumber > kPairTmp.Value.iWeight)
                                iNumber -= kPairTmp.Value.iWeight;
                            else
                            {
                                qResult.sMode = kPairTmp.Key;
                                break;
                            }
                        }
                    }

                    SetState(qResult);
                }
                #endregion


                private static Dictionary<BotStateMode, BotStateProperties> dStateProperties = null;
                private static List<BotQueueItem> lStateQueue = null;
            }
        }
        #endregion
        #region Types.Services
        public class BotSvcSimPlatform : BotPlatformService, IBotSimulatedPlatformService
        {
            #region Constants
            // Timer:
            public const int TMR_COUNT = 1;

            public const int TMR_ID_TICKER = 0;

            // Verhalten:
            public const BotTickerBehaviour BHV_DEFAULT = BotTickerBehaviour.eManual;
            public readonly TimeSpan BHV_CYCLE_LENGTH = new TimeSpan(0, 0, 15);

            // Wallet:
            public const double DEFAULT_BASE_CURRENCY_FUNDS = 1000;

            // PropertyGrid:
            internal const String PROP_CATEGORY_SIMULATION = "Simulation";

            internal const String PROP_NAME_INTERVAL = "Intervall";

            // DataFile:
            internal const String DATAFILE_JSON_WALLETS = "Wallets";

            internal const String DATAFILE_JSON_WALLET_CURRENCY = "Currency";
            internal const String DATAFILE_JSON_WALLET_AMOUNT = "Amount";

            internal const String DATAFILE_JSON_TICKERSIM_TIMERDELAY = "TickerSim_TimerDelay";
            internal const String DATAFILE_JSON_TICKERSIM_BEHAVIOUR = "TickerSim_Behaviour";
            internal const String DATAFILE_JSON_FILTER_FACTORCANDLE = "Filter_FactorCandle";
            internal const String DATAFILE_JSON_FILTER_FACTORTICKER = "dFilter_FactorTicker";
            #endregion


            #region Types
            // Ticker-Abonnement für Simulationen.
            public class BotSimTickerSubscription : BotTickerSubscription
            {
                public BotSimTickerSubscription(int iID, BotTradingPair tRefPair) : base(iID, tRefPair)
                {
                }


                #region Properties
                /// <summary>
                /// Manueller Kurswert.
                /// </summary>
                public double ManualValue { set; get; } = 0;
                #endregion
            }
            public class BotFilterSettings
            {
                public BotFilterSettings()
                {
                    FactorCandle = 0.1;
                    FactorTicker = 0.01;
                }


                #region Properties
                [DisplayName("Faktor Candle-Sticks")]
                [Description("Faktor zum glätten von Schwankungen in simulierten Candle-Stick-Werten.")]
                public double FactorCandle { set; get; }
                [DisplayName("Faktor Ticker")]
                [Description("Faktor zum glätten von Schwankungen in simulierten Ticker-Werten.")]
                public double FactorTicker { set; get; }
                #endregion
            }
            #endregion


            public BotSvcSimPlatform(IBotServiceProvider iProvider) : base(iProvider, string.Empty)
            {
                lLogin = new BotBaseLoginInfo();
                lPairProperties = new List<BotSimPairProperties>();
                tTimer = new System.Timers.Timer[TMR_COUNT];
                Filter = new BotFilterSettings();

                HasWebRequests = true;

                // Initialisieren:
                tTimer[TMR_ID_TICKER] = UtlTimer.CreateTimer((oSender, eArgs) => OnTimer_Ticker(this, oSender, eArgs));

                // Pairs initialisieren:
                BotTradingPair tPairBTCUSD = new BotTradingPair(BotCoinType.eBitcoin, BotCoinType.eUSDollar);
                BotTradingPair tPairLTCUSD = new BotTradingPair(BotCoinType.eLitecoin, BotCoinType.eUSDollar);
                BotTradingPair tPairIOTUSD = new BotTradingPair(BotCoinType.eIOTA, BotCoinType.eUSDollar);
                BotTradingPair tPairIOTBTC = new BotTradingPair(BotCoinType.eIOTA, BotCoinType.eBitcoin);
                BotTradingPair tPairETHUSD = new BotTradingPair(BotCoinType.eEthereum, BotCoinType.eUSDollar);
                BotTradingPair tPairDSHUSD = new BotTradingPair(BotCoinType.eDash, BotCoinType.eUSDollar);

                lPairs.Add(tPairBTCUSD);
                lPairs.Add(tPairLTCUSD);
                lPairs.Add(tPairIOTUSD);
                lPairs.Add(tPairIOTBTC);
                lPairs.Add(tPairETHUSD);
                lPairs.Add(tPairDSHUSD);

                // Wallets initialisieren:
                Wallets.Add(new BotWallet(BotCoinType.eUSDollar, BotWalletType.eExchange));

                foreach (BotTradingPair tPair in lPairs)
                {
                    if (Wallets.FindIndex(wWallet => (wWallet.Currency == tPair.CurrencyPrim)) == -1)
                        Wallets.Add(new BotWallet(tPair.CurrencyPrim, BotWalletType.eExchange));
                }

                // Standard-Eigenschaften für Kurs-Simulation definieren:
                lPairProperties.Add(new BotSimPairProperties(tPairBTCUSD));
                lPairProperties.Add(new BotSimPairProperties(tPairLTCUSD));
                lPairProperties.Add(new BotSimPairProperties(tPairIOTUSD));
                lPairProperties.Add(new BotSimPairProperties(tPairIOTBTC));
                lPairProperties.Add(new BotSimPairProperties(tPairETHUSD));
                lPairProperties.Add(new BotSimPairProperties(tPairDSHUSD));
            }


            #region Properties.CoinStream
            [Browsable(false)]
            public override BotCoinStreamPlayer CoinStreamPlayer { get { return cCoinStreamPlayer; } }
            private BotCoinStreamPlayer cCoinStreamPlayer = null;
            #endregion
            #region Properties.Config
            [Category(PROP_CATEGORY_SIMULATION)]
            [DisplayName(PROP_NAME_INTERVAL)]
            [Description("Timer zum simulieren von empfangenen Ticker-Daten.")]
            public int TimerDelay
            {
                get { return iCfgTickerSim_TimerDelay; }
                set { iCfgTickerSim_TimerDelay = UtlNumber.Limit(value, BotTickerSubscription.MIN_DELAY, BotTickerSubscription.MAX_DELAY); }
            }
            private int iCfgTickerSim_TimerDelay = 4000;
            [Category(PROP_CATEGORY_SIMULATION)]
            [DisplayName("Verhalten")]
            [Description("Verhalten von simulierten Ticker-Daten.")]
            [TypeConverter(typeof(CnvEnumDescription))]
            public BotTickerBehaviour Behaviour
            {
                get { return tCfgTickerSim_Behaviour; }
                set
                {
                    if (tCfgTickerSim_Behaviour != value)
                    {
                        bool bResult = false;
                        if ((value == BotTickerBehaviour.eCoinStream) && (cCoinStreamPlayer == null))
                        {
                            if (BotEnv.OnOpenFileDialog == null)
                                BotEnv.LogSystem.WriteDebugLog("B555", BotLogProcess.eCoinStream, "Aufrufen der File-Dialog fehlgeschlagen!", "", BotLogType.eError, BotLogPriority.eHigh);
                            else
                            {
                                string _sPath;
                                if (BotEnv.OnOpenFileDialog(BotCoinStream.RECORD_TITLE_OPEN, BotCoinStream.RECORD_FILE_EXTENT, BotCoinStream.RECORD_FILE_FILTER, true, BotEnv.Configuration.CoinStream_Directory, out _sPath))
                                {
                                    try
                                    {
                                        cCoinStreamPlayer = new BotCoinStreamPlayer(_sPath, OnCoinStreamEvent);
                                        if (!cCoinStreamPlayer.IsValid)
                                            throw new Exception("Aufnahme konnte nicht geladen werden da der Inhalt der angegebenen Datei ungültig ist.");
                                    }
                                    catch (Exception eExcpt)
                                    {
                                        cCoinStreamPlayer = null;
                                        BotEnv.LogSystem.ShowException("B168", BotLogProcess.eCoinStream, eExcpt);
                                    }

                                    if (Connection)
                                        cCoinStreamPlayer.Start();
                                    bResult = true;

                                    // Update last path:
                                    BotEnv.Configuration.CoinStream_Directory = Path.GetDirectoryName(_sPath);
                                }
                            }
                        }
                        else if (value != BotTickerBehaviour.eCoinStream)
                        {
                            StopCoinStreamPlay();
                            bResult = true;
                        }

                        if (bResult)
                        {
                            tCfgTickerSim_Behaviour = value;
                            InitStateMachine();
                        }
                    }
                }
            }
            private BotTickerBehaviour tCfgTickerSim_Behaviour = BotTickerBehaviour.eRandomFollowing;
            [Category(PROP_CATEGORY_SIMULATION)]
            [DisplayName("Filter")]
            [TypeConverter(typeof(CnvEmptyExpandableObject))]
            public BotFilterSettings Filter { set; get; }
            #endregion
            #region Properties.Simulation
            [Browsable(false)]
            public bool IsExchangeVal { get { return (tCfgTickerSim_Behaviour >= BotTickerBehaviour.ePlatform_Bitfinex); } }    // (BETA) ... [Services][Behaviour]
            #endregion
            #region Properties.Network
            [Browsable(false)]
            public override bool IsConnected { get { return (IsLoggedIn); } }
            #endregion


            #region Initialization
            public override bool ActivateService(BotServiceOwner bOwner = null)
            {
                if (base.ActivateService( bOwner))
                {
                    return (true);
                }
                else
                    return (false);
            }
            public override bool DeactivateService()
            {
                return (base.DeactivateService());
            }
            #endregion


            #region Management.CoinStream
            public bool StopCoinStreamPlay()
            {
                if (cCoinStreamPlayer == null)
                    return (false);
                else
                {
                    // Aktiven Player beenden:
                    cCoinStreamPlayer.Stop();
                    cCoinStreamPlayer = null;

                    Behaviour = BHV_DEFAULT;

                    GC.Collect();
                    return (true);
                }
            }
            #endregion
            #region Management
            public override void Reset()
            {
                // Sämtliche Timer zurücksetzen:
                for (int i = 0; i < TMR_COUNT; i++)
                    tTimer[i].Close();

                bTickerStateMachine = null;

                base.Reset();
            }
            public override BotBaseLoginInfo GetLoginInfo()
            {
                return (lLogin);
            }
            #endregion
            #region Management
            public void ResetWallet()
            {
                // Guthaben in Basis-Währung initialisieren:
                foreach (BotWallet wWallet in Wallets)
                    wWallet.Funds = (wWallet.Currency == BotConvention.TRADE_BASE_CURRENCY) ? DEFAULT_BASE_CURRENCY_FUNDS : 0;

                BotEnv.Post(() => BotEnv.OnWalletUpdated?.Invoke());
            }


            private void InitStateMachine()
            {
                if (bTickerStateMachine != null)
                    bTickerStateMachine = null;

                if (IsConnected)
                {
                    // Ggf. State-Machine erzeugen:
                    switch (tCfgTickerSim_Behaviour)
                    {
                        case BotTickerBehaviour.eRandomSmoothed:
                            bTickerStateMachine = new BotSimStateMachine.BotRandomSmoothed();
                            break;

                        case BotTickerBehaviour.eRandomFollowing:
                            bTickerStateMachine = new BotSimStateMachine.BotRandomFollowing();
                            break;

                        case BotTickerBehaviour.eRealistic:
                            bTickerStateMachine = new BotSimStateMachine.BotRealistic();
                            break;
                    }
                }

                GC.Collect();
            }

            // Simuliert empfangene Daten für alle eingerichteten Abonnements des angegebenen Typs 'sType'.
            private void SimulateSubscriptionData(BotSubscriptionType sType)
            {
                BotBaseSubscription tSubscrTmp;

                for (int i = 0; i < lSubscriptions.Count; i++)
                {
                    tSubscrTmp = lSubscriptions[i];

                    if (tSubscrTmp.GetSubscriptionType() == sType)
                    {
                        // Daten für entsprechenden Subscription-Typ simulieren:
                        switch (sType)
                        {
                            case BotSubscriptionType.eTicker:
                                if (IsExchangeVal)
                                    // Simulation von Werten aussetzen:
                                    continue;
                                else
                                {
                                    BotSimTickerSubscription tTicker = (BotSimTickerSubscription)tSubscrTmp;
                                    BotSimPairProperties sPropertiesTmp = FindSimProperties(tTicker.ReferredPair);

                                    if (sPropertiesTmp == null)
                                        Debug.Assert(false, "[SSD183]");
                                    else
                                    {
                                        BotSimTickerSubscription.BotTickerData tData = new BotSimTickerSubscription.BotTickerData();

                                        if (tTicker.lData.Count > 0)
                                            // Reale Abstände zwischen einzelnen Subscription-Datensätzen simulieren:
                                            tData.dReceivedTime = (tTicker.lData.Last().dReceivedTime + BHV_CYCLE_LENGTH);

                                        tTicker.lData.Add(tData);

                                        if (tCfgTickerSim_Behaviour == BotTickerBehaviour.eManual)
                                            // Manuellen Wert anwenden:
                                            tData.dLastPrice = tTicker.ManualValue;

                                        else if (bTickerStateMachine != null)
                                            bTickerStateMachine.SimulateTickerData(tTicker, sPropertiesTmp, Filter.FactorTicker);

                                        else
                                            // Ticker-Daten wurden empfangen.
                                            // > Notwendige Informationen simulieren:
                                            tData.dLastPrice = UtlNumber.GetRandom(sPropertiesTmp.dMinVal, sPropertiesTmp.dMaxVal);
                                    }
                                }

                                break;

                            default:
                                // (BETA) ...
                                Debug.Assert(false, "[SSD133] Unbehandelter Subscription-Type!");
                                break;
                        }

                        Owner.OnSubscriptionData(BotSubscriptionPurpose.eRegular, tSubscrTmp);
                    }
                }
            }
            // Simuliert eine Buchung (positiv oder negativ) für eine Wallet.
            private bool SimulateWalletUpdate(double dAmount, BotCoinType cCurrency, BotWalletType wType = BotWalletType.eExchange)
            {
                BotWallet wWallet = GetWallet(cCurrency, wType);

                if (wWallet == null)
                {
                    Debug.Assert(false, "[SWU322]");
                    return (false);
                }
                else
                {
                    wWallet.Funds += dAmount;

                    Owner.OnWalletUpdated(wWallet);
                    return (true);
                }
            }

            internal BotSimPairProperties FindSimProperties(BotTradingPair tPair)
            {
                return (lPairProperties.FirstOrDefault(_bProp => (_bProp.tRefPair == tPair)));
            }
            #endregion


            #region Events.CoinStream
            public BotTaskResult OnCoinStreamEvent(BotCoinStreamPlayer.BotEventType eType, BotBasePairSubscription bSubscription)
            {
                BotTaskResult tResult = Owner.OnCoinStreamEvent(eType, bSubscription);
                if (tResult == BotTaskResult.eError)
                    StopCoinStreamPlay();
                return (tResult);
            }
            #endregion


            #region Communication.Commands
            public override bool Login()
            {
                // Erfolgreichen Login simuieren:
                lLogin.bLoggedIn = true;

                Owner.OnLoggedIn();

                return (true);
            }
            public override bool PlaceOrder(BotTradingPair tPair, double dAmount, BotBaseOrder bOrder = null)
            {
                BotSimOrder sOrder = new BotSimOrder();

                if (BotDebugMode.ConstantOrders)
                {
                    // Konstante (d.h. immer die gleichen) Orders simulieren:
                    if (dAmount > 0)
                    {
                        dAmount = 0.1;
                        sOrder.dRate = 6600;
                    }
                    else
                    {
                        dAmount = -0.1;
                        sOrder.dRate = 6800;
                    }
                }
                else
                    // Aktuellen Preis anwenden:
                    sOrder.dRate = tPair.LastRateValue;

                return (base.PlaceOrder(tPair, dAmount, sOrder));
            }
            public override bool CancelOrder(BotBaseOrder bOrder)
            {
                return (oOrders.Remove(bOrder));
            }

            public override bool GetPlatformSpecific()
            {
                BotTradingPair tPair = FindPair(BotCoinType.eBitcoin, BotCoinType.eUSDollar);

                if (tPair != null)
                {
                    // Vorgaben simulieren:
                    tPair.Precision = 5;
                    tPair.MinOrderSize = 0.004;
                    tPair.MaxOrderSize = 2000.0;
                }

                return (true);
            }

            public override bool SubscribeTicker(BotTradingPair tPair)
            {
                if (FindPairSubscription(tPair, BotSubscriptionType.eTicker) == null)
                {
                    Debug.Assert((tPair != null), "[ST76]");

                    // Ticker-Subscription erzeugen:
                    BotSimTickerSubscription sTicker = new BotSimTickerSubscription(GenerateUniqueID(), tPair);

                    lSubscriptions.Add(sTicker);
                    Owner.OnSubscribed(sTicker);

                    // Currency-Bewertung ermitteln:
                    IBotCoinInfo cInfo;
                    if (BotCurrencyRating.GetLatestCoinInfo(tPair.CurrencyPrim, out cInfo))
                    {
                        // Simulierten Ticker mit aktuellstem Kurs-Wert initialisieren:
                        sTicker.ManualValue = cInfo.Price;

                        // Eigenschaften für Simulation initialisieren:
                        BotSimPairProperties sPropertiesTmp = FindSimProperties(tPair);

                        if (sPropertiesTmp == null)
                            Debug.Assert(false, "[SSD183]");
                        else
                        {
                            // Bereich (abhängig von aktuellem Kurs) definieren indem Ticker-Werte simuliert werden:
                            double dRange = (cInfo.Price * BotSimPairProperties.SIMVAL_LIMIT_FACTOR);

                            sPropertiesTmp.dMinVal = (cInfo.Price - dRange);
                            sPropertiesTmp.dMaxVal = (cInfo.Price + dRange);
                        }
                    }

                    // Timer zum Simulieren von Subscription-Daten starten:
                    UtlTimer.ModifyTimer(tTimer[TMR_ID_TICKER], 500);

                    return (true);
                }
                else
                    return (false);
            }

            public override bool SubscribeCandles(BotTradingPair tPair, TimeSpan tInterval)
            {
                Debug.Assert((tPair != null), "[SC128]");

                BotSimPairProperties sPropertiesTmp = FindSimProperties(tPair);

                if (sPropertiesTmp == null)
                {
                    Debug.Assert(false, "[SC135]");
                    return (false);
                }
                else
                {
                    // Candle-Subscription erzeugen:
                    BotCandleSubscription cCandle = new BotCandleSubscription(GenerateUniqueID(), tPair, tInterval);

                    lSubscriptions.Add(cCandle);
                    Owner.OnSubscribed(cCandle);

                    // Intervall wiederholender Zeitstempel definieren:
                    TimeSpan tToday = DateTime.Now.Subtract(new DateTime(1970, 1, 9, 0, 0, 00));
                    TimeSpan tStampTmp = new TimeSpan(tToday.Days, tToday.Hours, 0, 0);

                    // Subscription-Daten simulieren:
                    BotCandleSubscription.BotCandleData cData;
                    List<double> lSimValues = new List<double>();
                    double dValTmp;
                    bool bRisingTmp;

                    for (int i = 0; i < 200; i++)
                    {
                        cData = new BotCandleSubscription.BotCandleData();

                        // Zufällige Werte generieren und aufsteigend sortieren:
                        bRisingTmp = UtlNumber.GetRandom();

                        for (int u = 0; u < 4; u++)
                        {
                            dValTmp = UtlNumber.GetRandom(sPropertiesTmp.dMinVal, sPropertiesTmp.dMaxVal);

                            if (i == 0)
                                lSimValues.Add(dValTmp);
                            else
                                // Simulierten Wert glätten:
                                lSimValues[u] = UtlMath.FilterTP1(lSimValues[u], dValTmp, Filter.FactorCandle);
                        }

                        lSimValues.Sort();

                        // Notwendige Informationen simulieren:
                        cData.dOpen = lSimValues[bRisingTmp ? 1 : 2];
                        cData.dClose = lSimValues[bRisingTmp ? 2 : 1];
                        cData.dHigh = lSimValues[3];
                        cData.dLow = lSimValues[0];
                        cData.dVolume = ((cData.dHigh - cData.dLow) / 3);
                        cData.tTimeStamp = tStampTmp;

                        // Datensätze in umgekehrter Reinhenfolge aufnehmen:
                        // > Somit wird die Reihenfolge (nach Timestamp) aufsteigend sortiert ([0] = Ältestes Ereignis, [1] ...).
                        // > Sortierung würde dann auch zu nachfolgenden Subscription-Updates passen!
                        cCandle.lData.Insert(0, cData);

                        if (tStampTmp.TotalHours == 0)
                            tStampTmp = (new TimeSpan(1, 0, 0, 0) - tInterval);
                        else
                            tStampTmp = (tStampTmp - tInterval);
                    }

                    lSimValues.Clear();

                    Owner.OnSubscriptionData(BotSubscriptionPurpose.eRegular, cCandle);
                    return (true);
                }
            }
            public override bool Unsubscribe(int iChannelID)
            {
                BotBaseSubscription bSubscription = FindSubscription(iChannelID);

                if (bSubscription != null)
                {
                    lSubscriptions.Remove(bSubscription);
                    return (true);
                }
                else
                    return (false);
            }
            public override void UseSubscriptionWebRequest(BotSubscriptionWebRequest sWebRequest)
            {
                if ((HasWebRequests) && (IsExchangeVal))
                {
                    dynamic dData = JsonConvert.DeserializeObject<dynamic>(sWebRequest.Received);

                    if (dData != null)
                        // Ersten Datensatz ermitteln:
                        dData = dData[0];

                    if (dData != null)
                    {
                        switch (sWebRequest.Type)
                        {
                            case BotSubscriptionType.eTicker:
                                BotTradingPair tPairTmp;
                                BotTickerSubscription.BotTickerData tData;
                                BotBasePairSubscription bSubscription;

                                // Ticker-Daten auslesen;
                                while (dData != null)
                                {
                                    tPairTmp = FindPair((String)dData[0], false);

                                    if (tPairTmp == null)
                                    {
                                        // Abonniertes Pair wurde nicht gefunden!
                                        // > Das kann passieren wenn andere Platform ein Pärchen abboniert welches nicht von aktueller Platform unterstüzt wird.
                                    }
                                    else
                                    {
                                        tData = new BotTickerSubscription.BotTickerData();

                                        UtlParse.Double(dData[1], out tData.dBid);
                                        UtlParse.Double(dData[2], out tData.dBidSize);
                                        UtlParse.Double(dData[3], out tData.dAsk);
                                        UtlParse.Double(dData[4], out tData.dAskSize);
                                        UtlParse.Double(dData[5], out tData.dDailyChange);
                                        UtlParse.Double(dData[6], out tData.dDailyChangePerc);
                                        UtlParse.Double(dData[7], out tData.dLastPrice);
                                        UtlParse.Double(dData[8], out tData.dVolume);
                                        UtlParse.Double(dData[9], out tData.dHigh);
                                        UtlParse.Double(dData[10], out tData.dLow);

                                        // Auswertung:
                                        bSubscription = FindPairSubscription(tPairTmp);

                                        if (bSubscription != null)
                                        {
                                            // Daten zu Subscription hinzufügen:
                                            bSubscription.AddData(tData);

                                            Owner.OnSubscriptionData(BotSubscriptionPurpose.eRegular, bSubscription);
                                        }
                                    }

                                    dData = dData.Next;
                                }

                                break;

                            default:
                                // (BETA) ...
                                Debug.Assert(false, "[EWR310] Funktion noch nicht implementiert!");
                                break;
                        }
                    }
                }
            }

            // Übernimmt einen Betrag für die angegebene Wallet zu Simulationszwecken.
            public bool SetWalletAmount(double dAmount, BotCoinType cCurrency, BotWalletType wType = BotWalletType.eExchange)
            {
                BotWallet wWallet = GetWallet(cCurrency, wType);

                if (wWallet == null)
                {
                    BotEnv.LogSystem.WriteLog("SWA233", BotLogProcess.eWallet, ("Betrag übernehmen fehlgeschlagen: Wallet '" + UtlEnum.GetEnumDescription(cCurrency) + "' existiert nicht!"), BotLogType.eError);
                    return (false);
                }
                else
                {
                    wWallet.Funds = dAmount;
                    return (true);
                }
            }
            // Wendet einen manuellen Kurs-Wert an:
            public bool SetManualTickerSimValue(BotTradingPair tPair, double dValue)
            {
                // Subscription ermitteln:
                BotSimTickerSubscription tTicker = (BotSimTickerSubscription)FindPairSubscription(tPair, BotSubscriptionType.eTicker);
                if (tTicker != null)
                {
                    tTicker.ManualValue = dValue;
                    return (true);
                }
                else
                    return (false);
            }


            protected override bool OnSendNewOrder(BotBaseOrder bOrder)
            {
                BotCoinStreamPlayer cPlayer = CoinStreamPlayer;

                if ((cPlayer != null) &&
                    (bOrder.tRefPair.Compare(cPlayer.TradingPair)) && (cPlayer.LastTickerSubscription != null))
                {
                    // Zeitstempel von zuletzt aus Coin-Stream Replay geladenem Ticker anwenden:
                    if (((BotSimOrder)bOrder).CalcCreated(cPlayer.LastTickerSubscription) == false)
                        return (false);
                }

                // Erzeugte Order simulieren:
                Owner.OnOrderCreated(bOrder);

                // Gebühr simulieren:
                double iFee = 0.0008;

                if (bOrder.IsBuyOrder)
                    bOrder.AddFee((bOrder.dAmount * iFee), bOrder.tRefPair.CurrencyPrim);
                else
                    bOrder.AddFee((bOrder.Price * iFee), bOrder.tRefPair.CurrencySec);

                // Wallet-Update simulieren:
                SimulateWalletUpdate(bOrder.FinalAmount, bOrder.tRefPair.CurrencyPrim);
                SimulateWalletUpdate(bOrder.FinalPrice, bOrder.tRefPair.CurrencySec);

                // Abgeschlossene Order simulieren:
                bOrder.oState = BotOrderState.eExecuted;

                Owner.OnOrderClosed(bOrder, BotOrderResult.eExecuted);

                // Speicher freigeben:
                oOrders.Remove(bOrder);
                bOrder = null;

                return (true);
            }
            #endregion
            #region Communication.Initialization
            protected override bool Connect()
            {
                // Erfolgreichen Verbindungsaufbau simulieren:
                Owner.OnSocketConnected();

                InitStateMachine();

                return (true);
            }
            #endregion


            #region Configuration
            public override void WriteFile(JsonWriter jWriter, BotContentType bContent)
            {
                base.WriteFile(jWriter, bContent);
                {
                    if (bContent == BotContentType.eConfig)
                    {
                        jWriter.WritePropertyName(DATAFILE_JSON_TICKERSIM_TIMERDELAY);
                        jWriter.WriteValue(TimerDelay);

                        jWriter.WritePropertyName(DATAFILE_JSON_TICKERSIM_BEHAVIOUR);
                        jWriter.WriteValue(tCfgTickerSim_Behaviour);

                        jWriter.WritePropertyName(DATAFILE_JSON_FILTER_FACTORCANDLE);
                        jWriter.WriteValue(Filter.FactorCandle);

                        jWriter.WritePropertyName(DATAFILE_JSON_FILTER_FACTORTICKER);
                        jWriter.WriteValue(Filter.FactorTicker);

                        // Wallet-Informationen speichern:
                        jWriter.WritePropertyName(DATAFILE_JSON_WALLETS);
                        jWriter.WriteStartArray();
                        {
                            for (int i = 0; i < Wallets.Count; i++)
                            {
                                jWriter.WriteStartObject();
                                {
                                    jWriter.WritePropertyName(DATAFILE_JSON_WALLET_CURRENCY);
                                    jWriter.WriteValue(Wallets[i].Currency);

                                    jWriter.WritePropertyName(DATAFILE_JSON_WALLET_AMOUNT);
                                    jWriter.WriteValue(Wallets[i].Funds);

                                    jWriter.WriteEndObject();
                                }
                            }

                            jWriter.WriteEnd();
                        }
                    }

                    jWriter.WriteEndObject();
                }
            }
            public override bool ReadFile(Newtonsoft.Json.Linq.JObject dObjects, BotContentType fMode)
            {
                if (base.ReadFile(dObjects, fMode))
                {
                    switch (fMode)
                    {
                        case BotContentType.eConfig:
                            TimerDelay = dObjects.ParseValue(DATAFILE_JSON_TICKERSIM_TIMERDELAY, TimerDelay);
                            tCfgTickerSim_Behaviour = dObjects.ParseValue(DATAFILE_JSON_TICKERSIM_BEHAVIOUR, tCfgTickerSim_Behaviour);
                            Filter.FactorCandle = dObjects.ParseValue(DATAFILE_JSON_FILTER_FACTORCANDLE, Filter.FactorCandle);
                            Filter.FactorTicker = dObjects.ParseValue(DATAFILE_JSON_FILTER_FACTORTICKER, Filter.FactorTicker);

                            if (tCfgTickerSim_Behaviour == BotTickerBehaviour.eCoinStream)
                                // Standard-Wert anwenden:
                                tCfgTickerSim_Behaviour = BHV_DEFAULT;

                            // Wallet-Informationen lesen:
                            dynamic bWallets = dObjects[DATAFILE_JSON_WALLETS];

                            if ((bWallets != null) && (bWallets.Count > 0))
                            {
                                BotWallet wWalletTmp;

                                for (int i = 0; i < bWallets.Count; i++)
                                {
                                    wWalletTmp = GetWallet((BotCoinType)bWallets[i][DATAFILE_JSON_WALLET_CURRENCY], BotWalletType.eExchange);

                                    if (wWalletTmp != null)
                                        wWalletTmp.Funds = bWallets[i][DATAFILE_JSON_WALLET_AMOUNT];
                                }
                            }

                            return (true);

                        case BotContentType.eRiftSummary:
                            return (true);
                    }
                }

                return (false);
            }


            private BotSimStateMachine.BotBase bTickerStateMachine;

            private BotBaseLoginInfo lLogin;
            private List<BotSimPairProperties> lPairProperties;

            private System.Timers.Timer[] tTimer;

            private int iRunningID = 100;
            #endregion


            #region Timer
            private static void OnTimer_Ticker(BotSvcSimPlatform bOwner, object oSender, ElapsedEventArgs eArgs)
            {
                bOwner.tTimer[TMR_ID_TICKER].Stop();
                {
                    if (bOwner.IsExchangeVal == false)
                        // Ticker-Daten simulieren:
                        bOwner.SimulateSubscriptionData(BotSubscriptionType.eTicker);
                }
                UtlTimer.ModifyTimer(bOwner.tTimer[TMR_ID_TICKER], bOwner.TimerDelay);
            }
            #endregion


            #region Helper
            // Generiert eine einzigartige, fortlaufende ID.
            private int GenerateUniqueID()
            {
                return (iRunningID++);
            }
            #endregion
        }
        #endregion
    }
}
