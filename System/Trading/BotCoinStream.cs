﻿using CryptoRiftBot;
using CryptoRiftBot.Trading.Services;
using CryptoRiftBot.Trading.Strategy;
using CryptoRiftBot.Trading;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Timers;
using System.IO;
using Shared.Utils.Core;
using Shared.IO;
using System.Linq;
using System.IO.Compression;

namespace CryptoRiftBot
{
    // Enthält empfangene Informationen (Ticker, Candle-Sticks) zu Währungen.
    public class BotCoinStream
    {
        #region Constants
        // Datei:
        public const String RECORD_TITLE_OPEN = (RECORD_FILE_NAME + " öffnen.");
        public const String RECORD_TITLE_SAVE = (RECORD_FILE_NAME + " speichern.");
        public const String RECORD_FILE_NAME = "Coin-Stream Record";
        public const String RECORD_FILE_EXTENT = "csr";
        public static readonly String RECORD_FILE_FILTER = String.Format("{0} Dateien|*.{1}", RECORD_FILE_NAME, RECORD_FILE_EXTENT);

        internal static readonly bool PACKAGE_COMPRESS_DATA = true;
        internal static readonly int PACKAGE_ID_HEADER = 100;
        internal static readonly int PACKAGE_ID_CANDLE_PREVIEW = 101;
        internal static readonly int PACKAGE_ID_DATA = 102;
        internal static readonly TimeSpan PACKAGE_CANDLE_PREVIEW_INTERVAL = new TimeSpan(1, 0, 0);

        // Versionen:
        public const uint VER_FIRST = VER_1;
        public const uint VER_CURRENT = VER_3;

        public const uint VER_1 = 1;
        public const uint VER_2 = 2;
        public const uint VER_3 = 3;
        #endregion


        #region Structures
        public interface IFileHeader
        {
            #region Properties
            uint Version { get; }
            BotCoinType CurrencyPrimary { get; }
            BotCoinType CurrencySecondary { get; }
            public string Description { set; get; }
            #endregion
        }
        protected struct FileHeader : IFileHeader
        {
            #region Constants
            private const string CATEGORY_FILE = "Datei";
            private const string CATEGORY_COINSTREAM = "Coin-Stream";
            #endregion


            public FileHeader(BotTradingPair bPair, DateTime dCreated)
            {
                Debug.Assert((bPair != null), "[FH60]");

                this.DataCount = 0;
                this.Version = VER_CURRENT;
                this.Pair = bPair;
                this.Description = string.Empty;
                this.Created = dCreated;
                this.Duration = TimeSpan.Zero;
            }


            #region Properties.Management
            [Browsable(false)]
            public bool IsValid { get { return ((DataCount > 0) && (Duration != TimeSpan.Zero)); } }
            [Browsable(false)]
            public long DataCount { set; get; }
            [Browsable(false)]
            public BotTradingPair Pair { set; get; }
            #endregion
            #region Properties
            [Browsable(false)]
            public BotCoinType CurrencyPrimary { get { return (Pair.CurrencyPrim); } }
            [Browsable(false)]
            public BotCoinType CurrencySecondary { get { return (Pair.CurrencySec); } }
            #endregion
            #region Properties
            [Category(CATEGORY_FILE)]
            [DisplayName("Version")]
            [ReadOnly(true)]
            public uint Version { set; get; }

            [Category(CATEGORY_COINSTREAM)]
            [DisplayName("Symbol")]
            [ReadOnly(true)]
            public string Symbol { get { return (Pair.Symbol); } }
            [Category(CATEGORY_COINSTREAM)]
            [DisplayName("Beschreibung")]
            public string Description { set; get; }
            [Category(CATEGORY_COINSTREAM)]
            [DisplayName("Erstellt am")]
            [ReadOnly(true)]
            public DateTime Created { set; get; }
            [Category(CATEGORY_COINSTREAM)]
            [DisplayName("Dauer")]
            [ReadOnly(true)]
            public TimeSpan Duration { set; get; }
            #endregion


            #region Management
            public void Write(Stream fStream)
            {
                using (BinaryWriter bWriter = new BinaryWriter(fStream, PackageFileStream.ENCODING, true))
                {
                    bWriter.Write(Version);
                    bWriter.Write((int)CurrencyPrimary);
                    bWriter.Write((int)CurrencySecondary);
                    bWriter.Write(Description);
                    bWriter.Write(Created.Ticks);
                    bWriter.Write(Duration.Ticks);
                    bWriter.Write(DataCount);
                }
            }
            public static FileHeader Read(Stream fStream)
            {
                using (BinaryReader bReader = new BinaryReader(fStream, PackageFileStream.ENCODING, true))
                {
                    FileHeader fResult = new FileHeader()
                    {
                        Version = bReader.ReadUInt32(),
                        Pair = new BotTradingPair((BotCoinType)bReader.ReadInt32(), (BotCoinType)bReader.ReadInt32()),
                        Description = bReader.ReadString(),
                        Created = new DateTime(bReader.ReadInt64()),
                        Duration = new TimeSpan(bReader.ReadInt64()),
                        DataCount = bReader.ReadInt64()
                    };

                    if ((VER_FIRST <= fResult.Version) && (fResult.Version <= VER_CURRENT))
                    {
                        // [Archiv]
                        /*
                        if (fResult.Version == VER_1)
                        {
                            // Verschieben um 1, da Currency 'EUR' hinzugefügt wurde:
                            fResult.CurrencyPrimary += 1;
                            fResult.CurrencyPrimary += 1;
                        }
                        */
                        return (fResult);
                    }
                    else
                        throw new InvalidDataException("Invalid header!");
                }
            }
            #endregion
        }
        #endregion


        protected BotCoinStream(BotTradingPair tRefPair)
        {
            this.TradingPair = tRefPair;
        }
        ~BotCoinStream()
        {
        }


        #region Properties.Management
        [Browsable(false)]
        public virtual bool IsValid { get { return ((TradingPair != null) && (TradingPair.IsValid)); } }
        #endregion
        #region Properties.References
        [Browsable(false)]
        public BotTradingPair TradingPair { protected set; get; }
        #endregion


        #region IO
        public virtual bool Close()
        {
            if (IsValid)
            {
                GC.Collect();

                return (true);
            }
            else
                return (false);
        }
        /// <summary>
        /// Reads a preview from a coin-stream file.
        /// </summary>
        /// <param name="sFilePath"></param>
        /// <param name="iHeader"></param>
        /// <param name="bCandlePreview"></param>
        public static bool ReadFilePreview(string sFilePath, out IFileHeader iHeader, out BotCandleSubscription bCandlePreview)
        {
            try
            {
                // Read packages from file:
                var pPackStream = new PackageFileStream<BinaryReader>();
                pPackStream.ReadPackages(sFilePath, new int[] { PACKAGE_ID_HEADER, PACKAGE_ID_CANDLE_PREVIEW });

                // Read header:
                iHeader = FileHeader.Read(pPackStream.GetPackage(PACKAGE_ID_HEADER).CurrentStream);

                // Read preview:
                bCandlePreview = new BotCandleSubscription(new BotTradingPair(iHeader.CurrencyPrimary, iHeader.CurrencySecondary));
                bCandlePreview.Read(BotSubscriptionPurpose.eCoinStream, pPackStream.GetPackage(PACKAGE_ID_CANDLE_PREVIEW).Modifier);

                return (true);
            }
            catch (Exception)
            {
                iHeader = null;
                bCandlePreview = null;
                return (false);
            }
        }
        #endregion


        protected FileHeader fHeader;
    }

    // Speichert einen Coin-Stream in einer neuen Datei.
    public class BotCoinStreamRecorder : BotCoinStream
    {
        public BotCoinStreamRecorder(BotTradingStrategy bRefStrategy) : base(bRefStrategy.TradingPair)
        {
            this.fHeader = new FileHeader(TradingPair, DateTime.Now);
            this.pData = new Package<BinaryWriter>(PACKAGE_COMPRESS_DATA, PACKAGE_ID_DATA);
            this.bCandlePreview = new BotCandleSubscription(0, TradingPair, PACKAGE_CANDLE_PREVIEW_INTERVAL);

            this.bRefStrategy = bRefStrategy;
            this.dFirstWrite = DateTime.Now;
            this.dLastWrite = default(DateTime);

            if (IsValid)
            {
                lInstances.Add(this);

                // Aktuelle Candle-Subscription schreiben:
                Update((BotBasePairSubscription)BotCurrencyRating.GetCandles(TradingPair));
            }
        }
        ~BotCoinStreamRecorder()
        {
        }


        #region Properties.Management
        [Browsable(false)]
        public override bool IsValid { get { return ((base.IsValid) && (bRefStrategy != null) && (pData != null)); } }
        #endregion


        #region IO
        public override bool Close()
        {
            lInstances.Remove(this);

            if (IsValid)
            {
                // Finalize:
                fHeader.Duration = (dLastWrite - dFirstWrite);

                string _sPath;
                object[] _oResult;
                if (BotEnv.OnSaveFileDialogEx(RECORD_TITLE_SAVE, BotCoinStream.RECORD_FILE_EXTENT, BotCoinStream.RECORD_FILE_FILTER, GenerateFilePath(bRefStrategy), out _sPath, out _oResult, new object[] { fHeader, bCandlePreview }))
                {
                    // Apply (may changed) result:
                    fHeader = (FileHeader)_oResult[0];

                    // Write header:
                    var pHeader = new Package(false, PACKAGE_ID_HEADER);
                    fHeader.Write(pHeader.CurrentStream);

                    // Write candle-preview:
                    var pCandlePrev = new Package<BinaryWriter>(PACKAGE_COMPRESS_DATA, PACKAGE_ID_CANDLE_PREVIEW);
                    bCandlePreview.Write(BotSubscriptionPurpose.eCoinStream, pCandlePrev.Modifier);

                    // Write packages to file:
                    PackageFileStream pStream = new PackageFileStream();

                    pStream.Packages.Add(pHeader);
                    pStream.Packages.Add(pCandlePrev);
                    pStream.Packages.Add(pData);

                    pStream.WritePackages(_sPath);

                    // Update last path:
                    BotEnv.Configuration.CoinStream_Directory = Path.GetDirectoryName(_sPath);
                }
            }

            // Clear datasets:
            bCandlePreview = null;
            pData = null;

            return (base.Close());
        }
        #endregion
        #region Management
        public static void Update(BotCandleSubscription bSubscription)
        {
            // Sämtliche betroffenen Instanzen aktualisieren:
            lInstances
                .Where(_bInst => bSubscription.ReferredPair.Compare(_bInst.TradingPair))
                .ForEach(_bInst =>
                {
                    lock (_bInst.pData)
                        _bInst.Update((BotBasePairSubscription)bSubscription);
                });
        }
        #endregion
        #region Management
        public bool Update(BotBasePairSubscription bSubscription)
        {
            if (IsValid)
            {
                lock (pData)
                    lock (bSubscription.oThreadLockObj)
                    {
                        BotSubscriptionType sType = bSubscription.GetSubscriptionType();
                        try
                        {
                            object oParam = null;

                            // Initialize parameter:
                            switch (bSubscription)
                            {
                                case BotCandleSubscription _bCandleSubscr:
                                    BotCandleSubscription.BotCandleData _bLastCandle = _bCandleSubscr.lData.Last();

                                    // Filter new candles:
                                    oParam = tLastCandle;

                                    // Save last candle's timestamp:
                                    tLastCandle = _bLastCandle.TimeStamp;

                                    // Update candle preview:
                                    if (bCandlePreview.lData.Count == 0)
                                        // Initialize:
                                        bCandlePreview.AddData(_bLastCandle);
                                    else
                                        bCandlePreview.AddData(_bCandleSubscr);

                                    break;
                            }

                            // Meta-Daten schreiben:
                            DateTime dCurTime = DateTime.Now;
                            long lDelay = ((fHeader.DataCount == 0) ? 0 : dCurTime.Subtract(dLastWrite).Ticks);

                            pData.Modifier.Write(lDelay);
                            pData.Modifier.Write((int)sType);

                            // Subscription-Daten schreiben:
                            bSubscription.Write(BotSubscriptionPurpose.eCoinStream, pData.Modifier, oParam);

                            dLastWrite = dCurTime;
                            fHeader.DataCount++;

                            return (true);
                        }
                        catch (IOException iExcpt)
                        {
                            Console.WriteLine("BinaryWriter.Except [W128] {0}", iExcpt.Message);
                        }
                    }
            }
            return (false);
        }
        #endregion


        #region Helper
        private static String GenerateFilePath(BotTradingStrategy bRefStrategy)
        {
            string _sPath = BotEnv.Configuration.CoinStream_Directory;

            // Ggf. Verzeichnis erzeugen:
            Directory.CreateDirectory(_sPath);

            // Dateipfad ausgeben:
            return (String.Format("{0}\\{1}.{2}.{3:yyyy-MM-dd_HH-mm-ss}.{4}", _sPath, bRefStrategy.Platform.PlatformName, bRefStrategy.TradingPair.Symbol, DateTime.Now, RECORD_FILE_EXTENT));
        }
        #endregion


        private Package<BinaryWriter> pData;
        private BotCandleSubscription bCandlePreview;

        private BotTradingStrategy bRefStrategy;
        private DateTime dFirstWrite, dLastWrite;
        private TimeSpan tLastCandle = TimeSpan.Zero;

        private static List<BotCoinStreamRecorder> lInstances = new List<BotCoinStreamRecorder>();
    }
    // Läd einen Coin-Stream aus einer bestehenden Datei und spielt diesem ab.
    public class BotCoinStreamPlayer : BotCoinStream
    {
        #region Constants
        public const double TIMER_DELAY_RETRY = 100;
        public const double TIMER_DELAY_PLAY_NORMAL = 10;
        #endregion


        #region Delegates
        public delegate BotTaskResult EvaluateEventHandler(BotEventType eType, BotBasePairSubscription bSubscription = null);
        #endregion


        #region Enumerations
        public enum BotEventType
        {
            eEventOccured,
            eEndOfFile,
            eError,
        }
        // Enthält Vorgänge in der korrekten, im Coin-Stream Record gespeicherten, Reihenfolge.
        public enum BotOperation
        {
            Invalid = 0,

            eReadDelay = 1,
            eReadEvent = 2
        }
        #endregion


        #region Types
        public class BotRiftDetails
        {
            public BotRiftDetails()
            {
                Reset();
            }


            #region Management
            public void Reset()
            {
                Prophit = 0;
                BuyCount = 0;
                SellCount = 0;
                GoodRiftsCount = 0;
                BadRiftsCount = 0;
            }
            #endregion


            #region Properties
            [Browsable(false)]
            public double Prophit { set; get; }
            [Browsable(false)]
            public int BuyCount { set; get; }
            [Browsable(false)]
            public int SellCount { set; get; }
            [Browsable(false)]
            public int GoodRiftsCount { set; get; }
            [Browsable(false)]
            public int BadRiftsCount { set; get; }
            [Browsable(false)]
            public int TotalRiftsCount { get { return (GoodRiftsCount + BadRiftsCount); } }
            #endregion
        }
        public class BotEvent
        {
            public BotEvent(BinaryReader bDataReader, BotTradingPair tRefPair)
            {
                Type = (BotSubscriptionType)bDataReader.ReadInt32();
                switch (Type)
                {
                    case BotSubscriptionType.eTicker:
                        Subscription = (BotBasePairSubscription)new BotTickerSubscription(0, tRefPair);
                        break;

                    case BotSubscriptionType.eCandles:
                        Subscription = (BotBasePairSubscription)new BotCandleSubscription(tRefPair);
                        break;

                    default:
                        Debug.Assert(false, "[E242] Ungültiger Subscription-Typ!");
                        return;
                }

                // Subscription auslesen:
                Subscription.Read(BotSubscriptionPurpose.eCoinStream, bDataReader);
            }
            ~BotEvent()
            {
                Subscription = null;
            }


            #region Properties.Management
            [Browsable(false)]
            public bool IsValid { get { return ((Type != BotSubscriptionType.Invalid) && (Subscription != null)); } }
            #endregion
            #region Properties
            [Browsable(false)]
            public BotSubscriptionType Type { private set; get; }
            [Browsable(false)]
            public BotBasePairSubscription Subscription { private set; get; }
            #endregion
        }
        #endregion


        public BotCoinStreamPlayer(String sFilePath, EvaluateEventHandler OnEvaluate) : base(null)
        {
            Debug.Assert((OnEvaluate != null), "[CSP217]");

            // Read packages from file:
            var pPackStream = new PackageFileStream<BinaryReader>();
            pPackStream.ReadPackages(sFilePath, new int[] { PACKAGE_ID_HEADER, PACKAGE_ID_DATA });

            // Read header:
            fHeader = FileHeader.Read(pPackStream.GetPackage(PACKAGE_ID_HEADER).CurrentStream);
            if (fHeader.IsValid)
            {
                pData = pPackStream.GetPackage(PACKAGE_ID_DATA);

                // Initialize:
                this.OnEvaluate = OnEvaluate;
                this.oNextOp = BotOperation.Invalid;

                this.FileTitle = Path.GetFileNameWithoutExtension(sFilePath);
                this.TradingPair = new BotTradingPair(fHeader.CurrencyPrimary, fHeader.CurrencySecondary);
                this.RiftSummary = new BotRiftSummary(TradingPair);
            }
        }
        ~BotCoinStreamPlayer()
        {
            Reset();
        }


        #region Properties.Management
        [Browsable(false)]
        public override bool IsValid { get { return ((base.IsValid) && (pData != null) && (fHeader.DataCount > 0)); } }
        #endregion
        #region Properties.Management
        [Browsable(false)]
        public BotRiftSummary RiftSummary { private set; get; }
        [Browsable(false)]
        public bool IsBeginOfFile { get { return (lNextDataIndex == 0); } }
        [Browsable(false)]
        public bool IsEndOfFile { get { return (lNextDataIndex >= fHeader.DataCount); } }
        [Browsable(false)]
        public BotCandleSubscription LastCandleSubscription { private set; get; }
        [Browsable(false)]
        public BotTickerSubscription LastTickerSubscription { private set; get; }
        #endregion
        #region Properties
        [Browsable(false)]
        public String FileTitle { private set; get; }
        [Browsable(false)]
        public bool IsPaused
        {
            get { return (bIsPaused); }
            set
            {
                bIsPaused = value;
                if (tNextEventTimer != null)
                    // Event wiederholen:
                    UtlTimer.ModifyTimer(tNextEventTimer, TIMER_DELAY_RETRY);
            }
        }
        [Browsable(false)]
        public long FrameCount { get { return (fHeader.DataCount); } }
        [Browsable(false)]
        public long CurFramePos { get { return (Math.Max(0, (lNextDataIndex - 1))); } }
        [DisplayName("Fortschritt")]
        [Description("Fortschritt des aktuellen Coin-Stream Replays (in [%]).")]
        [Browsable(false)]
        public uint Progress
        {
            get
            {
                if (IsEndOfFile)
                    return (100);
                else
                    return ((FrameCount == 0) ? 0 : ((uint)((100.0 / (float)FrameCount) * (float)CurFramePos)));
            }
        }
        #endregion


        #region IO
        private object ReadRawData(BotOperation oOperation)
        {
            object oResult = null;

            if (oNextOp == BotOperation.Invalid)
                // Initialisieren:
                oNextOp = BotOperation.eReadDelay;

            // Prüfen ob angeforderter Vorgang ansteht:
            if (oNextOp == oOperation)
            {
                switch (oNextOp)
                {
                    case BotOperation.eReadDelay:
                        oResult = pData.Modifier.ReadInt64();
                        break;

                    case BotOperation.eReadEvent:
                        oResult = new BotEvent(pData.Modifier, TradingPair);
                        break;

                    default:
                        Debug.Assert(false, "[RRD728] Ungültiger Typ!");
                        return (null);
                }

                // Nächste anstehende Operation definieren:
                if (oNextOp == BotOperation.eReadEvent)
                    oNextOp = BotOperation.eReadDelay;
                else
                    oNextOp++;
            }
            return (oResult);
        }
        public override bool Close()
        {
            Stop();

            return (base.Close());
        }
        #endregion
        #region Management
        void Reset()
        {
            // Clear datasets:
            pData = null;

            LastCandleSubscription = null;
            LastTickerSubscription = null;

            eCurEvent = null;
        }
        public bool Start()
        {
            Debug.Assert(IsValid, "[S255]");

            if ((tNextEventTimer == null) && (IsEndOfFile == false))
            {
                BotEnv.LogSystem.WriteLog("S371", BotLogProcess.eCoinStream, String.Format("Abspielen der Aufnahme '{0}' gestartet.", FileTitle), BotLogType.eInfo, BotLogPriority.eLow);

                dStartTime = DateTime.Now;

                RiftSummary.Reset();

                return (ReadDelay());
            }
            else
                return (false);
        }
        public void Stop()
        {
            IsPaused = false;

            if (tNextEventTimer != null)
            {
                TimeSpan tPlayDuration = (DateTime.Now - dStartTime);

                tNextEventTimer.Close();
                tNextEventTimer = null;

                BotEnv.LogSystem.WriteLog("S382", BotLogProcess.eCoinStream, String.Format("Abspielen der Aufnahme '{0}' abgeschlossen.", FileTitle), BotLogType.eInfo, BotLogPriority.eLow);
                RiftSummary.WriteLog("S441", BotLogProcess.eCoinStream);

                Debug.WriteLine("[Coin-Stream Player] Abspieldauer: {0}", tPlayDuration);
            }

            Reset();
        }
        // Spult das aktuelle Replay bis zum gewünschten Frame 'lTargetFrame' vor.
        public bool Forward(long lTargetFrame)
        {
            if ((CurFramePos < lTargetFrame) && (lTargetFrame < (fHeader.DataCount - 1)))
            {
                bool bResume = !IsPaused;

                IsPaused = true;
                bIsOwnerBusy = false;
                eCurEvent = null;

                // Events auslesen:
                while (lNextDataIndex <= lTargetFrame)
                {
                    // Delay auslesen:
                    ReadRawData(BotOperation.eReadDelay);

                    // Event auslesen:
                    ReadRawData(BotOperation.eReadEvent);

                    lNextDataIndex++;
                }

                // Fortfahren mit normalem Ablauf:
                ReadRawData(BotOperation.eReadDelay);

                if (bResume)
                    IsPaused = false;

                return (true);
            }
            else
                return (false);
        }


        private bool ReadDelay()
        {
            if (IsValid)
            {
                if (IsEndOfFile)
                {
                    Close();

                    // Benachrichtigung für Ende der Datei ausgeben:
                    OnEvaluate(BotEventType.eEndOfFile);
                }
                else
                {
                    object oDelay = ReadRawData(BotOperation.eReadDelay);
                    if (oDelay != null)
                    {
                        double dDelay = Math.Max(TIMER_DELAY_PLAY_NORMAL, TimeSpan.FromTicks((long)oDelay).TotalMilliseconds);

                        if (BotEnv.Configuration.CoinStream_Delay > 0)
                            // Verzögerung für Abspielen der Aufnahme anpassen:
                            dDelay = BotEnv.Configuration.CoinStream_Delay;

                        if (tNextEventTimer == null)
                            tNextEventTimer = UtlTimer.CreateTimer((oSender, eArg) => OnTimer_EvaluateEvent(oSender, eArg), dDelay, true);
                        else
                            UtlTimer.ModifyTimer(tNextEventTimer, dDelay, true);

                        return (true);
                    }
                }
            }

            return (false);
        }
        private bool ReadEvent()
        {
            bool bResult = false;

            Debug.Assert(IsValid, "[RE416]");
            Debug.Assert((IsEndOfFile == false), "[RE417]");

            if (IsPaused)
                return (true);
            else if (bIsOwnerBusy)
            {
                Debug.Assert((eCurEvent != null), "[RE457]");
                bIsOwnerBusy = false;
            }
            else
            {
                object oEvent = ReadRawData(BotOperation.eReadEvent);
                if (oEvent == null)
                    return (false);
                else
                {
                    if ((eCurEvent != null) && (eCurEvent.Subscription != LastCandleSubscription))
                        // Vorheriges Event zum Löschen freigeben:
                        eCurEvent = null;

                    eCurEvent = (BotEvent)oEvent;
                }
            }

            if (eCurEvent.IsValid)
            {
                switch (eCurEvent.Type)
                {
                    case BotSubscriptionType.eCandles:
                        BotCandleSubscription _bNewSubscr = (BotCandleSubscription)eCurEvent.Subscription;
                        if (LastCandleSubscription == null)
                        {
                            LastCandleSubscription = _bNewSubscr;

                            // Limit quantity of data:
                            LastCandleSubscription.MaxDataCount = _bNewSubscr.lData.Count;
                        }
                        else
                            LastCandleSubscription?.AddData(_bNewSubscr);
                        break;

                    case BotSubscriptionType.eTicker:
                        LastTickerSubscription = (BotTickerSubscription)eCurEvent.Subscription;
                        break;
                }

                // Benachrichtigung für Event ausgeben:
                switch (OnEvaluate(BotEventType.eEventOccured, eCurEvent.Subscription))
                {
                    case BotTaskResult.eBusy:
                        Debug.Assert((tNextEventTimer != null), "[RE468]");

                        // Owner ist beschäftigt!
                        // > Event wiederholen:
                        UtlTimer.ModifyTimer(tNextEventTimer, TIMER_DELAY_RETRY);

                        bIsOwnerBusy = true;
                        bResult = true;
                        break;

                    case BotTaskResult.eError:
                    case BotTaskResult.eSuccess:
                        lNextDataIndex++;

                        bResult = ReadDelay();
                        break;

                    default:
                        Debug.Assert(false, "[RE469] Ungültiger Typ!");
                        break;
                }
            }
            else
            {
                eCurEvent = null;

                // Benachrichtigung für aufgetretenen Fehler ausgeben:
                OnEvaluate(BotEventType.eError);
            }

            return (bResult);
        }
        #endregion


        #region Timer
        private void OnTimer_EvaluateEvent(object oSender, ElapsedEventArgs eArgs)
        {
            tNextEventTimer.Enabled = false;

            // Aktuelles Event auslesen:
            ReadEvent();
        }
        #endregion


        private Package<BinaryReader> pData = null;

        private EvaluateEventHandler OnEvaluate = null;

        private BotEvent eCurEvent = null;
        private Timer tNextEventTimer = null;

        private long lNextDataIndex = 0;
        private bool bIsOwnerBusy = false;
        private bool bIsPaused = false;
        private BotOperation oNextOp;
        private DateTime dStartTime;
    }
}
