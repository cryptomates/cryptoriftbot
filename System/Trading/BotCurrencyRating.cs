﻿using CryptoRiftBot;
using CryptoRiftBot.Trading.Services;
using CryptoRiftBot.Trading.Strategy;
using CryptoRiftBot.Trading;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using Shared.Utils.Core;
using System.Threading.Tasks;

namespace CryptoRiftBot
{
    public class BotCurrencyRating
    {
        #region Constants
        // Percent Changes:
        public const short PERCENT_CHANGE_COUNT = 3;
        public const short PERCENT_CHANGE_1h = 0;
        public const short PERCENT_CHANGE_24h = 1;
        public const short PERCENT_CHANGE_7D = 2;

        public const short PERCENT_CHANGE_RATING = PERCENT_CHANGE_24h;

        // Update:
        public static readonly TimeSpan UPDATE_COIN_INFO_INTERVAL = new TimeSpan(5, 0, 0);
        public const int UPDATE_COIN_INFO_ENTRY_LIMIT = 100;
        public static readonly TimeSpan UPDATE_COIN_TICKER_INTERVAL = new TimeSpan(0, 5, 0);
        public static readonly TimeSpan UPDATE_FORCE_UPDATE = new TimeSpan(0, 0, 30);                   // Maximale Abstand zu letztem Update bevor nächstes Update erzwungen wird.

        // Candles:
        public static readonly TimeSpan CANDLES_INTERVAL = new TimeSpan(0, 15, 0);
        public const int CANDLES_COUNT_INIT = 1000;
        public static readonly TimeSpan CANDLES_HISTORY_LENGTH = new TimeSpan((int)((double)CANDLES_COUNT_INIT * CANDLES_INTERVAL.TotalHours), 0, 0);
        #endregion
        #region Enumerations
        public enum BotState
        {
            eInvalid,
            eBusy,
            eValid
        }
        #endregion


        #region Types
        // Informationen zu Bewertung von Währungen.
        [TypeConverter(typeof(CnvEmptyExpandableObject))]
        public class BotCoinInfo : IBotCoinInfo
        {
            public BotCoinInfo(String sName = "")
            {
                this.Name = sName;
            }


            #region Properties
            [DisplayName("Name")]
            public String Name { private set; get; }
            [DisplayName("URL")]
            public String URL { private set; get; }
            [DisplayName("Marktkapitalisierung")]
            public double MarketCap { private set; get; }

            [Browsable(false)]
            public double Price { private set; get; }
            [Browsable(false)]
            public double[] PercentChange { private set; get; } = new double[PERCENT_CHANGE_COUNT];
            #endregion


            #region Management
            public void Update(String sName = null, String sURL = null, double dPrice = double.NaN, double dMarketCap = double.NaN, double[] dPercentChange = null)
            {
                // Validate:
                if ((dPercentChange != null) && (PercentChange.Length != dPercentChange.Length))
                    throw new ArgumentOutOfRangeException(string.Format("Failed tu update '{0}': Invalid format used!", nameof(PercentChange)));
                else
                {
                    // Update:
                    if (sName != null)
                        Name = sName;
                    if (sURL != null)
                        URL = sURL;
                    if (dPrice != double.NaN)
                        Price = dPrice;
                    if (dMarketCap != double.NaN)
                        MarketCap = dMarketCap;
                    if (dPercentChange != null)
                    {
                        for (int i = 0; i < dPercentChange.Length; i++)
                        {
                            if (!double.IsNaN(dPercentChange[i]))
                                PercentChange[i] = dPercentChange[i];
                        }
                    }
                }
            }
            #endregion
        }
        public class BotCoinInfoList : List<KeyValuePair<BotCoinType, BotCurrencyRating.BotCoinInfo>>, IBotCoinInfoList
        {
            public void Add(BotCoinType cType, BotCoinInfo cInfo)
            {
                Add(new KeyValuePair<BotCoinType, BotCurrencyRating.BotCoinInfo>(cType, cInfo));
            }
            public void Insert(int iIndex, BotCoinType cType, BotCoinInfo cInfo)
            {
                Insert(iIndex, new KeyValuePair<BotCoinType, BotCurrencyRating.BotCoinInfo>(cType, cInfo));
            }
            public void SortRating()
            {
                Sort((cFirst, cSec) => (cFirst.Value.PercentChange[BotCurrencyRating.PERCENT_CHANGE_RATING].CompareTo(cSec.Value.PercentChange[BotCurrencyRating.PERCENT_CHANGE_RATING]) * -1));
            }
            public IBotCoinInfo Find(BotCoinType cType)
            {
                return (Find(cKey => (cKey.Key == cType)).Value);
            }
            public BotCoinType GetTypeAt(int iIndex)
            {
                return (this[iIndex].Key);
            }
        }
        public class BotExchangeRate : IBotExchangeRate
        {
            #region Properties.Mamagement
            [Browsable(false)]
            public bool IsValid { get { return (DollarEuro != 0); } }
            #endregion
            #region Properties
            public double DollarEuro { private set; get; }
            #endregion


            #region Management
            public void Reset()
            {
                DollarEuro = 0;
            }
            public void Update(double dDollarEuro = double.NaN)
            {
                if (dDollarEuro != double.NaN)
                    DollarEuro = dDollarEuro;
            }
            #endregion
        }
        private class BotTaskDictionary : Dictionary<Task<bool>, object>
        {
            #region Management
            public new void Add(Task<bool> tTask, object oTag = null)
            {
                base.Add(tTask, oTag);
            }
            #endregion
        }
        #endregion


        static BotCurrencyRating()
        {
            LastUpdate = default(DateTime);
            Candles = new List<BotCandleSubscription>();
            ExchangeRate = new BotExchangeRate();

            // Coins initialisieren:
            {
                CoinInfo = new BotCoinInfoList();

                // Initialize default coins:
                CoinInfo.Add(BotCoinType.eUSDollar, new BotCurrencyRating.BotCoinInfo("US Dollar"));

                // Initialize supported coins:
                Array aCoinVals = Enum.GetValues(typeof(BotCoinType));

                foreach (BotCoinType _bType in aCoinVals)
                {
                    if (_bType != BotCoinType.Invalid)
                        CoinInfo.Add(_bType, new BotCurrencyRating.BotCoinInfo());
                }
            }
        }


        #region Properties.Mamagement
        [Browsable(false)]
        public static BotState State
        {
            get
            {
                if (ExchangeRate.IsValid)
                    return (UpdateTasks.Any() ? BotState.eBusy: BotState.eValid);
                else
                    return (BotState.eInvalid);
            }
        }
        [Browsable(false)]
        public static DateTime LastUpdate { private set; get; }
        [Browsable(false)]
        public static BotCoinInfoList CoinInfo { private set; get; }
        [Browsable(false)]
        public static List<BotCandleSubscription> Candles { private set; get; }
        [Browsable(false)]
        public static BotExchangeRate ExchangeRate { private set; get; }
        #endregion
        #region Properties.Tasks
        [Browsable(false)]
        public static IEnumerable<Task<bool>> UpdateTasks { get{ return (bUpdateTasks.Keys); } }
        private static BotTaskDictionary bUpdateTasks = new BotTaskDictionary();
        #endregion


        #region Management
        // Aktualisiert die Bewertung einzelner Währungen synchron.
        public static bool UpdateRating()
        {
            lock (BotThreadLock.Object[BotThreadLock.OBJ_CURRENCY_RATING])
            {
                return (Task.Run(async () => await UpdateRatingAsync()).Result);
            }
        }
        // Aktualisiert die Bewertung einzelner Währungen asynchron.
        public static async Task<bool> UpdateRatingAsync()
        {
            Debug.Assert((CoinInfo.Count > 0), "[UR214]");

            if (bUpdateTasks.Count > 0)
                return (false); // Update wird bereits ausgeführt.
            else
            {
                bool bResult = true;

                BotTaskState.Use(BotTaskState.BotClass.eCurrencyRating);

                // Start tasks:
                bUpdateTasks.Add(Task.Run(() => UpdateRatingAsync(CoinInfo)));
                bUpdateTasks.Add(Task.Run(() => UpdateExchangeRateAsync(ExchangeRate)));

                foreach (BotTradingPair _tPair in BotTradingPair.lActivePairs)
                {
                    BotCandleSubscription _bCandle = Candles.Find(_cSubscr => _cSubscr.ReferredPair.Compare(_tPair));
                    if (_bCandle == null)
                    {
                        _bCandle = new BotCandleSubscription(0, _tPair, CANDLES_INTERVAL);
                        Candles.Add(_bCandle);
                    }
                    bUpdateTasks.Add(Task.Run(() => UpdateCandlesAsync(_bCandle)), _bCandle);
                }

                // Wait for tasks:
                await Task.WhenAll(UpdateTasks);

                // Post-processing tasks:
                foreach (var _bTask in bUpdateTasks)
                {
                    switch (_bTask.Value)
                    {
                        case BotCandleSubscription _bCandle: BotCoinStreamRecorder.Update(_bCandle); break;
                    }
                }

                LastUpdate = DateTime.Now;
                BotTaskState.Unuse(BotTaskState.BotClass.eCurrencyRating);

                // Speicher freigeben:
                bUpdateTasks.Clear();
                GC.Collect();

                if (!bResult)
                    BotEnv.LogSystem.ShowResult("RWC463", BotLogProcess.eUpdateRating, "Coin-Bewertung konnte nicht aktualisiert werden!");

                // User via Callback benachrichtigen:
                BotEnv.Post(() => BotEnv.OnRatingUpdateFinished?.Invoke(bResult));

                if (bResult)
                    BotAutomatic.Run();

                return (bResult);
            }
        }
        public static BotCandleSubscription GetCandles(BotTradingStrategy bUsingStrategy)
        {
            BotCoinStreamPlayer cPlayer = bUsingStrategy.CoinStreamPlayer;
            if (cPlayer != null)
                // Candle-Subscription von abspielendem CoinStream-Player ausgeben:
                return (cPlayer.LastCandleSubscription);
            else
                return (GetCandles(bUsingStrategy.TradingPair));
        }
        public static BotCandleSubscription GetCandles(BotTradingPair tPair)
        {
            return (Candles.Find(tObject => (tObject.ReferredPair == tPair)));
        }
        // Aktualisiert Candle-Sticks über aktuelle Kurs-Informationen.
        public static bool UpdateCandles(BotSubscriptionPurpose sPurpose, BotTickerSubscription tTickerSubscr, BotTradingStrategy bUsingStrategy)
        {
            BotCandleSubscription cCandleSubscr = null;

            switch (sPurpose)
            {
                case BotSubscriptionPurpose.eRegular:
                    cCandleSubscr = Candles.Find(cTemp => ((cTemp.ReferredPair == tTickerSubscr.ReferredPair) && ((cTemp.UpdatedBy == bUsingStrategy.Platform) || (bUsingStrategy.Platform.IsSimulating))));
                    break;

                case BotSubscriptionPurpose.eCoinStream:
                    // Candle-Subscription (void Coin-Stream) ermitteln:
                    cCandleSubscr = GetCandles(bUsingStrategy);
                    break;

                default:
                    Debug.Assert(false, "[UC179] Ungültiger Typ!");
                    break;
            }

            if ((cCandleSubscr != null) && (cCandleSubscr.lData.Count > 0))
            {
                lock (BotThreadLock.Object[BotThreadLock.OBJ_CURRENCY_RATING])
                {
                    lock (cCandleSubscr.oThreadLockObj)
                    {
                        cCandleSubscr.AddData(tTickerSubscr);
                    }
                }
                return (true);
            }
            else
                return (false);
        }

        public static bool GetLatestCoinInfo(BotCoinType cCurrency, out IBotCoinInfo cResult)
        {
            cResult = BotCurrencyRating.CoinInfo.Find(cCurrency);
            if (cResult == null)
            {
                Debug.Assert(false, "[ST478]");
                return (false);
            }
            else
                return (true);
        }
        #endregion


        #region Management
        private static async Task<bool> UpdateRatingAsync(IBotCoinInfoList iCoinInfo)
        {
            if (await BotServiceManager.TickerService.GetTickerAsync(iCoinInfo))
            {
                // Ergebnis nach Rating sortieren:
                iCoinInfo.SortRating();
                return (true);
            }
            else
                return (false);
        }
        private static async Task<bool> UpdateExchangeRateAsync(IBotExchangeRate iRate)
        {
            return (await BotServiceManager.ExchangeRateService.GetExchangeRateAsync(iRate));
        }
        private static async Task<bool> UpdateCandlesAsync(BotCandleSubscription bSubscription)
        {
            int iCount = BotCurrencyRating.CANDLES_COUNT_INIT;
            if (bSubscription.lData.Count > 0)
            {
                // Nur notwendige Candles (inkl. Reserve) empfangen:
                BotCandleSubscription.BotCandleData cLastCandle = bSubscription.lData.Last();
                TimeSpan tMissingDuration = (DateTime.Now - cLastCandle.dReceivedTime);

                iCount = (int)Math.Round(tMissingDuration.TotalMinutes / BotCurrencyRating.CANDLES_INTERVAL.TotalMinutes) + 3;
            }
            return (await BotServiceManager.CandleService.GetCandlesAsync(bSubscription, iCount));
        }
        #endregion


        #region Helper
        // Rechnet einen Betrag von einer Währung in eine andere Währung um.
        public static double TranslateAmount(double dAmount, BotCoinType cPrimCurrency, BotCoinType cSecCurrency, bool bPrim2Sec = true)
        {
            if (dAmount != 0)
            {
                if (cPrimCurrency == cSecCurrency)
                    return (dAmount);

                else if (cSecCurrency == BotConvention.TRADE_BASE_CURRENCY)
                {
                    IBotCoinInfo bPrimInfo = CoinInfo.Find(cPrimCurrency);
                    if (bPrimInfo != null)
                        return (BotConvention.TranslateAmount(dAmount, bPrimInfo.Price, bPrim2Sec));
                }
                else
                    Debug.Assert(false, "[TA192] Berechnung nur mit Basis-Währung ([$]) möglich!");
            }

            return (0);
        }
        #endregion
    }
}
