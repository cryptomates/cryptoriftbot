﻿using CryptoRiftBot;
using CryptoRiftBot.Trading.Services;
using CryptoRiftBot.Trading.Strategy;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using Shared.Utils.Core;

namespace CryptoRiftBot.Trading.Functions
{
    public class BotAttrib
    {
        public class BotRefSpeedTypeAttribute : Attribute
        {
            public BotRefSpeedTypeAttribute(BotSpeedType sValue)
            {
                this.Value = sValue;
            }

            public BotSpeedType Value { get; set; }
        }
        public class BotDoubleAvgValueAttribute : Attribute
        {
            public BotDoubleAvgValueAttribute(bool bValue)
            {
                this.Value = bValue;
            }

            public bool Value { get; set; }
        }
        public class BotRefOrderConditionAttribute : Attribute
        {
            public BotRefOrderConditionAttribute(BotOrderCondition oValue, bool bInvert = false)
            {
                this.oValue = oValue;
                this.bInvert = bInvert;
            }


            #region State
            public bool IsUsed(BotOrderCondition oCondition)
            {
                if (bInvert)
                    return (oValue != oCondition);
                else
                    return (oValue == oCondition);
            }
            #endregion


            private BotOrderCondition oValue;
            private bool bInvert;
        }
        public class BotRefOrderTypeAttribute : Attribute
        {
            public BotRefOrderTypeAttribute(BotOrderType sValue)
            {
                this.Value = sValue;
            }

            public BotOrderType Value { get; set; }
        }
    }

    public class BotBaseIndicatorParam : BotBaseFunctionParam
    {
        public BotBaseIndicatorParam(String sDisplayName, String sDescription, TimeSpan tAvg1_Period, TimeSpan tAvg2_Period, BotFunctionID bFunctionID) : base(sDisplayName, sDescription, bFunctionID)
        {
            this.SourceType = BotSourceType.eCandleSticks;
            this.AverageType = BotAverageType.eEMA;
            this.TickerInfluence = true;
            this.Avg1_Period = tAvg1_Period;
            this.Avg2_Period = tAvg2_Period;
        }


        #region Properties.Management
        [DisplayName("Geschwindigkeit")]
        [Browsable(false)]
        public BotSpeedType SpeedType { private set; get; }
        [Description("Gibt an ob zwei Durchschnittswerte notwendig sind um Indikator anwenden zu können.")]
        [Browsable(false)]
        public bool IsDoubleAvgValue
        {
            get
            {
                switch (AverageType)
                {
                    case BotAverageType.eCMO: return (false);
                }
                return (true);
            }
        }
        #endregion
        #region Properties
        [DisplayName("Quelle")]
        [TypeConverter(typeof(CnvEnumDescription))]
        public BotSourceType SourceType
        {
            get { return sSourceType; }
            set
            {
                sSourceType = value;

                // Speed-Type in abhängigkeit von Source-Type definieren:
                switch (value)
                {
                    case BotSourceType.eCandleSticks:
                        SpeedType = BotSpeedType.eSlow;
                        break;

                    case BotSourceType.eTicker:
                    case BotSourceType.eVariable:
                        SpeedType = BotSpeedType.eFast;
                        break;

                    default:
                        Debug.Assert(false, "[ST671] Ungültiger Source-Typ!");
                        break;
                }
            }
        }
        private BotSourceType sSourceType;
        [DisplayName(BotConvention.PROP_NAME_TICKER_INFLUENCE)]
        [Description("Berücksichtigt Ticker-Werte als Einfluss auf den aktuellen Indikator.")]
        [TypeConverter(typeof(CnvBooleanLabel.OffOn))]
        public bool TickerInfluence { set; get; }
        [DisplayName("Typ")]
        [Description("")]
        [TypeConverter(typeof(CnvEnumDescription))]
        public BotAverageType AverageType { set; get; }
        [DisplayName("Dauer Durchschnitt 1")]
        [Description(BotConvention.PROP_DESCR_AVG_PERIOD)]
        public TimeSpan Avg1_Period
        {
            get { return (tAvg1_Period); }
            set { tAvg1_Period = LimitVal(value); }
        }
        private TimeSpan tAvg1_Period;
        [DisplayName("Dauer Durchschnitt 2")]
        [Description(BotConvention.PROP_DESCR_AVG_PERIOD)]
        [BotAttrib.BotDoubleAvgValue(true)]
        public TimeSpan Avg2_Period
        {
            get { return (tAvg2_Period); }
            set { tAvg2_Period = LimitVal(value); }
        }
        private TimeSpan tAvg2_Period;
        #endregion


        #region Configuration
        public override void WriteFile(JsonWriter jWriter, String sNodeName = "")
        {
            base.WriteFile(jWriter, sNodeName);
            {
                jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_STRATEGYPARAM_INDICATOR_SOURCE_TYPE);
                jWriter.WriteValue(sSourceType);
                jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_STRATEGYPARAM_INDICATOR_AVERAGE_TYPE);
                jWriter.WriteValue(AverageType);
                jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_STRATEGYPARAM_INDICATOR_TICKER_INFLUENCE);
                jWriter.WriteValue(TickerInfluence);

                jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_STRATEGYPARAM_INDICATOR_AVG1_PERIOD);
                jWriter.WriteValue(Avg1_Period);
                jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_STRATEGYPARAM_INDICATOR_AVG2_PERIOD);
                jWriter.WriteValue(Avg2_Period);

                // WICHTIG:
                // An dieser Stelle aktuelle Node NICHT abschließen!
                // > Das wird von Sub-Class ausgeführt!
            }
        }
        public override bool ReadFile(JObject dObjects, String sNodeName = "")
        {
            if (base.ReadFile(dObjects, sNodeName))
            {
                SourceType = dObjects.ParseValue(BotConvention.DATAFILE_JSON_STRATEGYPARAM_INDICATOR_SOURCE_TYPE, SourceType);
                AverageType = dObjects.ParseValue(BotConvention.DATAFILE_JSON_STRATEGYPARAM_INDICATOR_AVERAGE_TYPE, AverageType);
                TickerInfluence = dObjects.ParseValue(BotConvention.DATAFILE_JSON_STRATEGYPARAM_INDICATOR_TICKER_INFLUENCE, TickerInfluence);

                Avg1_Period = dObjects.ParseValue(BotConvention.DATAFILE_JSON_STRATEGYPARAM_INDICATOR_AVG1_PERIOD, Avg1_Period);
                Avg2_Period = dObjects.ParseValue(BotConvention.DATAFILE_JSON_STRATEGYPARAM_INDICATOR_AVG2_PERIOD, Avg2_Period);

                return (true);
            }
            else
                return (false);
        }
        #endregion


        #region Helper
        protected TimeSpan LimitVal(TimeSpan tValue)
        {
            switch (SpeedType)
            {
                case BotSpeedType.eSlow:
                    if (tValue < BotCurrencyRating.CANDLES_INTERVAL)
                        return (BotCurrencyRating.CANDLES_INTERVAL);

                    else if (tValue > BotCurrencyRating.CANDLES_HISTORY_LENGTH)
                        return (BotCurrencyRating.CANDLES_HISTORY_LENGTH);

                    break;

                case BotSpeedType.eFast:
                    if (tValue < BotSubscriptionWebRequest.REQUEST_INTERVAL)
                        return (BotSubscriptionWebRequest.REQUEST_INTERVAL);

                    break;

                default:
                    Debug.Assert(false, "[LV243]");
                    break;
            }

            return (tValue);
        }
        #endregion


    }
    public class BotTrendIndicatorParam : BotBaseIndicatorParam
    {
        #region Types
        [TypeConverter(typeof(BotFunctionConverter))]
        public class BotEnableLimitInfo
        {
            #region Constants
            public const String DESCR_LIMITS = "Notwendige Bewertung des Trend-Indikators (in [%]) die mindestens vorliegen muss um referenzierende Reaktions-Indikatoren freizugeben.";
            #endregion


            public BotEnableLimitInfo()
            {
                dBuyIndicatorLimit = 0.5;
                dSellIndicatorLimit = -0.5;
            }


            #region Properties
            [DisplayName("Freigabe Kauf-Indikatoren")]
            [Description(DESCR_LIMITS)]
            public double BuyIndicatorLimit
            {
                get { return dBuyIndicatorLimit; }
                set { dBuyIndicatorLimit = Math.Max((dSellIndicatorLimit + 0.01), value); }
            }
            private double dBuyIndicatorLimit;
            [DisplayName("Freigabe Verkauf-Indikatoren")]
            [Description(DESCR_LIMITS)]
            public double SellIndicatorLimit
            {
                get { return dSellIndicatorLimit; }
                set { dSellIndicatorLimit = Math.Min((dBuyIndicatorLimit - 0.01), value); }
            }
            private double dSellIndicatorLimit;
            #endregion


            #region Configuration
            public void WriteFile(JsonWriter jWriter)
            {
                jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_STRATEGYPARAM_TREND_LIMIT_BUYINDICATOR);
                jWriter.WriteValue(dBuyIndicatorLimit);
                jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_STRATEGYPARAM_TREND_LIMIT_SELLINDICATOR);
                jWriter.WriteValue(dSellIndicatorLimit);
            }
            public bool ReadFile(JObject dObjects)
            {
                dBuyIndicatorLimit = dObjects.ParseValue(BotConvention.DATAFILE_JSON_STRATEGYPARAM_TREND_LIMIT_BUYINDICATOR, dBuyIndicatorLimit);
                dSellIndicatorLimit = dObjects.ParseValue(BotConvention.DATAFILE_JSON_STRATEGYPARAM_TREND_LIMIT_SELLINDICATOR, dSellIndicatorLimit);

                return (true);
            }
            #endregion
        }
        #endregion


        public BotTrendIndicatorParam(TimeSpan tAvg1_Period, TimeSpan tAvg2_Period) : base(BotConvention.PROP_NAME_TREND_INDICATOR, BotConvention.PROP_DESCR_TREND_INDICATORS, tAvg1_Period, tAvg2_Period, new BotFunctionID(BotFunctionID.BotFunctionType.eTrendIndicator))
        {
            IndicatorLimits = new BotEnableLimitInfo();
        }


        #region Properties
        [DisplayName("Grenzwerte")]
        [Description("Grenzwerte zur Freigabe von referenzierenden Reaktions-Indikatoren.")]
        public BotEnableLimitInfo IndicatorLimits { set; get; }
        #endregion


        #region Configuration
        public override void WriteFile(JsonWriter jWriter, String sNodeName = "")
        {
            base.WriteFile(jWriter, sNodeName);
            {
                IndicatorLimits.WriteFile(jWriter);

                jWriter.WriteEndObject();
            }
        }
        public override bool ReadFile(JObject dObjects, String sNodeName = "")
        {
            if (base.ReadFile(dObjects, sNodeName))
            {
                IndicatorLimits.ReadFile(dObjects);

                return (true);
            }
            else
                return (false);
        }
        #endregion


        #region Management
        protected override bool OnCheckPlausibility()
        {
            return (base.OnCheckPlausibility());
        }
        #endregion
    }
    public class BotReactionIndicatorParam : BotBaseIndicatorParam
    {
        #region Enumerations
        public enum BotOrderDependency
        {
            [Description("Keine")]
            eNone,

            [Description("Freigabe Kauf-Orders")]
            eEnabled_BuyOrders,
            [Description("Freigabe Verkauf-Orders")]
            eEnabled_SellOrders,
            [Description("Freigabe Kauf-/Verkauf-Orders")]
            eEnabled_AllOrders
        }
        #endregion


        #region Types
        public class BotOrderInfo : BotBasicOrderInfo
        {
            [TypeConverter(typeof(CnvEmptyExpandableObject))]
            public class BotLimitInfoFast : BotHysteresisParam
            {
                public BotLimitInfoFast(double dEnableLimit, double dExecuteLimit, bool bTrailExecution, BotOrderDirection oDirection) : base(dEnableLimit, dExecuteLimit, bTrailExecution, oDirection)
                {
                }
            }
            [TypeConverter(typeof(CnvEmptyExpandableObject))]
            public class BotLimitInfoSlow
            {
                public BotLimitInfoSlow(double dMinRating)
                {
                    this.MinRating = dMinRating;
                }


                #region Properties
                [DisplayName("Min. Änderung")]
                [Description("Notwendige Bewertung des Indikators (in [%]) die mindestens vorliegen muss um Orders ausführen zu können.")]
                // [Description("Notwendige Bewertung des Indikators (in [%]) die mindestens vorliegen muss um Kauf-Orders ausführen zu können.")]       // (BETA) ... [DESCRIPTION]
                public double MinRating { set; get; }
                #endregion
            }


            public BotOrderInfo(BotReactionIndicatorParam iParent, double dEnableLimit, double dExecuteLimit, bool bTrailExecution, BotOrderDirection oDirection, double dSlow_MinRating) : base()
            {
                this.iParent = iParent;
                this.LimitFast = new BotLimitInfoFast(dEnableLimit, dExecuteLimit, bTrailExecution, oDirection);
                this.LimitSlow = new BotLimitInfoSlow(dSlow_MinRating);
            }


            #region Properties.Management
            [Browsable(false)]
            public BotSpeedType SpeedType { get { return (iParent.SpeedType); } }
            #endregion
            #region Properties
            [DisplayName(BotConvention.PROP_NAME_LIMITS)]
            [Description(BotConvention.PROP_DESCR_LIMITS)]
            [BotAttrib.BotRefSpeedType(BotSpeedType.eFast)]
            public BotLimitInfoFast LimitFast { set; get; }
            [DisplayName(BotConvention.PROP_NAME_LIMITS)]
            [Description(BotConvention.PROP_DESCR_LIMITS)]
            [BotAttrib.BotRefSpeedType(BotSpeedType.eSlow)]
            public BotLimitInfoSlow LimitSlow { set; get; }
            #endregion


            #region Configuration
            public override void WriteFile(JsonWriter jWriter, String sNodeName)
            {
                base.WriteFile(jWriter, sNodeName);
                {
                    jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_STRATEGYPARAM_INDICATOR_SLOW_RATING_MIN);
                    jWriter.WriteValue(LimitSlow.MinRating);
                    jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_STRATEGYPARAM_INDICATOR_FAST_RATING_ENABLE);
                    jWriter.WriteValue(LimitFast.EnableLimit);
                    jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_STRATEGYPARAM_INDICATOR_FAST_RATING_EXECUTE);
                    jWriter.WriteValue(LimitFast.ExecuteLimit);
                    jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_STRATEGYPARAM_INDICATOR_FAST_RATING_TRAILEXECUTE);
                    jWriter.WriteValue(LimitFast.TrailExecution);

                    jWriter.WriteEndObject();
                }
            }
            public override bool ReadFile(JObject dObjects, String sNodeName)
            {
                bool bResult = false;

                if (base.ReadFile(dObjects, sNodeName))
                {
                    JToken _jToken = dObjects[sNodeName];
                    if (_jToken != null)
                    {
                        LimitSlow.MinRating = _jToken.ParseValue(BotConvention.DATAFILE_JSON_STRATEGYPARAM_INDICATOR_SLOW_RATING_MIN, LimitSlow.MinRating);
                        LimitFast.EnableLimit = _jToken.ParseValue(BotConvention.DATAFILE_JSON_STRATEGYPARAM_INDICATOR_FAST_RATING_ENABLE, LimitFast.EnableLimit);
                        LimitFast.ExecuteLimit = _jToken.ParseValue(BotConvention.DATAFILE_JSON_STRATEGYPARAM_INDICATOR_FAST_RATING_EXECUTE, LimitFast.ExecuteLimit);
                        LimitFast.TrailExecution = _jToken.ParseValue(BotConvention.DATAFILE_JSON_STRATEGYPARAM_INDICATOR_FAST_RATING_TRAILEXECUTE, LimitFast.TrailExecution);

                        bResult = true;
                    }
                }
                return (bResult);
            }
            #endregion


            private BotReactionIndicatorParam iParent;
        }
        [TypeConverter(typeof(BotFunctionConverter))]
        public class BotDependencyInfo
        {
            public BotDependencyInfo()
            {
                this.DoubleCheck = true;
                this.OrderDependency = BotOrderDependency.eEnabled_SellOrders;
            }


            #region Properties
            [DisplayName("Order-Platzierung")]
            [Description("Abängigkeiten zu Trend-Indikator zur Platzierung von Orders.")]
            [TypeConverter(typeof(CnvEnumDescription))]
            public BotOrderDependency OrderDependency { set; get; }
            [DisplayName("Bestätigung")]
            [Description("Änderung des Indikators muss mit dessen Tendenz übereinstimmen (z.B. jeweils steigend) um als aussagekräftig zu gelten und Orders plazieren zu können.")]
            [TypeConverter(typeof(CnvBooleanLabel.DisEnabled))]
            public bool DoubleCheck { set; get; }
            #endregion


            #region Configuration
            public void WriteFile(JsonWriter jWriter)
            {
                jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_DEPENDENCIES);
                jWriter.WriteStartObject();
                {
                    jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_STRATEGYPARAM_INDICATOR_DEPENDENCY_ORDERS);
                    jWriter.WriteValue(OrderDependency);
                    jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_STRATEGYPARAM_INDICATOR_DEPENDENCY_DOUBLECHECK);
                    jWriter.WriteValue(DoubleCheck);

                    jWriter.WriteEndObject();
                }
            }
            public bool ReadFile(JObject dObjects)
            {
                bool bResult = true;

                JToken _jToken = dObjects[BotConvention.DATAFILE_JSON_DEPENDENCIES];
                if ((bResult) && (_jToken != null))
                {
                    OrderDependency = _jToken.ParseValue(BotConvention.DATAFILE_JSON_STRATEGYPARAM_INDICATOR_DEPENDENCY_ORDERS, OrderDependency);
                    DoubleCheck = _jToken.ParseValue(BotConvention.DATAFILE_JSON_STRATEGYPARAM_INDICATOR_DEPENDENCY_DOUBLECHECK, DoubleCheck);
                }
                else
                    bResult = false;
                return (bResult);
            }
            #endregion
        }
        #endregion


        public BotReactionIndicatorParam(int iIndex, TimeSpan tAvg1_Period, TimeSpan tAvg2_Period) : base((BotConvention.PROP_NAME_INDICATOR + " {0}"), BotConvention.PROP_DESCR_REACTION_INDICATORS, tAvg1_Period, tAvg2_Period, new BotFunctionID(BotFunctionID.BotFunctionType.eReactionIndicator, (iIndex + 1)))
        {
            this.Enabled = (iIndex == 0);
            this.BuyOrders = new BotOrderInfo(this, -3, 0.5, true, BotOrderDirection.eBuy, 1);
            this.SellOrders = new BotOrderInfo(this, 3, -0.5, true, BotOrderDirection.eSell, -1);
            this.Dependency = new BotDependencyInfo();
        }


        #region Properties
        [DisplayName(BotConvention.PROP_CATEGORY_BUY_ORDERS)]
        [Description("")]
        public BotOrderInfo BuyOrders { set; get; }
        [DisplayName(BotConvention.PROP_CATEGORY_SELL_ORDERS)]
        [Description("")]
        public BotOrderInfo SellOrders { set; get; }
        [DisplayName("Abhängigkeiten")]
        [Description("")]
        public BotDependencyInfo Dependency { set; get; }
        #endregion


        #region Configuration
        public override void WriteFile(JsonWriter jWriter, String sNodeName = "")
        {
            base.WriteFile(jWriter, sNodeName);
            {
                Dependency.WriteFile(jWriter);
                BuyOrders.WriteFile(jWriter, BotConvention.DATAFILE_JSON_BUYORDERS);
                SellOrders.WriteFile(jWriter, BotConvention.DATAFILE_JSON_SELLORDERS);

                jWriter.WriteEndObject();
            }
        }
        public override bool ReadFile(JObject dObjects, String sNodeName = "")
        {
            if (base.ReadFile(dObjects, sNodeName))
            {
                Dependency.ReadFile(dObjects);
                BuyOrders.ReadFile(dObjects, BotConvention.DATAFILE_JSON_BUYORDERS);
                SellOrders.ReadFile(dObjects, BotConvention.DATAFILE_JSON_SELLORDERS);

                return (true);
            }
            else
                return (false);
        }
        #endregion


        #region Management
        protected override bool OnCheckPlausibility()
        {
            return (base.OnCheckPlausibility());
        }
        #endregion
        #region Management
        public override BotStateResetMode GetResetMode(BotOrderDirection oDirection)
        {
            return ((oDirection == BotOrderDirection.eBuy) ? BuyOrders.ResetMode : SellOrders.ResetMode);
        }
        #endregion
    }


    // Indikator für Kurs-Bewertung mittels zwei Durchschnittswerten.
    public class BotBaseIndicator : BotBaseFunction
    {
        #region Constants
        public const int TENDENCY_CALC_DATA_COUNT = 20;
        public const double VARIATION_MAX_VAL = 0.001;
        #endregion


        public BotBaseIndicator(BotBaseIndicatorParam bRefParams, Color cGraphColor) : base(bRefParams)
        {
            this.GraphColor = cGraphColor;

            this.lVariation = new List<double>();
            this.lVariationPercent = new List<double>();
            this.mAverage1 = new BotMovingAverage();
            this.mAverage2 = new BotMovingAverage();
        }


        public BotMovingAverage mAverage1;
        public BotMovingAverage mAverage2;


        #region Properties.Management
        [Browsable(false)]
        public String DisplayNameAvg1 { get { return String.Format("{0} ({1}{2})", DisplayName, UtlEnum.GetEnumDescription(ReferredParameters.AverageType), 1); } }
        [Browsable(false)]
        public String DisplayNameAvg2 { get { return String.Format("{0} ({1}{2})", DisplayName, UtlEnum.GetEnumDescription(ReferredParameters.AverageType), 2); } }
        [Browsable(false)]
        public Color GraphColorAvg1 { get { return (GraphColor); } }
        [Browsable(false)]
        public Color GraphColorAvg2 { get { return (Color.FromArgb(128, GraphColor)); } }
        [Browsable(false)]
        public BotSpeedType SpeedType { get { return (ReferredParameters.SpeedType); } }


        [DisplayName("Langsamerer Mittelwert")]
        [Description("Ermittelt den langsamereren von beiden, zur Berechnung des Indikator genutzten, Durchschnittswerten.")]
        [Browsable(false)]
        private BotMovingAverage LongerAverage { set; get; }
        #endregion
        #region Properties.Management
        [Browsable(false)]
        public new BotBaseIndicatorParam ReferredParameters { get { return (BotBaseIndicatorParam)base.ReferredParameters; } }
        [Browsable(false)]
        public override String SummaryName { get { return String.Format("{0} ({1} / {2})", DisplayName, UtlEnum.GetEnumDescription(ReferredParameters.AverageType), UtlEnum.GetEnumDescription(ReferredParameters.SourceType)); } }
        [Browsable(false)]
        public override bool IsReferredVariableUsed { get { return (ReferredParameters.SourceType == BotSourceType.eVariable); } }
        #endregion
        #region Properties.Management
        [Browsable(false)]
        public virtual bool IsLimitEnabled
        {
            get
            {
                Debug.Assert(false, "[ILE1728] Implementierung via Sub-Class!");
                return (false);
            }
        }
        #endregion
        #region Properties.State
        [DisplayName("Farbe")]
        [Description("Farbe zur Darstellung des Indikators.")]
        public Color GraphColor { private set; get; }
        [DisplayName("Gültig")]
        [Description("Prüft ob die Durchschnittswerte erfolgreich berechnet werden konnten.")]
        [Browsable(false)]
        public bool IsValid
        {
            get
            {
                if (mAverage1.IsValid == false)
                    return (false);
                else if ((ReferredParameters.IsDoubleAvgValue) && (mAverage2.IsValid == false))
                    return (false);
                else
                    return (true);
            }
        }
        [Browsable(false)]
        public bool IsNonInfluencedCandles { get { return ((ReferredParameters.SourceType == BotSourceType.eCandleSticks) && (ReferredParameters.TickerInfluence == false)); } }
        #endregion
        #region Properties
        [DisplayName("Durchschnitt 1")]
        [Description("Aktueller Durchschnittswert.")]
        public double ValueAvg1 { get { return (mAverage1.CurrentValue); } }
        [DisplayName("Durchschnitt 2")]
        [Description("Aktueller Durchschnittswert.")]
        [BotAttrib.BotDoubleAvgValue(true)]
        public double ValueAvg2 { get { return (mAverage2.CurrentValue); } }
        [DisplayName("Änderung")]
        [Description("Änderung von trägerem zu schnellerem Durchschnittswert.")]
        [Browsable(false)]
        public double Variation { get { return ((lVariation.Count == 0) ? 0 : lVariation.Last()); } }
        [DisplayName("Änderung")]
        [Description("Änderung (in [%]) von trägerem zu schnellerem Durchschnittswert.")]
        public double VariationPercent { get { return ((lVariationPercent.Count == 0) ? 0 : lVariationPercent.Last()); } }
        [DisplayName("Tendenz")]
        [Description("Richtung (steigend, bzw. fallend) in die Sich die ermittelte Änderung bewegt.")]
        public double Tendency { get { return (dTendency); } }
        [DisplayName("Niedrigster Wert")]
        [Description("Niedrigster aus allen Kurswerten, welche zur Berechnung des Indikators verwendet werden.")]
        [Browsable(false)]
        public double LowPrice { get { return ((LongerAverage == null) ? 0 : LongerAverage.LowValue); } }
        [DisplayName("Höchster Wert")]
        [Description("Höchster aus allen Kurswerten, welche zur Berechnung des Indikators verwendet werden.")]
        [Browsable(false)]
        public double HighPrice { get { return ((LongerAverage == null) ? 0 : LongerAverage.HighValue); } }
        [DisplayName("Kleinste Änderung")]
        [Description("Kleinste Änderung (in [%]) aus allen Änderungs-Werten, welche während der Perioden-Dauer des Indikators ermittelt wurden.")]
        [Browsable(false)]
        public double LowVarPercent { get { return ((lVariationPercent.Count > 0) ? lVariationPercent.Min() : 0); } }
        [DisplayName("Größte Änderung")]
        [Description("Größte Änderung (in [%]) aus allen Änderungs-Werten, welche während der Perioden-Dauer des Indikators ermittelt wurden.")]
        [Browsable(false)]
        public double HighVarPercent { get { return ((lVariationPercent.Count > 0) ? lVariationPercent.Max() : 0); } }
        #endregion


        #region Management.General
        public void Reset()
        {
            dTendency = 0;

            lVariation.Clear();
            lVariationPercent.Clear();
            mAverage1.Reset();
            mAverage2.Reset();
        }
        public bool Update()
        {
            if (ReferredParameters.Enabled)
            {
                if (IsNonInfluencedCandles)
                    // Einflussnahme durch Ticker verhindern:
                    return (true);
                else
                {
                    mAverage1.Update(ReferredVariable);

                    if (ReferredParameters.IsDoubleAvgValue)
                        mAverage2.Update(ReferredVariable);

                    return (CalcVariation());
                }
            }
            else
                return (false);
        }
        #endregion
        #region Management.CandleSubscription
        public bool Build(BotCandleSubscription cCandles)
        {
            if (IsActive(BotSpeedType.eSlow))
            {
                mAverage1.Initialize(cCandles, ReferredParameters.AverageType, ReferredParameters.Avg1_Period, IsNonInfluencedCandles);

                if (ReferredParameters.IsDoubleAvgValue)
                    mAverage2.Initialize(cCandles, ReferredParameters.AverageType, ReferredParameters.Avg2_Period, IsNonInfluencedCandles);

                return (CalcVariation());
            }
            else
                return (false);
        }
        #endregion
        #region Management.TickerSubscription
        public bool Build(BotTickerSubscription tTicker)
        {
            if (IsActive(BotSpeedType.eFast))
            {
                mAverage1.Initialize(tTicker, ReferredParameters.AverageType, ReferredParameters.Avg1_Period);

                if (ReferredParameters.IsDoubleAvgValue)
                    mAverage2.Initialize(tTicker, ReferredParameters.AverageType, ReferredParameters.Avg2_Period);

                return (CalcVariation());
            }
            else
                return (false);
        }
        #endregion
        #region Management
        private bool CalcVariation()
        {
            if (IsValid)
            {
                double dLowVal;
                double dHighVal;
                bool bFalling;

                if (LongerAverage == null)
                {
                    // Langsamereren Indikator ermitteln:
                    if (mAverage2.IsValid)
                        LongerAverage = (mAverage1.PeriodDuration >= mAverage2.PeriodDuration) ? mAverage1 : mAverage2;
                    else
                        LongerAverage = mAverage1;
                }

                switch (ReferredParameters.AverageType)
                {
                    case BotAverageType.eCMO:
                        dLowVal = Math.Min(mAverage1.CurrentValue, mAverage1.LastValue);
                        dHighVal = Math.Max(mAverage1.CurrentValue, mAverage1.LastValue);
                        bFalling = (mAverage1.CurrentValue == dLowVal);
                        break;

                    default:
                        Debug.Assert((ReferredParameters.IsDoubleAvgValue), "[CV885]");

                        dLowVal = Math.Min(ValueAvg1, ValueAvg2);
                        dHighVal = Math.Max(ValueAvg1, ValueAvg2);

                        // Ggf. Änderung invertieren (wenn fallend):
                        if (mAverage1.PeriodDuration < mAverage2.PeriodDuration)
                            bFalling = (ValueAvg1 < ValueAvg2);
                        else
                            bFalling = (ValueAvg1 > ValueAvg2);

                        break;
                }

                if ((dLowVal != 0) && (dHighVal != 0) && (dLowVal != dHighVal))
                {
                    // Änderung berechnen:
                    double dVariation = (dHighVal - dLowVal);
                    double dVariationPercent = Math.Max(VARIATION_MAX_VAL, ((Math.Abs(dVariation) * 100) / Math.Abs(dHighVal)));

                    if (bFalling)
                    {
                        dVariation *= -1;
                        dVariationPercent *= -1;
                    }

                    lVariation.Add(dVariation);
                    lVariationPercent.Add(dVariationPercent);

                    // Ggf. Anzahl an Werten begrenzen:
                    if ((LongerAverage != null) && (LongerAverage.IsValid))
                    {
                        int iMaxCount = LongerAverage.PeriodDataCount;

                        if (lVariation.Count > iMaxCount)
                            lVariation.RemoveRange(0, (lVariation.Count - iMaxCount));
                        if (lVariationPercent.Count > iMaxCount)
                            lVariationPercent.RemoveRange(0, (lVariationPercent.Count - iMaxCount));
                    }

                    // Ggf. Tendenz berechnen:
                    if (lVariation.Count > TENDENCY_CALC_DATA_COUNT)
                        dTendency = UtlMath.CalcSlope(lVariation.GetRange((lVariation.Count - TENDENCY_CALC_DATA_COUNT), TENDENCY_CALC_DATA_COUNT));
                }

                return (true);
            }
            else
                return (false);
        }
        #endregion


        #region State
        protected bool IsActive(BotSpeedType sUsingType)
        {
            return ((ReferredParameters.Enabled) && (ReferredParameters.SpeedType == sUsingType));
        }
        #endregion


        private List<double> lVariation;
        private List<double> lVariationPercent;
        private double dTendency;
    }
    public class BotTrendIndicator : BotBaseIndicator
    {
        #region Types
        [TypeConverter(typeof(BotFunctionConverter))]
        public class BotEnableStateInfo
        {
            public BotEnableStateInfo(BotTrendIndicator tParent)
            {
                this.tParent = tParent;

                Reset();
            }


            #region Properties
            [DisplayName("Kauf-Indikatoren")]
            [Description("Ausreichende Bewertung der Candle-Sticks zur Freigabe von Indikatoren für Platzierung von Kauf-Orders.")]
            [ReadOnly(true)]
            [TypeConverter(typeof(CnvBooleanLabel.BlockedReleased))]
            public bool Enable_BuyIndicator { set; get; }
            [DisplayName("Verkauf-Indikatoren")]
            [Description("Ausreichende Bewertung der Candle-Sticks zur Freigabe von Indikatoren für Platzierung von Verkauf-Orders.")]
            [ReadOnly(true)]
            [TypeConverter(typeof(CnvBooleanLabel.BlockedReleased))]
            public bool Enable_SellIndicator { set; get; }
            #endregion


            #region Management
            public void Reset()
            {
                Enable_BuyIndicator = false;
                Enable_SellIndicator = false;
            }
            #endregion
            #region Calculation
            public void CalcLimits(BotTradingStrategy bRefStrategy)
            {
                if ((bRefStrategy.Platform.IsSimulating) && (BotDebugMode.FunctionLimitsEnabled))
                {
                    Enable_BuyIndicator = true;
                    Enable_SellIndicator = true;
                }
                else
                {
                    Enable_BuyIndicator = (tParent.VariationPercent >= tParent.ReferredParameters.IndicatorLimits.BuyIndicatorLimit);
                    Enable_SellIndicator = (tParent.VariationPercent <= tParent.ReferredParameters.IndicatorLimits.SellIndicatorLimit);
                }
            }
            #endregion


            private BotTrendIndicator tParent;
        }
        #endregion


        public BotTrendIndicator(BotTrendIndicatorParam tRefParams, Color cGraphColor) : base(tRefParams, cGraphColor)
        {
            EnableState = new BotEnableStateInfo(this);
        }


        #region Properties.Management
        [Browsable(false)]
        public new BotTrendIndicatorParam ReferredParameters { get { return (BotTrendIndicatorParam)base.ReferredParameters; } }

        [Browsable(false)]
        public override bool IsLimitEnabled { get { return ((EnableState.Enable_BuyIndicator) || (EnableState.Enable_SellIndicator)); } }
        #endregion
        #region Properties
        [DisplayName("Freigaben")]
        [Description("Aktuelle Freigaben von referenzierenden Reaktions-Indikatoren.")]
        public BotEnableStateInfo EnableState { private set; get; }
        #endregion


        #region Management.General
        public override bool ApplyParameters(BotFormula.BotVariables vVariables)
        {
            return (base.ApplyParameters(vVariables));
        }
        #endregion
    }
    public class BotReactionIndicator : BotBaseIndicator
    {
        #region Types
        [TypeConverter(typeof(BotFunctionConverter))]
        public class BotLimitStateInfo
        {
            public BotLimitStateInfo(BotReactionIndicator tParent, BotOrderDirection oOrderDirection)
            {
                this.rParent = tParent;
                this.OrderDirection = oOrderDirection;

                Reset();
            }


            #region Properties.Management
            [Browsable(false)]
            public BotSpeedType SpeedType { get { return (rParent.SpeedType); } }
            [Browsable(false)]
            public BotOrderDirection OrderDirection { private set; get; }
            #endregion
            #region Properties.Reference
            [Browsable(false)]
            public BotFormula.BotCalculation Formula { get { return (OrderDirection == BotOrderDirection.eBuy) ? rParent.BuyFormula : rParent.SellFormula; } }
            #endregion
            #region Properties
            [DisplayName("Bewertung 'Candle-Sticks'")]
            [Description("Ausreichende Bewertung der Candle-Sticks zum Plazieren von Orders.")]
            [BotAttrib.BotRefSpeedType(BotSpeedType.eSlow)]
            [ReadOnly(true)]
            [TypeConverter(typeof(CnvBooleanLabel.InSufficient))]
            public bool SlowRating { private set; get; }
            [DisplayName("Freigabe 'Ticker'")]
            [Description("Ausreichende Bewertung des Tickers zum Plazieren von Orders.")]
            [BotAttrib.BotRefSpeedType(BotSpeedType.eFast)]
            [ReadOnly(true)]
            [TypeConverter(typeof(CnvBooleanLabel.BlockedReleased))]
            public bool FastEnabled { private set; get; }
            [DisplayName("Grenzwert Ausführung")]
            [Description("Grenzwert (Errechnet falls Nachführung aktiviert) für Ausführung von Orders.")]
            [BotAttrib.BotRefSpeedType(BotSpeedType.eFast)]
            [ReadOnly(true)]
            public double FastExecuteLimit { get { return (dFastExecuteLimit + dFastExecuteOffset); } }
            [DisplayName(BotConvention.PROP_NAME_FORMULA_RESULT)]
            [ReadOnly(true)]
            [TypeConverter(typeof(CnvEnumDescription))]
            public BotFormula.BotCalculation.BotResult FormulaResult { get { return (Formula.LastResult); } }
            #endregion


            #region Management
            public void Reset(bool bWarm = false)
            {
                if (bWarm == false)
                {
                    SlowRating = false;
                    FastEnabled = false;
                    dFastExecuteLimit = 0;
                }

                dFastExecuteOffset = 0;
            }
            #endregion
            #region Calculation
            public void CalcLimits(BotTradingStrategy bRefStrategy, BotTrendIndicator tTrendIndicator)
            {
                bool bIsReady;

                if (OrderDirection == BotOrderDirection.eBuy)
                    bIsReady = (bRefStrategy.Rift_BuyInfo.RemainingSteps.Total > 0);
                else
                    bIsReady = (bRefStrategy.Rift_SellInfo.RemainingSteps.Default > 0);

                if (bIsReady)
                {
                    // Prüfen ob Trend-Bewertung für Platzierung von Orders ausreichen:
                    switch (rParent.SpeedType)
                    {
                        case BotSpeedType.eSlow:
                            if ((bRefStrategy.Platform.IsSimulating) && (BotDebugMode.FunctionLimitsEnabled))
                                SlowRating = true;
                            else
                            {
                                if (OrderDirection == BotOrderDirection.eBuy)
                                    SlowRating = (rParent.VariationPercent >= rParent.ReferredParameters.BuyOrders.LimitSlow.MinRating);
                                else
                                    SlowRating = (rParent.VariationPercent <= rParent.ReferredParameters.SellOrders.LimitSlow.MinRating);
                            }
                            break;

                        case BotSpeedType.eFast:
                            if ((bRefStrategy.Platform.IsSimulating) && (BotDebugMode.FunctionLimitsEnabled))
                                FastEnabled = true;
                            else if (FastEnabled == false)
                            {
                                if (OrderDirection == BotOrderDirection.eBuy)
                                    FastEnabled = (rParent.VariationPercent <= rParent.ReferredParameters.BuyOrders.LimitFast.EnableLimit);
                                else
                                    FastEnabled = (rParent.VariationPercent >= rParent.ReferredParameters.SellOrders.LimitFast.EnableLimit);
                            }

                            // Grenzwert für Ausführung berechnen:
                            if (OrderDirection == BotOrderDirection.eBuy)
                                dFastExecuteLimit = rParent.ReferredParameters.BuyOrders.LimitFast.ExecuteLimit;
                            else
                                dFastExecuteLimit = rParent.ReferredParameters.SellOrders.LimitFast.ExecuteLimit;

                            if (FastEnabled)
                            {
                                // Offset für Grenzwert Ausführung berechnen:
                                if ((OrderDirection == BotOrderDirection.eBuy) && (rParent.ReferredParameters.BuyOrders.LimitFast.TrailExecution))
                                    dFastExecuteOffset = Math.Min(dFastExecuteOffset, (rParent.VariationPercent - rParent.ReferredParameters.BuyOrders.LimitFast.EnableLimit));

                                else if ((OrderDirection == BotOrderDirection.eSell) && (rParent.ReferredParameters.SellOrders.LimitFast.TrailExecution))
                                    dFastExecuteOffset = Math.Max(dFastExecuteOffset, (rParent.VariationPercent - rParent.ReferredParameters.SellOrders.LimitFast.EnableLimit));
                            }

                            break;

                        default:
                            Debug.Assert(false, "[CL2783] Ungültiger Typ!");
                            break;
                    }
                }
            }
            #endregion


            private double dFastExecuteLimit;
            private double dFastExecuteOffset;

            private BotReactionIndicator rParent;
        }
        #endregion


        public BotReactionIndicator(BotReactionIndicatorParam iRefParams, Color cGraphColor, BotFormula.BotVariables vVariables) : base(iRefParams, cGraphColor)
        {
            this.BuyLimitState = new BotLimitStateInfo(this, BotOrderDirection.eBuy);
            this.BuyFormula = new BotFormula.BotCalculation(vVariables);
            this.SellLimitState = new BotLimitStateInfo(this, BotOrderDirection.eSell);
            this.SellFormula = new BotFormula.BotCalculation(vVariables);
        }


        #region Properties.Management
        [Browsable(false)]
        public new BotReactionIndicatorParam ReferredParameters { get { return (BotReactionIndicatorParam)base.ReferredParameters; } }
        [Browsable(false)]
        public BotFormula.BotCalculation BuyFormula { private set; get; }
        [Browsable(false)]
        public BotFormula.BotCalculation SellFormula { private set; get; }

        [Browsable(false)]
        public override bool IsLimitEnabled { get { return (((BuyLimitState.FastEnabled) || (BuyLimitState.SlowRating) || (SellLimitState.FastEnabled) || (SellLimitState.SlowRating))); } }
        #endregion
        #region Properties
        [DisplayName("Bestätigt")]
        [Description("Aussagekraft der aktuellen Änderung durch Tendenz bestätigt.")]
        [TypeConverter(typeof(CnvBooleanLabel.NoYes))]
        public bool DoubleCheckState { get { return (IsValid ? ((Tendency < 0) == (VariationPercent < 0)) : false); } }
        [DisplayName("Zustände Kauf-Orders")]
        [Description("Zustände von Grenzwerten zum Plazieren von Kauf-Orders.")]
        public BotLimitStateInfo BuyLimitState { private set; get; }
        [DisplayName("Zustände Verkauf-Orders")]
        [Description("Zustände von Grenzwerten zum Plazieren von Verkauf-Orders.")]
        public BotLimitStateInfo SellLimitState { private set; get; }
        #endregion


        #region Management.General
        public override bool ApplyParameters(BotFormula.BotVariables vVariables)
        {
            if (base.ApplyParameters(vVariables))
            {
                BuyFormula.Expression = ReferredParameters.BuyOrders.FormulaExpression;
                SellFormula.Expression = ReferredParameters.SellOrders.FormulaExpression;

                return (true);
            }
            else
                return (false);
        }
        public override void ResetState(BotOrderDirection oDirection, bool bWarm = false)
        {
            if (oDirection == BotOrderDirection.eBuy)
                BuyLimitState.Reset(bWarm);
            else
                SellLimitState.Reset(bWarm);
        }
        #endregion
    }
}
