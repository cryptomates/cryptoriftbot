﻿using CryptoRiftBot;
using CryptoRiftBot.Trading.Services;
using CryptoRiftBot.Trading.Strategy;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.ComponentModel;
using System.Diagnostics;
using Shared.Utils.Core;

namespace CryptoRiftBot.Trading.Functions
{
    public class BotBaseTriggerParam : BotBaseFunctionParam
    {
        public BotBaseTriggerParam(int iIndex, String sDisplayName, String sDescription) : base(sDisplayName, sDescription, new BotFunctionID(BotFunctionID.BotFunctionType.eTrigger, (iIndex + 1)))
        {
        }


        #region Configuration
        public override void WriteFile(JsonWriter jWriter, String sNodeName = "")
        {
            base.WriteFile(jWriter, sNodeName);
            {
                // WICHTIG:
                // An dieser Stelle aktuelle Node NICHT abschließen!
                // > Das wird von Sub-Class ausgeführt!
            }
        }
        public override bool ReadFile(JObject dObjects, String sNodeName = "")
        {
            if (base.ReadFile(dObjects, sNodeName))
            {
                return (true);
            }
            else
                return (false);
        }
        #endregion
    }

    public class BotTriggerParam : BotBaseTriggerParam
    {
        #region Types
        [TypeConverter(typeof(CnvEmptyExpandableObject))]
        public class BotCoolDown
        {
            public BotCoolDown()
            {
                UnsuccessfulFormula = true;
                Duration = new TimeSpan(0, 0, 0);
            }


            #region Properties
            [DisplayName("Formel erfolglos")]
            [Description("Gibt den Trigger frei sobald das Ergebnis der angewendeten Formel nicht mehr zutrifft.")]
            [ReadOnly(true)]
            [TypeConverter(typeof(CnvBooleanLabel.NoYes))]
            public bool UnsuccessfulFormula { private set; get; }
            [DisplayName(BotConvention.PROP_NAME_DURATION)]
            [Description("Gibt den Trigger frei sobald die angegebene Dauer abgelaufen ist.")]
            public TimeSpan Duration { set; get; }
            #endregion


            #region Configuration
            public void WriteFile(JsonWriter jWriter)
            {
                jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_COOLDOWN);
                jWriter.WriteStartObject();
                {
                    jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_COOLDOWN_DURATION);
                    jWriter.WriteValue(Duration);

                    jWriter.WriteEndObject();
                }
            }
            public bool ReadFile(JObject dObjects)
            {
                bool bResult = true;

                JToken _jToken = dObjects[BotConvention.DATAFILE_JSON_COOLDOWN];
                if ((bResult) && (_jToken != null))
                {
                    Duration = _jToken.ParseValue(BotConvention.DATAFILE_JSON_COOLDOWN_DURATION, Duration);
                }
                else
                    bResult = false;
                return (bResult);
            }
            #endregion
        }
        #endregion


        public BotTriggerParam(int iIndex, BotOrderDirection oDirection) : base(iIndex, "Trigger {0}", BotConvention.PROP_DESCR_TRIGGER)
        {
            this.Enabled = (iIndex == 0);
            this.EnableTrailing = true;
            this.dHysteresis = 0.5;
            this.Dependency = true;
            this.Persistent = false;
            this.Direction = oDirection;
            this.FormulaExpression = "";
            this.CoolDownCondition = new BotCoolDown();
            this.ResetMode = BotStateResetMode.eConditional;
        }


        #region Properties
        [DisplayName("Nachführung")]
        [Description("Hält den Abstand zum Grenzwert zur Ausführung der Order bei Kursänderung (Steigung bei Kauf-Orders, Gefälle bei Verkauf-Orders) konstant (Trailing).")]
        [TypeConverter(typeof(CnvBooleanLabel.DisEnabled))]
        public bool EnableTrailing { set; get; }
        [DisplayName("Hysterese")]
        [Description("Hysterese (Anteil in [%] vom Kurs) für Ausführung der Order.")]
        public double Hysteresis
        {
            get { return dHysteresis; }
            set { dHysteresis = Math.Max(0, value); }
        }
        private double dHysteresis;
        [DisplayName("Abhängigkeit")]
        [Description("Abängigkeit zu Trend-Indikator zur Platzierung von Orders.")]
        [TypeConverter(typeof(CnvBooleanLabel.OffOn))]
        public bool Dependency { set; get; }
        [DisplayName("Persistent")]
        [Description("Sobald die Hystere einmal durch zutreffende Bedingungen freigegeben wurde bleibt die Freigabe erhalten, selbst wenn die Formel anschließend fehlschlagen sollte.")]
        [TypeConverter(typeof(CnvBooleanLabel.OffOn))]
        public bool Persistent { set; get; }
        [DisplayName("Order")]
        [Description("Ausführung Order bei Auslösenden des Triggers.")]
        [TypeConverter(typeof(CnvEnumDescription))]
        public BotOrderDirection Direction { set; get; }
        [DisplayName(BotConvention.PROP_NAME_FORMULA_EXPRESSION)]
        [Description(BotConvention.PROP_DESCR_FORMULA_EXPRESSION)]
        public String FormulaExpression { set; get; }
        [DisplayName("Abklingen")]
        [Description("Bedingungen, die nach dem Auslösen des Triggers erfüllt sein müssen, bevor dieser erneut aktiviert werden kann.")]
        public BotCoolDown CoolDownCondition { set; get; }
        [DisplayName(BotConvention.PROP_NAME_RESETMODE)]
        [Description(BotConvention.PROP_DESCR_RESETMODE)]
        [TypeConverter(typeof(CnvEnumDescription))]
        public BotStateResetMode ResetMode { set; get; }
        #endregion


        #region Management
        protected override bool OnCheckPlausibility()
        {
            bool bResult = base.OnCheckPlausibility();
            if ((bResult) && (Enabled))
            {
                if (FormulaExpression.Length == 0)
                    bResult = WriteLogInvalidParam("OCP176", BotConvention.PARAM_FORMULA_EXPRESSION);
            }
            return (bResult);
        }
        public override BotStateResetMode GetResetMode(BotOrderDirection oDirection)
        {
            return (ResetMode);
        }
        #endregion


        #region Configuration
        public override void WriteFile(JsonWriter jWriter, String sNodeName = "")
        {
            base.WriteFile(jWriter, sNodeName);
            {
                CoolDownCondition.WriteFile(jWriter);

                jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_STRATEGYPARAM_TRIGGER_TRAILING);
                jWriter.WriteValue(EnableTrailing);
                jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_STRATEGYPARAM_TRIGGER_HYSTERESIS);
                jWriter.WriteValue(dHysteresis);
                jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_STRATEGYPARAM_TRIGGER_DEPENDENT);
                jWriter.WriteValue(Dependency);
                jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_STRATEGYPARAM_TRIGGER_PERSISTENT);
                jWriter.WriteValue(Persistent);
                jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_STRATEGYPARAM_TRIGGER_FORMULA_EXPRESSION);
                jWriter.WriteValue(FormulaExpression);
                jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_STRATEGYPARAM_TRIGGER_ORDER_DIRECTION);
                jWriter.WriteValue(Direction);
                jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_STRATEGYPARAM_TRIGGER_RESETMODE);
                jWriter.WriteValue(ResetMode);

                jWriter.WriteEndObject();
            }
        }
        public override bool ReadFile(JObject dObjects, String sNodeName = "")
        {
            if (base.ReadFile(dObjects, sNodeName))
            {
                CoolDownCondition.ReadFile(dObjects);

                EnableTrailing = dObjects.ParseValue(BotConvention.DATAFILE_JSON_STRATEGYPARAM_TRIGGER_TRAILING, EnableTrailing);
                dHysteresis = dObjects.ParseValue(BotConvention.DATAFILE_JSON_STRATEGYPARAM_TRIGGER_HYSTERESIS, dHysteresis);
                Dependency = dObjects.ParseValue(BotConvention.DATAFILE_JSON_STRATEGYPARAM_TRIGGER_DEPENDENT, Dependency);
                Persistent = dObjects.ParseValue(BotConvention.DATAFILE_JSON_STRATEGYPARAM_TRIGGER_PERSISTENT, Persistent);
                FormulaExpression = dObjects.ParseValue(BotConvention.DATAFILE_JSON_STRATEGYPARAM_TRIGGER_FORMULA_EXPRESSION, FormulaExpression);
                Direction = dObjects.ParseValue(BotConvention.DATAFILE_JSON_STRATEGYPARAM_TRIGGER_ORDER_DIRECTION, Direction);
                ResetMode = dObjects.ParseValue(BotConvention.DATAFILE_JSON_STRATEGYPARAM_TRIGGER_RESETMODE, ResetMode);

                return (true);
            }
            else
                return (false);
        }
        #endregion
    }

    public class BotBaseTrigger : BotBaseFunction
    {
        public BotBaseTrigger(BotBaseTriggerParam bRefParams) : base(bRefParams)
        {
        }


        #region Properties.Management
        [Browsable(false)]
        public override String SummaryName { get { return (DisplayName); } }
        #endregion
    }
    public class BotTrigger : BotBaseTrigger
    {
        public BotTrigger(BotTriggerParam bRefParams, BotFormula.BotVariables vVariables) : base(bRefParams)
        {
            this.hCalc_HysteresisParam = new BotHysteresisParam();
            this.CoolDown = new BotCoolDownTimer();
            this.Hysteresis = new BotHysteresis(hCalc_HysteresisParam);
            this.Formula = new BotFormula.BotCalculation(vVariables);

            ResetState();
        }


        #region Properties.Management
        [Browsable(false)]
        public new BotTriggerParam ReferredParameters { get { return ((BotTriggerParam)base.ReferredParameters); } }
        [Browsable(false)]
        public BotFormula.BotCalculation Formula { private set; get; }
        [Browsable(false)]
        public bool IsLimitEnabled { get { return (Hysteresis.Enabled); } }
        [Browsable(false)]
        public BotCoolDownTimer CoolDown { private set; get; }
        #endregion
        #region Properties
        [DisplayName(BotConvention.PROP_NAME_HYSTERESIS)]
        [Description("Zustände von Grenzwerten zum Plazieren von Orders.")]
        [TypeConverter(typeof(CnvEmptyExpandableObject))]
        public BotHysteresis Hysteresis { private set; get; }
        [DisplayName(BotConvention.PROP_NAME_IS_READY)]
        [TypeConverter(typeof(CnvBooleanLabel.NoYes))]
        public bool IsReady { private set; get; }
        [DisplayName(BotConvention.PROP_NAME_COOLDOWN_TIME)]
        [Description("Abzuwartende Zeit bis Trigger erneut auslösen kann.")]
        public TimeSpan CoolDownTime { get { return (CoolDown.RemainingTime); } }
        [DisplayName(BotConvention.PROP_NAME_FORMULA_RESULT)]
        [ReadOnly(true)]
        [TypeConverter(typeof(CnvEnumDescription))]
        public BotFormula.BotCalculation.BotResult FormulaResult
        {
            get
            {
                if (IsReady == false)
                    return (BotFormula.BotCalculation.BotResult.eFaild);

                else if (ReferredParameters.Persistent)
                {
                    if ((Formula.LastResult == BotFormula.BotCalculation.BotResult.eFaild) && (Hysteresis.Enabled))
                        // Ergebnis der Formel wird ignoriert, da die Hysterese bereits aktiv ist:
                        return (BotFormula.BotCalculation.BotResult.eIgnored);
                }

                return (Formula.LastResult);
            }
        }
        #endregion


        #region Events
        public override void OnOrderClosed(BotBaseOrder oOrder)
        {
            if (ReferredParameters.CoolDownCondition.Duration != TimeSpan.Zero)
                CoolDown.Activate(ReferredParameters.CoolDownCondition.Duration);
        }
        #endregion


        #region Management
        public override bool ApplyParameters(BotFormula.BotVariables vVariables)
        {
            if (base.ApplyParameters(vVariables))
            {
                Formula.Expression = ReferredParameters.FormulaExpression;

                hCalc_HysteresisParam.SetDirection(ReferredParameters.Direction);
                hCalc_HysteresisParam.TrailExecution = ReferredParameters.EnableTrailing;

                return (true);
            }
            else
                return (false);
        }
        public override void ResetState(BotOrderDirection oDirection = default(BotOrderDirection), bool bWarm = false)
        {
            IsReady = false;

            if (bWarm == false)
                CoolDown.Reset();

            Hysteresis.Reset(false);
        }
        #endregion
        #region Calculation
        public void CalcLimits(BotTradingStrategy bRefStrategy)
        {
            bool bLastReady = IsReady;

            Debug.Assert((ReferredVariable != null), "[U1945]");
            Debug.Assert((ReferredVariable.IsValid), "[U1946]");

            // Aktuellen Wert ermitteln:
            double dCurValue = ReferredVariable.Value;

            CoolDown.Refresh();

            if (ReferredParameters.Direction == BotOrderDirection.eBuy)
                IsReady = (bRefStrategy.Rift_BuyInfo.RemainingSteps.Total > 0);
            else
                IsReady = ((bRefStrategy.IsRiftActive) && (bRefStrategy.Rift_SellInfo.RemainingSteps.Default > 0));

            if ((FormulaResult != BotFormula.BotCalculation.BotResult.eSuccess) || (CoolDown.IsActive))
                IsReady = false;

            if (IsReady != bLastReady)
            {
                switch (FormulaResult)
                {
                    case BotFormula.BotCalculation.BotResult.eSuccess:
                        // Hysterese-Parameter berechnen:
                        double dTemp = (dCurValue * (ReferredParameters.Hysteresis / 100));

                        if (ReferredParameters.Direction == BotOrderDirection.eSell)
                            dTemp *= -1;

                        hCalc_HysteresisParam.EnableLimit = dCurValue;
                        hCalc_HysteresisParam.ExecuteLimit = (dCurValue + dTemp);

                        break;

                    case BotFormula.BotCalculation.BotResult.eFaild:
                        hCalc_HysteresisParam.EnableLimit = 0;
                        hCalc_HysteresisParam.ExecuteLimit = 0;

                        Hysteresis.Reset();
                        break;
                }
            }

            if (IsReady)
                Hysteresis.CalcLimits(bRefStrategy, dCurValue);
        }
        #endregion


        private BotHysteresisParam hCalc_HysteresisParam;                                      // Hystere-Parameter werden zur Laufzeit berechnet, daher sind diese auch nicht in 'TriggerParam' enthalten.
    }
}