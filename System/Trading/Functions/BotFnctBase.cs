﻿using CryptoRiftBot.Trading.Services;
using CryptoRiftBot.Trading.Strategy;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using Shared.Utils.Core;

namespace CryptoRiftBot.Trading.Functions
{
    #region Enumerations
    public enum BotAverageType
    {
        [Description("SMA")]                    // Simple Moving Average
        eSMA,
        [Description("EMA")]                    // Exponential Moving Average
        eEMA,
        [Description("VMA")]                    // Variable Moving Average
        eVMA,
        [Description("CMO")]                    // Chande Momentum Oscillator
        eCMO
    }
    public enum BotSourceType
    {
        [Description("Ticker")]
        eTicker,
        [Description("Candle-Sticks")]
        eCandleSticks,
        [Description("Variable")]
        eVariable
    }
    public enum BotSpeedType
    {
        [Browsable(false)]
        Invalid,

        [Description("Fast")]
        eFast,
        [Description("Slow")]
        eSlow
    }
    #endregion


    public class BotFunctionConverter : CnvEmptyExpandableObject
    {
        public override PropertyDescriptorCollection GetProperties(ITypeDescriptorContext iContext, object oValue, Attribute[] aAttributes)
        {
            Type tObjType = oValue.GetType();
            PropertyDescriptorCollection pResult = base.GetProperties(iContext, oValue, aAttributes);
            List<PropertyDescriptor> lProperties = new List<PropertyDescriptor>();
            Func<PropertyDescriptor, object, bool> fCondition = null;
            BotSpeedType sSpeedTypeCondition = default(BotSpeedType);
            BotOrderType sOrderTypeCondition = default(BotOrderType);
            bool bDoubleAvgValueCondition = true;
            BotAttrib.BotDoubleAvgValueAttribute bDoubleAvgValueResult;
            BotAttrib.BotRefSpeedTypeAttribute bSpeedTypeResult;
            BotAttrib.BotRefOrderConditionAttribute bOrderConditionResult;
            BotAttrib.BotRefOrderTypeAttribute bOrderTypeResult;
            bool bTypeInvalid = false;

            // Aktive Properties filtern:
            if (tObjType.IsSubclassOf(typeof(BotPlatformService)))
            {
                fCondition = ((pProp, oObject) => ((pProp.Category != BotConvention.PROP_CATEGORY_AUTH) || (((BotPlatformService)oValue).IsSimulating == false)));
            }
            else if (tObjType == typeof(BotTradingStrategy))
            {
                BotTradingStrategy tStrategy = (BotTradingStrategy)oValue;

                fCondition = (pProp, oObject) =>
                {
                    if (((pProp.Category != BotConvention.PROP_CATEGORY_COINSTREAM_REPLAY) || (tStrategy.CoinStreamPlayer != null)) &&
                        ((pProp.DisplayName != BotConvention.PROP_NAME_COOLDOWN_TIME_BUYORDERS) || (tStrategy.Calc_Buy_CoolDown.IsActive)) &&
                        ((pProp.DisplayName != BotConvention.PROP_NAME_ACTIVE_SETTINGS) || (tStrategy.ActiveSettingsClone != null)) &&
                        ((pProp.DisplayName != BotConvention.PROP_NAME_REMAINING_ACTIVE_DURATION) || ((tStrategy.GetState() == BotTradingStrategy.BotState.eActive) && (tStrategy.RemainingActiveDuration != TimeSpan.Zero))))
                    {
                        if (pProp.Category == BotConvention.PROP_CATEGORY_SETTINGS_SELECTION)
                            return ((tStrategy.UsedSelectorClone != null));
                        else
                            return ((tStrategy.ActiveSettingsClone != null) || (pProp.Category == BotConvention.PROP_CATEGORY_MANAGEMENT));
                    }
                    else
                        return (false);
                };
            }
            else if (tObjType == typeof(BotStrategySettings))
            {
            }
            else if (tObjType == typeof(BotSelectorParameters))
            {
            }
            else if (tObjType == typeof(BotSelectorParameters.BotPriorityParameters))
            {
                BotSelectorParameters.BotPriorityParameters bPrioParam = (BotSelectorParameters.BotPriorityParameters)oValue;

                fCondition = ((pProp, oObject) => ((pProp.DisplayName == BotConvention.PROP_NAME_ENABLED) || (bPrioParam.Enabled)));
            }
            else if (tObjType == typeof(BotSelectorParameters.BotDefaultSelParameters))
            {
                BotSelectorParameters.BotDefaultSelParameters bPrioParam = (BotSelectorParameters.BotDefaultSelParameters)oValue;

                fCondition = (pProp, oObject) =>
                {
                    if (pProp.DisplayName == BotConvention.PROP_NAME_KEEP_PRIORITY)
                        return (bPrioParam.Enabled == false);
                    else
                        return ((pProp.DisplayName == BotConvention.PROP_NAME_ENABLED) || (bPrioParam.Enabled));
                };
            }
            else if (tObjType == typeof(BotTradingStrategy.BotRiftInfo.BotRemainingOrderSteps))
                fCondition = ((pProp, oObject) => ((pProp.DisplayName == BotTradingStrategy.BotRiftInfo.BotRemainingOrderSteps.NAME_DEFAULT) || (((BotTradingStrategy.BotRiftInfo.BotRemainingOrderSteps)oValue).IsReserveParam)));

            else if (tObjType == typeof(BotTradingStrategy.BotRiftInfo))
            {
                BotTradingStrategy.BotRiftInfo bInfo = (BotTradingStrategy.BotRiftInfo)oValue;

                fCondition = ((pProp, oObject) => ((pProp.DisplayName != BotTradingStrategy.BotRiftInfo.NAME_NEUTRALZONE) ||
                                                    ((bInfo.ReferredSettings.CfgSell_NeutralZone != 0) && (bInfo.RemainingSteps.Direction == BotOrderDirection.eSell))));
            }
            else if (tObjType == typeof(BotStrategySettings.BotOrderSteps))
                fCondition = ((pProp, oObject) => ((pProp.DisplayName == BotStrategySettings.BotOrderSteps.NAME_DEFAULT) || (((BotStrategySettings.BotOrderSteps)oValue).IsReserveParam)));

            else if (tObjType.IsSubclassOf(typeof(BotBaseFunction)))
            {
                BotBaseFunction bFunction = (BotBaseFunction)oValue;

                if (bFunction.IsParamValid)
                {
                    if (tObjType.IsSubclassOf(typeof(BotBaseIndicator)))
                    {
                        bDoubleAvgValueCondition = ((BotBaseIndicator)oValue).ReferredParameters.IsDoubleAvgValue;
                        fCondition = ((pProp, oObject) => ((pProp.DisplayName != BotConvention.PROP_NAME_PARAM_VALID) &&
                                                            ((pProp.DisplayName != BotConvention.PROP_NAME_STATE_RESET) || (bFunction.StateResetInfo.prpIsPendingEvaluation))));
                    }
                    else if (tObjType == typeof(BotTrigger))
                    {
                        fCondition = ((pProp, oObject) => ((pProp.DisplayName != BotConvention.PROP_NAME_PARAM_VALID) &&
                                                            ((pProp.DisplayName != BotConvention.PROP_NAME_STATE_RESET) || (bFunction.StateResetInfo.prpIsPendingEvaluation)) &&
                                                            ((pProp.DisplayName != BotConvention.PROP_NAME_COOLDOWN_TIME) || (((BotTrigger)oValue).CoolDown.IsActive))));
                    }
                    else
                        bTypeInvalid = true;
                }
                else
                    fCondition = ((pProp, oObject) => (pProp.DisplayName == BotConvention.PROP_NAME_PARAM_VALID));
            }
            else if (tObjType.IsSubclassOf(typeof(BotBaseFunctionParam)))
            {
                BotBaseFunctionParam bParam = (BotBaseFunctionParam)oValue;

                if (tObjType.IsSubclassOf(typeof(BotBaseIndicatorParam)))
                {
                    BotBaseIndicatorParam bIndParam = (BotBaseIndicatorParam)oValue;

                    bDoubleAvgValueCondition = bIndParam.IsDoubleAvgValue;
                    fCondition = ((pProp, oObject) => (((pProp.DisplayName == BotConvention.PROP_NAME_ENABLED) || (bIndParam.Enabled))) &&
                                                        ((pProp.DisplayName != BotConvention.PROP_NAME_TICKER_INFLUENCE) || (bIndParam.SourceType == BotSourceType.eCandleSticks)) &&
                                                        ((pProp.DisplayName != BotConvention.PROP_NAME_REFERENCE) || (bIndParam.SourceType == BotSourceType.eVariable)));
                }
                else if (tObjType == typeof(BotTriggerParam))
                {
                    BotTriggerParam bTrigParam = (BotTriggerParam)oValue;

                    fCondition = ((pProp, oObject) => ((pProp.DisplayName == BotConvention.PROP_NAME_ENABLED) || (bTrigParam.Enabled)));
                }
                else
                    bTypeInvalid = true;
            }
            else if (tObjType == typeof(BotTriggerParam))
            {
            }
            else if (tObjType == typeof(BotTrendIndicatorParam.BotEnableLimitInfo))
            {
            }
            else if (tObjType == typeof(BotTrendIndicator.BotEnableStateInfo))
            {
            }
            else if (tObjType == typeof(BotStrategySettings.BotAverageDown))
                fCondition = ((pProp, oObject) => ((pProp.DisplayName == BotConvention.PROP_NAME_ENABLED) || (((BotStrategySettings.BotAverageDown)oValue).Enabled)));

            else if (tObjType == typeof(BotReactionIndicatorParam.BotOrderInfo))
                sSpeedTypeCondition = ((BotReactionIndicatorParam.BotOrderInfo)oValue).SpeedType;

            else if (tObjType == typeof(BotReactionIndicator.BotLimitStateInfo))
            {
                sSpeedTypeCondition = ((BotReactionIndicator.BotLimitStateInfo)oValue).SpeedType;
                fCondition = ((pProp, oObject) => ((pProp.DisplayName != BotConvention.PROP_NAME_FORMULA_RESULT) || (((BotReactionIndicator.BotLimitStateInfo)oValue).Formula.IsDefined)));
            }
            else if (tObjType == typeof(BotReactionIndicatorParam.BotDependencyInfo))
            {
            }
            else if (tObjType == typeof(BotRiftSummary))
                fCondition = ((pProp, oObject) => ((pProp.DisplayName != BotRiftSummary.NAME_CLOSED_RIFTS) || (((BotRiftSummary)oValue).ClosedRifts.TotalRifts > 0)));

            else if (tObjType == typeof(BotPlatformService.BotOrderSettings))
                sOrderTypeCondition = (BotOrderType)((BotPlatformService.BotOrderSettings)oValue).Type;

            else
                bTypeInvalid = true;

            if (bTypeInvalid)
            {
                Debug.Assert(false, "[GP926] Unbehandelter Typ!");
                return (null);
            }
            else
            {
                for (int i = 0; i < pResult.Count; i++)
                {
                    // Property auf Abhängigkeit zu 'DoubleAvgValue'-Typ prüfen:
                    bDoubleAvgValueResult = pResult[i].Attributes.FirstOrDefault<BotAttrib.BotDoubleAvgValueAttribute>();
                    if ((bDoubleAvgValueResult != null) && (bDoubleAvgValueResult.Value != bDoubleAvgValueCondition))
                        continue;

                    // Property auf Abhängigkeit zu Speed-Typ prüfen:
                    bSpeedTypeResult = pResult[i].Attributes.FirstOrDefault<BotAttrib.BotRefSpeedTypeAttribute>();
                    if ((bSpeedTypeResult != null) && (bSpeedTypeResult.Value != sSpeedTypeCondition))
                        continue;

                    // Property auf Abhängigkeit zu Order-Typ prüfen:
                    bOrderTypeResult = pResult[i].Attributes.FirstOrDefault<BotAttrib.BotRefOrderTypeAttribute>();
                    if ((bOrderTypeResult != null) && (bOrderTypeResult.Value != sOrderTypeCondition))
                        continue;

                    // Property auf Abhängigkeit zu Order-Condition prüfen:
                    bOrderConditionResult = pResult[i].Attributes.FirstOrDefault<BotAttrib.BotRefOrderConditionAttribute>();
                    if (bOrderConditionResult != null)
                    {
                        if (bOrderConditionResult.IsUsed(((BotStrategySettings)oValue).CfgSell_Condition) == false)
                            continue;
                    }

                    // Property auf Abhängigkeit zu definierter Bedingung prüfen:
                    if ((fCondition != null) && (fCondition(pResult[i], oValue) == false))
                        continue;

                    // Property anwenden:
                    lProperties.Add(pResult[i]);
                }

                return (new PropertyDescriptorCollection(lProperties.ToArray()));
            }
        }
    }
    public class BotRefVariableConverter : CnvStringList
    {
        public override StandardValuesCollection GetStandardValues(ITypeDescriptorContext iContext)
        {
            object oObject = GetContextObject(iContext);

            if (oObject != null)
            {
                Type tObjType = oObject.GetType();

                if (tObjType.IsSubclassOf(typeof(BotBaseFunctionParam)))
                {
                    BotBaseFunctionParam bParam = (BotBaseFunctionParam)oObject;
                    List<String> lResult = new List<string>();

                    // Verfügbare Variablen auflisten:
                    foreach (var vEntry in BotFormula.BotVariables.VariableNames)
                    {
                        if (vEntry.Value.Compare(bParam.FunctionID) == false)
                            lResult.Add(vEntry.Key);
                    }

                    return (new StandardValuesCollection(lResult));
                }
                else
                    Debug.Assert(false, String.Format("[GSV242] Unbekannter Typ '{0}'!", tObjType.ToString()));
            }
            else
                Debug.Assert(false, "[GSV245]");

            return (null);
        }
    }

    public class BotFunctionID
    {
        #region Enumerations
        public enum BotFunctionType
        {
            // Independant Types:
            eTicker,
            eLastOrders,
            eRift,
            eCandle,

            // Dependant Types:
            eTrendIndicator,
            eReactionIndicator,
            eTrigger
        }
        public const BotFunctionType BotFunctionType_FirstIndependant = BotFunctionType.eTicker;
        public const BotFunctionType BotFunctionType_FirstDependant = BotFunctionType.eTrendIndicator;
        #endregion


        public BotFunctionID(BotFunctionType fType, int iNumber = -1)
        {
            Type = fType;
            Number = iNumber;
        }


        #region Properties.Management
        [Browsable(false)]
        public BotFunctionType Type { set; get; }
        [Browsable(false)]
        public int Number { set; get; }
        #endregion


        #region Management
        public bool Compare(BotFunctionID fCompareID)
        {
            return ((Type == fCompareID.Type) && (Number == fCompareID.Number));
        }
        #endregion
    }

    [TypeConverter(typeof(CnvEmptyExpandableObject))]
    public class BotHysteresisParam
    {
        public BotHysteresisParam()
        {
            this.EnableLimit = 0;
            this.ExecuteLimit = 0;
            this.TrailExecution = false;

            SetDirection(BotOrderDirection.eBuy);
        }
        public BotHysteresisParam(double dEnableLimit, double dExecuteLimit, bool bTrailExecution, BotOrderDirection oDirection)
        {
            this.EnableLimit = dEnableLimit;
            this.ExecuteLimit = dExecuteLimit;
            this.TrailExecution = bTrailExecution;

            SetDirection(oDirection);
        }


        #region Properties.State
        [DisplayName("Wirksinn")]
        [Description("Gibt an ob der Wirksinn (nach Aktivierung) steigend / fallend ist.")]
        [Browsable(false)]
        public bool IsRising { private set; get; }
        #endregion
        #region Properties
        [DisplayName("Limit Aktivierung")]
        [Description("Anzunehmender Grenzwert für Freigabe einer Operation.")]
        public double EnableLimit
        {
            set
            {
                dEnableLimit = value;

                // Sicherstellen dass Ausführungs-Grenzwert nicht verletzt wird:
                ExecuteLimit = ExecuteLimit;
            }
            get { return dEnableLimit; }
        }
        private double dEnableLimit;
        [DisplayName("Limit Ausführung")]
        [Description("Anzunehmender Grenzwert für Ausführung einer Order nach dessen Freigabe (Grenzwert kann durch Aktivieren der Option '" + BotConvention.PROP_NAME_TRAILING + "' variieren).")]
        public double ExecuteLimit
        {
            set
            {
                if (IsRising)
                    dExecuteLimit = Math.Max((dEnableLimit + 0.01), value);
                else
                    dExecuteLimit = Math.Min((dEnableLimit - 0.01), value);
            }
            get { return dExecuteLimit; }
        }
        private double dExecuteLimit;
        [DisplayName(BotConvention.PROP_NAME_TRAILING)]
        [Description("Führt das Limit zur Ausführung entsprechend der Überschreitung des Limits zur Aktivierung nach.")]
        [TypeConverter(typeof(CnvBooleanLabel.OffOn))]
        public bool TrailExecution { set; get; }
        #endregion


        #region Management
        public void SetDirection(BotOrderDirection oDirection)
        {
            IsRising = (oDirection == BotOrderDirection.eBuy);
        }
        #endregion
    }
    public class BotHysteresis
    {
        public BotHysteresis(BotHysteresisParam hReference)
        {
            this.hReference = hReference;

            Reset();
        }


        #region Properties
        [DisplayName("Freigegeben")]
        [Description("")]
        [ReadOnly(true)]
        [TypeConverter(typeof(CnvBooleanLabel.OffOn))]
        public bool Enabled { private set; get; }
        [DisplayName("Grenzwert Ausführung")]
        [Description("Grenzwert (Errechnet falls Nachführen aktiviert) für Ausführung von Orders.")]
        [ReadOnly(true)]
        public double ExecuteLimit { get { return (dExecuteLimit + dExecuteOffset); } }
        #endregion


        #region Management
        public void Reset(bool bWarm = false)
        {
            if (bWarm == false)
            {
                Enabled = false;
                dExecuteLimit = 0;
            }

            dExecuteOffset = 0;
        }
        #endregion


        #region Calculation
        public void CalcLimits(BotTradingStrategy bRefStrategy, double dCurValue)
        {
            if ((bRefStrategy.Platform.IsSimulating) && (BotDebugMode.FunctionLimitsEnabled))
                Enabled = true;
            else if (Enabled == false)
            {
                if (hReference.IsRising)
                    Enabled = (dCurValue <= hReference.EnableLimit);
                else
                    Enabled = (dCurValue >= hReference.EnableLimit);
            }

            // Grenzwert für Ausführung anwenden:
            dExecuteLimit = hReference.ExecuteLimit;

            if ((Enabled) && (hReference.TrailExecution))
            {
                // Offset für Grenzwert Ausführung berechnen:
                if (hReference.IsRising)
                    dExecuteOffset = Math.Min(dExecuteOffset, (dCurValue - hReference.EnableLimit));
                else
                    dExecuteOffset = Math.Max(dExecuteOffset, (dCurValue - hReference.EnableLimit));
            }
        }
        #endregion


        private double dExecuteLimit;
        private double dExecuteOffset;

        private BotHysteresisParam hReference;
    }

    // Grenzwert-Parameter.
    public class BotLimitParam
    {
        public BotLimitParam()
        {
            Reset();
        }


        #region Properties.State
        [DisplayName("Gültig")]
        [Description("Prüft ob der aktuelle Grenzwert gültig ist.")]
        [Browsable(false)]
        public bool IsValid { get { return (LimitValue != 0); } }
        #endregion
        #region Properties
        [DisplayName("Distanz")]
        [Description("Abstand bis zu errechnetem Grenzwert.")]
        public double Distance { private set; get; }
        [DisplayName("Wert")]
        [Description("Errechneter Grenzwert.")]
        public double LimitValue { set; get; }
        #endregion


        #region Management
        public void Reset()
        {
            Distance = 0;
            LimitValue = 0;
        }
        /// <summary>
        /// Berechnet grenzwerte und übernimmt diese ggf.
        /// </summary>
        /// <param name="dCurRate"></param>
        /// <param name="dRateDistanceFactor"></param>
        /// <param name="bPositive">Berechnet den Grenzwert für Kaufen ('true') oder Verkaufen ('false').</param>
        /// <param name="bForceCalc"></param>
        /// <returns></returns>
        public bool Recalc(double dCurRate, double dRateDistanceFactor, bool bPositive = true, bool bForceCalc = false)
        {
            bool bResult;

            // Neue Grenzwerte berechnen:
            double dNextDistance = (dCurRate * dRateDistanceFactor);
            double dNextLimit;

            if (bPositive)
            {
                dNextLimit = (dCurRate + dNextDistance);
                bResult = (dNextLimit < LimitValue);
            }
            else
            {
                dNextLimit = (dCurRate - dNextDistance);
                bResult = (dNextLimit > LimitValue);
            }

            if ((bForceCalc) || (LimitValue == 0) || (bResult))
            {
                // Grenzwerte übernehmen:
                Distance = dNextDistance;
                LimitValue = dNextLimit;

                return (true);
            }
            else
                return (false);
        }
        #endregion
        #region Management
        public override string ToString()
        {
            return (LimitValue.ToString());
        }
        #endregion
    }

    // Gleitender Mittelwert.
    [TypeConverter(typeof(CnvEmptyExpandableObject))]
    public class BotMovingAverage
    {
        #region Types
        public class BotPeriodValue
        {
            #region Types
            internal class BotCMOData
            {
                #region Properties
                public double pdmS { set; get; }
                public double mdmS { set; get; }
                public double pdiS { set; get; }
                public double mdiS { set; get; }
                public double vma { set; get; }
                public double iS { set; get; }
                #endregion
            }
            #endregion


            public BotPeriodValue(double dClose)
            {
                this.Close = dClose;
            }


            #region Properties
            public double Close { set; get; }
            internal BotCMOData CMO
            {
                get
                {
                    if (cCMO == null)
                        cCMO = new BotCMOData();
                    return (cCMO);
                }
            }
            private BotCMOData cCMO;
            #endregion
        }
        public class BotAverageData
        {
            public TimeSpan tTimeStamp = default(TimeSpan);
            public double dPrice = 0;
        }
        #endregion


        public BotMovingAverage()
        {
            lData = new List<BotAverageData>();
            lPeriodValues = new List<BotPeriodValue>();
        }


        #region Properties.Management
        [DisplayName("Periodendauer")]
        [Description("Dauer eines Zeitabschnitts.")]
        [Browsable(false)]
        public TimeSpan PeriodDuration { set; get; } = default(TimeSpan);
        [DisplayName("Typ")]
        [Description("")]
        [Browsable(false)]
        public BotAverageType AverageType { set; get; }
        [DisplayName("Quelle")]
        [Description("")]
        [Browsable(false)]
        public BotSpeedType SpeedType { set; get; } = BotSpeedType.Invalid;
        [DisplayName("Anzahl Datensätze")]
        [Description("")]
        [Browsable(false)]
        public int PeriodDataCount { get { return (lPeriodValues.Count); } }
        #endregion
        #region Properties.State
        [DisplayName("Gültig")]
        [Description("Prüft ob der Durchschnitt erfolgreich berechnet werden konnte.")]
        [Browsable(false)]
        public bool IsValid { get { return ((tRefPair != null) && (lData.Count > 0)); } }
        #endregion
        #region Properties
        [DisplayName("Aktueller Wert")]
        [Description("Aktueller Wert des errechneten Durchschnittswert.")]
        public double CurrentValue { get { return (IsValid ? lData.Last().dPrice : 0); } }
        [DisplayName("Letzter Wert")]
        [Description("Letzter Wert des errechneten Durchschnittswert.")]
        [Browsable(false)]
        public double LastValue { get { return (((IsValid) && (lData.Count >= 2)) ? lData[lData.Count - 2].dPrice : 0); } }
        [DisplayName("Niedrigster Wert")]
        [Description("Niedrigster aus allen Kurswerten, welche die Grundlage zur Berechnung des aktuellen Durchschnittswert bilden.")]
        [Browsable(false)]
        public double LowValue { get { return ((lPeriodValues.Count > 0) ? lPeriodValues.Min(oVal => oVal.Close) : 0); } }
        [DisplayName("Höchster Wert")]
        [Description("Höchster aus allen Kurswerten, welche die Grundlage zur Berechnung des aktuellen Durchschnittswert bilden.")]
        [Browsable(false)]
        public double HighValue { get { return ((lPeriodValues.Count > 0) ? lPeriodValues.Max(oVal => oVal.Close) : 0); } }
        #endregion


        public List<BotAverageData> lData;


        #region Management.General
        public void Reset()
        {
            lPeriodValues.Clear();
            lData.Clear();

            tFirstTimeStamp = default(TimeSpan);
            tLastTimeStamp = default(TimeSpan);
        }
        // Aktualisiert den aktuellen Durchschnittswert.
        public bool Update(BotFormula.BotVariable vVariable)
        {
            Debug.Assert((vVariable != null), "[U1945]");
            Debug.Assert((vVariable.IsValid), "[U1946]");

            switch (SpeedType)
            {
                case BotSpeedType.eSlow:
                    return (UpdatePeriod(vVariable.Value, vVariable.UpdateTimeStamp));

                case BotSpeedType.eFast:
                    bool bReady = true;

                    if (lData.Count == 0)
                    {
                        if (lPeriodValues.Count == 0)
                        {
                            // Zeitstempel der ersten Ticker-Daten ermitteln:
                            tFirstTimeStamp = vVariable.UpdateTimeStamp;
                            bReady = false;
                        }
                        if (bReady)
                            // Prüfen ob Datensätze ausreichen um Durchschnittswert zu erzeugen:
                            bReady = ((vVariable.UpdateTimeStamp - tFirstTimeStamp) >= PeriodDuration);
                    }

                    AddToPeriod(vVariable.Value, vVariable.UpdateTimeStamp, bReady);
                    return (true);

                default:
                    Debug.Assert(false, "[U1148]");
                    break;
            }

            return (false);
        }


        private bool Initialize(BotBasePairSubscription bSubscription, BotAverageType aAverageType, TimeSpan tPeriodDuration)
        {
            Debug.Assert((bSubscription != null), "I686");

            if (tPeriodDuration != default(TimeSpan))
                this.PeriodDuration = tPeriodDuration;

            if (this.PeriodDuration == default(TimeSpan))
                Debug.Assert(false, "[I697] Periodendauer undefiniert!");
            else
            {
                switch (bSubscription.GetSubscriptionType())
                {
                    case BotSubscriptionType.eCandles:
                        SpeedType = BotSpeedType.eSlow;

                        // Aktuellen Inhat zurücksetzen:
                        Reset();

                        break;

                    case BotSubscriptionType.eTicker:
                        SpeedType = BotSpeedType.eFast;
                        break;

                    default:
                        Debug.Assert(false, "[I1991] Ungültiger Source-Type!");
                        return (false);
                }

                this.AverageType = aAverageType;

                return (true);
            }

            return (false);
        }
        #endregion
        #region Management.CandleSubscription
        public bool Initialize(BotCandleSubscription cSubscription, BotAverageType aAverageType, TimeSpan tPeriodDuration = default(TimeSpan), bool bNoninfluenced = false)
        {
            lock (cSubscription.oThreadLockObj)
            {
                if (tPeriodDuration == TimeSpan.Zero)
                    tPeriodDuration = BotCurrencyRating.CANDLES_INTERVAL;

                // Anzahl an Candle-Sticks pro Periodendauer ermitteln:
                int iPeriodDataCount = (int)Math.Round((tPeriodDuration.TotalMinutes / BotCurrencyRating.CANDLES_INTERVAL.TotalMinutes), 0, MidpointRounding.AwayFromZero);

                if (iPeriodDataCount == 0)
                    Debug.Assert(false, "[I1889] Ungültige Perioden-Dauer!");

                else if (cSubscription.lData.Count >= iPeriodDataCount)
                {
                    if (lData.Count > 0)
                    {
                        BotAverageData aLatestAvg = lData.Last();

                        // Prüfen ob Aktualisierung überflüssig ist:
                        if (aLatestAvg.tTimeStamp == cSubscription.lData.Last().TimeStamp)
                            return (true);
                    }

                    if (Initialize((BotBasePairSubscription)cSubscription, aAverageType, tPeriodDuration))
                    {
                        tRefPair = cSubscription.ReferredPair;

                        // Ersten Datensatz (Periode) erstellen:
                        BotAverageData aAverage = new BotAverageData();

                        for (int i = 0; i < iPeriodDataCount; i++)
                            lPeriodValues.Add(new BotPeriodValue(cSubscription.lData[i].CurrentValue));

                        // Durchschnittswert berechnen:
                        aAverage.dPrice = lPeriodValues.Average(oPeriod => oPeriod.Close);
                        aAverage.tTimeStamp = cSubscription.lData[iPeriodDataCount - 1].TimeStamp;

                        lData.Add(aAverage);

                        // Durchschnitt restlicher Candles ermitteln:
                        return (Update(cSubscription));
                    }
                }

                Reset();
            }

            return (false);
        }
        // Aktualisiert den aktuellen Durchschnittswert.
        public bool Update(BotCandleSubscription cCandles)
        {
            Debug.Assert((SpeedType == BotSpeedType.eSlow), "[U2071]");

            if ((IsValid) && (cCandles != null))
            {
                lock (cCandles.oThreadLockObj)
                {
                    if (lData.Count > 1)
                        // Da letzter Datensatz von Ticker-Daten geändert worden sein kann, diesen entfernen und durch Candle-Update aktualisieren lassen:
                        lData.RemoveAt(lData.Count - 1);

                    // Candle-Stick zum aktuellsten Durchschnittswert ermitteln:
                    BotAverageData aLatestAvg = lData.Last();
                    int iIndex = cCandles.lData.FindIndex(cTemp => (cTemp.TimeStamp == aLatestAvg.tTimeStamp));

                    if (iIndex != -1)
                    {
                        // Durchschnitt weiterer Candle-Sticks berechnen:
                        for (int i = (iIndex + 1); i < cCandles.lData.Count; i++)
                            AddToPeriod(cCandles.lData[i].CurrentValue, cCandles.lData[i].TimeStamp);

                        tLastTimeStamp = aLatestAvg.tTimeStamp;

                        return (true);
                    }
                }
            }

            return (false);
        }
        #endregion
        #region Management.TickerSubscription
        public bool Initialize(BotTickerSubscription tSubscription, BotAverageType aAverageType, TimeSpan tPeriodDuration = default(TimeSpan))
        {
            if (Initialize((BotBasePairSubscription)tSubscription, aAverageType, tPeriodDuration))
            {
                tRefPair = tSubscription.ReferredPair;

                return (true);
            }
            else
                return (false);
        }
        #endregion


        #region Helper
        private void AddToPeriod(double dValue, TimeSpan tTimeStamp, bool bIsReady = true)
        {
            // Neuen Wert übernehmen:
            lPeriodValues.Add(new BotPeriodValue(dValue));

            if (bIsReady)
            {
                Debug.Assert((lPeriodValues.Count > 1), "[ATP2004]");

                // Ältesten Wert entfernen:
                lPeriodValues.RemoveAt(0);

                // Durchschnitt berechnen:
                BotAverageData aNewAvg = new BotAverageData();
                aNewAvg.tTimeStamp = tTimeStamp;
                if (lData.Count == 0)
                    // Einfachen Mittelwert bilden:
                    aNewAvg.dPrice = lPeriodValues.Average(oValue => oValue.Close);
                else
                    // Typen-abhängigen Durchschnittswert berechnen:
                    aNewAvg.dPrice = CalcAverageValue(lData.Last(), dValue);
                lData.Add(aNewAvg);

                if (lData.Count > BotConvention.DISPLAY_MAX_CANDLE_COUNT)
                    // Anzahl an anzuzeigenden Datensätzen begrenzen:
                    lData.RemoveRange(0, (lData.Count - BotConvention.DISPLAY_MAX_CANDLE_COUNT));
            }
        }
        private bool UpdatePeriod(double dValue, TimeSpan tTimeStamp)
        {
            if (lPeriodValues.Count == 0)
                Debug.Assert(false, "[UP987]");
            else if (lData.Count == 0)
                Debug.Assert(false, "[UP989]");
            else if (IsValid)
            {
                // Letzten Wert ersetzen:
                lPeriodValues[lPeriodValues.Count - 1] = new BotPeriodValue(dValue);

                // Typen-abhängigen Durchschnittswert berechnen:
                lData.RemoveAt(lData.Count - 1);

                BotAverageData aNewAvg = new BotAverageData();
                aNewAvg.tTimeStamp = tTimeStamp;
                aNewAvg.dPrice = CalcAverageValue(lData.Last(), dValue);

                lData.Add(aNewAvg);
                return (true);
            }
            return (false);
        }
        private double CalcAverageValue(BotAverageData aLastAverage, double dCurrentValue)
        {
            Debug.Assert((lPeriodValues.Count > 0), "CAV985");

            // Neuster Subscription-Wert wird via gewichtetem Mittelwert in Durchschnittswert-Bildung eiberechnet.
            // > Auf diese Weise spielt nur die Anzahl an Einzelwerten (von aktuellem Mittelwert) eine Rolle, nicht aber die einzelnen Werte an sich ('lPeriodValues').
            switch (AverageType)
            {
                case BotAverageType.eSMA:
                    return (UtlMath.GetWeightedAverage(lPeriodValues.Count, 1, aLastAverage.dPrice, dCurrentValue));

                case BotAverageType.eEMA:
                    return (UtlMath.GetExponentialAverage(((double)lPeriodValues.Count + 1), aLastAverage.dPrice, dCurrentValue));

                case BotAverageType.eVMA:
                    return (UtlMath.GetVariableAverage(((double)lPeriodValues.Count + 1), aLastAverage.dPrice, dCurrentValue));

                case BotAverageType.eCMO:
                    if (lPeriodValues.Count > 1)
                    {
                        // Tutorials:
                        // > https://www.tradingview.com/script/6Ix0E5Yr-Variable-Moving-Average-LazyBear/
                        // > https://www.prorealcode.com/prorealtime-indicators/variable-moving-average-vma/
                        BotPeriodValue pLastPeriod = lPeriodValues.ElementAt(lPeriodValues.Count - 2);
                        BotPeriodValue pCurPeriod = lPeriodValues.ElementAt(lPeriodValues.Count - 1);

                        double dAlpha = (1.0 / ((double)lPeriodValues.Count + 1));
                        pCurPeriod.CMO.pdmS = ((1 - dAlpha) * pLastPeriod.CMO.pdmS + dAlpha * Math.Max((pCurPeriod.Close - pLastPeriod.Close), 0));
                        pCurPeriod.CMO.mdmS = ((1 - dAlpha) * pLastPeriod.CMO.mdmS + dAlpha * Math.Max((pLastPeriod.Close - pCurPeriod.Close), 0));

                        double s = (pCurPeriod.CMO.pdmS + pCurPeriod.CMO.mdmS);
                        pCurPeriod.CMO.pdiS = ((1 - dAlpha) * pLastPeriod.CMO.pdiS + dAlpha * (pCurPeriod.CMO.pdmS / s));
                        pCurPeriod.CMO.mdiS = ((1 - dAlpha) * pLastPeriod.CMO.mdiS + dAlpha * (pCurPeriod.CMO.mdmS / s));

                        pCurPeriod.CMO.iS = ((1 - dAlpha) * pLastPeriod.CMO.iS + dAlpha * Math.Abs(pCurPeriod.CMO.pdiS - pCurPeriod.CMO.mdiS) / (pCurPeriod.CMO.pdiS + pCurPeriod.CMO.mdiS));

                        double hhv = lPeriodValues.Max(oValue => oValue.CMO.iS);
                        double llv = lPeriodValues.Min(oValue => oValue.CMO.iS);
                        double vI = ((pCurPeriod.CMO.iS - llv) / (hhv - llv));
                        pCurPeriod.CMO.vma = UtlMath.GetVariableAverage(dCurrentValue, vI, dAlpha, pLastPeriod.CMO.vma);

                        return (pCurPeriod.CMO.vma);
                    }
                    break;

                default:
                    Debug.Assert(false, "[ATP1307]");
                    break;
            }

            return (0);
        }
        #endregion


        private BotTradingPair tRefPair = null;

        private List<BotPeriodValue> lPeriodValues;                                                                // Werte der aktuellen Periode.

        private TimeSpan tFirstTimeStamp;
        private TimeSpan tLastTimeStamp;
    }


    [TypeConverter(typeof(BotFunctionConverter))]
    public class BotBaseFunctionParam : BotBaseParamEx
    {
        #region Enumerations
        public enum BotStateResetMode
        {
            [Description("Deaktiviert")]
            eDisabled,
            [Description("Bedingt")]
            eConditional,
            [Description("Aktiviert")]
            eEnabled
        }
        #endregion


        #region Types
        [TypeConverter(typeof(BotFunctionConverter))]
        public class BotBasicOrderInfo
        {
            public BotBasicOrderInfo()
            {
                ResetMode = BotStateResetMode.eConditional;
            }


            #region Properties
            [DisplayName("Formel")]
            [Description("Zu erfüllende Bedingung zur Freigabe von Orders.")]                   // (BETA) ... TODO-Description
            public String FormulaExpression { set; get; }
            [DisplayName(BotConvention.PROP_NAME_RESETMODE)]
            [Description(BotConvention.PROP_DESCR_RESETMODE)]
            [TypeConverter(typeof(CnvEnumDescription))]
            public BotStateResetMode ResetMode { set; get; }
            #endregion


            #region Config
            public virtual void WriteFile(JsonWriter jWriter, String sNodeName)
            {
                jWriter.WritePropertyName(sNodeName);
                jWriter.WriteStartObject();
                {
                    jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_STRATEGYPARAM_INDICATOR_FORMULA_EXPRESSION);
                    jWriter.WriteValue(FormulaExpression);
                    jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_STRATEGYPARAM_INDICATOR_RESETMODE);
                    jWriter.WriteValue(ResetMode);

                    // WICHTIG:
                    // An dieser Stelle aktuelle Node NICHT abschließen!
                    // > Das wird von Sub-Class ausgeführt!
                }
            }
            public virtual bool ReadFile(JObject dObjects, String sNodeName)
            {
                bool bResult = true;

                JToken _jToken = dObjects[sNodeName];
                if ((bResult) && (_jToken != null))
                {
                    FormulaExpression = _jToken.ParseValue(BotConvention.DATAFILE_JSON_STRATEGYPARAM_INDICATOR_FORMULA_EXPRESSION, FormulaExpression);
                    ResetMode = _jToken.ParseValue(BotConvention.DATAFILE_JSON_STRATEGYPARAM_INDICATOR_RESETMODE, ResetMode);
                }
                else
                    bResult = false;
                return (bResult);
            }
            #endregion
        }
        #endregion


        protected BotBaseFunctionParam(String sDisplayName, String sDescription, BotFunctionID bFunctionID) : base(String.Format(sDisplayName, bFunctionID.Number), sDescription)
        {
            FunctionID = bFunctionID;

            // Standard-Variable referenzieren:
            ReferredVariable = string.Empty;
        }


        #region Properties.Management
        [DisplayName("Index")]
        [Description("Index-Position in übergeordnetem Funktions-Array.")]
        [Browsable(false)]
        public BotFunctionID FunctionID { private set; get; }
        #endregion
        #region Properties
        [DisplayName(BotConvention.PROP_NAME_REFERENCE)]
        [Description("Referenzierte Variable, dessen Wert zur Verarbeitung verwendet wird.")]
        [TypeConverter(typeof(BotRefVariableConverter))]
        public String ReferredVariable
        {
            get { return (sRefVariable); }
            set
            {
                if (!String.IsNullOrEmpty(value))
                {
                    // Referenzierte Variable ermitteln:
                    var vVariable = BotFormula.BotVariables.VariableNames.FirstOrDefault(oEntry => (oEntry.Key == value));
                    if (vVariable.Key == null)
                        BotEnv.LogSystem.WriteLog("RV684", BotLogProcess.eConfig, String.Format("Ungültige Variable-Referenz '{0}'!", value), BotLogType.eError);
                    else if (vVariable.Value.Compare(FunctionID))
                        BotEnv.LogSystem.WriteLog("RV687", BotLogProcess.eConfig, String.Format("Fehler beim Referenzieren der Variable '{0}': Funktionen können sich nicht selbst referenzieren!", value), BotLogType.eError);
                    else
                    {
                        sRefVariable = value;
                        return;
                    }
                }

                // Standard-Variable anwenden:
                sRefVariable = BotFormula.BotVariables.I_DEFAULT_NAME;
            }
        }
        private String sRefVariable;
        #endregion


        #region Management
        public virtual BotStateResetMode GetResetMode(BotOrderDirection oDirection)
        {
            return (BotStateResetMode.eDisabled);
        }
        #endregion
        #region Config
        public override void WriteFile(JsonWriter jWriter, String sNodeName = "")
        {
            base.WriteFile(jWriter, sNodeName);
            {
                jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_FUNCTIONPARAM_REFVARIABLE);
                jWriter.WriteValue(ReferredVariable);

                // WICHTIG:
                // An dieser Stelle aktuelle Node NICHT abschließen!
                // > Das wird von Sub-Class ausgeführt!
            }
        }
        public override bool ReadFile(JObject dObjects, String sNodeName = "")
        {
            if (base.ReadFile(dObjects, sNodeName))
            {
                ReferredVariable = dObjects.ParseValue(BotConvention.DATAFILE_JSON_FUNCTIONPARAM_REFVARIABLE, ReferredVariable);

                return (true);
            }
            else
                return (false);
        }
        #endregion
    }
    [TypeConverter(typeof(BotFunctionConverter))]
    public abstract class BotBaseFunction : DscrMetaData
    {
        #region Types
        public class BotConditionalStateResetInfo
        {
            public BotConditionalStateResetInfo()
            {
                Reset();
            }


            #region Properties
            [DisplayName("Auswertung")]
            [Description("Auswertung für Zustands-Reset dauert an.")]
            [ReadOnly(true)]
            [TypeConverter(typeof(CnvBooleanLabel.InActive))]
            public bool prpIsPendingEvaluation
            {
                get { return (prpInitPrice > 0); }
            }
            [DisplayName("Kurs")]
            [Description("Kurswert zum Zeitpunkt der Initialisierung.")]
            [ReadOnly(true)]
            public double prpInitPrice
            {
                get;
                set;
            }
            #endregion


            #region Management
            public void Reset()
            {
                prpInitPrice = 0;
            }
            #endregion
        }
        #endregion


        #region Events
        public virtual void OnOrderClosed(BotBaseOrder oOrder)
        {
        }
        #endregion


        #region Properties.Management
        [Browsable(false)]
        public BotBaseFunctionParam ReferredParameters { protected set; get; }
        [Browsable(false)]
        public BotFormula.BotVariable ReferredVariable { protected set; get; }
        #endregion
        #region Properties.Management
        [Browsable(false)]
        public override bool IsBrowsable { get { return (ReferredParameters.Enabled); } }
        #endregion
        #region Properties.Management
        [Browsable(false)]
        public abstract String SummaryName { get; }

        [Browsable(false)]
        public virtual bool IsReferredVariableUsed { get { return (true); } }
        #endregion
        #region Properties.State
        [Browsable(false)]
        public bool IsDependant { protected set; get; }
        #endregion
        #region Properties
        [DisplayName(BotConvention.PROP_NAME_PARAM_VALID)]
        [Description("Gibt an ob die Konfiguration gültig ist oder Fehler enthält (welche separat im Log-Fenster ausgegeben werden).")]
        [TypeConverter(typeof(CnvBooleanLabel.NoYes))]
        public bool IsParamValid { get { return (ReferredParameters.IsValid); } }
        [DisplayName(BotConvention.PROP_NAME_STATE_RESET)]
        [Description("Informationen über Rücksetzen des aktuellen Zustands.")]
        [TypeConverter(typeof(CnvEmptyExpandableObject))]
        public BotConditionalStateResetInfo StateResetInfo { protected set; get; }
        #endregion


        #region Management.General
        public void EvaluateStateGetResetMode(BotOrderActivator oActivator, ref double dAmount, double dLastPrice)
        {
            bool bResetState = false;

            switch (ReferredParameters.GetResetMode(oActivator.OrderDirection))
            {
                case BotBaseFunctionParam.BotStateResetMode.eEnabled:
                    bResetState = (dAmount == 0);
                    break;

                case BotBaseFunctionParam.BotStateResetMode.eConditional:
                    // Bedingtes Rücksetzen des aktuellen Zustands:
                    // > Das Ausführen einer Order wird blockiert solange Bedingungen (z.B. unzureichende Trend-Bewertung) nicht zutreffen.
                    //   Sobald dann alle Bedingungen erfüllt sind wird geprüft, ob die Order zu gleichen oder besseren Konditionen ausgeführt werden kann.

                    if (StateResetInfo.prpIsPendingEvaluation)
                    {
                        if (dAmount != 0)
                        {
                            // Konditionen prüfen:
                            if (oActivator.OrderDirection == BotOrderDirection.eBuy)
                                bResetState = (dLastPrice > StateResetInfo.prpInitPrice);
                            else
                                bResetState = (dLastPrice < StateResetInfo.prpInitPrice);

                            if (bResetState)
                                // Order blockieren:
                                dAmount = 0;

                            // Andauernde Auswertung beenden:
                            StateResetInfo.Reset();
                        }
                    }
                    else
                    {
                        if (dAmount == 0)
                            // Auswertung des bedingten Rücksetzen des aktuellen Zustands beginnen:
                            StateResetInfo.prpInitPrice = dLastPrice;
                    }

                    // [ARCHIV]:
                    /*
                    if ((dAmount == 0) && (sSaleActivator.TriggerFunction != null) && (sSaleActivator.TriggerFunction.ReferredParameters.GetResetMode(OrderDirection.OD_Sell)))
                        // Auslösenden Grenzwert zurücksetzen:
                        sSaleActivator.TriggerFunction.ResetState(OrderDirection.OD_Sell, true);
                        */

                    break;
            }

            if (bResetState)
                // Auslösenden Grenzwert zurücksetzen:
                ResetState(oActivator.OrderDirection, true);
        }
        #endregion
        #region Management.General
        // Wendet vom Benutzer definierte Parameter an.
        public virtual bool ApplyParameters(BotFormula.BotVariables vVariables)
        {
            String _sRefVar = (IsReferredVariableUsed) ? ReferredParameters.ReferredVariable : BotFormula.BotVariables.I_DEFAULT_NAME;

            // Referenzierte Variable ermitteln:
            ReferredVariable = vVariables.Find(_sRefVar);

            if (ReferredVariable != null)
                IsDependant = (BotFormula.BotVariables.VariableNames[ReferredVariable.DisplayName].Type >= BotFunctionID.BotFunctionType_FirstDependant);
            else
                IsDependant = false;

            return (ReferredVariable != null);
        }
        // Setzt den aktuellen Zustand (Z.B. Hystere, Limits) zurück.
        public virtual void ResetState(BotOrderDirection oDirection, bool bWarm = false)
        {
        }
        #endregion


        protected BotBaseFunction(BotBaseFunctionParam bRefParams) : base(bRefParams.DisplayName)
        {
            this.ReferredParameters = bRefParams;
            this.ReferredVariable = null;
            this.StateResetInfo = new BotConditionalStateResetInfo();
        }
    }
}