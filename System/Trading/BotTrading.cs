﻿using CryptoRiftBot;
using CryptoRiftBot.Trading.Functions;
using CryptoRiftBot.Trading.Strategy;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared.Utils.Core;

namespace CryptoRiftBot.Trading
{
    #region Enumerations
    /// <summary>
    /// Enumeration von implementierten Crypto-Währungen.
    /// </summary>
    [DefaultValue(eUSDollar)]
    public enum BotCoinType
    {
        [Description("")] Invalid = -1,

        [Description("EUR")] eEuro,
        [Description("USD")] eUSDollar,

        [Description("BTC")] eBitcoin,
        [Description("BCH")] eBitcoinCash,
        [Description("ETH")] eEthereum,
        [Description("ETC")] eEthereumClassic,
        [Description("XMR")] eMonero,
        [Description("IOT")] eIOTA,
        [Description("LTC")] eLitecoin,
        [Description("XRP")] eRipple,
        [Description("DSH")] eDash,
        [Description("NEO")] eNeo
    };
    public enum BotWalletType
    {
        [Description("")] Invalid = -1,

        [Description("exchange")] eExchange,
        [Description("margin")] eMargin,
        [Description("funding")] eFunding
    };
    #endregion


    #region Types
    // Handelbares Paar von Crypto-Währungen.
    public class BotTradingPair
    {
        public BotTradingPair(BotCoinType cPrimCurrency, BotCoinType cSecCurrency)
        {
            Debug.Assert(cPrimCurrency > BotCoinType.Invalid);
            Debug.Assert(cSecCurrency > BotCoinType.Invalid);

            this.CurrencyPrim = cPrimCurrency;
            this.CurrencySec = cSecCurrency;
            this.Symbol = (UtlEnum.GetEnumDescription(cPrimCurrency) + UtlEnum.GetEnumDescription(cSecCurrency));
        }


        #region Management
        public static List<BotTradingPair> lActivePairs = new List<BotTradingPair>();
        #endregion


        #region Management
        /// <summary>
        /// Ermittelt sämtliche aktive (d.h. Durch aktive Bots verwendete) Trading-Pairs.
        /// </summary>
        /// <param name="bIncludeWebReqUsers">[TRUE], falls auch Trading-Pairs berücksichtigt werden sollen, die von Web-Requests-nutzenden Strategien angewendet werden.</param>
        public static void RefreshActivePairs(bool bIncludeWebReqUsers = true)
        {
            bool _bUse;


            // (BETA) ...
            // 'bIncludeWebReqUsers' löschen wenn nicht verwendet


            lActivePairs.Clear();

            foreach (BotTradeBot _tBot in BotTradeBot.TradeBots)
            {
                foreach (BotTradingStrategy _bStrategy in _tBot.Strategies)
                {
                    // Prüfen ob Strategie berücksichtigt werden muss:
                    if ((bIncludeWebReqUsers == false) && (_tBot.ReferredPlatform.HasWebRequests))
                        _bUse = false;
                    else
                        _bUse = _bStrategy.Enabled;

                    // Ggf. aktives Trading-Pair auflisten:
                    if ((_bUse) && (_bStrategy.TradingPair.IsActive == false))
                        lActivePairs.Add(_bStrategy.TradingPair);
                }
            }
        }
        #endregion
        #region Management
        public void Reset()
        {
            dRate_LastVal = 0;
        }
        #endregion


        #region General
        public bool Compare(BotTradingPair tComparePair)
        {
            return (Compare(tComparePair.CurrencyPrim, tComparePair.CurrencySec));
        }
        // Vergleicht ob die aktuelle Instanz die gesuchten Coin-Typen definiert.
        public bool Compare(BotCoinType cPrimCurrency, BotCoinType cSecCurrency)
        {
            return ((this.CurrencyPrim == cPrimCurrency) && (this.CurrencySec == cSecCurrency));
        }
        // Vergleicht ob die aktuelle Instanz das gesuchte Symbol definiert.
        public bool Compare(String sSymbol)
        {
            return ((this.Symbol == sSymbol));
        }
        #endregion


        #region Properties.Management
        [Browsable(false)]
        public bool IsValid { get { return ((CurrencyPrim != BotCoinType.Invalid) && (CurrencySec != BotCoinType.Invalid)); } }
        #endregion
        #region Properties
        public string Symbol { protected set; get; }
        public string Title { get { return String.Format("{0} | {1}", BotCurrencyRating.CoinInfo.Find(CurrencyPrim).Name, BotCurrencyRating.CoinInfo.Find(CurrencySec).Name); } }
        public BotCoinType CurrencyPrim { protected set; get; }
        public string SymbolPrim { get { return UtlEnum.GetEnumDescription(CurrencyPrim); } }
        public BotCoinType CurrencySec { protected set; get; }
        public string SymbolSec { get { return UtlEnum.GetEnumDescription(CurrencySec); } }

        public bool IsActive { get { return (lActivePairs.Find(tPair => tPair.Compare(this)) != null); } }
        /// <summary>
        /// Letzter Kurs-Wert.
        /// </summary>
        public double LastRateValue
        {
            set
            {
                dRate_LastVal = value;
                LastRateRefresh = DateTime.Now;
            }
            get { return dRate_LastVal; }
        }
        private double dRate_LastVal = 0;
        /// <summary>
        /// Zeitpunkt der letzten Kurs-Aktualisierung.
        /// </summary>
        public DateTime LastRateRefresh { private set; get; }
        #endregion
        #region Properties.PlatformSpecification
        /// <summary>
        /// Anzahl an Nachkommastellen (Optional)
        /// </summary>
        public int Precision { set; get; }
        /// <summary>
        /// Min. Betrag einer Order (Optional)
        /// </summary>
        public double MinOrderSize { set; get; }
        /// <summary>
        /// Max. Betrag einer Order (Optional)
        /// </summary>
        public double MaxOrderSize { set; get; }
        #endregion
    }
    // Wallet einer bestimmten Crypto-Währung.
    public class BotWallet
    {
        public BotWallet(BotCoinType cCurrency, BotWalletType wType)
        {
            Debug.Assert(cCurrency > BotCoinType.Invalid);

            this.Currency = cCurrency;
            this.Funds = 0;
            this.Type = wType;
        }


        #region Properties
        [DisplayName("Währung")]
        [Description("Von Wallet genutzte Währung.")]
        public BotCoinType Currency { protected set; get; }
        [DisplayName("Guthaben")]
        [Description("Auf Wallet verfügbares Guthaben.")]
        public double Funds { set; get; }
        [DisplayName("Guthaben (in [$])")]
        [Description("Auf Wallet verfügbares Guthaben in [$] umgerechnet.")]
        public double BaseCurrencyFunds { get { return BotCurrencyRating.TranslateAmount(Funds, Currency, BotConvention.TRADE_BASE_CURRENCY); } }
        [DisplayName("Typ")]
        [Description("Typ der Wallet.")]
        public BotWalletType Type { protected set; get; }
        #endregion


        #region Management
        public bool Compare(BotCoinType cCurrency, BotWalletType wType)
        {
            return ((this.Currency == cCurrency) && (this.Type == wType));
        }
        #endregion
    }

    // Basisklasse für Parameter.
    [TypeConverter(typeof(BotFunctionConverter))]
    public class BotBaseParam : DscrMetaData
    {
        protected BotBaseParam(String sDisplayName, String sDescription) : base(sDisplayName, sDescription)
        {
            Enabled = true;
            IsValid = true;
        }


        #region Properties.Management
        [DisplayName("Gültig")]
        [Description("Gibt an ob die Konfiguration gültig ist oder Fehler enthält (welche im Log-Fenster ausgegeben werden).")]
        [Browsable(false)]
        public bool IsValid { private set; get; }
        #endregion
        #region Properties
        [DisplayName(BotConvention.PROP_NAME_ENABLED)]
        [Description("")]
        [TypeConverter(typeof(CnvBooleanLabel.OffOn))]
        public bool Enabled { set; get; }
        #endregion


        #region Events
        protected virtual bool OnCheckPlausibility()
        {
            return (true);
        }
        #endregion


        #region Management
        public override string ToString()
        {
            return ("");
        }
        #endregion
        #region Management
        public bool CheckPlausibility()
        {
            // Konfiguration auf Plausibilität prüfen:
            IsValid = OnCheckPlausibility();

            return (IsValid);
        }
        #endregion


        #region Configuration
        public virtual void WriteFile(JsonWriter jWriter, String sNodeName = "")
        {
            if (sNodeName.Length > 0)
                // Node anlegen:
                jWriter.WritePropertyName(sNodeName);

            jWriter.WriteStartObject();
            {
                jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_PARAM_ENABLED);
                jWriter.WriteValue(Enabled);

                // WICHTIG:
                // An dieser Stelle aktuelle Node NICHT abschließen!
                // > Das wird von Sub-Class ausgeführt!
            }
        }
        public virtual bool ReadFile(JObject dObjects, String sNodeName = "")
        {
            JToken _jToken = GetNode(dObjects, sNodeName);
            bool bResult = true;

            if ((bResult) && (_jToken != null))
                Enabled = _jToken.ParseValue(BotConvention.DATAFILE_JSON_PARAM_ENABLED, Enabled);
            else
                bResult = false;
            return (bResult);
        }
        #endregion
        #region Configuration
        public dynamic GetNode(JObject dObjects, String sNodeName)
        {
            if (sNodeName == null)
                return (null);

            else if (sNodeName.Length == 0)
                return (dObjects);
            else
                // Node auslesen:
                return (dObjects[sNodeName]);
        }
        #endregion


        #region Log
        protected bool WriteLogInvalidParam(String sCode, BotPropertyTitle pParameter)
        {
            return (BotEnv.LogSystem.WriteLogInvalidParam(sCode, DisplayName, pParameter));
        }
        #endregion
    }
    [TypeConverter(typeof(BotFunctionConverter))]
    public class BotBaseParamEx : BotBaseParam
    {
        protected BotBaseParamEx(String sDisplayName, String sDescription) : base(sDisplayName, sDescription)
        {
        }


        #region Properties
        [DisplayName(BotConvention.PROP_NAME_COMMENT)]
        [Description(BotConvention.PROP_DESCR_COMMENT)]
        public String Comment { set; get; }
        #endregion


        #region Configuration
        public override void WriteFile(JsonWriter jWriter, String sNodeName = "")
        {
            base.WriteFile(jWriter, sNodeName);
            {
                jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_PARAM_COMMENT);
                jWriter.WriteValue(Comment);

                // WICHTIG:
                // An dieser Stelle aktuelle Node NICHT abschließen!
                // > Das wird von Sub-Class ausgeführt!
            }
        }
        public override bool ReadFile(JObject dObjects, String sNodeName = "")
        {
            if (base.ReadFile(dObjects, sNodeName))
            {
                Comment = dObjects.ParseValue(BotConvention.DATAFILE_JSON_PARAM_COMMENT, Comment);

                return (true);
            }
            else
                return (false);
        }
        #endregion
    }

    public class BotCoolDownTimer
    {
        public BotCoolDownTimer()
        {
            Reset();
        }


        #region Properties.Manegement
        public bool IsActive
        {
            get { return (dStopPoint != default(DateTime)); }
        }
        #endregion
        #region Properties
        public TimeSpan RemainingTime
        {
            get { return (IsActive) ? (dStopPoint - DateTime.Now) : TimeSpan.Zero; }
        }
        #endregion


        #region Management
        public void Reset()
        {
            dStopPoint = default(DateTime);
        }
        public void Activate(TimeSpan tDuration)
        {
            // Zeitpunkt zum Ablauf des Cool-Downs speichern:
            dStopPoint = (DateTime.Now + tDuration);
        }
        public bool Refresh()
        {
            if (IsActive)
            {
                if (dStopPoint <= DateTime.Now)
                    // Cooldown-Zeit ist abgelaufen:
                    Reset();

                return (true);
            }
            else
                return (false);
        }
        #endregion


        #region Configuration
        public void WriteFile(JsonWriter jWriter)
        {
            if (IsActive)
            {
                jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_COOLDOWN);
                jWriter.WriteStartObject();
                {
                    jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_COOLDOWN_STOPPOINT);
                    jWriter.WriteValue(dStopPoint);

                    jWriter.WriteEndObject();
                }
            }
        }
        public bool ReadFile(JObject dObjects)
        {
            JToken _jToken = dObjects[BotConvention.DATAFILE_JSON_COOLDOWN];
            if (_jToken != null)
                dStopPoint = _jToken.ParseValue(BotConvention.DATAFILE_JSON_COOLDOWN_STOPPOINT, dStopPoint);
            return (true);
        }
        #endregion


        private DateTime dStopPoint;
    }
    #endregion
}
