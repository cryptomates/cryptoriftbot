﻿using CryptoRiftBot;
using CryptoRiftBot.Trading.Functions;
using CryptoRiftBot.Trading.Services;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing.Design;
using System.IO;
using System.Windows.Forms;
using System.Windows.Forms.Design;
using Shared.Utils.Core;

namespace CryptoRiftBot.Trading.Strategy
{
    class BotParameterConverter : TypeConverter
    {
        public override bool GetStandardValuesSupported(ITypeDescriptorContext iContext)
        {
            // ComboBox anwenden:
            return (true);
        }
        public override bool GetStandardValuesExclusive(ITypeDescriptorContext iContext)
        {
            // DropDown-Liste anwenden:
            return (true);
        }
    }
    // UI-Type Editor zum Anzeigen von 'BasicParameters'-Objekten (und nicht nur deren Bezeichnungen)
    public class BotParameterTypeEditor : UITypeEditor
    {
        #region Constants
        // Control:
        public const int CTRL_ITEM_DISPLAY_LIMIT = 8;


        public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext iContext)
        {
            return (UITypeEditorEditStyle.DropDown);
        }

        public override object EditValue(ITypeDescriptorContext iContext, IServiceProvider iProvider, object oValue)
        {
            ListBox lControl = new ListBox();

            iEditorService = (IWindowsFormsEditorService)iProvider.GetService(typeof(IWindowsFormsEditorService));

            // Auswahl erstellen:
            Type _tParamType;
            int _iIndex;
            foreach (BotBaseParameters _bParam in BotParameterManager.Content)
            {
                // Zulässige Tpen filtern:
                _tParamType = _bParam.GetType();
                if ((iContext.PropertyDescriptor.PropertyType == _tParamType) || (_tParamType.IsSubclassOf(iContext.PropertyDescriptor.PropertyType)))
                {
                    _iIndex = lControl.Items.Add(_bParam);
                    if (_bParam.Equals(oValue))
                        lControl.SelectedIndex = _iIndex;
                }
            }

            // ListBox-Control initialisieren:
            lControl.SelectionMode = SelectionMode.One;
            lControl.BorderStyle = BorderStyle.None;
            lControl.Height = (Math.Min(CTRL_ITEM_DISPLAY_LIMIT, lControl.Items.Count) * lControl.ItemHeight);

            lControl.SelectedValueChanged += OnListBoxSelectedValueChanged;

            // Anzeige-Property definieren:
            lControl.DisplayMember = BotConvention.PROP_NAME_TITLE;

            iEditorService.DropDownControl(lControl);

            return ((lControl.SelectedItem == null) ? oValue : lControl.SelectedItem);
        }

        private void OnListBoxSelectedValueChanged(object oSender, EventArgs eArgs)
        {
            iEditorService.CloseDropDown();
        }
        #endregion


        private IWindowsFormsEditorService iEditorService;
    }


    public class BotParameterManager
    {
        #region Types
        public class BotParameterList : BindingList<BotBaseParameters>
        {
            #region Management
            public void ResetPendingChanges()
            {
                Foreach(bParam => { bParam.HasPendingChanges = false; });
            }

            // Führt eine Anweisung für sämtliche Parameter durch.
            public void Foreach(Action<BotBaseParameters> aPredicate)
            {
                foreach (BotBaseParameters _bParam in this)
                    aPredicate(_bParam);
            }
            #endregion
            #region Event
            public void FireOnListChanged(ListChangedEventArgs lArgs)
            {
                base.OnListChanged(lArgs);
            }
            #endregion
        }
        #endregion


        #region Properties
        public static BotParameterList Content { private set; get; } = new BotParameterList();
        #endregion


        #region Management
        public static BotBaseParameters GetParameters(Guid gGUID)
        {
            return (GetParameters(gGUID.ToString()));
        }
        public static BotBaseParameters GetParameters(String sGUID)
        {
            // Referenzierte Parameter ermitteln:
            try
            {
                Guid gCompareGUID = new Guid(sGUID);
                foreach (BotBaseParameters _bParam in Content)
                {
                    if (_bParam.GUID.CompareTo(gCompareGUID) == 0)
                        return (_bParam);
                }
            }
            catch (FormatException)
            {
            }
            return (null);
        }


        public static void Reset()
        {
            Content.Clear();
        }
        public static bool Remove(BotBaseParameters bParameter)
        {
            if (Content.Remove(bParameter))
            {
                BotTradeBot.ForeachPlatformsStrategy((BotTradingStrategy _bStrategy) =>
                {
                    if (_bStrategy.ReferredParameters == bParameter)
                        _bStrategy.ReferredParameters = null;
                });
                return (true);
            }
            else
                return (false);
        }
        #endregion


        #region Configuration
        public static void WriteFile(JsonWriter jWriter, Type tType = null)
        {
            foreach (BotBaseParameters _bParam in Content)
            {
                if ((tType == null) || (_bParam.GetType() == tType))
                    _bParam.WriteFile(jWriter);
            }
        }
        public static bool ReadFile(dynamic dObjects, Type tType)
        {
            BotBaseParameters _bObject;
            for (int i = 0; i < dObjects.Count; i++)
            {
                if (tType == typeof(BotStrategySettings))
                    _bObject = new BotStrategySettings();
                else if (tType == typeof(BotSelectorParameters))
                    _bObject = new BotSelectorParameters();
                else
                {
                    BotEnv.LogSystem.ShowResult("RF59", BotLogProcess.eConfig, String.Format("Ungültiger Parameter-Typ '{0}'!", tType.ToString()));
                    continue;
                }

                if (_bObject.ReadFile(dObjects[i]))
                    Content.Add(_bObject);
            }
            return (true);
        }
        #endregion
    }

    [TypeConverter(typeof(BotFunctionConverter))]
    public class BotBaseParameters
    {
        #region Constants
        // Format:
        public const String FORMAT_TITLE_EX = "[{0}] {1}";
        #endregion


        protected BotBaseParameters()
        {
            GUID = Guid.NewGuid();
            Title = "Unbenannt";

            HasPendingChanges = false;
        }


        #region Properties.Management
        [Browsable(false)]
        public Guid GUID { private set; get; }
        /// <summary>
        /// Originales Objekt von aktuellem Klon ausgeben (Gibt aktuelle Instanz aus FALLS aktuelle Instanz kein Klon ist).
        /// </summary>
        [Browsable(false)]
        public BotBaseParameters Original { get { return (BotParameterManager.GetParameters(GUID)); } }

        [Browsable(false)]
        public bool HasPendingChanges { set; get; }
        #endregion
        #region Properties
        [Category(BotConvention.PROP_CATEGORY_MANAGEMENT)]
        [DisplayName(BotConvention.PROP_NAME_TITLE)]
        [Description("")]
        public String Title { set; get; }
        [Category(BotConvention.PROP_CATEGORY_MANAGEMENT)]
        [DisplayName(BotConvention.PROP_NAME_COMMENT)]
        [Description(BotConvention.PROP_DESCR_COMMENT)]
        public String Comment { set; get; }
        #endregion


        #region Configuration
        // Kopiert sämtliche Properties (via JSON "WriteFile/ReadFile"-Mechanismus).
        public bool CopyFrom(BotBaseParameters bSource)
        {
            using (MemoryStream mMemStream = new MemoryStream())
            {
                // Eigenschaften in Stream schreiben:
                using (StreamWriter sStreamWriter = new StreamWriter(mMemStream))
                using (JsonWriter jWriter = new JsonTextWriter(sStreamWriter))
                {
                    bSource.WriteFile(jWriter);

                    jWriter.Flush();

                    // Stream auf Ausgangsposition zurücksetzen:
                    mMemStream.Seek(0, SeekOrigin.Begin);

                    // Eigenschaften aus Stream lesen:
                    using (StreamReader sStreamReader = new StreamReader(mMemStream))
                    {
                        dynamic dObjects = JsonConvert.DeserializeObject<dynamic>(sStreamReader.ReadToEnd());

                        if (dObjects != null)
                        {
                            this.ReadFile(dObjects);

                            return (true);
                        }
                        else
                            Debug.Assert(false, "[C784]");
                    }
                }
            }

            BotEnv.LogSystem.WriteLog("CF271", BotLogProcess.eConfig, "Interner Fehler beim übertragen der Einstellungen.", BotLogType.eError);

            return (false);
        }
        #endregion
        #region Configuration
        public virtual void WriteFile(JsonWriter jWriter)
        {
            jWriter.WriteStartObject();
            {
                jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_STRATEGYPARAM_MGMT_GUID);
                jWriter.WriteValue(GUID);
                jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_STRATEGYPARAM_MGMT_TITLE);
                jWriter.WriteValue(Title);
                jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_STRATEGYPARAM_MGMT_COMMENT);
                jWriter.WriteValue(Comment);

                // WICHTIG:
                // An dieser Stelle aktuelle Node NICHT abschließen!
                // > Das wird von Sub-Class ausgeführt!
            }
        }
        public virtual bool ReadFile(JObject dObjects)
        {
            GUID = dObjects.ParseValue(BotConvention.DATAFILE_JSON_STRATEGYPARAM_MGMT_GUID, GUID);
            Title = dObjects.ParseValue(BotConvention.DATAFILE_JSON_STRATEGYPARAM_MGMT_TITLE, Title);
            Comment = dObjects.ParseValue(BotConvention.DATAFILE_JSON_STRATEGYPARAM_MGMT_COMMENT, Comment);

            return (true);
        }
        #endregion


        #region Management
        public override string ToString()
        {
            return (Title);
        }
        #endregion
        #region Management
        // Prüft die Konfiguration auf Plausibilität.
        public virtual bool CheckPlausibility()
        {
            return (true);
        }
        #endregion
        #region Management
        public static bool IsObjectValid(object oObject)
        {
            return ((oObject != null) && (oObject.GetType().IsSubclassOf(typeof(BotBaseParameters))));
        }

        public static bool Compare(BotBaseParameters bParam1, BotBaseParameters bParam2)
        {
            if ((bParam1 == null) && (bParam2 == null))
                return (true);

            else if ((bParam1 != null) && (bParam2 != null))
                return (bParam1.GUID.CompareTo(bParam2.GUID) == 0);

            else
                return (false);
        }
        #endregion
    }

    public class BotStrategySettings : BotBaseParameters
    {
        #region Types
        [TypeConverter(typeof(BotFunctionConverter))]
        public class BotOrderSteps
        {
            #region Constants
            // Properties:
            public const String NAME_DEFAULT = "Reaktion";
            public const String NAME_RESERVE = "Reserve";
            #endregion


            public BotOrderSteps(BotStrategySettings bParent, BotOrderDirection oDirection)
            {
                this.bParent = bParent;
                this.Direction = oDirection;
                this.Default = 2;
                this.Reserve = (oDirection == BotOrderDirection.eBuy) ? 1 : 0;
            }


            #region Properties.Management
            [Description("Gibt an ob der Reserve-Parameter verfügbar ist.")]
            [Browsable(false)]
            public bool IsReserveParam { get { return ((Direction == BotOrderDirection.eBuy) && (bParent.CfgSell_Condition != BotOrderCondition.eNone)); } }
            [Browsable(false)]
            public BotOrderDirection Direction { private set; get; }
            [Browsable(false)]
            public int Total { get { return (Default + Reserve); } }
            #endregion
            #region Properties
            [DisplayName(NAME_DEFAULT)]
            [Description("Anzahl an Schritten für normales Trading via Reaktions-Indikatoren.")]
            //  [Description("Anzahl an verbleibenden Orders bis zum Beenden des aktuellen Rifts.")]      // (BETA) ... [DESCRIPTION]
            public int Default
            {
                get { return iDefault; }
                set { iDefault = Math.Max(1, value); }
            }
            private int iDefault;
            [DisplayName(NAME_RESERVE)]
            [Description("Anzahl an Schritten für Trading in Ausnahmesituationen.")]
            public int Reserve
            {
                get { return (IsReserveParam ? iReserve : 0); }
                set { iReserve = Math.Max(0, value); }
            }
            private int iReserve;
            #endregion


            private BotStrategySettings bParent;
        }
        [TypeConverter(typeof(CnvEmptyExpandableObject))]
        public class BotPanicSale
        {
            public BotPanicSale()
            {
                this.Enabled = true;
                this.dExecuteLimit = 50;
            }


            #region Properties
            [DisplayName("Freigabe")]
            [Description("Gibt den Panik-Verkauf frei sobald der Profit-Grenzwert überschritten wird.")]
            [TypeConverter(typeof(CnvBooleanLabel.OffOn))]
            public bool Enabled { set; get; }
            [DisplayName("Limit Ausführung")]
            [Description("Zu unterschreitender Grenzwert (Anteil in [%]) für Ausführung des Panik-Verkauf. Der Grenzwert bezieht sich (je nach Bedingung) entweder auf den eingestellten Profit (Option 'Min. Profit') oder auf den durchschnittlichen Einkaufskurs (Alle weiteren Optionen).")]
            public double ExecuteLimit
            {
                get { return dExecuteLimit; }
                set { dExecuteLimit = Math.Max(1, value); }
            }
            private double dExecuteLimit;
            #endregion
        }
        [TypeConverter(typeof(BotFunctionConverter))]
        public class BotAverageDown
        {
            public BotAverageDown()
            {
                this.Enabled = true;
                this.dDistance = 70;
            }


            #region Properties
            [DisplayName("Freigabe")]
            [Description("Ermöglicht das Nachkaufen sobald der Kurs zu weit unter den durchschnittlichen Einkaufskurs fällt.")]
            [TypeConverter(typeof(CnvBooleanLabel.OffOn))]
            public bool Enabled { set; get; }
            [DisplayName("Distanz")]
            [Description("Minimaler Abstand von aktuellem Kurs zum durchschnittlichen Einkaufskurs (Anteil in [%] von durchschnittlichem Einkaufskurs) für Nachkäufe.")]
            public double Distance
            {
                get { return dDistance; }
                set { dDistance = Math.Max(1, value); }
            }
            private double dDistance;
            #endregion
        }
        [TypeConverter(typeof(CnvEmptyExpandableObject))]
        public class BotCoolDown
        {
            #region Enumerations
            public enum BotScope
            {
                [Description("Lokal")]
                eStrategy,                                                             // Betrifft die aktuelle Strategie.
                [Description("Plattform")]
                ePlatform,                                                             // Betrifft die gesamte Plattform.
                [Description("Systemweit")]
                eComplete                                                              // Betrifft sämtliche aktive Strategien auf allen aktiven Plattformen.
            }
            #endregion


            public BotCoolDown()
            {
                Scope = BotScope.eComplete;
                Duration = new TimeSpan(0, 30, 0);
            }


            #region Properties.Management
            [Browsable(false)]
            public bool IsEnabled { get { return (Duration != TimeSpan.Zero); } }
            #endregion
            #region Properties
            [DisplayName("Reichweite")]
            [Description("Definiert die betroffenen Pärchen. Diese können sein: Das auslösende Pärchen, sämtliche aktiven Pärchen der auf der Plattform des auslösenden Pärchen oder alle aktiven Pärchen.")]
            [TypeConverter(typeof(CnvEnumDescription))]
            public BotScope Scope { set; get; }
            [DisplayName(BotConvention.PROP_NAME_DURATION)]
            [Description("Gibt die betroffenen Strategien frei sobald die angegebene Dauer abgelaufen ist.")]
            public TimeSpan Duration { set; get; }
            #endregion


            #region Configuration
            public void WriteFile(JsonWriter jWriter)
            {
                jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_COOLDOWN);
                jWriter.WriteStartObject();
                {
                    jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_COOLDOWN_SCOPE);
                    jWriter.WriteValue(Scope);
                    jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_COOLDOWN_DURATION);
                    jWriter.WriteValue(Duration);

                    jWriter.WriteEndObject();
                }
            }
            public bool ReadFile(JObject dObjects)
            {
                bool bResult = true;

                JToken _jToken = dObjects[BotConvention.DATAFILE_JSON_COOLDOWN];
                if ((bResult) && (_jToken != null))
                {
                    Scope = _jToken.ParseValue(BotConvention.DATAFILE_JSON_COOLDOWN_SCOPE, Scope);
                    Duration = _jToken.ParseValue(BotConvention.DATAFILE_JSON_COOLDOWN_DURATION, Duration);
                }
                else
                    bResult = false;
                return (bResult);
            }
            #endregion
        }
        #endregion


        public BotStrategySettings()
        {
            Cfg_MinActiveDuration = new TimeSpan(0, 30, 0);

            CfgBuy_MinPrice = 50;
            CfgBuy_Condition = BotOrderCondition.eNone;
            CfgBuy_Steps = new BotOrderSteps(this, BotOrderDirection.eBuy);
            CfgBuy_AverageDown = new BotAverageDown();
            CfgNextBuy_RateDistanceFactor = 0.01;
            CfgNextBuy_Trailing = true;

            CfgSell_Condition = BotOrderCondition.eMinProphit;
            CfgSell_NeutralZone = 0.7;
            CfgNextSell_RateDistanceFactor = 0.008;
            CfgNextSell_MinProphitPercent_BuyAvgRate = 1;
            CfgNextSell_Trailing = true;
            CfgShortSale_Oppressive = true;
            CfgShortSale_Steps = new BotOrderSteps(this, BotOrderDirection.eSell);
            CfgShortSale_Panic = new BotPanicSale();

            CfgRift_StopOnClose = false;
            CfgRift_CoolDown = new BotCoolDown();

            // Rating-Indicators initialisieren:
            TrendIndicator = new BotTrendIndicatorParam(new TimeSpan(1, 0, 0, 0), new TimeSpan(2, 0, 0, 0));

            // Reaction-Indicators initialisieren:
            int _iHours = 1;
            ReactionIndicators = new BotReactionIndicatorParam[BotTradingStrategy.IND_ARRAY_COUNT];
            for (int i = 0; i < BotTradingStrategy.IND_ARRAY_COUNT; i++, _iHours++)
                ReactionIndicators[i] = new BotReactionIndicatorParam(i, new TimeSpan(_iHours, 0, 0), new TimeSpan((_iHours * 2), 0, 0));

            // Trigger initialisieren:
            Trigger = new BotTriggerParam[BotTradingStrategy.TRIG_ARRAY_COUNT]
            {
                new BotTriggerParam(0, BotOrderDirection.eBuy),
                new BotTriggerParam(1, BotOrderDirection.eSell)
            };

            // Build cumulative lists:
            Indicators = new List<BotBaseIndicatorParam>();
            Indicators.Add(TrendIndicator);
            Indicators.AddRange(ReactionIndicators);
        }


        #region Properties.Management
        [Browsable(false)]
        public List<BotBaseIndicatorParam> Indicators { private set; get; }
        #endregion
        #region Properties.Configuration
        [Category(BotConvention.PROP_CATEGORY_RATING)]
        [DisplayName(BotConvention.PROP_NAME_INDICATORS)]           // (BETA) ... Hier Name "Trend-Indikatoren" verwenden
        [Description(BotConvention.PROP_DESCR_TREND_INDICATORS)]
        public BotTrendIndicatorParam TrendIndicator { private set; get; }
        [Category(BotConvention.PROP_CATEGORY_REACTION)]
        [DisplayName(BotConvention.PROP_NAME_INDICATORS)]           // (BETA) ... Hier Name "Reaktions-Indikatoren" verwenden
        [Description(BotConvention.PROP_DESCR_REACTION_INDICATORS)]
        [TypeConverter(typeof(CnvEmptyExpandableList))]
        public BotReactionIndicatorParam[] ReactionIndicators { private set; get; }
        [Category(BotConvention.PROP_CATEGORY_REACTION)]
        [DisplayName(BotConvention.PROP_NAME_TRIGGER)]
        [Description(BotConvention.PROP_DESCR_TRIGGER)]
        [TypeConverter(typeof(CnvEmptyExpandableList))]
        public BotTriggerParam[] Trigger { private set; get; }

        [Category(BotConvention.PROP_CATEGORY_BUY_ORDERS)]
        [DisplayName("Gewichtung")]
        [Description("Entspricht Anteil am verfügbaren Gesamtbetrag von Kauf-Orders im Vergleich zu allen weiteren registrierten Sekundär-Währungen ('0' deaktiviert die Gewichtung).")]        // (Sekundär-Währungen = 'tRefPair.cSecCurrency')
        public int CfgBuy_Weight
        {
            get { return iCfgBuy_Weight; }
            set { iCfgBuy_Weight = Math.Max(0, value); }
        }
        private int iCfgBuy_Weight;
        [Category(BotConvention.PROP_CATEGORY_BUY_ORDERS)]
        [DisplayName("Anzahl Schritte")]
        [Description("Anzahl an Kauf-Orders auf die der aktuelle Anteil am verfügbaren Gesamtbetrag von Kauf-Orders aufgeteilt wird.")]
        public BotOrderSteps CfgBuy_Steps { private set; get; }
        [Category(BotConvention.PROP_CATEGORY_BUY_ORDERS)]
        [DisplayName("Min. Einkaufpreis")]
        [Description("Minimales Guthaben (in [$]) welches zur berechnung von Einkaufspreisen für Kauf-Orders verwendet wird ('0' = keie Einschränkung).")]
        public double CfgBuy_MinPrice { set; get; }
        [Category(BotConvention.PROP_CATEGORY_BUY_ORDERS)]
        [DisplayName("Max. Einkaufpreis")]
        [Description("Maximales Guthaben (in [$]) welches zur berechnung von Einkaufspreisen für Kauf-Orders verwendet wird ('0' = keie Einschränkung).")]
        public double CfgBuy_MaxPrice { set; get; }
        [Category(BotConvention.PROP_CATEGORY_BUY_ORDERS)]
        [DisplayName("Kondition")]
        [Description("Bedingt das Überschreiten entsprechender Grenzwerte zum Platzieren von Kauf-Orders. Durch bedingte Platzieren von Kauf-Orders kann durch Fallende Kurse verhindert werden!")]
        [TypeConverter(typeof(CnvEnumDescription))]
        public BotOrderCondition CfgBuy_Condition { set; get; }
        [Category(BotConvention.PROP_CATEGORY_BUY_ORDERS)]
        [DisplayName("Nachkaufen")]
        [Description("Ermöglicht das Platzieren von Kauf-Orders (via Reaktions-Indikatoren) um den durchschnittlichen Einkaufskurs zu senken.")]
        [BotAttrib.BotRefOrderConditionAttribute(BotOrderCondition.eNone, true)]
        public BotAverageDown CfgBuy_AverageDown { set; get; }
        [Category(BotConvention.PROP_CATEGORY_BUY_ORDERS)]
        [DisplayName("Faktor Grenzwert")]
        [Description("Faktor für Neuberechnung des Grenzwert zur Ausführung der nächsten Kauf-Order.")]
        public double CfgNextBuy_RateDistanceFactor { set; get; }
        [Category(BotConvention.PROP_CATEGORY_BUY_ORDERS)]
        [DisplayName("Nachführung Grenzwert")]
        [Description("Hält den Abstand zum Grenzwert der nächsten Kauf-Order bei fallendem Kurs konstant (Trailing).")]
        [TypeConverter(typeof(CnvBooleanLabel.DisEnabled))]
        public bool CfgNextBuy_Trailing { set; get; }
        [Category(BotConvention.PROP_CATEGORY_SELL_ORDERS)]
        [DisplayName("Kondition")]
        [Description("Bedingt das Überschreiten entsprechender Grenzwerte zum Platzieren von Verkauf-Orders. Durch bedingungsloses Platzieren von Verkauf-Orders können Verluste entstehen!")]
        [TypeConverter(typeof(CnvEnumDescription))]
        public BotOrderCondition CfgSell_Condition { set; get; }
        [Category(BotConvention.PROP_CATEGORY_SELL_ORDERS)]
        [DisplayName("Neutrale Zone")]
        [Description("Definiert einen Bereich (Anteil vom letzten Verkaufskurs in [%]) in dem keine Kauf-Orders plaziert werden dürfen. Dieser Bereich wird bei jedem Verkauf bestimmt und automatisch verworfen sobald der aktuelle Kurs diesen Bereich verlässt.")]
        public double CfgSell_NeutralZone { set; get; }
        [Category(BotConvention.PROP_CATEGORY_SELL_ORDERS)]
        [DisplayName("Faktor Grenzwert")]
        [Description("Faktor für Neuberechnung des Grenzwert zur Ausführung der nächsten Verkauf-Order.")]
        public double CfgNextSell_RateDistanceFactor { set; get; }
        [Category(BotConvention.PROP_CATEGORY_SELL_ORDERS)]
        [DisplayName("Min. Profit")]
        [Description("Für die Ausführung der nächstes Verkaufs-Order erforderlicher Profit (in [%]) gemessen am durchschnittlichen Einkaufskurs.")]
        public double CfgNextSell_MinProphitPercent_BuyAvgRate
        {
            get { return dCfgNextSell_MinProphitPercent_BuyAvgRate; }
            set { dCfgNextSell_MinProphitPercent_BuyAvgRate = Math.Max(0.01, value); }
        }
        private double dCfgNextSell_MinProphitPercent_BuyAvgRate;
        [Category(BotConvention.PROP_CATEGORY_SELL_ORDERS)]
        [DisplayName("Nachführung Grenzwert")]
        [Description("Hält den Abstand zum Grenzwert der nächsten Verkauf-Order bei steigendem Kurs konstant (Trailing).")]
        [TypeConverter(typeof(CnvBooleanLabel.DisEnabled))]
        public bool CfgNextSell_Trailing { set; get; }
        [Category(BotConvention.PROP_CATEGORY_RIFT)]
        [DisplayName("Stoppen bei Abschluss")]
        [Description("Deaktiviert die aktuelle Strategie nach Abschließen des aktuellen Rift.")]
        [TypeConverter(typeof(CnvBooleanLabel.OffOn))]
        public bool CfgRift_StopOnClose { set; get; }
        [Category(BotConvention.PROP_CATEGORY_RIFT)]
        [DisplayName("Abklingen bei Verlust")]
        [Description("Abklingzeit, die nach verlustreichen Rifts ablaufen muss, um erneut Kauf-Orders ausführen zu können.")]
        public BotCoolDown CfgRift_CoolDown { set; get; }
        [Category(BotConvention.PROP_CATEGORY_SHORT_SALE)]
        [DisplayName("Anzahl Schritte")]
        [Description("Anzahl an Schritten in denen ein Leerverkauf durchgeführt wird.")]
        public BotOrderSteps CfgShortSale_Steps { private set; get; }
        [Category(BotConvention.PROP_CATEGORY_SHORT_SALE)]
        [DisplayName("Verkaufen in Bedrängnis")]
        [Description("Führt einen Leerverkauf früher als geplant (d.h. in weniger Schritten) aus falls die geforderte Verkauf-Bedingung (z.B. Einhalten von minimalem Profit) nicht erfüllt werden kann.")]
        [BotAttrib.BotRefOrderConditionAttribute(BotOrderCondition.eNone, true)]
        [TypeConverter(typeof(CnvBooleanLabel.OffOn))]
        public bool CfgShortSale_Oppressive { set; get; }
        [Category(BotConvention.PROP_CATEGORY_SHORT_SALE)]
        [DisplayName("Panik-Verkauf")]
        [Description("Führt (unabhängig von der Freigabe von Verkauf-Orders) einen Leerverkauf aus wenn der Kurs unter einen definierten Grenzwert fällt.")]
        public BotPanicSale CfgShortSale_Panic { set; get; }
        [Category(BotConvention.PROP_CATEGORY_MANAGEMENT)]
        [DisplayName("Min. Einschaltzeit")]
        [Description("Dauer, die nach Aktivieren der aktuellen Einstellungen abgelaufen sein muss, um diese wieder durch automatische Funktionen (wie z.B. Selektoren oder Bot-Automatik) zu deaktivieren.")]
        public TimeSpan Cfg_MinActiveDuration { set; get; }
        #endregion


        #region Management
        public bool IsValid()
        {
            return ((CfgBuy_Steps.Default > 0) && (CfgNextSell_RateDistanceFactor > 0) && (CfgShortSale_Steps.Default > 0) && (dCfgNextSell_MinProphitPercent_BuyAvgRate > 0));
        }
        #endregion
        #region Management
        public override string ToString()
        {
            return (String.Format(FORMAT_TITLE_EX, "Set", base.ToString()));
        }
        public override bool CheckPlausibility()
        {
            bool bResult = base.CheckPlausibility();
            if (bResult)
            {
                foreach (BotBaseIndicatorParam bParam in Indicators)
                {
                    if (bParam.CheckPlausibility() == false)
                        bResult = false;
                }
                foreach (BotBaseTriggerParam bTrigger in Trigger)
                {
                    if (bTrigger.CheckPlausibility() == false)
                        bResult = false;
                }
            }
            return (bResult);
        }
        #endregion


        #region Configuration
        public override void WriteFile(JsonWriter jWriter)
        {
            base.WriteFile(jWriter);
            {
                jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_STRATEGYPARAM_BUY_WEIGHT);
                jWriter.WriteValue(CfgBuy_Weight);
                jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_STRATEGYPARAM_BUY_STEPS_DEFAULT);
                jWriter.WriteValue(CfgBuy_Steps.Default);
                jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_STRATEGYPARAM_BUY_STEPS_RESERVE);
                jWriter.WriteValue(CfgBuy_Steps.Reserve);
                jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_STRATEGYPARAM_BUY_MINPRICE);
                jWriter.WriteValue(CfgBuy_MinPrice);
                jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_STRATEGYPARAM_BUY_MAXPRICE);
                jWriter.WriteValue(CfgBuy_MaxPrice);
                jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_STRATEGYPARAM_BUY_CONDITION);
                jWriter.WriteValue(CfgBuy_Condition);
                jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_STRATEGYPARAM_BUY_AVGDOWN_ENABLED);
                jWriter.WriteValue(CfgBuy_AverageDown.Enabled);
                jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_STRATEGYPARAM_BUY_AVGDOWN_DISTANCE);
                jWriter.WriteValue(CfgBuy_AverageDown.Distance);
                jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_STRATEGYPARAM_NEXTBUY_RATEDISTANCEFACTOR);
                jWriter.WriteValue(CfgNextBuy_RateDistanceFactor);
                jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_STRATEGYPARAM_NEXTBUY_TRAILING);
                jWriter.WriteValue(CfgNextBuy_Trailing);

                jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_STRATEGYPARAM_SELL_CONDITION);
                jWriter.WriteValue(CfgSell_Condition);
                jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_STRATEGYPARAM_SELL_NEUTRALZONE);
                jWriter.WriteValue(CfgSell_NeutralZone);
                jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_STRATEGYPARAM_NEXTSELL_RATEDISTANCEFACTOR);
                jWriter.WriteValue(CfgNextSell_RateDistanceFactor);
                jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_STRATEGYPARAM_NEXTSELL_MINPROPHITPERCENT_BUYAVGRATE);
                jWriter.WriteValue(CfgNextSell_MinProphitPercent_BuyAvgRate);
                jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_STRATEGYPARAM_NEXTSELL_TRAILING);
                jWriter.WriteValue(CfgNextSell_Trailing);
                jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_STRATEGYPARAM_SHORTSALE_STEPS);
                jWriter.WriteValue(CfgShortSale_Steps.Default);
                jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_STRATEGYPARAM_SHORTSALE_OPPRESSIVE);
                jWriter.WriteValue(CfgShortSale_Oppressive);
                jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_STRATEGYPARAM_SHORTSALE_PANIC_ENABLED);
                jWriter.WriteValue(CfgShortSale_Panic.Enabled);
                jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_STRATEGYPARAM_SHORTSALE_PANIC_EXECUTELIMIT);
                jWriter.WriteValue(CfgShortSale_Panic.ExecuteLimit);

                jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_STRATEGYPARAM_RIFT_STOPONCLOSE);
                jWriter.WriteValue(CfgRift_StopOnClose);
                CfgRift_CoolDown.WriteFile(jWriter);

                jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_STRATEGYPARAM_CFG_MINACTIVEDURATION);
                jWriter.WriteValue(Cfg_MinActiveDuration);

                // Indikatoren speichern:
                jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_TREND_INDICATOR);
                TrendIndicator.WriteFile(jWriter);

                jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_REACTION_INDICATORS);
                jWriter.WriteStartArray();
                {
                    for (int i = 0; i < BotTradingStrategy.IND_ARRAY_COUNT; i++)
                        ReactionIndicators[i].WriteFile(jWriter);

                    jWriter.WriteEnd();
                }

                // Trigger speichern:
                jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_TRIGGERS);
                jWriter.WriteStartArray();
                {
                    for (int i = 0; i < BotTradingStrategy.TRIG_ARRAY_COUNT; i++)
                        Trigger[i].WriteFile(jWriter);

                    jWriter.WriteEnd();
                }

                jWriter.WriteEndObject();
            }
        }
        public override bool ReadFile(JObject dObjects)
        {
            bool bResult = false;

            if (base.ReadFile(dObjects))
            {
                dynamic dIndicators;

                CfgBuy_Weight = dObjects.ParseValue(BotConvention.DATAFILE_JSON_STRATEGYPARAM_BUY_WEIGHT, CfgBuy_Weight);
                CfgBuy_Steps.Default = dObjects.ParseValue(BotConvention.DATAFILE_JSON_STRATEGYPARAM_BUY_STEPS_DEFAULT, CfgBuy_Steps.Default);
                CfgBuy_Steps.Reserve = dObjects.ParseValue(BotConvention.DATAFILE_JSON_STRATEGYPARAM_BUY_STEPS_RESERVE, CfgBuy_Steps.Reserve);
                CfgBuy_MinPrice = dObjects.ParseValue(BotConvention.DATAFILE_JSON_STRATEGYPARAM_BUY_MINPRICE, CfgBuy_MinPrice);
                CfgBuy_MaxPrice = dObjects.ParseValue(BotConvention.DATAFILE_JSON_STRATEGYPARAM_BUY_MAXPRICE, CfgBuy_MaxPrice);
                CfgBuy_Condition = dObjects.ParseValue(BotConvention.DATAFILE_JSON_STRATEGYPARAM_BUY_CONDITION, CfgBuy_Condition);
                CfgBuy_AverageDown.Enabled = dObjects.ParseValue(BotConvention.DATAFILE_JSON_STRATEGYPARAM_BUY_AVGDOWN_ENABLED, CfgBuy_AverageDown.Enabled);
                CfgBuy_AverageDown.Distance = dObjects.ParseValue(BotConvention.DATAFILE_JSON_STRATEGYPARAM_BUY_AVGDOWN_DISTANCE, CfgBuy_AverageDown.Distance);
                CfgNextBuy_RateDistanceFactor = dObjects.ParseValue(BotConvention.DATAFILE_JSON_STRATEGYPARAM_NEXTBUY_RATEDISTANCEFACTOR, CfgNextBuy_RateDistanceFactor);
                CfgNextBuy_Trailing = dObjects.ParseValue(BotConvention.DATAFILE_JSON_STRATEGYPARAM_NEXTBUY_TRAILING, CfgNextBuy_Trailing);

                CfgSell_Condition = dObjects.ParseValue(BotConvention.DATAFILE_JSON_STRATEGYPARAM_SELL_CONDITION, CfgSell_Condition);
                CfgSell_NeutralZone = dObjects.ParseValue(BotConvention.DATAFILE_JSON_STRATEGYPARAM_SELL_NEUTRALZONE, CfgSell_NeutralZone);
                CfgNextSell_RateDistanceFactor = dObjects.ParseValue(BotConvention.DATAFILE_JSON_STRATEGYPARAM_NEXTSELL_RATEDISTANCEFACTOR, CfgNextSell_RateDistanceFactor);
                CfgNextSell_MinProphitPercent_BuyAvgRate = dObjects.ParseValue(BotConvention.DATAFILE_JSON_STRATEGYPARAM_NEXTSELL_MINPROPHITPERCENT_BUYAVGRATE, CfgNextSell_MinProphitPercent_BuyAvgRate);
                CfgNextSell_Trailing = dObjects.ParseValue(BotConvention.DATAFILE_JSON_STRATEGYPARAM_NEXTSELL_TRAILING, CfgNextSell_Trailing);
                CfgShortSale_Steps.Default = dObjects.ParseValue(BotConvention.DATAFILE_JSON_STRATEGYPARAM_SHORTSALE_STEPS, CfgShortSale_Steps.Default);
                CfgShortSale_Oppressive = dObjects.ParseValue(BotConvention.DATAFILE_JSON_STRATEGYPARAM_SHORTSALE_OPPRESSIVE, CfgShortSale_Oppressive);
                CfgShortSale_Panic.Enabled = dObjects.ParseValue(BotConvention.DATAFILE_JSON_STRATEGYPARAM_SHORTSALE_PANIC_ENABLED, CfgShortSale_Panic.Enabled);
                CfgShortSale_Panic.ExecuteLimit = dObjects.ParseValue(BotConvention.DATAFILE_JSON_STRATEGYPARAM_SHORTSALE_PANIC_EXECUTELIMIT, CfgShortSale_Panic.ExecuteLimit);

                CfgRift_StopOnClose = dObjects.ParseValue(BotConvention.DATAFILE_JSON_STRATEGYPARAM_RIFT_STOPONCLOSE, CfgRift_StopOnClose);
                CfgRift_CoolDown.ReadFile(dObjects);

                Cfg_MinActiveDuration = dObjects.ParseValue(BotConvention.DATAFILE_JSON_STRATEGYPARAM_CFG_MINACTIVEDURATION, Cfg_MinActiveDuration);

                // Indikatoren auslesen:
                dIndicators = dObjects[BotConvention.DATAFILE_JSON_TREND_INDICATOR];

                if (dIndicators != null)
                    bResult = TrendIndicator.ReadFile(dIndicators);

                if (bResult)
                {
                    dIndicators = dObjects[BotConvention.DATAFILE_JSON_REACTION_INDICATORS];

                    if (dIndicators != null)
                    {
                        // Einzelne Reaktions-Indikatoren auslesen:
                        for (int i = 0; i < Math.Min(BotTradingStrategy.IND_ARRAY_COUNT, dIndicators.Count); i++)
                            ReactionIndicators[i].ReadFile(dIndicators[i]);
                    }
                }

                // Trigger auslesen:
                if (bResult)
                {
                    dynamic dTriggers = dObjects[BotConvention.DATAFILE_JSON_TRIGGERS];

                    if (dTriggers != null)
                    {
                        // Einzelne Trigger auslesen:
                        for (int i = 0; i < Math.Min(BotTradingStrategy.TRIG_ARRAY_COUNT, dTriggers.Count); i++)
                            Trigger[i].ReadFile(dTriggers[i]);
                    }
                }
            }

            return (bResult);
        }
        #endregion
    }

    public class BotSelectorParameters : BotBaseParameters
    {
        #region Types
        public class BotPriorityParameters : BotBaseParamEx
        {
            public BotPriorityParameters(int iIndex) : base(String.Format("Priorität {0}", (iIndex + 1)), BotConvention.PROP_DESCR_PRIORITY)
            {
                this.Enabled = (iIndex == 0);
                this.FormulaExpression = "";
            }


            #region Properties
            [DisplayName(BotConvention.PROP_NAME_ENABLED)]
            [Description("")]
            public new bool Enabled
            {
                get { return (base.Enabled); }
                set
                {
                    if (value == false)
                        Settings = null;
                    base.Enabled = value;
                }
            }
            #endregion
            #region Properties
            [DisplayName(BotConvention.PROP_NAME_FORMULA_EXPRESSION)]
            [Description(BotConvention.PROP_DESCR_FORMULA_EXPRESSION)]
            public String FormulaExpression { set; get; }
            #endregion
            #region Properties.References
            [DisplayName("Referenzierte Einstellungen")]
            [Description("Definiert die Einstellungen welche durch Auswahl der aktuellen Priorität zur Laufzeit zum Ausführen einer Strategie angewendet werden.")]
            [TypeConverter(typeof(BotParameterConverter))]
            [Editor(typeof(BotParameterTypeEditor), typeof(UITypeEditor))]
            public BotStrategySettings Settings { set; get; }
            #endregion


            #region Management
            protected override bool OnCheckPlausibility()
            {
                bool bResult = base.OnCheckPlausibility();

                if ((bResult) && (Enabled))
                {
                    if (Settings == null)
                        bResult = WriteLogInvalidParam("OCP1013", BotConvention.PARAM_REF_SETTINGS);

                    if (FormulaExpression.Length == 0)
                        bResult = WriteLogInvalidParam("OCP1016", BotConvention.PARAM_FORMULA_EXPRESSION);
                }

                return (bResult);
            }
            #endregion


            #region Configuration
            public override void WriteFile(JsonWriter jWriter, String sNodeName = "")
            {
                base.WriteFile(jWriter, sNodeName);
                {
                    jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_SELECTOR_REF_SETTINGS);
                    jWriter.WriteValue((Settings == null) ? "" : Settings.GUID.ToString());

                    jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_SELECTOR_FORMULA_EXPRESSION);
                    jWriter.WriteValue(FormulaExpression);

                    jWriter.WriteEndObject();
                }
            }
            public override bool ReadFile(JObject dObjects, String sNodeName = "")
            {
                bool bResult = base.ReadFile(dObjects, sNodeName);
                JToken _jToken = GetNode(dObjects, sNodeName);
                if ((bResult) && (_jToken != null))
                {
                    Settings = (BotStrategySettings)BotParameterManager.GetParameters((String)_jToken.ParseValue(BotConvention.DATAFILE_JSON_SELECTOR_REF_SETTINGS, ""));

                    FormulaExpression = _jToken.ParseValue(BotConvention.DATAFILE_JSON_SELECTOR_FORMULA_EXPRESSION, FormulaExpression);
                }
                return (bResult);
            }
            #endregion
        }
        public class BotDefaultSelParameters : BotBaseParam
        {
            public BotDefaultSelParameters() : base(BotConvention.PROP_NAME_DEFAULT_SETTINGS, "")
            {
                KeepPriority = false;
            }


            #region Properties
            [DisplayName(BotConvention.PROP_NAME_ENABLED)]
            [Description("")]
            public new bool Enabled
            {
                get { return (base.Enabled); }
                set
                {
                    if (value == false)
                        Settings = null;
                    base.Enabled = value;
                }
            }
            #endregion
            #region Properties
            [DisplayName(BotConvention.PROP_NAME_KEEP_PRIORITY)]
            [Description("Behält die zuletzt aktive Priorität bei nachdem dessen Bedingungen nicht mehr zutreffen, jedoch keine Standard-Einstellungen definiert sind.")]
            [TypeConverter(typeof(CnvBooleanLabel.NoYes))]
            public bool KeepPriority { set; get; }
            #endregion
            #region Properties.References
            [DisplayName("Referenzierte Einstellungen")]
            [Description("Definiert die Standard-Einstellungen die zur Laufzeit zum Ausführen einer Strategie angewendet werden.")]
            [TypeConverter(typeof(BotParameterConverter))]
            [Editor(typeof(BotParameterTypeEditor), typeof(UITypeEditor))]
            public BotStrategySettings Settings { set; get; }
            #endregion


            #region Configuration
            public override void WriteFile(JsonWriter jWriter, String sNodeName = "")
            {
                base.WriteFile(jWriter, sNodeName);
                {
                    jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_SELECTOR_REF_SETTINGS);
                    jWriter.WriteValue((Settings == null) ? "" : Settings.GUID.ToString());
                    jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_SELECTOR_KEEP_PRIORITY);
                    jWriter.WriteValue(KeepPriority);

                    jWriter.WriteEndObject();
                }
            }
            public override bool ReadFile(JObject dObjects, String sNodeName = "")
            {
                bool bResult = base.ReadFile(dObjects, sNodeName);
                JToken _jToken = GetNode(dObjects, sNodeName);
                if ((bResult) && (_jToken != null))
                {
                    Settings = (BotStrategySettings)BotParameterManager.GetParameters((String)_jToken.ParseValue(BotConvention.DATAFILE_JSON_SELECTOR_REF_SETTINGS, ""));
                    KeepPriority = _jToken.ParseValue(BotConvention.DATAFILE_JSON_SELECTOR_KEEP_PRIORITY, KeepPriority);
                }
                return (bResult);
            }
            #endregion


            #region Management
            protected override bool OnCheckPlausibility()
            {
                bool bResult = base.OnCheckPlausibility();

                if ((bResult) && (Enabled))
                {
                    if (Settings == null)
                        bResult = WriteLogInvalidParam("OCP1105", BotConvention.PARAM_REF_SETTINGS);
                }

                return (bResult);
            }
            #endregion
        }
        #endregion


        public BotSelectorParameters() : base()
        {
            // Prioritäten initialisieren:
            PriorityParameters = new BotPriorityParameters[BotTradingStrategy.BotSelectorInfo.PRIO_ARRAY_COUNT];
            for (int i = 0; i < BotTradingStrategy.BotSelectorInfo.PRIO_ARRAY_COUNT; i++)
                PriorityParameters[i] = new BotPriorityParameters(i);

            // Standard-parameter initialisieren:
            DefaultParameters = new BotDefaultSelParameters();
        }


        #region Properties
        [Category(BotConvention.PROP_CATEGORY_SETTINGS_SELECTION)]
        [DisplayName(BotConvention.PROP_NAME_PRIORITY)]
        [Description(BotConvention.PROP_DESCR_PRIORITY)]
        [TypeConverter(typeof(CnvEmptyExpandableList))]
        public BotPriorityParameters[] PriorityParameters { private set; get; }
        [Category(BotConvention.PROP_CATEGORY_SETTINGS_SELECTION)]
        [DisplayName(BotConvention.PROP_NAME_DEFAULT_SETTINGS)]
        [Description("Definiert die Einstellungen welche zur Laufzeit zum Ausführen einer Strategie angewendet werden wenn keine Auswahl durch Prioritäten getroffen wird.")]
        public BotDefaultSelParameters DefaultParameters { private set; get; }
        #endregion


        #region Management
        public override string ToString()
        {
            return (String.Format(FORMAT_TITLE_EX, "->", base.ToString()));
        }
        public override bool CheckPlausibility()
        {
            bool bResult = base.CheckPlausibility();

            if (bResult)
            {
                // Prioritäten auf Plausibilität prüfen:
                foreach (BotPriorityParameters pPriority in PriorityParameters)
                {
                    if (pPriority.CheckPlausibility() == false)
                        bResult = false;
                }

                // Standard-Parameter auf Plausibilität prüfen:
                if (DefaultParameters.CheckPlausibility() == false)
                    bResult = false;
            }

            return (bResult);
        }
        #endregion


        #region Configuration
        public override void WriteFile(JsonWriter jWriter)
        {
            base.WriteFile(jWriter);
            {
                // Prioritäten speichern:
                jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_PRIORITIES);
                jWriter.WriteStartArray();
                {
                    foreach (BotPriorityParameters pPriority in PriorityParameters)
                        pPriority.WriteFile(jWriter);

                    jWriter.WriteEnd();
                }

                DefaultParameters.WriteFile(jWriter, BotConvention.DATAFILE_JSON_DEFAULT_PARAMETERS);

                jWriter.WriteEndObject();
            }
        }
        public override bool ReadFile(JObject dObjects)
        {
            bool bResult = base.ReadFile(dObjects);

            if (bResult)
            {
                dynamic dObjPriorities = dObjects[BotConvention.DATAFILE_JSON_PRIORITIES];

                if (dObjPriorities != null)
                {
                    // Einzelne Prioritäten auslesen:
                    for (int i = 0; i < Math.Min(BotTradingStrategy.BotSelectorInfo.PRIO_ARRAY_COUNT, dObjPriorities.Count); i++)
                        PriorityParameters[i].ReadFile(dObjPriorities[i]);
                }

                DefaultParameters.ReadFile(dObjects, BotConvention.DATAFILE_JSON_DEFAULT_PARAMETERS);
            }

            return (bResult);
        }
        #endregion
    }
}
