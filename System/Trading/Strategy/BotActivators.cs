﻿using CryptoRiftBot.Trading.Functions;
using CryptoRiftBot.Trading.Services;
using System;
using System.ComponentModel;
using System.Diagnostics;
using Shared.Utils.Core;

namespace CryptoRiftBot.Trading.Strategy
{
    // Aktivatoren für Kauf. 
    public class BotOrderActivator
    {
        #region Enumerations
        public enum BotTriggerReason
        {
            [Description("")]
            None = 0,

            [Description("Benutzer")]
            eUser,
            [Description("Panik")]
            ePanic,
            [Description("Indikator")]
            eIndicator,
            [Description("Trigger")]
            eTrigger
        }
        public enum BotLockReason
        {
            [Description("Kondition 'Slow' unerfüllt")]
            eCondition_Slow = 0x01,                                               // Bedingung für Platzieren von Orders (via 'Slow'-Indikator) wurde nicht erreicht
            [Description("Kondition 'Fast' unerfüllt")]
            eCondition_Fast = 0x02,                                               // Bedingung für Platzieren von Orders (via 'Fast'-Indikator) wurde nicht erreicht
            [Description("Guthaben nicht ausreichend")]
            eInsufficientFunds = 0x04,                                            // Guthaben nicht ausreichend
            [Description("Max. Anzahl Schritte")]
            eStepLimit_Reached = 0x08,                                            // Max. Anzahl an Schritten erreicht
            [Description("Abklingzeit aktiv")]
            eCoolDown = 0x10,                                                     // CoolDown für Plazierung von Orders aktiv

            [Description("Trend-Bewertung nicht ausreichend")]
            eTrendIndicator = 0x100,                                              // [Trend-Indikator]: Bewertung nicht ausreichend

            [Description("Neutrale Zone")]
            eNeutralZone = 0x200                                                  // [Neutrale Zone]: Kurs befindet sich in neutraler Zone
        };
        public const int BotLockReason_Count = 4;
        public const uint BotLockReason_Valid = 0;
        #endregion


        #region Types
        // Blockierung für Orders.
        public class BotLock
        {
            public BotLock()
            {
                Reset();
            }


            #region Properties
            [Browsable(false)]
            public uint Reasons { private set; get; }
            #endregion


            #region Management
            public void Reset()
            {
                Reasons = BotLockReason_Valid;
            }
            public void Add(BotLockReason lReason)
            {
                Reasons |= (uint)lReason;
            }
            public void Remove(BotLockReason lReason)
            {
                Reasons &= ~(uint)lReason;
            }
            public void Conditional(bool bConditionResult, BotLockReason lReason)
            {
                if (bConditionResult)
                    Remove(lReason);
                else
                    Add(lReason);
            }
            #endregion
            #region Management.State
            public bool IsSet(BotLockReason lReason)
            {
                return ((Reasons & (uint)lReason) != 0);
            }
            public bool IsValid(uint uUnusedReasons = 0)
            {
                if (Reasons == BotLockReason_Valid)
                    return (true);
                else
                {
                    // Ungenutzte Gründe invertieren:
                    if (((uUnusedReasons ^ uint.MaxValue) & Reasons) == BotLockReason_Valid)
                        return (true);
                    else
                        return (false);
                }
            }
            #endregion
        }
        public BotOrderActivator()
        {
            Lock = new BotLock();

            ResetTrigger();
        }
        #endregion


        #region Properties.Management
        [Description("Idikator für aktives Platzieren einer Verkauf-Order.")]
        [Browsable(false)]
        public bool IsActive { get { return (TriggerReason != BotTriggerReason.None); } }
        [Browsable(false)]
        public BotTriggerReason TriggerReason { internal set; get; }
        [Browsable(false)]
        public BotBaseFunction TriggerFunction { internal set; get; }
        [Browsable(false)]
        public BotLock Lock { private set; get; }
        #endregion
        #region Properties.Management
        [Browsable(false)]
        public virtual String Description { get { return (UtlEnum.GetEnumDescription(TriggerReason)); } }
        [Browsable(false)]
        public virtual BotOrderDirection OrderDirection { get { return BotOrderDirection.eBuy; } }
        #endregion


        #region Management
        public virtual void ResetTrigger(bool bWarm = false)
        {
            TriggerFunction = null;

            // Ggf. Grund zurücksetzen:
            if (bWarm)
            {
                switch (TriggerReason)
                {
                    // Folgende Gründe werden nur via Kalt-Reset zurück gesetzt:
                    case BotTriggerReason.eUser:
                        return;
                }
            }

            TriggerReason = BotTriggerReason.None;
        }
        public virtual bool CanExecuteOrder()
        {
            if (TriggerReason == BotTriggerReason.eUser)
            {
                // Trigger sofort zurücksetzen:
                TriggerReason = BotTriggerReason.None;

                return (true);
            }

            if (IsUnlocked(TriggerFunction) == false)
                return (false);

            else if (TriggerFunction != null)
            {
                Type tType = TriggerFunction.GetType();
                BotFormula.BotCalculation.BotResult rResult = BotFormula.BotCalculation.BotResult.eSuccess;

                if (tType == typeof(BotReactionIndicator))
                {
                    BotReactionIndicator _bIndicator = (BotReactionIndicator)TriggerFunction;

                    // Bestätigung der Änderung durch Tendenz prüfen:
                    if ((_bIndicator.ReferredParameters.Dependency.DoubleCheck) && (_bIndicator.DoubleCheckState == false))
                        return (false);

                    // Ergebnis der Formel ermitteln:
                    BotReactionIndicator.BotLimitStateInfo lInfo = (OrderDirection == BotOrderDirection.eBuy) ? _bIndicator.BuyLimitState : _bIndicator.SellLimitState;

                    if (lInfo.Formula.IsDefined)
                        rResult = lInfo.FormulaResult;
                    else
                        // Keine Formel definiert:
                        rResult = BotFormula.BotCalculation.BotResult.eSuccess;
                }
                else if (tType == typeof(BotTrigger))
                    rResult = ((BotTrigger)TriggerFunction).FormulaResult;

                else
                    Debug.Assert(false, "[CEO225] Unbekannter Typ!");

                if (rResult == BotFormula.BotCalculation.BotResult.eFaild)
                    return (false);
            }

            return (true);
        }


        protected virtual uint OnGetUnusedLockReasons(BotReactionIndicator rRefIndicator = null, bool bUseIndividual = true)
        {
            return (0);
        }
        #endregion
        #region Management
        public bool IsUnlocked(BotBaseFunction bRefFunction = null, bool bUseIndividual = true)
        {
            uint uUnusedReasons = 0;

            if (bRefFunction != null)
            {
                Type tType = bRefFunction.GetType();
                if (tType == typeof(BotReactionIndicator))
                {
                    Debug.Assert((BotOrderActivator.BotLockReason_Count == 4), "[ULOR1409] Neue Konstanten berücksichtigen!");

                    // Ungenutzte Reasons ermitteln:
                    BotReactionIndicator _bIndicator = (BotReactionIndicator)bRefFunction;
                    switch (_bIndicator.SpeedType)
                    {
                        case BotSpeedType.eSlow: uUnusedReasons |= (uint)BotOrderActivator.BotLockReason.eCondition_Fast; break;
                        case BotSpeedType.eFast: uUnusedReasons |= (uint)BotOrderActivator.BotLockReason.eCondition_Slow; break;
                    }
                }
                else if (tType == typeof(BotTrigger))
                {
                    uUnusedReasons |= (uint)BotOrderActivator.BotLockReason.eCondition_Fast;
                    uUnusedReasons |= (uint)BotOrderActivator.BotLockReason.eCondition_Slow;
                }
                else
                    Debug.Assert(false, "[IU262] Unbekannter Typ!");

                if (IsUsingTrendIndicator(bRefFunction) == false)
                    // Keine Abhängigkeiten zu Trend-Indikator:
                    uUnusedReasons |= (uint)BotOrderActivator.BotLockReason.eTrendIndicator;
            }

            // Weitere ungenutzte Gründe (falls vorhanden) ermitteln:
            uUnusedReasons |= OnGetUnusedLockReasons();

            if (bUseIndividual == false)
                // Nicht zu verwendende Flags für gemeinsame Nutzung entfernen:
                uUnusedReasons |= (uint)BotOrderActivator.BotLockReason.eTrendIndicator;

            return (Lock.IsValid(uUnusedReasons));
        }
        #endregion


        #region Helper
        protected bool IsUsingTrendIndicator(BotBaseFunction bRefFunction)
        {
            Type tType = bRefFunction.GetType();

            if (tType == typeof(BotReactionIndicator))
            {
                switch (((BotReactionIndicator)bRefFunction).ReferredParameters.Dependency.OrderDependency)
                {
                    case BotReactionIndicatorParam.BotOrderDependency.eNone:
                        // Indikator ist von Trend-Bewertung unabhängig.
                        break;

                    case BotReactionIndicatorParam.BotOrderDependency.eEnabled_BuyOrders:
                        return (OrderDirection == BotOrderDirection.eBuy);

                    case BotReactionIndicatorParam.BotOrderDependency.eEnabled_SellOrders:
                        return (OrderDirection == BotOrderDirection.eSell);

                    case BotReactionIndicatorParam.BotOrderDependency.eEnabled_AllOrders:
                        return (true);

                    default:
                        Debug.Assert(false, "[IUTI315] Ungültiger Typ!");
                        break;
                }
            }
            else if (tType == typeof(BotTrigger))
            {
                BotTrigger tTrigger = (BotTrigger)bRefFunction;

                if (tTrigger.ReferredParameters.Dependency)
                    return (tTrigger.ReferredParameters.Direction == OrderDirection);
            }
            else
                Debug.Assert(false, "[IUTI327] Unbekannter Typ!");

            return (false);
        }
        #endregion
    }
    // Aktivatoren für Einkauf. 
    public class BotBuyActivator : BotOrderActivator
    {
        #region Constants
        public const short MARKER_COUNT = 2;

        public const short MARKER_AVERAGEDOWN_SLOW = 0;
        public const short MARKER_AVERAGEDOWN_FAST = 1;
        #endregion


        #region Properties.Marker
        [Browsable(false)]
        public bool AverageDown_Slow
        {
            get { return bMarker[MARKER_AVERAGEDOWN_SLOW]; }
            set { bMarker[MARKER_AVERAGEDOWN_SLOW] = value; }
        }
        [Browsable(false)]
        public bool AverageDown_Fast
        {
            get { return bMarker[MARKER_AVERAGEDOWN_FAST]; }
            set { bMarker[MARKER_AVERAGEDOWN_FAST] = value; }
        }
        #endregion


        #region Management
        public override void ResetTrigger(bool bWarm = false)
        {
            base.ResetTrigger(bWarm);

            if (bWarm == false)
            {
                // Marker zurücksetzen:
                for (short i = 0; i < MARKER_COUNT; i++)
                    bMarker[i] = false;
            }
        }
        #endregion


        private bool[] bMarker = new bool[MARKER_COUNT];
    }
    // Aktivatoren für Verkauf. 
    public class BotSellActivator : BotOrderActivator
    {
        #region Constants
        public const short MARKER_COUNT = 1;

        public const short MARKER_NEXTSELL_OPPRESSIVE = 0;
        #endregion


        #region Properties.Management
        [Description("Idikator für kompletten Leerverkauf.")]
        [Browsable(false)]
        public bool IsShortSale
        {
            get
            {
                switch (TriggerReason)
                {
                    case BotTriggerReason.ePanic: return (true);
                }
                if (bMarker[MARKER_NEXTSELL_OPPRESSIVE])
                    return (true);
                return (false);
            }
        }
        #endregion
        #region Properties.Management
        [Browsable(false)]
        public override String Description
        {
            get
            {
                String sResult = base.Description;
                if (bMarker[MARKER_NEXTSELL_OPPRESSIVE])
                    sResult += " (in Bedrängnis)";
                return (sResult);
            }
        }
        [Browsable(false)]
        public override BotOrderDirection OrderDirection { get { return BotOrderDirection.eSell; } }
        #endregion
        #region Properties.Marker
        [Browsable(false)]
        public bool NextSellOppressive
        {
            get { return bMarker[MARKER_NEXTSELL_OPPRESSIVE]; }
            set { bMarker[MARKER_NEXTSELL_OPPRESSIVE] = value; }
        }
        #endregion


        #region Management
        public override void ResetTrigger(bool bWarm = false)
        {
            base.ResetTrigger(bWarm);

            if (bWarm == false)
            {
                // Marker zurücksetzen:
                for (short i = 0; i < MARKER_COUNT; i++)
                    bMarker[i] = false;
            }
        }
        public override bool CanExecuteOrder()
        {
            switch (TriggerReason)
            {
                // In jedem Fall verkaufen bei folgenden Gründen:
                case BotTriggerReason.ePanic:
                    return (true);
            }

            return (base.CanExecuteOrder());
        }
        #endregion


        private bool[] bMarker = new bool[MARKER_COUNT];
    }
}
