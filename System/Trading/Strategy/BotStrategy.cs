﻿using CryptoRiftBot.Config;
using CryptoRiftBot.Trading.Functions;
using CryptoRiftBot.Trading.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Design;
using System.Linq;
using System.Windows.Forms;
using Shared.Utils.Core;
using Newtonsoft.Json.Linq;

namespace CryptoRiftBot.Trading.Strategy
{
    #region Enumerations
    public enum BotOrderCondition
    {
        [Description("Keine")]
        eNone,                                                                      // Orders ohne Bedingungen plazieren.
        [Description("Min. Einkauf-Durchschnitt")]
        eMinBuyAverage,                                                             // Grenzwert für durchschnittlichen Einkaufskurs berücksichtigen.
        [Description("Min. Profit")]
        eMinProphit                                                                 // Grenzwert für min. Profit berücksichtigen.
    }
    #endregion


    [TypeConverter(typeof(BotFunctionConverter))]
    public class BotTradingStrategy
    {
        #region Constants
        // Indikatoren:
        public const int IND_ARRAY_COUNT = 5;

        // Trigger:
        public const int TRIG_ARRAY_COUNT = 2;

        // Ticker Subscription:
        public const int TS_MAX_ENTRIES = 10;                                       // Maximale Anzahl an Ticker-Daten

        // Platform-Specific:
        public const int PS_MAX_RETRY_COUNT_DEFAULT = 3;
        #endregion


        #region Enumerations
        public enum BotState
        {
            eError,                                                                 // Ein Fehler ist aufgetreten
            eDisabled,                                                              // Strategie deaktiviert
            eIdle,                                                                  // Leerlauf (Keine Settings aktiv)
            eInit,                                                                  // Initialisiert
            eCalculating,                                                           // Berechnung wird ausgeführt
            eActive,                                                                // Trading-Funktion wird ausgeführt
        };
        public enum BotFormulaUpdateMode
        {
            eAll,                                                                   // Aktualisierung aller Variablen

            eDependant,                                                             // Aktualisierung aller Variablen, die nicht in 'FUM_Independant' inbegriffen sind
            eIndependant                                                            // Aktualisierung von Indikatoren, die ABHÄNGIG von weiteren Indikatoren arbeiten
        };
        #endregion


        #region Types
        // Speichert Informationen zu einem Interval.
        public class BotIntervalData
        {
            public double dAverage = 0;
            public double dLowest = 0;
            public double dHighest = 0;
        }
        // Sollwert.
        public class BotSetpoint
        {
            public BotSetpoint(double dValue = 0, double dMinLimit = -1, double dMaxLimit = -1)
            {
                this.MinLimit = dMinLimit;
                this.MaxLimit = dMaxLimit;

                this.Value = dValue;
            }


            #region Properties.Management
            [DisplayName("Min. Limit")]
            [Description("Kleinster anzunehmender Wert")]
            [Browsable(false)]
            public double MinLimit { set; get; }
            [DisplayName("Max. Limit")]
            [Description("Größter anzunehmender Wert")]
            [Browsable(false)]
            public double MaxLimit { set; get; }
            #endregion
            #region Properties
            [DisplayName("Aktueller Wert")]
            public double Value
            {
                set
                {
                    dValue = value;
                    if (MinLimit != -1)
                        dValue = Math.Max(MinLimit, dValue);
                    if (MaxLimit != -1)
                        dValue = Math.Min(MaxLimit, dValue);
                }
                get { return (dValue); }
            }
            private double dValue;
            #endregion
        }
        [TypeConverter(typeof(CnvEmptyExpandableObject))]
        public class BotLimitState
        {
            #region Properties
            [TypeConverter(typeof(CnvBooleanLabel.NoYes))]
            public bool Active { set; get; }
            public double ExecuteRate { set; get; }
            #endregion


            #region Management
            public void Reset()
            {
                Active = false;
                ExecuteRate = 0;
            }
            #endregion
        }
        [TypeConverter(typeof(CnvEmptyExpandableObject))]
        public class BotLastOrderInfo
        {
            public BotLastOrderInfo()
            {
                Reset();
            }


            #region Properties.Management
            [Browsable(false)]
            public bool IsValid { get { return (dCreationTime != default(DateTime)); } }
            #endregion
            #region Properties
            [DisplayName("Kurs")]
            [Description("")]
            public double Rate { private set; get; }
            [DisplayName("Vergangene Zeit")]
            [Description("Seit Ausführung der Order vergangene Zeit.")]
            public TimeSpan PastTime { private set; get; }
            #endregion


            #region Management
            public void Reset()
            {
                Rate = 0;
                dCreationTime = default(DateTime);
                PastTime = TimeSpan.Zero;
            }
            public bool Update(BotTickerSubscription tTicker)
            {
                if ((IsValid) && (tTicker.lData.Count > 0))
                {
                    BotTickerSubscription.BotTickerData tLastData = tTicker.lData.Last();

                    // Vergangene Zeit ermitteln:
                    PastTime = (tLastData.dReceivedTime - dCreationTime);

                    return (true);
                }
                else
                    return (false);
            }
            public void Apply(BotBaseOrder oOrder)
            {
                Reset();


                // WEITER MACHEN
                // warum wird 'tPastTime' falsch angezeigt bei coinstreams!??



                dCreationTime = oOrder.CreationTime;
                Rate = oOrder.dRate;
            }
            #endregion


            #region Configuration
            public void WriteFile(JsonWriter jWriter, String sNode)
            {
                jWriter.WritePropertyName(sNode);
                jWriter.WriteStartObject();
                {
                    jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_STAT_LASTORDERINFO_CREATIONTIME);
                    jWriter.WriteValue(dCreationTime);
                    jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_STAT_LASTORDERINFO_RATE);
                    jWriter.WriteValue(Rate);

                    jWriter.WriteEndObject();
                }
            }
            public bool ReadFile(JToken jToken, String sNode)
            {
                bool bResult = true;

                JToken _jToken = jToken[sNode];
                if ((bResult) && (_jToken != null))
                {
                    dCreationTime = _jToken.ParseValue(BotConvention.DATAFILE_JSON_STAT_LASTORDERINFO_CREATIONTIME, dCreationTime);
                    Rate = _jToken.ParseValue(BotConvention.DATAFILE_JSON_STAT_LASTORDERINFO_RATE, Rate);
                }
                else
                    bResult = false;
                return (bResult);
            }
            #endregion


            private DateTime dCreationTime;
        }
        // Enthält Informationen zum aktuellen Rift.
        [TypeConverter(typeof(BotFunctionConverter))]
        public class BotRiftInfo
        {
            #region Constants
            public const String NAME_NEUTRALZONE = "NeutraleZone";
            #endregion


            #region Types
            [TypeConverter(typeof(BotFunctionConverter))]
            public class BotRemainingOrderSteps
            {
                #region Constants
                public const String NAME_DEFAULT = "Reaktion";
                public const String NAME_RESERVE = "Reserve";
                #endregion


                public BotRemainingOrderSteps(BotRiftInfo bParent, BotOrderDirection oDirection)
                {
                    this.bParent = bParent;
                    this.Direction = oDirection;
                }


                #region Properties.Management
                [Description("Gibt an ob der Reserve-Parameter verfügbar ist.")]
                [Browsable(false)]
                public bool IsReserveParam { get { return ((Direction == BotOrderDirection.eBuy) && (bParent.ReferredSettings.CfgSell_Condition != BotOrderCondition.eNone)); } }
                [Browsable(false)]
                public BotOrderDirection Direction { private set; get; }
                [Browsable(false)]
                public int Total { get { return (Default + Reserve); } }
                [Browsable(false)]
                public bool IsInReserve { get { return ((Default == 0) && (Reserve > 0)); } }
                #endregion
                #region Properties
                [DisplayName(NAME_DEFAULT)]
                [Description("Anzahl an verbleibenden Orders für normales Trading via Reaktions-Indikatoren.")]
                //  [Description("Anzahl an verbleibenden Orders bis zum Beenden des aktuellen Rifts.")]      // (BETA) ... [DESCRIPTION]
                [ReadOnly(true)]
                public int Default { set; get; }
                [DisplayName(NAME_RESERVE)]
                [Description("Anzahl an verbleibenden Orders für Trading in Ausnahmesituationen.")]
                [ReadOnly(true)]
                public int Reserve { set; get; }
                #endregion

                #region Management
                public void Count(BotStrategySettings.BotOrderSteps oSteps, bool bIncrement)
                {
                    if (bIncrement)
                    {
                        // Schrittweise erhöhen (Reserve-Schritte priorisieren):
                        if (Reserve < oSteps.Reserve)
                            Reserve++;
                        else if (Default < oSteps.Default)
                            Default++;
                    }
                    else
                    {
                        // Schrittweise verringern (Default-Schritte priorisieren):
                        if (Default > 0)
                            Default--;
                        else if (Reserve > 0)
                            Reserve--;
                        else
                            Debug.Assert(false, "[C853] Verringern verbleibender Schritte nicht möglich!");
                    }
                }
                // Ermittelt die gesamte Anzahl an Schritten unter bestimmten Bedingungen.
                public int GetTotalStepsEx(BotBuyActivator bBuyActivator, BotSpeedType bType)
                {
                    int iTotal = Default;

                    Debug.Assert((bBuyActivator != null), "[GTS830]");

                    // Bedingungen prüfen
                    // > Nachkaufen möglich?
                    if ((bType == BotSpeedType.eSlow) ? bBuyActivator.AverageDown_Slow : bBuyActivator.AverageDown_Fast)
                        // Nachkaufen ist freigegeben:
                        iTotal += Reserve;

                    return (iTotal);
                }
                public int GetTotalStepsEx(BotBuyActivator bBuyActivator)
                {
                    Debug.Assert((bBuyActivator != null), "[GTS846]");

                    if (bBuyActivator.TriggerFunction != null)
                    {
                        Type tType = bBuyActivator.TriggerFunction.GetType();
                        BotSpeedType bType = BotSpeedType.Invalid;

                        if (tType == typeof(BotReactionIndicator))
                            bType = ((BotReactionIndicator)bBuyActivator.TriggerFunction).SpeedType;

                        else if (tType == typeof(BotTrigger))
                            bType = BotSpeedType.eFast;

                        else
                            Debug.Assert(false, "[CEO225] Unbekannter Typ!");

                        if (bType != BotSpeedType.Invalid)
                            // Bedingungen auswerten:
                            return (GetTotalStepsEx(bBuyActivator, bType));
                    }

                    return (Default);
                }
                #endregion


                private BotRiftInfo bParent;
            }

            [TypeConverter(typeof(CnvEmptyExpandableObject))]
            public class BotNeutralZone
            {
                public BotNeutralZone(BotStrategySettings bRefSettings)
                {
                    ReferredSettings = bRefSettings;

                    Reset();
                }


                #region Properties.Management
                [Browsable(false)]
                public BotStrategySettings ReferredSettings { private set; get; }
                #endregion
                #region Properties
                [DisplayName("Aktiv")]
                [ReadOnly(true)]
                [TypeConverter(typeof(CnvBooleanLabel.NoYes))]
                public bool Active { get { return ((0 < LimitMin) && (LimitMin < LimitMax)); } }
                [DisplayName("Unterer Grenzwert")]
                [ReadOnly(true)]
                public double LimitMin { private set; get; }
                [DisplayName("Oberer Grenzwert")]
                [ReadOnly(true)]
                public double LimitMax { private set; get; }
                #endregion


                #region Management
                public void Reset()
                {
                    LimitMin = 0;
                    LimitMax = 0;
                }
                public bool Activate(double dCurRate)
                {
                    if (ReferredSettings.CfgSell_NeutralZone == 0)
                        return (false);
                    else
                    {
                        double dRange = ((ReferredSettings.CfgSell_NeutralZone * dCurRate) / 100);

                        LimitMax = (dCurRate + (dRange / 2));
                        LimitMin = Math.Max(0, (LimitMax - dRange));

                        return (true);
                    }
                }
                public void Update(double dCurRate)
                {
                    if (Active)
                    {
                        if ((dCurRate < LimitMin) || (dCurRate > LimitMax))
                            // Neutrale Zone wird verlassen:
                            Reset();
                    }
                }
                #endregion
            }
            #endregion


            public BotRiftInfo(BotStrategySettings bRefSettings, BotOrderDirection oDirection)
            {
                ReferredSettings = bRefSettings;
                NextOrderLimit = new BotLimitParam();
                RemainingSteps = new BotRemainingOrderSteps(this, oDirection);
                NeutralZone = new BotNeutralZone(bRefSettings);
            }


            #region Properties.Management
            [Browsable(false)]
            public BotStrategySettings ReferredSettings { private set; get; }
            #endregion
            #region Properties
            [DisplayName("Order-Grenzwert")]
            [Description("Grenzwert zum Ausführen der nächsten Order.")]
            //  [Description("Grenzwert zum Ausführen der nächsten Order (bei überschreitendem Kurs).")]      // (BETA) ... [DESCRIPTION]
            public BotLimitParam NextOrderLimit { private set; get; }
            [DisplayName("Verbleibende Orders")]
            [Description("Anzahl an verbleibenden Orders.")]
            [ReadOnly(true)]
            public BotRemainingOrderSteps RemainingSteps { set; get; }
            [DisplayName(NAME_NEUTRALZONE)]
            [ReadOnly(true)]
            public BotNeutralZone NeutralZone { set; get; }
            #endregion


            #region Management
            public void Reset(bool bWarm = false)
            {
                if (bWarm == false)
                    NeutralZone.Reset();

                NextOrderLimit.Reset();
            }
            public void ResetRift(BotStrategySettings.BotOrderSteps oSteps = null)
            {
                // Verbleibende Anzahl an Schritten zurücksetzen:
                RemainingSteps.Default = ((oSteps == null) ? 0 : oSteps.Default);
                RemainingSteps.Reserve = ((oSteps == null) ? 0 : oSteps.Reserve);

                Reset(true);
            }
            #endregion
        }

        [TypeConverter(typeof(CnvEmptyExpandableObject))]
        public class BotRiftSummaryCollection
        {
            #region Enumerations
            public enum BotRiftRating
            {
                eBad,                                                                 // Rift mit Verlust abgeschlossen
                eNeutral,
                eGood,                                                                // Rift mit Profit abgeschlossen
            }
            #endregion


            public BotRiftSummaryCollection(BotTradingPair tRefPair)
            {
                this.ReferredPair = tRefPair;
                this.CurrentRift = new BotRiftSummary(tRefPair);
                this.WeeklyRiftSummary = new List<BotRiftSummary>();
                this.TotalRiftSummary = new List<BotRiftSummary>();

                // Typen für Rift-Summary initialisieren:
                for (int i = 0; i < UtlDateTime.REPEAT_TYPE_COUNT; i++)
                    TotalRiftSummary.Add(new BotRiftSummary(tRefPair));
            }


            #region Properties.Statistik
            [Category(BotConvention.PROP_CATEGORY_GENERAL)]
            [DisplayName("Aktueller Rift")]
            [Description("Zusammenfassende Informationen zum aktuellen Rift.")]
            public BotRiftSummary CurrentRift { private set; get; }
            [Category(BotConvention.PROP_CATEGORY_SUMMARY_CUMULATED)]
            [DisplayName("Wöchentlich")]
            [Description("Kumulierte Zusammenfassungen abgeschlossener Rifts der aktuellen Woche.")]
            public BotRiftSummary Total_WeeklyRift { get { return (TotalRiftSummary[(int)UtlDateTime.RepeatType.SRT_Weekly]); } }
            [Category(BotConvention.PROP_CATEGORY_SUMMARY_CUMULATED)]
            [DisplayName("Monatlich")]
            [Description("Kumulierte Zusammenfassungen abgeschlossener Rifts des aktuellen Monats.")]
            public BotRiftSummary Total_MonthlyRift { get { return (TotalRiftSummary[(int)UtlDateTime.RepeatType.SRT_Monthly]); } }
            [Category(BotConvention.PROP_CATEGORY_SUMMARY_CUMULATED)]
            [DisplayName("Jährlich")]
            [Description("Kumulierte Zusammenfassungen abgeschlossener Rifts des aktuellen Jahres.")]
            public BotRiftSummary Total_YearlyRift { get { return (TotalRiftSummary[(int)UtlDateTime.RepeatType.SRT_Yearly]); } }
            [Category(BotConvention.PROP_CATEGORY_SUMMARY_CUMULATED)]
            [DisplayName("Gesamt")]
            [Description("Kumulierte Zusammenfassungen abgeschlossener Rifts insgesamt.")]
            public BotRiftSummary Total_EverRift { get { return (TotalRiftSummary[(int)UtlDateTime.RepeatType.SRT_Ever]); } }
            #endregion
            #region Properties.Management
            [Browsable(false)]
            public BotTradingPair ReferredPair { private set; get; }
            /// <summary>
            /// Enthält einzelne Zusammenfassungen abgeschlossener Rifts der aktuellen Woche.
            /// </summary>
            [Browsable(false)]
            public List<BotRiftSummary> WeeklyRiftSummary { private set; get; }
            /// <summary>
            /// Enthält kumulierte Zusammenfassungen abgeschlossener Rifts getrennt nach Zeiträumen (Siehe 'Type').
            /// </summary>
            [Browsable(false)]
            public List<BotRiftSummary> TotalRiftSummary { private set; get; }
            [Browsable(false)]
            public bool IsRiftActive { get { return (CurrentRift.Buys.Orders > 0); } }
            #endregion


            #region Events
            public void OnOrderFinished(BotBaseOrder oOrder)
            {
                CurrentRift.OnOrderFinished(oOrder);

                for (int i = 0; i < UtlDateTime.REPEAT_TYPE_COUNT; i++)
                {
                    if (TotalRiftSummary[i].Active == false)
                        TotalRiftSummary[i].Begin = DateTime.Now;
                }
            }
            public BotRiftRating OnRiftClosed()
            {
                DateTime dCurTime = DateTime.Now;
                BotTradingPair tRefPair = CurrentRift.ReferredPair;
                BotRiftRating rRating = BotRiftRating.eNeutral;

                if (WeeklyRiftSummary.Count > 0)
                {
                    // Summaries entfernen wenn sie nicht zur aktuellen Woche gehören:
                    if (UtlDateTime.IsExceeded(WeeklyRiftSummary[0].Begin, dCurTime, UtlDateTime.RepeatType.SRT_Weekly))
                        WeeklyRiftSummary.Clear();
                }

                for (int i = 0; i < UtlDateTime.REPEAT_TYPE_COUNT; i++)
                {
                    if ((TotalRiftSummary[i].Active) && (UtlDateTime.IsExceeded(TotalRiftSummary[i].Begin, dCurTime, (UtlDateTime.RepeatType)i)))
                    {
                        TotalRiftSummary[i].Reset();

                        // Beginn-Zeitpunkt von aktuell abgeschlossenem Rift übernehmen:
                        TotalRiftSummary[i].Begin = CurrentRift.Begin;
                    }
                }

                CurrentRift.OnRiftClosed();
                CurrentRift.FinalizeSummary();

                // Summary zur aktuellen Woche hinzufügen:
                WeeklyRiftSummary.Add(CurrentRift);

                // Zusammenfassende Summen-Informationen für abgeschlossenem Rift erstellen:
                // > Übernimmt alle Werte des aktuellen Rifts (z.B. auch Azahl abgeschlossener Rifts)
                for (int i = 0; i < UtlDateTime.REPEAT_TYPE_COUNT; i++)
                    TotalRiftSummary[i] += CurrentRift;

                // Information ausgeben:
                CurrentRift.WriteLog("ORC1884", BotLogProcess.eRift);

                if (CurrentRift.Prophit < 0)
                    rRating = BotRiftRating.eBad;
                else if (CurrentRift.Prophit > 0)
                    rRating = BotRiftRating.eGood;

                // Neuen Rift erzeugen:
                CurrentRift = new BotRiftSummary(tRefPair);

                return (rRating);
            }
            #endregion


            #region Configuration
            public void WriteFile(JsonWriter jWriter)
            {
                // Wöchentliche Rifts speichern:
                jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_RIFTS_WEEKLY);
                jWriter.WriteStartArray();
                {
                    for (int i = 0; i < WeeklyRiftSummary.Count; i++)
                        WeeklyRiftSummary[i].WriteFile(jWriter);

                    jWriter.WriteEnd();
                }

                // Summen-Rifts speichern:
                jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_RIFTS_TOTAL);
                jWriter.WriteStartArray();
                {
                    for (int i = 0; i < TotalRiftSummary.Count; i++)
                        TotalRiftSummary[i].WriteFile(jWriter);

                    jWriter.WriteEnd();
                }
            }
            public bool ReadFile(JObject dObjects)
            {
                // Wöchentliche Rifts auslesen:
                dynamic bObjRifts = dObjects[BotConvention.DATAFILE_JSON_RIFTS_WEEKLY];
                if (bObjRifts != null)
                {
                    // Aktuellen Inhalt entfernen:
                    WeeklyRiftSummary.Clear();

                    BotRiftSummary _bSum;
                    for (int i = 0; i < bObjRifts.Count; i++)
                    {
                        _bSum = new BotRiftSummary(ReferredPair);

                        if (_bSum.ReadFile(bObjRifts[i]))
                            WeeklyRiftSummary.Add(_bSum);
                        else
                            _bSum = null;
                    }
                }

                // Summen-Rifts lesen:
                bObjRifts = dObjects[BotConvention.DATAFILE_JSON_RIFTS_TOTAL];

                if ((bObjRifts != null) && (bObjRifts.Count == TotalRiftSummary.Count))
                {
                    for (int i = 0; i < bObjRifts.Count; i++)
                        TotalRiftSummary[i].ReadFile(bObjRifts[i]);
                }

                return (true);
            }
            #endregion
        }
        [TypeConverter(typeof(CnvEmptyExpandableObject))]
        public class BotSelectorInfo
        {
            #region Constants
            // Priority:
            public const int PRIO_ARRAY_COUNT = 3;
            #endregion


            #region Enumerations
            public enum BotSettingsValidationResult
            {
                eNone,                                                                      // Keine Einstellungen angewendet
                eKeepPriority,                                                              // Einstellungen der zuletzt aktiven Priorität weiterverwenden
                eResult                                                                     // Resultierende Einstellungen anwenden
            }
            #endregion


            #region Types
            [TypeConverter(typeof(CnvEmptyExpandableObject))]
            public class BotPriorityState : DscrMetaData
            {
                public BotPriorityState(BotSelectorParameters.BotPriorityParameters pRefParams, BotFormula.BotVariables vVariables) : base(pRefParams.DisplayName)
                {
                    ReferredParameters = pRefParams;
                    Formula = new BotFormula.BotCalculation(vVariables);
                }


                #region Properties.Management
                [Browsable(false)]
                public BotSelectorParameters.BotPriorityParameters ReferredParameters { private set; get; }
                [Browsable(false)]
                public BotFormula.BotCalculation Formula { private set; get; }
                [Browsable(false)]
                public override bool IsBrowsable { get { return (ReferredParameters.Enabled); } }
                #endregion
                #region Properties
                [DisplayName(BotConvention.PROP_NAME_FORMULA_RESULT)]
                [ReadOnly(true)]
                [TypeConverter(typeof(CnvEnumDescription))]
                public BotFormula.BotCalculation.BotResult FormulaResult { get { return (Formula.LastResult); } }
                #endregion


                #region Management.General
                public bool ApplyParameters()
                {
                    Formula.Expression = ReferredParameters.FormulaExpression;

                    return (true);
                }
                #endregion
            }
            #endregion


            public BotSelectorInfo(BotSelectorParameters sRefSelector, BotFormula.BotVariables vVariables)
            {
                // Kopie des referenzierten Selektors erstellen:
                SelectorClone = new BotSelectorParameters();

                CopyParameters(sRefSelector);

                // Prioritäten initialisieren:
                Priorities = new BotPriorityState[PRIO_ARRAY_COUNT];
                for (int i = 0; i < PRIO_ARRAY_COUNT; i++)
                    Priorities[i] = new BotPriorityState(SelectorClone.PriorityParameters[i], vVariables);
            }


            #region Properties.Reference
            /// <summary>
            /// (Zur Laufzeit erstellte Kopie) des aktuell angewendeten Selektors (Falls vorhanden).
            /// </summary>
            [Browsable(false)]
            public BotSelectorParameters SelectorClone { private set; get; }
            #endregion
            #region Properties.Calculation
            [Category(BotConvention.PROP_CATEGORY_SETTINGS_SELECTION)]
            [DisplayName(BotConvention.PROP_NAME_PRIORITY)]
            [Description(BotConvention.PROP_DESCR_PRIORITY)]
            [TypeConverter(typeof(CnvEmptyExpandableList))]
            public BotPriorityState[] Priorities { private set; get; }
            #endregion


            #region Management
            // Wertet die Prioritäten und Standard-Werte aus und stellt so sicher dass die korrekten Settings aktiviert werden.
            public BotSettingsValidationResult ValidateCurrentSettings(BotTradingStrategy tRefStrategy, out BotStrategySettings sResult)
            {
                sResult = null;

                // Formeln überprüfen:
                foreach (BotPriorityState pPrioState in Priorities)
                {
                    if (pPrioState.ReferredParameters.Enabled)
                        pPrioState.Formula.Check();
                }

                // Priorisierte Auswahl treffen:
                foreach (BotPriorityState pPrioState in Priorities)
                {
                    if ((pPrioState.ReferredParameters.Enabled) && (pPrioState.FormulaResult == BotFormula.BotCalculation.BotResult.eSuccess))
                    {
                        sResult = pPrioState.ReferredParameters.Settings;
                        return (BotSettingsValidationResult.eResult);
                    }
                }

                if (SelectorClone.DefaultParameters.Enabled)
                {
                    sResult = SelectorClone.DefaultParameters.Settings;
                    return (BotSettingsValidationResult.eResult);
                }
                else
                    return (SelectorClone.DefaultParameters.KeepPriority ? BotSettingsValidationResult.eKeepPriority : BotSettingsValidationResult.eNone);
            }
            #endregion
            #region Management.General
            public bool CopyParameters(BotSelectorParameters sRefSelector)
            {
                return (SelectorClone.CopyFrom(sRefSelector));
            }
            public bool ApplyParameters()
            {
                foreach (BotPriorityState pState in Priorities)
                    pState.ApplyParameters();

                return (true);
            }
            #endregion
        }
        #endregion


        public BotTradingStrategy(BotTradeBot tRefBot, BotTradingPair tRefPair, BotWallet tRefWallet1, BotWallet tRefWallet2, BotTradingStrategy bTranslationStrategy = null)
        {
            Debug.Assert((tRefBot != null), "[BTS625]");

            this.TradingBot = tRefBot;
            this.Platform = tRefBot.ReferredPlatform;

            this.TradingPair = tRefPair;
            this.WalletPrim = tRefWallet1;
            this.WalletSec = tRefWallet2;
            this.ReferredParameters = null;
            this.bTranslationStrategy = bTranslationStrategy;
            this.Rift_BuyInfo = null;
            this.Rift_SellInfo = null;
            this.Calc_Variables = new BotFormula.BotVariables();
            this.Calc_Buy_CoolDown = new BotCoolDownTimer();
            this.LastBuyInfo = new BotLastOrderInfo();
            this.LastSellInfo = new BotLastOrderInfo();
            this.pCalc_Rift_PanicShortSale = new BotLimitState();

            this.bBuyActivator = new BotBuyActivator();
            this.sSaleActivator = new BotSellActivator();

            this.RiftSummaries = new BotRiftSummaryCollection(tRefPair);

            this.RemainingActiveDuration = TimeSpan.Zero;
        }
        ~BotTradingStrategy()
        {
            ResetCalc();
        }


        #region Properties.Statistik
        [Category(BotConvention.PROP_CATEGORY_GENERAL)]
        [DisplayName("Aktueller Kurs")]
        [Description("")]
        public double LastPrice { protected set; get; }
        [Category(BotConvention.PROP_CATEGORY_GENERAL)]
        [DisplayName("Letzte Kauf-Order")]
        [Description("")]
        public BotLastOrderInfo LastBuyInfo { protected set; get; }
        [Category(BotConvention.PROP_CATEGORY_GENERAL)]
        [DisplayName("Letzte Verkauf-Order")]
        [Description("")]
        public BotLastOrderInfo LastSellInfo { protected set; get; }
        [Category(BotConvention.PROP_CATEGORY_CALCULATION)]
        [DisplayName("Kaufen")]
        [Description("Informationen zu Kauf-Orders.")]
        public BotRiftInfo Rift_BuyInfo { protected set; get; }
        [Category(BotConvention.PROP_CATEGORY_CALCULATION)]
        [DisplayName("Verkaufen")]
        [Description("Informationen zu Verkauf-Orders.")]
        public BotRiftInfo Rift_SellInfo { protected set; get; }
        #endregion
        #region Properties.Specification
        [Category(BotConvention.PROP_CATEGORY_TRADINGPAIR)]
        [DisplayName("Nachkommastellen")]
        [Description("Anzahl an Nachkommastellen.")]
        public int Precision { get { return (TradingPair.Precision); } }
        [Category(BotConvention.PROP_CATEGORY_TRADINGPAIR)]
        [DisplayName("Min. Order-Betrag")]
        [Description("Minimaler Betrag einer Order.")]
        public double MinOrderSize { get { return (TradingPair.MinOrderSize); } }
        [Category(BotConvention.PROP_CATEGORY_TRADINGPAIR)]
        [DisplayName("Max. Order-Betrag")]
        [Description("Maximaler Betrag einer Order.")]
        public double MaxOrderSize { get { return (TradingPair.MaxOrderSize); } }
        #endregion
        #region Properties.Selection
        [Category(BotConvention.PROP_CATEGORY_SETTINGS_SELECTION)]
        [DisplayName("Selektor")]
        [Description("")]
        [TypeConverter(typeof(CnvEmptyExpandableObject))]
        public BotSelectorInfo SelectorInfo { protected set; get; }
        /// <summary>
        /// (Zur Laufzeit erstellte Kopie) der aktuell angewendeten Settings (Falls vorhanden).
        /// </summary>
        [Category(BotConvention.PROP_CATEGORY_SETTINGS_SELECTION)]
        [DisplayName(BotConvention.PROP_NAME_ACTIVE_SETTINGS)]
        [Description("Aktuell angewendete Einstellungen.")]
        [TypeConverter(typeof(BotParameterConverter))]
        [ReadOnly(true)]
        public BotStrategySettings ActiveSettingsClone { protected set; get; }
        [Browsable(false)]
        public BotStrategySettings ActiveSettingsOriginal { get { return (ActiveSettingsClone == null) ? null : ((BotStrategySettings)ActiveSettingsClone.Original); } }
        #endregion
        #region Properties.Calculation
        [Category(BotConvention.PROP_CATEGORY_RATING)]
        [DisplayName(BotConvention.PROP_NAME_INDICATORS)]
        [Description("")]
        public BotTrendIndicator TrendIndicator { private set; get; }
        [Category(BotConvention.PROP_CATEGORY_REACTION)]
        [DisplayName(BotConvention.PROP_NAME_INDICATORS + " ")]                // (BETA) ... [BUG-WORKAROUND] Keine Ahnung warum hier der DisplayName doch verändert werden muss (Siehe +" ") - es hat bisher immer funktioniert!?! :(
        [Description("")]
        [TypeConverter(typeof(CnvEmptyExpandableList))]
        public BotReactionIndicator[] ReactionIndicators { private set; get; }
        [Category(BotConvention.PROP_CATEGORY_REACTION)]
        [DisplayName(BotConvention.PROP_NAME_TRIGGER)]
        [Description("")]
        [TypeConverter(typeof(CnvEmptyExpandableList))]
        public BotTrigger[] Triggers { private set; get; }

        [Category(BotConvention.PROP_CATEGORY_CALCULATION)]
        [DisplayName("Kurs Nachkaufen")]
        [Description("Zu unterschreitender Kurswert um Kauf-Orders zum Nachkaufen platzieren zu können.")]
        public double Calc_Buy_AverageDownRate { protected set; get; }
        [Category(BotConvention.PROP_CATEGORY_CALCULATION)]
        [DisplayName(BotConvention.PROP_NAME_COOLDOWN_TIME_BUYORDERS)]
        [Description("Abzuwartende Zeit bis Kauf-Orders erneut ausgeführtwerden können.")]
        public TimeSpan Calc_Buy_CoolDownTime { get { return (Calc_Buy_CoolDown.RemainingTime); } }
        [Category(BotConvention.PROP_CATEGORY_CALCULATION)]
        [DisplayName("Bedrängter Verkauf")]
        [Description("Führt mit der nächsten Verkauf-Order einen Leerverkauf in Bedrängnis durch da die geforderte Verkauf-Bedingung (z.B. Einhalten von minimalem Profit) nicht erfüllt werden kann.")]
        [TypeConverter(typeof(CnvBooleanLabel.InActive))]
        public bool Calc_Rift_NextSellOppressive { get { return ((sSaleActivator.NextSellOppressive) && (Rift_SellInfo.RemainingSteps.Default > 1)); } }
        [Category(BotConvention.PROP_CATEGORY_CALCULATION)]
        [DisplayName("Kurs Panik-Leerverkauf")]
        [Description("Zu unterschreitender Kurswert zur Ausführung des Panik-Leerverkauf.")]
        public double Calc_Rift_PanicShortSale_ExecuteRate { get { return (pCalc_Rift_PanicShortSale.ExecuteRate); } }
        [Category(BotConvention.PROP_CATEGORY_CALCULATION)]
        [DisplayName("Profitabler Verkaufskurs")]
        [Description("Kurs der mindestens erreicht werden muss um profitabele Verkaufs-Orders ausführen zu können.")]
        public double Calc_Rift_MinProphitSellRate { protected set; get; }

        [Browsable(false)]
        public BotFormula.BotVariables Calc_Variables { protected set; get; }
        [Browsable(false)]
        public BotCoolDownTimer Calc_Buy_CoolDown { private set; get; }
        #endregion
        #region Properties.Configuration
        [Category(BotConvention.PROP_CATEGORY_MANAGEMENT)]
        [DisplayName(BotConvention.PROP_NAME_ACTIVATED)]
        [Description("Gibt die Strategie zur Anwendung durch den Trading-Bot frei.")]
        [TypeConverter(typeof(CnvBooleanLabel.NoYes))]
        public bool Enabled
        {
            get { return ((GetState() != BotState.eDisabled) && (IsAvailable)); }
            set { Enable(value); }
        }
        [Category(BotConvention.PROP_CATEGORY_MANAGEMENT)]
        [DisplayName("Freigabe Trading-Bot Automatik")]
        [Description("Gibt das automatische De-/Aktivieren via Trading-Bot Automatik für diese Pärchen frei.")]
        [TypeConverter(typeof(CnvBooleanLabel.InActive))]
        public bool IsUsableByBotAutomatik { set; get; }
        #endregion
        #region Properties.Management
        [Category(BotConvention.PROP_CATEGORY_MANAGEMENT)]
        [DisplayName(BotConvention.PROP_NAME_REMAINING_ACTIVE_DURATION)]
        [Description("Verbleibende Dauer, welche eine Strategie mit den aktuellen Einstellungen noch aktiv sein muss, um diese wieder durch automatische Funktionen (wie z.B. Selektoren oder Bot-Automatik) zu deaktivieren.")]
        [ReadOnly(true)]
        public TimeSpan RemainingActiveDuration { set; get; }
        [Category(BotConvention.PROP_CATEGORY_MANAGEMENT)]
        [DisplayName("Verfügbar")]
        [Description("Verfügbarkeit der Strategie (Abhängig von erfolgreich ermittelter Bewertung.")]
        [TypeConverter(typeof(CnvBooleanLabel.NoYes))]
        public bool IsAvailable
        {
            get
            {
                // (BETA) ...
                // Pärchen, die nicht $ als Sekundär-Währung haben, vorerst sperren!
                if (TradingPair.CurrencySec != TRADE_BASE_CURRENCY)
                    return (false);

                if (Platform.IsSimulating)
                {
                    BotCoinStreamPlayer cPlayer = Platform.CoinStreamPlayer;
                    if ((cPlayer != null) && (TradingPair.Compare(cPlayer.TradingPair) == false))
                        // Aktuelle Instanz ist nicht verfügbar da ein Coin-Stream von einem NICHT referenzierten Pärchen abgespielt wird:
                        return (false);
                }
                return ((BotCurrencyRating.CoinInfo.Find(TradingPair.CurrencyPrim) != null) && (BotCurrencyRating.CoinInfo.Find(TradingPair.CurrencySec) != null));
            }
        }

        [Browsable(false)]
        public bool IsActive { get { return (sState >= BotState.eIdle); } }
        [Browsable(false)]
        public bool IsValid { get { return ((TradingPair != null) && (WalletPrim != null) && (WalletSec != null)); } }
        [Browsable(false)]
        public bool IsReady
        {
            get
            {
                if (IsValid)
                {
                    if (sState == BotState.eIdle)
                        return (true);
                    else if (sState == BotState.eActive)
                    {
                        if (bTranslationStrategy == null)
                            // Prüfen ob der aktuelle Preis bekannt ist:
                            return (TradingPair.LastRateValue > 0);
                        else
                            return (bTranslationStrategy.IsValid);
                    }

                    // (BETA) ... [RATING]
                    // Alter Code:
                    /*
                    if ((sState >= State.BTSS_Calculating) && (iTrend_Length > 0) && (tTrend_Duration != TimeSpan.Zero) && (dCalc_Buy_WeightFactor > 0))
                    {
                        if (bTranslationStrategy == null)
                            // Prüfen ob der aktuelle Preis bekannt ist:
                            return (tRefPair.Rate_LastVal > 0);
                        else
                            return (bTranslationStrategy.IsValid());
                    }
                    */
                }
                return (false);
            }
        }
        [Browsable(false)]
        public bool HasActiveParameters { get { return ((SelectorInfo != null) || (ActiveSettingsClone != null)); } }

        [Browsable(false)]
        public DateTime ActivationTime { private set; get; }
        [Browsable(false)]
        public BotSelectorParameters UsedSelectorClone { get { return ((SelectorInfo == null) ? null : SelectorInfo.SelectorClone); } }
        [Browsable(false)]
        public BotSelectorParameters UsedSelectorOriginal { get { return (SelectorInfo == null) ? null : ((BotSelectorParameters)UsedSelectorClone.Original); } }
        [Browsable(false)]
        public string Title { get { return (String.Format("{0} ({1})", TradingPair.Title, Platform.PlatformName)); } }
        /// <summary>
        /// // Erster Zyklus nach Starten des Bots oder beendetem Rift.
        /// </summary>
        [Browsable(false)]
        public bool IsFirstCycle { set; get; }
        [Browsable(false)]
        public BotRiftSummaryCollection RiftSummaries { protected set; get; }
        [Browsable(false)]
        public bool IsRiftActive
        {
            get
            {
                if (RiftSummaries.IsRiftActive)
                {
                    Debug.Assert((ActiveSettingsClone != null), "[IRA1036]");
                    return (true);
                }
                else
                    return (false);
            }
        }
        [Description("Prüft bestehende Bedingungen welche das Abschalten der aktuellen Instanz via automatischer Funktionen (wie z.B. Selektoren oder Bot-Automatik) verhindern.")]
        [Browsable(false)]
        public bool CanBeDisabledByAutomatic { get { return ((IsRiftActive == false) && (RemainingActiveDuration == TimeSpan.Zero)); } }

        [Browsable(false)]
        public List<BotBaseIndicator> Indicators { private set; get; }

        [Browsable(false)]
        public bool BuySlow_LimitEnabled { get { return (IsSlowLimitEnabled(BotOrderDirection.eBuy)); } }
        [Browsable(false)]
        public bool SellSlow_LimitEnabled { get { return (IsSlowLimitEnabled(BotOrderDirection.eSell)); } }
        [Browsable(false)]
        public BotCoinStreamRecorder CoinStreamRecorder { get { return (cCoinStreamRecorder); } }
        [Browsable(false)]
        public BotCoinStreamPlayer CoinStreamPlayer
        {
            get
            {
                if (Platform.IsSimulating)
                {
                    BotCoinStreamPlayer cPlayer = Platform.CoinStreamPlayer;
                    if ((cPlayer != null) && (TradingPair.Compare(cPlayer.TradingPair)))
                        return (cPlayer);
                }
                return (null);
            }
        }
        #endregion
        #region Properties.References
        [Category(BotConvention.PROP_CATEGORY_MANAGEMENT)]
        [DisplayName("Referenzierte Parameter")]
        [Description("Definiert die Parameter (Selektor oder Einstellungen) welche zur Laufzeit zum Ausführen der aktuellen Strategie angewendet werden.")]
        [TypeConverter(typeof(BotParameterConverter))]
        [Editor(typeof(BotParameterTypeEditor), typeof(UITypeEditor))]
        public BotBaseParameters ReferredParameters { set; get; }

        [Browsable(false)]
        public BotTradingPair TradingPair { protected set; get; }
        [Browsable(false)]
        public BotTradeBot TradingBot { protected set; get; }
        [Browsable(false)]
        public BotPlatformService Platform { protected set; get; }
        [Browsable(false)]
        public BotWallet WalletPrim { protected set; get; }
        [Browsable(false)]
        public BotWallet WalletSec { protected set; get; }
        #endregion
        #region Properties.Debug
        [Category(BotConvention.PROP_CATEGORY_DEBUG)]
        [DisplayName("Lock Kauf-Order")]
        [Description("Grund für Blockieren der nächsten Kauf-Order.")]
        public String BuyOrderLock { get { return (FormatLockedOrderReason(bBuyActivator.Lock)); } }
        [Category(BotConvention.PROP_CATEGORY_DEBUG)]
        [DisplayName("Lock Verkauf-Order")]
        [Description("Grund für Blockieren der nächsten Verkauf-Order.")]
        public String SellOrderLock { get { return (FormatLockedOrderReason(sSaleActivator.Lock)); } }
        #endregion
        #region Properties
        [Category(BotConvention.PROP_CATEGORY_COINSTREAM_REPLAY)]
        [DisplayName("Frame")]
        [Description("Index-Position des aktuell abgespielten Frame.")]
        public String CoinStream_Replay_Progress
        {
            get
            {
                BotCoinStreamPlayer cPlayer = CoinStreamPlayer;
                String sResult = "";
                if (cPlayer != null)
                {
                    uint uPercent = cPlayer.Progress;
                    sResult = String.Format("{0} / {1}", cPlayer.CurFramePos, cPlayer.FrameCount);
                    if (uPercent > 0)
                        sResult += String.Format("  ({0} %)", uPercent);
                }
                return (sResult);
            }
        }
        #endregion


        #region Events.Display
        #endregion
        #region Events
        public void OnOrderClosed(BotBaseOrder oOrder, BotOrderResult oResult)
        {
            BotOrderActivator oActivator;
            BotLastOrderInfo lOrderInfo;
            bool bCloseRift = false;

            Debug.Assert((oResult >= BotOrderResult.eCanceled), "[OOC2641]");

            if (oOrder.IsSellOrder)
            {
                lOrderInfo = LastSellInfo;
                oActivator = (BotOrderActivator)sSaleActivator;
            }
            else
            {
                lOrderInfo = LastBuyInfo;
                oActivator = (BotOrderActivator)bBuyActivator;
            }

            if (oResult == BotOrderResult.eCanceled)
                oOrder.WriteLog(BotOrderResult.eCanceled, oActivator);
            else
            {
                if (ActiveSettingsClone != null)
                {
                    if (oOrder.IsBuyOrder)
                    {
                        // Einkaufs-Order ausgeführt.

                        // Anzahl an verbleibenden Kauf-Orders verringern:
                        Rift_BuyInfo.RemainingSteps.Count(ActiveSettingsClone.CfgBuy_Steps, false);

                        // Ggf. Anzahl an Verkauf-Orders erhöhen:
                        // > Möglichkeit aktuellen Rift zu verlängern mit jeder Einkaufs-Order.
                        Rift_SellInfo.RemainingSteps.Count(ActiveSettingsClone.CfgShortSale_Steps, true);
                    }
                    else if (oOrder.IsSellOrder)
                    {
                        // Verkaufs-Order ausgeführt.

                        // (BETA) ... Profit anpassen vorerst entfernt
                        // Es wurde immer der Profit angepasst :(
                        /*
                        String sMessage = "";

                        // Prüfen ob verkaufte zur eingekauften Menge passt:
                        double dAmountPerStep = Math.Abs(rRiftSummaries.CurrentRift.Buys.TotalAmount / sActiveSettingsClone.CfgShortSale_Steps.Default);
                        double dDifference = (Math.Abs(oOrder.dAmount) - dAmountPerStep);

                        if (dDifference > 0)
                        {
                            sMessage = String.Format("Ausgabe der Order-Ausführung angepasst (Ausgeführt: {0} {1}) aufgrund von Wallet-Anderungen.", oOrder.FinalAmount, MyEnum.GetEnumDescription(oOrder.tRefPair.CurrencyPrim));
                            oOrder.dAmount = (dAmountPerStep * -1);
                        }
                        else if (dDifference < 0)
                            sMessage = String.Format("Kauf-Betrag ({0} {1}) auf Grund von Wallet-Anderungen nicht verfügbar - Verluste erwartet!", rRiftSummaries.CurrentRift.Buys.TotalAmount, MyEnum.GetEnumDescription(oOrder.tRefPair.CurrencyPrim));

                        if (sMessage != "")
                            LogSystem.WriteLog("OOF4059", BotLogProcess.eOrder, sMessage, LogSystem.LogType.LT_PlatformTrading, LogSystem.EntryPriority.EP_High);
                        */

                        // Ggf. Anzahl an verbleibenden Kauf-Orders erhöhen:
                        Rift_BuyInfo.RemainingSteps.Count(ActiveSettingsClone.CfgBuy_Steps, true);

                        // Anzahl an Verkauf-Orders verringern:
                        Rift_SellInfo.RemainingSteps.Count(ActiveSettingsClone.CfgShortSale_Steps, false);

                        if ((Rift_SellInfo.RemainingSteps.Default <= 0) || (sSaleActivator.IsShortSale))
                            // Aktueller Rift ist mit Abschließen dieser Order ('oOrder') beendet:
                            bCloseRift = true;

                        // Ggf. neutrale Zone aktivieren:
                        Rift_SellInfo.NeutralZone.Activate(oOrder.dRate);
                    }
                    else
                        Debug.Assert(false, "[OOF2229]");

                    // Rift-Summararies aktualisieren:
                    BotCoinStreamPlayer cPlayer = CoinStreamPlayer;

                    if (cPlayer != null)
                        cPlayer.RiftSummary.OnOrderFinished(oOrder);

                    RiftSummaries.OnOrderFinished(oOrder);
                }

                // Information ausgeben:
                oOrder.WriteLog(oResult, oActivator);

                lOrderInfo.Apply(oOrder);

                // Order in Report aufnehmen:
                tTradesDoc.Execute(oOrder);

                // Owner benachrichtigen:
                BotEnv.Send(() => BotEnv.OnOrderFinished?.Invoke(this, oOrder));
            }

            if (ActiveSettingsClone != null)
            {
                // Grenzwerte zurücksetzen:
                // > Nur Order-Typ gegengesetzte Indikatoren zurücksetzen (Um z.B. sofortiges Kaufen nach Verkauf-Order zu verhindern)
                ResetLimits(oOrder.IsSellOrder, oOrder.IsBuyOrder);

                // Warm-Reset des Limits von auslösendem Indikator:
                if (oActivator.TriggerFunction != null)
                {
                    oActivator.TriggerFunction.OnOrderClosed(oOrder);
                    oActivator.TriggerFunction.ResetState(oOrder.Direction, true);
                }
            }

            // Ver- & Kauf-Indikatoren zurücksetzen:
            bBuyActivator.ResetTrigger();
            sSaleActivator.ResetTrigger();

            if (oResult > BotOrderResult.eCanceled)
            {
                ReaclcCycle(true);

                if (ActiveSettingsClone != null)
                {
                    // Ggf. aktivierten Grenzwert für Panik-Verkauf zurücksetzen:
                    pCalc_Rift_PanicShortSale.Reset();

                    // Ggf. aktivierten Grenzwert für Nachkaufen zurücksetzen:
                    Calc_Buy_AverageDownRate = 0;

                    if (bCloseRift)
                        OnRiftClosed();
                }
            }
        }

        // Der aktuelle Rift ist beendet.
        public void OnRiftClosed()
        {
            Debug.Assert((ActiveSettingsClone != null), "[ORC2809]");

            // Zusammenfassende Informationen zu abgeschlossenem Rift erstellen:
            BotRiftSummaryCollection.BotRiftRating rRating = RiftSummaries.OnRiftClosed();

            if ((rRating == BotRiftSummaryCollection.BotRiftRating.eBad) && (ActiveSettingsClone.CfgRift_CoolDown.IsEnabled))
            {
                switch (ActiveSettingsClone.CfgRift_CoolDown.Scope)
                {
                    case BotStrategySettings.BotCoolDown.BotScope.eStrategy:
                        Calc_Buy_CoolDown.Activate(ActiveSettingsClone.CfgRift_CoolDown.Duration);
                        break;

                    case BotStrategySettings.BotCoolDown.BotScope.ePlatform:
                        TradingBot.ForeachStrategy(tStrategy => tStrategy.Calc_Buy_CoolDown.Activate(ActiveSettingsClone.CfgRift_CoolDown.Duration));
                        break;

                    case BotStrategySettings.BotCoolDown.BotScope.eComplete:
                        BotTradeBot.ForeachPlatformsStrategy(tStrategy => tStrategy.Calc_Buy_CoolDown.Activate(ActiveSettingsClone.CfgRift_CoolDown.Duration));
                        break;

                    default:
                        Debug.Assert(false, "[ORC2831]");
                        break;
                }
            }

            // Abschließende Aufgaben ausführen:
            BotCoinStreamPlayer cPlayer = CoinStreamPlayer;

            if (cPlayer != null)
                cPlayer.RiftSummary.OnRiftClosed(RiftSummaries.CurrentRift);

            bIsWaitingForThreads = true;
            BotEnv.Post(() => BotEnv.OnRiftClosed?.Invoke(this, RiftSummaries.CurrentRift));
        }
        public void OnAfterRiftClosed(object oSender, RunWorkerCompletedEventArgs rArgs)
        {
            BotExcelDocThread eSender = (BotExcelDocThread)oSender;

            // Aktive Threads der aktuellen Gruppe ermitteln:
            if (BotExcelDocThread.GetActiveThreads(eSender.GroupID) == 0)
            {
                bIsWaitingForThreads = false;

                // Aktuellen Rift zurücksetzen:
                ResetRift();

                if (ActiveSettingsClone.CfgRift_StopOnClose)
                    // Strategie nach Abschluss des Rifts stoppen:
                    Enabled = false;
                else
                    // Neuen Rift beginnen:
                    ReaclcCycle();

                BotEnv.Post(() => BotEnv.OnRefreshView?.Invoke());

                BotAutomatic.Run();
            }
        }
        #endregion


        #region Management
        public BotState GetState()
        {
            return (sState);
        }
        // Ermittelt für Kalkulation notwendige Informationen.
        public void StartCalculating()
        {
            sState = BotState.eCalculating;

            // (BETA) ... [RATING]
            // Zur Zeit wird Trend-Bewertung via 'CurrencyRating.UpdateRating()' durchgeführt!
            /*
            // Candle-Sticks aktualisieren:
            tRefBot.RefPlatform.SubscribeCandles(tRefPair, CFG_STAT_CANDLE_TIMEFRAME);
            */
        }
        public BotTradingStrategy GetTranslationStrategy()
        {
            if (bTranslationStrategy == null)
            {
                // Sicherstellen dass aktuelle Sekundär-Währung Dollar ist:
                Debug.Assert((TradingPair.CurrencySec == TRADE_BASE_CURRENCY), "[GTS88]");

                return (this);
            }
            else
                return (bTranslationStrategy);
        }


        // Prüft die aktuelle Instanz auf Plausibilität.
        public bool CheckPlausibility()
        {
            if (ReferredParameters == null)
                return (BotEnv.LogSystem.WriteLogInvalidParam("OCP1332", "Strategie", BotConvention.PARAM_REF_PARAMETERS));
            else
                return (ReferredParameters.CheckPlausibility());
        }
        public bool Enable(bool bEnable, bool bReInit = true)
        {
            // Prüfen ob Bewertung verfügbar ist:
            if ((BotCurrencyRating.State == BotCurrencyRating.BotState.eInvalid) || (IsAvailable))
            {
                // Aktuelle Instanz de-/aktivieren:
                if (bEnable)
                    sState = BotState.eInit;
                else
                {
                    sState = BotState.eDisabled;
                    Stop();
                }

                // Zurücksetzen:
                ResetCalc(true);

                dCalc_Buy_WeightFactor = 0;

                if (bReInit)
                {
                    if ((TradingBot.ReInit()) && (bEnable))
                        // Trading-Bot war bereits aktiv, aktuelle Instanz starten:
                        Init(true);
                }

                return (true);
            }
            else
                return (false);
        }
        public bool Init(bool bWarmStart = false)
        {
            bool bResult = false;

            if (Enabled)
            {
                // Prüfen ob Parameter aktiv sind:
                if (HasActiveParameters)
                {
                    // Platform-Spezifische Informationen ermitteln:
                    for (int i = 0; i < PS_MAX_RETRY_COUNT_DEFAULT; i++)
                    {
                        bResult = Platform.GetPlatformSpecific();
                        if (bResult)
                            break;
                    }

                    if (bResult)
                    {
                        if (bWarmStart)
                        {
                            // Aktuelle Strategie wurde gestartet während Trading-Bot aktiv war.

                            // Currency-Rating aktualisieren:
                            BotCurrencyRating.UpdateRating();

                            StartCalculating();
                        }

                        if (tTradesDoc == null)
                            // Neues Dokument erstellen:
                            tTradesDoc = new BotTradesExcelDoc(this);

                        // (BETA) ... killme!!!warum war das hier überhaupt??? code löschen wenn ich nicht rausfinde warum das hier war
                        /*
                        // Rift (via Zurücksetzen) initialisieren:
                        ResetRift();
                        */

                        // Ggf. Ticker abonnieren:
                        Platform.SubscribeTicker(TradingPair);
                    }
                    else
                        // Hier Verbindung nicht trennen.
                        // > Denn wenn von Observation erknnt wird dass Verbindung aufgebaut, aber kein Login erfolgte, wird ein Reconnect ausgelöst.
                        BotEnv.LogSystem.WriteLog("I1406", BotLogProcess.eCommunication, ("Platform-spezifische Informationen konnten nicht empfangen werden!"), BotLogType.ePlatformError);
                }
                else
                    sState = BotState.eError;
            }

            return (bResult);
        }
        public void Stop()
        {
            // Aufnahme / Player von Coin-Streams beenden:
            if (CoinStreamPlayer != null)
                ((IBotSimulatedPlatformService)Platform).StopCoinStreamPlay();

            StopCoinStreamRecord();

            // Zurücksetzen:
            ResetCalc(true);

            // Ggf. Ticker-Subscription abbestellen:
            BotBasePairSubscription bSubscription = Platform.FindPairSubscription(TradingPair, BotSubscriptionType.eTicker);

            if (bSubscription != null)
                Platform.Unsubscribe(bSubscription.GetSubscriptionID());

            if (tTradesDoc != null)
            {
                // Dokument schließen:
                tTradesDoc.Close();

                tTradesDoc = null;
            }
        }
        // Wertet die referenzierten Parameter aus und stellt so sicher dass die korrekten Parameter/Settings aktiviert sind.
        public bool ValidateActiveParameters()
        {
            BotStrategySettings sActivateSettings = null;
            BotSelectorParameters sToUseSelector = null;
            bool bResult = false;

            // Referenzierte Parameter auswerten:
            if (ReferredParameters != null)
            {
                if (ReferredParameters.GetType() == typeof(BotStrategySettings))
                    sActivateSettings = (BotStrategySettings)ReferredParameters;

                else if (ReferredParameters.GetType() == typeof(BotSelectorParameters))
                    sToUseSelector = (BotSelectorParameters)ReferredParameters;

                else
                    Debug.WriteLine("[VAP1382] Ungültiger Parameter-Typ!");
            }

            // Selektor verwalten:
            if ((SelectorInfo != null) && ((sToUseSelector == null) || (BotBaseParameters.Compare(SelectorInfo.SelectorClone, sToUseSelector) == false)))
            {
                // Speicher von zuletzt angewendetem Selektor freigeben:
                SelectorInfo = null;

                GC.Collect();
            }

            if ((sToUseSelector != null) && (SelectorInfo == null))
                // Neuen Selektor erzeugen:
                SelectorInfo = new BotSelectorInfo(sToUseSelector, Calc_Variables);

            if (CanBeDisabledByAutomatic)
            {
                // Ggf. Settings validieren durch aktiven Selektor:
                if (SelectorInfo != null)
                {
                    BotStrategySettings _bResult;
                    switch (SelectorInfo.ValidateCurrentSettings(this, out _bResult))
                    {
                        case BotSelectorInfo.BotSettingsValidationResult.eNone:
                            sActivateSettings = null;
                            break;
                        case BotSelectorInfo.BotSettingsValidationResult.eKeepPriority:
                            // Aktive Einstellungen beibehalten:
                            sActivateSettings = ActiveSettingsClone;
                            break;
                        case BotSelectorInfo.BotSettingsValidationResult.eResult:
                            sActivateSettings = _bResult;
                            break;

                        default:
                            Debug.Assert(false, "[VAP1726]");
                            break;
                    }
                }
                bResult = ActivateSettings(sActivateSettings);
            }

            if ((ActiveSettingsClone != null) && (ActivationTime != default(DateTime)))
            {
                // Verbleibende Eischaltzeit berechnen:
                DateTime dMaxTime = (ActivationTime + ActiveSettingsClone.Cfg_MinActiveDuration);

                if (DateTime.Now < dMaxTime)
                    RemainingActiveDuration = (dMaxTime - DateTime.Now);
                else
                    RemainingActiveDuration = TimeSpan.Zero;
            }

            return (bResult);
        }
        public bool TriggerManualOrder(BotOrderDirection oDirection)
        {
            if (IsReady)
            {
                if (Platform.IsSimulating == false)
                {
                    // (BETA) ... [Uncomment]
                    /*
                    // Sichergehen dass Benutzer korrekt handelt:
                    if (BotEnv.LogSystem.ShowMessage("", BotLogProcess.eOrder, "Manuelles Auslösen der Order bestätigen.", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation) != DialogResult.OK)
                        return (false);
                    */
                }

                if (oDirection == BotOrderDirection.eBuy)
                    bBuyActivator.TriggerReason = BotOrderActivator.BotTriggerReason.eUser;
                else
                    sSaleActivator.TriggerReason = BotOrderActivator.BotTriggerReason.eUser;

                return (true);
            }
            else
                return (false);
        }

        /// <summary>
        /// Setzt die aktuelle Kalkulation zurück.
        /// </summary>
        /// <param name="bWarm">[FALSE] Setzt komplette Kalkuation zurück. [TRUE] Setzt nur Werte für Neukalulation zurück.</param>
        public void ResetCalc(bool bWarm = false)
        {
            // Informationen zurücksetzen:
            if (sState != BotState.eDisabled)
                sState = BotState.eInit;

            IsFirstCycle = true;
            dLatestAverage = 0;
            LastPrice = 0;

            if (ActiveSettingsClone != null)
            {
                Rift_BuyInfo.Reset();
                Rift_SellInfo.Reset();

                // Indikatoren zurücksetzen:
                foreach (BotBaseIndicator bIndicator in Indicators)
                {
                    if ((bIndicator.SpeedType == BotSpeedType.eSlow) || (bWarm == false))
                        bIndicator.Reset();
                }
            }

            if (bWarm == false)
            {
                BotTickerSubscription tTicker = (BotTickerSubscription)Platform.FindPairSubscription(TradingPair, BotSubscriptionType.eTicker);

                // Komplette Kalkulation zurücksetzen:
                if (tTicker != null)
                    tTicker.lData.Clear();

                bBuyActivator.Lock.Reset();
                sSaleActivator.Lock.Reset();

                ResetRift();
            }
        }
        // Setzt Grenzwerte von Indikatoren und Aktivatoren zurück.
        public void ResetLimits(bool bBuyLimit, bool bSellLimit)
        {
            foreach (BotReactionIndicator tIndicator in ReactionIndicators)
            {
                if (bBuyLimit)
                    tIndicator.BuyLimitState.Reset();
                if (bSellLimit)
                    tIndicator.SellLimitState.Reset();
            }
            foreach (BotTrigger tTrigger in Triggers)
            {
                if ((tTrigger.ReferredParameters.Direction == BotOrderDirection.eBuy) ? bBuyLimit : bSellLimit)
                    tTrigger.Hysteresis.Reset();
            }
        }
        // Setzt die Statistik für den aktuellen Rift zurück.
        public void ResetRift()
        {
            if (ActiveSettingsClone != null)
            {
                Rift_BuyInfo.ResetRift(ActiveSettingsClone.CfgBuy_Steps);
                Rift_SellInfo.ResetRift();
            }

            RiftSummaries.CurrentRift.Reset();
            pCalc_Rift_PanicShortSale.Reset();

            bBuyActivator.ResetTrigger();
            sSaleActivator.ResetTrigger();

            Calc_Buy_AverageDownRate = 0;
            Calc_Rift_MinProphitSellRate = 0;
        }


        //De-/Aktiviert Settings.
        protected bool ActivateSettings(BotStrategySettings sSettings = null, bool bForceActivate = false)
        {
            bool bResult = true;

            if ((BotBaseParameters.Compare(ActiveSettingsClone, sSettings) == false) || (bForceActivate))
            {
                // Bestehende Settings löschen:
                ActivationTime = default(DateTime);
                RemainingActiveDuration = TimeSpan.Zero;

                TrendIndicator = null;
                ReactionIndicators = null;
                Triggers = null;
                Rift_BuyInfo = null;
                Rift_SellInfo = null;

                ActiveSettingsClone = null;

                GC.Collect();

                // Ggf. Konfiguration auf Plausibilität prüfen:
                if ((sSettings != null) && (sSettings.CheckPlausibility() == false))
                {
                    sSettings = null;
                    bResult = false;
                }

                // Ggf. Settings anwenden:
                if (sSettings != null)
                {
                    // Kopie referenzierter Einstellungen erstellen:
                    ActiveSettingsClone = new BotStrategySettings();

                    if (ActiveSettingsClone.CopyFrom(sSettings))
                    {
                        Color[] cGraphColor = { Color.Green, Color.DodgerBlue, Color.Violet, Color.Orange, Color.Chocolate };

                        Debug.Assert((cGraphColor.Length == IND_ARRAY_COUNT), "[BTS626] Anzahl an Farben für Indikator-Graphen ungültig!");

                        // Variablen initialisieren:
                        ActivationTime = DateTime.Now;

                        Rift_BuyInfo = new BotRiftInfo(ActiveSettingsClone, BotOrderDirection.eBuy);
                        Rift_SellInfo = new BotRiftInfo(ActiveSettingsClone, BotOrderDirection.eSell);

                        // Rating-Indikators initialisieren:
                        TrendIndicator = new BotTrendIndicator(ActiveSettingsClone.TrendIndicator, Color.Crimson);

                        // Reaction-Indikators initialisieren:
                        ReactionIndicators = new BotReactionIndicator[IND_ARRAY_COUNT];
                        for (int i = 0; i < IND_ARRAY_COUNT; i++)
                            ReactionIndicators[i] = new BotReactionIndicator(ActiveSettingsClone.ReactionIndicators[i], cGraphColor[i], Calc_Variables);

                        // Trigger initialisieren:
                        Triggers = new BotTrigger[TRIG_ARRAY_COUNT];
                        for (int i = 0; i < TRIG_ARRAY_COUNT; i++)
                            Triggers[i] = new BotTrigger(ActiveSettingsClone.Trigger[i], Calc_Variables);

                        // Build cumulative lists:
                        Indicators = new List<BotBaseIndicator>();

                        Indicators.Add(TrendIndicator);
                        Indicators.AddRange(ReactionIndicators);

                        ApplyParameters();

                        // Aktuellen Zustand zurücksetzen:
                        ResetRift();
                    }
                    else
                    {
                        ActiveSettingsClone = null;
                        return (false);
                    }
                }

                // Variablen zurücksetzen:
                Calc_Variables.Reset();

                if (RestartCycle())
                {
                    // Benutzer informieren:
                    String sMessage = String.Format("Anwenden aktiver Einstellungen für '{0}': ", Title);

                    if (ActiveSettingsClone == null)
                        sMessage += "<Inaktiv>";
                    else
                        sMessage += String.Format("'{0}'", ActiveSettingsClone.Title);

                    BotEnv.LogSystem.WriteLog("AS2917", BotLogProcess.eConfig, sMessage, BotLogType.eInfo, BotLogPriority.eLow);
                }
            }

            return (bResult);
        }
        // Löst das Anwenden von Parametern (wo es notwendig ist) manuell aus:
        public bool ApplyParameters(bool bPendingChangesOnly = false)
        {
            if (IsActive)
            {
                String sMsgFormat = "Übernehmen geänderter Parameter '{0}' für '{1}'.";
                bool bResult = true;
                bool bExecuteStep;

                // Schritt 1):
                // > Selektor-Parameter anwenden:
                if (SelectorInfo != null)
                {
                    Debug.Assert((ReferredParameters != null), "[AP2960]");
                    Debug.Assert((ReferredParameters.GetType() == typeof(BotSelectorParameters)), "[AP2961]");

                    bExecuteStep = ((bPendingChangesOnly == false) || (ReferredParameters.HasPendingChanges));

                    if (ReferredParameters.HasPendingChanges)
                        // Geänderte Parameter übernehmen:
                        bResult = (SelectorInfo.CopyParameters((BotSelectorParameters)ReferredParameters));

                    if (bResult)
                    {
                        if (bExecuteStep)
                        {
                            // Änderungen übernehmen:
                            SelectorInfo.ApplyParameters();

                            if (bPendingChangesOnly)
                                BotEnv.LogSystem.WriteLog("AP2972", BotLogProcess.eConfig, String.Format(sMsgFormat, ReferredParameters.Title, Title), BotLogType.eInfo, BotLogPriority.eLow);
                        }
                    }
                    else
                    {
                        // [Debug]:
                        // > Fehlschlagen von Selektoren (noch) nicht berücksichtigt!
                        Debug.Assert(false, "[AP2959] Apply Selektor-Parameters");

                        return (false);
                    }
                }

                // Schritt 2):
                // > Settings-Parameter anwenden:
                BotStrategySettings sOriginal = ActiveSettingsOriginal;

                if (sOriginal != null)
                {
                    Debug.Assert((ActiveSettingsClone != null), "[AP2982]");

                    bExecuteStep = ((bPendingChangesOnly == false) || (ActiveSettingsOriginal.HasPendingChanges));

                    if (ActiveSettingsOriginal.HasPendingChanges)
                        // Geänderte Parameter übernehmen:
                        bResult = (ActiveSettingsClone.CopyFrom(sOriginal));

                    if ((bResult) && (bExecuteStep))
                    {
                        // Änderungen übernehmen.

                        bResult = ((bResult) && (TrendIndicator.ApplyParameters(Calc_Variables)));

                        foreach (BotReactionIndicator rIndicator in ReactionIndicators)
                            bResult = ((bResult) && (rIndicator.ApplyParameters(Calc_Variables)));

                        foreach (BotTrigger tTrigger in Triggers)
                            bResult = ((bResult) && (tTrigger.ApplyParameters(Calc_Variables)));

                        if (bResult)
                        {
                            // Einstellungen konnten angewendet werden:
                            // > Zyklus neustarten:
                            RestartCycle();

                            if ((bPendingChangesOnly) && (BotTradeBot.Active))
                                BotEnv.LogSystem.WriteLog("AP3021", BotLogProcess.eConfig, String.Format(sMsgFormat, ActiveSettingsClone.Title, Title), BotLogType.eInfo, BotLogPriority.eLow);
                        }
                        else
                            // Einstellungen konnten nicht angewendet werden:
                            // > Original-Settings aktivieren (und Rücksetzen der aktuellen Strategie-Kalkulation in kauf nehmen):
                            bResult = ActivateSettings(ActiveSettingsOriginal);
                    }
                }

                return (bResult);
            }
            else
                return (false);
        }

        protected bool RestartCycle()
        {
            if ((sState == BotState.eIdle) || (sState == BotState.eActive))
            {
                // Initialisieren:
                IsFirstCycle = true;

                RecalcStatistics();

                return (true);
            }
            else
                return (false);
        }
        #endregion
        #region Management.CoinStream
        public bool RecordCoinStream()
        {
            if ((cCoinStreamRecorder == null) && (IsReady))
            {
                // Aufnahme starten:
                cCoinStreamRecorder = new BotCoinStreamRecorder(this);
                return (true);
            }
            else
                return (false);
        }
        public bool StopCoinStreamRecord()
        {
            if (cCoinStreamRecorder == null)
                return (false);
            else
            {
                // Aufnahme beenden:
                cCoinStreamRecorder.Close();

                cCoinStreamRecorder = null;
                return (true);
            }
        }
        #endregion
        #region Management.TradingStrategy
        // Berechnet den aktuellen Trend.
        // > Dient als Einschätzung für genauere geplate Operationen der jeweiligen Strategie.
        public bool RecalcStatistics(bool bWarm = false)
        {
            bool bResult = false;

            if (BotCurrencyRating.State == BotCurrencyRating.BotState.eBusy)
            {
                // Update noch nicht abgeschlossen:
                // > Warten.
            }
            else
            {
                if (bWarm)
                {
                    if ((sState > BotState.eDisabled) && (BotCurrencyRating.State == BotCurrencyRating.BotState.eValid))
                    {
                        // Aktuellen Zustand aktualisieren:
                        // > So wird Berechnung mit nächstem Zyklus ausgeführt.
                        sState = BotState.eCalculating;

                        bResult = true;
                    }
                }
                else
                {
                    BotCandleSubscription cCandles = BotCurrencyRating.GetCandles(this);
                    if (cCandles == null)
                        // Candles nicht gefunden!
                        // > D.h. beim letzten Currency-Rating Update konnten keine Informationen ermittelt werden.
                        sState = BotState.eError;
                    else
                    {
                        IBotCoinInfo bCoinInfo = ((BotCurrencyRating.State == BotCurrencyRating.BotState.eValid) ? BotCurrencyRating.CoinInfo.Find(TradingPair.CurrencyPrim) : null);
                        if (bCoinInfo != null)
                        {
                            if (IsFirstCycle)
                                // Initialisieren:
                                ResetCalc(true);

                            if (ActiveSettingsClone == null)
                                sState = BotState.eIdle;
                            else
                            {
                                // Moving Averages berechnen:
                                foreach (BotBaseIndicator bIndicator in Indicators)
                                    bIndicator.Build(cCandles);

                                // Kalkulation abgeschlossen:
                                sState = BotState.eActive;
                            }

                            dLastRecalc = BotCurrencyRating.LastUpdate;

                            // Owner benachrichtigen:
                            BotEnv.Send(() => BotEnv.OnRecalcStatistik?.Invoke(this));

                            bResult = true;
                        }
                    }
                }
            }

            return (bResult);
        }
        // Berechnet den aktuellen Beutezug.
        // > Z.B. Grenzwerte für Ein-/Verkauf-Orders der jeweiligen Straegie usw.
        public void ReaclcCycle(bool bForceRecalc = false)
        {
            Debug.Assert((TradingPair.LastRateValue != 0), "[RR1978]");

            if ((sState == BotState.eActive) && (bIsWaitingForThreads == false))
            {
                double dCurRate = TradingPair.LastRateValue;
                bool _bForce;

                // Verkauf-Orders:
                if ((bForceRecalc) || (ActiveSettingsClone.CfgNextSell_Trailing))
                {
                    double dBuyAvgRate = RiftSummaries.CurrentRift.Buys.TotalAverageRate;
                    if (dBuyAvgRate == 0)
                    {
                        // Es existiert kein durchschnittlicher Einkaufskurs.
                        // > D.h. es gibt keine (Zuvor eingekauften) Beträge (Wallet NICHT berücksichtigt!) die verkauft werden können.
                        Rift_SellInfo.Reset(true);

                        Calc_Rift_MinProphitSellRate = 0;
                    }
                    else
                    {
                        // Wenn Grenzwert für Verkauf-Orders deaktiviert ist:
                        // > Trailing für Grenzwert erzwingen!
                        _bForce = ((bForceRecalc) || (SellSlow_LimitEnabled == false));

                        // Neue Grenzwerte für Verkauf-Orders berechnen:
                        if (Rift_SellInfo.NextOrderLimit.Recalc(dCurRate, ActiveSettingsClone.CfgNextSell_RateDistanceFactor, false, _bForce))
                        {
                            // Grezwert für erzwungenen Leerverkauf ermitteln:
                            // > Entspricht übernächstem Verkaufs-Grenzwert.
                            double dOpprSellLimit = (Rift_SellInfo.NextOrderLimit.LimitValue - Rift_SellInfo.NextOrderLimit.Distance);

                            // Verkaufskurs für minimalen Profit berechnen:
                            Calc_Rift_MinProphitSellRate = (dBuyAvgRate * (1.0 + (ActiveSettingsClone.CfgNextSell_MinProphitPercent_BuyAvgRate / 100)));

                            // Verkaufs-Bedingungen prüfen:
                            CalcOrderCondition(sSaleActivator.Lock, ActiveSettingsClone.CfgSell_Condition, Rift_SellInfo, dCurRate);

                            // Bedingungen für "Verkaufen in Bedrängnis" prüfen:
                            sSaleActivator.NextSellOppressive = false;

                            if ((ActiveSettingsClone.CfgShortSale_Oppressive) && (sSaleActivator.Lock.IsSet(BotOrderActivator.BotLockReason.eCondition_Slow)))
                            {
                                switch (ActiveSettingsClone.CfgSell_Condition)
                                {
                                    case BotOrderCondition.eMinProphit:
                                        sSaleActivator.NextSellOppressive = (Calc_Rift_MinProphitSellRate > dOpprSellLimit);
                                        break;
                                    case BotOrderCondition.eMinBuyAverage:
                                        sSaleActivator.NextSellOppressive = (dBuyAvgRate > dOpprSellLimit);
                                        break;
                                }
                            }
                        }
                    }
                }

                sSaleActivator.ResetTrigger(true);

                // Kauf-Orders:
                if ((bForceRecalc) || (ActiveSettingsClone.CfgNextBuy_Trailing))
                {
                    if (Rift_BuyInfo.RemainingSteps.Total == 0)
                        // Es verbleiben keine weiteren Einkaufs-Orders:
                        Rift_BuyInfo.Reset(true);
                    else
                    {
                        // Wenn Grenzwert für Kauf-Orders deaktiviert ist:
                        // > Trailing für Grenzwert erzwingen!
                        _bForce = ((bForceRecalc) || (BuySlow_LimitEnabled == false));

                        // Neue Grenzwerte für Kauf-Orders berechnen:
                        if (Rift_BuyInfo.NextOrderLimit.Recalc(dCurRate, ActiveSettingsClone.CfgNextBuy_RateDistanceFactor, true, _bForce))
                        {
                            // Kauf-Bedingungen prüfen:
                            CalcOrderCondition(bBuyActivator.Lock, ActiveSettingsClone.CfgBuy_Condition, Rift_BuyInfo, dCurRate);

                            bBuyActivator.AverageDown_Slow = false;
                            bBuyActivator.AverageDown_Fast = false;

                            if (Rift_BuyInfo.RemainingSteps.IsInReserve)
                                // Es verbleiben nur noch Reserve-Schritte:
                                bBuyActivator.Lock.Add(BotOrderActivator.BotLockReason.eStepLimit_Reached);
                        }
                    }
                }

                // Ggf. CoolDown aktualisieren:
                Calc_Buy_CoolDown.Refresh();

                bBuyActivator.Lock.Conditional(!Calc_Buy_CoolDown.IsActive, BotOrderActivator.BotLockReason.eCoolDown);

                bBuyActivator.ResetTrigger(true);

                // Trend-Bewertung für Freigabe von Reaktions-Indikatoren berechnen:
                if (TrendIndicator.ReferredParameters.Enabled)
                {
                    TrendIndicator.EnableState.CalcLimits(this);

                    bBuyActivator.Lock.Conditional(TrendIndicator.EnableState.Enable_BuyIndicator, BotOrderActivator.BotLockReason.eTrendIndicator);
                    sSaleActivator.Lock.Conditional(TrendIndicator.EnableState.Enable_SellIndicator, BotOrderActivator.BotLockReason.eTrendIndicator);
                }
            }
        }
        // Wertet einen neuen Zyklus aus.
        // > Üblicher Weise nach dem Empfang von aktuellen Kurs-Informationen (z.B. Ticker)
        public BotTaskResult EvaluateCycle(BotTickerSubscription tTicker)
        {
            if (cCoinStreamRecorder != null)
                // Aufnahme aktualisieren:
                cCoinStreamRecorder.Update(tTicker);

            if (bIsWaitingForThreads)
                // Aktuelle Instanz wartet auf den Abschluss von aktiven Threads:
                return (BotTaskResult.eBusy);
            else if ((sState == BotState.eInit) && (BotCurrencyRating.State == BotCurrencyRating.BotState.eBusy))
                // Aktuelle Instanz wartet auf den Abschluss von Currency-Rating:
                return (BotTaskResult.eBusy);
            else
            {
                if (tTicker.lData.Count > 0)
                {
                    BotTickerSubscription.BotTickerData tLatestData = tTicker.lData.Last();
                    BotTaskResult tResult = BotTaskResult.eError;
                    double dAmount = 0;

                    LastPrice = tLatestData.dLastPrice;

                    if ((sState > BotState.eDisabled) && (BotCurrencyRating.State == BotCurrencyRating.BotState.eValid))
                    {
                        bool bRecalc = IsFirstCycle;

                        if (CoinStreamPlayer == null)
                        {
                            if (BotCurrencyRating.LastUpdate != dLastRecalc)
                                bRecalc = true;
                        }

                        if (bRecalc)
                            // Aktuelles Currency-Rating wurde aktualisiert.
                            // > Statistik neuberechnen:
                            RecalcStatistics(true);
                    }

                    if (sState == BotState.eCalculating)
                    {
                        RecalcStatistics();

                        if (ActiveSettingsClone != null)
                        {
                            // Moving Averages berechnen:
                            foreach (BotBaseIndicator bIndicator in Indicators)
                                bIndicator.Build(tTicker);
                        }

                        // (BETA) ... [RATING]
                        // Alter Code:
                        /*
                        if (lIntervals.Count == 0)
                        {
                            // Candle-Sticks wurden noch nicht empfangen.
                        }
                        else if (IsReady())
                            // Kalkulation abgeschlossen:
                            sState = State.BTSS_Active;
                        */
                    }

                    switch (sState)
                    {
                        case BotState.eIdle:
                            // Formeln aktualisieren:
                            UpdateFormula(tLatestData);

                            // Ggf. manuelles Auslösen von Orders verarbeiten:
                            if (sSaleActivator.TriggerReason == BotOrderActivator.BotTriggerReason.eUser)
                                dAmount = GetSellAmount();

                            else if (bBuyActivator.TriggerReason == BotOrderActivator.BotTriggerReason.eUser)
                                dAmount = GetBuyAmount();

                            tResult = BotTaskResult.eSuccess;
                            break;

                        case BotState.eActive:
                            // Anzahl an Ticker-Daten begrenzen:
                            while (tTicker.lData.Count > TS_MAX_ENTRIES)
                                tTicker.lData.RemoveAt(0);

                            // Zyklus neuberechnen:
                            ReaclcCycle(IsFirstCycle);

                            if (tTicker.lData.Count > 0)
                            {
                                double dBuyAvgRate = RiftSummaries.CurrentRift.Buys.TotalAverageRate;

                                // Aktuelle Ticker-Informationen auswerten:
                                pCalc_Rift_PanicShortSale.ExecuteRate = 0;
                                Calc_Buy_AverageDownRate = 0;

                                LastBuyInfo.Update(tTicker);
                                LastSellInfo.Update(tTicker);

                                if (ActiveSettingsClone.CfgShortSale_Panic.Enabled)
                                {
                                    if (pCalc_Rift_PanicShortSale.Active == false)
                                    {
                                        if (ActiveSettingsClone.CfgSell_Condition == BotOrderCondition.eMinProphit)
                                            // Prüfen ob aktueller Kurs unter die minimale Profit-Grenze fällt:
                                            pCalc_Rift_PanicShortSale.Active = (tLatestData.dLastPrice >= Calc_Rift_MinProphitSellRate);
                                        else
                                            // Panik-Verkauf aktivieren sobald Kauf-Orders platziert wurden:
                                            pCalc_Rift_PanicShortSale.Active = (dBuyAvgRate != 0);
                                    }
                                    else
                                    {
                                        // Zu unterschreitenden Grenzwert berechnen:
                                        if (ActiveSettingsClone.CfgSell_Condition == BotOrderCondition.eMinProphit)
                                        {
                                            if (Calc_Rift_MinProphitSellRate > 0)
                                                // Grenzwert abhängig von Profit:
                                                pCalc_Rift_PanicShortSale.ExecuteRate = (dBuyAvgRate + ((Calc_Rift_MinProphitSellRate - dBuyAvgRate) * ActiveSettingsClone.CfgShortSale_Panic.ExecuteLimit / 100));
                                        }
                                        else
                                        {
                                            if (dBuyAvgRate > 0)
                                                // Grenzwert abhängig von durchschnittlichem Einkaufspreis:
                                                pCalc_Rift_PanicShortSale.ExecuteRate = (dBuyAvgRate * (ActiveSettingsClone.CfgShortSale_Panic.ExecuteLimit / 100));
                                        }

                                        if (tLatestData.dLastPrice <= pCalc_Rift_PanicShortSale.ExecuteRate)
                                            // Panik-Leerverkauf aktiv wenn Profit-Grenzwert unterschritten wird:
                                            sSaleActivator.TriggerReason = BotOrderActivator.BotTriggerReason.ePanic;
                                    }
                                }

                                // Ggf. Neutrale Zone von Verkauf-Orders berechnen:
                                Rift_SellInfo.NeutralZone.Update(tLatestData.dLastPrice);

                                // NIEMALS Sell-Orders via neutraler Zone blockieren:
                                sSaleActivator.Lock.Remove(BotOrderActivator.BotLockReason.eNeutralZone);

                                if ((ActiveSettingsClone.CfgSell_Condition != BotOrderCondition.eNone) && (ActiveSettingsClone.CfgBuy_AverageDown.Enabled))
                                {
                                    // Maximalen Kurs für Nachkaufen berechnen:
                                    Calc_Buy_AverageDownRate = (dBuyAvgRate * (ActiveSettingsClone.CfgBuy_AverageDown.Distance / 100));

                                    // Nachkaufen freigeben wenn entsprechender Grenzwert unterschritten wird:
                                    if (Rift_BuyInfo.NextOrderLimit.LimitValue <= Calc_Buy_AverageDownRate)
                                    {
                                        bBuyActivator.Lock.Remove(BotOrderActivator.BotLockReason.eCondition_Slow);
                                        bBuyActivator.AverageDown_Slow = true;
                                    }
                                    if (tLatestData.dLastPrice <= Calc_Buy_AverageDownRate)
                                    {
                                        bBuyActivator.Lock.Remove(BotOrderActivator.BotLockReason.eStepLimit_Reached);
                                        bBuyActivator.Lock.Remove(BotOrderActivator.BotLockReason.eCondition_Fast);
                                        bBuyActivator.AverageDown_Fast = true;
                                    }
                                }

                                // Ggf. Kauf-Orders blockieren wenn neutrale Zone von Sell-Orders aktiv ist:
                                bBuyActivator.Lock.Conditional(!Rift_SellInfo.NeutralZone.Active, BotOrderActivator.BotLockReason.eNeutralZone);

                                // Aktualisierung:
                                {
                                    // Formeln aktualisieren (Unabhängige Indikatoren):
                                    UpdateFormula(tLatestData, BotFormulaUpdateMode.eIndependant);

                                    // Indikatoren aktualisieren:
                                    foreach (BotBaseIndicator bIndicator in Indicators)
                                        bIndicator.Update();

                                    // Grenzwerte für Platzierung von Orders berechnen:
                                    foreach (BotReactionIndicator tIndicator in ReactionIndicators)
                                    {
                                        if (tIndicator.ReferredParameters.Enabled)
                                        {
                                            tIndicator.BuyLimitState.CalcLimits(this, TrendIndicator);
                                            tIndicator.SellLimitState.CalcLimits(this, TrendIndicator);
                                        }
                                    }
                                    foreach (BotTrigger tTrigger in Triggers)
                                        tTrigger.CalcLimits(this);

                                    // Formeln aktualisieren (Unabhängige Indikatoren):
                                    UpdateFormula(tLatestData, BotFormulaUpdateMode.eDependant);
                                }

                                // Prüfen ob eine blockierende Order existiert:
                                BotBaseOrder bBlockingOrder = Platform.IsOrderingBusy(tTicker.ReferredPair);

                                if (bBlockingOrder == null)
                                {
                                    // Auswertung Grenzwerte:
                                    // > Verkauf-Orders
                                    if (sSaleActivator.TriggerReason == BotOrderActivator.BotTriggerReason.None)
                                    {
                                        foreach (BotReactionIndicator tIndicator in ReactionIndicators)
                                        {
                                            if (tIndicator.ReferredParameters.Enabled)
                                            {
                                                if ((sSaleActivator.Lock.IsSet(BotOrderActivator.BotLockReason.eCondition_Slow) == false) &&
                                                    ((tIndicator.SellLimitState.SlowRating) && (Rift_SellInfo.NextOrderLimit.IsValid) && (tLatestData.dLastPrice <= Rift_SellInfo.NextOrderLimit.LimitValue)))
                                                    sSaleActivator.TriggerReason = BotOrderActivator.BotTriggerReason.eIndicator;

                                                else if ((sSaleActivator.Lock.IsSet(BotOrderActivator.BotLockReason.eCondition_Fast) == false) &&
                                                    ((tIndicator.SellLimitState.FastEnabled) && (tIndicator.VariationPercent <= tIndicator.SellLimitState.FastExecuteLimit)))
                                                    sSaleActivator.TriggerReason = BotOrderActivator.BotTriggerReason.eIndicator;

                                                else
                                                    continue;

                                                sSaleActivator.TriggerFunction = tIndicator;
                                                break;
                                            }
                                        }
                                    }
                                    if (sSaleActivator.TriggerReason == BotOrderActivator.BotTriggerReason.None)
                                    {
                                        foreach (BotTrigger tTrigger in Triggers)
                                        {
                                            if ((tTrigger.ReferredParameters.Enabled) && (tTrigger.ReferredParameters.Direction == BotOrderDirection.eSell))
                                            {
                                                if (((tTrigger.Hysteresis.Enabled) && (tLatestData.dLastPrice <= tTrigger.Hysteresis.ExecuteLimit)))
                                                {
                                                    sSaleActivator.TriggerReason = BotOrderActivator.BotTriggerReason.eTrigger;
                                                    sSaleActivator.TriggerFunction = tTrigger;
                                                    break;
                                                }
                                            }
                                        }
                                    }

                                    if (sSaleActivator.TriggerReason != BotOrderActivator.BotTriggerReason.None)
                                    {
                                        dAmount = GetSellAmount();

                                        if (sSaleActivator.TriggerFunction != null)
                                            sSaleActivator.TriggerFunction.EvaluateStateGetResetMode(sSaleActivator, ref dAmount, tLatestData.dLastPrice);
                                    }

                                    // Auswertung Grenzwerte:
                                    // > Einkauf-Orders
                                    if (dAmount == 0)
                                    {
                                        if (bBuyActivator.TriggerReason == BotOrderActivator.BotTriggerReason.None)
                                        {
                                            // Auswertung Grenzwerte für Kauf-Orders:
                                            foreach (BotReactionIndicator tIndicator in ReactionIndicators)
                                            {
                                                if (tIndicator.ReferredParameters.Enabled)
                                                {
                                                    if ((tIndicator.BuyLimitState.SlowRating) && (Rift_BuyInfo.NextOrderLimit.IsValid) && (tLatestData.dLastPrice >= Rift_BuyInfo.NextOrderLimit.LimitValue))
                                                        bBuyActivator.TriggerReason = BotOrderActivator.BotTriggerReason.eIndicator;

                                                    else if ((tIndicator.BuyLimitState.FastEnabled) && (tIndicator.VariationPercent >= tIndicator.BuyLimitState.FastExecuteLimit))
                                                        bBuyActivator.TriggerReason = BotOrderActivator.BotTriggerReason.eIndicator;

                                                    else
                                                        continue;

                                                    bBuyActivator.TriggerFunction = tIndicator;
                                                    break;
                                                }
                                            }
                                        }
                                        if (bBuyActivator.TriggerReason == BotOrderActivator.BotTriggerReason.None)
                                        {
                                            foreach (BotTrigger tTrigger in Triggers)
                                            {
                                                if ((tTrigger.ReferredParameters.Enabled) && (tTrigger.ReferredParameters.Direction == BotOrderDirection.eBuy))
                                                {
                                                    if (((tTrigger.Hysteresis.Enabled) && (tLatestData.dLastPrice >= tTrigger.Hysteresis.ExecuteLimit)))
                                                    {
                                                        bBuyActivator.TriggerReason = BotOrderActivator.BotTriggerReason.eTrigger;
                                                        bBuyActivator.TriggerFunction = tTrigger;
                                                        break;
                                                    }
                                                }
                                            }
                                        }

                                        if (bBuyActivator.TriggerReason != BotOrderActivator.BotTriggerReason.None)
                                        {
                                            dAmount = GetBuyAmount();

                                            if (bBuyActivator.TriggerFunction != null)
                                                bBuyActivator.TriggerFunction.EvaluateStateGetResetMode(bBuyActivator, ref dAmount, tLatestData.dLastPrice);
                                        }
                                    }
                                }
                                else
                                {
                                    // Zur Zeit wird eine Order ausgeführt!
                                    // > Das blockiert das Ausführen von weiteren Orders bis zum Abschluss der ausstehenden Order.

                                    if ((Platform.OrderSettings.Timeout > TimeSpan.Zero) &&
                                        ((DateTime.Now - bBlockingOrder.CreationTime) > Platform.OrderSettings.Timeout))
                                    {
                                        BotEnv.LogSystem.WriteLog("EC3608", BotLogProcess.eOrder, String.Format("Abbrechen der letzten Order ('{0}') da diese nicht (vollständig innerhalb von {1}) ausgeführt wurde.", bBlockingOrder.tRefPair.Symbol, Platform.OrderSettings.Timeout), BotLogType.ePlatformInfo, BotLogPriority.eLow);

                                        // Blockierende Order stornieren:
                                        bBlockingOrder.oState = BotOrderState.Canceling;

                                        Platform.CancelOrder(bBlockingOrder);
                                    }
                                }
                            }

                            tResult = BotTaskResult.eSuccess;
                            break;
                    }

                    if (tResult == BotTaskResult.eSuccess)
                    {
                        if (dAmount != 0)
                            // Order plazieren:
                            Platform.PlaceOrder(TradingPair, dAmount);
                        else
                        {
                            // Zyklus abschließen:
                            IsFirstCycle = false;

                            // Aktive Parameter ermitteln:
                            ValidateActiveParameters();
                        }
                    }

                    return (tResult);
                }
                else
                {
                    sState = BotState.eError;
                    Debug.Assert(false, "[EC231]");
                }
            }

            return (BotTaskResult.eError);
        }
        #endregion


        #region Configuration
        public void WriteFile(JsonWriter jWriter, BotContentType bContent)
        {
            jWriter.WriteStartObject();
            {
                jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_PAIR);
                jWriter.WriteValue(TradingPair.Symbol);

                switch (bContent)
                {
                    case BotContentType.eConfig:
                        jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_ACTIVE);
                        jWriter.WriteValue(Enabled);

                        jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_BOTAUTOMATIC_ISUSABLE);
                        jWriter.WriteValue(IsUsableByBotAutomatik);

                        jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_STRATEGY_REF_PARAMETERS);
                        jWriter.WriteValue((ReferredParameters == null) ? "" : ReferredParameters.GUID.ToString());

                        jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_BOTAUTOMATIC_ISUSABLE);
                        jWriter.WriteValue(IsUsableByBotAutomatik);

                        Calc_Buy_CoolDown.WriteFile(jWriter);

                        if (IsRiftActive)
                        {
                            // Persistente Informationen speichern:
                            jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_PERSISTENT);
                            jWriter.WriteStartObject();
                            {
                                // Angewendete Settings speichern:
                                jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_STRATEGY_ACTIVE_SETTINGS);
                                jWriter.WriteValue(ActiveSettingsClone.GUID.ToString());

                                // Informationen zu aktuellem Rift speichern:
                                jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_RIFT);
                                RiftSummaries.CurrentRift.WriteFile(jWriter);

                                // Weitere persistente Informationen speichern:
                                LastBuyInfo.WriteFile(jWriter, BotConvention.DATAFILE_JSON_LASTBUYORDER);
                                LastSellInfo.WriteFile(jWriter, BotConvention.DATAFILE_JSON_LASTSELLORDER);

                                jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_CALC_RIFT_REMAININGBUYSTEPS_DEFAULT);
                                jWriter.WriteValue(Rift_BuyInfo.RemainingSteps.Default);
                                jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_CALC_RIFT_REMAININGBUYSTEPS_RESERVE);
                                jWriter.WriteValue(Rift_BuyInfo.RemainingSteps.Reserve);

                                jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_CALC_RIFT_REMAININGSELLSTEPS);
                                jWriter.WriteValue(Rift_SellInfo.RemainingSteps.Default);
                                jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_CALC_RIFT_NEXTSELLOPPRESSIVE);
                                jWriter.WriteValue(sSaleActivator.NextSellOppressive);

                                jWriter.WriteEndObject();
                            }
                        }

                        break;

                    case BotContentType.eRiftSummary:
                        RiftSummaries.WriteFile(jWriter);
                        break;

                    default:
                        Debug.Assert(false, "[RF653]");
                        break;
                }

                jWriter.WriteEndObject();
            }
        }
        public bool ReadFile(JObject dObjects, BotContentType bContent)
        {
            bool bResult = false;

            switch (bContent)
            {
                case BotContentType.eConfig:
                    JToken _jToken;

                    // Einstellungen lesen:
                    _jToken = dObjects[BotConvention.DATAFILE_JSON_SETTINGS];

                    Enabled = dObjects.ParseValue(BotConvention.DATAFILE_JSON_ACTIVE, false);

                    IsUsableByBotAutomatik = dObjects.ParseValue(BotConvention.DATAFILE_JSON_BOTAUTOMATIC_ISUSABLE, IsUsableByBotAutomatik);
                    ReferredParameters = BotParameterManager.GetParameters((String)dObjects.ParseValue(BotConvention.DATAFILE_JSON_STRATEGY_REF_PARAMETERS, ""));

                    Calc_Buy_CoolDown.ReadFile(dObjects);

                    // Persistente Informationen lesen:
                    _jToken = dObjects[BotConvention.DATAFILE_JSON_PERSISTENT];
                    if (_jToken != null)
                    {
                        // Zuletzt angewendete Settings lesen:
                        BotBaseParameters bLastActiveSettings = BotParameterManager.GetParameters((String)_jToken.ParseValue(BotConvention.DATAFILE_JSON_STRATEGY_ACTIVE_SETTINGS, ""));

                        if ((bLastActiveSettings != null) && (bLastActiveSettings.GetType() == typeof(BotStrategySettings)))
                        {
                            // Zuletzt angewendete Settings aktivieren:
                            if (ActivateSettings((BotStrategySettings)bLastActiveSettings))
                            {
                                // Informationen zu aktuellem Rift lesen:
                                RiftSummaries.CurrentRift.ReadFile((dynamic)_jToken[BotConvention.DATAFILE_JSON_RIFT]);

                                // Weitere persistente Informationen lesen:
                                LastBuyInfo.ReadFile(_jToken, BotConvention.DATAFILE_JSON_LASTBUYORDER);
                                LastSellInfo.ReadFile(_jToken, BotConvention.DATAFILE_JSON_LASTSELLORDER);

                                Rift_BuyInfo.RemainingSteps.Default = _jToken.ParseValue(BotConvention.DATAFILE_JSON_CALC_RIFT_REMAININGBUYSTEPS_DEFAULT, Rift_BuyInfo.RemainingSteps.Default);
                                Rift_BuyInfo.RemainingSteps.Reserve = _jToken.ParseValue(BotConvention.DATAFILE_JSON_CALC_RIFT_REMAININGBUYSTEPS_RESERVE, Rift_BuyInfo.RemainingSteps.Reserve);

                                Rift_SellInfo.RemainingSteps.Default = _jToken.ParseValue(BotConvention.DATAFILE_JSON_CALC_RIFT_REMAININGSELLSTEPS, Rift_SellInfo.RemainingSteps.Default);
                                sSaleActivator.NextSellOppressive = _jToken.ParseValue(BotConvention.DATAFILE_JSON_CALC_RIFT_NEXTSELLOPPRESSIVE, sSaleActivator.NextSellOppressive);
                            }
                        }
                        else
                            BotEnv.LogSystem.WriteLog("EPD2599", BotLogProcess.eConfig, "Fehler beim Anwenden persistenter Informationen: Daten sind ungültig!", BotLogType.eWarning, BotLogPriority.eHigh);
                    }

                    bResult = true;
                    break;

                case BotContentType.eRiftSummary:
                    bResult = RiftSummaries.ReadFile(dObjects);
                    break;

                default:
                    Debug.Assert(false, "[RF653]");
                    break;
            }

            return (bResult);
        }


        internal const BotCoinType TRADE_BASE_CURRENCY = BotCoinType.eUSDollar;                                       // Basis-Währung (z.B. für Übersetzungen in andere Währungen)
        internal const BotCoinType TRADE_EXCHANGE_CURRENCY = BotCoinType.eEuro;                                       // Währung für Gegenwerte
        internal const BotCoinType CFG_FIRST_CRYPTO_CURRENCY = (BotConvention.TRADE_BASE_CURRENCY + 1);

        internal readonly TimeSpan CFG_STAT_CANDLE_TIMEFRAME = new TimeSpan(0, 5, 0);                           // Zeitlicher Intervall von Candle-Sticks für Neuberechnung
        internal const int CFG_STAT_AVERAGE_INTERVAL = 30;                                                      // Zeitspanne (in [min]) aus der Kurswerte zu einem Durschnittswert zusammengefasst werden
        internal const int CFG_STAT_AVERAGE_LATEST = 60;                                                        // Zeitspanne (in [min]) aus der Kurswerte (aus jüngster Vergangenheit) zu einem Durschnittswert zusammengefasst werden
        internal const int CFG_STAT_100PERC_TREND_LENGTH = 120;                                                 // Zeitspanne (in [min]) bei der die Dauer eines Trends maximal gut (100%) bewertet wird
        #endregion


        #region Display
        #endregion
        #region Formula
        protected bool UpdateFormula(BotTickerSubscription.BotTickerData tLastData, BotFormulaUpdateMode fMode = BotFormulaUpdateMode.eAll)
        {
            BotCandleSubscription cCandles = BotCurrencyRating.GetCandles(this);

            if (cCandles == null)
                Debug.Assert(false, "[UF2467] Candle-Sticks konnten nicht ermittelt werden!");
            else
            {
                BotCandleSubscription.BotCandleData cLastCandle = cCandles.lData.Last();
                IBotCoinInfo bCoinInfo = BotCurrencyRating.CoinInfo.Find(TradingPair.CurrencyPrim);
                double dLastPrice = tLastData.dLastPrice;

                Debug.Assert((cLastCandle != null), "[UF2472] Dandle-Daten konnten nicht ermitelt werden!");

                // Unabhängige Variablen:
                if (fMode != BotFormulaUpdateMode.eDependant)
                {
                    if (bCoinInfo != null)
                    {
                        Calc_Variables.Ticker.Variables[BotFormula.BotVariables.BotTicker.I_CHANGE_1H].Value = bCoinInfo.PercentChange[BotCurrencyRating.PERCENT_CHANGE_1h];
                        Calc_Variables.Ticker.Variables[BotFormula.BotVariables.BotTicker.I_CHANGE_24H].Value = bCoinInfo.PercentChange[BotCurrencyRating.PERCENT_CHANGE_24h];
                        Calc_Variables.Ticker.Variables[BotFormula.BotVariables.BotTicker.I_CHANGE_7D].Value = bCoinInfo.PercentChange[BotCurrencyRating.PERCENT_CHANGE_7D];
                    }

                    Calc_Variables.Ticker.Variables[BotFormula.BotVariables.BotTicker.I_RATE].Value = dLastPrice;

                    Calc_Variables.Candle.Variables[BotFormula.BotVariables.BotCandle.I_OPEN].Value = cLastCandle.dOpen;
                    Calc_Variables.Candle.Variables[BotFormula.BotVariables.BotCandle.I_HIGH].Value = cLastCandle.dHigh;
                    Calc_Variables.Candle.Variables[BotFormula.BotVariables.BotCandle.I_LOW].Value = cLastCandle.dLow;
                    Calc_Variables.Candle.Variables[BotFormula.BotVariables.BotCandle.I_CLOSE].Value = cLastCandle.dClose;

                    if (ActiveSettingsClone != null)
                    {
                        double dBuyAvgRate = RiftSummaries.CurrentRift.Buys.TotalAverageRate;

                        Calc_Variables.Ticker.Variables[BotFormula.BotVariables.BotTicker.I_BUYAVERAGERATE].Value = dBuyAvgRate;
                        Calc_Variables.Ticker.Variables[BotFormula.BotVariables.BotTicker.I_AVERAGEDOWNLIMIT].Value = Calc_Buy_AverageDownRate;
                        Calc_Variables.Ticker.Variables[BotFormula.BotVariables.BotTicker.I_NEXTBUYLIMIT].Value = Rift_BuyInfo.NextOrderLimit.LimitValue;
                        Calc_Variables.Ticker.Variables[BotFormula.BotVariables.BotTicker.I_NEXTSELLLIMIT].Value = Rift_SellInfo.NextOrderLimit.LimitValue;
                        Calc_Variables.Ticker.Variables[BotFormula.BotVariables.BotTicker.I_PANICSELLLIMIT].Value = Calc_Rift_PanicShortSale_ExecuteRate;
                        Calc_Variables.Ticker.Variables[BotFormula.BotVariables.BotTicker.I_MINPROPHITSELLRATE].Value = Calc_Rift_MinProphitSellRate;

                        Calc_Variables.LastOrders.Variables[BotFormula.BotVariables.BotLastOrders.I_BUY_RATE].Value = LastBuyInfo.Rate;
                        Calc_Variables.LastOrders.Variables[BotFormula.BotVariables.BotLastOrders.I_BUY_PASTTIME].Value = (ulong)LastBuyInfo.PastTime.TotalMinutes;
                        Calc_Variables.LastOrders.Variables[BotFormula.BotVariables.BotLastOrders.I_SELL_RATE].Value = LastSellInfo.Rate;
                        Calc_Variables.LastOrders.Variables[BotFormula.BotVariables.BotLastOrders.I_SELL_PASTTIME].Value = (ulong)LastSellInfo.PastTime.TotalMinutes;

                        if (dBuyAvgRate == 0)
                            Calc_Variables.Rift.Reset();
                        else
                        {
                            Calc_Variables.Rift.Variables[BotFormula.BotVariables.BotRift.I_ACTIVE].Value = 1;
                            Calc_Variables.Rift.Variables[BotFormula.BotVariables.BotRift.I_PROPHIT].Value = (((100 / dBuyAvgRate) * dLastPrice) - 100);
                        }
                    }
                }

                // Ggf. abhängige Variablen:
                if (ActiveSettingsClone != null)
                {
                    Predicate<BotBaseFunction> pTryEvaluate = (bFunction) =>
                    {
                        if (fMode == BotFormulaUpdateMode.eAll)
                            return (true);
                        else
                            return ((bFunction.IsDependant) == (fMode == BotFormulaUpdateMode.eDependant));
                    };

                    if (pTryEvaluate(TrendIndicator))
                    {
                        Calc_Variables.TrendIndicator.Variables[BotFormula.BotVariables.BotTrendIndicator.I_BUY_ORDER_STATE].Value = (TrendIndicator.EnableState.Enable_BuyIndicator ? 1 : 0);
                        Calc_Variables.TrendIndicator.Variables[BotFormula.BotVariables.BotTrendIndicator.I_SELL_ORDER_STATE].Value = (TrendIndicator.EnableState.Enable_SellIndicator ? 1 : 0);
                        Calc_Variables.TrendIndicator.Variables[BotFormula.BotVariables.BotTrendIndicator.I_AVERAGE1].Value = TrendIndicator.ValueAvg1;
                        Calc_Variables.TrendIndicator.Variables[BotFormula.BotVariables.BotTrendIndicator.I_AVERAGE2].Value = TrendIndicator.ValueAvg2;
                        Calc_Variables.TrendIndicator.Variables[BotFormula.BotVariables.BotTrendIndicator.I_VARIATION].Value = TrendIndicator.VariationPercent;
                        Calc_Variables.TrendIndicator.Variables[BotFormula.BotVariables.BotTrendIndicator.I_TENDENCY].Value = TrendIndicator.Tendency;
                    }

                    for (int i = 0; i < ReactionIndicators.Length; i++)
                    {
                        if (pTryEvaluate(ReactionIndicators[i]))
                        {
                            Calc_Variables.ReactionIndicator[i].Variables[BotFormula.BotVariables.BotReactionIndicator.I_AVERAGE1].Value = ReactionIndicators[i].ValueAvg1;
                            Calc_Variables.ReactionIndicator[i].Variables[BotFormula.BotVariables.BotReactionIndicator.I_AVERAGE2].Value = ReactionIndicators[i].ValueAvg2;
                            Calc_Variables.ReactionIndicator[i].Variables[BotFormula.BotVariables.BotReactionIndicator.I_VARIATION].Value = ReactionIndicators[i].VariationPercent;
                            Calc_Variables.ReactionIndicator[i].Variables[BotFormula.BotVariables.BotReactionIndicator.I_TENDENCY].Value = ReactionIndicators[i].Tendency;
                            Calc_Variables.ReactionIndicator[i].Variables[BotFormula.BotVariables.BotReactionIndicator.I_DOUBLECHECK_STATE].Value = (ReactionIndicators[i].DoubleCheckState ? 1 : 0);
                            Calc_Variables.ReactionIndicator[i].Variables[BotFormula.BotVariables.BotReactionIndicator.I_LOW_PRICE].Value = ReactionIndicators[i].LowPrice;
                            Calc_Variables.ReactionIndicator[i].Variables[BotFormula.BotVariables.BotReactionIndicator.I_HIGH_PRICE].Value = ReactionIndicators[i].HighPrice;
                            Calc_Variables.ReactionIndicator[i].Variables[BotFormula.BotVariables.BotReactionIndicator.I_LOW_VARIATION].Value = ReactionIndicators[i].LowVarPercent;
                            Calc_Variables.ReactionIndicator[i].Variables[BotFormula.BotVariables.BotReactionIndicator.I_HIGH_VARIATION].Value = ReactionIndicators[i].HighVarPercent;

                            Calc_Variables.ReactionIndicator[i].Variables[BotFormula.BotVariables.BotReactionIndicator.I_BUYLIMIT_SLOW_RATING_STATE].Value = (ReactionIndicators[i].BuyLimitState.SlowRating ? 1 : 0);
                            Calc_Variables.ReactionIndicator[i].Variables[BotFormula.BotVariables.BotReactionIndicator.I_BUYLIMIT_FAST_ENABLED_STATE].Value = (ReactionIndicators[i].BuyLimitState.FastEnabled ? 1 : 0);
                            Calc_Variables.ReactionIndicator[i].Variables[BotFormula.BotVariables.BotReactionIndicator.I_SELLLIMIT_SLOW_RATING_STATE].Value = (ReactionIndicators[i].SellLimitState.SlowRating ? 1 : 0);
                            Calc_Variables.ReactionIndicator[i].Variables[BotFormula.BotVariables.BotReactionIndicator.I_SELLLIMIT_FAST_ENABLED_STATE].Value = (ReactionIndicators[i].SellLimitState.FastEnabled ? 1 : 0);
                        }
                    }

                    for (int i = 0; i < Triggers.Length; i++)
                    {
                        if (pTryEvaluate(Triggers[i]))
                            Calc_Variables.Trigger[i].Variables[BotFormula.BotVariables.BotTrigger.I_ENABLED].Value = (Triggers[i].Hysteresis.Enabled ? 1 : 0);
                    }
                }

                if (ActiveSettingsClone != null)
                {
                    // Formel prüfen:
                    for (int i = 0; i < IND_ARRAY_COUNT; i++)
                    {
                        ReactionIndicators[i].BuyFormula.Check();
                        ReactionIndicators[i].SellFormula.Check();
                    }
                    for (int i = 0; i < TRIG_ARRAY_COUNT; i++)
                        Triggers[i].Formula.Check();
                }

                // Zeitstempel der Aktualisierung speichern:
                Calc_Variables.UpdateTimeStamp = tLastData.TimeStamp;

                return (true);
            }

            return (false);
        }
        #endregion
        #region Statistics
        protected double dLatestAverage;
        #endregion
        #region Calculation
        public double dCalc_Buy_WeightFactor;                                                                   // Faktor zur Anwendung der Gewichtung ('iCfgBuy_Weight') bei einer Kauf-Order.
        protected BotLimitState pCalc_Rift_PanicShortSale;

        protected bool bIsWaitingForThreads = false;                                                            // Wartet auf eine Gruppe von aktiven Threads
        #endregion


        #region Helper
        // Rechnet einen Betrag von einer Währung in eine andere Währung um.
        protected double TranslateAmount(double dAmount, BotCoinType cPrimCurrency, BotCoinType cSecCurrency, bool bPrim2Sec = true, bool bCanUseCurrencyRating = false)
        {
            if (dAmount != 0)
            {
                if (cPrimCurrency == cSecCurrency)
                    return (dAmount);
                else
                {
                    double dRate = 0;

                    if (TradingPair.Compare(cPrimCurrency, cSecCurrency))
                        // Aktuellsten Kurs (falls bereits ermittelt) für Berechnung anwenden:
                        dRate = TradingPair.LastRateValue;
                    else
                    {
                        BotTradingStrategy _bStrategy = TradingBot.FindStrategy(cPrimCurrency, cSecCurrency);
                        if (_bStrategy != null)
                        {
                            Debug.Assert((_bStrategy != this), "[TA1646]");

                            // Aktuellsten Kurs (falls bereits ermittelt) für Berechnung anwenden:
                            dRate = _bStrategy.TradingPair.LastRateValue;
                        }
                    }

                    if (dRate == 0)
                    {
                        if (bCanUseCurrencyRating)
                            // Übersetzung mit Werten aus Currency-Bewertung durchführen:
                            return (BotCurrencyRating.TranslateAmount(dAmount, cPrimCurrency, cSecCurrency, bPrim2Sec));
                    }
                    else
                        // Betrag in angeforderte Währung umrechnen:
                        return (BotConvention.TranslateAmount(dAmount, dRate, bPrim2Sec));
                }
            }
            return (0);
        }


        // Ermittelt den Betrag einer Kauf-Order der sich aus der Gewichtung und dem Verfügbaren Betrag (in der Wallet) ergibt.
        private double GetBuyAmount()
        {
            double dBuyAmount = 0;

            // Initialisieren:
            bBuyActivator.Lock.Remove(BotOrderActivator.BotLockReason.eInsufficientFunds);

            if (IsReady)
            {
                if ((bBuyActivator.TriggerReason != BotOrderActivator.BotTriggerReason.eUser) &&
                    (ActiveSettingsClone != null) && (Rift_BuyInfo.RemainingSteps.GetTotalStepsEx(bBuyActivator) == 0))
                {
                    // Anzahl an möglichen Schritten bereits erreicht.
                }
                else if (WalletSec.Funds <= 0)
                    // Kein Guthaben für Kauf-Order vorhanden:
                    bBuyActivator.Lock.Add(BotOrderActivator.BotLockReason.eInsufficientFunds);
                else
                {
                    BotTradingStrategy bTranslStr = GetTranslationStrategy();

                    Debug.Assert((bTranslStr != null), "[GBA2285]");
                    Debug.Assert((bTranslStr.TradingPair.LastRateValue > 0), "[GBA2286]");

                    if (bTranslationStrategy != null)
                    {
                        Debug.Assert((TradingPair.LastRateValue > 0), "[GBA2290]");

                        // Verfügbares Guthaben (in [Sekundär-Währung]) in Basis-Währung (in [$]) umrechnen:
                        dBuyAmount = (WalletSec.Funds * bTranslationStrategy.TradingPair.LastRateValue);
                    }
                    else
                        // Verfügbares Guthaben (in [Sekundär-Währung], entspricht [$]) anwenden:
                        dBuyAmount = WalletSec.Funds;

                    if (ActiveSettingsClone != null)
                    {
                        // Anzahl an (verbleibenden) Zwischenschritten berücksichtigen:
                        dBuyAmount = (dBuyAmount / Rift_BuyInfo.RemainingSteps.Total);

                        // Ggf. anzuwendenden Betrag begrenzen (Benutzerdefiniertes Minimum / Maximum):
                        if (ActiveSettingsClone.CfgBuy_MinPrice != 0)
                            dBuyAmount = Math.Max(dBuyAmount, ActiveSettingsClone.CfgBuy_MinPrice);
                        if (ActiveSettingsClone.CfgBuy_MaxPrice != 0)
                            dBuyAmount = Math.Min(dBuyAmount, ActiveSettingsClone.CfgBuy_MaxPrice);
                    }

                    // Ggf. Betrag von Umrechnungs-Währung (in [$]) in aktuelle Sekundär-Währung umrechnen:
                    if (bTranslationStrategy != null)
                        dBuyAmount = (dBuyAmount / bTranslationStrategy.TradingPair.LastRateValue);

                    // Einkaufs-Preis (in [Sekundär-Währung]) ermitteln:
                    // > Gewichtung berücksichtigen.
                    dBuyAmount = (dBuyAmount * dCalc_Buy_WeightFactor);

                    if (dBuyAmount > WalletSec.Funds)
                        // Vorhandenes Guthaben reicht nicht für Kauf-Order aus:
                        bBuyActivator.Lock.Add(BotOrderActivator.BotLockReason.eInsufficientFunds);
                    else
                    {
                        // Einkaufs-Preis von [Sekundär-Währung] in [Primär-Währung] umrechnen:
                        dBuyAmount = (dBuyAmount / TradingPair.LastRateValue);

                        // Ggf. anzuwendenden Betrag begrenzen (Platform-spezifisches Minimum / Maximum):
                        if ((TradingPair.MinOrderSize != 0) && (TradingPair.MinOrderSize > dBuyAmount))
                        {
                            // Minimaler Betrag unterschritten
                            // > Prüfen ob Guthaben ([Sekundär-Währung]) für Minimal-Betrag (in [Primär-Währung]) ausreicht:
                            if ((TradingPair.MinOrderSize * TradingPair.LastRateValue) > WalletSec.Funds)
                                // Vorhandenes Guthaben reicht nicht für Kauf-Order aus:
                                bBuyActivator.Lock.Add(BotOrderActivator.BotLockReason.eInsufficientFunds);
                            else
                                dBuyAmount = TradingPair.MinOrderSize;
                        }
                        if (TradingPair.MaxOrderSize != 0)
                            dBuyAmount = Math.Min(dBuyAmount, TradingPair.MaxOrderSize);
                    }
                }
            }

            if (bBuyActivator.CanExecuteOrder())
                return (dBuyAmount);
            else
                return (0);
        }
        // Ermittelt den Betrag einer Verkauf-Order.
        private double GetSellAmount()
        {
            double dSellAmount = 0;

            // Initialisieren:
            sSaleActivator.Lock.Remove(BotOrderActivator.BotLockReason.eInsufficientFunds);

            if (WalletPrim.Funds > 0)
            {
                // Zu verkaufenden Betrag (in [Primär-Währung]) ermitteln:
                if (ActiveSettingsClone == null)
                {
                    Debug.Assert((sSaleActivator.TriggerReason == BotOrderActivator.BotTriggerReason.eUser), "[GSA3176]");

                    // Verfügbaren Betrag anwenden:
                    dSellAmount = WalletPrim.Funds;
                }
                else if (sSaleActivator.IsShortSale)
                    // Leerverkauf unter bestimmten Bedingungen:
                    // > Verfügbaren Betrag anwenden!
                    dSellAmount = WalletPrim.Funds;
                else
                    // Verfügbaren Betrag in verbleibende Schritte aufteilen:
                    dSellAmount = (WalletPrim.Funds / Rift_SellInfo.RemainingSteps.Default);

                // Ggf. anzuwendenden Betrag begrenzen (Platform-spezifisches Minimum / Maximum):
                if ((TradingPair.MinOrderSize != 0) && (TradingPair.MinOrderSize > dSellAmount))
                {
                    // Minimaler Betrag unterschritten
                    // > Prüfen ob Guthaben ([Primär-Währung]) für Minimal-Betrag ausreicht:
                    if (TradingPair.MinOrderSize > WalletPrim.Funds)
                        // Vorhandenes Guthaben reicht nicht für Verkauf-Order aus:
                        sSaleActivator.Lock.Add(BotOrderActivator.BotLockReason.eInsufficientFunds);
                    else
                        dSellAmount = TradingPair.MinOrderSize;
                }
                if (TradingPair.MaxOrderSize != 0)
                    dSellAmount = Math.Min(dSellAmount, TradingPair.MaxOrderSize);

                // Betrag zum VERKAUF definieren:
                dSellAmount = (dSellAmount * -1);
            }
            else
                // Guthaben reicht NICHT aus:
                sSaleActivator.Lock.Add(BotOrderActivator.BotLockReason.eInsufficientFunds);

            if (sSaleActivator.CanExecuteOrder())
                return (dSellAmount);
            else
                return (0);
        }
        private void CalcOrderCondition(BotOrderActivator.BotLock oLock, BotOrderCondition oCondition, BotRiftInfo sRiftInfo, double dCurRate)
        {
            bool bConditionSlow = true;
            bool bConditionFast = true;

            // Initialisieren:
            oLock.Reset();

            switch (oCondition)
            {
                case BotOrderCondition.eMinProphit:
                    // Prüfen ob Erzielen eines minimalen Profits möglich ist:
                    bConditionSlow = (sRiftInfo.NextOrderLimit.LimitValue >= Calc_Rift_MinProphitSellRate);
                    bConditionFast = (dCurRate >= Calc_Rift_MinProphitSellRate);
                    break;

                case BotOrderCondition.eMinBuyAverage:
                    // Prüfen ob durchschnittlicher Einkaufskurs verletzt wird:
                    double dBuyAvgRate = RiftSummaries.CurrentRift.Buys.TotalAverageRate;

                    bConditionSlow = (sRiftInfo.NextOrderLimit.LimitValue >= dBuyAvgRate);
                    bConditionFast = (dCurRate >= dBuyAvgRate);
                    break;

                case BotOrderCondition.eNone:
                    // Ggf. Verluste akzeptieren.
                    break;

                default:
                    Debug.Assert(false, "[RR1324] Unbehandelter Typ!");
                    break;
            }

            // Bedingungen prüfen:
            oLock.Conditional(bConditionSlow, BotOrderActivator.BotLockReason.eCondition_Slow);
            oLock.Conditional(bConditionFast, BotOrderActivator.BotLockReason.eCondition_Fast);
        }
        private bool IsSlowLimitEnabled(BotOrderDirection oDirection)
        {
            BotOrderActivator oActivator;

            if (oDirection == BotOrderDirection.eBuy)
            {
                // Prüfen ob Anzahl an Schritten ausreichend ist:
                if (Rift_BuyInfo.RemainingSteps.GetTotalStepsEx(bBuyActivator) == 0)
                    return (false);

                oActivator = (BotOrderActivator)bBuyActivator;
            }
            else
                oActivator = (BotOrderActivator)sSaleActivator;

            foreach (BotReactionIndicator tIndicator in ReactionIndicators)
            {
                if (tIndicator.ReferredParameters.Enabled)
                {
                    // Bewertung prüfen:
                    if ((oDirection == BotOrderDirection.eBuy) ? tIndicator.BuyLimitState.SlowRating : tIndicator.SellLimitState.SlowRating)
                    {
                        // Prüfen ob Aktivator blockiert ist:
                        if (oActivator.IsUnlocked(tIndicator, false))
                            return (true);
                    }
                }
            }

            return (false);
        }
        private String FormatLockedOrderReason(BotOrderActivator.BotLock oLock)
        {
            Type tReason = typeof(BotOrderActivator.BotLockReason);
            Array aValArray = Enum.GetValues(tReason);
            String sResult = "";

            foreach (int iVal in aValArray)
            {
                if ((iVal & oLock.Reasons) == iVal)
                {
                    if (sResult.Length > 0)
                        sResult += ", ";

                    sResult += UtlEnum.GetEnumDescription(tReason.GetField(Enum.GetName(tReason, iVal)));
                }
            }

            return (sResult);
        }
        #endregion


        #region General
        protected BotState sState = BotState.eDisabled;
        protected DateTime dLastRecalc = new DateTime();

        protected BotBuyActivator bBuyActivator = null;
        protected BotSellActivator sSaleActivator = null;

        private BotTradingStrategy bTranslationStrategy = null;                                                   // Strategie-Objekt für Umrechnung in Referenz-Währung (= [TRADE_BASE_CURRENCY]).
        private BotTradesExcelDoc tTradesDoc = null;
        private BotCoinStreamRecorder cCoinStreamRecorder = null;
        #endregion
    }
}
