﻿using CryptoRiftBot;
using CryptoRiftBot.Trading.Functions;
using CryptoRiftBot.Trading.Strategy;
using org.mariuszgromada.math.mxparser;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Shared.Utils.Core;

namespace CryptoRiftBot
{
    public class BotFormula
    {
        public class BotVariable : DscrMetaData
        {
            public BotVariable(String sName, String Description) : base(sName, Description)
            {
                Argument = new Argument(sName, 0);
            }


            #region Properties.Management
            [Browsable(false)]
            public Argument Argument { private set; get; }
            [Browsable(false)]
            public BotVariables Parent { internal set; get; }
            [Browsable(false)]
            public bool IsValid { get { return (Parent != null); } }
            [Browsable(false)]
            public TimeSpan UpdateTimeStamp { get { return ((IsValid) ? Parent.UpdateTimeStamp : default(TimeSpan)); } }
            #endregion
            #region Properties
            [Browsable(false)]
            public double Value
            {
                get { return (Argument.getArgumentValue()); }
                set { Argument.setArgumentValue(value); }
            }
            #endregion


            #region Management
            public override string ToString()
            {
                return (Value.ToString());
            }
            #endregion
        }

        public class BotVariables
        {
            #region Constants
            public const String I_DEFAULT_NAME = "Price";
            #endregion


            #region Types
            public class BotVariableName : Dictionary<String, BotFunctionID> { }

            // Definiert eine Gruppe von Variablen.
            [TypeConverter(typeof(CnvEmptyExpandableObject))]
            public class BotBaseGroup
            {
                #region Constants
                // Syntax:
                public const String SYNTAX_SEPARATOR = "_";
                #endregion


                #region Properties.Management
                [Browsable(false)]
                public Dictionary<int, BotVariable> Variables { get; } = new Dictionary<int, BotVariable>();
                [Browsable(false)]
                public int IndexCount { get { return (Variables.Last().Key + 1); } }
                [Browsable(false)]
                public BotFunctionID Scope { private set; get; }
                #endregion
                #region Properties.Display
                [DisplayName("Variablen")]
                [TypeConverter(typeof(CnvEmptyExpandableList))]
                public List<BotVariable> VariableList { get { return (Variables.Values.ToList()); } }
                #endregion


                public BotBaseGroup(BotFunctionID fScope)
                {
                    Scope = fScope;
                }


                #region Initialization
                public void Init(BotVariables vParent, Expression eExpression)
                {
                    foreach (int iKey in Variables.Keys)
                    {
                        Variables[iKey].Parent = vParent;
                        eExpression.addArguments(Variables[iKey].Argument);
                    }
                }
                #endregion
                #region Management
                public void Reset()
                {
                    foreach (int iKey in Variables.Keys)
                        Variables[iKey].Value = 0;
                }
                #endregion
            }
            public class BotTicker : BotBaseGroup
            {
                #region Constants
                // Indices von definierten Konstanten:
                public const int I_RATE = 0;
                public const int I_CHANGE_1H = 1;
                public const int I_CHANGE_24H = 2;
                public const int I_CHANGE_7D = 3;
                public const int I_BUYAVERAGERATE = 4;
                public const int I_AVERAGEDOWNLIMIT = 5;
                public const int I_NEXTBUYLIMIT = 6;
                public const int I_NEXTSELLLIMIT = 7;
                public const int I_PANICSELLLIMIT = 8;
                public const int I_MINPROPHITSELLRATE = 9;
                #endregion


                public BotTicker() : base(new BotFunctionID(BotFunctionID.BotFunctionType.eTicker))
                {
                    Variables[I_RATE] = new BotVariable(I_DEFAULT_NAME, "Aktueller Kurs.");
                    Variables[I_CHANGE_1H] = new BotVariable("Change1h", "Kursänderung der letzten Stunde (in [%]).");
                    Variables[I_CHANGE_24H] = new BotVariable("Change24h", "Kursänderung der letzten 24 Stunden (in [%]).");
                    Variables[I_CHANGE_7D] = new BotVariable("Change7d", "Kursänderung der letzten 7 Tage (in [%]).");
                    Variables[I_BUYAVERAGERATE] = new BotVariable("BuyAvgPrice", "Durchschnittlicher Einkaufs-Kurs");
                    Variables[I_AVERAGEDOWNLIMIT] = new BotVariable("AvgDownLimit", "Grenzwert für Nachkaufen");
                    Variables[I_NEXTBUYLIMIT] = new BotVariable("NextBuyLimit", "Grenzwert für Platzierung der nächsten Kauf-Order.");
                    Variables[I_NEXTSELLLIMIT] = new BotVariable("NextSellLimit", "Grenzwert für Platzierung der nächsten Verkauf-Order.");
                    Variables[I_PANICSELLLIMIT] = new BotVariable("PanicSellLimit", "Grenzwert für Panik-Leerverkauf.");
                    Variables[I_MINPROPHITSELLRATE] = new BotVariable("MinProphitSellPrice", "Grenzwert für minimalen Profit.");
                }
            }
            public class BotLastOrders : BotBaseGroup
            {
                #region Constants
                // Indices von definierten Konstanten:
                public const int I_BUY_RATE = 0;
                public const int I_BUY_PASTTIME = 1;
                public const int I_SELL_RATE = 2;
                public const int I_SELL_PASTTIME = 3;
                #endregion


                public BotLastOrders() : base(new BotFunctionID(BotFunctionID.BotFunctionType.eLastOrders))
                {
                    Variables[I_BUY_RATE] = new BotVariable("LastBuyRate", "Kurs zur Ausführung der letzten Kauf-Order.");
                    Variables[I_BUY_PASTTIME] = new BotVariable("LastBuyDst", "Abstand zur Ausführung der letzten Kauf-Order (in [min]).");
                    Variables[I_SELL_RATE] = new BotVariable("LastSellRate", "Kurs zur Ausführung der letzten Sell-Order.");
                    Variables[I_SELL_PASTTIME] = new BotVariable("LastSellDst", "Abstand zur Ausführung der letzten Sell-Order (in [min]).");
                }
            }
            public class BotRift : BotBaseGroup
            {
                #region Constants
                // Indices von definierten Konstanten:
                public const int I_ACTIVE = 0;
                public const int I_PROPHIT = 1;
                #endregion


                public BotRift() : base(new BotFunctionID(BotFunctionID.BotFunctionType.eRift))
                {
                    Variables[I_ACTIVE] = new BotVariable("RiftActive", "Ein Rift ist momentan aktiv.");
                    Variables[I_PROPHIT] = new BotVariable("Prophit", "Aktueller Profit.");
                }
            }
            public class BotCandle : BotBaseGroup
            {
                #region Constants
                // Indices von definierten Konstanten:
                public const int I_OPEN = 0;
                public const int I_HIGH = 1;
                public const int I_LOW = 2;
                public const int I_CLOSE = 3;
                #endregion


                public BotCandle() : base(new BotFunctionID(BotFunctionID.BotFunctionType.eCandle))
                {
                    String sPrefix = String.Format("Candle{0}", SYNTAX_SEPARATOR);

                    Variables[I_OPEN] = new BotVariable(sPrefix + "Open", "Erster Wert des aktuellen Candle-Sticks.");
                    Variables[I_HIGH] = new BotVariable(sPrefix + "High", "Höchster Wert des aktuellen Candle-Sticks.");
                    Variables[I_LOW] = new BotVariable(sPrefix + "Low", "Niedrigster Wert des aktuellen Candle-Sticks.");
                    Variables[I_CLOSE] = new BotVariable(sPrefix + "Close", "Letzter Wert des aktuellen Candle-Sticks.");
                }
            }
            public class BotBaseFunction : BotBaseGroup
            {
                public BotBaseFunction(String sPrefix, BotFunctionID fScope = null) : base(fScope)
                {
                    this.sPrefix = sPrefix;
                }


                protected String sPrefix;
            }
            public class BotBaseIndicator : BotBaseFunction
            {
                #region Constants
                // Indices von definierten Konstanten:
                public const int I_AVERAGE1 = 0;
                public const int I_AVERAGE2 = 1;
                public const int I_VARIATION = 2;
                public const int I_TENDENCY = 3;
                #endregion


                public BotBaseIndicator(String sPrefix, BotFunctionID fScope = null) : base(sPrefix, fScope)
                {
                    Variables[I_AVERAGE1] = new BotVariable(sPrefix + "Avg1", "Aktueller Durchschnittswert 1.");
                    Variables[I_AVERAGE2] = new BotVariable(sPrefix + "Avg2", "Aktueller Durchschnittswert 2.");
                    Variables[I_VARIATION] = new BotVariable(sPrefix + "Var", "Aktuelle Änderung.");
                    Variables[I_TENDENCY] = new BotVariable(sPrefix + "Tendency", "Tendenz der aktuellen Änderung.");
                }
            }
            public class BotTrendIndicator : BotBaseIndicator
            {
                #region Constants
                // Indices von definierten Konstanten:
                public const int I_BUY_ORDER_STATE = 100;
                public const int I_SELL_ORDER_STATE = 101;
                #endregion


                public BotTrendIndicator() : base(String.Format("tInd{0}", SYNTAX_SEPARATOR), new BotFunctionID(BotFunctionID.BotFunctionType.eTrendIndicator))
                {
                    Variables[I_BUY_ORDER_STATE] = new BotVariable(String.Format("{0}Buy{1}State", sPrefix, SYNTAX_SEPARATOR), "Freigabe von Kauf-Orders.");
                    Variables[I_SELL_ORDER_STATE] = new BotVariable(String.Format("{0}Sell{1}State", sPrefix, SYNTAX_SEPARATOR), "Freigabe von Verkauf-Orders.");
                }
            }
            public class BotReactionIndicator : BotBaseIndicator
            {
                #region Constants
                // Indices von definierten Konstanten:
                public const int I_BUYLIMIT_SLOW_RATING_STATE = 100;
                public const int I_BUYLIMIT_FAST_ENABLED_STATE = 101;
                public const int I_SELLLIMIT_SLOW_RATING_STATE = 102;
                public const int I_SELLLIMIT_FAST_ENABLED_STATE = 103;
                public const int I_DOUBLECHECK_STATE = 104;
                public const int I_LOW_PRICE = 105;
                public const int I_HIGH_PRICE = 106;
                public const int I_LOW_VARIATION = 107;
                public const int I_HIGH_VARIATION = 108;
                #endregion


                public BotReactionIndicator(int iIndicatorNum) : base(String.Format("rInd{0}{1}", iIndicatorNum, SYNTAX_SEPARATOR), new BotFunctionID(BotFunctionID.BotFunctionType.eReactionIndicator, iIndicatorNum))
                {
                    Variables[I_BUYLIMIT_SLOW_RATING_STATE] = new BotVariable(String.Format("{0}Buy{1}SlowState", sPrefix, SYNTAX_SEPARATOR), "Bewertung 'Candle-Sticks' (Kauf-Orders).");
                    Variables[I_BUYLIMIT_FAST_ENABLED_STATE] = new BotVariable(String.Format("{0}Buy{1}FastState", sPrefix, SYNTAX_SEPARATOR), "Freigabe 'Ticker' (Kauf-Orders).");
                    Variables[I_SELLLIMIT_SLOW_RATING_STATE] = new BotVariable(String.Format("{0}Sell{1}SlowState", sPrefix, SYNTAX_SEPARATOR), "Bewertung 'Candle-Sticks' (Verkauf-Orders).");
                    Variables[I_SELLLIMIT_FAST_ENABLED_STATE] = new BotVariable(String.Format("{0}Sell{1}FastState", sPrefix, SYNTAX_SEPARATOR), "Freigabe 'Ticker' (Verkauf-Orders).");
                    Variables[I_DOUBLECHECK_STATE] = new BotVariable(sPrefix + "Confirmed", "Aussagekraft der aktuellen Änderung durch Tendenz bestätigt.");
                    Variables[I_LOW_PRICE] = new BotVariable(sPrefix + "LowPrice", "Niedrigster aus allen Kurswerten, welche zur Berechnung des Indikators verwendet werden.");
                    Variables[I_HIGH_PRICE] = new BotVariable(sPrefix + "HighPrice", "Höchster aus allen Kurswerten, welche zur Berechnung des Indikators verwendet werden.");
                    Variables[I_LOW_VARIATION] = new BotVariable(sPrefix + "LowVarPerc", "Kleinste Änderung (in [%]) aus allen Änderungs-Werten, welche während der Perioden-Dauer des Indikators ermittelt wurden.");
                    Variables[I_HIGH_VARIATION] = new BotVariable(sPrefix + "HighVarPerc", "Größte Änderung (in [%]) aus allen Änderungs-Werten, welche während der Perioden-Dauer des Indikators ermittelt wurden.");
                }
            }
            public class BotTrigger : BotBaseFunction
            {
                #region Constants
                // Indices von definierten Konstanten:
                public const int I_ENABLED = 0;
                #endregion


                public BotTrigger(int iTriggerNum) : base(String.Format("Trg{0}{1}", iTriggerNum, SYNTAX_SEPARATOR), new BotFunctionID(BotFunctionID.BotFunctionType.eTrigger, iTriggerNum))
                {
                    Variables[I_ENABLED] = new BotVariable(String.Format("{0}Enabled", sPrefix), "Freigabe.");
                }
            }
            #endregion


            public BotVariables()
            {
                Ticker = new BotTicker();
                LastOrders = new BotLastOrders();
                Rift = new BotRift();
                Candle = new BotCandle();
                TrendIndicator = new BotTrendIndicator();
                ReactionIndicator = new BotReactionIndicator[BotTradingStrategy.IND_ARRAY_COUNT];
                Trigger = new BotTrigger[BotTradingStrategy.TRIG_ARRAY_COUNT];

                for (int i = 0; i < ReactionIndicator.Length; i++)
                    ReactionIndicator[i] = new BotReactionIndicator(i + 1);

                for (int i = 0; i < Trigger.Length; i++)
                    Trigger[i] = new BotTrigger(i + 1);

                // INFO:
                // > Expressions müssen via 'Calculation::Calculation()' initialisiert werden!

                // Ggf Sämtliche Variablen-Namen bekannt machen:
                ListVariableNames();
            }


            #region Properties.Management
            [Browsable(false)]
            public static BotVariableName VariableNames { private set; get; }
            #endregion
            #region Properties.Management
            [Browsable(false)]
            public TimeSpan UpdateTimeStamp;
            #endregion
            #region Properties
            [Category(BotConvention.PROP_CATEGORY_GENERAL)]
            [DisplayName("Ticker")]
            public BotTicker Ticker { private set; get; }
            [Category(BotConvention.PROP_CATEGORY_GENERAL)]
            [DisplayName("Letzt Orders")]
            public BotLastOrders LastOrders { private set; get; }
            [Category(BotConvention.PROP_CATEGORY_GENERAL)]
            [DisplayName("Rift")]
            [Description("Variablen zu aktuellem Rift.")]
            public BotRift Rift { private set; get; }
            [Category(BotConvention.PROP_CATEGORY_GENERAL)]
            [DisplayName("Candle-Sticks")]
            public BotCandle Candle { private set; get; }
            [Category(BotConvention.PROP_CATEGORY_RATING)]
            [DisplayName(BotConvention.PROP_NAME_TREND_INDICATOR)]
            public BotTrendIndicator TrendIndicator { private set; get; }
            [Category(BotConvention.PROP_CATEGORY_REACTION)]
            [DisplayName("Reaktions-Indikatoren")]
            public BotReactionIndicator[] ReactionIndicator { private set; get; }
            [Category(BotConvention.PROP_CATEGORY_REACTION)]
            [DisplayName("Trigger")]
            public BotTrigger[] Trigger { private set; get; }
            #endregion


            #region Management
            public void Reset()
            {
                ForeachGroup(bGroup => bGroup.Reset());
            }

            // Führt eine Anweisung für sämtlichen Variablen durch.
            public void ForeachGroup(Action<BotBaseGroup> aPredicate)
            {
                aPredicate(Ticker);
                aPredicate(LastOrders);
                aPredicate(Rift);
                aPredicate(Candle);
                aPredicate(TrendIndicator);

                for (int i = 0; i < ReactionIndicator.Length; i++)
                    aPredicate(ReactionIndicator[i]);

                for (int i = 0; i < Trigger.Length; i++)
                    aPredicate(Trigger[i]);
            }
            public BotVariable Find(String sName)
            {
                // (BETA) ... TODO
                // Hier werden immer alle Gruppen durchlaufen (Siehe 'ForeachGroup()', wo jedes Prädikat auf alle Gruppen angewendet wird).
                // > Hier beenden können, sobald Ergebnis gefunden wird!

                BotVariable _bVar = null;
                ForeachGroup(oGroup =>
                {
                    foreach (var vEntry in oGroup.Variables)
                    {
                        if (vEntry.Value.DisplayName == sName)
                        {
                            _bVar = vEntry.Value;
                            return;
                        }
                    }
                });
                return (_bVar);
            }


            private void ListVariableNames()
            {
                if (VariableNames == null)
                {
                    VariableNames = new BotVariableName();
                    ForeachGroup(bGroup =>
                    {
                        foreach (int _iKey in bGroup.Variables.Keys)
                            VariableNames.Add(bGroup.Variables[_iKey].DisplayName, bGroup.Scope);
                    });
                }
            }
            #endregion
        }

        public class BotCalculation
        {
            #region Enumerations
            public enum BotResult
            {
                [Description("Syntax-Fehler")]
                eSyntaxError,

                [Description("Verfehlt")]
                eFaild,
                [Description("Ignoriert")]
                eIgnored,
                [Description("Zutreffend")]
                eSuccess
            }
            #endregion


            public BotCalculation(BotVariables vRefVariables)
            {
                this.vRefVariables = vRefVariables;
                this.eExpression = new Expression("");
                this.LastResult = BotResult.eFaild;

                // Gruppen initialisieren:
                vRefVariables.ForeachGroup(bGroup => bGroup.Init(vRefVariables, eExpression));
            }


            #region Properties.Management
            [Browsable(false)]
            public bool IsDefined { get { return (Expression != string.Empty); } }
            [Browsable(false)]
            public String Expression
            {
                get { return (eExpression.getExpressionString()); }
                set
                {
                    if ((value != null) && (value.Length > 0))
                        eExpression.setExpressionString(value.Replace(',', '.'));
                    else
                        eExpression.clearDescription();
                }
            }
            [Browsable(false)]
            public BotResult LastResult { private set; get; }
            #endregion


            #region Management
            public BotResult Check()
            {
                try
                {
                    if (IsDefined == false)
                        LastResult = BotResult.eFaild;
                    else
                    {
                        double dCalcResult = eExpression.calculate();
                        if (Double.IsNaN(dCalcResult))
                            LastResult = BotResult.eSyntaxError;
                        else
                            LastResult = (dCalcResult > 0) ? BotResult.eSuccess : BotResult.eFaild;
                    }
                }
                catch (System.IO.IOException)
                {
                    LastResult = BotResult.eFaild;
                }
                return (LastResult);
            }
            #endregion


            private BotVariables vRefVariables = null;
            private Expression eExpression = null;
        }
    }
}