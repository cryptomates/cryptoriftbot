﻿using CryptoRiftBot;
using CryptoRiftBot.Config;
using CryptoRiftBot.Trading.Functions;
using CryptoRiftBot.Trading.Strategy;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using Shared.Utils.Core;
using Shared.Net;
using WebSocketSharp;

namespace CryptoRiftBot.Trading.Services
{
    #region Attributes.Library
    [TypeConverter(typeof(CnvEmptyExpandableObject))]
    public class BotServiceDescriptorAttribute : Attribute
    {
        public BotServiceDescriptorAttribute(string sServiceDomain, string sServiceName, string[] sDocumentation = null)
        {
            this.ServiceDomain = sServiceDomain;
            this.ServiceName = sServiceName;
            if (sDocumentation != null)
                this.sDocumentation.AddRange(sDocumentation);
        }


        #region Properties
        [DisplayName("Domain")]
        public string ServiceDomain { private set; get; }
        [DisplayName("Name")]
        public string ServiceName { private set; get; }

        [Browsable(false)]
        public IReadOnlyList<string> ServiceDocumentation { private set; get; }
        #endregion


        private List<string> sDocumentation = new List<string>();
    }
    #endregion


    #region Enumerations.Non-Platform
    public enum BotServiceState
    {
        eError,
        eUnknown,
        eOffline,
        eOnline
    };
    public enum BotSubscriptionType
    {
        Invalid,

        eTicker,
        eTrade,
        eBook,
        eRawBook,
        eCandles
    };
    public enum BotSubscriptionPurpose
    {
        eRegular,
        eCoinStream
    };
    public enum BotOrderType
    {
        [Description("")]
        Invalid = -1,

        [Description("LIMIT")]
        eLimit,
        [Description("MARKET")]
        eMarket,
        [Description("STOP")]
        eStop,
        [Description("TRAILING STOP")]
        eTrailingStop,
        [Description("EXCHANGE MARKET")]
        eExchangeMarket,
        [Description("EXCHANGE LIMIT")]
        eExchangeLimit,
        [Description("EXCHANGE STOP")]
        eExchangeStop,
        [Description("EXCHANGE TRAILING STOP")]
        eExchangeTrailingStop,
        [Description("FOK")]
        eFillOrKill,
        [Description("EXCHANGE FOK")]
        eExchangeFillOrKill
    };
    public enum BotOrderDirection
    {
        [Description("Kauf")]
        eBuy,
        [Description("Verkauf")]
        eSell
    };
    public enum BotOrderState
    {
        [Description("")]
        Canceling = -2,
        [Description("")]
        Invalid = -1,
        [Description("")]
        ExInitialized = 0,                           // Order ist von Trade-Bot initialisiert, aber noch nicht von Platform behandelt worden.

        [Description("ACTIVE")]
        eActive,
        [Description("EXECUTED")]
        eExecuted,
        [Description("PARTIALLY FILLED")]
        ePartiallyFilled,
        [Description("CANCELED")]
        eCanceled
    };
    public enum BotOrderResult
    {
        [Description("Erstellt")]
        eCreated,                                     // Order wurde erstellt

        [Description("Abgrebrochen")]
        eCanceled,                                    // Order wurde abgebrochen
        [Description("Teilweise ausgeführt")]
        ePartiallyExecuted,                           // Order wurde teilweise ausgeführt
        [Description("Ausgeführt")]
        eExecuted                                     // Order wurde vollständig ausgeführt
    };
    public enum BotTickerBehaviour  // (BETA) ... [Services][Behaviour] Kommt irgendwann weg
    {
        [Description("Standard")]
        eRandomDefault,                               // Zufällig, Ungefiltert
        [Description("Manuell")]
        eManual,                                      // Von Benutzer definierter Kurswert
        [Description("Geglättet")]
        eRandomSmoothed,                              // Zufällig, geglättet
        [Description("Folgend")]
        eRandomFollowing,                             // Zufälligen, aufeinanderfolgenden Punkten folgend
        [Description("Realistisch")]
        eRealistic,                                   // Simulation eines realistischen Kurses
        [Description("Coin-Stream")]
        eCoinStream,                                  // CoinStream-Aufnahme

        [Description("Bitfinex")]
        ePlatform_Bitfinex                            // Aktueller Kurs von Bitfinex
    }
    // public const BotTickerBehaviour BotTickerBehaviour_IsExchange = BotTickerBehaviour.ePlatform_Bitfinex;
    #endregion


    #region Exceptions
    /// <summary>
    /// Thrown if received invalid data of a web request.
    /// </summary>
    public class BotInvalidRequestDataException : Exception { }
    #endregion


    #region Interfaces.Trading
    public interface IBotExchangeRate
    {
        #region Properties
        public double DollarEuro { get; }
        #endregion


        #region Management
        public void Update(double dDollarEuro);
        #endregion
    }
    public interface IBotCoinInfo
    {
        #region Properties
        public String Name { get; }
        public String URL { get; }
        public double MarketCap { get; }
        public double Price { get; }
        public double[] PercentChange { get; }
        #endregion


        #region Management
        public void Update(String sName = null, String sURL = null, double dPrice = double.NaN, double dMarketCap = double.NaN, double[] dPercentChange = null);
        #endregion
    }

    public interface IBotCoinInfoList
    {
        #region Properties.Management
        public int Count { get; }
        #endregion


        #region Management
        public void SortRating();
        public IBotCoinInfo Find(BotCoinType cType);
        public BotCoinType GetTypeAt(int iIndex);
        #endregion
    }
    #endregion
    #region Interfaces.Services.Base
    public interface IBotServiceProvider
    {
        #region Properties.Management
        IReadOnlyList<IBotBaseService> Services { get; }
        #endregion
        #region Properties.Library
        BotServiceDescriptorAttribute ServiceDescriptor { get; }
        Version Version { get; }
        #endregion
    }
    public interface IBotBaseService
    {
        #region Properties.Library
        Type ServiceType { get; }
        IBotServiceProvider Provider { get; }
        BotServiceDescriptorAttribute ServiceDescriptor { get; }
        Version Version { get; }
        #endregion
        #region Properties.Management
        bool IsValid { get; }
        bool IsActive { get; }
        #endregion
        #region Properties
        string Name { get; }
        BotServiceState State { get; }
        #endregion


        #region Initialization
        bool ActivateService(BotServiceOwner bOwner = null);
        bool DeactivateService();
        #endregion
    }

    /// <summary>
    /// Service can receive ticker data.
    /// </summary>
    public interface IBotTickerService : IBotBaseService
    {
        public Task<bool> GetTickerAsync(IBotCoinInfoList cResult);
    }
    /// <summary>
    /// Service can receive candle data.
    /// </summary>
    public interface IBotCandleService : IBotBaseService
    {
        public Task<bool> GetCandlesAsync(BotCandleSubscription cResult, int iLimit = 100);
    }
    /// <summary>
    /// Service can receive ticker data.
    /// </summary>
    public interface IBotExchangeRateService : IBotBaseService
    {
        public Task<bool> GetExchangeRateAsync(IBotExchangeRate eRate);
    }
    /// <summary>
    /// Service represents an exchange platform.
    /// </summary>
    public interface IBotPlatformService : IBotBaseService
    {
        #region Management
        BotWallet GetWallet(BotCoinType cCurrency, BotWalletType wType);
        #endregion
    }
    /// <summary>
    /// Service represents a simulation of exchange platforms.
    /// </summary>
    public interface IBotSimulatedPlatformService : IBotPlatformService
    {
        #region Properties.CoinStream
        BotCoinStreamPlayer CoinStreamPlayer { get; }
        #endregion
        #region Properties.Config
        BotTickerBehaviour Behaviour { get; }
        #endregion


        #region Management
        void ResetWallet();
        #endregion
        #region Management.CoinStream
        bool StopCoinStreamPlay();
        #endregion
        #region Communication.Commands
        bool SetManualTickerSimValue(BotTradingPair tPair, double dValue);
        #endregion
    }
    #endregion


    #region Types.Management
    #endregion
    #region Types.System
    public abstract class BotServiceOwner
    {
    }
    /// <summary>
    /// Interface für Kommunikation mit Trading-Bot.
    /// </summary>
    public abstract class BotPlatformServiceOwner : BotServiceOwner
    {
        #region Management
        public abstract void OnSubscribed(BotBaseSubscription bSubscription);
        public abstract BotTaskResult OnSubscriptionData(BotSubscriptionPurpose sPurpose, BotBaseSubscription bSubscription);

        public abstract void OnOrderCreated(BotBaseOrder oOrder);
        public abstract void OnOrderClosed(BotBaseOrder oOrder, BotOrderResult oResult);

        public abstract void OnWalletUpdated(BotWallet wWallet);
        public abstract void OnMaintenance(bool bEnabled);

        public abstract BotTaskResult OnCoinStreamEvent(BotCoinStreamPlayer.BotEventType eType, BotBasePairSubscription bSubscription);
        #endregion


        #region Communication
        public abstract void OnSocketConnected();
        public abstract void OnSocketClosed();
        public abstract void OnSocketDisconnected();
        public abstract void OnLoggedIn();
        public abstract void OnError(String sCode, String sMessage);
        #endregion
    }
    #endregion
    #region Types.General.Base
    public class BotSubscriptionWebRequest : WebRequestEx
    {
        #region Constants
        public static readonly TimeSpan REQUEST_INTERVAL = new TimeSpan(0, 0, 15);
        #endregion


        public BotSubscriptionWebRequest(BotSubscriptionType sType, String sFormatURI) : base("", REQUEST_INTERVAL)
        {
            this.sFormatURI = sFormatURI;
            this.Type = sType;

            Debug.Assert((sFormatURI.Length > 0), "[PWR272] URI-Format muss definiert sein!");
        }


        #region Properties.Config
        [Browsable(false)]
        public BotSubscriptionType Type { private set; get; }
        #endregion


        #region Management
        public void Init(List<BotTradingPair> lUsingPairs)
        {
            String sSymbols = "";

            Debug.Assert((lUsingPairs != null), "[I282]");

            if (sFormatURI.Length > 0)
            {
                // Aktivierte Pairs auflisten:
                foreach (BotTradingPair tPairTmp in lUsingPairs)
                {
                    if (sSymbols.Length > 0)
                        sSymbols = (sSymbols + ",");

                    sSymbols = (String.Format("{0}t{1}", sSymbols, tPairTmp.Symbol));
                }
            }

            URI = (sSymbols.Length == 0) ? "" : (String.Format(sFormatURI, sSymbols));
        }
        #endregion


        private String sFormatURI = "";
    }

    // Anmeldeinformationen.
    public class BotBaseLoginInfo
    {
        public bool bLoggedIn;


        #region Management
        public virtual void Reset()
        {
            bLoggedIn = false;
        }
        #endregion
    }
    public abstract class BotBaseOrder
    {
        public double dRate = 0;                            // Kurs
        public double dAmount = 0;                          // Betrag (Anzahl an Coins)
        public double dExecutedAmountCum = 0;               // Ausgeführter, (pro Trade) kumulierter Betrag (Falls eine Order in mehreren Teilen ausgeführt wurde)
        public int iExecutedTrades = 0;                     // Anzahl an ausgeführten Trades (Falls eine Order in mehreren Teilen ausgeführt wurde)

        public double dLimitRate = 0;                       // Kurs-Limit (Nicht für Market-Orders)

        public ulong ulClientOrderID = 0;                   // Benutzerdefinierte ID (Zur Identifikation von Orders für den anwendenden Client)

        public BotOrderType oType = BotOrderType.Invalid;
        public BotOrderState oState = BotOrderState.ExInitialized;
        public BotTradingPair tRefPair = null;


        #region Properties.Management
        [Browsable(false)]
        public bool IsBuyOrder { get { return (dAmount > 0); } }
        [Browsable(false)]
        public bool IsSellOrder { get { return (dAmount < 0); } }
        [Browsable(false)]
        public BotOrderDirection Direction { get { return (dAmount < 0) ? BotOrderDirection.eSell : BotOrderDirection.eBuy; } }
        #endregion
        #region Properties
        [DisplayName("Erstellungs-Zeitpunkt")]
        [Description("Zeitpunkt an den die Order erstellt wurde.")]
        public DateTime CreationTime { protected set; get; }
        [DisplayName("Gebühr")]
        [Description("Angefallene Gebühr für Ausführung der Order.")]
        public double Fee
        {
            // Transaktionsgebühr (NICHT in 'Price' enthalten!)
            protected set; get;
        }
        [DisplayName("Preis")]
        [Description("Preis der sich aus Betrag und Kurs ergibt.")]
        public double Price { get { return (dRate * dAmount * -1); } }
        [DisplayName("Entgültiger Betrag")]
        [Description("Betrag inkl. einberechneter Gebühr.")]
        public double FinalAmount
        {
            // Gebühr von Betrag abziehen wenn Order eine Kauf-Order ist:
            get { return (IsBuyOrder ? (dAmount - Fee) : dAmount); }
        }
        [DisplayName("Entgültiger Preis")]
        [Description("Preis inkl. einberechneter Gebühr.")]
        public double FinalPrice
        {
            // Gebühr von Preis abziehen wenn Order eine Verkauf-Order ist:
            get { return (IsSellOrder ? (Price - Fee) : Price); }
        }
        [DisplayName("Gegenwert")]
        [Description("Entsprechender Gegenwert (in [Euro]).")]
        public double ExchangePrice { get { return (FinalPrice * dExchangeRateEuro); } }
        #endregion


        #region Management
        public bool IsValid()
        {
            if ((oState != BotOrderState.Invalid) && (tRefPair != null))
            {
                // Werte auf Gültigkeit prüfen:
                switch (oType)
                {
                    case BotOrderType.eExchangeMarket:
                        return (dAmount != 0);

                    case BotOrderType.eExchangeLimit:
                        return ((dAmount != 0) && (dLimitRate != 0));

                    default:
                        // (BETA) ...
                        // Noch nicht implementiert!
                        break;
                }
            }

            return (false);
        }
        public bool AddFee(double dFee, BotCoinType cType)
        {
            bool bResult;

            Debug.Assert((dAmount != 0), "[SF505]");
            Debug.Assert((tRefPair != null), "[SF506]");

            // Prüfen ob Gebühr in zu erwartender Währung angegeben ist:
            if (IsBuyOrder)
                bResult = (cType == tRefPair.CurrencyPrim);
            else
                bResult = (cType == tRefPair.CurrencySec);

            if (bResult)
                // Gebühr (ohne Vorzeichen) übernehmen:
                this.Fee += Math.Abs(dFee);

            return (bResult);
        }
        #endregion
        #region Management
        public void WriteLog(BotOrderResult oResult, BotOrderActivator oActivator = null)
        {
            String sAction = UtlEnum.GetEnumDescription(oResult);
            if (oActivator != null)
                sAction += String.Format(" ({0})", UtlEnum.GetEnumDescription(oActivator.OrderDirection));

            String sMessage = String.Format("{0} {1}: Betrag={2} @ {3}, Preis={4}", sAction, tRefPair.Symbol, FinalAmount, dRate, FinalPrice);

            if (iExecutedTrades > 0)
                sMessage += String.Format(" (Trades: {0})", iExecutedTrades);

            if (oActivator != null)
            {
                String sReason;

                if (oActivator.TriggerFunction == null)
                    sReason = oActivator.Description;
                else
                    sReason = oActivator.TriggerFunction.SummaryName;

                sMessage += String.Format(", {0}", sReason);
            }

            if (oResult == BotOrderResult.eCreated)
                BotEnv.LogSystem.WriteLogVolatile("WL693", BotLogProcess.eOrder, sMessage, this, BotLogType.ePlatformTrading, BotLogPriority.eLow);
            else
            {
                // Log-Eintrag "Created" zu aktueller Order entfernen:
                BotEnv.LogSystem.RemoveLogEntry(this);

                BotEnv.LogSystem.WriteLog("WL695", BotLogProcess.eOrder, sMessage, BotLogType.ePlatformTrading);
            }
        }


        protected BotBaseOrder()
        {
            CreationTime = DateTime.Now;

            if (BotCurrencyRating.ExchangeRate.IsValid)
                dExchangeRateEuro = BotCurrencyRating.ExchangeRate.DollarEuro;
            else
            {
                dExchangeRateEuro = 1;

                // An dieser Stelle MUSS der Wechselkurs eigentlich ermittelt worden sein!?
                Debug.Assert(false, "[BO644] Ungültiger Wechselkurs!");
            }
        }
        #endregion


        protected double dExchangeRateEuro = 0;
    }
    #endregion
    #region Types.Subscriptions.Base
    // Basis-Klasse für Abonnements.
    public abstract class BotBaseSubscription
    {
        public class BotBasicSubscriptionData
        {
            public BotBasicSubscriptionData()
            {
                dReceivedTime = DateTime.Now;
            }


            #region Properties.Management
            [Browsable(false)]
            public virtual double CurrentValue
            {
                get
                {
                    Debug.Assert(false, "[CV617] Implementation via Sub-Class!");
                    return (0);
                }
            }
            [Browsable(false)]
            public virtual TimeSpan TimeStamp { get { return (UtlDateTime.DateTimeToUTCTimeStamp(dReceivedTime)); } }
            #endregion


            #region Management
            public virtual void Write(BotSubscriptionPurpose sPurpose, BinaryWriter bWriter)
            {
                bWriter.Write(dReceivedTime.Ticks);
            }
            public virtual void Read(BotSubscriptionPurpose sPurpose, BinaryReader bReader)
            {
                dReceivedTime = new DateTime(bReader.ReadInt64());
            }
            #endregion


            public DateTime dReceivedTime;
        }


        protected BotBaseSubscription(BotSubscriptionType sType, int iID)
        {
            this.iID = iID;
            this.sType = sType;
            this.oThreadLockObj = new object();
        }
        ~BotBaseSubscription()
        {
            oThreadLockObj = null;
        }


        public object oThreadLockObj;


        #region Properties.Management
        [Browsable(false)]
        public virtual bool IsReady
        {
            get
            {
                Debug.Assert(false, "[IR708] Implementation via Sub-Class!");
                return (false);
            }
        }
        #endregion


        #region Management
        public int GetSubscriptionID()
        {
            return (iID);
        }
        public BotSubscriptionType GetSubscriptionType()
        {
            return (sType);
        }
        #endregion
        #region Management
        public virtual String GetName()
        {
            Debug.Assert(false, "[GN185] Implementation via SubClass!");

            return ("");
        }
        #endregion


        private int iID;                                                        // Von Trading-Platform zugewiesene ID der Subscription (z.B. Channel-ID auf Bitfinex)
        private BotSubscriptionType sType;
    }
    public abstract class BotBasePairSubscription : BotBaseSubscription
    {
        public BotBasePairSubscription(BotSubscriptionType sType, int iID, BotTradingPair tRefPair) : base(sType, iID)
        {
            this.tRefPair = tRefPair;
        }


        #region Properties
        [Browsable(false)]
        public String Name { get { return (tRefPair.Symbol); } }
        [Browsable(false)]
        public BotTradingPair ReferredPair { get { return (tRefPair); } }
        #endregion


        #region Management
        public virtual void AddData(BotBaseSubscription.BotBasicSubscriptionData bData)
        {
            Debug.Assert(false, "[AD701] Implementierung via Sub-Class!");
        }
        public virtual void Write(BotSubscriptionPurpose sPurpose, BinaryWriter bWriter, object oParam = null)
        {
            Debug.Assert(false, "[AD701] Implementierung via Sub-Class!");
        }
        public virtual void Read(BotSubscriptionPurpose sPurpose, BinaryReader bReader)
        {
            Debug.Assert(false, "[AD706] Implementierung via Sub-Class!");
        }
        #endregion


        protected BotTradingPair tRefPair;
    }
    // Ticker-Abonnement.
    public class BotTickerSubscription : BotBasePairSubscription
    {
        #region Constants
        public const int MIN_DELAY = 100;
        public const int MAX_DELAY = (60 * 1000);
        #endregion


        public class BotTickerData : BotBasicSubscriptionData, ICloneable
        {
            #region Properties.Management
            [Browsable(false)]
            public override double CurrentValue { get { return dLastPrice; } }
            #endregion


            #region Management
            public override void Write(BotSubscriptionPurpose sPurpose, BinaryWriter bWriter)
            {
                base.Write(sPurpose, bWriter);

                bWriter.Write(dLastPrice);
            }
            public override void Read(BotSubscriptionPurpose sPurpose, BinaryReader bReader)
            {
                base.Read(sPurpose, bReader);

                dLastPrice = bReader.ReadDouble();
            }
            #endregion
            #region Management
            public object Clone()
            {
                return (MemberwiseClone());
            }
            #endregion


            public double dBid;
            public double dBidSize;
            public double dAsk;
            public double dAskSize;
            public double dDailyChange;
            public double dDailyChangePerc;
            public double dLastPrice;
            public double dVolume;
            public double dHigh;
            public double dLow;
        }


        public List<BotTickerData> lData;


        public BotTickerSubscription(int iID, BotTradingPair tRefPair) : base(BotSubscriptionType.eTicker, iID, tRefPair)
        {
            this.lData = new List<BotTickerData>();
        }
        ~BotTickerSubscription()
        {
            lData.Clear();
        }


        #region Properties.Management
        [Browsable(false)]
        public override bool IsReady { get { return (lData.Count > 0); } }
        #endregion


        #region Management
        public override void AddData(BotBaseSubscription.BotBasicSubscriptionData bData)
        {
            Debug.Assert((bData.GetType() == typeof(BotTickerData)), "[AD596]");

            lData.Add((BotTickerData)bData);
        }
        public override void Write(BotSubscriptionPurpose sPurpose, BinaryWriter bWriter, object oParam = null)
        {
            // Letzte Ticker-Daten schreiben:
            BotTickerData tLastData = lData.Last();

            tLastData.Write(sPurpose, bWriter);
        }
        public override void Read(BotSubscriptionPurpose sPurpose, BinaryReader bReader)
        {
            BotTickerData tData = new BotTickerData();

            tData.Read(sPurpose, bReader);
            lData.Add(tData);
        }
        #endregion
    }
    // Candle-Abonnement.
    public class BotCandleSubscription : BotBasePairSubscription
    {
        #region Constants
        public const int DATACOUNT_MIN = 100;                                       // Anzahl an Datensätzen die wenigstens vorhanden sein sollte.
        #endregion


        public class BotCandleData : BotBasicSubscriptionData, ICloneable
        {
            #region Properties.Management
            [Browsable(false)]
            public override double CurrentValue { get { return dClose; } }
            [Browsable(false)]
            public override TimeSpan TimeStamp { get { return tTimeStamp; } }
            #endregion


            #region Management
            public object Clone()
            {
                return (new BotCandleData()
                {
                    dReceivedTime = this.dReceivedTime,
                    tTimeStamp = this.tTimeStamp,
                    dOpen = this.dOpen,
                    dClose = this.dClose,
                    dHigh = this.dHigh,
                    dLow = this.dLow,
                    dVolume = this.dVolume
                });
            }
            public override string ToString()
            {
                return (string.Format("[{0}] O={1} | C={2} | H={3} | L={4}", TimeStamp, dOpen, dClose, dHigh, dLow));
            }

            /// <summary>
            /// Merges data of current instance with other candle-data (Result is stored in current instance).
            /// </summary>
            /// <param name="bData"></param>
            /// <returns></returns>
            public bool Merge(BotCandleData bData)
            {
                if (this == bData)
                    return (true);
                else if (this.TimeStamp == bData.TimeStamp)
                    // Unable to merge candle data of same timestamps:
                    return (false);
                else
                {
                    BotCandleData _bFirst = (this.TimeStamp < bData.TimeStamp) ? this : bData;
                    BotCandleData _bLast = (this.TimeStamp < bData.TimeStamp) ? bData : this;

                    this.dOpen = _bFirst.dOpen;
                    this.dClose = _bLast.dClose;
                    this.dHigh = Math.Max(_bFirst.dHigh, _bLast.dHigh);
                    this.dLow = Math.Min(_bFirst.dLow, _bLast.dLow);
                    this.dVolume = (_bFirst.dVolume + _bLast.dVolume);

                    return (true);
                }
            }
            public override void Write(BotSubscriptionPurpose sPurpose, BinaryWriter bWriter)
            {
                base.Write(sPurpose, bWriter);

                bWriter.Write(tTimeStamp.Ticks);
                bWriter.Write(dOpen);
                bWriter.Write(dClose);
                bWriter.Write(dHigh);
                bWriter.Write(dLow);
                bWriter.Write(dVolume);
            }
            public override void Read(BotSubscriptionPurpose sPurpose, BinaryReader bReader)
            {
                base.Read(sPurpose, bReader);

                tTimeStamp = new TimeSpan(bReader.ReadInt64());
                dOpen = bReader.ReadDouble();
                dClose = bReader.ReadDouble();
                dHigh = bReader.ReadDouble();
                dLow = bReader.ReadDouble();
                dVolume = bReader.ReadDouble();
            }
            #endregion


            public TimeSpan tTimeStamp;
            public double dOpen;
            public double dClose;
            public double dHigh;
            public double dLow;

            public double dVolume = 0;
        }


        public List<BotCandleData> lData;


        public BotCandleSubscription(BotTradingPair tRefPair) : this(0, tRefPair, TimeSpan.Zero)
        {
        }
        public BotCandleSubscription(int iID, BotTradingPair tRefPair, TimeSpan tInterval) : base(BotSubscriptionType.eCandles, iID, tRefPair)
        {
            this.tRefPair = tRefPair;
            this.Interval = tInterval;
            this.iMaxDataCount = -1;
            this.lData = new List<BotCandleData>();
        }
        ~BotCandleSubscription()
        {
            lData.Clear();
        }


        #region Properties.Management
        [Browsable(false)]
        public override bool IsReady { get { return (lData.Count > 0); } }
        #endregion
        #region Properties
        [Browsable(false)]
        public TimeSpan Interval { private set; get; }
        [Browsable(false)]
        public TimeSpan Duration { get { return ((lData.Count <= 1) ? TimeSpan.Zero : (lData.Last().TimeStamp - lData.First().TimeStamp)); } }
        [Browsable(false)]
        public DateTime ReceivedTime { get { return ((lData.Count == 0) ? default(DateTime) : lData.Last().dReceivedTime); } }
        [Browsable(false)]
        public int MaxDataCount
        {
            set
            {
                iMaxDataCount = ((value == -1) ? value : Math.Max(DATACOUNT_MIN, value));

                // Ggf. aktuelle Anzahl an Datensätzen begrenzen:
                if ((iMaxDataCount != -1) && (lData.Count > iMaxDataCount))
                    lData.RemoveRange(0, (lData.Count - iMaxDataCount));
            }
            get { return (iMaxDataCount); }
        }
        private int iMaxDataCount;
        [Browsable(false)]
        public IBotCandleService UpdatedBy { set; get; }
        #endregion


        #region Management
        public override void AddData(BotBaseSubscription.BotBasicSubscriptionData bData)
        {
            switch (bData)
            {
                case BotCandleData _bCandleData:
                    // Prüfen ob Daten bereits existieren:
                    BotCandleData cDataTmp = lData.Find(_bData => (_bData.TimeStamp == _bCandleData.TimeStamp));

                    if (cDataTmp == null)
                    {
                        // Datensätze in umgekehrter Reinhenfolge aufnehmen:
                        // > Somit wird die Reihenfolge (nach Timestamp) aufsteigend sortiert ([0] = Ältestes Ereignis, [1] ...).
                        // > Sortierung würde dann auch zu nachfolgenden Subscription-Updates passen!
                        lData.Insert(0, _bCandleData);

                        ValidateMaxCount();
                    }
                    else
                    {
                        // Daten übernehmen:
                        cDataTmp = _bCandleData;

                        bData = null;
                    }
                    break;

                default:
                    throw new InvalidOperationException("[AD596] Unexpected subscription data!");
            }
        }
        /// <summary>
        /// Add a candle-subscriptions candle-data.
        /// Candles are merged if configured intervall is too high.
        /// </summary>
        /// <param name="bSubscription"></param>
        /// <returns></returns>
        public bool AddData(BotCandleSubscription bSubscription)
        {
            BotCandleSubscription.BotCandleData _cLastCandle = this.lData.Last();

            // Determine to add candle-data:
            int _iStart;
            if (_cLastCandle == null)
                // Add whole subscription data:
                _iStart = 0;
            else
                // Add remaining subscription data:
                _iStart = bSubscription.lData.FindIndex(_bData => (_cLastCandle.TimeStamp <= _bData.TimeStamp));

            if (_iStart == -1)
                // Subscription does not contain new candles:
                return (false);
            else
            {
                BotCandleSubscription.BotCandleData _bData;

                // Replace last candle:
                if (_cLastCandle != null)
                {
                    _bData = bSubscription.lData[_iStart];
                    if (_cLastCandle.TimeStamp == _bData.TimeStamp)
                    {
                        this.lData[this.lData.Count-1] = (BotCandleSubscription.BotCandleData)_bData.Clone();
                        _iStart++;
                    }
                }

                // Add candle-data:
                for (int i = _iStart; i < bSubscription.lData.Count; i++)
                {
                    _bData = bSubscription.lData[i];
                    Debug.Assert((_cLastCandle.TimeStamp < _bData.TimeStamp), "[AD986]");

                    if ((_bData.TimeStamp - _cLastCandle.TimeStamp) >= Interval)
                    {
                        _cLastCandle = (BotCandleSubscription.BotCandleData)_bData.Clone();
                        this.lData.Add(_cLastCandle);

                        ValidateMaxCount();
                    }
                    else
                        _cLastCandle.Merge(_bData);
                }
                return (true);
            }
        }
        /// <summary>
        /// Add a ticker-subscriptions ticker-data. 
        /// </summary>
        /// <param name="bSubscription"></param>
        public void AddData(BotTickerSubscription bSubscription)
        {
            Debug.Assert((bSubscription.lData.Count > 0), "[UC144]");

            // Prüfen ob Dauer von letztem Candle-Stick mit diesem Update überschritten wird:
            BotCandleSubscription.BotCandleData cLastCandle = this.lData.Last();
            BotTickerSubscription.BotTickerData cLastTicker = bSubscription.lData.Last();
            TimeSpan tReceived = UtlDateTime.DateTimeToUTCTimeStamp(cLastTicker.dReceivedTime);

            if ((tReceived - cLastCandle.TimeStamp) > Interval)
            {
                // INFO:
                // Theoretisch kann diese Funktion eine Weile nicht aufgerufen werden oder das Candle-Update hat noch nicht die aktuellsten Daten.
                // Das hätte zur Folge dass die entsprechenden Candle-Sticks nicht durch diese Funktion generiert werden würden!

                // Neuen Datensatz erzeugen:
                cLastCandle = new BotCandleSubscription.BotCandleData();

                cLastCandle.tTimeStamp = UtlDateTime.DateTimeToUTCTimeStamp(UtlDateTime.RoundDown(cLastTicker.dReceivedTime, Interval));
                cLastCandle.dOpen = cLastTicker.dLastPrice;
                cLastCandle.dLow = cLastTicker.dLastPrice;
                cLastCandle.dHigh = cLastTicker.dLastPrice;

                this.lData.Add(cLastCandle);
                this.ValidateMaxCount();
            }
            else
            {
                cLastCandle = this.lData.Last();

                cLastCandle.dLow = Math.Min(cLastCandle.dLow, cLastTicker.dLastPrice);
                cLastCandle.dHigh = Math.Max(cLastCandle.dHigh, cLastTicker.dLastPrice);
            }

            cLastCandle.dReceivedTime = cLastTicker.dReceivedTime;
            cLastCandle.dClose = cLastTicker.dLastPrice;
        }
        public override void Write(BotSubscriptionPurpose sPurpose, BinaryWriter bWriter, object oParam = null)
        {
            int _iFirst = 0;

            // Choose data:
            switch (oParam)
            {
                case TimeSpan _tLast:
                    if (_tLast != TimeSpan.Zero)
                        _iFirst = lData.FindIndex(_bData => (_bData.TimeStamp > _tLast));
                    break;
            }
            if (_iFirst == -1)
                // Nothing to write:
                _iFirst = lData.Count;

            // Write candles:
            bWriter.Write(Interval.Ticks);
            bWriter.Write(lData.Count-_iFirst);

            for (int i = _iFirst; i < lData.Count; i++)
                lData[i].Write(sPurpose, bWriter);
        }
        public override void Read(BotSubscriptionPurpose sPurpose, BinaryReader bReader)
        {
            int iCount;

            Interval = new TimeSpan(bReader.ReadInt64());
            iCount = bReader.ReadInt32();

            if (iCount > 0)
            {
                BotCandleSubscription.BotCandleData _bData;
                for (int i = 0; i < iCount; i++)
                {
                    _bData = new BotCandleData();

                    _bData.Read(sPurpose, bReader);
                    lData.Add(_bData);
                }
            }
        }
        #endregion
        #region Helper
        private void ValidateMaxCount()
        {
            // Ensure respected limits:
            if ((MaxDataCount != -1) && (lData.Count > MaxDataCount))
                lData.RemoveRange(0, (lData.Count - MaxDataCount));
        }
        #endregion
    }
    #endregion

    #region Types.Formatters
    internal abstract class BotBaseServiceTypeFormatter : UtlFormat.BaseFormatter
    {
        #region Management
        public override string Format(string sFormat, object oArg, IFormatProvider iFormatProvider)
        {
            Type _tType = null;
            if (oArg is Type)
                _tType = (Type)oArg;
            else if (oArg is IBotBaseService)
                _tType = ((IBotBaseService)oArg).ServiceType;

            if ((_tType != null) && (typeof(IBotBaseService).IsAssignableFrom(_tType)))
                return (Format(_tType));
            else
                return (base.Format(sFormat, oArg, iFormatProvider));
        }
        #endregion
        #region Management
        protected virtual string Format(Type tServiceType)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
    internal class BotServiceTypeConfigFormatter : BotBaseServiceTypeFormatter
    {
        #region Management
        protected override string Format(Type tServiceType)
        {
            string sResult;
            if (tServiceType.Name.TryRemoveStart("IBot", out sResult))
                return (sResult);
            else
                throw new InvalidCastException(string.Format("Failed to format unexpected type '{0}'!", tServiceType.ToString()));
        }
        #endregion
    }
    internal class BotServiceTypeNameFormatter : BotServiceTypeConfigFormatter
    {
        #region Management
        protected override string Format(Type tServiceType)
        {
            string sResult = base.Format(tServiceType);

            // Insert space:
            int _iIdx = sResult.IndexOf("Service");
            if (_iIdx != -1)
                sResult = sResult.Insert(_iIdx, " ");
            return (sResult);
        }
        #endregion
    }
    internal class BotServiceTypeShortCutFormatter : BotServiceTypeConfigFormatter
    {
        #region Management
        protected override string Format(Type tServiceType)
        {
            /*
            // Return upper letters only:
            return (new String(base.Format(tServiceType)
                .Replace("Service", string.Empty)
                .Where(_cChar => char.IsUpper(_cChar))
                .ToArray()));
            */

            // Return only the first consonants of each word:
            string sResult = string.Empty;
            int _iChars = 0;
            base.Format(tServiceType)
                .Replace("Service", string.Empty)
                .ForEach(_cChar =>
                {
                    if (char.IsUpper(_cChar))
                        _iChars = 1;
                    else if ((UtlChar.IsConsonant(_cChar)) && (_iChars < 4))
                    {
                        if (_iChars == 0)
                            _cChar = Char.ToUpper(_cChar);
                        _iChars++;
                    }
                    else
                        return;
                    sResult += _cChar;
                });
            return (sResult);
        }
        #endregion
    }
    #endregion
    #region Types.Services
    public static class BotServiceManager
    {
        #region Types
        public interface IBotTypedServiceList : IEnumerable<IBotBaseService>
        {
            #region Properties.Management
            Type ServiceType { get; }
            int MaxParallel { get; }
            bool CanActivateService { get; }
            #endregion
            #region Properties
            IEnumerable<IBotBaseService> SelectedServices { get; }
            #endregion
            #region Management
            internal bool Add(IBotBaseService iService);
            bool Select(string sDomain);
            #endregion
            #region Format
            string FormatServiceType(IFormatProvider iFormat);
            #endregion
        }
        internal class BotTypedServiceList<TService> : List<TService>, IBotTypedServiceList where TService : IBotBaseService
        {
            public BotTypedServiceList(int iMaxParallel = -1)
            {
                ServiceType = typeof(TService);
                MaxParallel = iMaxParallel;

                ServiceTypes.Add(this);
            }


            #region Properties.Management
            public Type ServiceType { private set; get; }

            public int MaxParallel { private set; get; }
            public bool CanActivateService { get { return ((MaxParallel <= 0) || (ActiveServices < MaxParallel)); } }
            #endregion
            #region Properties
            public IEnumerable<IBotBaseService> SelectedServices { get { return ((MaxParallel == 0) ? null : this.First<IBotBaseService>(MaxParallel)); } }
            public int ActiveServices { get { return (this.Where<IBotBaseService>(_bSvc => _bSvc.IsActive).Count()); } }
            #endregion


            #region Management.Enumerator
            IEnumerator<IBotBaseService> IEnumerable<IBotBaseService>.GetEnumerator()
            {
                foreach (IBotBaseService _iSvc in this)
                    yield return (_iSvc);
            }
            IEnumerator IEnumerable.GetEnumerator()
            {
                return (GetEnumerator());
            }
            #endregion
            #region Management
            bool IBotTypedServiceList.Add(IBotBaseService iService)
            {
                if (ServiceType.IsAssignableFrom(iService.GetType()))
                {
                    base.Add((TService)iService);
                    return (true);
                }
                else
                    return (false);
            }
            public void Select(int iIndex)
            {
                if (iIndex > 0)
                    this.Move(iIndex, 0);
            }
            public void Select(TService iService)
            {
                Select(IndexOf(iService));
            }
            public bool Select(string sDomain)
            {
                TService _iSelSvc = (TService)this.FirstOrDefault<IBotBaseService>(_iSvc => _iSvc.ServiceDescriptor.ServiceDomain == sDomain);
                if (_iSelSvc == null)
                    return (false);
                else
                {
                    Select(_iSelSvc);
                    return (true);
                }
            }
            #endregion
            #region Format
            public string FormatServiceType(IFormatProvider iFormat)
            {
                return (string.Format(iFormat, "{0}", ServiceType));
            }
            #endregion
        }
        public class BotServiceProviderDictionary : Dictionary<string, List<IBotServiceProvider>> { }
        public interface IBotServiceTypeDictionary : IReadOnlyDictionary<Type, IBotTypedServiceList>
        {
            #region Management
            internal void Add(IBotTypedServiceList iServiceList);
            #endregion
        }
        internal class BotServiceTypeDictionary : Dictionary<Type, IBotTypedServiceList>, IBotServiceTypeDictionary
        {
            #region Management
            void IBotServiceTypeDictionary.Add(IBotTypedServiceList iServiceList)
            {
                base.Add(iServiceList.ServiceType, iServiceList);
            }
            #endregion
        }
        #endregion


        #region Properties.Management
        public static bool IsReady { private set; get; }

        /// <summary>
        /// List of services, separated per service-type.
        /// </summary>
        public static IBotServiceTypeDictionary ServiceTypes { get { return ((IBotServiceTypeDictionary)bServiceTypes); } }
        private static BotServiceTypeDictionary bServiceTypes { get; } = new BotServiceTypeDictionary();

        /// <summary>
        /// List of services providers, separated per service-domain.
        /// </summary>
        public static BotServiceProviderDictionary ServiceProviders { get; } = new BotServiceProviderDictionary();

        /// <summary>
        /// List of all services.
        /// </summary>
        public static IReadOnlyList<IBotBaseService> Services { get { return (lServices); } }
        private static List<IBotBaseService> lServices = new List<IBotBaseService>();

        /// <summary>
        /// List of all platform services.
        /// </summary>
        public static IReadOnlyList<IBotPlatformService> PlatformServices { get { return (lPlatformServices); } }
        private static BotTypedServiceList<IBotPlatformService> lPlatformServices = new BotTypedServiceList<IBotPlatformService>();
        /// <summary>
        /// Selected ticker service.
        /// </summary>
        public static IBotTickerService TickerService { get { return (lTickerServices[0]); } }
        private static BotTypedServiceList<IBotTickerService> lTickerServices = new BotTypedServiceList<IBotTickerService>(1);
        /// <summary>
        /// Selected candle service.
        /// </summary>
        public static IBotCandleService CandleService { get { return (lCandleServices[0]); } }
        private static BotTypedServiceList<IBotCandleService> lCandleServices = new BotTypedServiceList<IBotCandleService>(1);
        /// <summary>
        /// Selected exchange rate service.
        /// </summary>
        public static IBotExchangeRateService ExchangeRateService { get { return (lExchangeRateServices[0]); } }
        private static BotTypedServiceList<IBotExchangeRateService> lExchangeRateServices = new BotTypedServiceList<IBotExchangeRateService>(1);
        #endregion
        #region Properties.Format
        internal static IFormatProvider ConfigFormatter { get; } = new BotServiceTypeConfigFormatter();
        public static IFormatProvider NameFormatter { get; } = new BotServiceTypeNameFormatter();
        public static IFormatProvider ShortCutFormatter { get; } = new BotServiceTypeShortCutFormatter();
        #endregion


        #region Initialization
        internal static bool Init()
        {
            // Load service providers:
            foreach (string _sLib in Directory.EnumerateFiles(AppContext.BaseDirectory, "svc.*.dll", SearchOption.TopDirectoryOnly))
                TryLoadServices(Assembly.LoadFrom(_sLib));

            // Load services:
            foreach (var _bProviders in ServiceProviders)
            {
                IBotServiceProvider _pNewestPrv = _bProviders.Value.OrderByDescending(_pPrv => _pPrv.Version).First();
                foreach (IBotBaseService _iSvc in _pNewestPrv.Services)
                {
                    if (_iSvc.IsValid)
                        ServiceTypes[_iSvc.ServiceType].Add(_iSvc);
                }
            }

            // Validate:
            IsReady = true;
            foreach(IBotTypedServiceList _iList in ServiceTypes.Values)
            {
                if (_iList.Count() == 0)
                {
                    IsReady = false;
                    BotEnv.LogSystem.WriteLog("I1040", BotLogProcess.eInit, String.Format("No services of type '{0}' loaded!", _iList.ServiceType.ToString()), BotLogType.eError);
                }
            }

            if (IsReady)
            {
                // Register trade bot for newest version of each platform service:
                foreach (IBotPlatformService _iSvc in PlatformServices)
                    BotTradeBot.RegisterTradeBot((BotPlatformService)_iSvc);

                // Select default services:
                lCandleServices.Select("api.bitfinex.com");
                lTickerServices.Select("min-api.cryptocompare.com");
                lExchangeRateServices.Select("ecb.int/eurofxref");

                return (true);
            }
            else
                return (false);
        }
        /// <summary>
        /// Load service from an assembly by determinating exported CryptoRiftBot-Types. 
        /// </summary>
        /// <param name="aAssembly"></param>
        /// <returns></returns>
        private static void TryLoadServices(Assembly aAssembly)
        {
            if (aAssembly != null)
            {
                Version _vVersion = aAssembly.GetName().Version;
                foreach (Type _tType in aAssembly.ExportedTypes)
                {
                    BotServiceDescriptorAttribute _bDescriptor = (_tType.GetCustomAttribute(typeof(BotServiceDescriptorAttribute)) as BotServiceDescriptorAttribute);
                    if (_bDescriptor != null)
                    {
                        IBotServiceProvider iProvider = (Activator.CreateInstance(_tType) as IBotServiceProvider);
                        if (iProvider != null)
                            ServiceProviders.GetOrCreateValue(iProvider.ServiceDescriptor.ServiceDomain).Add(iProvider);
                    }
                }
            }
        }
        #endregion


        #region Configuration
        public static void WriteFile(JsonWriter jWriter)
        {
            ServiceTypes.ForEach(_iType =>
            {
                jWriter.WritePropertyName(_iType.Value.FormatServiceType(ConfigFormatter));
                jWriter.WriteStartArray();
                {
                    _iType.Value.ForEach(_iSvc =>
                    {
                        jWriter.WriteStartObject();
                        {
                            jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_SERVICE_DOMAIN);
                            jWriter.WriteValue(_iSvc.ServiceDescriptor.ServiceDomain);

                            // (BETA) ...
                            // jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_SERVICE_ACTIVE);
                            // jWriter.WriteValue(true);

                            jWriter.WriteEndObject();
                        }
                    });
                    jWriter.WriteEnd();
                }
            });
        }
        public static bool ReadFile(JObject dObjects)
        {
            ServiceTypes.ForEach(_iType =>
            {
                // Apply read order to service list:
                JToken _jDomains = dObjects[_iType.Value.FormatServiceType(ConfigFormatter)];
                JToken _jDomain = _jDomains.Last;
                while (_jDomain != null)
                {
                    _iType.Value.Select((string)_jDomain.ParseValue(BotConvention.DATAFILE_JSON_SERVICE_DOMAIN, string.Empty));
                    _jDomain = _jDomain.Previous;
                }
            });

            return (true);
        }
        #endregion
    }
    public abstract class BotServiceProvider : IBotServiceProvider
    {
        public BotServiceProvider()
        {
            Version = GetType().Assembly.GetName().Version;
            ServiceDescriptor = (GetType().GetCustomAttribute(typeof(BotServiceDescriptorAttribute)) as BotServiceDescriptorAttribute);
            if (ServiceDescriptor == null)
                throw new CustomAttributeFormatException("Failed to determinate service descriptor!");

            Debug.Assert((Version != null), "[BBSP1131]");
        }


        #region Properties.Management
        public IReadOnlyList<IBotBaseService> Services { get { return (lServices); } }
        private List<IBotBaseService> lServices = new List<IBotBaseService>();
        #endregion
        #region Properties.Library
        public BotServiceDescriptorAttribute ServiceDescriptor { private set; get; }
        public Version Version { private set; get; }
        #endregion


        #region Initialization
        protected void AddService(IBotBaseService iService)
        {
            Debug.Assert((iService != null), "[AS1184]");

            lServices.Add(iService);
        }
        #endregion
    }
    public abstract class BotBaseService : IBotBaseService
    {
        #region Delegates.Communication
        public delegate void EvaluateRequestHandler(string sResponse);
        #endregion


        public BotBaseService(IBotServiceProvider iProvider)
        {
            Provider = iProvider;

            // Validate inherited interfaces:
            foreach (Type _tItf in GetType().GetInterfaces())
            {
                if (_tItf == typeof(IBotBaseService))
                    ;
                else if (typeof(IBotBaseService).IsAssignableFrom(_tItf))
                {
                    if (ServiceType == null)
                        ServiceType = _tItf;
                    else if (ServiceType.IsAssignableFrom(_tItf))
                        ; // Interfaces are compatible.
                    else
                    {
                        BotEnv.LogSystem.WriteLog("BBS1290", BotLogProcess.eInit, string.Format("Failed to initialize service of type '{0}': Parallel service-interface implementations are restricted!", GetType()), BotLogType.eError);
                        ServiceType = null;
                        break;
                    }
                }
            }
            if (ServiceType == null)
                BotEnv.LogSystem.WriteLog("BBS1295", BotLogProcess.eInit, string.Format("Failed to initialize service of type '{0}': No service-interface implemented!", GetType()), BotLogType.eError);
        }
        ~BotBaseService()
        {
            dRequests.Clear();
            dRequests = null;
        }


        #region Properties.Library
        [Browsable(false)]
        public Type ServiceType { private set; get; }
        [Browsable(false)]
        public IBotServiceProvider Provider { private set; get; }

        [Category(BotConvention.PROP_CATEGORY_SERVICE)]
        [DisplayName("Description")]
        public BotServiceDescriptorAttribute ServiceDescriptor { get { return (Provider.ServiceDescriptor); } }
        [Category(BotConvention.PROP_CATEGORY_SERVICE)]
        [DisplayName("Version")]
        public Version Version { get { return (Provider.Version); } }
        #endregion
        #region Properties.Management
        [Browsable(false)]
        public bool IsValid { get { return ((Provider != null) && (ServiceType != null)); } }
        [Browsable(false)]
        public bool IsActive { private set; get; }

        [Browsable(false)]
        public virtual BotServiceOwner Owner { private set; get; }
        #endregion
        #region Properties
        public string Name { get { return (ServiceDescriptor.ServiceName); } }
        public BotServiceState State
        {
            set
            {
                if (bState != value)
                {
                    BotServiceState _bOldState = bState;
                    bState = value;

                    // Notify:
                    BotEnv.Post(() => BotEnv.OnServiceStateChanged?.Invoke(this, _bOldState));
                }
            }
            get { return (bState); }
        }
        private BotServiceState bState = BotServiceState.eOffline;
        #endregion


        public override string ToString()
        {
            return (Name);
        }


        #region Initialization
        public virtual bool ActivateService(BotServiceOwner bOwner = null)
        {
            if (!BotServiceManager.ServiceTypes[ServiceType].CanActivateService)
                throw new InvalidOperationException(string.Format("Failed to activate service of type {0}: Reached limit of max. parallel active services!", ServiceType));
            else if (IsActive)
                return (false);
            else
            {
                Owner = bOwner;
                IsActive = true;
                State = BotServiceState.eUnknown;
                return (true);
            }
        }
        public virtual bool DeactivateService()
        {
            if (!IsActive)
                return (false);
            else
            {
                Owner = null;
                IsActive = false;
                State = BotServiceState.eOffline;
                return (true);
            }
        }
        #endregion
        #region Communication.HTTP-Requests
        /// <summary>
        /// Executes a web request.
        /// </summary>
        /// <param name="sURI">Requested URI.</param>
        /// <param name="eAction">Implemtation of data evaluation.</param>
        /// <returns></returns>
        protected async Task<bool> ExecuteRequestAsync(BotLogProcess bProcess, string sURI, EvaluateRequestHandler eAction)
        {
            return (await ExecuteRequestAsync(bProcess, new WebRequestEx(sURI), eAction) == WebRequestEx.Result.Success);
        }
        /// <summary>
        /// Executes a web request than is validated to be called once in a certain time period.
        /// </summary>
        /// <param name="iID">Request Identifier.</param>
        /// <param name="tDelay">Minimum delay between two request calls.</param>
        /// <param name="sURI"></param>
        /// <param name="eAction"></param>
        /// <returns></returns>
        protected async Task<WebRequestEx.Result> ExecuteRequestAsync(BotLogProcess bProcess, int iID, TimeSpan tDelay, string sURI, EvaluateRequestHandler eAction)
        {
            WebRequestEx _wReq;
            if (dRequests.TryGetValue(iID, out _wReq))
                // Update request:
                _wReq.Delay = tDelay;
            else
                // Initialize new request:
                _wReq = new WebRequestEx(sURI, tDelay);

            return (await ExecuteRequestAsync(bProcess, _wReq, eAction));
        }
        private async Task<WebRequestEx.Result> ExecuteRequestAsync(BotLogProcess bProcess, WebRequestEx wRequest, EvaluateRequestHandler eAction)
        {
            Debug.Assert(wRequest != null);

            if (eAction == null)
                throw new NotImplementedException("Failed to execute not implemented request!");
            else
            {
                // (BETA) ... [Show in Log]
                // Show in log messages: actual service

                WebRequestEx.Result _rResult = await wRequest.GetContentAsync();
                switch (_rResult)
                {
                    case WebRequestEx.Result.Success:
                        _rResult = WebRequestEx.Result.Error;
                        try
                        {
                            eAction(wRequest.Received);
                            _rResult = WebRequestEx.Result.Success;
                        }
                        catch (BotInvalidRequestDataException)
                        {
                            BotEnv.LogSystem.WriteDebugLog("ERA1245", bProcess, "Ungültige Daten empfangen!", wRequest, BotLogType.ePlatformError);
                        }
                        catch (Exception eExcpt)
                        {
                            BotEnv.LogSystem.WriteDebugLog("ERA1250", bProcess, eExcpt.Message, wRequest, BotLogType.ePlatformError);
                        }
                        break;

                    case WebRequestEx.Result.Error:
                        BotEnv.LogSystem.WriteLog("ERA1258", bProcess, wRequest.LastError.sMessage, BotLogType.ePlatformError);
                        break;
                }

                // Update state:
                State = (_rResult == WebRequestEx.Result.Error) ? BotServiceState.eError : BotServiceState.eOnline;

                return (_rResult);
            }
        }
        #endregion


        private Dictionary<int, WebRequestEx> dRequests = new Dictionary<int, WebRequestEx>();
    }
    // Basisklasse für Trading-Platforms.
    [TypeConverter(typeof(BotFunctionConverter))]
    public abstract class BotPlatformService : BotBaseService, IBotPlatformService
    {
        #region Constants
        // Timer:
        public const int TMR_COUNT = 1;
        public const int TMR_ID_CONNECTION_OBSERVATION = 0;

        // Verbindung:
        public const int CON_DELAY_OBSERVATION_DEFAULT = (35 * 1000);
        public const int CON_DELAY_OBSERVATION_DESPERATE = (3 * (60 * 1000));

        public const int CON_MAX_RETRY_COUNT_DEFAULT = 5;
        public const int CON_MAX_RETRY_COUNT_DESPERATE = (100 + CON_MAX_RETRY_COUNT_DEFAULT);

        // Authentifizierung:
        public const String AUTH_PASSWORD_CHARS = "xxx";
        #endregion
        #region Enumerations
        public enum BotPlatformState
        {
            [Description("Offline")]
            BTSS_Offline,
            [Description("Initialisation")]
            BTSS_InitConnection,
            [Description("Online")]
            BTSS_Online,
            [Description("Wartung")]
            BTSS_Maintenance
        };
        #endregion


        #region Types
        [TypeConverter(typeof(BotFunctionConverter))]
        public class BotOrderSettings
        {
            #region Enumerations
            public enum BotOrderTypeEx
            {
                [Description("Market")]
                eMarket = BotOrderType.eExchangeMarket,
                [Description("Limit")]
                eLimit = BotOrderType.eExchangeLimit
            };
            #endregion


            #region Types
            [TypeConverter(typeof(CnvEmptyExpandableObject))]
            public class BotLimitOffset
            {
                public BotLimitOffset()
                {
                    dBuyOrders = 0;
                    dSellOrders = 0;
                }


                #region Properties
                [DisplayName("Kauf-Orders")]
                [Description("Anteil vom Kurswert der auszuführenden Order (in [%]) welcher zur Bestimmung des Limit-Grenzwert angewendet wird indem er zum Kurswert addiert wird. (0 = Limit entspricht aktuellem Kurswert)")]
                public double BuyOrders
                {
                    get { return (dBuyOrders); }
                    set { dBuyOrders = UtlMath.Between(value, 0, 100); }
                }
                private double dBuyOrders;
                [DisplayName("Sell-Orders")]
                [Description("Anteil vom Kurswert der auszuführenden Order (in [%]) welcher zur Bestimmung des Limit-Grenzwert angewendet wird indem er vom Kurswert abgezogen wird. (0 = Limit entspricht aktuellem Kurswert)")]
                public double SellOrders
                {
                    get { return (dSellOrders); }
                    set { dSellOrders = UtlMath.Between(value, -100, 0); }
                }
                private double dSellOrders;
                #endregion
            }
            public BotOrderSettings()
            {
                Type = BotOrderTypeEx.eLimit;
                Timeout = new TimeSpan(0, 3, 0);
                LimitOrderOffset = new BotLimitOffset();
            }
            #endregion


            #region Properties
            [DisplayName("Typ")]
            [Description("Order-Typ der für auszuführende Orders angewendet werden soll.")]
            [TypeConverter(typeof(CnvEnumDescription))]
            public BotOrderTypeEx Type { set; get; }
            [DisplayName("Zeitüberschreitung")]
            [Description("Bricht eine ausgeführte Order ab wenn sie bis zum Überschreiten des Timeouts nicht abgeschlossen wurde.")]
            public TimeSpan Timeout { set; get; }
            [DisplayName("Limit-Offset")]
            [Description("Anteil vom Kurswert der auszuführenden Order (in [%]) welcher zur Bestimmung des Limit-Grenzwert angewendet wird.")]
            [BotAttrib.BotRefOrderType(BotOrderType.eExchangeLimit)]
            public BotLimitOffset LimitOrderOffset { set; get; }
            #endregion


            #region Config
            public virtual void WriteFile(JsonWriter jWriter)
            {
                jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_ORDERS);
                jWriter.WriteStartObject();
                {
                    jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_ORDERS_TYPE);
                    jWriter.WriteValue(Type);
                    jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_ORDERS_TIMEOUT);
                    jWriter.WriteValue(Timeout);
                    jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_ORDERS_LIMITOFFSET_BUY);
                    jWriter.WriteValue(LimitOrderOffset.BuyOrders);
                    jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_ORDERS_LIMITOFFSET_SELL);
                    jWriter.WriteValue(LimitOrderOffset.SellOrders);

                    jWriter.WriteEndObject();
                }
            }
            public virtual bool ReadFile(JObject dObjects)
            {
                bool bResult = true;

                JToken _jToken = dObjects[BotConvention.DATAFILE_JSON_ORDERS];
                if ((bResult) && (_jToken != null))
                {
                    Type = _jToken.ParseValue(BotConvention.DATAFILE_JSON_ORDERS_TYPE, Type);
                    Timeout = _jToken.ParseValue(BotConvention.DATAFILE_JSON_ORDERS_TIMEOUT, Timeout);
                    LimitOrderOffset.BuyOrders = _jToken.ParseValue(BotConvention.DATAFILE_JSON_ORDERS_LIMITOFFSET_BUY, LimitOrderOffset.BuyOrders);
                    LimitOrderOffset.SellOrders = _jToken.ParseValue(BotConvention.DATAFILE_JSON_ORDERS_LIMITOFFSET_SELL, LimitOrderOffset.SellOrders);
                }
                else
                    bResult = false;

                return (bResult);
            }
            #endregion
        }
        #endregion


        public BotPlatformService(IBotServiceProvider iProvider, String sURL) : base(iProvider)
        {
            IsSimulating = ((this as IBotSimulatedPlatformService) != null);

            this.eAuthAPIKey = new BotEncryptedObject(BotMemoryEncryption.BotDataType.eAPIKey);
            this.eAuthAPISecret = new BotEncryptedObject(BotMemoryEncryption.BotDataType.eAPISecret);
            this.lPairs = new List<BotTradingPair>();
            this.Wallets = new List<BotWallet>();
            this.lSubscriptions = new List<BotBaseSubscription>();
            this.oOrders = new List<BotBaseOrder>();
            this.SubscriptionWebRequests = new List<BotSubscriptionWebRequest>();
            this.tTimer = new System.Timers.Timer[TMR_COUNT];
            this.sConnectRetryCount = 0;
            this.bConnection = false;
            this.LastMsgReceived = new DateTime(0);
            this.IsUsableByBotAutomatik = false;
            this.OrderSettings = new BotOrderSettings();

            if (!string.IsNullOrEmpty(sURL))
            {
                // Initialisieren:
                tTimer[TMR_ID_CONNECTION_OBSERVATION] = UtlTimer.CreateTimer((oSender, eArgs) => OnTimer_ConnectionObservation(this, oSender, eArgs), CON_DELAY_OBSERVATION_DEFAULT);

                wSocket = new WebSocket(sURL);
                {
                    wSocket.OnOpen += (oSender, eArgs) =>
                    {
                        Debug.Assert(IsActive);

                        Console.WriteLine(String.Format("[{0}] OnOpen", PlatformName));

                        OnSockOpen(oSender, eArgs);
                    };
                    wSocket.OnMessage += (oSender, eArgs) =>
                    {
                        Debug.Assert(IsActive);

                        LastMsgReceived = DateTime.Now;

                        string _sMsg = (string)eArgs.Data;
                        if (OnSockMessage(_sMsg))
                        {
                            if (BotDebugMode.Active)
                                BotDebugMode.OnSockMessage(this, _sMsg);
                        }
                        else
                            BotEnv.LogSystem.WriteDebugLog("OSM2566", BotLogProcess.eCommunication, String.Format("{0}: Fehler beim Verarbeiten einer empfangenen Nachricht!", PlatformName), _sMsg, BotLogType.ePlatformError);
                    };
                    wSocket.OnError += (oSender, eArgs) =>
                    {
                        Debug.Assert(IsActive);

                        BotEnv.LogSystem.WriteDebugLog("OSE2569", BotLogProcess.eCommunication, String.Format("{0}: Socket-Fehler!", PlatformName), eArgs.Message, BotLogType.ePlatformError);

                        OnSockError(oSender, eArgs);
                    };
                    wSocket.OnClose += (oSender, eArgs) =>
                    {
                        Debug.Assert(IsActive);

                        bool _bConnect = Connection;

                        Reset();

                        OnSockClose(oSender, eArgs);
                        Owner.OnSocketClosed();

                        if (_bConnect)
                        {
                            // Verbindung wurde unerwünscht unterbrochen:
                            BotEnv.LogSystem.WriteLog("OSC69", BotLogProcess.eCommunication, String.Format("Verbindung getrennt! {0} (Code: {1}, Clean: {2})", eArgs.Reason, eArgs.Code, eArgs.WasClean), BotLogType.ePlatformWarning);

                            // Verbindung neu aufbauen:
                            Connect();
                        }
                    };
                }
            }
        }
        ~BotPlatformService()
        {
            lPairs.Clear();

            wSocket = null;
        }


        #region Properties.Library
        [Browsable(false)]
        public string PlatformDomain { get { return (ServiceDescriptor.ServiceDomain); } }
        [Browsable(false)]
        public string PlatformName { get { return (ServiceDescriptor.ServiceName); } }
        #endregion
        #region Properties.Management
        [Browsable(false)]
        public new BotPlatformServiceOwner Owner { get { return ((BotPlatformServiceOwner)base.Owner); } }

        [Browsable(false)]
        public virtual BotCoinStreamPlayer CoinStreamPlayer { get { return (null); } }
        #endregion
        #region Properties.Management
        /// <summary>
        /// Aktuelle Instanz simuliert eine Platform.
        /// </summary>
        [Browsable(false)]
        public bool IsSimulating { private set; get; }
        [Browsable(false)]
        public bool HasWebRequests { protected set; get; }
        [Browsable(false)]
        public bool IsLoggedIn { get { return (GetLoginInfo().bLoggedIn); } }
        [Description("Indikator für Nutzung einer Funktion die nicht im Debug-Modus genutzt werden darf.")]
        [Browsable(false)]
        public bool IsNonDebugUsable { get { return ((IsSimulating == false) || (BotDebugMode.Active)); } }
        /// <summary>
        /// Web-Requests für Subscriptions (FALLS von Sub-Class unterstüzt!)
        /// </summary>
        [Browsable(false)]
        public List<BotSubscriptionWebRequest> SubscriptionWebRequests { internal set; get; }
        [Browsable(false)]
        public List<BotWallet> Wallets { protected set; get; }
        [Browsable(false)]
        public int PairCount { get { return (lPairs.Count); } }

        [Category(BotConvention.PROP_CATEGORY_MANAGEMENT)]
        [DisplayName("Letzter Empfang")]
        [Description("Zeitpunkt zum Empfang der letzten Socket-Nachricht.")]
        [Browsable(false)]
        public DateTime LastMsgReceived { private set; get; }
        #endregion
        #region Properties.Network
        [Description("Setzt/Ermittelt den gewünschten Zustand der Verbindung zur Platform.")]
        [Browsable(false)]
        public bool Connection
        {
            get { return (bConnection); }
            set
            {
                if (bConnection != value)
                {
                    if (value)
                        bConnection = Connect();
                    else
                    {
                        bConnection = false;
                        Disconnect();
                    }
                }
            }
        }
        #endregion
        #region Properties.Config
        [Category(BotConvention.PROP_CATEGORY_MANAGEMENT)]
        [DisplayName("Freigabe Trading-Bot Automatik")]
        [Description("Gibt das automatische De-/Aktivieren einzelner Pärchen via Trading-Bot Automatik für diese Platform frei.")]
        [TypeConverter(typeof(CnvBooleanLabel.DisEnabled))]
        public bool IsUsableByBotAutomatik { set; get; }
        [Category(BotConvention.PROP_CATEGORY_MANAGEMENT)]
        [DisplayName("Orders")]
        public BotOrderSettings OrderSettings { private set; get; }
        #endregion
        #region Properties.Network
        [Browsable(false)]
        public virtual bool IsConnected { get { return ((wSocket != null) && (wSocket.ReadyState == WebSocketState.Open)); } }
        #endregion
        #region Properties
        [Category(BotConvention.PROP_CATEGORY_AUTH)]
        [DisplayName("API-Key")]
        [PasswordPropertyText(true)]
        public String APIKey
        {
            get { return (eAuthAPIKey.State != BotEncryptedObject.BotState.eInvalid) ? AUTH_PASSWORD_CHARS : ""; }
            set { eAuthAPIKey.SetValue(value); }
        }
        [Category(BotConvention.PROP_CATEGORY_AUTH)]
        [DisplayName("API-Secret")]
        [PasswordPropertyText(true)]
        public String APISecret
        {
            get { return (eAuthAPISecret.State != BotEncryptedObject.BotState.eInvalid) ? AUTH_PASSWORD_CHARS : ""; }
            set { eAuthAPISecret.SetValue(value); }
        }
        [Category(BotConvention.PROP_CATEGORY_MANAGEMENT)]
        [DisplayName("Verbindung")]
        [Description("Aktueller Verbindungszustand zur Platform.")]
        [TypeConverter(typeof(CnvEnumDescription))]
        public BotPlatformState State
        {
            get
            {
                if (IsConnected)
                {
                    if (IsLoggedIn)
                        return (bIsMaintenance ? BotPlatformState.BTSS_Maintenance : BotPlatformState.BTSS_Online);
                    else
                        return (BotPlatformState.BTSS_InitConnection);
                }
                else
                    return (BotPlatformState.BTSS_Offline);
            }
        }
        [Browsable(false)]
        [Category(BotConvention.PROP_CATEGORY_WALLET)]
        [DisplayName("Gesamt-Guthaben")]
        [Description("Kumuliertes Guthaben (in [$] umgerechnet) aller verfügbaren Währungen.")]
        public double TotalBaseCurrencyFunds
        {
            get
            {
                double dAmount = 0;

                foreach (BotWallet wWalletTmp in Wallets)
                    dAmount += wWalletTmp.BaseCurrencyFunds;

                return (dAmount);
            }
        }
        #endregion



        #region Initialization
        public override bool ActivateService(BotServiceOwner bOwner = null)
        {
            if ((bOwner as BotPlatformServiceOwner) == null)
                throw new ArgumentException(string.Format("Failed to initialize service '{0}' due to Invalid type '{1}' of service owner!", ServiceDescriptor.ServiceName, bOwner.GetType().ToString()));
            else if (!base.ActivateService(bOwner))
                return (false);
            else
                return (true);
        }
        public override bool DeactivateService()
        {
            return (base.DeactivateService());
        }
        #endregion
        #region Management
        public BotWallet GetWallet(BotCoinType cCurrency, BotWalletType wType)
        {
            return ((BotWallet)Wallets.Find(obj => ((BotWallet)obj).Compare(cCurrency, wType)));
        }
        public BotTradingPair GetPair(int iIndex)
        {
            if ((0 <= iIndex) && (iIndex < lPairs.Count))
                return (lPairs[iIndex]);
            else
                return (null);
        }
        // Ermittelt ein Pair.
        public BotTradingPair FindPair(BotCoinType cPrimCurrency, BotCoinType cSecCurrency)
        {
            BotTradingPair tPairTmp;

            for (int i = 0; i < lPairs.Count; i++)
            {
                tPairTmp = lPairs[i];

                if (tPairTmp.Compare(cPrimCurrency, cSecCurrency))
                    return (tPairTmp);
            }

            return (null);
        }

        // Ermittelt ein Pair.
        public BotTradingPair FindPair(String sSymbol, bool bReportError = true)
        {
            BotTradingPair tPairTmp;

            if (sSymbol[0] == 't')
                // Führendes Zeichen entfernen
                sSymbol = sSymbol.Remove(0, 1);

            sSymbol = sSymbol.ToUpper();

            for (int i = 0; i < lPairs.Count; i++)
            {
                tPairTmp = lPairs[i];

                if (tPairTmp.Compare(sSymbol))
                    return (tPairTmp);
            }

            if (bReportError)
                BotEnv.LogSystem.WriteLog("FP156", BotLogProcess.eEvaluation, ("Unbekanntes Symbol '" + sSymbol + "'."), BotLogType.eWarning);

            return (null);
        }

        // Ermittelt ein Abonnement.
        public BotBaseSubscription FindSubscription(int iID)
        {
            BotBaseSubscription tSubscrTmp;

            for (int i = 0; i < lSubscriptions.Count; i++)
            {
                tSubscrTmp = lSubscriptions[i];

                if (tSubscrTmp.GetSubscriptionID() == iID)
                    return (tSubscrTmp);
            }

            return (null);
        }
        public BotBasePairSubscription FindPairSubscription(BotTradingPair tPair, BotSubscriptionType sType = BotSubscriptionType.Invalid)
        {
            BotBasePairSubscription tSubscrTmp;

            for (int i = 0; i < lSubscriptions.Count; i++)
            {
                if (lSubscriptions[i].GetType().IsSubclassOf(typeof(BotBasePairSubscription)))
                {
                    tSubscrTmp = (BotBasePairSubscription)lSubscriptions[i];

                    if ((sType == BotSubscriptionType.Invalid) || (lSubscriptions[i].GetSubscriptionType() == sType))
                    {
                        if (tPair.Compare(tSubscrTmp.ReferredPair))
                            return (tSubscrTmp);
                    }
                }
            }

            return (null);
        }

        /// <summary>
        /// Ermittelt die jüngste Order vom Zustand 'oState'.
        /// </summary>
        /// <param name="oState"></param>
        /// <param name="rPair"></param>
        /// <param name="iMaxTries">Definiert die maximale Anzahl an Versuchen bevor der Vorgang abgebrochen wird (Optional).</param>
        /// <returns></returns>
        public BotBaseOrder FindOrderLatest(BotOrderState oState, BotTradingPair rPair = null, int iMaxTries = -1)
        {
            for (int i = (oOrders.Count - 1); i >= 0; i--)
            {
                if ((rPair != null) && (rPair.Compare(oOrders[i].tRefPair) == false))
                    continue;
                else if (oOrders[i].oState == oState)
                    return (oOrders[i]);

                if (iMaxTries != -1)
                {
                    if (iMaxTries > 0)
                        iMaxTries--;
                    else
                        break;
                }
            }

            return (null);
        }
        /// <summary>
        /// Prüft ob zur Zeit eine Order ausgeführt wird.
        /// </summary>
        /// <param name="rPair">Prüft nur das angegebene Pärchen.</param>
        /// <returns>Blockierende Order</returns>
        public BotBaseOrder IsOrderingBusy(BotTradingPair rPair = null)
        {
            return (oOrders.Find(oTemp => ((rPair == null) || (oTemp.tRefPair == rPair)) &&
                                            ((oTemp.oState == BotOrderState.ExInitialized) || (oTemp.oState >= BotOrderState.eActive))));
        }
        #endregion
        #region Management
        public virtual void Reset()
        {
            BotBaseLoginInfo bInfo = GetLoginInfo();

            bInfo.bLoggedIn = false;
            bIsMaintenance = false;

            lSubscriptions.Clear();
            oOrders.Clear();

            for (int i = 0; i < lPairs.Count; i++)
                lPairs[i].Reset();

            GC.Collect();
        }

        public virtual BotBaseLoginInfo GetLoginInfo()
        {
            // Implementation via SubClass!
            Debug.Assert(false, "[GLI576]");

            return (null);
        }
        #endregion
        #region Management
        private static void OnTimer_ConnectionObservation(BotPlatformService pPlatform, object oSender, ElapsedEventArgs eArgs)
        {
            bool bReconnect = true;

            pPlatform.sConnectRetryCount++;

            if (pPlatform.IsConnected == false)
            {
                String sError = "";
                BotLogPriority lPriority = BotLogPriority.eNormal;

                if (pPlatform.sConnectRetryCount == CON_MAX_RETRY_COUNT_DEFAULT)
                {
                    sError = "verlangsahmt";

                    // Verzweifelt weiter versuchen die Verbindung aufzubauen.
                    // > Delay erhöhen:
                    pPlatform.tTimer[TMR_ID_CONNECTION_OBSERVATION].Interval = CON_DELAY_OBSERVATION_DESPERATE;
                }
                else if (pPlatform.sConnectRetryCount == CON_MAX_RETRY_COUNT_DESPERATE)
                {
                    sError = "fehlgeschlagen";
                    bReconnect = false;
                    lPriority = BotLogPriority.eHigh;

                    // Normalen Delay anwenden für nächsten Verbindungsaufbau:
                    pPlatform.tTimer[TMR_ID_CONNECTION_OBSERVATION].Interval = CON_DELAY_OBSERVATION_DEFAULT;
                }

                if (sError != "")
                    BotEnv.LogSystem.WriteLog("OTCO1920", BotLogProcess.eCommunication, String.Format("Verbindungsaufbau zu '{0}' nach {1} Versuchen {2}!)", pPlatform.PlatformName, pPlatform.sConnectRetryCount, sError), BotLogType.ePlatformError, lPriority);

                if (bReconnect == false)
                    pPlatform.Owner.OnSocketDisconnected();
            }
            else
            {
                bReconnect = false;

                if ((pPlatform.State == BotPlatformState.BTSS_Online) && (pPlatform.TestConnection()))
                {
                    // Verbindungszustand einwandfrei.

                    if ((DateTime.Now - pPlatform.LastMsgReceived).TotalMilliseconds > CON_DELAY_OBSERVATION_DEFAULT)
                    {
                        if (pPlatform.sConnectRetryCount == 1)
                        {
                            // Erste Zeitüberschreitung.
                            // > Ping senden:
                            pPlatform.Ping();
                            return;
                        }
                        else
                        {
                            BotEnv.LogSystem.WriteLog("OTCO2022", BotLogProcess.eCommunication, String.Format("Es werden keine Nachrichten von '{0}' empfangen! Verbindung neu aufbauen...)", pPlatform.PlatformName), BotLogType.ePlatformWarning);
                            bReconnect = true;
                        }
                    }
                }
                else
                {
                    // Verbindungszustand nicht korrekt.

                    if (pPlatform.sConnectRetryCount > 1)
                    {
                        BotEnv.LogSystem.WriteLog("OTCO2033", BotLogProcess.eCommunication, String.Format("Verbindung zu '{0}' konnte nicht initialisiert werden! Verbindung neu aufbauen...)", pPlatform.PlatformName), BotLogType.ePlatformWarning);
                        bReconnect = true;
                    }
                }
            }

            if (bReconnect)
                pPlatform.Reconnect();
            else
                pPlatform.sConnectRetryCount = 0;
        }
        #endregion


        #region Communication.Initialization
        protected virtual bool Connect()
        {
            if ((eAuthAPIKey.State != BotEncryptedObject.BotState.eReady) || (eAuthAPISecret.State != BotEncryptedObject.BotState.eReady))
            {
                BotEnv.LogSystem.WriteLog("C2596", BotLogProcess.eAccount, "Angegebene Informationen für Authentifizierung ungültig!", BotLogType.eError, BotLogPriority.eHigh);
                return (false);
            }
            else
            {
                wSocket.Connect();

                // Timer zum Überwachen des Verbindungszustands starten:
                tTimer[TMR_ID_CONNECTION_OBSERVATION].Start();

                return (true);
            }
        }
        protected virtual void Disconnect()
        {
            Reset();

            if (wSocket != null)
            {
                // Timer zum Überwachen des Verbindungszustands stoppen:
                tTimer[TMR_ID_CONNECTION_OBSERVATION].Stop();

                wSocket.Close();
            }
        }
        #endregion
        #region Communication.Commands
        public void Reconnect()
        {
            // Verbindung trennen:
            bConnection = false;

            Disconnect();

            // Verbindung neu aufbauen:
            bConnection = Connect();
        }
        public bool TestConnection()
        {
            // Socket-Ping ausführen aus um Verbindung zu testen:
            return ((wSocket != null) && (wSocket.IsAlive));
        }

        // Initialisiert verfügbare Web-Requests.
        public void InitWebRequests()
        {
            List<BotTradingPair> lAvailablePairs = new List<BotTradingPair>();
            BotTradingPair tPairTmp;

            // Verfügbare Traind-Pairs ermitteln:
            for (int i = 0; i < PairCount; i++)
            {
                tPairTmp = GetPair(i);

                if (tPairTmp.IsActive)
                    lAvailablePairs.Add(tPairTmp);
            }

            // Web-Requests mit verfügbaren Trading-Pairs initialisieren:
            foreach (BotSubscriptionWebRequest pReqTmp in SubscriptionWebRequests)
                pReqTmp.Init(lAvailablePairs);
        }
        // Führt einen Web-Request aus.
        public bool ExecuteWebRequest(int iIndex)
        {
            if (SubscriptionWebRequests[iIndex] != null)
            {
                if (SubscriptionWebRequests[iIndex].IsValid)
                    return (SubscriptionWebRequests[iIndex].GetContentAsync().Result != WebRequestEx.Result.Error);
            }
            return (false);
        }
        #endregion
        #region Communication.Commands
        public virtual bool Ping()
        {
            return (false);
        }

        // Meldet sich an einem Account an.
        public virtual bool Login()
        {
            // Implementation via SubClass!
            Debug.Assert(false, "[L537]");

            return (false);
        }

        // Plaziert eine neue Order.
        public virtual bool PlaceOrder(BotTradingPair tPair, double dAmount, BotBaseOrder bOrder = null)
        {
            bool bResult = false;

            if (bOrder == null)
                Debug.Assert(false, "[PO2249] Order 'bOrder' MUSS via SubClass erzeugt werden!");

            else if (dAmount == 0)
                Debug.Assert(false, "[PO2252] Kein Betrag für Order definiert!");

            else
            {
                // Neue Order erstellen:
                bOrder.ulClientOrderID = ulong.Parse(GenerateClientOrderID());
                bOrder.oType = (BotOrderType)OrderSettings.Type;
                bOrder.dAmount = dAmount;
                bOrder.tRefPair = tPair;

                switch (bOrder.oType)
                {
                    case BotOrderType.eExchangeMarket:
                        // Keine weitere Behandlung notwendig.
                        break;

                    case BotOrderType.eExchangeLimit:
                        double dOffset;

                        if (bOrder.IsBuyOrder)
                            dOffset = OrderSettings.LimitOrderOffset.BuyOrders;
                        else
                            dOffset = OrderSettings.LimitOrderOffset.SellOrders;

                        bOrder.dLimitRate = (tPair.LastRateValue + (tPair.LastRateValue * 0.01 * dOffset));
                        break;

                    default:
                        Debug.Assert(false, "[PO2261] Ungültiger Order-Typ!");
                        break;
                }

                // Order ausführen:
                bResult = SendNewOrder(bOrder);
            }

            return (bResult);
        }
        // Sendet eine Anforderung zur Stornierung der entsprechenden Order an die Plattform.
        public virtual bool CancelOrder(BotBaseOrder bOrder)
        {
            // Implementation via SubClass!
            Debug.Assert(false, "[CO2371]");

            return (false);
        }

        // Ermittelt Platform-spezifische Informationen.
        public virtual bool GetPlatformSpecific()
        {
            // Ggf. Implementation via SubClass!
            return (true);
        }

        // Abboniert einen Ticker.
        public virtual bool SubscribeTicker(BotTradingPair tPair)
        {
            // Implementation via SubClass!
            Debug.Assert(false, "[ST675]");

            return (false);
        }
        // Abboniert Candle-Sticks.
        public virtual bool SubscribeCandles(BotTradingPair tPair, TimeSpan tInterval)
        {
            // Implementation via SubClass!
            Debug.Assert(false, "[SC681]");

            return (false);
        }
        // Bestellt einen abboniert einen Kanal ab.
        public virtual bool Unsubscribe(int iChannelID)
        {
            // Implementation via SubClass!
            Debug.Assert(false, "[U687]");

            return (false);
        }
        public virtual void UseSubscriptionWebRequest(BotSubscriptionWebRequest sWebRequest)
        {
        }
        #endregion


        #region Config
        public virtual void WriteFile(JsonWriter jWriter, BotContentType bContent)
        {
            jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_DOMAIN);
            jWriter.WriteValue(PlatformDomain);

            if (bContent == BotContentType.eConfig)
            {
                eAuthAPIKey.WriteFile(jWriter, BotConvention.DATAFILE_JSON_AUTH_APIKEY);
                eAuthAPISecret.WriteFile(jWriter, BotConvention.DATAFILE_JSON_AUTH_APISECRET);

                jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_BOTAUTOMATIC_ISUSABLE);
                jWriter.WriteValue(IsUsableByBotAutomatik);

                OrderSettings.WriteFile(jWriter);
            }
        }
        public virtual bool ReadFile(JObject dObjects, BotContentType bContent)
        {
            Debug.Assert(dObjects != null);

            if (bContent == BotContentType.eConfig)
            {
                eAuthAPIKey.ReadFile(dObjects, BotConvention.DATAFILE_JSON_AUTH_APIKEY);
                eAuthAPISecret.ReadFile(dObjects, BotConvention.DATAFILE_JSON_AUTH_APISECRET);

                IsUsableByBotAutomatik = dObjects.ParseValue(BotConvention.DATAFILE_JSON_BOTAUTOMATIC_ISUSABLE, IsUsableByBotAutomatik);

                OrderSettings.ReadFile(dObjects);
            }

            return (true);
        }
        #endregion


        #region Communication.Commands
        protected virtual bool OnSendNewOrder(BotBaseOrder bOrder)
        {
            // Implementation via SubClass!
            Debug.Assert(false, "[OSNO1006]");

            return (false);
        }
        #endregion
        #region Communication.Commands
        protected bool SocketSend(String sMessage)
        {
            if (sMessage.Length == 0)
                Debug.Assert(false, "[SS2051]");
            else
            {
                if (BotDebugMode.Active)
                    // Event weiterleiten:
                    BotDebugMode.OnSocketSend(this, sMessage);

                try
                {
                    wSocket.Send(sMessage);
                    return (true);
                }
                catch (InvalidOperationException iExcpt)
                {
                    BotEnv.LogSystem.WriteLog("SS2065", BotLogProcess.eCommunication, String.Format("Ungültige Operation bei Übertragung (Code {0}): ", iExcpt.HResult, iExcpt.Message), BotLogType.eError);
                }
                catch (ArgumentException aExcpt)
                {
                    BotEnv.LogSystem.WriteLog("SS2069", BotLogProcess.eCommunication, String.Format("Ungültiges Argument bei Übertragung (Code {0}): ", aExcpt.HResult, aExcpt.Message), BotLogType.eError);
                }
            }

            return (false);
        }


        private bool SendNewOrder(BotBaseOrder bOrder)
        {
            Debug.Assert((bOrder != null), "[SNO254]");

            if (bOrder.IsValid())
            {
                // Order registrieren:
                oOrders.Add(bOrder);

                return (OnSendNewOrder(bOrder));
            }
            else
            {
                Debug.Assert(false, "[SNO271]");
                bOrder = null;

                return (false);
            }
        }
        #endregion


        #region Events.Socket
        protected virtual void OnSockOpen(object oSender, EventArgs eArgs)
        {
        }
        protected virtual bool OnSockMessage(string sMessage)
        {
            return (false);
        }
        protected virtual void OnSockError(object oSender, WebSocketSharp.ErrorEventArgs eArgs)
        {
        }
        protected virtual void OnSockClose(object oSender, WebSocketSharp.CloseEventArgs eArgs)
        {
        }
        #endregion


        #region Helper
        // Generiert eine fortlaufende OrderID.
        // > ID muss X stellen jeweils mit '0' ersetzt werden, sonst kann Bitfinex die ID nicht korrekt verarbeiten.
        protected String GenerateClientOrderID()
        {
            const long lFactor = 10000;

            if (lLastClientOrderID == 0)
                // Initialisierung:
                lLastClientOrderID = (DateTime.Now.Ticks / lFactor);
            else
                lLastClientOrderID++;

            String sResult = String.Format("{0}", (lLastClientOrderID * lFactor));

            return (sResult);
        }
        #endregion


        #region Debug
        public void Debug_OnReceive(String sMessage)
        {
            OnSockMessage(sMessage);
        }
        #endregion


        protected List<BotTradingPair> lPairs;
        protected List<BotBaseSubscription> lSubscriptions;

        protected List<BotBaseOrder> oOrders;

        protected BotEncryptedObject eAuthAPIKey, eAuthAPISecret;
        protected bool bIsMaintenance;


        private long lLastClientOrderID = 0;

        private WebSocket wSocket;
        private bool bConnection;
        private short sConnectRetryCount;

        private System.Timers.Timer[] tTimer;
    }
    #endregion
}
