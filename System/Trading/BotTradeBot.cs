﻿using CryptoRiftBot;
using CryptoRiftBot.Trading.Functions;
using CryptoRiftBot.Trading.Services;
using CryptoRiftBot.Trading.Strategy;
using CryptoRiftBot.Trading;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Timers;
using System.Windows.Forms;
using Shared.Utils.Core;
using System.Reflection;
using CryptoRiftBot.Config;
using Newtonsoft.Json.Linq;

namespace CryptoRiftBot
{
    #region Types
    // Enthält zusammenfassende Informationen zu einem abgeschlossenen Rift.
    [TypeConverter(typeof(BotFunctionConverter))]
    public class BotRiftSummary : ICloneable
    {
        #region Constants
        public const String NAME_CLOSED_RIFTS = "Abgeschlossene Rifts";
        #endregion


        #region Types
        [TypeConverter(typeof(CnvEmptyExpandableObject))]
        public class BotOrderInfo
        {
            #region Properties
            [DisplayName("Anzahl Orders")]
            [Description("Anzahl an Orders in aktuellem Rift.")]
            [ReadOnly(true)]
            public int Orders { set; get; }
            [DisplayName("Betrag Orders")]
            [Description("Betrag aller Orders des aktuellen Rifts.")]
            [ReadOnly(true)]
            public double TotalAmount { set; get; }
            [DisplayName("Preis Orders")]
            [Description("Kumulierter Preis aller Orders des aktuellen Rifts.")]
            [ReadOnly(true)]
            public double TotalPrice { set; get; }
            [DisplayName("Gegenwert Orders")]
            [Description("Kumulierter Gegenwert (in [Euro]) aller Orders des aktuellen Rifts.")]
            [Browsable(true)]
            public double TotalExchangePrice { set; get; }
            [DisplayName("Durchschn. Kurs")]
            [Description("Durchschnittskurs zu dem Orders des aktuellen Rifts ausgeführt wurden.")]
            [ReadOnly(true)]
            public double TotalAverageRate { set; get; }
            #endregion


            #region Management
            public void Reset()
            {
                Orders = 0;
                TotalAmount = 0;
                TotalPrice = 0;
                TotalAverageRate = 0;
                TotalExchangePrice = 0;
            }
            #endregion
        }
        [TypeConverter(typeof(CnvEmptyExpandableObject))]
        public class BotClosedRiftInfo
        {
            #region Properties.Management
            [Browsable(false)]
            public int TotalRifts { get { return (GoodRifts + BadRifts); } }
            #endregion
            #region Properties
            [DisplayName("Erfolgreich")]
            [Description("Anzahl profitable abgeschlossener Rifts.")]
            [ReadOnly(true)]
            public int GoodRifts { set; get; }
            [DisplayName("Verlustreich")]
            [Description("Anzahl verlustreich abgeschlossener Rifts.")]
            [ReadOnly(true)]
            public int BadRifts { set; get; }
            #endregion


            #region Management
            public void Reset()
            {
                GoodRifts = 0;
                BadRifts = 0;
            }
            #endregion
        }
        #endregion


        public BotRiftSummary(BotTradingPair tRefPair)
        {
            this.ReferredPair = tRefPair;
            this.Buys = new BotOrderInfo();
            this.Sells = new BotOrderInfo();
            this.ClosedRifts = new BotClosedRiftInfo();
        }


        #region Properties.Management
        [Browsable(false)]
        public bool Active { get { return (Begin.Kind != DateTimeKind.Unspecified); } }
        [Browsable(false)]
        public bool Finished { get { return ((Begin.Kind != DateTimeKind.Unspecified) && (End.Kind != DateTimeKind.Unspecified)); } }
        [Browsable(false)]
        public int TotalOrders { get { return (Buys.Orders + Sells.Orders); } }
        #endregion
        #region Properties.References
        [Browsable(false)]
        public BotTradingPair ReferredPair { private set; get; }
        #endregion
        #region Properties
        [Browsable(false)]
        public DateTime Begin { set; get; }
        [Browsable(false)]
        public DateTime End { set; get; }
        [DisplayName("Kauf-Orders")]
        public BotOrderInfo Buys { private set; get; }
        [DisplayName("Verkauf-Orders")]
        public BotOrderInfo Sells { private set; get; }
        [DisplayName(NAME_CLOSED_RIFTS)]
        public BotClosedRiftInfo ClosedRifts { private set; get; }
        [DisplayName("Durchschn. Order-Kurs")]
        [Description("Durchschnittskurs zu dem sämtliche Ver- und Kauf-Orders des aktuellen Rifts ausgeführt wurden.")]
        public double TotalOrderAverageRate { private set; get; }
        [DisplayName("Dauer")]
        [Description("Laufzeit (Ab Zeitpunkt der zuerst ausgeführten Order) des aktuellen Rifts.")]
        public TimeSpan Duration
        {
            get
            {
                if (Active)
                    return (DateTime.Now - Begin);
                else if (Finished)
                    return (End - Begin);
                else
                    return (new TimeSpan(0, 0, 0, 0));
            }
        }
        [DisplayName("Profit")]
        [Description("Profit des aktuellen Rifts.")]
        public double Prophit { get { return ((Buys.TotalPrice) + (Sells.TotalPrice)); } }
        [DisplayName("Profit Gegenwert")]
        [Description("Profit-Gegenwert (in [Euro]) des aktuellen Rifts.")]
        [Browsable(true)]
        public double ProphitExchange { get { return ((Buys.TotalExchangePrice) + (Sells.TotalExchangePrice)); } }
        #endregion


        #region Events
        public void OnOrderFinished(BotBaseOrder oOrder)
        {
            if (Active == false)
                Begin = DateTime.Now;

            if (oOrder.IsBuyOrder)
            {
                // Einkaufs-Order ausgeführt.

                // Durchschnittlichen Einkaufskurs (Gewichteter Mittelwert) berechnen:
                Buys.TotalAverageRate = GetAverageRate(Buys.TotalAverageRate, Buys.TotalAmount, oOrder);

                Buys.Orders++;
                Buys.TotalAmount += oOrder.FinalAmount;
                Buys.TotalPrice += oOrder.FinalPrice;
                Buys.TotalExchangePrice += oOrder.ExchangePrice;
            }
            else if (oOrder.IsSellOrder)
            {
                // Verkaufs-Order ausgeführt.

                // Durchschnittlichen Verkaufskurs (Gewichteter Mittelwert) berechnen:
                Sells.TotalAverageRate = GetAverageRate(Sells.TotalAverageRate, Sells.TotalAmount, oOrder);

                Sells.Orders++;
                Sells.TotalAmount += Math.Abs(oOrder.FinalAmount);
                Sells.TotalPrice += oOrder.FinalPrice;
                Sells.TotalExchangePrice += oOrder.ExchangePrice;
            }
            else
                Debug.Assert(false, "[OOF1425]");

            CalcOrderAvgRate();
        }
        public void OnRiftClosed(BotRiftSummary rClosedRiftSummary = null)
        {
            if (rClosedRiftSummary == null)
                // Abgeschlossene Summary ist aktuelle Instanz:
                rClosedRiftSummary = this;

            // Profit von abgeschlossenem Rift auswerten:
            if (rClosedRiftSummary.Prophit >= 0)
                ClosedRifts.GoodRifts++;
            else
                ClosedRifts.BadRifts++;
        }
        #endregion


        #region Operators
        public static BotRiftSummary operator +(BotRiftSummary rObj1, BotRiftSummary rObj2)
        {
            BotRiftSummary rResult = new BotRiftSummary(rObj2.ReferredPair);

            rResult.Begin = (rObj1.Begin < rObj2.Begin) ? rObj1.Begin : rObj2.Begin;
            rResult.End = (rObj1.End > rObj2.End) ? rObj1.End : rObj2.End;
            rResult.Buys.Orders = (rObj1.Buys.Orders + rObj2.Buys.Orders);
            rResult.Sells.Orders = (rObj1.Sells.Orders + rObj2.Sells.Orders);
            rResult.Buys.TotalAmount = (rObj1.Buys.TotalAmount + rObj2.Buys.TotalAmount);
            rResult.Buys.TotalPrice = (rObj1.Buys.TotalPrice + rObj2.Buys.TotalPrice);
            rResult.Buys.TotalExchangePrice = (rObj1.Buys.TotalExchangePrice + rObj2.Buys.TotalExchangePrice);
            rResult.Buys.TotalAverageRate = UtlMath.GetWeightedAverage(rObj1.Buys.TotalAmount, rObj2.Buys.TotalAmount, rObj1.Buys.TotalAverageRate, rObj2.Buys.TotalAverageRate);
            rResult.Sells.TotalAmount = (rObj1.Sells.TotalAmount + rObj2.Sells.TotalAmount);
            rResult.Sells.TotalPrice = (rObj1.Sells.TotalPrice + rObj2.Sells.TotalPrice);
            rResult.Sells.TotalExchangePrice = (rObj1.Sells.TotalExchangePrice + rObj2.Sells.TotalExchangePrice);
            rResult.Sells.TotalAverageRate = UtlMath.GetWeightedAverage(rObj1.Sells.TotalAmount, rObj2.Sells.TotalAmount, rObj1.Sells.TotalAverageRate, rObj2.Sells.TotalAverageRate);
            rResult.ClosedRifts.GoodRifts = (rObj1.ClosedRifts.GoodRifts + rObj2.ClosedRifts.GoodRifts);
            rResult.ClosedRifts.BadRifts = (rObj1.ClosedRifts.BadRifts + rObj2.ClosedRifts.BadRifts);

            rResult.CalcOrderAvgRate();

            return (rResult);
        }
        public object Clone()
        {
            return (MemberwiseClone());
        }
        #endregion


        #region Management
        public void Reset()
        {
            Begin = new DateTime();
            End = new DateTime();
            TotalOrderAverageRate = 0;

            Buys.Reset();
            Sells.Reset();
            ClosedRifts.Reset();
        }
        public void FinalizeSummary()
        {
            End = DateTime.Now;
        }
        #endregion
        #region Configuration
        public virtual void WriteFile(JsonWriter jWriter)
        {
            jWriter.WriteStartObject();
            {
                jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_STAT_RIFT_BEGIN);
                jWriter.WriteValue(Begin);
                jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_STAT_RIFT_END);
                jWriter.WriteValue(End);
                jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_STAT_RIFT_GOODCOUNT);
                jWriter.WriteValue(ClosedRifts.GoodRifts);
                jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_STAT_RIFT_BADCOUNT);
                jWriter.WriteValue(ClosedRifts.BadRifts);

                jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_BUYORDERS);
                jWriter.WriteStartObject();
                {
                    jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_STAT_RIFT_ORDERS);
                    jWriter.WriteValue(Buys.Orders);
                    jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_STAT_RIFT_AMOUNT);
                    jWriter.WriteValue(Buys.TotalAmount);
                    jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_STAT_RIFT_PRICE);
                    jWriter.WriteValue(Buys.TotalPrice);
                    jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_STAT_RIFT_EXCHANGEPRICE);
                    jWriter.WriteValue(Buys.TotalExchangePrice);
                    jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_STAT_RIFT_AVERAGERATE);
                    jWriter.WriteValue(Buys.TotalAverageRate);

                    jWriter.WriteEndObject();
                }

                jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_SELLORDERS);
                jWriter.WriteStartObject();
                {
                    jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_STAT_RIFT_ORDERS);
                    jWriter.WriteValue(Sells.Orders);
                    jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_STAT_RIFT_AMOUNT);
                    jWriter.WriteValue(Sells.TotalAmount);
                    jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_STAT_RIFT_PRICE);
                    jWriter.WriteValue(Sells.TotalPrice);
                    jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_STAT_RIFT_EXCHANGEPRICE);
                    jWriter.WriteValue(Sells.TotalExchangePrice);
                    jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_STAT_RIFT_AVERAGERATE);
                    jWriter.WriteValue(Sells.TotalAverageRate);

                    jWriter.WriteEndObject();
                }

                jWriter.WriteEndObject();
            }
        }
        public virtual bool ReadFile(JObject dObjects)
        {
            if (dObjects == null)
            {
                Reset();
                return (false);
            }
            else
            {
                JToken _jToken;

                Begin = dObjects.ParseValue(BotConvention.DATAFILE_JSON_STAT_RIFT_BEGIN, Begin);
                End = dObjects.ParseValue(BotConvention.DATAFILE_JSON_STAT_RIFT_END, End);
                ClosedRifts.GoodRifts = dObjects.ParseValue(BotConvention.DATAFILE_JSON_STAT_RIFT_GOODCOUNT, ClosedRifts.GoodRifts);
                ClosedRifts.BadRifts = dObjects.ParseValue(BotConvention.DATAFILE_JSON_STAT_RIFT_BADCOUNT, ClosedRifts.BadRifts);

                _jToken = dObjects[BotConvention.DATAFILE_JSON_BUYORDERS];
                if (_jToken != null)
                {
                    Buys.Orders = _jToken.ParseValue(BotConvention.DATAFILE_JSON_STAT_RIFT_ORDERS, Buys.Orders);
                    Buys.TotalAmount = _jToken.ParseValue(BotConvention.DATAFILE_JSON_STAT_RIFT_AMOUNT, Buys.TotalAmount);
                    Buys.TotalPrice = _jToken.ParseValue(BotConvention.DATAFILE_JSON_STAT_RIFT_PRICE, Buys.TotalPrice);
                    Buys.TotalExchangePrice = _jToken.ParseValue(BotConvention.DATAFILE_JSON_STAT_RIFT_EXCHANGEPRICE, Buys.TotalExchangePrice);
                    Buys.TotalAverageRate = _jToken.ParseValue(BotConvention.DATAFILE_JSON_STAT_RIFT_AVERAGERATE, Buys.TotalAverageRate);
                }

                _jToken = dObjects[BotConvention.DATAFILE_JSON_SELLORDERS];
                if (_jToken != null)
                {
                    Sells.Orders = _jToken.ParseValue(BotConvention.DATAFILE_JSON_STAT_RIFT_ORDERS, Sells.Orders);
                    Sells.TotalAmount = _jToken.ParseValue(BotConvention.DATAFILE_JSON_STAT_RIFT_AMOUNT, Sells.TotalAmount);
                    Sells.TotalPrice = _jToken.ParseValue(BotConvention.DATAFILE_JSON_STAT_RIFT_PRICE, Sells.TotalPrice);
                    Sells.TotalExchangePrice = _jToken.ParseValue(BotConvention.DATAFILE_JSON_STAT_RIFT_EXCHANGEPRICE, Sells.TotalExchangePrice);
                    Sells.TotalAverageRate = _jToken.ParseValue(BotConvention.DATAFILE_JSON_STAT_RIFT_AVERAGERATE, Sells.TotalAverageRate);
                }

                CalcOrderAvgRate();

                return (true);
            }
        }
        #endregion


        #region Report
        public void WriteLog(String sCode, BotLogProcess eProcess)
        {
            BotEnv.LogSystem.WriteLog(sCode, eProcess, String.Format("[Zusammenfassung] Profit: {0} {1}, Profitable Rifts: {2}/{3}, Käufe: {4}, Verkäufe: {5}", Prophit, ReferredPair.SymbolSec, ClosedRifts.GoodRifts, ClosedRifts.TotalRifts, Buys.Orders, Sells.Orders), BotLogType.ePlatformEvent, BotLogPriority.eHigh);
        }
        #endregion


        #region Helper
        private double GetAverageRate(double dCurAvgRate, double dCurAmount, BotBaseOrder oOrder)
        {
            if (dCurAvgRate == 0)
                return (oOrder.dRate);
            else
                // Durchschnittlichen Einkaufskurs (Gewichteter Mittelwert) berechnen:
                return (UtlMath.GetWeightedAverage(dCurAmount, Math.Abs(oOrder.FinalAmount), dCurAvgRate, oOrder.dRate));
        }
        private void CalcOrderAvgRate()
        {
            if ((Buys.Orders > 0) && (Sells.Orders > 0))
                // Durchschnittlichen Order-Kurs (Gewichteter Mittelwert) berechnen:
                TotalOrderAverageRate = UtlMath.GetWeightedAverage(Buys.TotalAmount, Sells.TotalAmount, Buys.TotalAverageRate, Sells.TotalAverageRate);
            else
                // Durchschnittlichen Ein- ODER Verkauf-Kurs anwenden:
                TotalOrderAverageRate = Math.Max(Buys.TotalAverageRate, Sells.TotalAverageRate);


            // (BETA) ... weiter machen
            // wie wird hier der durchschnittskurs gebildet!?!
            // es ist die diskussion gemeint ob ein gemeinsamer durchschnittskurs für kauf und verkauf orders gemacht werden soll
        }
        #endregion
    }


    // Automatisches Management für Trading-Bots:
    public static class BotAutomatic
    {
        #region Enumerations
        public enum BotMode
        {
            [Description("Parallel")]
            eParallel,
            [Description("Erster Trade gewinnt")]
            eFirstWins
        }
        #endregion


        #region Types
        // Parameter für automatische Steuerung von TradingBots.
        public class BotAutomaticParam
        {
            public BotAutomaticParam()
            {
                Cfg_Mode = BotMode.eParallel;
                CfgPltf_MaxActiveStrategies = 3;
                CfgPltf_MinTotalBaseCurrencyFunds = 10.0;
                CfgCoin_MinMarketCap = 100000000;
                CfgCoin_MinPercentChange = 4;
            }


            #region Properties.Config
            [Category(BotConvention.PROP_CATEGORY_MANAGEMENT)]
            [DisplayName("Aktiviert")]
            [Description("Gibt das automatische De-/Aktivieren einzelner Pärchen (Trading-Bot Automatik) abhängig von aktuellen Trendauswertungen.")]
            [TypeConverter(typeof(CnvBooleanLabel.NoYes))]
            public bool Cfg_Enabled
            {
                get { return (bCfgEnabled); }
                set
                {
                    bCfgEnabled = value;
                    if (value)
                        BotAutomatic.Run();
                }
            }
            private bool bCfgEnabled = false;
            [Category(BotConvention.PROP_CATEGORY_MANAGEMENT)]
            [DisplayName("Modus")]
            [Description("Anzuwendenden Modus zum de-/aktivieren von einzelnen Pärchen.")]
            [TypeConverter(typeof(CnvEnumDescription))]
            [ReadOnly(true)]
            public BotMode Cfg_Mode { set; get; }
            #endregion
            #region Properties.Config.Exchange
            [Category(BotConvention.PROP_CATEGORY_PLATFORM)]
            [DisplayName("Max. aktive Pärchen")]
            [Description("Definiert die maximale Anzahl an zu aktivieren Pärchen (pro Platform).")]
            public int CfgPltf_MaxActiveStrategies { set; get; }
            [Category(BotConvention.PROP_CATEGORY_PLATFORM)]
            [DisplayName("Min. Gesamt-Guthaben")]
            [Description("Definiert das minimale Gesamt-Guthaben (Anteil in [%]) vom Gesamtguthaben (umgerechnet in [$]) welches zur Aktivierung notwendig ist.")]
            public double CfgPltf_MinTotalBaseCurrencyFunds { set; get; }
            #endregion
            #region Properties.Config.Coin
            [Category(BotConvention.PROP_CATEGORY_CURRENCY)]
            [DisplayName("Min. Marktkapitalisierung")]
            [Description("Definiert die minimale Marktkapitalisierung (in [$]) einer Währung die zur Aktivierung notwendig ist.")]
            public double CfgCoin_MinMarketCap { set; get; }
            [Category(BotConvention.PROP_CATEGORY_CURRENCY)]
            [DisplayName("Min. Änderung")]
            [Description("Definiert die minimale Änderung (in [%]) einer Währung die zur Aktivierung notwendig ist.")]
            public double CfgCoin_MinPercentChange { set; get; }
            #endregion


            #region Configuration
            public virtual void WriteFile(JsonWriter jWriter)
            {
                jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_BOTAUTOMATIC);
                jWriter.WriteStartObject();
                {
                    jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_BOTAUTOMATICPARAM_MGMT_ENABLED);
                    jWriter.WriteValue(bCfgEnabled);
                    jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_BOTAUTOMATICPARAM_MGMT_MODE);
                    jWriter.WriteValue(Cfg_Mode);
                    jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_BOTAUTOMATICPARAM_PLTF_MAXACTIVESTRATEGIES);
                    jWriter.WriteValue(CfgPltf_MaxActiveStrategies);
                    jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_BOTAUTOMATICPARAM_PLTF_MINTOTALBASECURRENCYFUNDS);
                    jWriter.WriteValue(CfgPltf_MinTotalBaseCurrencyFunds);
                    jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_BOTAUTOMATICPARAM_COIN_MINMARKETCAP);
                    jWriter.WriteValue(CfgCoin_MinMarketCap);
                    jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_BOTAUTOMATICPARAM_COIN_MINPERCENTCHANGE);
                    jWriter.WriteValue(CfgCoin_MinPercentChange);

                    jWriter.WriteEndObject();
                }
            }
            public virtual bool ReadFile(JObject dObjects)
            {
                bool bResult = true;

                JToken _jToken = dObjects[BotConvention.DATAFILE_JSON_BOTAUTOMATIC];
                if ((bResult) && (_jToken != null))
                {
                    bCfgEnabled = _jToken.ParseValue(BotConvention.DATAFILE_JSON_BOTAUTOMATICPARAM_MGMT_ENABLED, bCfgEnabled);
                    Cfg_Mode = _jToken.ParseValue(BotConvention.DATAFILE_JSON_BOTAUTOMATICPARAM_MGMT_MODE, Cfg_Mode);
                    CfgPltf_MaxActiveStrategies = _jToken.ParseValue(BotConvention.DATAFILE_JSON_BOTAUTOMATICPARAM_PLTF_MAXACTIVESTRATEGIES, CfgPltf_MaxActiveStrategies);
                    CfgPltf_MinTotalBaseCurrencyFunds = _jToken.ParseValue(BotConvention.DATAFILE_JSON_BOTAUTOMATICPARAM_PLTF_MINTOTALBASECURRENCYFUNDS, CfgPltf_MinTotalBaseCurrencyFunds);
                    CfgCoin_MinMarketCap = _jToken.ParseValue(BotConvention.DATAFILE_JSON_BOTAUTOMATICPARAM_COIN_MINMARKETCAP, CfgCoin_MinMarketCap);
                    CfgCoin_MinPercentChange = _jToken.ParseValue(BotConvention.DATAFILE_JSON_BOTAUTOMATICPARAM_COIN_MINPERCENTCHANGE, CfgCoin_MinPercentChange);
                }
                else
                    bResult = false;
                return (bResult);
            }
            #endregion
        }
        #endregion


        static BotAutomatic()
        {
            Parameter = new BotAutomaticParam();
        }


        #region Properties.Management
        [Browsable(false)]
        public static BotAutomaticParam Parameter { private set; get; }
        [Browsable(false)]
        public static bool IsEnabled { get { return (Parameter.Cfg_Enabled); } }
        #endregion


        #region Management
        // Fordert das Ausführen der Management-Funktion (ggf. nach Verzögerung) aus.
        public static bool Run()
        {
            if (IsEnabled)
            {
                if (tRetryRunTimer == null)
                    // Timer initialisieren:
                    tRetryRunTimer = UtlTimer.CreateTimer((oSender, eArgs) => OnTimer_RetryRun(oSender, eArgs));
                else
                    // Ggf aktiven Timer deaktivieren:
                    UtlTimer.ModifyTimer(tRetryRunTimer, 1, false);

                if (BotExcelDocThread.GetActiveThreads() == 0)
                    // Management-Funktion ausführen:
                    AutomaticManageBots();
                else
                    // Timer 
                    UtlTimer.ModifyTimer(tRetryRunTimer, 1000);

                return (true);
            }
            else
                return (false);
        }


        private static void OnTimer_RetryRun(object oSender, ElapsedEventArgs eArgs)
        {
            Run();
        }
        // Automatische Management-Fuktion für Trading-Bots.
        private static void AutomaticManageBots()
        {
            if (Parameter.Cfg_Enabled)
            {
                BotTradingStrategy _bStrategy;
                List<BotTradingStrategy> lUsable = new List<BotTradingStrategy>();
                List<BotTradingStrategy> lEnabled = new List<BotTradingStrategy>();
                BotWallet wBaseCurrencyWallet;
                IBotCoinInfo bPrimInfo;
                BotCurrencyRating.BotCoinInfoList cCoinInfo = BotCurrencyRating.CoinInfo;
                BotPlatformService pPlatform;
                int iRemaining, iEnabled, iActiveRifts;

                // Automatische Management-Fuktion für einzelne Platformen separat ausführen:
                foreach (BotTradeBot _bBot in BotTradeBot.TradeBots)
                {
                    pPlatform = _bBot.ReferredPlatform;
                    wBaseCurrencyWallet = pPlatform.GetWallet(BotConvention.TRADE_BASE_CURRENCY, BotWalletType.eExchange);

                    if ((wBaseCurrencyWallet != null) && (pPlatform.IsUsableByBotAutomatik))
                    {
                        // Bedingungen (für Platform) prüfen:
                        if (wBaseCurrencyWallet.Funds >= (pPlatform.TotalBaseCurrencyFunds * (Parameter.CfgPltf_MinTotalBaseCurrencyFunds / 100)))
                        {
                            // Informationen sammeln.
                            // > Höher bewertete Strategien priorisiert (aufsteigend):
                            iActiveRifts = 0;
                            iEnabled = 0;
                            iRemaining = _bBot.Strategies.Count;

                            for (int i = 0; i < cCoinInfo.Count; i++)
                            {
                                _bStrategy = _bBot.FindStrategy(cCoinInfo[i].Key, BotConvention.TRADE_BASE_CURRENCY);

                                if (_bStrategy != null)
                                {
                                    if (_bStrategy.Enabled)
                                        iEnabled++;

                                    // Bedingungen (für Strategie) prüfen:
                                    if (_bStrategy.IsRiftActive)
                                        iActiveRifts++;

                                    else if ((_bStrategy.IsUsableByBotAutomatik) && (_bStrategy.TradingPair.CurrencySec == BotConvention.TRADE_BASE_CURRENCY))
                                    {
                                        bPrimInfo = cCoinInfo.Find(_bStrategy.TradingPair.CurrencyPrim);

                                        if ((_bStrategy.Enabled) && (_bStrategy.CanBeDisabledByAutomatic))
                                            // Strategie kann bei Bedarf deaktiviert werden:
                                            lUsable.Add(_bStrategy);

                                        else if (((_bStrategy.Enabled == false) && (_bStrategy.IsAvailable)) &&
                                            (bPrimInfo != null) &&
                                            (bPrimInfo.PercentChange[BotCurrencyRating.PERCENT_CHANGE_RATING] >= Parameter.CfgCoin_MinPercentChange) &&
                                            (bPrimInfo.MarketCap >= Parameter.CfgCoin_MinMarketCap))
                                        {
                                            // Strategie kann bei Bedarf aktiviert werden:
                                            lUsable.Add(_bStrategy);
                                        }
                                    }

                                    if ((--iRemaining) == 0)
                                        // Maximale Anzahl an Strategien bereits ermittelt.
                                        // > Fortfahren des Vorgangs währe sinnlos.
                                        break;
                                }
                            }

                            // Bedingungen (für Platform) auswerten:
                            int iMax = lUsable.Count;

                            iRemaining = (Parameter.CfgPltf_MaxActiveStrategies - iEnabled);

                            // So viele deaktivierte Strategien (die eine höhere Bewertung haben als passive Strategien) aktivieren wie möglich:
                            for (int i = 0; i < iMax; i++)
                            {
                                _bStrategy = lUsable[i];

                                if (_bStrategy.Enabled == false)
                                {
                                    for (int u = (lUsable.Count - 1); u > i; u--)
                                    {
                                        if (iRemaining > 0)
                                            // Deaktivieren passiver Strategien nicht (mehr) notwendig:
                                            break;

                                        else if (lUsable[u].Enabled)
                                        {
                                            EnableStrategy(lUsable[u], false);

                                            iMax = u;
                                            iRemaining++;
                                        }
                                    }

                                    if (iRemaining > 0)
                                    {
                                        if (EnableStrategy(_bStrategy))
                                        {
                                            lEnabled.Add(_bStrategy);
                                            iRemaining--;
                                        }
                                    }
                                    else
                                        // Keine weiteren passiven Strategien zum deaktivieren vorhanden
                                        // > Vorgang beenden:
                                        break;
                                }
                            }

                            // Strategien deaktivieren wenn zuviele Strategien (z.B. durch Benutzer) aktiviert sind:
                            for (int i = (lUsable.Count - 1); i >= 0; i--)
                            {
                                _bStrategy = lUsable[i];

                                if ((iRemaining < 0) && (_bStrategy.Enabled))
                                {
                                    EnableStrategy(_bStrategy, false);
                                    iRemaining++;
                                }
                            }

                            if (_bBot.IsActive())
                            {
                                // Trading-Bot reinitialisieren:
                                _bBot.ReInit();

                                // Aktivierte Instanzen starten:
                                for (int i = 0; i < lEnabled.Count; i++)
                                    lEnabled[i].Init(true);
                            }

                            lUsable.Clear();
                            lEnabled.Clear();
                        }
                    }
                }

                lUsable = null;
                lEnabled = null;
            }
        }
        #endregion


        #region Helper
        private static bool EnableStrategy(BotTradingStrategy bStrategy, bool bEnable = true)
        {
            if (bStrategy.Enable(bEnable, false))
            {
                String sOperation = (bEnable ? "aktiviert" : "deaktiviert");

                BotEnv.LogSystem.WriteLog("ES2772", BotLogProcess.eBotAutomatic, String.Format("Pärchen '{0}' ({1}) {2}.", bStrategy.TradingPair.Symbol, bStrategy.Platform.ServiceDescriptor.ServiceName, sOperation), BotLogType.eEvent, BotLogPriority.eLow);
                return (true);
            }
            else
                return (false);
        }
        #endregion


        private static System.Timers.Timer tRetryRunTimer;
    }


    // Trading-Bot:
    public class BotTradeBot : BotPlatformServiceOwner
    {
        private BotTradeBot(BotPlatformService bService)
        {
            Debug.Assert((bService != null), "[TB1883]");
            Debug.Assert((BotEnv.IsValid), "[TB1884]");

            this.bStrategies = new List<BotTradingStrategy>();
            this.bActive = false;
            this.ReferredPlatform = bService;

            Debug.Assert(IsValid(), "[TB63]");
        }
        ~BotTradeBot()
        {
            Activate(false);

            bStrategies.Clear();
        }


        #region Properties.Management
        [Browsable(false)]
        public static bool Active
        {
            get { return (BotTradeBot.TradeBots.FirstOrDefault(_tBot => _tBot.IsActive()) != null); }
            set
            {
                if (value)
                {
                    bool bResult = true;

                    // Sämtliche Parameter auf Gültigkeit prüfen:
                    foreach (BotBaseParameters _bParam in BotParameterManager.Content)
                    {
                        if (_bParam.CheckPlausibility() == false)
                            bResult = false;
                    }

                    if (bResult)
                    {
                        BotTradingPair.RefreshActivePairs();

                        // Currency-Rating aktualisieren:
                        BotCurrencyRating.UpdateRating();

                        BotAutomatic.Run();
                    }
                    else
                        return;
                }

                BotTradeBot.lTradeBots.ForEach(tBot => tBot.Activate(value));

                // Prüfen ob Trading-Bot aktiviert werden konnte:
                if (Active == value)
                    // Applikation benachrichtigen:
                    BotEnv.Send(() => BotEnv.OnBotStateChanged?.Invoke(value));
            }
        }

        [Browsable(false)]
        public static IReadOnlyList<BotTradeBot> TradeBots { get { return (lTradeBots); } }
        private static List<BotTradeBot> lTradeBots;

        [Browsable(false)]
        public IReadOnlyList<BotTradingStrategy> Strategies { get { return (bStrategies); } }
        private List<BotTradingStrategy> bStrategies;
        #endregion
        #region Properties.References
        [Browsable(false)]
        public BotPlatformService ReferredPlatform { private set; get; }
        #endregion


        #region Interface.Management
        public override void OnSubscribed(BotBaseSubscription bSubscription)
        {
            // (BETA) ...
            // LogSystem.WriteLog("OS17", BotLogProcess.eSubscription, ("NAME=" + bSubscription.GetName() + ", TYPE=" + bSubscription.GetSubscriptionType().ToString()), LogSystem.LogType.LT_PlatformEvent);
        }
        public override BotTaskResult OnSubscriptionData(BotSubscriptionPurpose sPurpose, BotBaseSubscription bSubscription)
        {
            BotTaskResult tResult = BotTaskResult.eError;

            if (bSubscription.GetType().IsSubclassOf(typeof(BotBasePairSubscription)))
            {
                BotTradingStrategy bStrategy = FindStrategy(((BotBasePairSubscription)bSubscription).ReferredPair);
                if (bStrategy == null)
                    Debug.Assert(false, "[OSD130]");
                else
                {
                    BotTradingPair tRefPair = bStrategy.TradingPair;
                    bool bIsPlaying = false;

                    if (bStrategy.Platform.IsSimulating)
                    {
                        IBotSimulatedPlatformService iSimulation = (IBotSimulatedPlatformService)bStrategy.Platform;

                        bIsPlaying = (iSimulation.CoinStreamPlayer != null);
                        if ((bIsPlaying) && (bStrategy.CoinStreamPlayer.IsBeginOfFile))
                        {
                            // Aufnahme wird gestartet:
                            // Rift komplett zurücksetzen.
                            bStrategy.ResetCalc();
                            iSimulation.ResetWallet();
                        }
                    }

                    switch (bSubscription.GetSubscriptionType())
                    {
                        case BotSubscriptionType.eCandles:
                            if (sPurpose == BotSubscriptionPurpose.eCoinStream)
                            {
                                if (bIsPlaying)
                                    bStrategy.RecalcStatistics(true);
                            }
                            else
                            {
                                // (BETA) ...
                                // Keine weiteren Operationen geplant.

                                // Abonnement abbestellen (da Candle-Sticks nur einmalig pro Kalkulation verwendet werden):
                                ReferredPlatform.Unsubscribe(bSubscription.GetSubscriptionID());
                            }

                            tResult = BotTaskResult.eSuccess;
                            break;

                        case BotSubscriptionType.eTicker:
                            if ((bIsPlaying) == (sPurpose == BotSubscriptionPurpose.eCoinStream))
                            {
                                BotTickerSubscription tTicker = (BotTickerSubscription)bSubscription;

                                // Aktuellsten Preis speichern:
                                tRefPair.LastRateValue = tTicker.lData.Last().dLastPrice;

                                lock (BotThreadLock.Object[BotThreadLock.OBJ_CURRENCY_RATING])
                                {
                                    // Ggf. Currency-Rating aktualisieren:
                                    BotCurrencyRating.UpdateCandles(sPurpose, tTicker, bStrategy);

                                    if (tRefPair.LastRateValue == 0)
                                        // Socket kann inzwischen getrennt worden sein (Siehe 'wSocket.OnClose()').
                                        // > Vorgang abbrechen:
                                        return (BotTaskResult.eSuccess);
                                    else
                                        // Zyklus auswerten:
                                        tResult = bStrategy.EvaluateCycle(tTicker);
                                }

                                if (bStrategy.GetState() == BotTradingStrategy.BotState.eError)
                                    // Auswertung fehlgeschlagen.
                                    // > Trade-Bot deaktvieren:
                                    Activate(false);
                                else
                                {
                                    switch (tResult)
                                    {
                                        case BotTaskResult.eBusy:
                                        case BotTaskResult.eError:
                                            // Nicht weiter unternehmen an dieser Stelle.
                                            break;

                                        case BotTaskResult.eSuccess:
                                            BotEnv.Post(() => BotEnv.OnTickerUpdate?.Invoke(bStrategy, tTicker));
                                            break;

                                        default:
                                            Debug.Assert(false, "[OSD3520] Ungültiger Typ!");
                                            break;
                                    }
                                }
                            }

                            break;
                    }
                }
            }

            return (tResult);
        }

        public override void OnOrderCreated(BotBaseOrder oOrder)
        {
            oOrder.WriteLog(BotOrderResult.eCreated);
        }
        public override void OnOrderClosed(BotBaseOrder oOrder, BotOrderResult oResult)
        {
            BotTradingStrategy bStrategy = FindStrategy(oOrder.tRefPair);
            if (bStrategy == null)
                Debug.Assert(false, "[OOF683]");
            else
                bStrategy.OnOrderClosed(oOrder, oResult);
        }

        public override void OnWalletUpdated(BotWallet wWallet)
        {
            // LogSystem.WriteLog("OWU33", BotLogProcess.eWallet, (wWallet.Currency.ToString() + ", AMOUNT= " + wWallet.Amount), LogSystem.LogType.LT_PlatformEvent);
            BotEnv.Post(() => BotEnv.OnWalletUpdated?.Invoke(wWallet));
        }
        public override void OnMaintenance(bool bEnabled)
        {
            if (bEnabled)
            {
                // Aktive Strategien neu berechnen:
                for (int i = 0; i < bStrategies.Count; i++)
                {
                    if (bStrategies[i].Enabled)
                        bStrategies[i].RecalcStatistics();
                }
            }
        }

        public override BotTaskResult OnCoinStreamEvent(BotCoinStreamPlayer.BotEventType eType, BotBasePairSubscription bSubscription)
        {
            switch (eType)
            {
                case BotCoinStreamPlayer.BotEventType.eEventOccured:
                    return (OnSubscriptionData(BotSubscriptionPurpose.eCoinStream, bSubscription));

                case BotCoinStreamPlayer.BotEventType.eEndOfFile:
                    // Aktuelle Instanz beenden:
                    Active = false;

                    BotEnv.Post(() => BotEnv.OnRefreshView?.Invoke(false));
                    break;

                case BotCoinStreamPlayer.BotEventType.eError:
                    BotEnv.LogSystem.WriteLog("OEE786", BotLogProcess.eCoinStream, "Fehler während des Abspielens aufgetreten.", BotLogType.eError);
                    break;

                default:
                    Debug.Assert(false, "[OEE793] Ungültiger Typ!");
                    break;
            }

            return (BotTaskResult.eError);
        }
        #endregion
        #region Interface.Communication
        public override void OnSocketConnected()
        {
            // Verbindung aufgebaut.
            // > An Platform anmelden:
            if (ReferredPlatform.Login() == false)
                // Verbindung trennen:
                ReferredPlatform.Connection = false;
        }
        public override void OnSocketClosed()
        {
        }
        public override void OnSocketDisconnected()
        {
            Activate(false);
        }
        public override void OnLoggedIn()
        {
            for (int i = 0; i < bStrategies.Count; i++)
                bStrategies[i].Init();
        }
        public override void OnError(String sCode, String sMessage)
        {
            BotEnv.LogSystem.WriteLog("OOE24", BotLogProcess.eCommunication, string.Format("Server returned: {0} (Code '{1}')", sMessage, sCode));
        }
        #endregion




        #region Configuration
        public void WriteFile(JsonWriter jWriter, BotContentType bContent)
        {
            jWriter.WriteStartObject();
            {
                jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_PLATFORM);
                jWriter.WriteStartObject();
                {
                    // Platform speichern:
                    ReferredPlatform.WriteFile(jWriter, bContent);

                    // Strategien speichern:
                    jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_STRATEGIES);
                    jWriter.WriteStartArray();
                    {
                        for (int i = 0; i < bStrategies.Count; i++)
                            bStrategies[i].WriteFile(jWriter, bContent);

                        jWriter.WriteEnd();
                    }

                    jWriter.WriteEndObject();
                }
            }
        }
        public bool ReadFile(dynamic dObjects, BotContentType bContent)
        {
            // Platform lesen:
            bool bResult = ReferredPlatform.ReadFile(dObjects[BotConvention.DATAFILE_JSON_PLATFORM], bContent);
            if (bResult)
            {
                dynamic dObjStrategies = dObjects[BotConvention.DATAFILE_JSON_STRATEGIES];
                if ((bResult) && (dObjStrategies != null) && (dObjStrategies.Count > 0))
                {
                    BotTradingStrategy _bStrategy;
                    String _sPair;

                    // Einzelne Strategien auslesen:
                    for (int i = 0; i < dObjStrategies.Count; i++)
                    {
                        _sPair = dObjStrategies[i][BotConvention.DATAFILE_JSON_PAIR];
                        _bStrategy = bStrategies.Find(obj => ((BotTradingStrategy)obj).TradingPair.Symbol == _sPair);

                        if (_bStrategy == null)
                            continue;
                        else if (_bStrategy.ReadFile(dObjStrategies[i], bContent) == false)
                        {
                            bResult = false;
                            break;
                        }
                    }
                }
                else
                    bResult = false;
            }
            return (bResult);
        }
        #endregion


        #region Initialization
        internal static void Init()
        {
            lTradeBots = new List<BotTradeBot>();
        }
        internal static bool RegisterTradeBot(BotPlatformService bService)
        {
            BotTradeBot _tBot = new BotTradeBot(bService);
            if (lTradeBots.FirstOrDefault(_tBot => (_tBot.ReferredPlatform.PlatformDomain == bService.ServiceDescriptor.ServiceDomain)) == null)
            {
                if (bService.ActivateService(_tBot))
                {
                    lTradeBots.Add(_tBot);

                    _tBot.Initialize();
                    return (true);
                }
                else
                    return (false);
            }
            else
                throw new InvalidOperationException(string.Format("Failed to register Trade-Bot: Exchange platform '{0}' already in use!", _tBot.ReferredPlatform.PlatformName));
        }
        #endregion
        #region Management
        public bool IsValid()
        {
            return ((BotEnv.IsValid) && (ReferredPlatform != null));
        }
        public bool IsActive()
        {
            return (bActive);
        }
        public bool Activate(bool bActivate = true)
        {
            bool bOldActive = bActive;

            if (bActivate)
            {
                ApplySettingsToActiveParameters();

                // Parameter verfügbarer Strategien auswerten um korrekte Settings zu aktivieren:
                BotTradeBot.ValidateActiveParameters();
            }

            // Initialisierung:
            bActive = ((bActivate) && (Initialize()));

            if (bActive == bActivate)
            {
                if (bOldActive != bActive)
                {
                    BotEnv.LogSystem.WriteLog("A549", BotLogProcess.eBot, String.Format("Betrieb von '{0}' {1}", ReferredPlatform.PlatformName, (bActive ? "gestartet" : "beendet")), BotLogType.eInfo, BotLogPriority.eLow);

                    if (bActive)
                    {
                        BotCoinStreamPlayer cPlayer = ReferredPlatform.CoinStreamPlayer;
                        if (cPlayer != null)
                            cPlayer.Start();
                    }
                    else
                        // Strategien beenden:
                        bStrategies.ForEach(tStrategy => tStrategy.Stop());

                    // Verbindung aufbauen / trenen:
                    ReferredPlatform.Connection = bActive;
                }

                return (true);
            }
            else
                return (false);
        }
        public BotTradingStrategy FindStrategy(BotCoinType cPrimCurrency, BotCoinType cSecCurrency)
        {
            for (int i = 0; i < bStrategies.Count; i++)
            {
                if (bStrategies[i].TradingPair.Compare(cPrimCurrency, cSecCurrency))
                    return (bStrategies[i]);
            }
            return (null);
        }
        public BotTradingStrategy FindStrategy(BotTradingPair tPair)
        {
            if (tPair == null)
                return (null);
            else
                return (FindStrategy(tPair.CurrencyPrim, tPair.CurrencySec));
        }

        // Führt eine Anweisung für sämtliche Strategien durch.
        public void ForeachStrategy(Action<BotTradingStrategy> aPredicate)
        {
            foreach (BotTradingStrategy tStrategy in bStrategies)
                aPredicate(tStrategy);
        }
        public bool ForeachStrategy(Func<BotTradingStrategy, bool> aPredicate)
        {
            bool bResult = true;

            foreach (BotTradingStrategy tStrategy in bStrategies)
            {
                if (aPredicate(tStrategy) == false)
                    bResult = false;
            }

            return (bResult);
        }

        public bool Initialize()
        {
            if (!BotServiceManager.IsReady)
                return (false);
            else if (bStrategies.Count == 0)
            {
                // Erstmaliges Initialisieren.

                // Verfügbare Strategien hinzufügen:
                // > Bisher werden nur 'Exchange'-Konten unterstüzt!
                // > Initialisierung sämtlicher Währungen mit der Standard-Strategie 'TStratStairs'.
                //   Das Umstellen auf weitere Strategien ist zur Laufzeit durch den Benutzer möglich!
                for (int i = 0; i < ReferredPlatform.PairCount; i++)
                    AddStrategy(ReferredPlatform.GetPair(i), BotWalletType.eExchange);

                // Liste sortieren:
                bStrategies.Sort((tStrategy1, tStrategy2) =>
                {
                    return (tStrategy1.TradingPair.Symbol.CompareTo(tStrategy2.TradingPair.Symbol));
                });

                return (true);
            }
            else
            {
                bool bResult = false;

                // Gewichtung ermitteln:
                Array aCoinVals = Enum.GetValues(typeof(BotCoinType));
                var dTypeWeight = new Dictionary<BotCoinType, int>(aCoinVals.Length);
                List<BotTradingStrategy> lActiveStrategy = new List<BotTradingStrategy>();
                double _dWeight;

                foreach (BotCoinType _bType in aCoinVals)
                {
                    if (_bType != BotCoinType.Invalid)
                    {
                        dTypeWeight[_bType] = 0;
                        foreach (BotTradingStrategy tTemp in bStrategies)
                        {
                            if ((tTemp.Enabled) && (_bType == tTemp.TradingPair.CurrencySec))
                            {
                                bResult = true;
                                if (tTemp.ActiveSettingsClone != null)
                                {
                                    lActiveStrategy.Add(tTemp);

                                    dTypeWeight[_bType] += tTemp.ActiveSettingsClone.CfgBuy_Weight;
                                }
                            }
                        }
                    }
                }

                foreach (BotTradingStrategy tTemp in lActiveStrategy)
                {
                    // Gewichtungs-Faktor bestimmen:
                    _dWeight = (double)dTypeWeight[tTemp.TradingPair.CurrencySec];

                    if (tTemp.ActiveSettingsClone.CfgBuy_Weight == 0)
                        // Gewichtung nicht berücksichtigen:
                        tTemp.dCalc_Buy_WeightFactor = 1;
                    else
                        tTemp.dCalc_Buy_WeightFactor = (1 / (_dWeight / (double)tTemp.ActiveSettingsClone.CfgBuy_Weight));

                    // Ggf. Rift initialisieren (falls nicht bereits einRift aktiv ist):
                    if (tTemp.IsRiftActive == false)
                        tTemp.ResetRift();
                }

                // Ggf. Web-Requests initialisieren:
                InitWebRequests();

                return (bResult);
            }
        }
        public bool ReInit()
        {
            if (BotTradeBot.Active)
                BotTradingPair.RefreshActivePairs();

            if (IsActive())
            {
                if (Initialize())
                    return (true);
                else
                    Activate(false);
            }

            return (false);
        }


        private bool AddStrategy(BotTradingPair tPair, BotWalletType wWalletType)
        {
            BotTradingStrategy bTranslationStrategy = null;
            BotWallet wWallet1, wWallet2;
            String sStratName = String.Format("'{0}' ({1})", ((tPair == null) ? "Unbekannt" : tPair.Symbol), ReferredPlatform.PlatformName);

            Debug.Assert((tPair != null), "[AS124]");

            // (BETA) ...
            Debug.Assert((wWalletType == BotWalletType.eExchange), "[AS152] Zur Zeit ist nur der Wallet-Typ 'WT_Exchange' zu verwenden!");

            wWallet1 = ReferredPlatform.GetWallet(tPair.CurrencyPrim, wWalletType);
            wWallet2 = ReferredPlatform.GetWallet(tPair.CurrencySec, wWalletType);

            if (tPair.CurrencySec != BotConvention.TRADE_BASE_CURRENCY)
            {
                // Übersetzungs-Strategie ermitteln:
                bTranslationStrategy = FindStrategy(ReferredPlatform.FindPair(tPair.CurrencySec, BotConvention.TRADE_BASE_CURRENCY));

                if (bTranslationStrategy == null)
                {
                    BotEnv.LogSystem.WriteLog("AS669", BotLogProcess.eStrategy, ("Währung zum Übersetzen von Beträgen für Strategie '" + sStratName + "' nicht gefunden!"), BotLogType.eError);
                    return (false);
                }
            }

            // Strategie-Objekt erzeugen:
            BotTradingStrategy bStrategy = new BotTradingStrategy(this, tPair, wWallet1, wWallet2, bTranslationStrategy);

            if (bStrategy.IsValid)
            {
                bStrategy.Enabled = false;

                bStrategies.Add(bStrategy);
                return (true);
            }
            else
            {
                Debug.Assert(false, string.Format("[AS688] Initialisierung für Trading-Strategie '{0}' fehlgeschlagen!", sStratName));

                bStrategy = null;
                GC.Collect();

                return (false);
            }
        }
        #endregion
        #region Management.Parameters
        // Parameter auswerten um korrekte Settings zu aktivieren.
        public static void ValidateActiveParameters()
        {
            BotTradeBot.ForeachPlatformsStrategy(tStrategy => tStrategy.ValidateActiveParameters());
        }
        // Parameter in angewendeten Settings übernehmen.
        public static void ApplySettingsToActiveParameters(bool bPendingChangesOnly = false)
        {
            // Parameter übernehmen:
            BotTradeBot.ForeachPlatformsStrategy(tStrategy => tStrategy.ApplyParameters(bPendingChangesOnly));

            // Änderungen an sämtlichen Parametern zurücksetzen:
            BotParameterManager.Content.ResetPendingChanges();
        }
        #endregion
        #region Management.WebRequests
        public static void InitWebRequests()
        {
            // Von referenzierter Platform unterstüzte Trading-pairs ermitteln:
            foreach (BotTradeBot lTradeBots in BotTradeBot.TradeBots)
                lTradeBots.ReferredPlatform.InitWebRequests();
        }
        public static bool ExecuteWebRequests()
        {
            bool bResult = false;
            for (int i = 0; i < BotTradeBot.TradeBots.Count; i++)
            {
                // Subscription Web-Requests:
                for (int t = 0; t < BotTradeBot.TradeBots[i].ReferredPlatform.SubscriptionWebRequests.Count; t++)
                {
                    if (BotTradeBot.TradeBots[i].ReferredPlatform.ExecuteWebRequest(t))
                        bResult = true;
                }
            }
            return (bResult);
        }
        public static void UseSubscriptionWebRequests()
        {
            for (int i = 0; i < BotTradeBot.TradeBots.Count; i++)
            {
                // Subscription Web-Requests:
                foreach (BotSubscriptionWebRequest _bReq in BotTradeBot.TradeBots[i].ReferredPlatform.SubscriptionWebRequests)
                {
                    if (_bReq.IsEvaluable)
                    {
                        // Subscription Web-Requests von Trade-Bots anwenden lassen:
                        for (int u = 0; u < BotTradeBot.TradeBots.Count; u++)
                            BotTradeBot.TradeBots[u].ReferredPlatform.UseSubscriptionWebRequest(_bReq);
                        _bReq.IsEvaluated = true;
                    }
                }
            }
        }
        #endregion
        #region Management.Platform
        public static BotTradeBot FindPlatform(String sPltfDomain)
        {
            return (TradeBots.FirstOrDefault(_tBot => (_tBot.ReferredPlatform.PlatformDomain == sPltfDomain)));
        }

        // Führt eine Anweisung für sämtliche Strategien aller Plattformen durch.
        public static void ForeachPlatformsStrategy(Action<BotTradingStrategy> aPredicate)
        {
            foreach (BotTradeBot _tBot in TradeBots)
                _tBot.ForeachStrategy(aPredicate);
        }
        public static bool ForeachPlatformsStrategy(Func<BotTradingStrategy, bool> aPredicate)
        {
            bool bResult = true;
            foreach (BotTradeBot _tBot in TradeBots)
            {
                if (_tBot.ForeachStrategy(aPredicate) == false)
                    bResult = false;
            }
            return (bResult);
        }
        #endregion

        private bool bActive;
    }
    #endregion
}
