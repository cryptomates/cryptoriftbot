using CryptoRiftBot.Config;
using CryptoRiftBot.Trading;
using CryptoRiftBot.Trading.Functions;
using CryptoRiftBot.Trading.Services;
using CryptoRiftBot.Trading.Strategy;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CryptoRiftBot
{
    #region Types
    public static class BotEnv
    {
        #region Delegates.Log
        public delegate void WriteLogEventHandler(BotLogEntry bEntry);
        public delegate void RemoveLogEventHandler(object oTag);
        #endregion
        #region Delegates.User
        public delegate bool EnterPasswordEventHandler(string sTitle, string sMessage, out String sResult);
        public delegate bool OpenFileDialogEventHandler(string sTitle, string sDefaultExt, string sFilter, bool bCheckPathExists, string sInitialPath, out String sFilePath);
        public delegate bool SaveFileDialogEventHandler(string sTitle, string sDefaultExt, string sFilter, string sInitialPath, out String sFilePath, params object[] oParams);
        public delegate bool SaveFileDialogExEventHandler(string sTitle, string sDefaultExt, string sFilter, string sInitialPath, out String sFilePath, out object[] oResult, params object[] oParams);
        #endregion
        #region Delegates.TaskState
        public delegate void TaskStateChangedEventHandler();
        #endregion
        #region Delegates.Display
        public delegate void RefreshViewEventHandler(bool bSaveConfig = true);
        public delegate void TickerUpdateEventHandler(BotTradingStrategy bStrategy, BotTickerSubscription tTicker);
        #endregion
        #region Delegates.Service
        public delegate void ServiceStateChangedEventHandler(IBotBaseService iService, BotServiceState bOldState);
        #endregion
        #region Delegates.Trading
        public delegate void BotStateChangeEventHandler(bool bActive);
        public delegate void RatingUpdateFinishEventHandler(bool bResult);
        public delegate void WalletUpdateEventHandler(BotWallet wWallet = null);
        public delegate void OrderFinishEventHandler(BotTradingStrategy bStrategy, BotBaseOrder bOrder);
        public delegate void RiftCloseEventHandler(BotTradingStrategy bStrategy, BotRiftSummary bSummary);
        public delegate void RecalcStatistikEventHandler(BotTradingStrategy bStrategy);
        #endregion


        #region Config
        public static bool SaveConfig(BotContentType bContent)
        {
            return (BotConfigFile.WriteFile(bContent));
        }
        #endregion
        #region Management
        public static void Init(string sTitle)
        {
            Init(sTitle, typeof(BotConfig));
        }
        public static bool Init(string sTitle, Type tConfigType)
        {
            Title = sTitle;
            sContext = SynchronizationContext.Current;

            // Initialize components:
            LogSystem = new BotLogSystem();

            BotConfigFile.Init(tConfigType);
            BotThreadLock.Init();
            BotTaskState.Init();
            BotTradeBot.Init();
            if (BotServiceManager.Init())
            {
                // Read configuration:
                BotConfigFile.ReadFile(BotContentType.eConfig);
                BotConfigFile.ReadFile(BotContentType.eRiftSummary);

                return (true);
            }
            else
                return (false);
        }
        public static void Exit()
        {
            LogSystem = null;
            sContext = null;

            GC.Collect();
        }
        /// <summary>
        /// Posts a asynchroneous message to the environments synchronization context.
        /// </summary>
        /// <param name="aAction"></param>
        public static void Post(Action aAction)
        {
            if (sContext == null)
                aAction();
            else
                sContext.Post((_sState) => aAction(), null);
        }
        /// <summary>
        /// Posts a synchroneous message to the environments synchronization context.
        /// </summary>
        /// <param name="aAction"></param>
        public static void Send(Action aAction)
        {
            if (sContext == null)
                aAction();
            else
                sContext.Send((_sState) => aAction(), null);
        }
        #endregion


        #region Properties.Management
        public static bool IsValid { get { return (LogSystem != null); } }
        public static IBotLogSystem LogSystem { private set; get; }
        #endregion
        #region Properties.Config
        public static BotConfig Configuration { get { return (BotConfigFile.Configuration); } }
        #endregion
        #region Properties
        public static string Title { private set; get; }
        #endregion


        #region Events.Log
        public static WriteLogEventHandler OnWriteLog;
        public static RemoveLogEventHandler OnRemoveLog;
        #endregion
        #region Events.User
        public static EnterPasswordEventHandler OnEnterPassword;
        public static OpenFileDialogEventHandler OnOpenFileDialog;
        public static SaveFileDialogEventHandler OnSaveFileDialog;
        public static SaveFileDialogExEventHandler OnSaveFileDialogEx;
        #endregion
        #region Events.TaskState
        public static TaskStateChangedEventHandler OnTaskStateChanged;
        #endregion
        #region Events.Display
        public static RefreshViewEventHandler OnRefreshView;
        public static TickerUpdateEventHandler OnTickerUpdate;
        #endregion
        #region Events.Service
        public static ServiceStateChangedEventHandler OnServiceStateChanged;
        #endregion
        #region Events.Trading
        public static BotStateChangeEventHandler OnBotStateChanged;
        public static RatingUpdateFinishEventHandler OnRatingUpdateFinished;
        public static WalletUpdateEventHandler OnWalletUpdated;
        public static OrderFinishEventHandler OnOrderFinished;
        public static RiftCloseEventHandler OnRiftClosed;
        public static RecalcStatistikEventHandler OnRecalcStatistik;
        #endregion


        private static SynchronizationContext sContext;
    }
    #endregion
}