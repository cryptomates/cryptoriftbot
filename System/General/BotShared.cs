﻿using CryptoRiftBot.Trading.Functions;
using CryptoRiftBot.Trading.Services;
using CryptoRiftBot.Trading.Strategy;
using CryptoRiftBot.Trading;
using Newtonsoft.Json;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using Shared.Utils.Core;

namespace CryptoRiftBot
{
    #region Enumerations.Non-Platform
    public enum BotTaskResult
    {
        eError,
        eBusy,
        eSuccess
    };
    #endregion


    #region Types
    public class BotConvention
    {
        #region Application
        public const String APP_TITLE = "Crypto Rift Bot";
        public const String APP_SHORTCUT = "CRB";
        #endregion


        #region General
        internal const String TITLE_SECURITY_QUESTION = "Sicherheitsabfrage";

        internal const String FILE_EXT = "crot";
        #endregion


        #region Numbers
        public const int NUM_SMALL_DOUBLE_DECIMALS = 4;
        #endregion
        #region Display
        public const int DISPLAY_MAX_TICKER_COUNT = 260;
        public const int DISPLAY_MAX_CANDLE_COUNT = 120;
        #endregion
        #region Colors
        public static Color COLOR_EXCLAMATION = Color.DarkOrange;
        public static Color COLOR_ERROR = Color.Crimson;

        public static Color COLOR_INACTIVE = SystemColors.ButtonShadow;
        public static Color COLOR_ACTIVE = SystemColors.ControlText;

        public static Color COLOR_UNKNOWN = Color.DarkSeaGreen;
        public static Color COLOR_ONLINE = Color.Green;
        public static Color COLOR_MAINTENANCE = Color.DarkMagenta;
        #endregion


        #region Properties
        internal const String PROP_PREFIX_STATISTICS = "";
        internal const String PROP_PREFIX_CALCULATION = "Calc_";
        internal const String PROP_PREFIX_CONFIGURATION = "Cfg_";
        internal const String PROP_PREFIX_MANAGEMENT = "";
        internal const String PROP_PREFIX_REFERENCES = "";
        internal const String PROP_PREFIX_DEBUG = "Dbg_";
        #endregion


        #region Trading
        /// <summary>
        /// Basis-Währung (z.B. für Übersetzungen in andere Währungen).
        /// </summary>
        public const BotCoinType TRADE_BASE_CURRENCY = BotCoinType.eUSDollar;
        /// <summary>
        /// Währung für Gegenwerte.
        /// </summary>
        public const BotCoinType TRADE_EXCHANGE_CURRENCY = BotCoinType.eEuro;

        public const BotCoinType TRADE_FIRST_CRYPTO_CURRENCY = (BotConvention.TRADE_BASE_CURRENCY + 1);
        #endregion


        #region Calculation
        internal static double TranslateAmount(double dAmount, double dRate, bool bPrim2Sec = true)
        {
            // Betrag in angeforderte Währung umrechnen:
            return ((bPrim2Sec) ? (dAmount * dRate) : (dAmount / dRate));
        }
        #endregion


        #region PropertyGrid
        internal const string PROP_CATEGORY_GENERAL = "Allgemein";
        internal const string PROP_CATEGORY_SERVICE = "Service";
        internal const string PROP_CATEGORY_MANAGEMENT = "Management";
        internal const string PROP_CATEGORY_AUTH = "Authentifizierung";
        internal const string PROP_CATEGORY_BUY_ORDERS = "Kauf-Orders";
        internal const string PROP_CATEGORY_SELL_ORDERS = "Verkauf-Orders";
        internal const string PROP_CATEGORY_SHORT_SALE = "Leerverkauf";
        internal const string PROP_CATEGORY_FUNCTIONS = "Funktionen";
        internal const string PROP_CATEGORY_INDICATORS = "Indikatoren";
        internal const string PROP_CATEGORY_TRIGGERS = "Trigger";
        internal const string PROP_CATEGORY_RATING = "Funktionen für Bewertung";
        internal const string PROP_CATEGORY_REACTION = "Funktionen für Reaktionen";
        internal const string PROP_CATEGORY_SETTINGS_SELECTION = "Funktionen für Auswahl der Einstellungen";
        internal const string PROP_CATEGORY_VARIATION = "Änderung";
        internal const string PROP_CATEGORY_CURRENT_RIFT = "Aktueller Rift";
        internal const string PROP_CATEGORY_RIFT = "Rift";
        internal const string PROP_CATEGORY_SUMMARY = "Zusammenfassung";
        internal const string PROP_CATEGORY_SUMMARY_CUMULATED = "Kumuliert";
        internal const string PROP_CATEGORY_CALCULATION = "Kalkulation";
        internal const string PROP_CATEGORY_TRADINGPAIR = "Pärchen";
        internal const string PROP_CATEGORY_PLATFORM = "Platform";
        internal const string PROP_CATEGORY_CURRENCY = "Währung";
        internal const string PROP_CATEGORY_WALLET = "Wallet";
        internal const string PROP_CATEGORY_DEBUG = "Debug";
        internal const string PROP_CATEGORY_COINSTREAM_REPLAY = "Coin-Stream Replay";

        public const string PROP_NAME_INDICATORS = "Indikatoren";
        public const string PROP_NAME_INDICATOR = "Indikator";
        public const string PROP_NAME_TREND_INDICATOR = "Trend-Indikator";
        public const string PROP_NAME_TRIGGER = "Trigger";
        public const string PROP_NAME_PRIORITY = "Prioritäten";
        public const string PROP_NAME_ACTIVATED = "Aktiviert";
        public const string PROP_NAME_ENABLED = "Freigabe";
        public const string PROP_NAME_LIMITS = "Grenzwerte";
        public const string PROP_NAME_RESETMODE = "Rücksetzbar";
        public const string PROP_NAME_TICKER_INFLUENCE = "Ticker-Einfluß";
        public const string PROP_NAME_TRAILING = "Nachführen";
        public const string PROP_NAME_IS_READY = "Bereit";
        public const string PROP_NAME_COOLDOWN_TIME = "Abklingzeit";
        public const string PROP_NAME_COOLDOWN_TIME_BUYORDERS = PROP_NAME_COOLDOWN_TIME + " Kauf-Orders";
        public const string PROP_NAME_PARAM_VALID = "Parameter Gültig";
        public const string PROP_NAME_FORMULA_EXPRESSION = "Formel";
        public const string PROP_NAME_FORMULA_RESULT = "Ergebnis Formel";
        public const string PROP_NAME_HYSTERESIS = "Hysterese";
        public const string PROP_NAME_TITLE = "Bezeichnung";
        public const string PROP_NAME_COMMENT = "Kommentar";
        public const string PROP_NAME_ACTIVE_SETTINGS = "Aktive Einstellungen";
        public const string PROP_NAME_DEFAULT_SETTINGS = "Standard-Einstellungen";
        public const string PROP_NAME_REMAINING_ACTIVE_DURATION = "Verbleibende Einschaltzeit";
        public const string PROP_NAME_KEEP_PRIORITY = "Priorität beibehalten";
        public const string PROP_NAME_STATE_RESET = "Zustand Rücksetzen";
        public const string PROP_NAME_DURATION = "Dauer";
        public const string PROP_NAME_REFERENCE = "Referenz";

        internal const string PROP_DESCR_REACTION_INDICATORS = "Geeignet für verzögerte Reaktionen auf Kurs-Bewegungen.";
        internal const string PROP_DESCR_TREND_INDICATORS = "Geeignet für mittel- und langfristige Kurs-Bewertungen.";
        internal const string PROP_DESCR_TRIGGER = "Geeignet für direkte Reaktionen auf Kurs-Bewegungen.";
        internal const string PROP_DESCR_PRIORITY = "Priorisierte Auswahl referenzierter Einstellungen unter definierten Bedingungen zur Laufzeit.";
        internal const string PROP_DESCR_LIMITS = "Grenzwerte für Ausführung von Orders.";
        internal const string PROP_DESCR_RESETMODE = "Ermöglicht das Rücksetzen der Grenzwerte zum Zeitpunt des fehlgeschlagenen Auslösens einer Order aufgrund von nicht zutreffenden Abhängigkeiten. Ein Bedingtes Rücksetzen wird ausgeführt nachdem nicht zutreffende Abhängigkeiten gelöst sind, die Konditionen zum Ausführen der Order sich jedoch (verglichen mit dem Zeitpunkt der Auslösung) verschlechtert haben.";
        internal const string PROP_DESCR_AVG_PERIOD = "Dauer des Zeitraums in dem der Durchschnittswert gebildet wird.";
        internal const string PROP_DESCR_COMMENT = "Optionaler Kommentar als Möglichkeit zur genaueren Beschreibung.";
        internal const string PROP_DESCR_FORMULA_EXPRESSION = "Zu erfüllende Bedingung zur Freigabe von Orders.";
        #endregion


        #region Parameters
        internal static readonly BotPropertyTitle PARAM_FORMULA_EXPRESSION = new BotPropertyTitle(PROP_NAME_FORMULA_EXPRESSION, "Ausdruck nicht definiert.");
        internal static readonly BotPropertyTitle PARAM_REF_PARAMETERS = new BotPropertyTitle(PROP_NAME_REFERENCE, "Referenzierte Parameter nicht definiert.");
        internal static readonly BotPropertyTitle PARAM_REF_SETTINGS = new BotPropertyTitle(PROP_NAME_REFERENCE, "Referenzierte Einstellungen nicht definiert.");
        #endregion


        #region DataFile
        internal const String DATAFILE_JSON_TBOTS = "Bots";
        internal const String DATAFILE_JSON_SETTINGS = "Settings";
        internal const String DATAFILE_JSON_SERVICES = "Services";
        internal const String DATAFILE_JSON_PARAMETERS = "Parameters";
        internal const String DATAFILE_JSON_DEFAULT_PARAMETERS = "DefaultParameters";
        internal const String DATAFILE_JSON_SELECTORS = "Selectors";
        internal const String DATAFILE_JSON_PLATFORM = "Platform";
        internal const String DATAFILE_JSON_STRATEGIES = "Strategies";
        internal const String DATAFILE_JSON_PRIORITIES = "Priorities";
        internal const String DATAFILE_JSON_PERSISTENT = "Persistent";
        internal const String DATAFILE_JSON_RIFT = "Rift";
        internal const String DATAFILE_JSON_RIFTS_WEEKLY = "WeeklyRifts";
        internal const String DATAFILE_JSON_RIFTS_TOTAL = "TotalRifts";
        internal const String DATAFILE_JSON_BOTAUTOMATIC = "BotAutomatic";
        internal const String DATAFILE_JSON_TREND_INDICATOR = "Trend-Indicator";
        internal const String DATAFILE_JSON_REACTION_INDICATORS = "Reaction-Indicators";
        internal const String DATAFILE_JSON_TRIGGERS = "Triggers";
        internal const String DATAFILE_JSON_DEPENDENCIES = "Dependencies";
        internal const String DATAFILE_JSON_COOLDOWN = "Cooldown";
        internal const String DATAFILE_JSON_ORDERS = "Orders";
        internal const String DATAFILE_JSON_BUYORDERS = "BuyOrders";
        internal const String DATAFILE_JSON_SELLORDERS = "SellOrders";
        internal const String DATAFILE_JSON_LASTBUYORDER = "LastBuyOrder";
        internal const String DATAFILE_JSON_LASTSELLORDER = "LastSellOrder";

        internal const String DATAFILE_JSON_VERSION = "Version";
        internal const String DATAFILE_JSON_ACTIVE = "Enabled";
        internal const String DATAFILE_JSON_PAIR = "Pair";
        internal const String DATAFILE_JSON_DOMAIN = "Domain";
        internal const String DATAFILE_JSON_AUTH_APIKEY = "AuthAPIKey";
        internal const String DATAFILE_JSON_AUTH_APISECRET = "AuthAPISecret";

        internal const String DATAFILE_JSON_SERVICE_ACTIVE = "Enabled";
        internal const String DATAFILE_JSON_SERVICE_DOMAIN = "Domain";

        internal const String DATAFILE_JSON_BOTAUTOMATIC_ISUSABLE = "BotAutomatik_IsUsable";

        internal const String DATAFILE_JSON_STRATEGY_REF_PARAMETERS = "RefParameters";
        internal const String DATAFILE_JSON_STRATEGY_ACTIVE_SETTINGS = "ActiveSettings";

        internal const String DATAFILE_JSON_ORDERS_TYPE = "Type";
        internal const String DATAFILE_JSON_ORDERS_TIMEOUT = "Timeout";
        internal const String DATAFILE_JSON_ORDERS_LIMITOFFSET_BUY = "LimitOffset_Buy";
        internal const String DATAFILE_JSON_ORDERS_LIMITOFFSET_SELL = "LimitOffset_Sell";

        internal const String DATAFILE_JSON_REPORTS_DIRECTORY = "Reports_Directory";
        internal const String DATAFILE_JSON_COINSTREAM_DIRECTORY = "CoinStream_Directory";
        internal const String DATAFILE_JSON_COINSTREAM_DELAY = "CoinStream_Delay";
        internal const String DATAFILE_JSON_DEBUGMODE_ENABLED = "DebugMode_Enabled";

        internal const String DATAFILE_JSON_COOLDOWN_SCOPE = "Scope";
        internal const String DATAFILE_JSON_COOLDOWN_DURATION = "Duration";
        internal const String DATAFILE_JSON_COOLDOWN_STOPPOINT = "Stop";

        internal const String DATAFILE_JSON_STAT_RIFT_BEGIN = "Begin";
        internal const String DATAFILE_JSON_STAT_RIFT_END = "End";
        internal const String DATAFILE_JSON_STAT_RIFT_ORDERS = "Orders";
        internal const String DATAFILE_JSON_STAT_RIFT_AMOUNT = "Amount";
        internal const String DATAFILE_JSON_STAT_RIFT_PRICE = "Price";
        internal const String DATAFILE_JSON_STAT_RIFT_EXCHANGEPRICE = "ExchangePrice";
        internal const String DATAFILE_JSON_STAT_RIFT_AVERAGERATE = "AverageRate";
        internal const String DATAFILE_JSON_STAT_RIFT_GOODCOUNT = "GoodRifts";
        internal const String DATAFILE_JSON_STAT_RIFT_BADCOUNT = "BadRifts";

        internal const String DATAFILE_JSON_STAT_LASTORDERINFO_CREATIONTIME = "CreationTime";
        internal const String DATAFILE_JSON_STAT_LASTORDERINFO_RATE = "Rate";

        internal const String DATAFILE_JSON_CALC_RIFT_REMAININGBUYSTEPS_DEFAULT = "Calc_Rift_RemainingBuySteps_Default";
        internal const String DATAFILE_JSON_CALC_RIFT_REMAININGBUYSTEPS_RESERVE = "Calc_Rift_RemainingBuySteps_Reserve";
        internal const String DATAFILE_JSON_CALC_RIFT_REMAININGSELLSTEPS = "Calc_Rift_RemainingSellSteps";
        internal const String DATAFILE_JSON_CALC_RIFT_NEXTSELLOPPRESSIVE = "Calc_Rift_NextSellOppressive";

        internal const String DATAFILE_JSON_PARAM_ENABLED = "Enabled";
        internal const String DATAFILE_JSON_PARAM_COMMENT = "Comment";

        internal const String DATAFILE_JSON_FUNCTIONPARAM_REFVARIABLE = "RefVariable";

        internal const String DATAFILE_JSON_SELECTOR_REF_SETTINGS = "RefSettings";
        internal const String DATAFILE_JSON_SELECTOR_FORMULA_EXPRESSION = "Formula_Expression";
        internal const String DATAFILE_JSON_SELECTOR_KEEP_PRIORITY = "KeepPriority";

        internal const String DATAFILE_JSON_STRATEGYPARAM_MGMT_GUID = "GUID";
        internal const String DATAFILE_JSON_STRATEGYPARAM_MGMT_TITLE = "Title";
        internal const String DATAFILE_JSON_STRATEGYPARAM_MGMT_COMMENT = "Comment";

        internal const String DATAFILE_JSON_STRATEGYPARAM_BUY_WEIGHT = "Buy_Weight";
        internal const String DATAFILE_JSON_STRATEGYPARAM_BUY_STEPS_DEFAULT = "Buy_Steps_Default";
        internal const String DATAFILE_JSON_STRATEGYPARAM_BUY_STEPS_RESERVE = "Buy_Steps_Reserve";
        internal const String DATAFILE_JSON_STRATEGYPARAM_BUY_MINPRICE = "Buy_MinPrice";
        internal const String DATAFILE_JSON_STRATEGYPARAM_BUY_MAXPRICE = "Buy_MaxPrice";
        internal const String DATAFILE_JSON_STRATEGYPARAM_BUY_CONDITION = "Buy_Condition";

        internal const String DATAFILE_JSON_STRATEGYPARAM_INDICATOR_SOURCE_TYPE = "SourceType";
        internal const String DATAFILE_JSON_STRATEGYPARAM_INDICATOR_AVERAGE_TYPE = "AverageType";
        internal const String DATAFILE_JSON_STRATEGYPARAM_INDICATOR_SLOW_RATING_MIN = "Slow_MinRating";
        internal const String DATAFILE_JSON_STRATEGYPARAM_INDICATOR_TICKER_INFLUENCE = "TickerInfluence";
        internal const String DATAFILE_JSON_STRATEGYPARAM_INDICATOR_FAST_RATING_ENABLE = "Fast_RatingEnable";
        internal const String DATAFILE_JSON_STRATEGYPARAM_INDICATOR_FAST_RATING_EXECUTE = "Fast_RatingExecute";
        internal const String DATAFILE_JSON_STRATEGYPARAM_INDICATOR_FAST_RATING_TRAILEXECUTE = "Fast_RatingTrailExecute";
        internal const String DATAFILE_JSON_STRATEGYPARAM_INDICATOR_FORMULA_EXPRESSION = "Formula_Expression";
        internal const String DATAFILE_JSON_STRATEGYPARAM_INDICATOR_RESETMODE = "ResetMode";
        internal const String DATAFILE_JSON_STRATEGYPARAM_INDICATOR_AVG1_PERIOD = "AVG1_Period";
        internal const String DATAFILE_JSON_STRATEGYPARAM_INDICATOR_AVG2_PERIOD = "AVG2_Period";
        internal const String DATAFILE_JSON_STRATEGYPARAM_INDICATOR_DEPENDENCY_ORDERS = "Orders";
        internal const String DATAFILE_JSON_STRATEGYPARAM_INDICATOR_DEPENDENCY_DOUBLECHECK = "DoubleCheck";
        internal const String DATAFILE_JSON_STRATEGYPARAM_TREND_LIMIT_BUYINDICATOR = "Limit_BuyIndicators";
        internal const String DATAFILE_JSON_STRATEGYPARAM_TREND_LIMIT_SELLINDICATOR = "Limit_SellIndicators";
        internal const String DATAFILE_JSON_STRATEGYPARAM_TRIGGER_TRAILING = "Trailing";
        internal const String DATAFILE_JSON_STRATEGYPARAM_TRIGGER_HYSTERESIS = "Hysteresis";
        internal const String DATAFILE_JSON_STRATEGYPARAM_TRIGGER_DEPENDENT = "Dependent";
        internal const String DATAFILE_JSON_STRATEGYPARAM_TRIGGER_PERSISTENT = "Persistent";
        internal const String DATAFILE_JSON_STRATEGYPARAM_TRIGGER_FORMULA_EXPRESSION = "Formula_Expression";
        internal const String DATAFILE_JSON_STRATEGYPARAM_TRIGGER_ORDER_DIRECTION = "Order_Direction";
        internal const String DATAFILE_JSON_STRATEGYPARAM_TRIGGER_RESETMODE = "ResetMode";

        internal const String DATAFILE_JSON_STRATEGYPARAM_CFG_MINACTIVEDURATION = "Cfg_MinActiveDuration";
        internal const String DATAFILE_JSON_STRATEGYPARAM_RIFT_STOPONCLOSE = "Rift_StopOnClose";
        internal const String DATAFILE_JSON_STRATEGYPARAM_NEXTBUY_RATEDISTANCEFACTOR = "NextBuy_RateDistanceFactor";
        internal const String DATAFILE_JSON_STRATEGYPARAM_NEXTBUY_TRAILING = "NextBuy_Trailing";
        internal const String DATAFILE_JSON_STRATEGYPARAM_SELL_CONDITION = "Sell_Condition";
        internal const String DATAFILE_JSON_STRATEGYPARAM_NEXTSELL_RATEDISTANCEFACTOR = "NextSell_RateDistanceFactor";
        internal const String DATAFILE_JSON_STRATEGYPARAM_NEXTSELL_MINPROPHITPERCENT_BUYAVGRATE = "NextSell_MinProphitPercent_BuyAvgRate";
        internal const String DATAFILE_JSON_STRATEGYPARAM_NEXTSELL_TRAILING = "NextSell_Trailing";
        internal const String DATAFILE_JSON_STRATEGYPARAM_SELL_NEUTRALZONE = "Sell_NeutralZone";
        internal const String DATAFILE_JSON_STRATEGYPARAM_BUY_AVGDOWN_ENABLED = "Buy_AvgDown_Enabled";
        internal const String DATAFILE_JSON_STRATEGYPARAM_BUY_AVGDOWN_DISTANCE = "Buy_AvgDown_Distance";
        internal const String DATAFILE_JSON_STRATEGYPARAM_SHORTSALE_STEPS = "ShortSale_Steps";
        internal const String DATAFILE_JSON_STRATEGYPARAM_SHORTSALE_OPPRESSIVE = "ShortSale_Oppressive";
        internal const String DATAFILE_JSON_STRATEGYPARAM_SHORTSALE_PANIC_ENABLED = "ShortSale_Panic_Enabled";
        internal const String DATAFILE_JSON_STRATEGYPARAM_SHORTSALE_PANIC_EXECUTELIMIT = "ShortSale_Panic_ExecuteLimit";

        internal const String DATAFILE_JSON_BOTAUTOMATICPARAM_MGMT_ENABLED = "Enabled";
        internal const String DATAFILE_JSON_BOTAUTOMATICPARAM_MGMT_MODE = "Mode";
        internal const String DATAFILE_JSON_BOTAUTOMATICPARAM_PLTF_MAXACTIVESTRATEGIES = "Pltf_MaxActiveStrategies";
        internal const String DATAFILE_JSON_BOTAUTOMATICPARAM_PLTF_MINTOTALBASECURRENCYFUNDS = "Pltf_MinTotalBaseCurrencyFunds";
        internal const String DATAFILE_JSON_BOTAUTOMATICPARAM_COIN_MINMARKETCAP = "Coin_MinMarketCap";
        internal const String DATAFILE_JSON_BOTAUTOMATICPARAM_COIN_MINPERCENTCHANGE = "Coin_MinPercentChange";
        #endregion
    }
    // Stellt Objekte zur Verfügung, die als "Critical Section" für Threads angewendet werden.
    // > Diese Objekte dienen zur Anwendung von statischen Locks.
    // > Für Instanz-bezogene Locks sollten alle entsprechenden Klassen (zur besseren Übersicht) ein 'object oThreadLockObj' definieren!
    public class BotThreadLock
    {
        #region Constants
        public const int OBJ_COUNT = 2;

        public const int OBJ_EXCEL_DOC = 0;
        public const int OBJ_CURRENCY_RATING = 1;

        public const int LOCK_TIMEOUT = 4000;
        #endregion


        #region Properties
        public static object[] Object { private set; get; } = new object[OBJ_COUNT];
        #endregion


        #region Initialization
        public static void Init()
        {
            for (int i = 0; i < OBJ_COUNT; i++)
                Object[i] = new object();
        }
        #endregion
    }

    public class BotDebugMode
    {
        #region Constants
        // Optionen:
        public const int OPT_COUNT = 8;

        public const int OPT_FUNCTION_LIMITS_ENABLED = 0;               // Aktiviert alle Grenze-Werte für Funktionen (Indikatoren, Trigger, ...)
        public const int OPT_SHOW_CONSOLE_SOCKET_MESSAGES = 1;          // Zeit den gesamten Netzwerk-Verkehr im Log-Fenster an
        public const int OPT_WRITE_FILE_SOCKET_MESSAGES = 2;            // Schreibt den gesamten Netzwerk-Verkehr in eine Log-Datei
        public const int OPT_WRITE_FILE_COIN_INFO = 3;                  // Schreibt aktuelle Informationen zu einem Coin in eine CSV-Datei
        public const int OPT_CONSTANT_ORDERS = 4;                       // Führt Ein- und Verkauf-Orders immer zum selben Kurs aus (Nur in Simulation)
        public const int OPT_STATESHOT_ON_ORDER = 5;                    // Speichert einen StateShot sobald eine Order ausgeführt wird
        public const int OPT_LOGFILE_WRITE_DEBUGINFO = 6;               // Schreibt Debug-Informationen in die LogFile
        public const int OPT_LOGFILE_WRITE_VOLATILE = 7;                // Schreibt auch volatile Benachrichtigungen in die LogFile

        // Files:
        public const String FILE_SOCK_MSG = "Debug.SockMsg.txt";
        public const String FILE_COIN_INFO = "CoinInfo.{0}.{1}.csv";
        #endregion


        #region Properties.Management
        [Browsable(false)]
        public static bool Active
        {
            get { return bOptions.FirstOrDefault(bValue => (bValue == true)); }
            set
            {
                Debug.Assert((OPT_COUNT == 8), "[A74]");

                // Ausgewählte Optionen aktivieren:
                bOptions[OPT_FUNCTION_LIMITS_ENABLED] = false;
                bOptions[OPT_SHOW_CONSOLE_SOCKET_MESSAGES] = false;
                bOptions[OPT_WRITE_FILE_SOCKET_MESSAGES] = value;
                bOptions[OPT_WRITE_FILE_COIN_INFO] = value;
                bOptions[OPT_CONSTANT_ORDERS] = false;
                bOptions[OPT_STATESHOT_ON_ORDER] = value;
                bOptions[OPT_LOGFILE_WRITE_DEBUGINFO] = value;
                bOptions[OPT_LOGFILE_WRITE_VOLATILE] = value;
            }
        }
        #endregion
        #region Properties
        public static bool FunctionLimitsEnabled { get { return (bOptions[OPT_FUNCTION_LIMITS_ENABLED]); } }
        public static bool SocketMsgShown { get { return (bOptions[OPT_SHOW_CONSOLE_SOCKET_MESSAGES]); } }
        public static bool SocketMsgWrite { get { return (bOptions[OPT_WRITE_FILE_SOCKET_MESSAGES]); } }
        public static bool CoinInfoWrite { get { return (bOptions[OPT_WRITE_FILE_COIN_INFO]); } }
        public static bool ConstantOrders { get { return (bOptions[OPT_CONSTANT_ORDERS]); } }
        public static bool StateShotOnOrder { get { return (bOptions[OPT_STATESHOT_ON_ORDER]); } }
        public static bool LogFileWriteDebugInfo { get { return (bOptions[OPT_LOGFILE_WRITE_DEBUGINFO]); } }
        public static bool LogFileWriteVolatile { get { return (bOptions[OPT_LOGFILE_WRITE_VOLATILE]); } }
        #endregion


        #region Events
        public static void OnSocketSend(BotPlatformService pPlatform, String sMessage)
        {
            if (BotDebugMode.SocketMsgShown)
                Console.WriteLine(String.Format("[{0}] SocketSend: {1}", pPlatform.PlatformName, sMessage));

            if (BotDebugMode.SocketMsgWrite)
                WriteFile(pPlatform, FILE_SOCK_MSG, nameof(OnSocketSend), sMessage);
        }
        public static void OnSockMessage(BotPlatformService pPlatform, String sMessage)
        {
            if (BotDebugMode.SocketMsgWrite)
                WriteFile(pPlatform, FILE_SOCK_MSG, nameof(OnSockMessage), sMessage);
        }
        public static void OnCoinInfoUpdate(BotTradingStrategy tStrategy, BotCandleSubscription.BotCandleData cCurCandleData, BotTickerSubscription.BotTickerData tCurTickerData)
        {
            if (BotDebugMode.CoinInfoWrite)
            {
                BotPlatformService pPlatform = tStrategy.Platform;
                String sFileName = String.Format(FILE_COIN_INFO, pPlatform.PlatformName, tStrategy.TradingPair.Symbol);
                bool bInitTitle = !File.Exists(sFileName);
                char cSeparator = ';';

                Debug.Assert((tStrategy != null), "[OCIU136]");
                Debug.Assert((cCurCandleData != null), "[OCIU137]");
                Debug.Assert((tCurTickerData != null), "[OCIU138]");

                using (StreamWriter sWriter = new StreamWriter(sFileName, true))
                {
                    IBotCoinInfo bCoinInfo = BotCurrencyRating.CoinInfo.Find(tStrategy.TradingPair.CurrencyPrim);
                    DateTime dDate = DateTime.Now;

                    if (bInitTitle)
                    {
                        sWriter.Write("Date" + cSeparator);
                        sWriter.Write("Time" + cSeparator);
                        sWriter.Write("Volume" + cSeparator);
                        sWriter.Write("Ask" + cSeparator);
                        sWriter.Write("AskSize" + cSeparator);
                        sWriter.Write("Bid" + cSeparator);
                        sWriter.Write("BidSize" + cSeparator);
                        sWriter.Write("Open" + cSeparator);
                        sWriter.Write("High" + cSeparator);
                        sWriter.Write("Low" + cSeparator);
                        sWriter.Write("Close" + cSeparator);
                        sWriter.Write("Price" + cSeparator);
                        sWriter.Write("Change 1h" + cSeparator);
                        sWriter.Write("Change 24h" + cSeparator);
                        sWriter.Write("Change 7d" + cSeparator);
                        sWriter.WriteLine();
                    }

                    sWriter.Write(String.Format("{0}", dDate.Date) + cSeparator);
                    sWriter.Write(String.Format("{0}", dDate.TimeOfDay) + cSeparator);
                    sWriter.Write(tCurTickerData.dVolume.ToString() + cSeparator);
                    sWriter.Write(tCurTickerData.dAsk.ToString() + cSeparator);
                    sWriter.Write(tCurTickerData.dAskSize.ToString() + cSeparator);
                    sWriter.Write(tCurTickerData.dBid.ToString() + cSeparator);
                    sWriter.Write(tCurTickerData.dBidSize.ToString() + cSeparator);
                    sWriter.Write(cCurCandleData.dOpen.ToString() + cSeparator);
                    sWriter.Write(cCurCandleData.dHigh.ToString() + cSeparator);
                    sWriter.Write(cCurCandleData.dLow.ToString() + cSeparator);
                    sWriter.Write(cCurCandleData.dClose.ToString() + cSeparator);
                    sWriter.Write(tCurTickerData.dLastPrice.ToString() + cSeparator);

                    if (bCoinInfo != null)
                    {
                        sWriter.Write(bCoinInfo.PercentChange[BotCurrencyRating.PERCENT_CHANGE_1h].ToString() + cSeparator);
                        sWriter.Write(bCoinInfo.PercentChange[BotCurrencyRating.PERCENT_CHANGE_24h].ToString() + cSeparator);
                        sWriter.Write(bCoinInfo.PercentChange[BotCurrencyRating.PERCENT_CHANGE_7D].ToString() + cSeparator);
                    }
                    else
                    {
                        for (int i = 0; i < 3; i++)
                            sWriter.Write("0" + cSeparator);
                    }

                    sWriter.WriteLine();
                }
            }
        }
        #endregion


        #region Helper
        private static void WriteFile(BotPlatformService pPlatform, String sFileName, String sProcess, String sLine)
        {
            using (StreamWriter sWriter = new StreamWriter(sFileName, true))
            {
                sWriter.WriteLine(String.Format("{0} [{1}] {2}: {3}", DateTime.Now, pPlatform.PlatformName, sProcess, sLine));
            }
        }
        #endregion


        private static bool[] bOptions = new bool[OPT_COUNT];
    }


    public struct BotPropertyTitle
    {
        public BotPropertyTitle(String sName = "", String sDescription = "")
        {
            this.sName = sName;
            this.sDescription = sDescription;
        }


        public String sName;
        public String sDescription;
    }
    #endregion
}
