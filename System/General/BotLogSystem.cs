﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using Shared.Utils.Core;
using Shared.Net;

namespace CryptoRiftBot
{
    #region Enumerations
    public enum BotLogType
    {
        eInfo = 0,
        eWarning = 1,
        eError = 2,
        eEvent = 3,
        ePlatformInfo = 4,
        ePlatformWarning = 5,
        ePlatformError = 6,
        ePlatformEvent = 7,
        ePlatformTrading = 8
    };
    public enum BotLogPriority
    {
        eLow = 0,
        eNormal = 1,
        eHigh = 2,
    };
    public enum BotLogProcess
    {
        [Description("Initialisierung")] eInit,
        [Description("Allgemein")] eGeneral,
        [Description("Version")] eVersion,
        [Description("Account")] eAccount,
        [Description("Kommunikation")] eCommunication,
        [Description("Konfiguration")] eConfig,
        [Description("Event")] eEvent,
        [Description("Login")] eLogin,
        [Description("Evaluation")] eEvaluation,
        [Description("Rift")] eRift,
        [Description("Order")] eOrder,
        [Description("Wallet")] eWallet,
        [Description("Trading-Strategie")] eStrategy,
        [Description("Trading-Bot")] eBot,
        [Description("Trading-Bot Automatik")] eBotAutomatic,
        [Description("Rating-Update")] eUpdateRating,
        [Description("Wechselkurs-Update")] eUpdateExchangeRate,
        [Description("Ticker-Update")] eUpdateTicker,
        [Description("Candle-Update")] eUpdateCandles,
        [Description("Coin-Stream")] eCoinStream,
        [Description("Webserver")] eWebserver,
        [Description("Verschlüsselung")] eEncryption
    }
    #endregion


    #region Interfaces
    public interface IBotLogSystem
    {
        #region Properties
        IReadOnlyList<BotLogEntry> Entries { get; }
        #endregion
        #region Properties
        public String[] Columns { get; }
        #endregion


        #region MessageBox
        void ShowException(String sCode, BotLogProcess eProcess, Exception eExcpt);
        void ShowResult(String sCode, BotLogProcess eProcess, String sDescription, int iResult = -1);
        #endregion
        #region Log
        internal bool WriteLogInvalidParam(String sCode, String sContext, BotPropertyTitle pParameter);
        void WriteLog(String sCode, BotLogProcess eProcess, Exception eException, BotLogType lType = BotLogType.eInfo, BotLogPriority ePriority = BotLogPriority.eNormal);
        void WriteLog(String sCode, BotLogProcess eProcess, String sDescription, BotLogType lType = BotLogType.eInfo, BotLogPriority ePriority = BotLogPriority.eNormal);
        void WriteDebugLog(String sCode, BotLogProcess eProcess, String sDescription, WebRequestEx wRequest, BotLogType lType = BotLogType.eInfo, BotLogPriority ePriority = BotLogPriority.eNormal);
        void WriteDebugLog(String sCode, BotLogProcess eProcess, String sDescription, String sDebugInfo, BotLogType lType = BotLogType.eInfo, BotLogPriority ePriority = BotLogPriority.eNormal);
        /// <summary>
        /// Fügt einen Log-Eintrag hinzu, der nur im Log-Fenster angezeigt wird.
        /// </summary>
        /// <param name="sCode"></param>
        /// <param name="sProcess"></param>
        /// <param name="sDescription"></param>
        /// <param name="oTag">Eintrag wird mittels Label identifiziert.</param>
        /// <param name="lType"></param>
        /// <param name="ePriority"></param>
        void WriteLogVolatile(String sCode, BotLogProcess eProcess, String sDescription, Object oTag, BotLogType lType = BotLogType.eInfo, BotLogPriority ePriority = BotLogPriority.eNormal);
        /// <summary>
        /// Entfernt einen Log-Eintrag.
        /// </summary>
        /// <param name="oTag">Übereinstimmender Tag des zu entfernen Eintrags.</param>
        internal void RemoveLogEntry(object oTag);
        #endregion
    }
    #endregion


    #region Types
    public class BotLogEntry
    {
        public BotLogEntry(String sCode, BotLogProcess eProcess, String sDescription, String sDebugInfo = "", BotLogType lType = BotLogType.eInfo, BotLogPriority ePriority = BotLogPriority.eNormal, Object oTag = null)
        {
            this.Priority = ePriority;
            this.Code = sCode;
            this.Process = UtlEnum.GetEnumDescription(eProcess);
            this.Description = sDescription;
            this.DebugInfo = sDebugInfo;
            this.Type = lType;

            this.TimeStamp = DateTime.Now;
            this.Tag = oTag;
        }


        #region Properties.Management
        public object Tag { private set; get; }
        [TypeConverter(typeof(CnvBooleanLabel.NoYes))]
        public bool IsVolatile { get { return (Tag != null); } }
        public bool HasDebugInfo { get { return ((DebugInfo != null) && (DebugInfo.Length > 0)); } }
        #endregion
        #region Properties
        public BotLogPriority Priority { private set; get; }
        public String Code { private set; get; }
        public String Process { private set; get; }
        public String Description { private set; get; }
        public String DebugInfo { private set; get; }
        public BotLogType Type { private set; get; }
        public String TypeTitle
        {
            get
            {
                switch (Type)
                {
                    case BotLogType.eWarning:
                    case BotLogType.ePlatformWarning:
                        return ("WARNUNG");

                    case BotLogType.eError:
                    case BotLogType.ePlatformError:
                        return ("ERROR");

                    case BotLogType.eEvent:
                    case BotLogType.ePlatformEvent:
                        return ("EVENT");

                    case BotLogType.ePlatformTrading:
                        return ("TRADE");
                }
                return ("INFO");
            }
        }
        public DateTime TimeStamp { private set; get; }
        #endregion


        #region Management
        public String FormatCSVFileEntry()
        {
            return (String.Format("{0:dd.MM.yyyy};{0:HH:mm:ss};{1};{2};{3};{4}", TimeStamp, Code, TypeTitle, Process, Description));
        }
        public String FormatCSVFileDebugEntry()
        {
            return (String.Format("{0:dd.MM.yyyy};{0:HH:mm:ss};{1};;;{2}", TimeStamp, Code, DebugInfo));
        }
        #endregion
    }
    internal class BotLogSystem : IBotLogSystem
    {
        #region Constants
        // Content Array:
        public const int ARRAY_MAX_ENTRY_COUNT = 300;

        // Log-File:
        public const String FILE_NAME = "log.csv";

        // Log-Control:
        public const int CTRL_COL_TIMESTAMP = 0;
        public const int CTRL_COL_CODE = 1;
        public const int CTRL_COL_TYPE = 2;
        public const int CTRL_COL_PROCESS = 3;
        public const int CTRL_COL_DESCRIPTION = 4;

        public const int CTRL_IMG_ERROR = 0;
        public const int CTRL_IMG_EXCLAMATION = 1;
        public const int CTRL_IMG_INFO = 2;
        #endregion


        public BotLogSystem()
        {
            String sFilePath = String.Format("{0}\\{1}", AppContext.BaseDirectory, FILE_NAME);
            bool bExisting = File.Exists(sFilePath);

            // Array initialisieren:
            lEntries = new List<BotLogEntry>();

            // Datei initialisieren:
            sFile = new StreamWriter(sFilePath, bExisting);

            if (bExisting == false)
            {
                for (int i = 0; i < Columns.Length; i++)
                {
                    if (i > 0)
                        sFile.Write(";");

                    sFile.Write(Columns[i]);
                }
                sFile.WriteLine("");
            }
            
            Debug.Assert((sFile != null), "[BLS211]");
        }
        ~BotLogSystem()
        {
            Debug.Assert((sFile != null), "[BLS215]");

            //sFile.Close();
            //sFile = null;

            //lEntries.Clear();
            //lEntries = null;
        }


        #region Properties
        public IReadOnlyList<BotLogEntry> Entries { get { return (lEntries); } }
        private List<BotLogEntry> lEntries;
        #endregion
        #region Properties
        public String[] Columns { get; } = new String[] { "Zeitpunkt", "Code", "Typ", "Prozess", "Beschreibung" };
        #endregion


        #region MessageBox
        public void ShowException(String sCode, BotLogProcess eProcess, Exception eExcpt)
        {
            ShowResult(sCode, eProcess, String.Format("{0}Exception occured:{0}{1}", Environment.NewLine, eExcpt.Message), eExcpt.HResult);
        }
        public void ShowResult(String sCode, BotLogProcess eProcess, String sDescription, int iResult = -1)
        {
            MessageBoxIcon mIcon = (iResult == 0) ? MessageBoxIcon.Information : MessageBoxIcon.Error;

            ShowMessage(sCode, eProcess, String.Format("Error-Code 0x{0:X}:{1}{2}", iResult, Environment.NewLine, sDescription), MessageBoxButtons.OK, mIcon);
        }
        /// <summary>
        /// Zeigt eine Nachricht für den Benutzer an.
        /// </summary>
        /// <param name="sCode">(Optional)</param>
        /// <param name="sProcess"></param>
        /// <param name="sDescription"></param>
        /// <param name="mButtons"></param>
        /// <param name="mIcon"></param>
        /// <returns></returns>
        public DialogResult ShowMessage(String sCode, BotLogProcess eProcess, String sDescription, MessageBoxButtons mButtons = MessageBoxButtons.OK, MessageBoxIcon mIcon = MessageBoxIcon.Error)
        {
            String sTitle = UtlEnum.GetEnumDescription(eProcess);
            if (sCode.Length > 0)
                sTitle = String.Format("[{0}] {1}", sCode, sTitle);
            return (MessageBox.Show(sDescription, sTitle, mButtons, mIcon));
        }
        #endregion
        #region Log
        bool IBotLogSystem.WriteLogInvalidParam(String sCode, String sContext, BotPropertyTitle pParameter)
        {
            WriteLog(sCode, BotLogProcess.eConfig, String.Format("Parameter '{0}.{1}': {2}", sContext.Replace(" ", ""), pParameter.sName, pParameter.sDescription), BotLogType.eWarning);
            return (false);
        }
        public void WriteLog(String sCode, BotLogProcess eProcess, Exception eException, BotLogType lType = BotLogType.eInfo, BotLogPriority ePriority = BotLogPriority.eNormal)
        {
            WriteLog(new BotLogEntry(sCode, eProcess, eException.Message, string.Empty, lType, ePriority));
        }
        public void WriteLog(String sCode, BotLogProcess eProcess, String sDescription, BotLogType lType = BotLogType.eInfo, BotLogPriority ePriority = BotLogPriority.eNormal)
        {
            WriteLog(new BotLogEntry(sCode, eProcess, sDescription, string.Empty, lType, ePriority));
        }
        public void WriteDebugLog(String sCode, BotLogProcess eProcess, String sDescription, WebRequestEx wRequest, BotLogType lType = BotLogType.eInfo, BotLogPriority ePriority = BotLogPriority.eNormal)
        {
            String sDebugInfo;

            if (wRequest.LastError == null)
                sDebugInfo = (String)wRequest.Received;
            else
                sDebugInfo = wRequest.LastError.sMessage;

            WriteDebugLog(sCode, eProcess, sDescription, sDebugInfo, lType, ePriority);
        }
        public void WriteDebugLog(String sCode, BotLogProcess eProcess, String sDescription, String sDebugInfo, BotLogType lType = BotLogType.eInfo, BotLogPriority ePriority = BotLogPriority.eNormal)
        {
            WriteLog(new BotLogEntry(sCode, eProcess, sDescription, sDebugInfo, lType, ePriority));
        }
        public void WriteLogVolatile(String sCode, BotLogProcess eProcess, String sDescription, Object oTag, BotLogType lType = BotLogType.eInfo, BotLogPriority ePriority = BotLogPriority.eNormal)
        {
            WriteLog(new BotLogEntry(sCode, eProcess, sDescription, string.Empty, lType, ePriority, oTag));
        }

        /// <summary>
        /// Entfernt einen Log-Eintrag.
        /// </summary>
        /// <param name="oTag">Übereinstimmender Tag des zu entfernen Eintrags.</param>
        void IBotLogSystem.RemoveLogEntry(object oTag)
        {
            BotEnv.Send(() => BotEnv.OnRemoveLog?.Invoke(oTag));
        }
        #endregion


        #region Management
        private void WriteLog(BotLogEntry bEntry)
        {
            BotEnv.Post(() => {
                Debug.Assert((lEntries != null), "[WL1302]");
                Debug.Assert((bEntry != null), "[WL1303]");

                if ((bEntry.IsVolatile == false) || (BotDebugMode.LogFileWriteVolatile))
                {
                    // Eintrag hinzufügen:
                    int iLimitCount = (lEntries.Count - ARRAY_MAX_ENTRY_COUNT);

                    lEntries.Add(bEntry);

                    if (iLimitCount > 0)
                        lEntries.RemoveRange(0, iLimitCount);

                    // Eintrag in Datei schreiben:
                    sFile.WriteLine(bEntry.FormatCSVFileEntry());

                    if ((bEntry.HasDebugInfo) && (BotDebugMode.LogFileWriteDebugInfo))
                        sFile.WriteLine(bEntry.FormatCSVFileDebugEntry());

                    sFile.Flush();
                }

                BotEnv.OnWriteLog?.Invoke(bEntry);
            });
        }
        #endregion


        private StreamWriter sFile = null;
    }
    #endregion
}
