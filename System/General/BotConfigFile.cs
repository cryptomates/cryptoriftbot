﻿using CryptoRiftBot.Trading.Services;
using CryptoRiftBot.Trading.Strategy;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Shared.Utils.Core;

namespace CryptoRiftBot.Config
{
    #region Enumerations
    public enum BotContentType
    {
        /// <summary>
        /// Datei beinhaltet die Konfiguration der Applikation.
        /// </summary>
        [Description("Konfiguration")] eConfig,
        /// <summary>
        /// Datei beinhaltet Rift-Informationen.
        /// </summary>
        [Description("Rift-Zusammenfassung")] eRiftSummary
    };
    #endregion


    #region Types
    internal class BotConfigFile
    {
        #region Properties.Management
        public static BotConfig Configuration { private set; get; }
        #endregion


        #region Management
        internal static void Init(Type tConfigType)
        {
            Configuration = (BotConfig)Activator.CreateInstance(tConfigType);
        }
        public static bool WriteFile(BotContentType bContent)
        {
            StreamWriter sFile = new StreamWriter(GetFilePath(bContent), false);
            StringBuilder sBuilder = new StringBuilder();
            bool bResult = false;

            // Datei öffnen:
            try
            {
                // Inhalt schreiben:
                using (StringWriter sWriter = new StringWriter(sBuilder))
                using (JsonWriter jWriter = new JsonTextWriter(sWriter))
                {
                    jWriter.Formatting = Formatting.Indented;

                    jWriter.WriteStartObject();
                    {
                        jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_VERSION);
                        jWriter.WriteValue(FILE_VERSION);

                        if (bContent == BotContentType.eConfig)
                        {
                            // Allgemeine Einstellungen speichern:
                            jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_SETTINGS);
                            jWriter.WriteStartObject();
                            {
                                // Einstellungen für Bot-Automatic speichern:
                                BotAutomatic.Parameter.WriteFile(jWriter);

                                // Write settings object:
                                Configuration.Write(jWriter);

                                jWriter.WriteEndObject();
                            }

                            // Service-Informationen speichern:
                            jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_SERVICES);
                            jWriter.WriteStartObject();
                            {
                                BotServiceManager.WriteFile(jWriter);

                                jWriter.WriteEndObject();
                            }

                            // Parameter speichern:
                            jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_PARAMETERS);
                            jWriter.WriteStartObject();
                            {
                                // Settings speichern:
                                jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_SETTINGS);
                                jWriter.WriteStartArray();
                                {
                                    BotParameterManager.WriteFile(jWriter, typeof(BotStrategySettings));
                                    jWriter.WriteEnd();
                                }

                                // Selektoren speichern:
                                jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_SELECTORS);
                                jWriter.WriteStartArray();
                                {
                                    BotParameterManager.WriteFile(jWriter, typeof(BotSelectorParameters));
                                    jWriter.WriteEnd();
                                }

                                jWriter.WriteEndObject();
                            }
                        }

                        // Trading-Bots speichern:
                        jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_TBOTS);
                        jWriter.WriteStartArray();
                        {
                            for (int i = 0; i < BotTradeBot.TradeBots.Count; i++)
                                BotTradeBot.TradeBots[i].WriteFile(jWriter, bContent);

                            jWriter.WriteEnd();
                        }

                        jWriter.WriteEndObject();
                    }
                }

                bResult = true;
            }
            catch (Exception eExcpt)
            {
                BotEnv.LogSystem.ShowException("RF70", BotLogProcess.eConfig, eExcpt);
            }

            sFile.WriteLine(sBuilder.ToString());
            sFile.Close();

            return (bResult);
        }
        public static bool ReadFile(BotContentType bContent)
        {
            bool bResult = false;

            try
            {
                // Datei öffnen:
                StreamReader sFile = new StreamReader(GetFilePath(bContent));

                // Inhalt der Datei in JSON-Format umwandeln:
                dynamic dObjects = JsonConvert.DeserializeObject<dynamic>(sFile.ReadToEnd());
                if (dObjects != null)
                {
                    uint iFileVersion = (uint)dObjects[BotConvention.DATAFILE_JSON_VERSION];
                    if (iFileVersion <= FILE_VERSION)
                    {
                        dynamic _bObj;
                        if (bContent == BotContentType.eConfig)
                        {
                            // Allgemeine Einstellungen lesen:
                            _bObj = dObjects[BotConvention.DATAFILE_JSON_SETTINGS];
                            if (_bObj != null)
                            {
                                bResult = true;

                                // Einstellungen für Bot-Automatic speichern:
                                BotAutomatic.Parameter.ReadFile(_bObj);

                                // Read settings object:
                                Configuration.Read(_bObj);
                            }

                            // Service-Informationen lesen:
                            _bObj = dObjects[BotConvention.DATAFILE_JSON_SERVICES];
                            if (_bObj != null)
                                BotServiceManager.ReadFile(_bObj);

                            // Parameter lesen:
                            dynamic dParameters = dObjects[BotConvention.DATAFILE_JSON_PARAMETERS];
                            if (dParameters != null)
                            {
                                BotParameterManager.Reset();

                                // Settings lesen:
                                _bObj = dParameters[BotConvention.DATAFILE_JSON_SETTINGS];
                                if (_bObj != null)
                                    BotParameterManager.ReadFile(_bObj, typeof(BotStrategySettings));

                                // Selektoren lesen:
                                _bObj = dParameters[BotConvention.DATAFILE_JSON_SELECTORS];
                                if (_bObj != null)
                                    BotParameterManager.ReadFile(_bObj, typeof(BotSelectorParameters));
                            }
                        }
                        else
                            bResult = true;

                        // Trading-Bots lesen:
                        dynamic bObjBots = dObjects[BotConvention.DATAFILE_JSON_TBOTS];
                        if ((bResult) && (bObjBots != null) && (bObjBots.Count > 0))
                        {
                            // Einzelne Trading-Bots auslesen:
                            string _sDomain;
                            BotTradeBot _bBot;
                            for (int i = 0; i < bObjBots.Count; i++)
                            {
                                _sDomain = bObjBots[i][BotConvention.DATAFILE_JSON_PLATFORM][BotConvention.DATAFILE_JSON_DOMAIN];
                                _bBot = BotTradeBot.TradeBots.First(obj => ((BotTradeBot)obj).ReferredPlatform.PlatformDomain == _sDomain);
                                if ((_bBot == null) || (_bBot.ReadFile(bObjBots[i], bContent) == false))
                                {
                                    bResult = false;
                                    break;
                                }
                            }
                        }
                        else
                            bResult = false;

                        if (bResult == false)
                        {
                            BotEnv.LogSystem.ShowResult("RF139", BotLogProcess.eConfig, String.Format("Inhalt der Datei '{0}' ungültig!", UtlEnum.GetEnumDescription(bContent)));

                            // Programm beenden:
                            Application.Exit();
                        }
                    }
                    else
                        BotEnv.LogSystem.ShowResult("RF146", BotLogProcess.eConfig, String.Format("Ungültige Version der Datei '{0}'!", UtlEnum.GetEnumDescription(bContent)));
                }
                else
                    BotEnv.LogSystem.ShowResult("RF149", BotLogProcess.eConfig, String.Format("Ungültiges Format der Datei '{0}'!", UtlEnum.GetEnumDescription(bContent)));

                sFile.Close();
            }
            catch (FileNotFoundException)
            {
                // Nichts weiter unternehmen.
            }
            catch (IOException iExcpt)
            {
                BotEnv.LogSystem.ShowException("RF159", BotLogProcess.eConfig, iExcpt);
            }

            BotTradeBot.ValidateActiveParameters();

            // Ggf. unterdrückte Meldungen anzeigen:
            BotMemoryEncryption.ShowDecryptionPromt = true;

            return (bResult);
        }
        #endregion


        #region Helper
        private static String GetFilePath(BotContentType bContent)
        {
            String _sName;
            switch (bContent)
            {
                case BotContentType.eConfig: _sName = "Config"; break;
                case BotContentType.eRiftSummary: _sName = "RiftSummary"; break;

                default:
                    return (string.Empty);
            }
            return (String.Format("{0}\\{1}.{2}", AppContext.BaseDirectory, _sName, BotConvention.FILE_EXT));
        }
        #endregion


        #region Configuration
        internal const uint FILE_VERSION = 1;
        #endregion
    }
    public class BotConfig
    {
        #region Constants
        private const String DEFAULT_REPORTS_DIRECTORY = ".\\Reports";
        private const String DEFAULT_COINSTREAM_DIRECTORY = ".\\CoinStream";
        #endregion


        #region Properties
        /// <summary>
        /// Verzeichnis zum Ablegen von Reports (Optional).
        /// </summary>
        [Browsable(false)]
        public String Reports_Directory
        {
            get { return sReports_Directory; }
            set { sReports_Directory = value.EnsureEndsWith("\\"); }
        }
        private String sReports_Directory;
        /// <summary>
        /// Verzeichnis zum Ablegen von Coin-Stream Records (Optional).
        /// </summary>
        [Browsable(false)]
        public String CoinStream_Directory
        {
            get { return sCoinStream_Directory; }
            set { sCoinStream_Directory = value.EnsureEndsWith("\\"); }
        }
        private String sCoinStream_Directory;
        /// <summary>
        /// Verzögerung für Abspielen von Coin-Stream Replays.
        /// </summary>
        [Browsable(false)]
        public double CoinStream_Delay
        {
            get { return dCoinStream_Delay; }
            set
            {
                if (value == 0)
                    // Verzögerung undefiniert:
                    dCoinStream_Delay = value;
                else
                    dCoinStream_Delay = Math.Max(BotCoinStreamPlayer.TIMER_DELAY_PLAY_NORMAL, value);
            }
        }
        private double dCoinStream_Delay;
        #endregion


        public BotConfig()
        {
            Reports_Directory = UtlPath.GetAbsolutePath(DEFAULT_REPORTS_DIRECTORY);
            CoinStream_Directory = UtlPath.GetAbsolutePath(DEFAULT_COINSTREAM_DIRECTORY);
            CoinStream_Delay = BotCoinStreamPlayer.TIMER_DELAY_PLAY_NORMAL;
        }


        #region Configuration
        public virtual void Write(JsonWriter jWriter)
        {
            jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_DEBUGMODE_ENABLED);
            jWriter.WriteValue(BotDebugMode.Active);
            jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_REPORTS_DIRECTORY);
            jWriter.WriteValue(UtlPath.GetRelativePath(Reports_Directory));
            jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_COINSTREAM_DIRECTORY);
            jWriter.WriteValue(UtlPath.GetRelativePath(CoinStream_Directory));
            jWriter.WritePropertyName(BotConvention.DATAFILE_JSON_COINSTREAM_DELAY);
            jWriter.WriteValue(CoinStream_Delay);
        }
        public virtual bool Read(JObject dObjects)
        {
            Reports_Directory = UtlPath.GetAbsolutePath((string)dObjects.ParseValue(BotConvention.DATAFILE_JSON_REPORTS_DIRECTORY, DEFAULT_REPORTS_DIRECTORY));
            CoinStream_Directory = UtlPath.GetAbsolutePath((string)dObjects.ParseValue(BotConvention.DATAFILE_JSON_COINSTREAM_DIRECTORY, DEFAULT_COINSTREAM_DIRECTORY));
            CoinStream_Delay = dObjects.ParseValue(BotConvention.DATAFILE_JSON_COINSTREAM_DELAY, CoinStream_Delay);

            if ((bool)dObjects.ParseValue(BotConvention.DATAFILE_JSON_DEBUGMODE_ENABLED, false))
                BotDebugMode.Active = true;

            return (true);
        }
        #endregion
    }
    #endregion
}
