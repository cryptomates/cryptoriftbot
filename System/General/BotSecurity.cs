﻿using CryptoRiftBot.Trading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Net.NetworkInformation;
using System.Windows.Forms;
using Shared.Utils.Core;
using Shared.Security.Cryptography;

namespace CryptoRiftBot
{
    public class BotMemoryEncryption
    {
        #region Enumerations
        public enum BotProcessType
        {
            [Description("Verschlüsseln")]
            eEncrypt = 0,
            [Description("Bestätigen")]
            eConfirm = 1,
            [Description("Entschlüsseln")]
            eDecrypt = 2
        }
        public enum BotDataType
        {
            Invalid = 0,

            eAPIKey = 1,
            eAPISecret = 2,
            eMACAdress = 3
        }
        #endregion


        #region Properties.Management
        [Browsable(false)]
        public static bool IsReady { get { return (sPassword.Length > 0); } }
        [Browsable(false)]
        public static bool ShowDecryptionPromt { set; get; } = true;
        #endregion


        #region Management
        public static bool ConfirmPassword()
        {
            if (bConfirmed)
                return (true);
            else
            {
                if (string.IsNullOrEmpty(sPassword))
                {
                    if (EnterUserPassword(out sPassword, BotProcessType.eEncrypt) == false)
                        return (false);
                }
                if (!string.IsNullOrEmpty(sPassword))
                {
                    String sConfirmInput;

                    // Passwort wiederholen:
                    while (true)
                    {
                        if (EnterUserPassword(out sConfirmInput, BotProcessType.eConfirm))
                        {
                            if (sPassword == sConfirmInput)
                            {
                                // Passwort übernehmen:
                                bConfirmed = true;

                                return (true);
                            }
                            else
                                BotEnv.LogSystem.ShowResult("RF116", BotLogProcess.eEncryption, "Bestätigung der Eingabe ungültig! Bitte wiederholen.");
                        }
                        else
                        {
                            // Vorgang abgebrochen:
                            Reset();

                            break;
                        }
                    }
                }
                return (false);
            }
        }
        public static bool Encrypt(String sOriginValue, BotDataType dDataType, out String sResult)
        {
            sResult = "";

            if ((sOriginValue != "") && (Init()))
            {
                sResult = RijndaelEncryption.EncryptString(sOriginValue, sPassword, dSalts[dDataType]);
                return (true);
            }
            else
                return (false);
        }
        public static bool Decrypt(String sEncryptedValue, BotDataType dDataType, out String sResult)
        {
            sResult = string.Empty;
            if (string.IsNullOrEmpty(sEncryptedValue))
                return (true);
            else
            {
                if (Init())
                {
                    try
                    {
                        sResult = RijndaelEncryption.DecryptString(sEncryptedValue, sPassword, dSalts[dDataType]);
                        return (true);
                    }
                    catch (System.Security.Cryptography.CryptographicException cExcpt)
                    {
                        // Eingabe-Informationen zurücksetzen, da diese anscheinend fehlerhaft sind:
                        Reset();

                        BotEnv.LogSystem.WriteLog("D99", BotLogProcess.eEncryption, "Das eingegebene Passwort ist ungültig!", BotLogType.eWarning, BotLogPriority.eHigh);
                    }
                }
            }
            return (false);
        }


        private static bool Init()
        {
            if (dSalts == null)
            {
                // Seeds pro Prozess-Typ erzeugen:
                dSalts = new Dictionary<BotDataType, byte[]>();

                dSalts[BotDataType.eAPIKey] = new byte[] { 0x93, 0x3c, 0x22, 0x9e, 0x88, 0x84, 0x1f, 0x69, 0x7a, 0x17, 0x14, 0x3f, 0x75, 0x71, 0x1e, 0x06 };
                dSalts[BotDataType.eAPISecret] = new byte[] { 0x10, 0x19, 0x40, 0x28, 0x0b, 0x05, 0x1a, 0x79, 0x29, 0x15, 0x21, 0x81, 0x3d, 0x87, 0x79, 0x4f };
            }

            if ((IsReady == false) && (ShowDecryptionPromt))
            {
                ShowDecryptionPromt = false;
                EnterUserPassword(out sPassword, BotProcessType.eDecrypt);
            }

            return (IsReady);
        }
        private static void Reset()
        {
            bConfirmed = false;
            sPassword = string.Empty;
            dSalts = null;
        }
        #endregion


        #region UserInput
        private static bool EnterUserPassword(out String sResult, BotProcessType pProcessType)
        {
            sResult = string.Empty;

            if (BotEnv.OnEnterPassword == null)
            {
                BotEnv.LogSystem.WriteDebugLog("EUP207", BotLogProcess.eAccount, "Aufrufen der Passworteingabe fehlgeschlagen!", "", BotLogType.eError, BotLogPriority.eHigh);
                return (false);
            }
            else
                return (BotEnv.OnEnterPassword(BotConvention.TITLE_SECURITY_QUESTION, String.Format("Passwort zum {0} der Zugangsdaten eingeben.", UtlEnum.GetEnumDescription(pProcessType)), out sResult));
        }
        #endregion


        private static Dictionary<BotDataType, byte[]> dSalts = null;
        private static bool bConfirmed = false;
        private static String sPassword = "";
    }

    public class BotEncryptedObject
    {
        #region Enumerations
        public enum BotState
        {
            eInvalid = 0,
            eReady = 1,                            // Daten liegen verschlüsselt vor, können aber bei Anwendung entschlüsselt werden
            eEncrypted = 2                         // Daten liegen verschlüsselt vor und können NICHT entschlüsselt werden
        }
        #endregion


        public BotEncryptedObject(BotMemoryEncryption.BotDataType dDataType)
        {
            this.dDataType = dDataType;
            this.State = BotState.eInvalid;
        }


        #region Properties.Management
        [Browsable(false)]
        public BotState State { private set; get; }
        #endregion


        #region Management
        public bool SetValue(String sValue)
        {
            Debug.Assert((dDataType != BotMemoryEncryption.BotDataType.Invalid), "[SV250]");

            // Ggf. Passwort bestätigen:
            if ((BotMemoryEncryption.ConfirmPassword()) && (BotMemoryEncryption.Encrypt(sValue, dDataType, out sValue)))
            {
                this.sValue = sValue;
                this.State = BotState.eEncrypted;

                return (true);
            }
            else
                return (false);
        }
        public bool GetValue(out String sResult)
        {
            bool bResult = false;

            if (State == BotState.eReady)
                bResult = BotMemoryEncryption.Decrypt(sValue, dDataType, out sResult);
            else
                // Daten können nicht entschlüsselt werden:
                sResult = "";

            return (bResult);
        }
        #endregion


        #region Configuration
        public bool WriteFile(JsonWriter jWriter, String sPropertyName)
        {
            if (State != BotState.eInvalid)
            {
                // Verschlüsselte Daten schreiben:
                jWriter.WritePropertyName(sPropertyName);
                jWriter.WriteValue(sValue);

                return (true);
            }
            else
                return (false);
        }
        public bool ReadFile(JObject dObjects, String sPropertyName)
        {
            sValue = dObjects.ParseValue(sPropertyName, "");

            if (sValue.Length > 0)
            {
                String sTemp;

                if (BotMemoryEncryption.Decrypt(sValue, dDataType, out sTemp))
                {
                    State = BotState.eReady;
                    return (true);
                }
                else
                    // Informationen konnten nicht entschlüsselt werden:
                    State = BotState.eEncrypted;
            }
            else
                State = BotState.eInvalid;

            return (false);
        }
        #endregion


        private BotMemoryEncryption.BotDataType dDataType;
        private String sValue;
    }
}
