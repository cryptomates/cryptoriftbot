﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using Shared.Utils.Core;

namespace CryptoRiftBot
{
    // Zeigt Informationen zu aktuellen Zuständen an.
    public class BotTaskState
    {
        #region Enumerations
        public enum BotClass
        {
            [Description("Initialisation")]
            eInit,
            [Description("Coin-Bewertung aktualisieren")]
            eCurrencyRating,
            [Description("Excel Dokument schreiben")]
            eExcelDoc,
        }
        #endregion


        #region Types
        public class BotClassInfo
        {
            public BotClassInfo(String sDescription)
            {
                Description = sDescription;
                UseCount = 0;
            }


            #region Properties
            [Browsable(false)]
            public String Description { private set; get; }
            [Browsable(false)]
            public int UseCount { private set; get; }
            #endregion


            #region Management
            public void Use()
            {
                Debug.Assert((UseCount >= 0), String.Format("[U1718] Ungültige Nutzung von '{0}'!", Description));

                UseCount++;
            }
            public void Unse()
            {
                UseCount--;
            }
            #endregion
        }
        #endregion


        #region Properties.Management
        [Browsable(false)]
        public static IEnumerable<KeyValuePair<BotClass, BotClassInfo>> CurUsage { private set; get; }
        [Browsable(false)]
        public static int CurTotalCount { private set; get; }
        #endregion
        #region Properties
        [Browsable(false)]
        public static bool IsBusy { get { return (CurTotalCount > 0); } }
        #endregion


        #region Management
        public static void Init()
        {
            BotTaskState.iProcessed = 0;

            // Classen initialisieren:
            Array aClasses = Enum.GetValues(typeof(BotClass));
            foreach (BotClass _bClass in aClasses)
                dClassInfo[_bClass] = new BotClassInfo(UtlEnum.GetEnumDescription(_bClass));
        }

        public static void Use(BotClass cClass)
        {
            BotEnv.Post(() => {
                dClassInfo[cClass].Use();
                NotifyStateChange();
            });
        }
        public static void Unuse(BotClass cClass)
        {
            BotEnv.Post(() => {
                iProcessed++;
                dClassInfo[cClass].Unse();
                NotifyStateChange();
            });
        }
        #endregion


        #region Notification
        private static void NotifyStateChange()
        {
            int iCount;

            CurUsage = dClassInfo.Where(_bInfo => (_bInfo.Value.UseCount > 0));
            iCount = CurUsage.Count();

            if (iCount == 0)
                // Alle Vorgänge abgeschlossen:
                iProcessed = 0;

            // Benachrichtigen:
            CurTotalCount = (iCount + iProcessed);

            BotEnv.OnTaskStateChanged?.Invoke();
        }
        #endregion


        private static Dictionary<BotClass, BotClassInfo> dClassInfo = new Dictionary<BotClass, BotClassInfo>();
        private static int iProcessed;
    }
}
