﻿using CryptoRiftBot.Trading.Services;
using CryptoRiftBot.Trading.Strategy;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using Shared.Utils.Core;
using Excel = Microsoft.Office.Interop.Excel;
using CryptoRiftBot.Trading;
using System.Reflection;
using static Shared.Utils.Core.UtlComponentModel;

namespace CryptoRiftBot
{
    public class BotStateShotExcelDoc : BotSingleUsageExcelDoc
    {
        #region Types
        public class BotObjectInfo
        {
            public BotObjectInfo(Type tType, int iColumn, String sName, String sValue, String sCategory = "")
            {
                this.tType = tType;
                this.iColumn = iColumn;
                this.sName = sName;
                this.sValue = sValue;
                this.sCategory = sCategory;
            }


            public Type tType;
            public int iColumn;
            public String sCategory;

            public String sName;
            public String sValue;
        }


        public BotStateShotExcelDoc(BotTradingStrategy bStrategy) : base("Stateshots", bStrategy, "yyyy-MM-dd_HH-mm-ss")
        {
            InitDocument(DateTime.Now.Year.ToString());
        }
        #endregion


        #region Management
        public override bool Execute(object oArgument = null)
        {
            if (IsValid())
            {
                Bitmap bChartImage = (Bitmap)oArgument;
                Excel.Worksheet eCurSheet;
                Excel.Range _rRange;
                int iIndex;
                object[,] oCurData;

                if (bChartImage != null)
                {
                    eDefaultSheet.Activate();

                    // Bitmap einfügen:
                    Excel.Range eImageRange = eDefaultSheet.get_Range((Excel.Range)eDefaultSheet.Cells[(PROP_SHEET_ROW_DATA + 1), 1], (Excel.Range)eDefaultSheet.Cells[(PROP_SHEET_ROW_DATA + 2), 1]);

                    eDefaultSheet.Paste(eImageRange, bChartImage);

                    bChartImage = null;
                }

                // Log-Einträge:
                eCurSheet = CreateWorksheet("Log");
                oCurData = FormatExcelArray();

                SetFormat_DateTime(eCurSheet, (BotLogSystem.CTRL_COL_TIMESTAMP + 1));

                // Daten übertragen:
                _rRange = eCurSheet.get_Range((Excel.Range)eCurSheet.Cells[(PROP_SHEET_ROW_TITLES + 1), 1], (Excel.Range)eCurSheet.Cells[(PROP_SHEET_ROW_TITLES + 1 + BotEnv.LogSystem.Entries.Count), BotEnv.LogSystem.Columns.Length]);
                _rRange.Value = oCurData;
                _rRange.EntireColumn.AutoFit();

                // Objekt-Informationen übertragen:
                InsertObjectInfo("Trading-Bot Automatik", BotAutomatic.Parameter);
                InsertObjectInfo("Parameter", Strategy.ActiveSettingsClone);
                InsertObjectInfo("Strategie", Strategy);
                InsertObjectInfo("Plattform", Strategy.Platform);

                // Wallet-Informationen:
                List<BotWallet> wWallets = Strategy.Platform.Wallets;

                eCurSheet = CreateWorksheet("Wallets");
                oCurData = new object[(wWallets.Count + PROP_SHEET_ROW_DATA), PROP_SHEET_WALLET_COL_COUNT];

                // Spalten erzeugen:
                oCurData[PROP_SHEET_ROW_TITLES, PROP_SHEET_WALLET_COL_CURRENCY] = "Coin";
                oCurData[PROP_SHEET_ROW_TITLES, PROP_SHEET_WALLET_COL_AMOUNT] = "Guthaben";

                // Daten aufbereiten:
                for (int i = 0; i < wWallets.Count; i++)
                {
                    iIndex = (i + 1);

                    oCurData[iIndex, PROP_SHEET_WALLET_COL_CURRENCY] = UtlEnum.GetEnumDescription(wWallets[i].Currency);
                    oCurData[iIndex, PROP_SHEET_WALLET_COL_AMOUNT] = wWallets[i].Funds;
                }

                // Daten übertragen:
                _rRange = eCurSheet.get_Range((Excel.Range)eCurSheet.Cells[(PROP_SHEET_ROW_TITLES + 1), 1], (Excel.Range)eCurSheet.Cells[(PROP_SHEET_ROW_TITLES + 1 + wWallets.Count), (PROP_SHEET_WALLET_COL_AMOUNT + 1)]);
                _rRange.Value = oCurData;

                return (true);
            }
            else
                return (false);
        }
        #endregion
        #region Management
        private void InsertObjectInfo(String sName, object oObject)
        {
            if (oObject != null)
            {
                // Initialisieren:
                lObjectInfo = new List<BotObjectInfo>();
                iLastColumn = 0;

                Type[] tExclude = new Type[] { typeof(BotServiceDescriptorAttribute) };
                UtlComponentModel.EvaluateObject(oObject, OnEvaluateCallback, null, tExclude);

                if (lObjectInfo.Count > 0)
                {
                    Excel.Worksheet eCurSheet = CreateWorksheet(sName);
                    Excel.Range _rRange;
                    int iIndex, iColValue = (PROP_SHEET_INFO_COL_PROPERTY + iLastColumn + 1);

                    // Initialisieren:
                    object[,] oCurData = new object[(lObjectInfo.Count + PROP_SHEET_ROW_DATA), (iColValue + 1)];

                    // Spalten erzeugen:
                    oCurData[PROP_SHEET_ROW_TITLES, PROP_SHEET_INFO_COL_TYPE] = "Typ";
                    oCurData[PROP_SHEET_ROW_TITLES, PROP_SHEET_INFO_COL_CATEGORY] = "Kategorie";
                    oCurData[PROP_SHEET_ROW_TITLES, PROP_SHEET_INFO_COL_PROPERTY] = "Property";
                    oCurData[PROP_SHEET_ROW_TITLES, iColValue] = "Wert";

                    // Daten aufbereiten:
                    for (int i = 0; i < lObjectInfo.Count; i++)
                    {
                        iIndex = (i + 1);

                        if (lObjectInfo[i].tType != null)
                            oCurData[iIndex, PROP_SHEET_INFO_COL_TYPE] = lObjectInfo[i].tType.Name;

                        oCurData[iIndex, PROP_SHEET_INFO_COL_CATEGORY] = lObjectInfo[i].sCategory;
                        oCurData[iIndex, (PROP_SHEET_INFO_COL_PROPERTY + lObjectInfo[i].iColumn)] = lObjectInfo[i].sName;
                        oCurData[iIndex, iColValue] = lObjectInfo[i].sValue;
                    }

                    // Daten übertragen:
                    _rRange = eCurSheet.get_Range((Excel.Range)eCurSheet.Cells[(PROP_SHEET_ROW_TITLES + 1), 1], (Excel.Range)eCurSheet.Cells[(PROP_SHEET_ROW_TITLES + 1 + lObjectInfo.Count), (iColValue + 1)]);

                    _rRange.NumberFormat = "@";
                    _rRange.EntireColumn.Style.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;

                    _rRange.Value = oCurData;

                    // Spaltenbreiten definieren:
                    _rRange = eCurSheet.get_Range((Excel.Range)eCurSheet.Cells[1, 1], (Excel.Range)eCurSheet.Cells[1, (PROP_SHEET_INFO_COL_CATEGORY + 1)]);
                    _rRange.EntireColumn.AutoFit();
                    _rRange = eCurSheet.get_Range((Excel.Range)eCurSheet.Cells[1, (PROP_SHEET_INFO_COL_PROPERTY + 1)], (Excel.Range)eCurSheet.Cells[1, (PROP_SHEET_INFO_COL_PROPERTY + 1 + iLastColumn)]);
                    _rRange.ColumnWidth = 3;
                    _rRange = eCurSheet.get_Range((Excel.Range)eCurSheet.Cells[1, iColValue], (Excel.Range)eCurSheet.Cells[1, (iColValue + 1)]);
                    _rRange.ColumnWidth = 40;
                }
            }
        }
        #endregion


        #region Events
        private EvaluationResult OnEvaluateCallback(PropertyInfo pInfo, String sName, String sValue, int iDepth, ref Object oTag)
        {
            string _sCategory;
            pInfo.TryGetAttributeValue<CategoryAttribute>(out _sCategory);
            lObjectInfo.Add(new BotObjectInfo(pInfo.PropertyType, iDepth, sName, sValue, _sCategory));

            iLastColumn = Math.Max(iLastColumn, iDepth);

            return (EvaluationResult.eSuccess);
        }
        #endregion


        #region Configuration
        internal const int PROP_SHEET_INFO_COL_TYPE = 0;
        internal const int PROP_SHEET_INFO_COL_CATEGORY = 1;
        internal const int PROP_SHEET_INFO_COL_PROPERTY = 2;

        internal const int PROP_SHEET_WALLET_COL_COUNT = 2;
        internal const int PROP_SHEET_WALLET_COL_CURRENCY = 0;
        internal const int PROP_SHEET_WALLET_COL_AMOUNT = 1;


        internal List<BotObjectInfo> lObjectInfo;
        internal int iLastColumn;
        #endregion


        #region Helper
        private object[,] FormatExcelArray(bool bUseTitles = true)
        {
            int iIndex, iRowOffset = (bUseTitles ? 1 : 0);
            object[,] oCurData = new object[(BotEnv.LogSystem.Entries.Count + iRowOffset), BotEnv.LogSystem.Columns.Length];
            BotLogEntry _bEntry;

            if (bUseTitles)
            {
                // Spalten erzeugen:
                for (int i = 0; i < BotEnv.LogSystem.Columns.Length; i++)
                    oCurData[0, i] = BotEnv.LogSystem.Columns[i];
            }

            // Daten aufbereiten:
            for (int i = 0; i < BotEnv.LogSystem.Entries.Count; i++)
            {
                _bEntry = BotEnv.LogSystem.Entries[i];
                iIndex = (i + iRowOffset);

                oCurData[iIndex, BotLogSystem.CTRL_COL_TIMESTAMP] = String.Format(BotSingleUsageExcelDoc.FORMAT_DATETIME, _bEntry.TimeStamp);
                oCurData[iIndex, BotLogSystem.CTRL_COL_CODE] = _bEntry.Code;
                oCurData[iIndex, BotLogSystem.CTRL_COL_TYPE] = _bEntry.TypeTitle;
                oCurData[iIndex, BotLogSystem.CTRL_COL_PROCESS] = _bEntry.Process;
                oCurData[iIndex, BotLogSystem.CTRL_COL_DESCRIPTION] = _bEntry.Description;
            }

            return (oCurData);
        }
        #endregion
    }

    public class BotSubscriptionExcelDoc : BotSingleUsageExcelDoc
    {
        public BotSubscriptionExcelDoc(String sCategory, BotTradingStrategy bStrategy) : base(sCategory, bStrategy, "yyyy-MM-dd_HH-mm-ss")
        {
            InitDocument();
        }


        #region Management
        public override bool Execute(object oArgument)
        {
            BotBaseSubscription bSubscription = (BotBaseSubscription)oArgument;
            bool bResult = false;

            if (IsValid())
            {
                Excel.Worksheet eCurSheet = CreateWorksheet();
                Excel.Range eCurRange = null;
                Excel.XlChartType eChartType = 0;
                object[,] oCurData = null;

                SetFormat_TimeSpan(eCurSheet, (PROP_SHEET_CANDLES_COL_TIME + 1));

                // Schritt 1:
                // > Sheet für Daten erstellen
                switch (bSubscription.GetSubscriptionType())
                {
                    case BotSubscriptionType.eTicker:
                        BotTickerSubscription tTicker = (BotTickerSubscription)bSubscription;
                        break;

                    case BotSubscriptionType.eCandles:
                        BotCandleSubscription cCandle = (BotCandleSubscription)bSubscription;
                        int _iRow = PROP_SHEET_ROW_DATA;

                        lock (cCandle.oThreadLockObj)
                        {
                            // Initialisieren:
                            eCurSheet.Name = "Candles";

                            oCurData = new object[(cCandle.lData.Count + PROP_SHEET_ROW_DATA), PROP_SHEET_CANDLES_COLS];

                            // Spalten erzeugen:
                            oCurData[PROP_SHEET_ROW_TITLES, PROP_SHEET_CANDLES_COL_TIME] = "Time";
                            oCurData[PROP_SHEET_ROW_TITLES, PROP_SHEET_CANDLES_COL_VOLUME] = "Volume";
                            oCurData[PROP_SHEET_ROW_TITLES, PROP_SHEET_CANDLES_COL_OPEN] = "Open";
                            oCurData[PROP_SHEET_ROW_TITLES, PROP_SHEET_CANDLES_COL_HIGH] = "High";
                            oCurData[PROP_SHEET_ROW_TITLES, PROP_SHEET_CANDLES_COL_LOW] = "Low";
                            oCurData[PROP_SHEET_ROW_TITLES, PROP_SHEET_CANDLES_COL_CLOSE] = "Close";

                            // Daten aufbereiten:
                            for (int i = 0; i < cCandle.lData.Count; i++, _iRow++)
                            {
                                oCurData[_iRow, PROP_SHEET_CANDLES_COL_TIME] = String.Format(FORMAT_TIMESPAN, cCandle.lData[i].TimeStamp);
                                oCurData[_iRow, PROP_SHEET_CANDLES_COL_VOLUME] = cCandle.lData[i].dVolume;
                                oCurData[_iRow, PROP_SHEET_CANDLES_COL_OPEN] = cCandle.lData[i].dOpen;
                                oCurData[_iRow, PROP_SHEET_CANDLES_COL_HIGH] = cCandle.lData[i].dHigh;
                                oCurData[_iRow, PROP_SHEET_CANDLES_COL_LOW] = cCandle.lData[i].dLow;
                                oCurData[_iRow, PROP_SHEET_CANDLES_COL_CLOSE] = cCandle.lData[i].dClose;
                            }

                            // Daten übertragen:
                            Excel.Range _rRange;

                            _rRange = eCurSheet.get_Range((Excel.Range)eCurSheet.Cells[(PROP_SHEET_ROW_TITLES + 1), 1], (Excel.Range)eCurSheet.Cells[(PROP_SHEET_ROW_TITLES + 1 + cCandle.lData.Count), PROP_SHEET_CANDLES_COLS]);
                            _rRange.Value = oCurData;

                            // Range für Chart-Daten definieren:
                            eChartType = Excel.XlChartType.xlStockVOHLC;
                            eCurRange = eCurSheet.get_Range((Excel.Range)eCurSheet.Cells[(PROP_SHEET_ROW_TITLES + 1), (PROP_SHEET_CANDLES_COL_VOLUME + 1)], (Excel.Range)eCurSheet.Cells[(PROP_SHEET_ROW_TITLES + 1 + cCandle.lData.Count), (PROP_SHEET_CANDLES_COL_CLOSE + 1)]);
                        }
                        break;

                    default:
                        // (BETA) ...
                        Debug.Assert(false, "[ISD80] Unbehandelter Subscription-Type!");
                        break;
                }

                // Schritt 2:
                // > Chart erstellen
                if (oCurData != null)
                {
                    Excel.Chart eCurChart = AddChart(eChartType, eCurSheet.Name);

                    Debug.Assert((eCurRange != null), String.Format("[E290] Invalid Object '{0}'", nameof(eCurRange)));

                    eCurChart.SetSourceData(eCurRange, System.Reflection.Missing.Value);
                    oCurData = null;

                    bResult = true;
                }
            }

            return (bResult);
        }
        #endregion


        #region Configuration
        internal const int PROP_SHEET_CANDLES_COLS = 6;
        internal const int PROP_SHEET_CANDLES_COL_TIME = 0;
        internal const int PROP_SHEET_CANDLES_COL_VOLUME = 1;
        internal const int PROP_SHEET_CANDLES_COL_OPEN = 2;
        internal const int PROP_SHEET_CANDLES_COL_HIGH = 3;
        internal const int PROP_SHEET_CANDLES_COL_LOW = 4;
        internal const int PROP_SHEET_CANDLES_COL_CLOSE = 5;
        #endregion
    }
    public class BotRiftSummaryExcelDoc : BotSingleUsageExcelDoc
    {
        public BotRiftSummaryExcelDoc(UtlDateTime.RepeatType tMode) : base("Rift Summary")
        {
            this.tMode = tMode;
        }


        #region Management
        public override bool Execute(object oArgument = null)
        {
            BotTradingStrategy bStrategy = (BotTradingStrategy)oArgument;
            if (bStrategy == null)
            {
                // Fügt eine komplette Auflistung aller Rifts ein.
                if (IsValid() == false)
                {
                    sDefaultWorksheet = PROP_SHEET_CURRENCIES_NAME;

                    InitDocument();
                }
                if (IsValid())
                {
                    BotTradingStrategy _bStrategy;
                    BotPlatformService _bPlt;
                    BotRiftSummary _bSum;
                    Excel.Worksheet eCurSheet = CreateWorksheet(PROP_SHEET_PLATFORMS_NAME);
                    Excel.Range _rRange;
                    String _sPlt;
                    var dCoinProphit = new Dictionary<BotCoinType, double>();
                    object[,] oCurData;
                    int iStratCount, _iRow = (PROP_SHEET_ROW_DATA + 1);

                    // Profit-Map für Coins initialisieren:
                    Array aCoinVals = Enum.GetValues(typeof(BotCoinType));

                    foreach (BotCoinType _bType in aCoinVals)
                    {
                        if (_bType != BotCoinType.Invalid)
                            dCoinProphit[_bType] = 0;
                    }

                    // Zeilen reservieren für Titel, Strategien:
                    oCurData = new object[(PROP_SHEET_ROW_TITLES + 1), PROP_SHEET_PLATFORMS_COLS];

                    // Spalten erzeugen:
                    oCurData[PROP_SHEET_ROW_TITLES, PROP_SHEET_PLATFORMS_COL_PLATFORM] = "Plattform";
                    oCurData[PROP_SHEET_ROW_TITLES, PROP_SHEET_PLATFORMS_COL_STRATEGY] = "Paar";
                    oCurData[PROP_SHEET_ROW_TITLES, PROP_SHEET_PLATFORMS_COL_BUYS] = PROP_TITLE_BUYS;
                    oCurData[PROP_SHEET_ROW_TITLES, PROP_SHEET_PLATFORMS_COL_SELLS] = PROP_TITLE_SELLS;
                    oCurData[PROP_SHEET_ROW_TITLES, PROP_SHEET_PLATFORMS_COL_PROPHIT_VAL] = PROP_TITLE_PROPHIT;
                    oCurData[PROP_SHEET_ROW_TITLES, PROP_SHEET_PLATFORMS_COL_PROPHIT_EXCHANGE] = (PROP_TITLE_PROPHIT + " " + PROP_TITLE_EXCHANGE);
                    oCurData[PROP_SHEET_ROW_TITLES, PROP_SHEET_PLATFORMS_COL_BUYAMOUNT_VAL] = PROP_TITLE_BUYAMOUNT;
                    oCurData[PROP_SHEET_ROW_TITLES, PROP_SHEET_PLATFORMS_COL_BUYAVERAGERATE_VAL] = PROP_TITLE_BUYAVERAGERATE;
                    oCurData[PROP_SHEET_ROW_TITLES, PROP_SHEET_PLATFORMS_COL_SELLAMOUNT_VAL] = PROP_TITLE_SELLAMOUNT;
                    oCurData[PROP_SHEET_ROW_TITLES, PROP_SHEET_PLATFORMS_COL_SELLAVERAGERATE_VAL] = PROP_TITLE_SELLAVERAGERATE;

                    // Spalten übertragen:
                    _rRange = eCurSheet.get_Range((Excel.Range)eCurSheet.Cells[(PROP_SHEET_ROW_TITLES + 1), 1], (Excel.Range)eCurSheet.Cells[(PROP_SHEET_ROW_TITLES + 1), PROP_SHEET_PLATFORMS_COLS]);
                    _rRange.Value = oCurData;

                    // Schritt 1:
                    // > Zusammenfassungen sämtlicher Strategien einfügen:
                    for (int i = 0; i < BotTradeBot.TradeBots.Count; i++)
                    {
                        _bPlt = BotTradeBot.TradeBots[i].ReferredPlatform;

                        // Prüfen ob Platform Reports generieren darf:
                        if (_bPlt.IsNonDebugUsable)
                        {
                            iStratCount = BotTradeBot.TradeBots[i].Strategies.Count;
                            oCurData = new object[iStratCount, PROP_SHEET_PLATFORMS_COLS];
                            _sPlt = _bPlt.PlatformName;

                            for (int u = 0; u < iStratCount; u++)
                            {
                                _bStrategy = BotTradeBot.TradeBots[i].Strategies[u];
                                _bSum = _bStrategy.RiftSummaries.TotalRiftSummary[(int)tMode];

                                // Daten aufbereiten:
                                oCurData[u, PROP_SHEET_PLATFORMS_COL_PLATFORM] = _sPlt;
                                oCurData[u, PROP_SHEET_PLATFORMS_COL_STRATEGY] = _bSum.ReferredPair.Symbol;
                                oCurData[u, PROP_SHEET_PLATFORMS_COL_BUYS] = _bSum.Buys.Orders;
                                oCurData[u, PROP_SHEET_PLATFORMS_COL_SELLS] = _bSum.Sells.Orders;
                                oCurData[u, PROP_SHEET_PLATFORMS_COL_PROPHIT_VAL] = _bSum.Prophit;
                                oCurData[u, PROP_SHEET_PLATFORMS_COL_PROPHIT_CURRENCY] = _bSum.ReferredPair.SymbolPrim;
                                oCurData[u, PROP_SHEET_PLATFORMS_COL_PROPHIT_EXCHANGE] = _bSum.ProphitExchange;
                                oCurData[u, PROP_SHEET_PLATFORMS_COL_BUYAMOUNT_VAL] = _bSum.Buys.TotalAmount;
                                oCurData[u, PROP_SHEET_PLATFORMS_COL_BUYAMOUNT_CURRENCY] = _bSum.ReferredPair.SymbolPrim;
                                oCurData[u, PROP_SHEET_PLATFORMS_COL_BUYAVERAGERATE_VAL] = _bSum.Buys.TotalAverageRate;
                                oCurData[u, PROP_SHEET_PLATFORMS_COL_BUYAVERAGERATE_CURRENCY] = _bSum.ReferredPair.SymbolSec;
                                oCurData[u, PROP_SHEET_PLATFORMS_COL_SELLAMOUNT_VAL] = _bSum.Sells.TotalAmount;
                                oCurData[u, PROP_SHEET_PLATFORMS_COL_SELLAMOUNT_CURRENCY] = _bSum.ReferredPair.SymbolPrim;
                                oCurData[u, PROP_SHEET_PLATFORMS_COL_SELLAVERAGERATE_VAL] = _bSum.Sells.TotalAverageRate;
                                oCurData[u, PROP_SHEET_PLATFORMS_COL_SELLAVERAGERATE_CURRENCY] = _bSum.ReferredPair.SymbolSec;

                                dCoinProphit[_bStrategy.WalletSec.Currency] += _bSum.Prophit;
                            }

                            // Daten übertragen:
                            _rRange = eCurSheet.get_Range((Excel.Range)eCurSheet.Cells[_iRow, 1], (Excel.Range)eCurSheet.Cells[(_iRow + iStratCount - 1), PROP_SHEET_PLATFORMS_COLS]);
                            _rRange.Value = oCurData;

                            _iRow += iStratCount;
                        }
                    }

                    // Schritt 2:
                    // > Zusammenfassungen sämtlicher Coins einfügen:
                    eCurSheet = GetWorksheet(PROP_SHEET_CURRENCIES_NAME);

                    if (eCurSheet != null)
                    {
                        // Zeilen reservieren für Titel, Coins (ausgenommen 'CT_Invalid'):
                        oCurData = new object[(PROP_SHEET_ROW_DATA + (aCoinVals.GetLength(0) - 1) + 1), PROP_SHEET_CURRENCIES_COLS];

                        // Spalten erzeugen:
                        oCurData[PROP_SHEET_ROW_TITLES, PROP_SHEET_CURRENCIES_COL_COIN] = "Coin";
                        oCurData[PROP_SHEET_ROW_TITLES, PROP_SHEET_CURRENCIES_COL_PROPHIT] = PROP_TITLE_PROPHIT;

                        // Daten aufbereiten:
                        _iRow = PROP_SHEET_ROW_DATA;

                        foreach (BotCoinType _bType in aCoinVals)
                        {
                            if (_bType != BotCoinType.Invalid)
                            {
                                oCurData[_iRow, PROP_SHEET_CURRENCIES_COL_COIN] = UtlEnum.GetEnumDescription(_bType);
                                oCurData[_iRow, PROP_SHEET_CURRENCIES_COL_PROPHIT] = dCoinProphit[_bType];

                                _iRow++;
                            }
                        }

                        // Daten übertragen:
                        _rRange = eCurSheet.get_Range((Excel.Range)eCurSheet.Cells[(PROP_SHEET_ROW_TITLES + 1), 1], (Excel.Range)eCurSheet.Cells[(_iRow + 1), PROP_SHEET_CURRENCIES_COLS]);
                        _rRange.Value = oCurData;

                        return (true);
                    }
                }
            }
            else
            {
                // Fügt eine Liste von Zusammenfassungen ein.

                if (IsValid() == false)
                {
                    Strategy = bStrategy;
                    InitDocument("", String.Format("MM-kw{0}", UtlDateTime.GetWeekOfYear(DateTime.Now)));
                }

                if (IsValid())
                {
                    List<BotRiftSummary> rRifts = bStrategy.RiftSummaries.WeeklyRiftSummary;
                    Excel.Worksheet eCurSheet = CreateWorksheet(PROP_SHEET_SUMMARY_NAME);
                    Excel.Range _rRange;
                    object[,] oCurData;
                    int _iRow = PROP_SHEET_ROW_DATA;

                    // Zeilen reservieren für Titel, Daten, Summen:
                    oCurData = new object[(PROP_SHEET_ROW_DATA + 1 + rRifts.Count), PROP_SHEET_SUMMARY_COLS];

                    SetFormat_DateTime(eCurSheet, (PROP_SHEET_SUMMARY_COL_BEGIN + 1));
                    SetFormat_DateTime(eCurSheet, (PROP_SHEET_SUMMARY_COL_END + 1));

                    // Spalten erzeugen:
                    oCurData[PROP_SHEET_ROW_TITLES, PROP_SHEET_SUMMARY_COL_INDEX] = "Index";
                    oCurData[PROP_SHEET_ROW_TITLES, PROP_SHEET_SUMMARY_COL_BEGIN] = "Beginn";
                    oCurData[PROP_SHEET_ROW_TITLES, PROP_SHEET_SUMMARY_COL_END] = "Ende";
                    oCurData[PROP_SHEET_ROW_TITLES, PROP_SHEET_SUMMARY_COL_BUYS] = PROP_TITLE_BUYS;
                    oCurData[PROP_SHEET_ROW_TITLES, PROP_SHEET_SUMMARY_COL_SELLS] = PROP_TITLE_SELLS;
                    oCurData[PROP_SHEET_ROW_TITLES, PROP_SHEET_SUMMARY_COL_PROPHIT] = String.Format(FORMAT_UNIT, PROP_TITLE_PROPHIT, UtlEnum.GetEnumDescription(Strategy.WalletSec.Currency));
                    oCurData[PROP_SHEET_ROW_TITLES, PROP_SHEET_SUMMARY_COL_PROPHIT_EXCHANGE] = (PROP_TITLE_PROPHIT + " " + PROP_TITLE_EXCHANGE);
                    oCurData[PROP_SHEET_ROW_TITLES, PROP_SHEET_SUMMARY_COL_BUYAMOUNT] = String.Format(FORMAT_UNIT, PROP_TITLE_BUYAMOUNT, UtlEnum.GetEnumDescription(Strategy.WalletPrim.Currency));
                    oCurData[PROP_SHEET_ROW_TITLES, PROP_SHEET_SUMMARY_COL_BUYAVERAGERATE] = String.Format(FORMAT_UNIT, PROP_TITLE_BUYAVERAGERATE, UtlEnum.GetEnumDescription(Strategy.WalletSec.Currency));
                    oCurData[PROP_SHEET_ROW_TITLES, PROP_SHEET_SUMMARY_COL_SELLAMOUNT] = String.Format(FORMAT_UNIT, PROP_TITLE_SELLAMOUNT, UtlEnum.GetEnumDescription(Strategy.WalletPrim.Currency));
                    oCurData[PROP_SHEET_ROW_TITLES, PROP_SHEET_SUMMARY_COL_SELLAVERAGERATE] = String.Format(FORMAT_UNIT, PROP_TITLE_SELLAVERAGERATE, UtlEnum.GetEnumDescription(Strategy.WalletSec.Currency));

                    // Schritt 1:
                    // > Daten aufbereiten:
                    for (int i = 0; i < rRifts.Count; i++, _iRow++)
                    {
                        oCurData[_iRow, PROP_SHEET_SUMMARY_COL_INDEX] = (i + 1);
                        oCurData[_iRow, PROP_SHEET_SUMMARY_COL_BEGIN] = String.Format(FORMAT_DATETIME, rRifts[i].Begin);
                        oCurData[_iRow, PROP_SHEET_SUMMARY_COL_END] = String.Format(FORMAT_DATETIME, rRifts[i].End);
                        oCurData[_iRow, PROP_SHEET_SUMMARY_COL_BUYS] = rRifts[i].Buys.Orders;
                        oCurData[_iRow, PROP_SHEET_SUMMARY_COL_SELLS] = rRifts[i].Sells.Orders;
                        oCurData[_iRow, PROP_SHEET_SUMMARY_COL_PROPHIT] = rRifts[i].Prophit;
                        oCurData[_iRow, PROP_SHEET_SUMMARY_COL_PROPHIT_EXCHANGE] = rRifts[i].ProphitExchange;
                        oCurData[_iRow, PROP_SHEET_SUMMARY_COL_BUYAMOUNT] = rRifts[i].Buys.TotalAmount;
                        oCurData[_iRow, PROP_SHEET_SUMMARY_COL_BUYAVERAGERATE] = rRifts[i].Buys.TotalAverageRate;
                        oCurData[_iRow, PROP_SHEET_SUMMARY_COL_SELLAMOUNT] = rRifts[i].Sells.TotalAmount;
                        oCurData[_iRow, PROP_SHEET_SUMMARY_COL_SELLAVERAGERATE] = rRifts[i].Sells.TotalAverageRate;
                    }

                    // Daten übertragen:
                    _rRange = eCurSheet.get_Range((Excel.Range)eCurSheet.Cells[(PROP_SHEET_ROW_TITLES + 1), 1], (Excel.Range)eCurSheet.Cells[(PROP_SHEET_ROW_DATA + 1 + rRifts.Count), PROP_SHEET_SUMMARY_COLS]);
                    _rRange.Value = oCurData;

                    // Zeile für Summen erzeugen:
                    object[,] oFormula = new object[1, PROP_SHEET_SUMMARY_COLS];
                    string _sOp;

                    _iRow++;
                    _rRange = eCurSheet.get_Range((Excel.Range)eCurSheet.Cells[_iRow, 1], (Excel.Range)eCurSheet.Cells[_iRow, PROP_SHEET_SUMMARY_COLS]);

                    for (int i = PROP_SHEET_SUMMARY_COL_BUYS; i <= PROP_SHEET_SUMMARY_COL_SELLAVERAGERATE; i++)
                    {
                        _sOp = ((i == PROP_SHEET_SUMMARY_COL_BUYAVERAGERATE) || (i == PROP_SHEET_SUMMARY_COL_SELLAVERAGERATE)) ? "AVERAGE" : "SUM";
                        oFormula[0, i] = String.Format("={0}({1})", _sOp, GetColRangeName((i + 1), (PROP_SHEET_ROW_DATA + 1), (i + 1), (_iRow - 1)));
                    }

                    _rRange.Value2 = oFormula;
                    _rRange.EntireRow.Font.Bold = true;
                    _rRange.EntireRow.Borders[Excel.XlBordersIndex.xlEdgeTop].LineStyle = Excel.XlLineStyle.xlContinuous;

                    // Schritt 2:
                    // > Daten in Charts darstellen
                    Excel.Chart eCurChart = AddChart(Excel.XlChartType.xlLineMarkers, eCurSheet.Name);

                    _rRange = eCurSheet.get_Range((Excel.Range)eCurSheet.Cells[(PROP_SHEET_ROW_TITLES + 1), (PROP_SHEET_SUMMARY_COL_BUYS + 1)], (Excel.Range)eCurSheet.Cells[(PROP_SHEET_ROW_TITLES + 1 + rRifts.Count), (PROP_SHEET_SUMMARY_COL_PROPHIT + 1)]);
                    eCurChart.SetSourceData(_rRange, Excel.XlRowCol.xlColumns);

                    ((Excel.Series)eCurChart.SeriesCollection(1)).AxisGroup = Excel.XlAxisGroup.xlSecondary;
                    ((Excel.Series)eCurChart.SeriesCollection(2)).AxisGroup = Excel.XlAxisGroup.xlSecondary;

                    oCurData = null;
                    return (true);
                }
            }
            return (false);
        }
        #endregion
        #region Initialization
        protected override bool InitDocument(String sSubDocPath = "", String sFileTitleDateFormat = "")
        {
            String sFileTitle = "";

            if (Strategy == null)
                sSubDocPath = GetSubDocPath(tMode, PROP_PATH_TOTAL);

            if (sFileTitleDateFormat == "")
                sFileTitle = GetFileTitle(tMode, PROP_PATH_TOTAL);
            else
                // Dateititel via Base-Class ermitteln.
                // > Datumsformat für Dateititel definieren:
                this.sFileTitleDateFormat = sFileTitleDateFormat;

            return (base.InitDocument(sSubDocPath, sFileTitle));
        }
        #endregion


        #region Configuration
        internal const String PROP_PATH_TOTAL = "Total";

        internal const String PROP_SHEET_SUMMARY_NAME = "Zusammenfassung";
        internal const String PROP_SHEET_PLATFORMS_NAME = "Platforms";
        internal const String PROP_SHEET_CURRENCIES_NAME = "Currencies";

        internal const int PROP_SHEET_SUMMARY_COLS = 11;
        internal const int PROP_SHEET_SUMMARY_COL_INDEX = 0;
        internal const int PROP_SHEET_SUMMARY_COL_BEGIN = 1;
        internal const int PROP_SHEET_SUMMARY_COL_END = 2;
        internal const int PROP_SHEET_SUMMARY_COL_BUYS = 3;
        internal const int PROP_SHEET_SUMMARY_COL_SELLS = 4;
        internal const int PROP_SHEET_SUMMARY_COL_PROPHIT = 5;
        internal const int PROP_SHEET_SUMMARY_COL_PROPHIT_EXCHANGE = 6;
        internal const int PROP_SHEET_SUMMARY_COL_BUYAMOUNT = 7;
        internal const int PROP_SHEET_SUMMARY_COL_BUYAVERAGERATE = 8;
        internal const int PROP_SHEET_SUMMARY_COL_SELLAMOUNT = 9;
        internal const int PROP_SHEET_SUMMARY_COL_SELLAVERAGERATE = 10;

        internal const int PROP_SHEET_PLATFORMS_COLS = 15;
        internal const int PROP_SHEET_PLATFORMS_COL_PLATFORM = 0;
        internal const int PROP_SHEET_PLATFORMS_COL_STRATEGY = 1;
        internal const int PROP_SHEET_PLATFORMS_COL_BUYS = 2;
        internal const int PROP_SHEET_PLATFORMS_COL_SELLS = 3;
        internal const int PROP_SHEET_PLATFORMS_COL_PROPHIT_VAL = 4;
        internal const int PROP_SHEET_PLATFORMS_COL_PROPHIT_CURRENCY = 5;
        internal const int PROP_SHEET_PLATFORMS_COL_PROPHIT_EXCHANGE = 6;
        internal const int PROP_SHEET_PLATFORMS_COL_BUYAMOUNT_VAL = 7;
        internal const int PROP_SHEET_PLATFORMS_COL_BUYAMOUNT_CURRENCY = 8;
        internal const int PROP_SHEET_PLATFORMS_COL_BUYAVERAGERATE_VAL = 9;
        internal const int PROP_SHEET_PLATFORMS_COL_BUYAVERAGERATE_CURRENCY = 10;
        internal const int PROP_SHEET_PLATFORMS_COL_SELLAMOUNT_VAL = 11;
        internal const int PROP_SHEET_PLATFORMS_COL_SELLAMOUNT_CURRENCY = 12;
        internal const int PROP_SHEET_PLATFORMS_COL_SELLAVERAGERATE_VAL = 13;
        internal const int PROP_SHEET_PLATFORMS_COL_SELLAVERAGERATE_CURRENCY = 14;

        internal const int PROP_SHEET_CURRENCIES_COLS = 2;
        internal const int PROP_SHEET_CURRENCIES_COL_COIN = 0;
        internal const int PROP_SHEET_CURRENCIES_COL_PROPHIT = 1;
        #endregion


        private UtlDateTime.RepeatType tMode;
    }
    public class BotTradesExcelDoc : BotPermanentDayUsageExcelDoc
    {
        #region Constants
        protected const String TYPE_BUY = "Buy";
        protected const String TYPE_SELL = "Sell";
        #endregion


        public BotTradesExcelDoc(BotTradingStrategy bStrategy) : base(bStrategy, "Trades", UtlDateTime.RepeatType.SRT_Weekly)
        {
        }


        #region Management
        public override bool Execute(object oArgument)
        {
            BotBaseOrder oOrder = (BotBaseOrder)oArgument;

            if ((InitDocument(RepeatType)) && (IsValid()) && (Strategy.IsValid))
            {
                object[,] oCurData = null;
                int iIndex = 0;

                if (eDataSheet == null)
                {
                    const String sTitleBalance = "Guthaben";

                    // Initialisieren:
                    eDataSheet = CreateWorksheet(Category);
                    oCurData = new object[2, PROP_SHEET_TRADES_COLS];

                    SetFormat_DateTime(eDataSheet, (PROP_SHEET_TRADES_COL_TIME + 1));

                    // Spalten erzeugen:
                    oCurData[iIndex, PROP_SHEET_TRADES_COL_INDEX] = "Index";
                    oCurData[iIndex, PROP_SHEET_TRADES_COL_TIME] = "Zeit";
                    oCurData[iIndex, PROP_SHEET_TRADES_COL_TYPE] = "Typ";
                    oCurData[iIndex, PROP_SHEET_TRADES_COL_RATE] = "Kurs";
                    oCurData[iIndex, PROP_SHEET_TRADES_COL_AMOUNT] = String.Format(FORMAT_UNIT, "Betrag", UtlEnum.GetEnumDescription(Strategy.WalletPrim.Currency));
                    oCurData[iIndex, PROP_SHEET_TRADES_COL_PRICE] = String.Format(FORMAT_UNIT, "Preis", UtlEnum.GetEnumDescription(Strategy.WalletSec.Currency));
                    oCurData[iIndex, PROP_SHEET_TRADES_COL_EXCHANGE_PRICE] = PROP_TITLE_EXCHANGE;
                    oCurData[iIndex, PROP_SHEET_TRADES_COL_WALLET] = "Wallet";
                    oCurData[iIndex, PROP_SHEET_TRADES_COL_BALANCE_PRIM] = String.Format(FORMAT_UNIT, sTitleBalance, UtlEnum.GetEnumDescription(Strategy.WalletPrim.Currency));
                    oCurData[iIndex, PROP_SHEET_TRADES_COL_BALANCE_SEC] = String.Format(FORMAT_UNIT, sTitleBalance, UtlEnum.GetEnumDescription(Strategy.WalletSec.Currency));

                    // Charts erstellen:
                    eChart1 = AddChart(Excel.XlChartType.xlLineMarkers, Category);
                    eChart2 = AddChart(Excel.XlChartType.xlLineMarkers, sTitleBalance);

                    // Fortschritt speichern:
                    eWorkbook.Save();

                    iIndex++;
                }
                else
                {
                    oCurData = new object[1, PROP_SHEET_TRADES_COLS];

                    Debug.Assert((eChart1 != null), String.Format("[E667] Invalid Object '{0}'", nameof(eChart1)));
                    Debug.Assert((eChart2 != null), String.Format("[E668] Invalid Object '{0}'", nameof(eChart2)));
                }

                // Schritt 1:
                // > Daten aufbereiten:
                oCurData[iIndex, PROP_SHEET_TRADES_COL_INDEX] = (iCurDataset_Row - PROP_SHEET_ROW_DATA + 1 + iIndex);
                oCurData[iIndex, PROP_SHEET_TRADES_COL_TIME] = String.Format(FORMAT_DATETIME, oOrder.CreationTime);
                oCurData[iIndex, PROP_SHEET_TRADES_COL_TYPE] = (oOrder.IsSellOrder ? TYPE_SELL : TYPE_BUY);
                oCurData[iIndex, PROP_SHEET_TRADES_COL_RATE] = oOrder.dRate;
                oCurData[iIndex, PROP_SHEET_TRADES_COL_AMOUNT] = oOrder.FinalAmount;
                oCurData[iIndex, PROP_SHEET_TRADES_COL_PRICE] = oOrder.FinalPrice;
                oCurData[iIndex, PROP_SHEET_TRADES_COL_EXCHANGE_PRICE] = oOrder.ExchangePrice;
                oCurData[iIndex, PROP_SHEET_TRADES_COL_WALLET] = UtlEnum.GetEnumDescription(Strategy.WalletPrim.Type);
                oCurData[iIndex, PROP_SHEET_TRADES_COL_BALANCE_PRIM] = Strategy.WalletPrim.Funds;
                oCurData[iIndex, PROP_SHEET_TRADES_COL_BALANCE_SEC] = Strategy.WalletSec.Funds;

                // Daten übertragen:
                Excel.Range _rRange;

                iIndex++;

                _rRange = eDataSheet.get_Range((Excel.Range)eDataSheet.Cells[(iCurDataset_Row + 1), 1], (Excel.Range)eDataSheet.Cells[(iCurDataset_Row + iIndex), PROP_SHEET_TRADES_COLS]);
                _rRange.Value = oCurData;

                iCurDataset_Row += iIndex;

                // Schritt 2:
                // > Daten in Charts darstellen
                _rRange = eDataSheet.get_Range((Excel.Range)eDataSheet.Cells[(PROP_SHEET_ROW_TITLES + 1), (PROP_SHEET_TRADES_COL_RATE + 1)], (Excel.Range)eDataSheet.Cells[iCurDataset_Row, (PROP_SHEET_TRADES_COL_AMOUNT + 1)]);
                eChart1.SetSourceData(_rRange, Excel.XlRowCol.xlColumns);

                ((Excel.Series)eChart1.SeriesCollection(1)).AxisGroup = Excel.XlAxisGroup.xlSecondary;

                _rRange = eDataSheet.get_Range((Excel.Range)eDataSheet.Cells[(PROP_SHEET_ROW_TITLES + 1), (PROP_SHEET_TRADES_COL_BALANCE_PRIM + 1)], (Excel.Range)eDataSheet.Cells[iCurDataset_Row, (PROP_SHEET_TRADES_COL_BALANCE_SEC + 1)]);
                eChart2.SetSourceData(_rRange, Excel.XlRowCol.xlColumns);

                ((Excel.Series)eChart2.SeriesCollection(2)).AxisGroup = Excel.XlAxisGroup.xlSecondary;

                // Fortschritt speichern:
                eWorkbook.Save();

                return (true);
            }
            else
                return (false);
        }
        #endregion


        #region Configuration
        internal const int PROP_SHEET_TRADES_COLS = 10;

        internal const int PROP_SHEET_TRADES_COL_INDEX = 0;
        internal const int PROP_SHEET_TRADES_COL_TIME = 1;
        internal const int PROP_SHEET_TRADES_COL_TYPE = 2;
        internal const int PROP_SHEET_TRADES_COL_RATE = 3;
        internal const int PROP_SHEET_TRADES_COL_AMOUNT = 4;
        internal const int PROP_SHEET_TRADES_COL_PRICE = 5;
        internal const int PROP_SHEET_TRADES_COL_EXCHANGE_PRICE = 6;
        internal const int PROP_SHEET_TRADES_COL_WALLET = 7;
        internal const int PROP_SHEET_TRADES_COL_BALANCE_PRIM = 8;
        internal const int PROP_SHEET_TRADES_COL_BALANCE_SEC = 9;
        #endregion
    }


    #region Types.Base
    // Dauerhaft zu verwendende Excel-Dokumente.
    // > D.h. Inhalte werden kontinuierlich in das Dokument geschrieben bis dieses geschlossen wird wenn es nicht weiter verwendet wird.
    public class BotPermanentDayUsageExcelDoc : BotSingleUsageExcelDoc
    {
        protected BotPermanentDayUsageExcelDoc(BotTradingStrategy bStrategy, String sCategory, UtlDateTime.RepeatType rRepeatMode) : base(sCategory, bStrategy, "", false)
        {
            this.RepeatType = rRepeatMode;

            InitDocument(rRepeatMode);
        }


        #region Properties.Management
        [Browsable(false)]
        public bool IsExceeded
        {
            get
            {
                if (dCreated.Kind == DateTimeKind.Unspecified)
                    return (false);
                else
                    return (UtlDateTime.IsExceeded(dCreated, DateTime.Now, RepeatType));
            }
        }
        [Browsable(false)]
        public UtlDateTime.RepeatType RepeatType { private set; get; }
        #endregion


        #region Initialization
        protected bool InitDocument(UtlDateTime.RepeatType rRepeatMode)
        {
            if (Strategy == null)
            {
                Debug.Assert(false, "[ID745]");
                return (false);
            }
            else
            {
                String sSubDocPath = "";

                if (rRepeatMode > UtlDateTime.RepeatType.SRT_Monthly)
                    sSubDocPath = GetSubDocPath(rRepeatMode, Category);

                sFileTitleDateFormat = GetFileTitleDateFormat(rRepeatMode);

                return (InitDocument(sSubDocPath, ""));
            }
        }
        #endregion
        #region Initialization
        protected override bool InitDocument(String sSubDocPath = "", String sFileTitle = "")
        {
            if (dCreated.Kind == DateTimeKind.Unspecified)
            {
                // (Neues) Dokument initialisieren.
            }
            else if (IsExceeded)
                // Aktuelles Dokument schließen:
                Close();
            else
                return (true);

            if (base.InitDocument(sSubDocPath, sFileTitle))
            {
                eDataSheet = GetWorksheet(Category);

                // Ggf. Existierenden Fortschritt aus Dokument lesen:
                eChart1 = GetChart(1);
                eChart2 = GetChart(2);
                eChart3 = GetChart(3);
                iCurDataset_Row = GetLastRow(eDataSheet);

                dCreated = DateTime.Now.Date;

                // Anwendung unsichtbar halten:
                var tThread = new Thread(KeepAppInvisible);
                tThread.Start();

                return (true);
            }
            else
                return (false);
        }
        #endregion
        #region Management
        private void KeepAppInvisible()
        {
            // (BETA) ...
            // Wenn ein anderes Excel-Dokument geöffnet wird, werden alle Dokumente von dieser Funktion unsichtbar gemacht :(
            // > Evtl. kann man das noch trennen!? Hab mich (noch) nicht weiter damit beschäftigt.
            return;


            while (true)
            {
                Thread.Sleep(1000);

                if (IsValid())
                    eApp.Visible = false;
                else
                    break;
            }
        }
        #endregion


        protected Excel.Worksheet eDataSheet;
        protected Excel.Chart eChart1 = null;
        protected Excel.Chart eChart2 = null;
        protected Excel.Chart eChart3 = null;
        protected int iCurDataset_Row = -1;

        private DateTime dCreated;
    }
    // Einmalig zu verwendende Excel-Dokumente.
    // > D.h. Inhalte werden in das Dokument geschrieben. Anschließend wird das Dokument geschlossen und nicht weiter verwendet.
    public class BotSingleUsageExcelDoc
    {
        #region Constants
        // Kategorien:
        public const String CATEGORY_CANDLES = "Candles";
        public const String CATEGORY_TRADES = "Trades";

        // Formate:
        public const String FORMAT_TIMESPAN = "{0:dd\\,HH\\:mm\\:ss}";
        public const String FORMAT_DATETIME = "{0:dd.MM.yyyy, HH\\:mm\\:ss}";

        // Titel:
        internal const String PROP_TITLE_BUYS = "Einkäufe";
        internal const String PROP_TITLE_SELLS = "Verkäufe";
        internal const String PROP_TITLE_PROPHIT = "Profit";
        internal readonly String PROP_TITLE_EXCHANGE = String.Format(FORMAT_UNIT, "Gegenwert", UtlEnum.GetEnumDescription(BotTradingStrategy.TRADE_EXCHANGE_CURRENCY));
        internal const String PROP_TITLE_BUYAMOUNT = "Einkaufsbetrag";
        internal const String PROP_TITLE_BUYAVERAGERATE = "Einkaufsdurchschnitt";
        internal const String PROP_TITLE_SELLAMOUNT = "Verkaufsbetrag";
        internal const String PROP_TITLE_SELLAVERAGERATE = "Verkaufsdurchschnitt";
        #endregion


        /// <summary>
        /// </summary>
        /// <param name="sCategory"></param>
        /// <param name="bStrategy"></param>
        /// <param name="sFileTitleDateFormat">Format des Datumsteils des Dateinamens (z.B. "yyyy-MM-dd").</param>
        /// <param name="bDeleteSheets"></param>
        protected BotSingleUsageExcelDoc(String sCategory, BotTradingStrategy bStrategy = null, String sFileTitleDateFormat = "yyyy", bool bDeleteSheets = true)
        {
            this.bStrategy = bStrategy;
            this.Category = sCategory;
            this.sFileTitleDateFormat = sFileTitleDateFormat;
            this.bDeleteSheets = bDeleteSheets;
            this.sDefaultWorksheet = PROP_SHEET_CHARTS_NAME;
        }
        ~BotSingleUsageExcelDoc()
        {
        }


        #region Properties.Manegement
        public Excel.ChartObjects ChartObjects
        {
            get
            {
                Debug.Assert((eDefaultSheet != null), String.Format("[CO890] Invalid Object '{0}'", nameof(eDefaultSheet)));
                return (Excel.ChartObjects)eDefaultSheet.ChartObjects(Type.Missing);
            }
        }
        #endregion
        #region Properties
        [Browsable(false)]
        public BotTradingStrategy Strategy
        {
            set
            {
                if (bStrategy == null)
                    bStrategy = value;
                else
                    Debug.Assert(false, "[SS667] Referenz bereits initialisiert!");
            }
            get { return bStrategy; }
        }
        [Browsable(false)]
        public String Category { private set; get; }
        #endregion


        #region Initialization   
        /// <summary>
        /// Initialisiert das Dokument.
        /// </summary>
        /// <param name="sSubDocPath">Unterverzeichnis von kategorisiertem Report-Verzeichnis (Optional).</param>
        /// <param name="sFileTitle">Titel (OHNE Erweiterung) der Datei (Optional).</param>
        /// <returns></returns>
        protected virtual bool InitDocument(String sSubDocPath = "", String sFileTitle = "")
        {
            
            // (BETA) ... [Disabled Excel]
            // Excel.Interop stürzt immer ab und das nervt.... muss noch gegen OpenXmlDoc getauscht werden.
            BotEnv.LogSystem.RemoveLogEntry(BotEnv.LogSystem);
            BotEnv.LogSystem.WriteLogVolatile("ExcelDoc", BotLogProcess.eGeneral, "(BETA) ... Alle Excel-Funktionen vorübergehend deaktiviert.", BotEnv.LogSystem);
            return (false);


            DateTime dCurDate = DateTime.Now;
            String sDocPath, sPltName = "", sPairSymbol = "";

            if (bStrategy != null)
            {
                sPltName = bStrategy.Platform.PlatformName;
                sPairSymbol = bStrategy.TradingPair.Symbol;
            }

            if (sSubDocPath == "")
            {
                Debug.Assert((bStrategy != null), "[ID704]");
                sDocPath = String.Format("{0}{1}\\{2}\\{3}\\{4}\\{5}", BotEnv.Configuration.Reports_Directory, Category.ToUpper(), sPltName, sPairSymbol, dCurDate.Year, dCurDate.Month);
            }
            else
                sDocPath = String.Format("{0}{1}\\{2}", BotEnv.Configuration.Reports_Directory, Category.ToUpper(), sSubDocPath);

            Directory.CreateDirectory(sDocPath);

            if (eApp == null)
                eApp = new Excel.Application();

            if (eApp != null)
            {
                int iIndex = 0;
                bool bExistingDoc;
                String _sFilePath;

                eApp.Visible = false;
                eApp.DisplayAlerts = false;

                if (sFileTitle == "")
                {
                    Debug.Assert((bStrategy != null), "[ID724]");
                    Debug.Assert((sFileTitleDateFormat != ""), "[ID710]");
                    _sFilePath = String.Format(("{0}\\{1}.{2}.{3}.{4:" + sFileTitleDateFormat + "}"), sDocPath, sPltName, sPairSymbol, Category, dCurDate);
                }
                else
                    _sFilePath = String.Format(("{0}\\{1}"), sDocPath, sFileTitle);

                while (true)
                {
                    sFilePath = _sFilePath;
                    if (iIndex != 0)
                        // Index anwenden falls Dateipfad nicht verfügbar sein sollte:
                        sFilePath += String.Format(("({0})"), iIndex);
                    sFilePath += ".xlsx";

                    try
                    {
                        bExistingDoc = File.Exists(sFilePath);
                        if ((bDeleteSheets) && (bExistingDoc))
                        {
                            File.Delete(sFilePath);
                            bExistingDoc = false;
                        }

                        if (bExistingDoc)
                            // Ggf. vorhandenes Dokument öffnen:
                            eWorkbook = eApp.Workbooks.Open(sFilePath, Type.Missing, false, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, true);
                        else
                            // Neues Dokument erstellen:
                            eWorkbook = eApp.Workbooks.Add(oMissingVal);

                        eWorksheets = eWorkbook.Worksheets;

                        // Standard-Sheet erstellen:
                        eDefaultSheet = GetWorksheet(sDefaultWorksheet);

                        if (eDefaultSheet == null)
                            eDefaultSheet = CreateWorksheet(sDefaultWorksheet);

                        // Dokument speichern:
                        eWorkbook.SaveAs(sFilePath);

                        break;
                    }
                    catch (COMException cExcpt)
                    {
                        Console.WriteLine("Excel.Except [ID842] {0}", cExcpt.Message);
                        if ((uint)cExcpt.ErrorCode != 0x800A03EC)
                        {
                            Debug.Assert(false, "[ID433] " + cExcpt.Message);
                            return (false);
                        }
                    }
                    catch (IOException iExcpt)
                    {
                        Console.WriteLine("Excel.Except [ID852] {0}", iExcpt.Message);
                    }

                    // Alternativen Dateinamen versuchen:
                    iIndex++;

                    Close();
                }

                Debug.Assert((oMissingVal != null), String.Format("[ID1082] Invalid Object '{0}'", nameof(oMissingVal)));
                Debug.Assert((eDefaultSheet != null), String.Format("[ID1083] Invalid Object '{0}'", nameof(eDefaultSheet)));
                Debug.Assert((eWorkbook != null), String.Format("[ID1084] Invalid Object '{0}'", nameof(eWorkbook)));
                Debug.Assert((eWorksheets != null), String.Format("[ID1085] Invalid Object '{0}'", nameof(eWorksheets)));
                return (true);
            }
            else
            {
                Debug.Assert((eApp != null), String.Format("[ID1098] Invalid Object '{0}'", nameof(eApp)));
                return (false);
            }
        }
        #endregion


        #region Management
        public virtual bool Execute(object oArgument)
        {
            // Implementation via SubClass!
            Debug.Assert(false, "[E723]");

            return (false);
        }
        // Schließt das aktuelle Dokument.
        public virtual void Close()
        {
            if (IsValid())
            {
                // Workbook schließen:
                eWorkbook.Close(true, sFilePath, oMissingVal);

                ReleaseObject(eDefaultSheet);
                ReleaseObject(eWorksheets);
                ReleaseObject(eWorkbook);

                eDefaultSheet = null;
                eWorksheets = null;
                eWorkbook = null;

                GC.Collect();
            }
        }
        #endregion
        #region Management
        public bool IsValid()
        {
            if (eWorkbook != null)
            {
                Debug.Assert((eApp != null), "[IV40]");
                return (true);
            }
            else
                return (false);
        }


        protected Excel.Worksheet CreateWorksheet(String sTitle = "Unnamed")
        {
            Excel.Worksheet eResult = GetWorksheet(sTitle);
            if (eResult == null)
            {
                object oInsertAfter = ((eDefaultSheet == null) ? oMissingVal : eDefaultSheet);

                eResult = (Excel.Worksheet)eWorksheets.Add(oMissingVal, oInsertAfter);
                if (eResult == null)
                    Debug.Assert(false, "[CW1102]");
                else
                {
                    Excel.Range _rRange;

                    eResult.Name = sTitle;

                    eResult = (Excel.Worksheet)eWorksheets.Add(oMissingVal, oInsertAfter);
                    _rRange = eResult.Cells[(PROP_SHEET_ROW_TITLES + 1), 1];

                    // Formatierung:
                    _rRange.EntireRow.Font.Bold = true;
                    _rRange.EntireRow.Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Excel.XlLineStyle.xlContinuous;

                    // Titel-Zeile einfrieren:
                    eResult.Activate();
                    // (BETA) ... Exception bei "Excel/FreezePanes"
                    // Zum testen auskommentiert
                    /*
                    eResult.Application.ActiveWindow.SplitRow = (PROP_SHEET_ROW_TITLES + 1);
                    eResult.Application.ActiveWindow.FreezePanes = true;
                    */
                }
            }
            return (eResult);
        }
        protected Excel.Chart AddChart(Excel.XlChartType eChartType, String sTitle)
        {
            Excel.Chart eCurChart = (ChartObjects.Add(0, iChart_PosY, PROP_SHEET_CHART_WIDTH, PROP_SHEET_CHART_HEIGHT)).Chart;

            Debug.Assert((eCurChart != null), String.Format("[AC1145] Invalid Object '{0}'", nameof(eCurChart)));

            eCurChart.HasTitle = true;
            eCurChart.ChartTitle.Text = sTitle;
            eCurChart.ChartType = eChartType;

            // Position für nächstes Chart definieren:
            iChart_PosY += PROP_SHEET_CHART_HEIGHT;

            return (eCurChart);
        }
        protected void WriteTitle(Excel.Worksheet eSheet, int iColumn, String sValue)
        {
            Excel.Range _rRange = eSheet.Cells[PROP_SHEET_ROW_TITLES, iColumn];

            _rRange.Value = sValue;
            _rRange.EntireRow.Font.Bold = true;
            _rRange.Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Excel.XlLineStyle.xlContinuous;
        }

        protected void SetFormat_TimeSpan(Excel.Worksheet eSheet, int iColumn)
        {
            Excel.Range _rRange = eSheet.Cells[1, iColumn];
            _rRange.EntireColumn.NumberFormat = "dd, HH:mm:ss";
        }
        protected void SetFormat_DateTime(Excel.Worksheet eSheet, int iColumn)
        {
            Excel.Range _rRange = eSheet.Cells[1, iColumn];
            _rRange.EntireColumn.NumberFormat = "dd-MM-yyyy HH:mm:ss";
        }
        #endregion


        #region Configuration
        internal const String PROP_SHEET_CHARTS_NAME = "Charts";

        internal const int PROP_SHEET_ROW_TITLES = 0;
        internal const int PROP_SHEET_ROW_DATA = 1;

        internal const int PROP_SHEET_CHART_WIDTH = 1024;
        internal const int PROP_SHEET_CHART_HEIGHT = 300;

        internal const String FORMAT_UNIT = "{0} [{1}]";
        #endregion


        #region Helper.Initialization
        protected String GetSubDocPath(UtlDateTime.RepeatType rRepeatMode, String sBaseFolder)
        {
            DateTime dCurDate = DateTime.Now;

            // Dateipfad ermitteln:
            sBaseFolder = sBaseFolder.ToUpper();

            switch (rRepeatMode)
            {
                case UtlDateTime.RepeatType.SRT_Daily:
                case UtlDateTime.RepeatType.SRT_Weekly:
                case UtlDateTime.RepeatType.SRT_Monthly:
                    return (String.Format("{0}\\{1}\\{2}", sBaseFolder, dCurDate.Year, dCurDate.Month));

                case UtlDateTime.RepeatType.SRT_Yearly:
                    return (String.Format("{0}\\{1}", sBaseFolder, dCurDate.Year));

                case UtlDateTime.RepeatType.SRT_Ever:
                    return (String.Format("{0}", sBaseFolder));

                default:
                    Debug.Assert(false, "[GSDP563] Ungültiger Typ!");
                    break;
            }

            return ("");
        }
        protected String GetFileTitleDateFormat(UtlDateTime.RepeatType rRepeatMode)
        {
            DateTime dCurDate = DateTime.Now;

            switch (rRepeatMode)
            {
                case UtlDateTime.RepeatType.SRT_Daily:
                    return (String.Format("{0:yyyy-MM-dd}", dCurDate));

                case UtlDateTime.RepeatType.SRT_Weekly:
                    return (String.Format("{0:yyyy-MM}-kw{1}", dCurDate, UtlDateTime.GetWeekOfYear(dCurDate)));

                case UtlDateTime.RepeatType.SRT_Monthly:
                    return (String.Format("{0:yyyy-MM}", dCurDate));

                case UtlDateTime.RepeatType.SRT_Yearly:
                    return (String.Format("{0:yyyy}", dCurDate));

                case UtlDateTime.RepeatType.SRT_Ever:
                    break;

                default:
                    Debug.Assert(false, "[GFTDF590] Ungültiger Typ!");
                    break;
            }

            return ("");
        }
        protected String GetFileTitle(UtlDateTime.RepeatType rRepeatMode, String sBaseFolder)
        {
            return (String.Format("{0}.{1}.{2}", sBaseFolder, Category, GetFileTitleDateFormat(rRepeatMode)));
        }
        #endregion
        #region Helper.Application
        protected void ReleaseObject(object oObject)
        {
            if (oObject != null)
            {
                try
                {
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(oObject);
                    oObject = null;
                }
                catch (Exception eExcp)
                {
                    oObject = null;
                    Debug.Assert(false, "[RO117] " + eExcp.ToString());
                }
                finally
                {
                }
            }
        }
        protected Excel.Worksheet GetWorksheet(String sName)
        {
            Excel.Worksheet _wSheet;

            Debug.Assert((eWorksheets != null), String.Format("[GW1276] Invalid Object '{0}'", nameof(eWorksheets)));

            for (int i = 1; i <= eWorksheets.Count; i++)
            {
                _wSheet = eWorksheets[i];
                if (_wSheet.Name == sName)
                    return (_wSheet);
            }
            return (null);
        }
        protected Excel.Chart GetChart(int iChart)
        {
            Excel.ChartObjects cCharts = ChartObjects;

            if ((1 <= iChart) && (iChart <= cCharts.Count))
                return ((cCharts.Item(iChart)).Chart);
            else
                return (null);
        }
        protected int GetLastRow(Excel.Worksheet eSheet)
        {
            if (eSheet != null)
            {
                Excel.Range rLast = eSheet.Cells.SpecialCells(Excel.XlCellType.xlCellTypeLastCell, Type.Missing);

                return (rLast.Row);
            }
            else
                return (PROP_SHEET_ROW_TITLES);
        }
        // Ermittelt den Spalten-Namen (z.B. '1=A', '30=AD')
        public static string GetColName(int iIndex)
        {
            int iBaseVal = Convert.ToInt32('A');
            int iColNum = iIndex - 1;
            string sResult = "";

            if (iIndex > 26)
                sResult = GetColName(iColNum / 26);

            return (sResult + Convert.ToChar(iBaseVal + (iColNum % 26)));
        }
        public static string GetColRangeName(int iX1, int iY1, int iX2, int iY2)
        {
            string sX1 = GetColName(iX1);
            string sX2 = ((iX1 == iX2) ? sX1 : GetColName(iX2));

            return (String.Format("{0}{1}:{2}{3}", sX1, iY1, sX2, iY2));
        }
        #endregion


        protected static object oMissingVal = System.Reflection.Missing.Value;
        protected static Excel.Application eApp = null;

        protected Excel.Worksheet eDefaultSheet = null;
        protected Excel.Workbook eWorkbook = null;
        protected Excel.Sheets eWorksheets = null;

        protected String sFileTitleDateFormat;
        protected String sDefaultWorksheet;


        private BotTradingStrategy bStrategy;
        private String sFilePath;
        private bool bDeleteSheets;

        private int iChart_PosY = 0;
    }


    // Thread zum Bearbeiten von Excel-Dokumenten.
    public class BotExcelDocThread
    {
        public BotExcelDocThread(BotSingleUsageExcelDoc sRefExcelDoc, object oArgument = null, int iGroupID = -1, RunWorkerCompletedEventHandler OnCompletedHandler = null)
        {
            Debug.Assert((sRefExcelDoc != null), "[EDT1019]");

            lock (BotThreadLock.Object[BotThreadLock.OBJ_EXCEL_DOC])
            {
                lInstances.Add(this);
            }

            // Thread initialisieren:
            Thread tThread = new Thread(OnExecute);

            tThread.IsBackground = false;
            tThread.Name = nameof(BotExcelDocThread);

            // Job initialisieren:
            this.sRefExcelDoc = sRefExcelDoc;
            this.GroupID = iGroupID;
            this.OnCompletedHandler += OnCompletedHandler;

            tThread.Start(oArgument);
        }


        #region Properties.Management
        public static int ActiveCount { get { return (lInstances.Count); } }
        #endregion
        #region Properties
        public int GroupID { private set; get; }
        #endregion


        #region Management
        protected void OnExecute(object oArgument)
        {
            BotTaskState.Use(BotTaskState.BotClass.eExcelDoc);

            // Vorgang ausführen:
            bool bResult = sRefExcelDoc.Execute(oArgument);

            if (sRefExcelDoc.GetType().IsSubclassOf(typeof(BotPermanentDayUsageExcelDoc)) == false)
                // Excel-Dokument ist für einmalige Anwendung vorgesehen.
                // > Dokument schließen:
                sRefExcelDoc.Close();

            lock (BotThreadLock.Object[BotThreadLock.OBJ_EXCEL_DOC])
            {
                lInstances.Remove(this);
            }

            BotTaskState.Unuse(BotTaskState.BotClass.eExcelDoc);

            // Benachrichtung:
            if (OnCompletedHandler != null)
                OnCompletedHandler(this, new RunWorkerCompletedEventArgs(bResult, null, false));

            // Speicher freigeben:
            GC.Collect();
        }
        #endregion
        #region General
        public static int GetUnusedGroupID()
        {
            return (iUnusedGroupID++);
        }
        /// <summary>
        /// Ermittelt alle aktiven Threads.
        /// </summary>
        /// <param name="iGroupID">Begrenzt das Ergebnis auf eine bestimmte Gruppe.</param>
        /// <returns></returns>
        public static int GetActiveThreads(int iGroupID = -1)
        {
            int iCount = 0;

            if (iGroupID == -1)
                iCount = lInstances.Count;
            else
            {
                foreach (BotExcelDocThread eThread in lInstances)
                {
                    if (eThread.GroupID == iGroupID)
                        iCount++;
                }
            }

            return (iCount);
        }
        #endregion


        private BotSingleUsageExcelDoc sRefExcelDoc;
        private RunWorkerCompletedEventHandler OnCompletedHandler;

        private static int iUnusedGroupID = 0;
        private static List<BotExcelDocThread> lInstances = new List<BotExcelDocThread>();                 // Anzahl an aktiven Threads
    }
    #endregion
}
