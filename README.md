# Crypto Rift Bot
Welcome to _Crypto Rift Bot_ - the open source trading bot for crypto currencies.

[[_TOC_]]


## Screenshots
Typical **view** of application's **main window**
![screenshot](https://gitlab.com/cryptomates/cryptoriftbot/-/wikis/uploads/d50baafdc3401e3c7b7062036a9068ce/image.png "Screenshot application")


## Requirements
- **Application**
  - Windows 7 or higher
  - Microsoft .net Framework v4.7.2

## Participate
Your welcome if you want to join the project in any way you like to :relieved:

**Communication**
- Feel free to join our :pager: [telegram channel](https://t.me/joinchat/DW_mCNFP7apbZnOi).


## Topology
The trading bot is divided into the following components:
- [System](https://gitlab.com/cryptomates/cryptoriftbot/-/wikis/Components/System)
- [Services](https://gitlab.com/cryptomates/cryptoriftbot/-/wikis/Components/Services)
- [Application](https://gitlab.com/cryptomates/cryptoriftbot/-/wikis/Components/Application)


## Requirements
- **Application**
  - Windows 7 or higher
  - Microsoft .net-framework v4.7.2


# Jobs & To do's
There is quite a lot of work do be done... :smirk: :clap:

- **Trading**
  - Inventing more [trading strategies](https://gitlab.com/cryptomates/cryptoriftbot/-/wikis/Trading/Strategy).
  - Creating more [coin stream records](https://gitlab.com/cryptomates/cryptoriftbot/-/wikis/Components/Application/Features/Coin-Stream-Record) for testing purposes.

- **Documentation**
  - There will be lots of [wiki](https://gitlab.com/cryptomates/cryptoriftbot/-/wikis/home) articles that can be written...

- **Development**
  - Implement more [services](https://gitlab.com/cryptomates/cryptoriftbot/-/wikis/Components/Services).
    - New [trading platforms](https://gitlab.com/cryptomates/cryptoriftbot/-/wikis/Components/Services/Platform).
    - Some [ticker-](https://gitlab.com/cryptomates/cryptoriftbot/-/wikis/Components/Services/Ticker), [candle-](https://gitlab.com/cryptomates/cryptoriftbot/-/wikis/Components/Services/Candle) or [exchange-rate-services](https://gitlab.com/cryptomates/cryptoriftbot/-/wikis/Components/Services/Exchange-Rate)

  - **System**
    - Make **.net-core** compatible (e.g. to be able to run on linux systems).
    - Port the current [Excel-implementation](https://gitlab.com/cryptomates/cryptoriftbot/-/wikis/Components/System/Features/Excel-Documents) (_Microsoft.Office.Interop_) into **open-xml** format.

  - **Applications / GUIs**
    - **Web-Frontend** (e.g. HTML5?)

- **Miscellaneous**
  - **Graphic arts** (Like _icons_ and _pictures_)


# Current status
Application is currently in re-design phase.
Since it was separated into the [described components](#Topology) some of the functionalities got broken. Also lots of stuff has to be tested as well.

Current mission is to accomplish the re-design phase to reach the old stability and go on with extending the trading bot.
